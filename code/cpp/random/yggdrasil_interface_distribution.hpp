#pragma once
/**
 * \brief	THis file defines an interface for the generation
 * 		 of random numbers that are not covered by the
 * 		 C++ standard.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>
#include <core/yggdrasil_PDFValueArray.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <random/yggdrasil_random.hpp>


//Including std
#include <fstream>
#include <memory>
#include <string>


//Including Eigen
#include <Eigen/Core>


YGGDRASIL_NS_START(yggdrasil)

/**
 * \brief	This enum is for caracterizing the distributions
 * \enum 	eDistiType
 *
 * This enum is used for the factory functions.
 */
enum class eDistiType
{
	None,
	Gauss,
	Dirichlet,
	Outlier,	//This is the outlier distribution, a sum of two gaussians
	Uniform, 	//Uniform distributions
	Gamma,		//The Gamma Distribution
	Beta,		//The Beta Distribution
}; //End enum: eDistiType







/**
 * \brief	Generall interface for all the distributions in Yggdrasil.
 * \class 	yggdrasil_randomDistribution_i
 *
 * This class provides an interface that is used for the generation
 * of the random numbers. It is not located in the interface folder
 * since the generation of random numbers is more tithly
 * bound to the testing functionality.
 *
 * The user only needs to implement some functions to get a fully working
 * distribution:
 * 	- wh_generateSample:	Generate a single sample.
 * 	- wh_pdf:		Caclualte the PDF of a sample.
 * 	- sampleDomain:		This is the domain where the sample lies, can be invalid, if unbound.
 * 				 This fucntion is optinal, the default is return an invalid hyüper cube.
 * 	- getMean:		Returns the mean of the distribution, is a vector of type MeanVector_t.
 * 	- getCoVar:		Retrurns the covariance of the distribution, is of type CopVArMat_t.
 * 				 For 1D distribution this is a 1x1 matric, that is the variance.
 *
 * All other functions of the interface will be reduced to these fucntions.
 * For the numpy integration, they also support functions that returns an Eigen type.
 *
 * This class uses the hook concept, this also ensures concistent behaviour over all distributions.
 * It has some special behaviour that the user might not expect at first glance.
 * First of all it supports a smapling domain.
 * The base class will querry the deriving class until a sample is generated that lies in that domain, if the
 * domain is valid, in that case no restriction is applied.
 * For performance the derived calss should do that itself.
 * Also all samples that are generated, will have a non zero probability.
 * Since we operate on finite precission, this can be an issue, so this si avoided.
 * If a probability is nan or infinity an error is generated.
 * Infinity is technically possible but Yggdrasil will not allow that.
 */
class yggdrasil_randomDistribution_i
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using Sample_t 		= ::yggdrasil::yggdrasil_sample_t;	//!< This is thge type for the sample
	using SampleList_t 	= ::yggdrasil::yggdrasil_sampleList_t;	//!< This is the type for a list of samples
	using SampleArray_t	= ::yggdrasil::yggdrasil_arraySample_t;	//!< This is a space efficient way to stoirage a slot of sampels.
	using SampleCollection_t= ::yggdrasil::yggdrasil_sampleCollection_t;	//!< This is the sample collection type
	using PDFValueArray_t 	= yggdrasil_PDFValueArray_t<Numeric_t>;	//!< This is the type for representing many values of pdfs.
	using ValueArray_t	= PDFValueArray_t;			//!< This is a compability layer, do not use it.
	using PDFValueArrayEigen_t = ::Eigen::VectorXd;			//!< THis is the type for returning Eigen vectors.

	using HyperCube_t 	= yggdrasil_hyperCube_t;		//!< This is the class that describes the domain.
	using IntervalBound_t	= HyperCube_t::IntervalBound_t;

	using pRNGenerator_t	= ::yggdrasil::yggdrasil_pRNG_t;	//!< This is the type for the random number generator

	using Distribution_t 	= yggdrasil_randomDistribution_i;
	using Distribution_ptr 	= std::unique_ptr<Distribution_t>;	//!< This is the pointer for the factory method

	//Statistical information
	using MeanVec_t 	= ::Eigen::VectorXd;		//!< This is the mean of the distribution.
	using CoVarMat_t 	= ::Eigen::MatrixXd;		//!< This si the covariance of the distribution.
	using Index_t 		= MeanVec_t::Index;		//!< This is the index type used inside Eigen.

	/**
	 * \brief	Factory return type.
	 *
	 * This is the return type of the factroy fucntion.
	 * as foirst it contains a unique pointer that
	 * contains an insatnce of the
	 * distribution, and as second memeber
	 * it contains a string that names the distribution.
	 *
	 * Tne name is not ment for humans.
	 * it is more as an name extension, it is guaranteed
	 * that it does not contain any spaces or other
	 * funny things.
	 */
	using FactoryRet_t 	= ::std::pair<Distribution_ptr, ::std::string>;


	/*
	 * ======================
	 * Factory
	 */
public:
	/**
	 * \brief	This function offers a factroy like method, to
	 * 		 create the standard distributions.
	 *
	 * These distributions are taken directly from the paper.
	 * This allows defining them once in one point
	 *
	 * \param  dType	This is the type opf the distribution.
	 * \param  dim		This is the dimension, actually it is more like a code
	 * 			 That allows to ask for many different distributions.
	 *
	 * \throw	If the distribution is not known.
	 *
	 * \note	This function is implemented in the file
	 * 		 "random/yggdrasil_stdDistributions.cpp".
	 */
	static
	FactoryRet_t
	FACTORY(
		const eDistiType 	dType,
		const Int_t 		dim);


	/**
	 * \brief	This is a colne function.
	 *
	 * This function returns a copy of *this.
	 * It is like the java fucntion.
	 * It allows calling a copy cvonstructor without the need
	 * of knnowing the exact type.
	 */
	virtual
	Distribution_ptr
	clone()
	 const
	 = 0;





	/*
	 * ======================
	 * Constructors
	 *
	 * All constructor except the destructor are defaulted
	 * and protected.
	 */
public:
	/**
	 * \brief	This is the destructor.
	 *
	 * It is defaulted.
	 */
	virtual
	~yggdrasil_randomDistribution_i();


protected:
	/**
	 * \brief	This is teh copy constrructor
	 *
	 * It is defaulted.
	 */
	yggdrasil_randomDistribution_i(
		const yggdrasil_randomDistribution_i&);


	/**
	 * \brief	The copy assignment operator.
	 *
	 * Is defaulted.
	 */
	yggdrasil_randomDistribution_i&
	operator= (
		const yggdrasil_randomDistribution_i&);


	/**
	 * \brief	The move constructor
	 *
	 * Is defaulted.
	 */
	yggdrasil_randomDistribution_i(
		yggdrasil_randomDistribution_i&&);


	/**
	 * \brief	The move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_randomDistribution_i&
	operator= (
		yggdrasil_randomDistribution_i&&);


	/**
	 * \brief	The default constructor.
	 *
	 * Defaulted.
	 */
	yggdrasil_randomDistribution_i();


	/*
	 * ======================
	 * ======================
	 * Interface fucntions
	 *
	 * Allmost all of the interface fucntions,
	 * that deals with the generation of random
	 * numbers have a default implementation.
	 * Since it is possible to reduce all of them
	 * to a single fucntion.
	 *
	 * This is not particular efficient, since it
	 * requers some class that could be avoided,
	 * but will speed up the implementation.
	 */
public:


	/*
	 * =====================
	 * =====================
	 * Random number functions
	 */
public:

	/*
	 * ======================
	 * Single sample
	 */
public:
	/**
	 * \brief	Generate a sample, that is described by *this.
	 *
	 * \param  Generator	The RNG that is used to generate the sample.
	 *
	 * \note	Thsi function provides a default implementation.
	 */
	virtual
	Sample_t
	generateSample(
		pRNGenerator_t&		generator);


	/**
	 * \brief	This function computes the pdf at the sample location.
	 *
	 * This function basically evaluates the probablity density function.
	 * For syntactical reason this function
	 * has a default implementation, that calls the implementaion.
	 * For efficient reasons it can be overwritten.
	 *
	 * \param  x	The position where to evaluate the density function.
	 *
	 * \note	You can do it, but the default implementations
	 * 		 Will rely on the wh_pdf() function.
	 *
	 */
	virtual
	Real_t
	pdf(
		const Sample_t&		x)
	 const;


	/*
	 * ===================
	 * SampleList functions
	 */
public:
	/**
	 * \brief	This function generates many samples.
	 *
	 * This function is provided for convenience.
	 *
	 * \param  generator	The RNG that should be used.
	 * \param  n		The number of samples that should be generated.
	 *
	 * \note	This fucntion provides a default implementation.
	 */
	virtual
	SampleList_t
	generateSamplesList(
		pRNGenerator_t&		generator,
		const Size_t 		N);


	/**
	 * \brief	This function evaluates teh pdf for all samples.
	 *
	 * This function does not work on a single sample,
	 * but on a sample list
	 * It also returns a value array.
	 *
	 * \param  samples	The sample list, that should be executed.
	 */
	virtual
	PDFValueArray_t
	pdf(
		const SampleList_t&	sList)
	 const;


	/**
	 * \brief	This function generates a sample list and
	 * 		 the pdfs of many samples in one step
	 *
	 * This function does not generate a sample array but a list.
	 * Thus it needs more space.
	 *
	 * This fucntion provides a default implementation.
	 *
	 * \param  generator	This is the random nunmber generator that is used.
	 * \param  N		The number of samples that should be used.
	 */
	virtual
	::std::pair<SampleList_t, PDFValueArray_t>
	generateSamplesListPDF(
		pRNGenerator_t&		generator,
		const Size_t 		N);


	/**
	 * \brief	This function evaluates teh pdf for all samples.
	 *
	 * This function is similar to the pdf(const SampleList&) function
	 * but this one returns an Eigen type.
	 * This si needed for the numpy integration.
	 *
	 * \param  samples	The sample list, that should be executed.
	 */
	virtual
	PDFValueArrayEigen_t
	pdf_Eigen(
		const SampleList_t&	sList)
	 const;


	/**
	 * \brief	This function generates a sample list and
	 * 		 the pdfs of many samples in one step
	 *
	 * Thsi funciton returns an Eigen type.
	 *
	 * \param  generator	This is the random nunmber generator that is used.
	 * \param  N		The number of samples that should be used.
	 */
	virtual
	::std::pair<SampleList_t, PDFValueArrayEigen_t>
	generateSamplesListPDF_Eigen(
		pRNGenerator_t&		generator,
		const Size_t 		N);



	/*
	 * =======================
	 * SampleArray Functions
	 */
public:
	/**
	 * \brief	This function is a bit more space efficient
	 * 		 when generating many samples.
	 *
	 * The generateSamples() function generate a list of samples, which is basically am
	 * array of arrays. It is conceptioonaly nice, but not very space efficient.
	 * For tackle this problem, this function,
	 * which returns a SampleArray_t object is provided.
	 *
	 * \param  generator	The random number generator that is used.
	 * \param  N		The number of samples that should be generated.
	 *
	 * \note	This funciton provides a default implementation.
	 */
	virtual
	SampleArray_t
	generateSamplesArray(
		pRNGenerator_t&		generator,
		const Size_t 		N);


	/**
	 * \brief	This function evaluates teh pdf for all samples.
	 *
	 * This function does not work on a single sample,
	 * but on a sample array.
	 * It also returns a value array.
	 *
	 * \param  samples	The sample array, that should be executed.
	 */
	virtual
	PDFValueArray_t
	pdf(
		const SampleArray_t&	sArray)
	 const;


	/**
	 * \brief	This function generates an array fo sampels and the
	 * 		 corresponding PDFs in one step.
	 *
	 * This function generates the samples and their corresponding pdfs.
	 *
	 * This function has a default implementation.
	 *
	 * \param  generator	This is the random nunmber generator that is used.
	 * \param  N		The number of samples that should be used.
	 */
	virtual
	::std::pair<SampleArray_t, PDFValueArray_t>
	generateSamplesArrayPDF(
		pRNGenerator_t&		generator,
		const Size_t 		N);


	/**
	 * \brief	This function evaluates teh pdf for all samples.
	 *
	 * This function does not work on a single sample,
	 * but on a sample array.
	 * This function returns an Eigen type.
	 *
	 * \param  samples	The sample array, that should be executed.
	 */
	virtual
	PDFValueArrayEigen_t
	pdf_Eigen(
		const SampleArray_t&	sArray)
	 const;


	/**
	 * \brief	This function generates an array fo sampels and the
	 * 		 corresponding PDFs in one step.
	 *
	 * This function generates the samples and their corresponding pdfs.
	 * This function returns the pdfs in an Eigen type.
	 *
	 * \param  generator	This is the random nunmber generator that is used.
	 * \param  N		The number of samples that should be used.
	 */
	virtual
	::std::pair<SampleArray_t, PDFValueArrayEigen_t>
	generateSamplesArrayPDF_Eigen(
		pRNGenerator_t&		generator,
		const Size_t 		N);


	/*
	 * =====================
	 * Sample Collection functions
	 */
public:
	/**
	 * \brief	This function generates N samples and stores them in a sample collection.
	 *
	 * The advantage of this fucntion is that the data is in a format that can be
	 * easaly loaded into the tree.
	 *
	 * \param  generator	The RNG that should be used.
	 * \param  N		The number of samples that should be generated.
	 */
	virtual
	SampleCollection_t
	generateSamplesCollection(
		pRNGenerator_t&		generator,
		const Size_t 		N);


	/**
	 * \brief	This function evaluates teh pdf for all samples.
	 *
	 * This function does not work on a single sample,
	 * but on a sample collection.
	 * It also returns a value array.
	 *
	 * \param  samples	The sample collection, that should be executed.
	 */
	virtual
	PDFValueArray_t
	pdf(
		const SampleCollection_t&	sColl)
	 const;


	/**
	 * \brief	This function generates an collection of sampels and the
	 * 		 corresponding PDFs in one step.
	 *
	 * This function generates the samples and their corresponding pdfs.
	 *
	 * This function has a default implementation.
	 *
	 * \param  generator	This is the random nunmber generator that is used.
	 * \param  N		The number of samples that should be used.
	 */
	virtual
	::std::pair<SampleCollection_t, PDFValueArray_t>
	generateSamplesCollectionPDF(
		pRNGenerator_t&		generator,
		const Size_t 		N);


	/**
	 * \brief	This function evaluates teh pdf for all samples.
	 *
	 * This function does not work on a single sample,
	 * but on a sample collection.
	 * This function returns an Eigen type.
	 *
	 * \param  samples	The sample collection, that should be executed.
	 */
	virtual
	PDFValueArrayEigen_t
	pdf_Eigen(
		const SampleCollection_t&	sColl)
	 const;


	/**
	 * \brief	This function generates an collection of sampels and the
	 * 		 corresponding PDFs in one step.
	 *
	 * This function generates the samples and their corresponding pdfs.
	 * This function returns an Eigen type.
	 *
	 * \param  generator	This is the random nunmber generator that is used.
	 * \param  N		The number of samples that should be used.
	 */
	virtual
	::std::pair<SampleCollection_t, PDFValueArrayEigen_t>
	generateSamplesCollectionPDF_Eigen(
		pRNGenerator_t&		generator,
		const Size_t 		N);



	/*
	 * ========================
	 * ========================
	 * Other functions
	 */
public:
	/*
	 * =======================
	 * Distribution infos.
	 *
	 * Thsi fucntions returns information about the status of the distribution.
	 * The distribution is about multi dimensional distribution.
	 * So they are defined one eigen vectors.
	 */
public:
	/**
	 * \brief	Thsi returns the mean of the distribution.
	 *
	 * This function returns a vector (Eigen::VectorXd).
	 * In the case of a 1D distribution, the vector has length one.
	 */
	virtual
	MeanVec_t
	getMean()
	 const
	 = 0;


	/**
	 * \brief	This function returns the covariance matrix of the distribution.
	 *
	 * This is comparable to the variance.
	 * And in a one dimensional distribution, the fucntion should return a 1x1 Matrix,
	 * and its entry should be the variance.
	 */
	virtual
	CoVarMat_t
	getCoVar()
	 const
	 = 0;



	/*
	 * ====================
	 * Domain functions
	 */
public:
	/**
	 * \brief	This function returns the
	 * 		 dimensionality of the underling samples.
	 *
	 * This function has a default implementation,
	 * that gets the cube and queries it.
	 * Not in the case of an invalid domain, the user
	 * has to specify the domain.
	 */
	virtual
	Size_t
	nDims()
	 const;


	/**
	 * \brief	This function returns the sample domain.
	 *
	 * This is the doamin where samples could be generated.
	 * If the domain is unbounded, with respect to the
	 * computer numbers, of corse, this function shall return
	 * an invalid domain.
	 *
	 * The user has to implement this fucntion.
	 *
	 * \note	This function returns a copy of teh
	 * 		 cupe, so it is potentialy expensive.
	 */
	virtual
	HyperCube_t
	getSampleDomain()
	 const
	 = 0;


	/**
	 * \brief	This is some sugar for thesting if the
	 * 		 domain is unbounded.
	 *
	 * The default is that the getSampleDomain() is called
	 * and checked if the returned domain is invalid.
	 * This is inefficient and it is advised, to
	 * implkement it corecly.
	 */
	virtual
	bool
	isUnbounded()
	 const;





	/*
	 * =====================
	 * Dump fucntion
	 *
	 * This funciton allows to dump the state of the
	 * internal state.
	 * It is important that this does not same the parameter.
	 * it only recreates the internal stet of te ::std
	 * components.
	 */
public:
	/**
	 * \brief	This fucntion writes the internal distribution state,
	 * 		 but not the parameter to the given dump file.
	 *
	 * The default calls the dumpToStream() fucntion.
	 *
	 * \param  dumpFile	The file we dump to.
	 */
	virtual
	void
	dumpToFile(
		const std::string& 	dumpFile);

	/**
	 * \brief	This fucntion writes the internal distribution state,
	 * 		 but not the parameter to the given stream
	 *
	 * The default throws.
	 *
	 * \param  dumpStream	The strim to write to.
	 */
	virtual
	std::ostream&
	dumpToStream(
		std::ostream&	dumpStream);


	/**
	 * \brief	This fucntion loads the state, but not the parameters,
	 * 		 that where previously dumped, from the dump file.
	 *
	 * The default calls the loadFrimStream() function
	 *
	 * \param  dumpFile	The dump file to load.
	 */
	virtual
	void
	loadFromFile(
		const std::string&	dumpFile);

	/**
	 * \brief	This fucntion loads the state, but not the parameters,
	 * 		 that where previously dumped, from the stream.
	 *
	 * The default implementation throws.
	 *
	 * \param  dumpStream	The stream we load from
	 */
	virtual
	std::istream&
	loadFromStream(
		std::istream&		dumpStream);


	/*
	 * =====================
	 * Work Horse functions
	 *
	 * The functionality of allmost all fucntions
	 * can be reduced to some very simple function.
	 *
	 * Doing it this way, is not so efficient form
	 * a runtime perspective, but saves time when
	 * implemeting many differet distributions.
	 */
protected:
	/**
	 * \brief	This function generates one sample that is
	 * 		 distributed as the implementation.
	 *
	 * Note that this function does not return by value,
	 * but the return value is passed as an argument.
	 *
	 * \param  generator		This is the random generator
	 * \param  sample		This is the sample that should be generated.
	 */
	virtual
	void
	wh_generateSample(
		pRNGenerator_t&		generator,
		Sample_t*		sample)
	 = 0;


	/**
	 * \brief	This function return the pdf of the sample.
	 *
	 * \param  sample	The pdf to compute
	 */
	virtual
	Real_t
	wh_pdf(
		const Sample_t&		sample)
	 const
	 = 0;

	/*
	 * ======================
	 * Friends
	 */
private:

	/**
	 * \brief	This is an internal helper function for the sampling generation.
	 *
	 * \param  self		A reference to the distribution object itself.
	 * \param  generator 	The genrator that should be used.
	 * \param  N 		The number of samples that should be used.
	 * \param  samples	The container where the samples should be safed to.
	 * \param  pdfs 	A pointer to the pdf vector that should be manibulated.
	 * 			 If nullptr, the output will be ignored.
	 *
	 * \tparam  Conatiner_t		The type of the sample container.
	 * \tparam  pdfVector_t		This is the type that is used to returns the pdf vectors.
	 * 				 It must support operator[](i), size() and resize(), which is not
	 * 				 requiered to preserve the values.
	 */
	template<
		class 	Container_t,
		class 	pdfVector_t
	>
	friend
	void
	ygInternal_generateManySamples(
		yggdrasil_randomDistribution_i& 	self,
		pRNGenerator_t&				generator,
		Size_t 					N,
		Container_t* const 			samples,
		pdfVector_t* const 			pdfs);



	/**
	 * \brief	This function evaluates all the samples based in the given container
	 *
	 * \param  self		A reference to the distribution object itself.
	 * \param  samples	The container where the samples should be prcessed
	 *
	 * \tparam  Conatiner_t	This is teh container that is used
	 * \tparam  pdfVector_t		This is the type that is used to returns the pdf vectors.
	 * 				 It must support operator[](i), size() and resize(), which is not
	 * 				 requiered to preserve the values.
	 */
	template<
		class 	Container_t,
		class 	pdfVector_t
	>
	friend
	pdfVector_t
	ygInteranl_evaluateManySamples(
		const yggdrasil_randomDistribution_i& 	self,
		const Container_t& 			samples);
}; //End: class(yggdrasil_randomDistribution_i)




YGGDRASIL_NS_END(yggdrasil)


