#pragma once
/**
 * \brief	This file implements a true multimodal distribution.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>


//Including std


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)


/**
 * \brief	This class implements multi modal distributions.
 * \class	yggdrasil_multiModalDistribution_t
 *
 * A mulrtimodal distribution is a distribution that is composed of the linear
 * combination of iother distributins.
 * In a mathematical language this is:
 * 	f(x) \sum_i \omega_i f_i(x)
 * 	where \sum_i \omega_i == 1
 *
 * This means tha the distribution is the composition of may other distributions.
 *
 * This class is completly agnostic against the underlings distributions.
 *
 * \note	The implementation is very inefficient.
 *
 */
class yggdrasil_multiModalDistribution_t final : public yggdrasil_randomDistribution_i
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using Super_t 		= yggdrasil_randomDistribution_i;	//!< This is the interface.

	//This is the vector to store the mode distributions
	using ModeVector_t 	= ::std::vector<Distribution_ptr>;	//!< This ios the type that stores the distributions of *this, they are managed pointers
	using WeightVector_t 	= yggdrasil_valueArray_t<Numeric_t>;	//!< This is the type that stores the weights of the distribution
	using ModeSelector_t	= std::discrete_distribution<Size_t>;	//!< This is the type that is used to select the mode.


	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * It takes a vector of distribution pointers, lets say n.
	 * The distributions are cloned with the clone function of the interface.
	 * The weight vector must have length n - 1, and its components,
	 * all of them must be positive, and their summ must be less than one.
	 * The last weight is then estimated by *this.
	 * The sampling if the sampling domain is invalid it is tried to estimate it
	 * form the provided distributions, if at most one of them has an invalid domain,
	 * then the sampling domain of *this is also invalid.
	 * The domain is tried to be estimated from merging the domains.
	 *
	 * \param  modes	This is the distributions.
	 * \param  weights 	This is the weight vector, must be positive, and summ less than one, and one less than distributions.
	 * \param  domain 	This is the sampling domain.
	 *
	 */
	yggdrasil_multiModalDistribution_t(
		const ModeVector_t& 		modes,
		const WeightVector_t&		weights,
		const HyperCube_t&		domain);

	/**
	 * \brief	Building constructor.
	 *
	 * This constructor takes no domain argumnent.
	 * It is first tried to estimate the domain of *this,
	 * by merging the domain of the provided distributions.
	 * If one of them is invalid, then the domain of *this is set to invalid as well.
	 *
	 * \param  modes	This is the distributions.
	 * \param  weights 	This is the weight vector, must be positive, and summ less than one, and one less than distributions.
	 * \param  domain 	This is the sampling domain.
	 *
	 */
	yggdrasil_multiModalDistribution_t(
		const ModeVector_t& 		modes,
		const WeightVector_t&		weights);


	/**
	 * \brief	This is the destructor.
	 *
	 * It is defaulted.
	 */
	~yggdrasil_multiModalDistribution_t();


	/**
	 * \brief	This is a colne function.
	 *
	 * This function returns a copy of *this.
	 * It is like the java fucntion.
	 */
	virtual
	Distribution_ptr
	clone()
	 const
	 final;


	/**
	 * \brief	This is teh copy constrructor
	 *
	 * It is not defaulted.
	 * But behaves as if, the distributions will be cloned.
	 */
	yggdrasil_multiModalDistribution_t(
		const yggdrasil_multiModalDistribution_t&);


	/**
	 * \brief	The copy assignment operator.
	 *
	 * This constructor will make a copy of its argument
	 * and then move assigne the copy of *this.
	 */
	yggdrasil_multiModalDistribution_t&
	operator= (
		const yggdrasil_multiModalDistribution_t&);


	/**
	 * \brief	The move constructor
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiModalDistribution_t(
		yggdrasil_multiModalDistribution_t&&);


	/**
	 * \brief	The move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiModalDistribution_t&
	operator= (
		yggdrasil_multiModalDistribution_t&&);

	/*
	 * ====================
	 * Private constructords
	 */
private:
	/**
	 * \brief	The default constructor.
	 *
	 * Is private and default like.
	 * THe problem is that the sampling domain does not
	 * support a default constructor.
	 * So it must be done manually.
	 */
	yggdrasil_multiModalDistribution_t();


	/*
	 * =====================
	 * Random number functions
	 */
public:
	/*
	 * No functions here, since all of the defaults
	 * are used.
	 *
	 * If we would list them we would need to
	 * explicitly code that the default are used.
	 */


	/*
	 * ======================
	 * Statistical parameters
	 */
public:
	/**
	 * \brief	This function gives the mean of *this.
	 *
	 * This is the mean that the distribution has.
	 *
	 * The mean of the multimodal distribution is the weighted
	 * sum of the mean of the underling distributions.
	 */
	virtual
	MeanVec_t
	getMean()
	 const
	 final;


	/**
	 * \brief	This fucntion returns the covariance matrix of *this.
	 *
	 * The calculations are relatively complex, see "https://math.stackexchange.com/questions/147497/covariance-matrix-of-mixture-distribution"
	 * for more information.
	 */
	virtual
	CoVarMat_t
	getCoVar()
	 const
	 final;


	/**
	 * \brief	This function returns the weight vector.
	 *
	 * Note that unlike the weight vector that was passed to this upon construction,
	 * this one contains a weight for all modes.
	 */
	virtual
	const WeightVector_t&
	getWeights()
	 const
	 final;

	/**
	 * \brief	This function returns the weight vector, as an Eigen type.
	 *
	 * Note that unlike the weight vector that was passed to this upon construction,
	 * this one contains a weight for all modes.
	 *
	 * \note	This function is meant for Python.
	 */
	virtual
	MeanVec_t
	getWeightsEigen()
	 const
	 final;


	/*
	 * ===================
	 * Domain functions
	 */
public:
	/**
	 * \brief	THis function reetruns the dimension the generated samples has.
	 */
	virtual
	Size_t
	nDims()
	 const
	 final;


	/**
	 * \brief	Returns true if this is undounded.
	 */
	virtual
	bool
	isUnbounded()
	 const
	 final;


	/**
	 * \brief	This function returns the domain, in which the
	 * 		 sampleing is restricted.
	 *
	 * If given this the cube returned by this fucntion is the second
	 * Argument of the constructor.
	 * Notice that if tone the support domain was given at construction,
	 * i.e. the constrctor that only takes one argument, the support domain
	 * is returned.
	 *
	 */
	virtual
	HyperCube_t
	getSampleDomain()
	 const
	 final;


	/*
	 * ===================
	 * Workhorse interface
	 */
protected:

	/**
	 * \brief	This function generates one sample that is
	 * 		 distributed as the implementation.
	 *
	 * This randomly selects a distribution and then calls the undeling implementation.
	 *
	 * \param  generator		This is the random generator
	 * \param  sample		This is the sample that should be generated.
	 */
	virtual
	void
	wh_generateSample(
		pRNGenerator_t&		generator,
		Sample_t*		sample)
	 final;


	/**
	 * \brief	This function return the pdf of the sample.
	 *
	 * This is the weighted summ of the probabilities of all
	 * underling distributions.
	 *
	 * \param  sample	The pdf to compute
	 */
	virtual
	Real_t
	wh_pdf(
		const Sample_t&		sample)
	 const
	 final;

	/*
	 * ========================
	 * Dumping ffunction
	 *
	 * As stated in the interface, this only
	 * saves the state of the random distrinbution
	 * that are defined from the standard.
	 * Other parameter are not saved/loaded.
	 */
public:
	/**
	 * \brief	Write to a stream
	 */
	virtual
	std::ostream&
	dumpToStream(
		std::ostream&	dumpStream)
	 final;

	/**
	 * \brief	Load from a stream
	 */
	virtual
	std::istream&
	loadFromStream(
		std::istream&	dumpStream)
	 final;


	/*
	 * =======================
	 * Private Memeber
	 */
private:
	WeightVector_t 		m_weights;		//!< This is the vector that represents the weights of the different modes
	ModeVector_t 		m_modes;		//!< This vector stores the different modes.
	ModeSelector_t		m_selector;		//!< This is the distribution that is used to select the mode.
	HyperCube_t 		m_samplingDomain; 	//!< This is the sampling domainS
	Size_t 			m_nDims;		//!< This is the variable that stores the dimensionality of the problem.
}; //End: class(yggdrasil_multiModalDistribution_t)




YGGDRASIL_NS_END(yggdrasil)


