#pragma once
/**
 * \brief	This function implements a beta distribution
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>


//Including std


//Including Eigen


YGGDRASIL_NS_START(yggdrasil)


/**
 * \brief	This class allows a beta distribution.
 * \class	yggdrasil_betaDistri_t
 *
 * This is  class allows sample from a betta distribution.
 * The implementation follows:
 * 	- https://en.wikipedia.org/wiki/Beta_distribution
 * 	- https://en.wikipedia.org/wiki/Beta_distribution#Generating_beta-distributed_random_variates
 */
class yggdrasil_betaDistri_t final : public yggdrasil_randomDistribution_i
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using StdGammaDist_t 	= ::std::gamma_distribution<Numeric_t>;
	using Super_t 		= yggdrasil_randomDistribution_i;	//!< This is the interface.


	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This function uses the terminology of the C++ standard.
	 * Note that Wikipedia uses different symbols.
	 * The domain must be an onedimensional hypercube.
	 *
	 * \param  Alpha 	The shape value, must be greater than zero.
	 * \param  Beta		The also called shape value, must be greater than zero.
	 * \param  domain	The domain, is an interval, the lower bound must be greater than zero.
	 *
	 * \note	This class follows the wikipedia page.
	 * 		 Also the support is limited to the interval ]0, 1[.
	 * 		 domains can be greater, but will be truncatted.
	 */
	yggdrasil_betaDistri_t(
		const Numeric_t 	Alpha,
		const Numeric_t 	Beta,
		const HyperCube_t& 	domain);


	/**
	 * \brief	This is the building constructor.
	 *
	 * This function uses the terminology of the C++ standard.
	 * Note that Wikipedia uses different symbols.
	 *
	 * \param  Alpha 	The shape value, must be greater than zero.
	 * \param  Beta		The also called shape value, must be greater than zero.
	 * \param  domain	The domain, is an interval, the lower bound must be greater than zero.
	 *
	 * \note 	This function takes an interval instead of a hyper cube.
	 * 		 This constructor is only provided for backaards compability.
	 */
	yggdrasil_betaDistri_t(
		const Numeric_t 	Alpha,
		const Numeric_t 	Beta,
		const IntervalBound_t&	domain);


	/**
	 * \brief	This version of teh constructor uses an invalid domain.
	 *
	 * Same as above, but the standard interval is used, which is the unit interval.
	 */
	yggdrasil_betaDistri_t(
		const Numeric_t 	Alpha,
		const Numeric_t 	Beta);



	/**
	 * \brief	This is the destructor.
	 *
	 * It is defaulted.
	 */
	~yggdrasil_betaDistri_t();


	/**
	 * \brief	This is a colne function.
	 *
	 * This function returns a copy of *this.
	 * It is like the java fucntion.
	 */
	virtual
	Distribution_ptr
	clone()
	 const
	 final;


	/**
	 * \brief	This is teh copy constrructor
	 *
	 * It is defaulted.
	 */
	yggdrasil_betaDistri_t(
		const yggdrasil_betaDistri_t&);


	/**
	 * \brief	The copy assignment operator.
	 *
	 * Is defaulted.
	 */
	yggdrasil_betaDistri_t&
	operator= (
		const yggdrasil_betaDistri_t&);


	/**
	 * \brief	The move constructor
	 *
	 * Is defaulted.
	 */
	yggdrasil_betaDistri_t(
		yggdrasil_betaDistri_t&&);


	/**
	 * \brief	The move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_betaDistri_t&
	operator= (
		yggdrasil_betaDistri_t&&);


	/**
	 * \brief	The default constructor.
	 *
	 * Is deleted.
	 */
	yggdrasil_betaDistri_t()
	 = delete;


	/*
	 * =====================
	 * Random number functions
	 */
public:
	/*
	 * No functions here, since all of the defaults
	 * are used.
	 *
	 * If we would list them we would need to
	 * explicitly code that the default are used.
	 */


	/*
	 * ======================
	 * Statistical parameters
	 */
public:
	/**
	 * \brief	This function gives the mean of *this.
	 *
	 * This is the mean that the distribution has.
	 *
	 */
	virtual
	MeanVec_t
	getMean()
	 const
	 final;


	/**
	 * \broief	Returns the variance
	 */
	virtual
	CoVarMat_t
	getCoVar()
	 const
	 final;


	/**
	 * \brief	This function returns the Alpha Value of the distribution.
	 */
	virtual
	Numeric_t
	getAlpha()
	 const
	 final;


	/**
	 * \brief	This function returns the Beta value of the distribution.
	 */
	virtual
	Numeric_t
	getBeta()
	 const
	 final;


	/*
	 * ====================
	 * Domain information
	 */
public:
	/**
	 * \brief	THis function reetruns the dimension the generated samples has.
	 *
	 * This is trivially always one.
	 */
	virtual
	Size_t
	nDims()
	 const
	 final;


	/**
	 * \brief	Returns true if this is undounded.
	 */
	virtual
	bool
	isUnbounded()
	 const
	 final;


	/**
	 * \brief	This function returns the domain, in which the
	 * 		 sampleing is restricted.
	 *
	 * This function returns an invalid domain if the sampling is unbounded.
	 *
	 * Note that for compability reasons thsi fuction returns a hypercube.
	 */
	virtual
	HyperCube_t
	getSampleDomain()
	 const
	 final;


	/*
	 * ===================
	 * Workhorse interface
	 */
protected:

	/**
	 * \brief	This function generates one sample that is
	 * 		 distributed as the implementation.
	 *
	 * See in teh description above for how it works.
	 *
	 * \param  generator		This is the random generator
	 * \param  sample		This is the sample that should be generated.
	 */
	virtual
	void
	wh_generateSample(
		pRNGenerator_t&		generator,
		Sample_t*		sample)
	 final;


	/**
	 * \brief	This function return the pdf of the sample.
	 *
	 * This is the normal version of the exponential
	 * we are used.
	 *
	 * \param  sample	The pdf to compute
	 */
	virtual
	Real_t
	wh_pdf(
		const Sample_t&		sample)
	 const
	 final;

	/*
	 * ========================
	 * Dumping ffunction
	 *
	 * As stated in the interface, this only
	 * saves the state of the random distrinbution
	 * that are defined from the standard.
	 * Other parameter are not saved/loaded.
	 */
public:
	/**
	 * \brief	Write to a stream
	 */
	virtual
	std::ostream&
	dumpToStream(
		std::ostream&	dumpStream)
	 final;

	/**
	 * \brief	Load from a stream
	 */
	virtual
	std::istream&
	loadFromStream(
		std::istream&	dumpStream)
	 final;


	/*
	 * =======================
	 * Private Memeber
	 */
private:
	StdGammaDist_t 		m_gammaX;	//!< This is the distribution for the first gamma distribution
	StdGammaDist_t 		m_gammaY;	//!< This is the distribution for the second gamma distribution
	IntervalBound_t 	m_domain;
	Numeric_t 		m_normFac;	//This is the normalization factor; it is basically 1/B(\alpha, \beta)
}; //End: class(yggdrasil_betaDistri_t)




YGGDRASIL_NS_END(yggdrasil)


