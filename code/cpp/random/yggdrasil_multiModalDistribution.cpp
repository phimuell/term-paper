//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_multiModalDistribution.hpp>


//Including std
#include <random>


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)

yggdrasil_multiModalDistribution_t::yggdrasil_multiModalDistribution_t()
 :
  m_weights(),
  m_modes(),
  m_selector(),
  m_samplingDomain(HyperCube_t::MAKE_INVALID_CUBE()),
  m_nDims(0)
{};


yggdrasil_multiModalDistribution_t::yggdrasil_multiModalDistribution_t(
	const yggdrasil_multiModalDistribution_t& o)
 :
  m_weights(o.m_weights),
  m_modes(),				//Will be filled later
  m_selector(o.m_selector.param()),	//Init form the parameters, (can not be moved)
  m_samplingDomain(o.m_samplingDomain),
  m_nDims(o.m_nDims)
{
	yggdrasil_assert(m_nDims > 0);
	yggdrasil_assert(m_weights.empty() == false);
	yggdrasil_assert(o.m_modes.size() == o.m_weights.size()); //o.m_modes is giaranteed to be non empty, if this succeed
	yggdrasil_assert(m_selector.probabilities().size() == m_weights.size());
	yggdrasil_assert(std::all_of(
			m_weights.cbegin(), m_weights.cend(),
			[](const Numeric_t x) -> bool {return (isValidFloat(x) && (x > 0.0)) ? true : false;}));
	yggdrasil_assert(std::abs(std::accumulate(m_weights.cbegin(), m_weights.cend(), Numeric_t(0.0)) - 1.0) < Constants::EPSILON * 100);

	//
	//Copy the modes

	//Number of modes
	const Size_t nModes = m_modes.size();

	//Clone the modes
	m_modes.reserve(nModes);

	for(Size_t i = 0; i != nModes; ++i)
	{
		m_modes.push_back(o.m_modes.at(i)->clone());	//Will be aliased
	}; //End for(i): going through the modes

	yggdrasil_assert(m_modes.size() == o.m_modes.size());
}; //End copy constructor


yggdrasil_multiModalDistribution_t::~yggdrasil_multiModalDistribution_t()
 = default;


yggdrasil_multiModalDistribution_t::Distribution_ptr
yggdrasil_multiModalDistribution_t::clone()
 const
{
	return Distribution_ptr(new yggdrasil_multiModalDistribution_t(*this));
};

yggdrasil_multiModalDistribution_t::yggdrasil_multiModalDistribution_t(
	yggdrasil_multiModalDistribution_t&&)
 = default;


yggdrasil_multiModalDistribution_t&
yggdrasil_multiModalDistribution_t::operator= (
	yggdrasil_multiModalDistribution_t&&)
 = default;

yggdrasil_multiModalDistribution_t&
yggdrasil_multiModalDistribution_t::operator= (
	const yggdrasil_multiModalDistribution_t& o)
{
	//Make a copy of the argument
	yggdrasil_multiModalDistribution_t tmp(o);

	//Move assigne the tmp to *this
	*this = std::move(tmp);

	//Return the reference of *this
	return *this;
}; //End copy assignment

const yggdrasil_multiModalDistribution_t::WeightVector_t&
yggdrasil_multiModalDistribution_t::getWeights()
 const
{
	yggdrasil_assert(m_nDims > 0);
	yggdrasil_assert(m_weights.empty() == false);
	yggdrasil_assert(m_weights.size() == m_modes.size());

	return m_weights;
}; //End get weights

yggdrasil_multiModalDistribution_t::MeanVec_t
yggdrasil_multiModalDistribution_t::getWeightsEigen()
 const
{
	yggdrasil_assert(m_nDims > 0);
	yggdrasil_assert(m_weights.empty() == false);
	yggdrasil_assert(m_weights.size() == m_modes.size());

	//Get the numbers of modes, and thus weights
	const Size_t nModes = m_weights.size();

	//Generate the return type
	MeanVec_t eWeights(nModes);

	//Copy the weights manually
	for(Size_t i = 0; i != nModes; ++i)
	{
		eWeights[i] = m_weights[i];
	}; //ENd for(i):

	//Return the weights
	return eWeights;
}; //End: get weights as eigen type


yggdrasil_multiModalDistribution_t::MeanVec_t
yggdrasil_multiModalDistribution_t::getMean()
 const
{
	//Small consistency check
	yggdrasil_assert(m_nDims > 0);

	//Get the number of different distributions
	const Size_t N = m_modes.size();
	yggdrasil_assert(N == m_weights.size());	//The one less is only for the imput
	yggdrasil_assert(N > 0);

	//this is the vector that will hold the mean value
	MeanVec_t mean(m_nDims);
	mean.setZero();
	yggdrasil_assert((Size_t)mean.size() == m_nDims);

	//Going over the modes to get the mean
	for(Size_t i = 0; i != N; ++i)
	{
		yggdrasil_assert(m_modes.at(i)->nDims() == m_nDims);

		//This is the mean of distribution i
		const MeanVec_t mean_i   = m_modes[i]->getMean();
		const Numeric_t weight_i = m_weights.at(i);

		//Incooperate the mean
		mean += weight_i * mean_i;
	}; //End going over the mean

	return mean;
}; //End: getMean


yggdrasil_multiModalDistribution_t::CoVarMat_t
yggdrasil_multiModalDistribution_t::getCoVar()
 const
{
	/*
	 * Here we use the following formula.
	 * https://math.stackexchange.com/questions/147497/covariance-matrix-of-mixture-distributionuz
	 *
	 * Written down thsi vormula is
	 *
	 * 	\Sigma := \sum_k \Sigma_k \omega_k + \sum_k \omega_k (\mu_k^T \mu_k) - \mu^T \mu
	 */

	//Small consistency check
	yggdrasil_assert(m_nDims > 0);

	//Get the number of different distributions
	const Size_t N = m_modes.size();
	yggdrasil_assert(N == m_weights.size());	//The one less is only for the imput
	yggdrasil_assert(N > 0);

	//
	//Helper variables

	//This vector will be used as the total mean
	MeanVec_t Mean(m_nDims);
	Mean.setZero();
	yggdrasil_assert(Mean.size() == m_nDims);
	yggdrasil_assert(Mean.cols() == 1);
	yggdrasil_assert(Mean.rows() == m_nDims);

	//This variable will hold the first sumand, the weighted sum of teh covariance matrices
	CoVarMat_t SigmaK_sum(m_nDims, m_nDims);
	SigmaK_sum.setZero();
	yggdrasil_assert(SigmaK_sum.rows() == m_nDims);
	yggdrasil_assert(SigmaK_sum.cols() == m_nDims);

	//This will hold the weighted sum of the tensor product of the means
	CoVarMat_t MuMuTK_sum(m_nDims, m_nDims);
	MuMuTK_sum.setZero();
	yggdrasil_assert(MuMuTK_sum.rows() == m_nDims);
	yggdrasil_assert(MuMuTK_sum.cols() == m_nDims);

	//Going over the distributions to get all we need
	for(Size_t k = 0; k != N; ++k)
	{
		//Small test
		yggdrasil_assert(m_modes.at(k)->nDims() == m_nDims);

		//Get a reference to the distribution
		const Distribution_t& mode_k = *(m_modes.at(k));

		//Get the mean, sigma and weight that are associated with mode k
		const MeanVec_t  mean_k   = mode_k.getMean();
		const CoVarMat_t sigma_k  = mode_k.getCoVar();
		const Numeric_t  weight_k = m_weights.at(k);

		yggdrasil_assert(mean_k.rows()  == m_nDims);
		yggdrasil_assert(mean_k.size()  == m_nDims);
		yggdrasil_assert(sigma_k.rows() == m_nDims);
		yggdrasil_assert(sigma_k.cols() == m_nDims);
		yggdrasil_assert(isValidFloat(weight_k));
		yggdrasil_assert(weight_k > 0.0);
		yggdrasil_assert(Mean.size() == m_nDims);
		yggdrasil_assert(Mean.rows() == m_nDims);
		yggdrasil_assert(MuMuTK_sum.rows() == m_nDims);
		yggdrasil_assert(MuMuTK_sum.cols() == m_nDims);
		yggdrasil_assert(SigmaK_sum.rows() == m_nDims);
		yggdrasil_assert(SigmaK_sum.cols() == m_nDims);


		//
		//Now we compuet teh different components

		//Compute the mean
		Mean += weight_k * mean_k;

		//The tensor product of the meas
		MuMuTK_sum += (weight_k * mean_k) * mean_k.transpose();

		//The summ of the sigmas
		SigmaK_sum += weight_k * sigma_k;

		yggdrasil_assert(Mean.size() == m_nDims);
		yggdrasil_assert(Mean.rows() == m_nDims);
		yggdrasil_assert(MuMuTK_sum.rows() == m_nDims);
		yggdrasil_assert(MuMuTK_sum.cols() == m_nDims);
		yggdrasil_assert(SigmaK_sum.rows() == m_nDims);
		yggdrasil_assert(SigmaK_sum.cols() == m_nDims);
	}; //End for(k):


	//Now compute the final value
	CoVarMat_t Sigma(m_nDims, m_nDims);
	Sigma = SigmaK_sum + MuMuTK_sum - Mean * Mean.transpose();
	yggdrasil_assert(Sigma.rows() == m_nDims);
	yggdrasil_assert(Sigma.cols() == m_nDims);

	return Sigma;
}; //End: get covar


Size_t
yggdrasil_multiModalDistribution_t::nDims()
 const
{
	yggdrasil_assert(m_modes.empty() == false);
	yggdrasil_assert(m_modes.front()->nDims() == m_nDims);

	return m_nDims;
}; //End getDim


bool
yggdrasil_multiModalDistribution_t::isUnbounded()
 const
{
	return m_samplingDomain.isValid() == false;
};


yggdrasil_multiModalDistribution_t::HyperCube_t
yggdrasil_multiModalDistribution_t::getSampleDomain()
 const
{
	return m_samplingDomain;
};


yggdrasil_multiModalDistribution_t::yggdrasil_multiModalDistribution_t(
	const ModeVector_t& 		modes,
	const WeightVector_t&		weights)
 :
  yggdrasil_multiModalDistribution_t(modes, weights, HyperCube_t::MAKE_INVALID_CUBE())
{};


yggdrasil_multiModalDistribution_t::yggdrasil_multiModalDistribution_t(
	const ModeVector_t& 		modes,
	const WeightVector_t&		weights,
	const HyperCube_t&		domain)
 :
  yggdrasil_multiModalDistribution_t()	//Calling the default, we will fill up the dat later
{
	//The number of modes
	const Size_t nModes = modes.size();
	if(nModes <= 1)
	{
		throw YGGDRASIL_EXCEPT_InvArg("There are less than two modes that are passed to the constructor.");
	};
	if(nModes != (weights.size() + 1))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The number of weights is not correct.");
	};

	//
	//Now loads and test the weights

	//This is the tentative value of the last wehight that is not passed to this.
	Numeric_t lastWeight = 1.0;

	//Allocate space for the last value
	m_weights.reserve(nModes);

	//Going throght the weights and compute the alst one and check them
	for(Size_t i = 0; i != (nModes - 1); ++i)
	{
		//This is the current weight
		const Numeric_t weight_i = weights.at(i);

		//Check if the weight is positive
		if(weight_i <= 0.0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("One of the weights is too small.");
		};
		yggdrasil_assert(isValidFloat(weight_i) == true);

		//Update the last value
		lastWeight -= weight_i;

		//Check if the last weight has become too small
		if(lastWeight <= 0.0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The weights are to large, they summ up to more than one.");
		};

		//Store the last weight
		m_weights.push_back(weight_i);
	}; //End for(i): handling the modes
	yggdrasil_assert(lastWeight > 0.0);
	yggdrasil_assert(isValidFloat(lastWeight));

	//Store the last weight in the vector
	m_weights.push_back(lastWeight);
	yggdrasil_assert(m_weights.size() == nModes);

	//
	//test if the passed domain is valid
	const bool isGivenDomainValid = domain.isValid();

	//If the domain is valid we must assigne it to ther meber
	//We will chgeck its dimensions later
	if(isGivenDomainValid == true)
	{
		m_samplingDomain = domain;
	};


	//
	//Handling the modes

	//Allocate space for the modes
	m_modes.reserve(nModes);

	//Going through the domain and handling it
	for(Size_t k = 0; k != nModes; ++k)
	{
		//Clone the domain
		Distribution_ptr mode_k = modes[k]->clone();

		//In the first step we must load the dimabnion
		//in othe other steps we must check the dimenbsion
		if(k == 0)
		{
			//First step just load
			m_nDims = mode_k->nDims();
		}
		else
		{
			//Not the first mode, wo we have to check
			if(mode_k->nDims() != m_nDims)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The modes have different dimansions.");
			};
		}; //End else: not first mode

		//Now handling the domain
		//We only habe to do that if the poassed domain is not valid
		//Since then no estimation is needed
		if(isGivenDomainValid == false)
		{
			//In the fiorst step we have to do sometzhing differently
			//We just have to assimulate the domain
			//It does not mather if it is valid or not
			if(k == 0)
			{
				//just assimilate
				yggdrasil_assert(m_samplingDomain.isValid() == false);
				m_samplingDomain = mode_k->getSampleDomain();

				//We can not do any checks
			}
			else
			{
				//We have to merge, if one of the underling distributions
				//Had an invalid domain the merge operation will take care of this.
				m_samplingDomain = m_samplingDomain.mergeWith(mode_k->getSampleDomain());
			}; //End else: mergeing
		}; //End if: the given domain is invalid

		yggdrasil_assert(m_modes.size() == k);

		//Now we save the mode in the container
		m_modes.push_back(std::move(mode_k));
	};  //End for(k): going through the modes
	yggdrasil_assert(m_modes.size() == nModes);

	//
	//Selector
	//
	//We can use the constructorfor that
	m_selector = ModeSelector_t(m_weights.cbegin(), m_weights.cend());
}; //End: building constructor



Real_t
yggdrasil_multiModalDistribution_t::wh_pdf(
	const Sample_t&		x)
 const
{
	yggdrasil_assert(m_modes.empty() == false);
	yggdrasil_assert(m_modes.front()->nDims() == m_nDims);

	if((Size_t)x.nDims() != m_nDims)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of *this and the passed samples do not agree.");
	};
	yggdrasil_assert(x.isValid());

	//Get the number of modes
	const Size_t N = m_modes.size();

	//This is the probability
	Real_t pdfSum = 0.0;

	//We must iterate through the modes to sum up the different probabilities
	for(Size_t k = 0; k != N; ++k)
	{
		//Get the weight
		const Numeric_t weight_k = m_weights.at(k);

		//Evaluate the pdf ofmode k at that point
		const Real_t pdf_k = m_modes.at(k)->pdf(x);
		yggdrasil_assert(isValidFloat(pdf_k));
		yggdrasil_assert(pdf_k >= 0.0);

		//Now supp up
		pdfSum += weight_k * pdf_k;
	}; //End for(k):

	//Return the sum of teh pdf
	return pdfSum;
}; //End: generate PDF


void
yggdrasil_multiModalDistribution_t::wh_generateSample(
	pRNGenerator_t&		generator,
	Sample_t*		s)
{
	yggdrasil_assert(m_modes.empty() == false);
	yggdrasil_assert(m_modes.front()->nDims() == m_nDims);

	//Select the mode
	const Size_t sMode_idx = m_selector(generator);

	//This is a reference to the mode
	Distribution_t& selectedMode = *(m_modes.at(sMode_idx));

	//Test if the domain is valid
	const bool isDomainValid = m_samplingDomain.isValid();

	//This are stravation protection
	const Size_t maxTries = 10000;	//This is the maximum tries we have
	Size_t       tries    = 0;	//This si the try counter

	do
	{
		//Increase the try count
		tries += 1;

		//test if to many tries where made
		if(tries >= maxTries)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The maximaum number of tries where used to sample a sample.");
		};

		//Generate a smaple
		//The other distributions guarantees us that the pdf of the samples are numbericaly positive.
		*s = selectedMode.generateSample(generator);
		yggdrasil_assert(s->isValid());

		//Compute the probability
		yggdrasil_assert(selectedMode.pdf(*s) > 0.0);	//Because the sample function of the interface guarantees numerically greater as
								//Zero we can/must requiere that the probability of the sample is greater han zero

	}
	while(
		(isDomainValid && m_samplingDomain.isInside(*s) == false)		//In the case of a valid domain, we requiere that the smaplie is inside
	);

	//End of process
	return;
}; //End generate a single sample


std::ostream&
yggdrasil_multiModalDistribution_t::dumpToStream(
	std::ostream&	s)
{
	//First dump the selctor to the stream
	s << m_selector;

	//Now dump all the other states to the stream
	for(Size_t k = 0; k != m_modes.size(); ++k)
	{
		m_modes.at(k)->dumpToStream(s);
	}; //End for(k):

	return s;
}; //ENd dump to


std::istream&
yggdrasil_multiModalDistribution_t::loadFromStream(
	std::istream&	s)
{
	//We first load the selctor
	s >> m_selector;

	//Now we load the mode states
	for(Size_t k = 0; k != m_modes.size(); ++k)
	{
		m_modes.at(k)->loadFromStream(s);
	}; //End for(k):

	return s;
}; //End load:





YGGDRASIL_NS_END(yggdrasil)


