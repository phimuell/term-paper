#pragma once
/**
 * \brief	This file contains a wrapper for the random number generator.
 *
 * It is basically a wrapper around the random number generator.
 * It is only used in the interface.
 *
 * Not thes file was previously located inside the pyygdrasil foplder.
 * But for cvarious reason it was decided to move it to this location.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>

//Including std
#include <random>

YGGDRASIL_NS_START(yggdrasil)

/**
 * \brief	This class is a wrapper arround the generator that is used in yggdrasil.
 * \class 	pyYggdrasil_pRNGenerator_t
 *
 * This class is only a very thin wrapper aroung the random number generator.
 * It's only purpose is to act as a nice helper point.
 */
class pyYggdrasil_pRNGenerator_t
{
	/*
	 * ====================
	 * Typedefs
	 */
public:
	using BaseGenerator_t 		= ::yggdrasil::yggdrasil_pRNG_t;	//!< Underling generator type
	using result_type 		= BaseGenerator_t::result_type;		//!< This is the result type


	/*
	 * ===================
	 * Constructors
	 *
	 * All are defaulted and it is possible to construct *this from a base type
	 */
public:
	/**
	 * \brief	This is a building constructor.
	 *
	 * This seeds the underling generator with the provided seed.
	 *
	 * \param  seed 	This is the seed that should be used.
	 */
	pyYggdrasil_pRNGenerator_t(
		const result_type	seed);


	/**
	 * \brief	This function builds the generator.
	 *
	 * It will seed the generator with seed and then calls
	 * discard with N as arguments.
	 *
	 * \param  seed 	The seed to use.
	 * \param  N 		The number of advances we should do.
	 */
	pyYggdrasil_pRNGenerator_t(
		const result_type 	seed,
		const Size_t 		N);


	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted.
	 */
	pyYggdrasil_pRNGenerator_t();


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~pyYggdrasil_pRNGenerator_t();


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	pyYggdrasil_pRNGenerator_t(
		const pyYggdrasil_pRNGenerator_t&);


	/**
	 * \brief	The move constructor.
	 *
	 * Is defaulted.
	 */
	pyYggdrasil_pRNGenerator_t(
		pyYggdrasil_pRNGenerator_t&&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Defaulted.
	 */
	pyYggdrasil_pRNGenerator_t&
	operator= (
		const pyYggdrasil_pRNGenerator_t&);


	/**
	 * \brief 	Move assignment
	 *
	 * Is defaulted.
	 */
	pyYggdrasil_pRNGenerator_t&
	operator= (
		pyYggdrasil_pRNGenerator_t&&);


	/*
	 * =============
	 * Base constructor
	 *
	 * These allow an inizalisation from the base
	 */
public:
	pyYggdrasil_pRNGenerator_t(
		const BaseGenerator_t& 		geni);


	pyYggdrasil_pRNGenerator_t(
		BaseGenerator_t&&		geni);


	pyYggdrasil_pRNGenerator_t&
	operator= (
		const BaseGenerator_t&		geni);


	pyYggdrasil_pRNGenerator_t&
	operator= (
		BaseGenerator_t&&		geni);

	/*
	 * ====================
	 * Conversions to base
	 */
public:
	/**
	 * \brief	Returns a reference to the underling generator
	 */
	operator
	BaseGenerator_t&();


	/**
	 * \brief	This returns a constant reference to the underling generator.
	 *
	 * There are not so many situation where this is usefull.
	 */
	operator
	const BaseGenerator_t&();

	/**
	 * \brief	This function returns a reference to the wrapped generator
	 */
	BaseGenerator_t&
	get();


	/*
	 * ======================
	 * State functions
	 */
public:
	/**
	 * \brief	This function sets the seed of the wrapped generaor to newSeed.
	 *
	 * This does not work for seed sequences.
	 *
	 * \param  newSeed
	 */
	void
	seed(
		const result_type 	newSeed);


	/**
	 * \brief	Thsi fucntion discarges the next N numbers form the generator.
	 *
	 * Thsi fucntion is like calling the genrator function of *this N times.
	 * But it is a bit more efficient.
	 *
	 * \param  N	The number of sates *this should advance.
	 */
	void
	discard(
		const Size_t 	N);


	/**
	 * \brief	This fucntion returns the nex random number form *this.
	 *
	 * Note on the C++ side this fucntion is implemented by operator()().
	 */
	result_type
	generate();


	/*
	 * ========================
	 * Private members
	 */
private:
	BaseGenerator_t 	m_geni;		//!< This is the wrapped generator
}; //ENd: class(pyYggdrasil_pRNGenerator_t)



YGGDRASIL_NS_END(yggdrasil)


