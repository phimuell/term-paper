

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_gammaPDF.hpp>


//Including std


//Including Eigen


YGGDRASIL_NS_START(yggdrasil)


yggdrasil_gammaDistri_t::yggdrasil_gammaDistri_t(
	const yggdrasil_gammaDistri_t&)
 = default;

yggdrasil_gammaDistri_t::~yggdrasil_gammaDistri_t()
 = default;


yggdrasil_gammaDistri_t::Distribution_ptr
yggdrasil_gammaDistri_t::clone()
 const
{
	return Distribution_ptr(new yggdrasil_gammaDistri_t(*this));
};

yggdrasil_gammaDistri_t::yggdrasil_gammaDistri_t(
	yggdrasil_gammaDistri_t&&)
 = default;


yggdrasil_gammaDistri_t&
yggdrasil_gammaDistri_t::operator= (
	yggdrasil_gammaDistri_t&&)
 = default;

yggdrasil_gammaDistri_t&
yggdrasil_gammaDistri_t::operator= (
	const yggdrasil_gammaDistri_t&)
 = default;


yggdrasil_gammaDistri_t::MeanVec_t
yggdrasil_gammaDistri_t::getMean()
 const
{
	return MeanVec_t::Constant(1, m_gamma.alpha() * m_gamma.beta());
}; //End: getMean


yggdrasil_gammaDistri_t::CoVarMat_t
yggdrasil_gammaDistri_t::getCoVar()
 const
{
	yggdrasil_assert(m_isBounded == m_domain.isValid());

	const Numeric_t Alpha = m_gamma.alpha();
	const Numeric_t Beta  = m_gamma.beta();

	const Numeric_t var = Alpha * Beta * Beta;

	return CoVarMat_t::Constant(1, 1, var);
}; //End: get covar

Numeric_t
yggdrasil_gammaDistri_t::getAlpha()
 const
{
	yggdrasil_assert(m_isBounded == m_domain.isValid());
	return m_gamma.alpha();
};

Numeric_t
yggdrasil_gammaDistri_t::getBeta()
 const
{
	yggdrasil_assert(m_isBounded == m_domain.isValid());
	return m_gamma.beta();
};


Size_t
yggdrasil_gammaDistri_t::nDims()
 const
{
	yggdrasil_assert(m_isBounded == m_domain.isValid());
	return 1;
}; //End getDim


bool
yggdrasil_gammaDistri_t::isUnbounded()
 const
{
	yggdrasil_assert(m_isBounded == m_domain.isValid());
	return m_isBounded == false;
};

yggdrasil_gammaDistri_t::HyperCube_t
yggdrasil_gammaDistri_t::getSampleDomain()
 const
{
	return HyperCube_t(m_domain);
};


yggdrasil_gammaDistri_t::yggdrasil_gammaDistri_t(
		const Numeric_t 	Alpha,
		const Numeric_t 	Beta,
		const HyperCube_t&	domain)
 :
  m_gamma(Alpha, Beta),
  m_domain(IntervalBound_t::CREAT_INVALID_INTERVAL()),
  m_normFac(NAN),
  m_isBounded(domain.isValid())
{
	if(isValidFloat(Alpha) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The alpha value is not valid.");
	};
	if(Alpha <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The alpha value that was passed, " + std::to_string(Alpha) + ", is not positive.");
	};

	if(isValidFloat(Beta) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The beta value is not valid.");
	};
	if(Beta <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The beta value that was passed, " + std::to_string(Beta) + ", is not positive.");
	};

	if(domain.isValid() == true)	//Important must be the argument and not the memer
	{
		//We can now test iof the cube has one dimension.
		//Since an invalid cube, can have an arbitrary numbers of dimesions
		if(domain.nDims() != 1)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The domain of did not have dimension one.");
		}; //End if: test dimension one

		//Now aquire the domain
		m_domain = domain.at(0);

		//We do not perform any more tests on the domain.
		//So we make only the test if the domain can in prionciple hold
		//any samples
		if(m_domain.getUpperBound() <= 0.0)
		{
			//IN this case it is impossible that the domain can hold any samples
			throw YGGDRASIL_EXCEPT_InvArg("The domain is too restrictive, the upper bound is below zero, so no sample can be generated.");
		};
	}; //End if: the domain is valid

	/*
	 * In the case the domain (hyper cube given as argument) is invalid
	 * The member domain is already initalized correctly.
	 */

	m_normFac = 1.0 / (::std::tgamma(Alpha) * std::pow(Beta, Alpha));
	if(isValidFloat(m_normFac) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Error in computing the normalization factor.");
	};

	yggdrasil_assert(m_gamma.alpha() == Alpha);
	yggdrasil_assert(m_gamma.beta() == Beta);
}; //End: constructor


yggdrasil_gammaDistri_t::yggdrasil_gammaDistri_t(
	const Numeric_t 	Alpha,
	const Numeric_t 	Beta)
 :
  yggdrasil_gammaDistri_t(
  	Alpha, Beta, IntervalBound_t::CREAT_INVALID_INTERVAL())
{}; //End domain is invalid


yggdrasil_gammaDistri_t::yggdrasil_gammaDistri_t(
	const Numeric_t 	Alpha,
	const Numeric_t 	Beta,
	const IntervalBound_t&	domain)
 :
  yggdrasil_gammaDistri_t(
  	Alpha, Beta, HyperCube_t(domain))
{}; //End domain is invalid



Real_t
yggdrasil_gammaDistri_t::wh_pdf(
	const Sample_t&		x)
 const
{
	yggdrasil_assert(m_isBounded == m_domain.isValid());
	if(x.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The sample is not valid.");
	};
	if(x.nDims() != 1)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension is wrong.");
	};

	//Load the only valid sample position
	const Numeric_t x_0   = x[0];

	//Test if we are inside the interval
	if(m_isBounded == true)
	{
		if(m_domain.isInside(x_0) == false)
		{
			//The sample lies outside the domain
			return Real_t(0.0);
		}; //Ednd if: outside domain
	}; //End if: domain is valid in the first place

	//Load some parameter
	const Numeric_t Alpha = m_gamma.alpha();
	const Numeric_t Beta  = m_gamma.beta();

	const Numeric_t expVal = std::exp(-x_0 / Beta);
	const Numeric_t xVal   = std::pow(x_0, Alpha - 1.0);

	const Numeric_t finalPDF = m_normFac * xVal * expVal;

	//Return the value
	return finalPDF;
}; //End: generate PDF





void
yggdrasil_gammaDistri_t::wh_generateSample(
	pRNGenerator_t&		generator,
	Sample_t*		s)
{
	//Resize the dimension
	if(s->nDims() != 1)
	{
		*s = Sample_t(1);
	}; //End if, resizing
	yggdrasil_assert(m_isBounded == m_domain.isValid());

	//This is the value that we will generate
	Numeric_t g = NAN;

	do
	{
		g = m_gamma(generator);
		yggdrasil_assert(isValidFloat(g));
		yggdrasil_assert(g > 0.0);
	}
	while(m_isBounded && m_domain.isInside(g) == false);

	//Writting the sample
	yggdrasil_assert(isValidFloat(g));
	yggdrasil_assert(g > 0.0);
	s->at(0) = g;

	//Return
	return;
}; //End generate a single sample


std::ostream&
yggdrasil_gammaDistri_t::dumpToStream(
	std::ostream&	s)
{
	yggdrasil_assert(m_isBounded == m_domain.isValid());

	s << m_gamma;
	return s;
}; //ENd dump to


std::istream&
yggdrasil_gammaDistri_t::loadFromStream(
	std::istream&	s)
{
	yggdrasil_assert(m_isBounded == m_domain.isValid());

	s >> m_gamma;
	return s;
};





YGGDRASIL_NS_END(yggdrasil)


