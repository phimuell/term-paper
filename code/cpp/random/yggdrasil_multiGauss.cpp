/**
 * \brief	This file contains the code that allows to
 * 		 generate multi dimensionals Gaussians.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_multiGauss.hpp>


//Including std


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)


yggdrasil_multiDimGauss_t::yggdrasil_multiDimGauss_t(
	const yggdrasil_multiDimGauss_t&)
 = default;

yggdrasil_multiDimGauss_t::~yggdrasil_multiDimGauss_t()
 = default;

yggdrasil_multiDimGauss_t::Distribution_ptr
yggdrasil_multiDimGauss_t::clone()
 const
{
	return Distribution_ptr(new yggdrasil_multiDimGauss_t(*this));
};


yggdrasil_multiDimGauss_t::yggdrasil_multiDimGauss_t(
	yggdrasil_multiDimGauss_t&&)
 = default;


yggdrasil_multiDimGauss_t&
yggdrasil_multiDimGauss_t::operator= (
	yggdrasil_multiDimGauss_t&&)
 = default;

yggdrasil_multiDimGauss_t&
yggdrasil_multiDimGauss_t::operator= (
	const yggdrasil_multiDimGauss_t&)
 = default;


yggdrasil_multiDimGauss_t::MeanVec_t
yggdrasil_multiDimGauss_t::getMean()
 const
{
	yggdrasil_assert(m_mean.size() > 0);

	return m_mean;
}; //End: getMean


yggdrasil_multiDimGauss_t::CoVarMat_t
yggdrasil_multiDimGauss_t::getCoVar()
 const
{
	//Get the reconstructed matrix.
	CoVarMat_t Sigma = m_chol.reconstructedMatrix();

	return Sigma;
}; //End: get covar


Size_t
yggdrasil_multiDimGauss_t::nDims()
 const
{
	yggdrasil_assert(m_mean.size() > 0);

	return m_mean.size();
}; //End getDim


bool
yggdrasil_multiDimGauss_t::isUnbounded()
 const
{
	return m_domain.isValid() == false;
};

yggdrasil_multiDimGauss_t::HyperCube_t
yggdrasil_multiDimGauss_t::getSampleDomain()
 const
{
	return m_domain;
};


yggdrasil_multiDimGauss_t::yggdrasil_multiDimGauss_t(
	const MeanVec_t&	mean,
	const CoVarMat_t&	coVar)
 :
  yggdrasil_multiDimGauss_t(mean, coVar, HyperCube_t::MAKE_INVALID_CUBE())
{};


yggdrasil_multiDimGauss_t::yggdrasil_multiDimGauss_t(
	const MeanVec_t&	mean,
	const CoVarMat_t&	coVar,
	const HyperCube_t&	domain)
 :
  m_mean(mean),
  m_chol(),
  m_1DGauss(0.0, 1.0),	//This is a standard normal
  m_pdfScale(NAN),
  m_domain(domain)
{
	//Test the dimensioon compability
	const Index_t dim = m_mean.size();
	if(dim <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the mean, " + std::to_string(dim) + ", was too small.");
	};
	if(coVar.rows() != coVar.cols())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The covariance matrix is not square.");
	};
	const auto coVarCol = coVar.cols();
	if(coVar.cols() != dim)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the covariance matrix " + std::to_string(coVarCol) + "x" + std::to_string(coVarCol) + ", did not agree with the dimension of teh mean, " + std::to_string(dim));
	};

	if(m_domain.isValid() == true)
	{
		if(m_mean.size() != m_domain.nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension of the domain is not the same as the mean vector.");
		};
	}; //End test dimensionality of the domain


	//Testing if the mean is valid
	for(Index_t i = 0; i != m_mean.size(); ++i)
	{
		if(isValidFloat(m_mean[i]) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The mean of dimension " + std::to_string(i) + " was not valid");
		};
	}; //End for(i)

	//Test if the mean covariance matrix is mean
	for(Index_t r = 0; r != coVar.rows(); ++r)
	{
		for(Index_t c = 0; c != coVar.cols(); ++c)
		{
			if(isValidFloat(coVar(r, c)) == false)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The covariance matrix was consiudered invalid.");
			};
		}; //End for(c)
	}; //End for(r)


	/*
	 * Now compute the cholesky decomposition
	 */
	m_chol.compute(coVar);

	//Test the result
	if(Eigen::ComputationInfo::Success != m_chol.info())
	{
		throw YGGDRASIL_EXCEPT_InvArg("Eigen failed to compute the colesky decomposition of the covariance matroix.");
	};


	/*
	 * Now we have to compute the scalling coefficient.
	 *
	 * The coefficient is:
	 * 	\sqrt{\frac{1}{(2 \pi)^k \cdot \det{\Sigma} }}
	 * Where \Sigma is the covariance matrix and k is the number of dimensions
	 */
	const Real_t k      = m_mean.size();
	const Real_t twoPi  = 2 * M_PI;
	const Real_t twoPiK = ::std::pow(twoPi, k);	//(2\pi)^k
	yggdrasil_assert(isValidFloat(twoPiK) == true);

	//The determinant is the square of the digaonal element of the
	//cholesky factor
	const CoVarMat_t cholFactorL  = m_chol.matrixL();
	const Index_t    cholEnd      = cholFactorL.rows();
	const Index_t    cholEndMod2  = cholEnd - (cholEnd % 2);
	yggdrasil_assert(cholEndMod2 >= 0);
	yggdrasil_assert(cholFactorL.cols() == cholEnd);

	Real_t sqrtDet_1 = 1.0;
	Real_t sqrtDet_2 = 1.0;
	for(Index_t i = 0; i != cholEndMod2; i += 2)
	{
		sqrtDet_1 *= cholFactorL(i + 1, i + 1);
		sqrtDet_2 *= cholFactorL(i    , i    );
	}; //ENd for(i):
	yggdrasil_assert(isValidFloat(sqrtDet_1) == true);
	yggdrasil_assert(isValidFloat(sqrtDet_2) == true);

	//Is there a left over
	if(cholEnd % 2 == 1)
	{
		sqrtDet_1 *= cholFactorL(cholEnd - 1, cholEnd - 1);
	};

	//Combine these twos
	const Real_t sqrtDet = sqrtDet_1 * sqrtDet_2;
	yggdrasil_assert(isValidFloat(sqrtDet) == true);


	//To get the real determinante the square is missing
	const Real_t DetSigma = sqrtDet * sqrtDet;
	yggdrasil_assert(isValidFloat(DetSigma) == true);
	yggdrasil_assert(DetSigma > 0.0);	//Is positiv semi definit

	//Only this one must be positive

	//Now combining it with the other factor
	const Real_t iScaleFactorSquared = twoPiK * DetSigma;
	yggdrasil_assert(iScaleFactorSquared > 0.0);

	const Real_t iScaleFactor        = ::std::sqrt(iScaleFactorSquared);
	yggdrasil_assert(isValidFloat(iScaleFactor) == true);

	//Now this is the final value
	m_pdfScale = 1.0 / iScaleFactor;

	if(isValidFloat(m_pdfScale) == false && m_pdfScale > 0.0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The normalizaion factor of the gauss distribution is not valid.");
	};
}; //End: building constructor



Real_t
yggdrasil_multiDimGauss_t::wh_pdf(
	const Sample_t&		x)
 const
{
	if(x.nDims() != m_mean.size())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of *this and the passed samples do not agree.");
	};
	yggdrasil_assert(x.isValid());

	const Size_t xDim = x.size();

	/*
	 * We must make the unbounded test
	 */
	if(m_domain.isValid() == true)
	{
		//Test if we are inside and if not tehn we return zero
		if(m_domain.isInside(x) == false)
		{
			//We are not inside, so the result is trivially zero
			return Real_t(0.0);
		}; //End if: is not inside
	}; //End if: we are bounded

	/*
	 * The formula (only in the expoinential, and without the -0.5) is:
	 * 	(\vec{x} - \vec{\mu})^{T} \Sigma^{-1} (\vec{x} - \vec{\mu})
	 *
	 * Setting
	 * 	\vec{y} := \vec{x} - \vec{\mu}
	 *
	 * we get the following expresion:
	 * 	\vec{y}^{T} \Sigma^{-1} \vec{y}
	 *
	 * By defining the following
	 * 	\vec{\nu} := \Sigma^{-1} \vec{y}
	 *
	 * we can write the above as:
	 * 	\vec{y}^{T} \cdot \vec{\nu}
	 *
	 * \vec{nu} can be easaly computed by solving the following
	 * linear system:
	 * 	\Sigma \vec{\nu} == \vec{y}
	 *
	 * Since we have the cholesky decomposition of of \Sigma, this
	 * can easaly be done.
	 */

	//Generate a map and directly compute the y vector
	const MeanVec_t y = ::Eigen::Map<const MeanVec_t>(x.data(), xDim) - m_mean;

	//Compute \nu, by solvin a system of equation
	const MeanVec_t Nu = m_chol.solve(y);
	yggdrasil_assert([&Nu]() -> bool {for(Index_t i = 0; i != Nu.size(); ++i) {if(isValidFloat(Nu[i]) == false) {return false;}}; return true;}());

	//Compute the value of the exponent, but do not forget the 2
	const Real_t expArg_timesTwoAndMinus = y.dot(Nu);
	yggdrasil_assert(::std::isnan(expArg_timesTwoAndMinus) == false);

	//Get the real value of the exponent value
	const Real_t expArg = -expArg_timesTwoAndMinus * 0.5;

	//Now Compute the exponential of this
	const Real_t expValue = ::std::exp(expArg);

	//Now scale the value approriate
	const Real_t finalPDF = expValue * m_pdfScale;
	yggdrasil_assert(finalPDF >= 0.0);	//Technically greater as zero, but finite precision

	if(isValidFloat(finalPDF) == false && finalPDF >= 0.0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The pdf value is determined invalid.");
	};


	//Return the value
	return finalPDF;
}; //End: generate PDF


void
yggdrasil_multiDimGauss_t::wh_generateSample(
	pRNGenerator_t&		generator,
	Sample_t*		s)
{
	/*
	 * The generation of the sample is done as described
	 * by the following article:
	 * 	https://en.wikipedia.org/wiki/Multivariate_normal_distribution#Drawing_values_from_the_distribution
	 */
	const Size_t nDim = this->nDims();

	//Test if the dimension of the argument is correct
	//and change it to the correct value.
	if((Size_t)s->nDims() != nDim)
	{
		//Not the same numbers of dmensions
		//we have to update it
		*s = Sample_t(nDim);
	}; //End: addapt sample if needed.

	//This is the chol fgactor that is needed for computation
	const CoVarMat_t cholFactorL = m_chol.matrixL();

	//THis is a const copy of the mean
	const MeanVec_t Mean(m_mean);

	//This is the solution vector
	::Eigen::Map<MeanVec_t> newSample(s->data(), nDim);

	//This vector contains the one dimensioal decoupled normal samples
	MeanVec_t normalSamples(nDim);

	//Test if the domain is unbounded
	const bool isDomValid = m_domain.isValid();

	//This is the probability vriable
	Real_t thisPDF = NAN;

	//This is for the starvation prtection
	const Size_t maxTries = 10000;
	Size_t tries = 0;

	do
	{
		//Implement the starvation protection
		tries += 1;
		if(tries >= maxTries)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Detected starvation.");
		};

		//Set the probability to NAN
		thisPDF = NAN;

		//Generate the decoupled standard samples
		for(Size_t d = 0; d != nDim; ++d)
		{
			normalSamples[d] = m_1DGauss(generator);
		}; //End for(d): generate the 1d sampels

		//Now perform the trasformation
		newSample.noalias() = cholFactorL * normalSamples + Mean;

		//Compute the probability of the sample
		thisPDF = this->wh_pdf(*s);

		//Test the probability
		if(isValidFloat(thisPDF) == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Detected an invalid pdf value.");
		};

		//We enforce positivity or zero
		yggdrasil_assert(thisPDF >= 0.0);

	}
	while(
		(isDomValid && m_domain.isInside(*s) == false)		//Try as long untill we are insie the pdf
		||
		(thisPDF <= 0.0)						//The pdf must be strictly greater
	);

#ifndef NDEBUG
	for(Size_t i = 0; i != nDim; ++i)
	{
		if(isValidFloat(s->at(i)) == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The sample that was generated is invalid.");
		};
		if(isDomValid)
		{
			if(m_domain.at(i).isInside(s->at(i)) == false)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("The sample is not inside the domain.");
			};
		};
	};
#endif

	//Test if the sample is vaid, this is a final test that can not be bypassed
	//The test above is more ment for debugers
	if(s->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The generation of gauss samples leads to invalid samples.");
	};


	//Return
	return;
}; //End generate a single sample


std::ostream&
yggdrasil_multiDimGauss_t::dumpToStream(
	std::ostream&	s)
{
	s << m_1DGauss;

	return s;
}; //ENd dump to


std::istream&
yggdrasil_multiDimGauss_t::loadFromStream(
	std::istream&	s)
{
	s >> m_1DGauss;

	return s;
};





YGGDRASIL_NS_END(yggdrasil)


