/**
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>
#include <random/yggdrasil_multiGauss.hpp>
#include <random/yggdrasil_multiDirichle.hpp>
#include <random/yggdrasil_outlierPDF.hpp>
#include <random/yggdrasil_biModalUniform.hpp>
#include <random/yggdrasil_gammaPDF.hpp>
#include <random/yggdrasil_multiUniform.hpp>
#include <random/yggdrasil_betaPDF.hpp>


//Including std
#include <algorithm>
#include <memory>
#include <utility>

//Include Boost
#include <boost/filesystem.hpp>


YGGDRASIL_NS_START(yggdrasil)

using Gauss_t 		= ::yggdrasil::yggdrasil_multiDimGauss_t;
using HyperCube_t 	= Gauss_t::HyperCube_t;
using IntervalBound_t 	= HyperCube_t::IntervalBound_t;
using Dirichlet_t 	= ::yggdrasil::yggdrasil_multiDimDirichlet_t;
using ::std::make_pair;

yggdrasil_randomDistribution_i::FactoryRet_t
yggdrasil_randomDistribution_i::FACTORY(
	const eDistiType 	dType,
	const Int_t 		dim)
{
	using Ret_t = Distribution_ptr;

	if(dType == eDistiType::Gauss)
	{
		if(dim == 2)
		{
			Gauss_t::CoVarMat_t Sigma(2, 2);
			Sigma << 2, 0, 0, 2;

			Gauss_t::MeanVec_t Mu(2);
			Mu << 1.4, -4;

			//This cube is so large, that there should not be any problems
			const HyperCube_t domain(2, IntervalBound_t(-10.0, 10.0));


			//Create a gauss
			Gauss_t dist(Mu, Sigma, domain);

			return ::std::make_pair(Ret_t(new Gauss_t(dist)), "gauss_2d_simple");
		}; //End dim2

		//This case is inside teh random generator paper
		if(dim == 3)
		{
			Gauss_t::CoVarMat_t Sigma(3, 3);
			Sigma <<
				0.35,	0.25,	0.5,
				0.25,	0.4,	0.6,
				0.5,	0.6,	1;


			Gauss_t::MeanVec_t Mu(3);
			Mu <<
				0.0,	0.0,	0.0;

			//This cube is so large, that there should not be any problems
			const HyperCube_t domain(3, IntervalBound_t(-10.0, 10.0));


			//Create a gauss
			Gauss_t dist(Mu, Sigma, domain);

			return ::std::make_pair(Ret_t(new Gauss_t(dist)), "gauss_3d_gPaper");

		}; //End dim3

		if(dim == 4)
		{
			//This cube is so large, that there should not be any problems
			const HyperCube_t domain(4, IntervalBound_t(-15.0, 15.0));

			Gauss_t::CoVarMat_t Sigma(4, 4);
			Sigma
				<< 1.0, -0.344, 0.141, -0.486,
				-0.344, 1, 0.586, 0.244,
				0.141, 0.586, 1, -0.544,
				-0.486, 0.244, -0.544, 1;

			Gauss_t::MeanVec_t Mu(4);
			Mu.setZero();


			//Create a gauss
			Gauss_t dist(Mu, Sigma, domain);

			return std::make_pair(Ret_t(new Gauss_t(dist)), "gauss_4d");
		}; //End if: dim == 4

		if(dim == 7)
		{
			//This cube is so large, that there should not be any problems
			const HyperCube_t domain(7, IntervalBound_t(-15.0, 15.0));

			Gauss_t::CoVarMat_t Sigma(7, 7);
			Sigma <<
				1.0,	-0.216,	0.161,	-0.0496,  0.0342,  -0.116,	0.749,
				-0.216,	1.0,	0.301,	0.0391,	  -0.217,   0.0189,	-0.381,
				0.161,	0.301,	1.0,	0.574,	  -0.312,   0.109,	0.386,
				-0.0496,0.0391,	0.574,	1.0,	  -0.483,   0.730,	-0.0572,
				0.0342, -0.271, -0.312, -0.438,	  1.0,	    -0.475,	0.258,
				-0.116,	0.0189,	0.109,	0.730,	  -0.475,   1.0,	-0.386,
				0.749,	-0.381,	0.386,	-0.0572,  0.258,    -0.386,	1.0;

			Gauss_t::MeanVec_t Mu(7);
			Mu.setZero();

			//Create a gauss
			Gauss_t dist(Mu, Sigma, domain);

			return make_pair(Ret_t(new Gauss_t(dist)), "gauss_7d");
		}; //End if: dim == 7

		if(dim == -2)
		{
			Gauss_t::CoVarMat_t Sigma(2, 2);
			Sigma << 4.0, -2.28, -2.28, 1.44;

			Gauss_t::MeanVec_t Mu(2);
			Mu << 0.0, 0.0;

			//This cube is so large, that there should not be any problems
			const HyperCube_t domain(2, IntervalBound_t(-8.0, 8.0));


			//Create a gauss
			Gauss_t dist(Mu, Sigma, domain);

			return ::std::make_pair(Ret_t(new Gauss_t(dist)), "gauss_2d_paper");
		}; //End iof:

		if(dim == -22)
		{
			Gauss_t::CoVarMat_t Sigma(2, 2);
			Sigma << 4.0, -2.28, -2.28, 1.44;

			Gauss_t::MeanVec_t Mu(2);
			Mu << 0.5, -0.5;

			//This cube is so large, that there should not be any problems
			const HyperCube_t domain(2, IntervalBound_t(-8.0, 8.0));


			//Create a gauss
			Gauss_t dist(Mu, Sigma, domain);

			return ::std::make_pair(Ret_t(new Gauss_t(dist)), "gauss_2d_paper");
		}; //End iof:

		if(dim == -1)
		{
			Gauss_t::CoVarMat_t Sigma(1, 1);
			Sigma << 1.0;

			Gauss_t::MeanVec_t Mu(1);
			Mu << 0.0;

			//This cube is so large, that there should not be any problems
			const HyperCube_t domain(1, IntervalBound_t(-8.0, 8.0));


			//Create a gauss
			Gauss_t dist(Mu, Sigma, domain);

			return ::std::make_pair(Ret_t(new Gauss_t(dist)), "gauss_1dStd");
		}; //End iof:



		throw YGGDRASIL_EXCEPT_InvArg("The imput was not recognized.");
	}; //ENd it is a gauss

	if(dType == eDistiType::Dirichlet)
	{
		//3.2.3
		if(dim == 2)
		{
			//This cube is so large, that there should not be any problems
			const HyperCube_t domain(2, IntervalBound_t(0.0, 1.0));

			//Create the parameter
			Dirichlet_t::Alpha_t Alpha(3);
			Alpha << 0.9, 1.5, 3.0;

			//Create a gauss
			Dirichlet_t dist(Alpha, domain);

			return make_pair(Ret_t(new Dirichlet_t(dist)), "dirichlet_2d");
		};


		if(dim == 4)
		{
			//This cube is so large, that there should not be any problems
			const HyperCube_t domain(4, IntervalBound_t(0.0, 1.0));

			//Create the parameter
			Dirichlet_t::Alpha_t Alpha(5);
			Alpha << 6.13, 9.29, 10.6, 8.24, 3.91;

			//Create a gauss
			Dirichlet_t dist(Alpha, domain);

			return make_pair(Ret_t(new Dirichlet_t(dist)), "dirichlet_4d");
		}; //ENd 5 dim

		if(dim == 7)
		{
			//This cube is so large, that there should not be any problems
			const HyperCube_t domain(7, IntervalBound_t(0.0, 1.0));

			//Create the parameter
			Dirichlet_t::Alpha_t Alpha(8);
			Alpha << 9.0, 5.71, 8.96, 4.51, 5.81, 4.06, 10.7, 1.51;

			//Create a gauss
			Dirichlet_t dist(Alpha, domain);

			return make_pair(Ret_t(new Dirichlet_t(dist)), "dirichlet_7d");
		}; //End if: dim == 8

		throw YGGDRASIL_EXCEPT_InvArg("The imput was not recognized.");
	}; //ENd dirichlet


	if(dType == eDistiType::Outlier)
	{
		using Outlier_t = yggdrasil_outlierPDF_t;

		//We missuse dîm for that
		if(dim == 0)
		{
			const Numeric_t Alpha   = 0.1;
			const Numeric_t Mean    = 0.0;
			const Numeric_t Sigma_1 = 1.0;
			const Numeric_t Sigma_2 = 0.1;

			HyperCube_t domain(IntervalBound_t(-5.0, 5.0));

			return make_pair(Distribution_ptr(new Outlier_t(Alpha, Mean, Sigma_1, Sigma_2, domain)), "outlier_paper");
		}; //End if: first kind

		//We missuse dîm for that
		if(dim == 1)
		{
			const Numeric_t Alpha   = 0.5;
			const Numeric_t Mean    = 0.0;
			const Numeric_t Sigma_1 = 1.0;
			const Numeric_t Sigma_2 = 1.0;

			HyperCube_t domain(IntervalBound_t(-5.0, 5.0));

			return make_pair(Distribution_ptr(new Outlier_t(Alpha, Mean, Sigma_1, Sigma_2, domain)), "outlier_decay");
		}; //End if: first kind


		throw YGGDRASIL_EXCEPT_InvArg("The dimension " + std::to_string(dim) + " was unkonw.");
	}; //End if: it is an outlier distribution


	if(dType == eDistiType::Uniform)
	{
		using BiModalUniform_t  = ::yggdrasil::yggdrasil_biModalUniform_t;
		using MultiDimUniform_t = ::yggdrasil::yggdrasil_multiDimUniform_t;


		//This is  a special kind of domain
		if(dim > 1000)
		{
			//This is not with the restricted version
			const auto corrDim = dim - 1000;
			return make_pair(Distribution_ptr(new MultiDimUniform_t(
					HyperCube_t(corrDim, IntervalBound_t(0.25, 0.75))
					//The sampling domain is equal the support domain
					))
					//Name is not changed for allowing compability
					,
					std::string("multiUniform") + std::to_string(corrDim) + "D");
		};

		if(dim > 0)
		{
			return make_pair(Distribution_ptr(new MultiDimUniform_t(
					HyperCube_t(dim, IntervalBound_t(0.25, 0.75)), 	//Support
					HyperCube_t(dim, IntervalBound_t(0.0, 1.0)))),	//Sampling domain
					std::string("multiUniform") + std::to_string(dim) + "D");
		};


		if(dim == -1)
		{
			return make_pair(Distribution_ptr(new BiModalUniform_t(
					IntervalBound_t(0.23, 0.232), IntervalBound_t(0.233, 0.235), 0.5, HyperCube_t(1, IntervalBound_t(0.0, 1.0)))),
					"spikyUniformPaper");
		};

		if(dim == -2)
		{
			return make_pair(Distribution_ptr(new BiModalUniform_t(
					IntervalBound_t(0.25, 0.5), IntervalBound_t(0.7, 0.8), 0.1, HyperCube_t(1, IntervalBound_t(0.0, 1.0)))),
					"spikyUniformMy");
		};

		throw YGGDRASIL_EXCEPT_InvArg("THe bimodal uniform was not knwon.");
	}; //End if: uniform

	if(dType == eDistiType::Gamma)
	{
		using GammaD_t = yggdrasil_gammaDistri_t;

		if(dim == -1)
		{
			/*
			 * See section 3.1.6
			 *
			 * Note due to the small allowed support this
			 * distribution will not pass the test.
			 */
			return make_pair(Distribution_ptr(new GammaD_t(
					2.0 / 3.0, 50.0, IntervalBound_t(0.001, 300))),
					"gammaDistPaper");

		};

		if(dim == -2)
		{
			/*
			 * The support of this distribution was increased, such that
			 * the sit passes the test.
			 */
			return make_pair(Distribution_ptr(new GammaD_t(
					2.0 / 3.0, 50.0, IntervalBound_t(0.001, 30000))),
					"gammaDistPaper");

		};


		if(dim == -3)
		{
			return make_pair(Distribution_ptr(new GammaD_t(
					1.0, 2.0, IntervalBound_t(0.001, 300))),
					"gammaDist_1_2");

		};

		if(dim == -4)
		{
			return make_pair(Distribution_ptr(new GammaD_t(
					9.0, 0.5, IntervalBound_t(0.001, 300))),
					"gammaDist_9_05");

		};
	}; //End gamma


	if(dType == eDistiType::Beta)
	{
		using BetaD_t = ::yggdrasil::yggdrasil_betaDistri_t;
		/*
		 * See 3.1.5 of the paper
		 */
		if(dim == 1)
		{
			return make_pair(
				Distribution_ptr(new BetaD_t(1.05, 0.8)),
				"betaDist_paper");
		}; //End: dim -1

		/*
		 * The followings comming from wikipedia and also looks relatively crazy
		 */
		if(dim == -1)
		{
			return make_pair(
				Distribution_ptr(new BetaD_t(0.5, 0.5)),
				"betaDist_05_05");
		}; //ENd
		if(dim == -2)
		{
			return make_pair(
				Distribution_ptr(new BetaD_t(2.0, 5.0)),
				"betaDist_2_5");
		}; //ENd
		if(dim == -3)
		{
			return make_pair(
				Distribution_ptr(new BetaD_t(1.0, 3.0)),
				"betaDist_1_3");
		}; //ENd

		throw YGGDRASIL_EXCEPT_InvArg("The kind parameter of the beta argument was not understood.");
	}; //ENd: Beta

	throw YGGDRASIL_EXCEPT_InvArg("The imput was not recognized.");
}; //End get Distri


YGGDRASIL_NS_END(yggdrasil)


