/**
 * \brief	This file contains the code that allows to
 * 		 generate multi dimensionals Gaussians.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_outlierPDF.hpp>


//Including std


//Including Eigen


YGGDRASIL_NS_START(yggdrasil)


yggdrasil_outlierPDF_t::yggdrasil_outlierPDF_t(
	const yggdrasil_outlierPDF_t&)
 = default;

yggdrasil_outlierPDF_t::~yggdrasil_outlierPDF_t()
 = default;


yggdrasil_outlierPDF_t::Distribution_ptr
yggdrasil_outlierPDF_t::clone()
 const
{
	return Distribution_ptr(new yggdrasil_outlierPDF_t(*this));
};

yggdrasil_outlierPDF_t::yggdrasil_outlierPDF_t(
	yggdrasil_outlierPDF_t&&)
 = default;


yggdrasil_outlierPDF_t&
yggdrasil_outlierPDF_t::operator= (
	yggdrasil_outlierPDF_t&&)
 = default;

yggdrasil_outlierPDF_t&
yggdrasil_outlierPDF_t::operator= (
	const yggdrasil_outlierPDF_t&)
 = default;


yggdrasil_outlierPDF_t::MeanVec_t
yggdrasil_outlierPDF_t::getMean()
 const
{
	return MeanVec_t::Constant(1, m_mean);
}; //End: getMean


yggdrasil_outlierPDF_t::CoVarMat_t
yggdrasil_outlierPDF_t::getCoVar()
 const
{
	/*
	 * The sum of two gaussians is again a gaussian.
	 * See also:
	 * 	https://en.wikipedia.org/wiki/Sum_of_normally_distributed_random_variables#Independent_random_variables
	 * 	https://en.wikipedia.org/wiki/Variance#Basic_properties
	 * 	https://stats.stackexchange.com/questions/205126/standard-deviation-for-weighted-sum-of-normal-distributions/205128#205128
	 *
	 * We also have independence between the two.
	 */

	//Get the variance
	const Numeric_t var1 = m_gauss1.stddev() * m_gauss1.stddev();
	const Numeric_t var2 = m_gauss2.stddev() * m_gauss2.stddev();

	//This is the weighting
	const Numeric_t w1 = m_alpha;
	const Numeric_t w2 = (1.0 - m_alpha);

	//Compute the variance
	const Numeric_t var = w1 * var1 + w2 * var2;

	return CoVarMat_t::Constant(1, 1, var);
}; //End: get covar


Size_t
yggdrasil_outlierPDF_t::nDims()
 const
{
	return 1;
}; //End getDim

Numeric_t
yggdrasil_outlierPDF_t::getStdDev1()
 const
 noexcept
{
	return m_gauss1.stddev();
};


Numeric_t
yggdrasil_outlierPDF_t::getStdDev2()
 const
 noexcept
{
	return m_gauss2.stddev();
};

Numeric_t
yggdrasil_outlierPDF_t::getAlpha()
 const
 noexcept
{
	return m_alpha;
};


bool
yggdrasil_outlierPDF_t::isUnbounded()
 const
{
	return m_domain.isValid() == false;
};

yggdrasil_outlierPDF_t::HyperCube_t
yggdrasil_outlierPDF_t::getSampleDomain()
 const
{
	return HyperCube_t(m_domain);
};


yggdrasil_outlierPDF_t::yggdrasil_outlierPDF_t(
	const Numeric_t 	alpha,
	const Numeric_t 	mean,
	const Numeric_t 	std_1,
	const Numeric_t 	std_2,
	const HyperCube_t&	domain)
 :
  m_alpha(alpha),
  m_mean(mean),
  m_uniform(0, 1),
  m_gauss1(mean, std_1),
  m_gauss2(mean, std_2),
  m_domain(IntervalBound_t::CREAT_INVALID_INTERVAL())
{
	if(domain.size() != 1)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The cube has more than one dimensoin.");
	};

	//Load the dimension
	m_domain = domain[0];

	if(std_1 <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The std of teh fiorst gaussian is negative.");
	};
	if(std_2 <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The std of the second gaussinan is negative.");
	};
	if(!((0 <= m_alpha) && (m_alpha <= 1.0)))
	{
		throw YGGDRASIL_EXCEPT_InvArg("Alpha, " + std::to_string(m_alpha) + ", is out of range.");
	};
}; //End building constructor


Real_t
yggdrasil_outlierPDF_t::wh_pdf(
	const Sample_t&		x)
 const
{
	if(x.nDims() != 1)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of *this and the passed samples do not agree.");
	};
	yggdrasil_assert(x.isValid());

	//Thi si sthe x value we use
	const Numeric_t x0 = x[0];

	//Test if we are inside the domain, if valid
	if(m_domain.isValid() && (m_domain.isInside(x0) == false))
	{
		//The point is outside, so return zero
		return Real_t(0.0);
	}; //ENd bound checking

	//Now get the parameter
	const Real_t Sigma1 = this->getStdDev1();
	const Real_t Sigma2 = this->getStdDev2();
	const Real_t Mean   = m_mean;
	const Real_t Alpha  = m_alpha;		//Weight of first mode
	const Real_t Beta   = 1.0 - Alpha;	//Weight of second mode
	const Real_t D      = x0 - Mean;
	const Real_t D2     = D * D;
	const Real_t sqrt2Pi= std::sqrt(2 * M_PI);

	//This Lambda is able to calculate the PDF, given the std
	auto PDF = [D2, sqrt2Pi](const Real_t Sigma) -> Real_t
	  {
	  	return (Real_t(1.0) / (sqrt2Pi * Sigma)) * std::exp(-Real_t((0.5 * D2) / (Sigma * Sigma)));
	  }; //End: lambda

	//Now caluclate the PDF as weighted sum
	const Real_t pV = (Alpha * PDF(Sigma1) + Beta * PDF(Sigma2));

	//Return the value
	return pV;
}; //End: generate PDF



yggdrasil_outlierPDF_t::yggdrasil_outlierPDF_t(
	const Numeric_t 	alpha,
	const Numeric_t 	mean,
	const Numeric_t 	std_1,
	const Numeric_t 	std_2)
 :
  yggdrasil_outlierPDF_t(
  	alpha,
  	mean,
  	std_1,
  	std_2,
  	HyperCube_t(IntervalBound_t::CREAT_INVALID_INTERVAL()))
{}; //End domain is invalid



void
yggdrasil_outlierPDF_t::wh_generateSample(
	pRNGenerator_t&		generator,
	Sample_t*		s)
{
	/*
	 * The generation of such numbers is relatively simple.
	 * We first roll a fair dice in order to figuring out which
	 * gauss we use.
	 * Then we will generate a sample accordingly.
	 */

	//Resize the dimension
	if(s->nDims() != 1)
	{
		*s = Sample_t(1);
	}; //End if, resizing

	//This is the value that we will generate
	Numeric_t g = NAN;

	//Test if the domain is valid
	const bool isDomValid = m_domain.isValid();

	do
	{
		if(m_uniform(generator) <= m_alpha)
		{
			//The first gauss was selected
			g = m_gauss1(generator);
		}
		else
		{
			//The secons sample was selected
			g = m_gauss2(generator);
		};
		yggdrasil_assert(isValidFloat(g));
	}
	while(isDomValid && m_domain.isInside(g) == false);

	//Writting the sample
	yggdrasil_assert(isValidFloat(g));
	s->at(0) = g;

	//Return
	return;
}; //End generate a single sample


std::ostream&
yggdrasil_outlierPDF_t::dumpToStream(
	std::ostream&	s)
{
	s << m_uniform << m_gauss1 << m_gauss2;

	return s;
}; //ENd dump to


std::istream&
yggdrasil_outlierPDF_t::loadFromStream(
	std::istream&	s)
{
	s >> m_uniform >> m_gauss1 >> m_gauss2;

	return s;
};





YGGDRASIL_NS_END(yggdrasil)


