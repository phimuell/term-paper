/**
 * \brief	This file contains the code that allows to
 * 		 generate multi dimensionals Gaussians.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_multiUniform.hpp>


//Including std


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)


yggdrasil_multiDimUniform_t::yggdrasil_multiDimUniform_t(
	const yggdrasil_multiDimUniform_t&)
 = default;

yggdrasil_multiDimUniform_t::~yggdrasil_multiDimUniform_t()
 = default;


yggdrasil_multiDimUniform_t::Distribution_ptr
yggdrasil_multiDimUniform_t::clone()
 const
{
	return Distribution_ptr(new yggdrasil_multiDimUniform_t(*this));
};

yggdrasil_multiDimUniform_t::yggdrasil_multiDimUniform_t(
	yggdrasil_multiDimUniform_t&&)
 = default;


yggdrasil_multiDimUniform_t&
yggdrasil_multiDimUniform_t::operator= (
	yggdrasil_multiDimUniform_t&&)
 = default;

yggdrasil_multiDimUniform_t&
yggdrasil_multiDimUniform_t::operator= (
	const yggdrasil_multiDimUniform_t&)
 = default;


yggdrasil_multiDimUniform_t::MeanVec_t
yggdrasil_multiDimUniform_t::getMean()
 const
{
	const Size_t N = m_uniSamp.size();
	yggdrasil_assert(N > 0);
	yggdrasil_assert(N == (Size_t)m_suppDom.nDims());
	yggdrasil_assert(N == (Size_t)m_sampDom.nDims());

	MeanVec_t mean(N);
	yggdrasil_assert((Size_t)mean.size() == N);

	for(Size_t i = 0; i != N; ++i)
	{
		mean[i] = m_suppDom[i].middPoint();
	};

	return mean;
}; //End: getMean


yggdrasil_multiDimUniform_t::CoVarMat_t
yggdrasil_multiDimUniform_t::getCoVar()
 const
{
	const Size_t N = m_uniSamp.size();
	yggdrasil_assert(N > 0);
	yggdrasil_assert(N == (Size_t)m_suppDom.nDims());
	yggdrasil_assert(N == (Size_t)m_sampDom.nDims());

	CoVarMat_t Var(N, N);
	Var.setZero();

	for(Size_t i = 0; i != N; ++i)
	{
		const Numeric_t dimLeng = m_suppDom[i].getLength();
		const Numeric_t dimVar  = dimLeng * dimLeng / 12.0;

		Var(i, i) = dimVar;
	}; //End for(i)

	return Var;
}; //End: get covar


Size_t
yggdrasil_multiDimUniform_t::nDims()
 const
{
	yggdrasil_assert(m_uniSamp.size() > 0);

	return m_uniSamp.size();
}; //End getDim


bool
yggdrasil_multiDimUniform_t::isUnbounded()
 const
{
	return m_sampDom.isValid() == false;
};


yggdrasil_multiDimUniform_t::HyperCube_t
yggdrasil_multiDimUniform_t::getSampleDomain()
 const
{
	return m_sampDom;
};


yggdrasil_multiDimUniform_t::HyperCube_t
yggdrasil_multiDimUniform_t::getSupportDomain()
 const
{
	return m_suppDom;
};


yggdrasil_multiDimUniform_t::yggdrasil_multiDimUniform_t(
	const HyperCube_t&	supportDom)
 :
  yggdrasil_multiDimUniform_t(supportDom, supportDom)
{};


yggdrasil_multiDimUniform_t::yggdrasil_multiDimUniform_t(
	const HyperCube_t&	supportDom,
	const HyperCube_t&	samplingDom)
 :
  m_suppDom(supportDom),
  m_sampDom(samplingDom),
  m_uniSamp(),
  m_iVol(NAN)
{
	//Test the dimensioon compability
	const Index_t dim = m_suppDom.nDims();
	if(dim <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the mean, " + std::to_string(dim) + ", was too small.");
	};
	if(m_suppDom.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The support domain is not valid.");
	};
	if(m_sampDom.isValid() == true)
	{
		if(m_suppDom.isWeaklyContainedIn(m_sampDom) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The support domain is not contained inside the sampling domain.");
		};
	}; //ENd if: sampling dom is valid

	m_uniSamp.reserve(dim);
	for(Index_t i = 0; i != dim; ++i)
	{
		m_uniSamp.push_back(UniformDist_t(m_suppDom.getLowerBound(i), m_suppDom.getUpperBound(i)));
	}; //End for(i)
	yggdrasil_assert(m_uniSamp.size() == (Size_t)dim);

	m_iVol = 1.0 / m_suppDom.getVol();
	if(isValidFloat(m_iVol) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The normalizaing factor is not valid.");
	};
}; //End: building constructor



Real_t
yggdrasil_multiDimUniform_t::wh_pdf(
	const Sample_t&		x)
 const
{
	if((Size_t)x.nDims() != m_uniSamp.size())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of *this and the passed samples do not agree.");
	};
	yggdrasil_assert(x.isValid());
	yggdrasil_assert(m_suppDom.isValid());

	if(m_suppDom.isInside(x) == true)
	{
		return Real_t(m_iVol);
	};

	return Real_t(0.0);
}; //End: generate PDF


void
yggdrasil_multiDimUniform_t::wh_generateSample(
	pRNGenerator_t&		generator,
	Sample_t*		s)
{
	const Size_t nDim = m_uniSamp.size();
	yggdrasil_assert(nDim > 0);


	//Test if the dimension of the argument is correct
	//and change it to the correct value.
	if((Size_t)s->nDims() != nDim)
	{
		//Not the same numbers of dmensions
		//we have to update it
		*s = Sample_t(nDim);
	}; //End: addapt sample if needed.

	Numeric_t* const dS = s->data();
	for(Size_t i = 0; i != nDim; ++i)
	{
		dS[i] = m_uniSamp[i](generator);
	}; //End for(i):

	//Return
	return;
}; //End generate a single sample


std::ostream&
yggdrasil_multiDimUniform_t::dumpToStream(
	std::ostream&	s)
{
	const Size_t nDim = m_uniSamp.size();
	yggdrasil_assert(nDim > 0);

	for(Size_t i = 0; i != nDim; ++i)
	{
		s << m_uniSamp[i];
	};

	return s;
}; //ENd dump to


std::istream&
yggdrasil_multiDimUniform_t::loadFromStream(
	std::istream&	s)
{
	const Size_t nDim = m_uniSamp.size();
	yggdrasil_assert(nDim > 0);

	for(Size_t i = 0; i != nDim; ++i)
	{
		s >> m_uniSamp[i];
	};

	return s;
};





YGGDRASIL_NS_END(yggdrasil)


