#pragma once
/**
 * \brief	This function implements an gamma function
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>


//Including std


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)


/**
 * \brief	This class allows a gamma distribution.
 * \class	yggdrasil_gammaDistri_t
 *
 */
class yggdrasil_gammaDistri_t final : public yggdrasil_randomDistribution_i
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using StdGammaDist_t 	= ::std::gamma_distribution<Numeric_t>;

	using Super_t 		= yggdrasil_randomDistribution_i;	//!< This is the interface.


	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This function uses the terminology of the C++ standard.
	 * Note that Wikipedia uses different symbols.
	 * The domain can also be invalid, in that case no restriction is applied.
	 * Never the less the domain must be a hyper cube of diomension one.
	 *
	 * \param  Alpha 	The shape value, wikipedia names this one k.
	 * \param  Beta		The rate value, wiokipedia calls this one theta.
	 * \param  domain	The domain, is an interval, the lower bound must be greater than zero, if valid.
	 */
	yggdrasil_gammaDistri_t(
		const Numeric_t 	Alpha,
		const Numeric_t 	Beta,
		const HyperCube_t&	domain);

	/**
	 * \brief	This is the building constructor.
	 *
	 * This function uses the terminology of the C++ standard.
	 * Note that Wikipedia uses different symbols.
	 * The domain can also be invalid, in that case no restriction is applied.
	 *
	 * \param  Alpha 	The shape value, wikipedia names this one k.
	 * \param  Beta		The rate value, wiokipedia calls this one theta.
	 * \param  domain	The domain, is an interval, the lower bound must be greater than zero, if valid.
	 *
	 * \note	This constructor is only provided for backwards compability reasons and sould not be used.
	 */
	yggdrasil_gammaDistri_t(
		const Numeric_t 	Alpha,
		const Numeric_t 	Beta,
		const IntervalBound_t&	domain);


	/**
	 * \brief	This version of teh constructor uses an invalid domain.
	 *
	 * This function is equivalent as when the other coonstructor is called with an invalid domain.
	 * This means that no restriction is applied.
	 *
	 * \param  Alpha 	The shape value, wikipedia names this one k.
	 * \param  Beta		The rate value, wiokipedia calls this one theta.
	 */
	yggdrasil_gammaDistri_t(
		const Numeric_t 	Alpha,
		const Numeric_t 	Beta);



	/**
	 * \brief	This is the destructor.
	 *
	 * It is defaulted.
	 */
	~yggdrasil_gammaDistri_t();


	/**
	 * \brief	This is a colne function.
	 *
	 * This function returns a copy of *this.
	 * It is like the java fucntion.
	 */
	virtual
	Distribution_ptr
	clone()
	 const
	 final;


	/**
	 * \brief	This is teh copy constrructor
	 *
	 * It is defaulted.
	 */
	yggdrasil_gammaDistri_t(
		const yggdrasil_gammaDistri_t&);


	/**
	 * \brief	The copy assignment operator.
	 *
	 * Is defaulted.
	 */
	yggdrasil_gammaDistri_t&
	operator= (
		const yggdrasil_gammaDistri_t&);


	/**
	 * \brief	The move constructor
	 *
	 * Is defaulted.
	 */
	yggdrasil_gammaDistri_t(
		yggdrasil_gammaDistri_t&&);


	/**
	 * \brief	The move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_gammaDistri_t&
	operator= (
		yggdrasil_gammaDistri_t&&);


	/**
	 * \brief	The default constructor.
	 *
	 * Is deleted.
	 */
	yggdrasil_gammaDistri_t()
	 = delete;


	/*
	 * =====================
	 * Random number functions
	 */
public:
	/*
	 * No functions here, since all of the defaults
	 * are used.
	 *
	 * If we would list them we would need to
	 * explicitly code that the default are used.
	 */


	/*
	 * ======================
	 * Statistical parameters
	 */
public:
	/**
	 * \brief	This function gives the mean of *this.
	 *
	 * This is the mean that the distribution has.
	 *
	 */
	virtual
	MeanVec_t
	getMean()
	 const
	 final;


	/**
	 * \brief	Returns the variance
	 */
	virtual
	CoVarMat_t
	getCoVar()
	 const
	 final;


	/**
	 * \brief	Returns the alpha value of of *this.
	 *
	 * This is the value that was passed on construction as Alpha value.
	 */
	virtual
	Numeric_t
	getAlpha()
	 const
	 final;


	/**
	 * \brief	This function returns the Beta value of *this.
	 *
	 * This is the beta value that was passed to this uppon construction.
	 */
	virtual
	Numeric_t
	getBeta()
	 const
	 final;





	/*
	 * ===================
	 * Domain information
	 *
	 */
public:
	/**
	 * \brief	THis function reetruns the dimension the generated samples has.
	 *
	 * This is trivially always one.
	 */
	virtual
	Size_t
	nDims()
	 const
	 override;


	/**
	 * \brief	Returns true if this is undounded.
	 */
	virtual
	bool
	isUnbounded()
	 const
	 override;


	/**
	 * \brief	This function returns the domain, in which the
	 * 		 sampleing is restricted.
	 *
	 * This function returns an invalid domain if the sampling is unbounded.
	 *
	 * Note that for compability reasons thsi fuction returns a hypercube.
	 */
	virtual
	HyperCube_t
	getSampleDomain()
	 const
	 override;


	/*
	 * ===================
	 * Workhorse interface
	 */
protected:

	/**
	 * \brief	This function generates one sample that is
	 * 		 distributed as the implementation.
	 *
	 * See in teh description above for how it works.
	 *
	 * \param  generator		This is the random generator
	 * \param  sample		This is the sample that should be generated.
	 */
	virtual
	void
	wh_generateSample(
		pRNGenerator_t&		generator,
		Sample_t*		sample)
	 override;


	/**
	 * \brief	This function return the pdf of the sample.
	 *
	 * This is the normal version of the exponential
	 * we are used.
	 *
	 * \param  sample	The pdf to compute
	 */
	virtual
	Real_t
	wh_pdf(
		const Sample_t&		sample)
	 const
	 override;

	/*
	 * ========================
	 * Dumping ffunction
	 *
	 * As stated in the interface, this only
	 * saves the state of the random distrinbution
	 * that are defined from the standard.
	 * Other parameter are not saved/loaded.
	 */
public:
	/**
	 * \brief	Write to a stream
	 */
	virtual
	std::ostream&
	dumpToStream(
		std::ostream&	dumpStream)
	 override;

	/**
	 * \brief	Load from a stream
	 */
	virtual
	std::istream&
	loadFromStream(
		std::istream&	dumpStream)
	 override;


	/*
	 * =======================
	 * Private Memeber
	 */
private:
	StdGammaDist_t 		m_gamma;
	IntervalBound_t 	m_domain;
	Numeric_t 		m_normFac;	//This is the normalization factor
	bool 			m_isBounded;	//This indicates if the domain is bounded or not
}; //End: class(yggdrasil_gammaDistri_t)




YGGDRASIL_NS_END(yggdrasil)


