#pragma once
/**
 * \brief	This file contains the code that allows to
 * 		 generate multi dimensionals dirichlet numbers.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>


//Including std


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)


/**
 * \brief	This class allows multidimensional Dirichlet numbers.
 * \class	yggdrasil_multiDimDirichlet_t
 *
 * This class will generate multi dimensional dirichlet samples.
 * The method is described at:
 * 	https://en.wikipedia.org/wiki/Dirichlet_distribution#Random_number_generation
 *
 * This is the second incranation of this class.
 * The first followed the definition outlined in Wikipedia, the second, this, versin followes,
 * the one that is used inside the paper.
 *
 * In the new convention the K dimensional dirichlet distribution is described
 * by K+1 many parameters. Also we only requere that $\sum_{i = 1}^{k} x_i < 1$
 * has to hold.
 * This means the dimension correction for the dirichlet distribution has gone.
 */
class yggdrasil_multiDimDirichlet_t final : public yggdrasil_randomDistribution_i
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using Alpha_t		= ::Eigen::VectorXd;		//!< This is the type for representing the mean vector.
	using GammaDistri_t	= ::std::gamma_distribution<Numeric_t>;	//!< This is the gamma distribution that is needed to sample.
	using GammaDistVector_t = ::std::vector<GammaDistri_t>;		//!< This is a vector with the individual distributions.

	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor takes a vector of the parameter that describes
	 * the Dirichlet distribution.
	 * It is an Eigen vector, Alpha_t.
	 * The parameter must be greater than two.
	 *
	 * Notice that for an K dimensional dirchlet distribution,
	 * the Alpha vector must have K+1 many parameters and the
	 * hyper cube must have K many dimension.
	 *
	 * Note that when using this constructer, the domain must be valid.
	 * This is the only requierment that it is imposed.
	 * If the donain can contain dirichlet samples at all, is not checked.
	 *
	 * \param  Alpha	The parameter vector that is used.
	 * \param  domain 	The domain where we sample in.
	 */
	yggdrasil_multiDimDirichlet_t(
		const Alpha_t&		Alpha,
		const HyperCube_t&	domain);



	/**
	 * \brief	Building contructor.
	 *
	 * This constructs a K dimensional Dirichlet distribution.
	 *
	 * \param  Alpha	The parameter vector that is used, its length is K+1
	 */
	yggdrasil_multiDimDirichlet_t(
		const Alpha_t&		Alpha);



	/**
	 * \brief	This is the destructor.
	 *
	 * It is defaulted.
	 */
	virtual
	~yggdrasil_multiDimDirichlet_t();


	/**
	 * \brief	This is a colne function.
	 *
	 * This function returns a copy of *this.
	 * It is like the java fucntion.
	 */
	virtual
	Distribution_ptr
	clone()
	 const
	 final;


	/**
	 * \brief	This is teh copy constrructor
	 *
	 * It is defaulted.
	 */
	yggdrasil_multiDimDirichlet_t(
		const yggdrasil_multiDimDirichlet_t&);


	/**
	 * \brief	The copy assignment operator.
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiDimDirichlet_t&
	operator= (
		const yggdrasil_multiDimDirichlet_t&);


	/**
	 * \brief	The move constructor
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiDimDirichlet_t(
		yggdrasil_multiDimDirichlet_t&&);


	/**
	 * \brief	The move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiDimDirichlet_t&
	operator= (
		yggdrasil_multiDimDirichlet_t&&);


	/**
	 * \brief	The default constructor.
	 *
	 * Is deleted.
	 */
	yggdrasil_multiDimDirichlet_t()
	 = delete;


	/*
	 * =====================
	 * Random number functions
	 */
public:
	/*
	 * No functions here, since all of the defaults
	 * are used.
	 *
	 * If we would list them we would need to
	 * explicitly code that the default are used.
	 */


	/*
	 * ======================
	 * Statistical parameters
	 */
public:
	/**
	 * \brief	This function returns the parameter vector alpha.
	 *
	 * Notice that this vector is of length K + 1, where K is the dimension.
	 */
	Alpha_t
	getAlpha()
	 const;


	/**
	 * \brief	This function returns the mean of *this.
	 *
	 * See Wikipedia for the formula.
	 */
	virtual
	MeanVec_t
	getMean()
	 const
	 final;


	/**
	 * \brief	Get the covariance of *this.
	 *
	 * See Wikipedia for the formula.
	 */
	virtual
	CoVarMat_t
	getCoVar()
	 const
	 final;


	/*
	 * ==================
	 * Domain information
	 */
public:
	/**
	 * \brief	THis function reetruns the dimension the generated samples has.
	 *
	 * This is K, the number of dimension.
	 */
	virtual
	Size_t
	nDims()
	 const
	 final;


	/**
	 * \brief	Returns true if this is undounded.
	 *
	 * This function alves return false.
	 */
	virtual
	bool
	isUnbounded()
	 const
	 final;


	/**
	 * \brief	This function returns the domain, in which the
	 * 		 sampleing is restricted.
	 *
	 * This function returns a copy off the domain that was
	 * passed upon construction.
	 * If no domain was given, then the unit hypercube is given.
	 */
	virtual
	HyperCube_t
	getSampleDomain()
	 const
	 final;


	/*
	 * ===================
	 * Workhorse interface
	 */
protected:

	/**
	 * \brief	This function generates one sample that is
	 * 		 distributed as the implementation.
	 *
	 * See in teh description above for how it works.
	 *
	 * \param  generator		This is the random generator
	 * \param  sample		This is the sample that should be generated.
	 */
	virtual
	void
	wh_generateSample(
		pRNGenerator_t&		generator,
		Sample_t*		sample)
	 final;


	/**
	 * \brief	This function return the pdf of the sample.
	 *
	 * This is the normal version of the exponential
	 * we are used.
	 *
	 * \param  sample	The pdf to compute
	 */
	virtual
	Real_t
	wh_pdf(
		const Sample_t&		sample)
	 const
	 final;

	/*
	 * ========================
	 * Dumping ffunction
	 *
	 * As stated in the interface, this only
	 * saves the state of the random distrinbution
	 * that are defined from the standard.
	 * Other parameter are not saved/loaded.
	 */
public:
	/**
	 * \brief	Write to a stream
	 */
	virtual
	std::ostream&
	dumpToStream(
		std::ostream&	dumpStream)
	 final;

	/**
	 * \brief	Load from a stream
	 */
	virtual
	std::istream&
	loadFromStream(
		std::istream&	dumpStream)
	 final;

	/*
	 * =======================
	 * Private Memeber
	 */
private:
	GammaDistVector_t 	m_gammaDist;	//!< This are the gamma distribution that are used for the generation.
	HyperCube_t 		m_domain;	//!< The domain where we restrinc ourself. If unvalid unrestrcted.
	Real_t 			m_invBeta;	//!< This is the inverse of the betta function of the parameters
						//!<  Is needed for the pdf.
}; //End: class(yggdrasil_multiDimDirichlet_t)




YGGDRASIL_NS_END(yggdrasil)


