

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_betaPDF.hpp>


//Including std
#include <cmath>


//Include Boost
#include <boost/math/special_functions/beta.hpp>


//Including Eigen


YGGDRASIL_NS_START(yggdrasil)


yggdrasil_betaDistri_t::yggdrasil_betaDistri_t(
	const yggdrasil_betaDistri_t&)
 = default;

yggdrasil_betaDistri_t::~yggdrasil_betaDistri_t()
 = default;


yggdrasil_betaDistri_t::Distribution_ptr
yggdrasil_betaDistri_t::clone()
 const
{
	return Distribution_ptr(new yggdrasil_betaDistri_t(*this));
};

yggdrasil_betaDistri_t::yggdrasil_betaDistri_t(
	yggdrasil_betaDistri_t&&)
 = default;


yggdrasil_betaDistri_t&
yggdrasil_betaDistri_t::operator= (
	yggdrasil_betaDistri_t&&)
 = default;

yggdrasil_betaDistri_t&
yggdrasil_betaDistri_t::operator= (
	const yggdrasil_betaDistri_t&)
 = default;


yggdrasil_betaDistri_t::MeanVec_t
yggdrasil_betaDistri_t::getMean()
 const
{
	//Very starge see constructor
	const Real_t Alpha    = m_gammaX.alpha();
	const Real_t Beta     = m_gammaY.alpha();

	const Real_t Mean = Alpha / (Alpha + Beta);

	return MeanVec_t::Constant(1, Mean);
}; //End: getMean


yggdrasil_betaDistri_t::CoVarMat_t
yggdrasil_betaDistri_t::getCoVar()
 const
{
	//Very starge see constructor
	const Real_t Alpha    = m_gammaX.alpha();
	const Real_t Beta     = m_gammaY.alpha();

	const Real_t numerator   = Alpha * Beta;
	const Real_t sumAB       = Alpha + Beta;
	const Real_t denumerator = (sumAB + 1.0) * (sumAB * sumAB);

	const Real_t var = numerator / denumerator;

	return CoVarMat_t::Constant(1, 1, var);
}; //End: get covar

Numeric_t
yggdrasil_betaDistri_t::getAlpha()
 const
{
	return m_gammaX.alpha();
};

Numeric_t
yggdrasil_betaDistri_t::getBeta()
 const
{
	return m_gammaY.beta();
};




Size_t
yggdrasil_betaDistri_t::nDims()
 const
{
	return 1;
}; //End getDim


bool
yggdrasil_betaDistri_t::isUnbounded()
 const
{
	return m_domain.isValid() == false;
};

yggdrasil_betaDistri_t::HyperCube_t
yggdrasil_betaDistri_t::getSampleDomain()
 const
{
	return HyperCube_t(m_domain);
};

yggdrasil_betaDistri_t::yggdrasil_betaDistri_t(
	const Numeric_t 	Alpha,
	const Numeric_t 	Beta)
 :
  yggdrasil_betaDistri_t(
  	Alpha, Beta, IntervalBound_t::CREAT_UNIT_INTERVAL())
{
}; //End domain is invalid

yggdrasil_betaDistri_t::yggdrasil_betaDistri_t(
		const Numeric_t 	Alpha,
		const Numeric_t 	Beta,
		const IntervalBound_t&	domain)
 :
  yggdrasil_betaDistri_t(Alpha, Beta, HyperCube_t(domain))
{};

yggdrasil_betaDistri_t::yggdrasil_betaDistri_t(
		const Numeric_t 	Alpha,
		const Numeric_t 	Beta,
		const HyperCube_t&	domain)
 :
  m_gammaX(Alpha, 1.0),
  m_gammaY(Beta,  1.0),
  m_domain(IntervalBound_t::CREAT_INVALID_INTERVAL()),	//This is a temprary state
  m_normFac(NAN)
{
	yggdrasil_assert(Alpha > 0.0);
	yggdrasil_assert(Beta  > 0.0);
	yggdrasil_assert(m_gammaX.alpha() == Alpha);
	yggdrasil_assert(m_gammaY.alpha() == Beta );

	if(domain.isValid() == false)	//Important must be the argument domain
	{
		//The argument domain is not valid, so we use the unit interval anyway.
		m_domain = IntervalBound_t::CREAT_UNIT_INTERVAL();
	}
	else
	{
		//Since an invalid cube han have any number of dimensions,
		//we can only now test, if the cube has dimension one, as we requiere
		if(domain.nDims() != 1)		//Must use the argument
		{
			throw YGGDRASIL_EXCEPT_InvArg("the used domain has not dimension 1.");
		};

		//Now we can load the opresented domain
		m_domain = domain.at(0);

		//We makes some smll test if it is possible that samples could be genrated
		if(1.0 <= m_domain.lower())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The domain has a lower bound of more than one, this means it is impossible to generate any samples.");
		};
		if(m_domain.upper() <= 0.0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The upper bound of the domain is less than zero, so no samples at all can be generated.");
		};
	}; //End: handling a valid domain as argument

	//Calculating the normalization value, this is the inverse of the betta function
	const Real_t BetaF = ::boost::math::beta<Real_t, Real_t>(Alpha, Beta);
	m_normFac = 1.0 / BetaF;
	if(isValidFloat(m_normFac) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Error in computing the normalization factor.");
	};
}; //ENd: building constructor


Real_t
yggdrasil_betaDistri_t::wh_pdf(
	const Sample_t&		x)
 const
{
	if(x.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The sample is not valid.");
	};
	if(x.nDims() != 1)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension is wrong.");
	};

	/*
	 * Test if the sample lies inside the domain.
	 * We also have to take care since our interval
	 * definition is not particulary
	 * compatible with teh one deeded by the
	 * beta distribution.
	 */

	//Load the smaples
	const Real_t x_0      = x[0];

	if((m_domain.isInside(x_0) == false) || (x_0 <= 0.0))
	{
		return Real_t(0.0);
	};

	//Very starge see constructor
	const Real_t Alpha    = m_gammaX.alpha();
	const Real_t Beta     = m_gammaY.alpha();

	const Real_t AlphaPow = ::std::pow(x_0      , Alpha - 1.0);
	const Real_t BetaPow  = ::std::pow(1.0 - x_0, Beta  - 1.0);

	const Numeric_t finalPDF = m_normFac * AlphaPow * BetaPow;

	//Return the value
	return finalPDF;
}; //End: generate PDF





void
yggdrasil_betaDistri_t::wh_generateSample(
	pRNGenerator_t&		generator,
	Sample_t*		s)
{
	/*
	 * This fucntion follows the notation of Wikipedia
	 */

	//Resize the dimension
	if(s->nDims() != 1)
	{
		*s = Sample_t(1);
	}; //End if, resizing

	//These are the two generated gamma distributed numbers
	//that will be used to generate a beta distribution
	Numeric_t X = NAN;
	Numeric_t Y = NAN;

	//This is teh final value
	Numeric_t g = NAN;

	//Test if the domain is valid
	const bool isDomValid = m_domain.isValid();


	//THis is a counter and a bound for the generation
	//This will prevent nerly infinity loops
	Size_t tries = 0;
	const Size_t maxTries = 10000;

	/*
	 * Tring as long as we must to generate a gamma distribnuted sample
	 */
	do
	{
		g = 10.0;	//Can not be initialized wuith NAN, there is an obscure point where we will have a problem.
		X = NAN;
		Y = NAN;

		//Increment the try count
		tries += 1;

		//TEst if we have tried too many times
		if(tries > maxTries)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Was not able to generate a sample.");
		};

		//Generate the two gamma distributed values
		X = m_gammaX(generator);
		Y = m_gammaY(generator);
		yggdrasil_assert(isValidFloat(X));
		yggdrasil_assert(isValidFloat(Y));

		//It can happen that we have -0 what is not so funny
		if((X <= 0.0) || (Y <= 0.0))
		{
			continue;
		};

		//Add the two samples together
		const Numeric_t Z = X + Y;
		yggdrasil_assert(isValidFloat(Z));

		//It can happens that due to finite numerics the result is
		//zero, so we must try again in that case
		if(Z == 0.0)
		{
			continue;
		};

		//There we must have only positiove Z
		yggdrasil_assert(Z > 0.0);

		//Now generate the sample value
		g = X / Z;
		yggdrasil_assert(isValidFloat(g));

		//In the case that the sample is zero, we must try again
		if(g == 0.0)
		{
			continue;
		};
		yggdrasil_assert(isValidFloat(g));
	}
	while(isDomValid && (m_domain.isInside(g) == false));

	yggdrasil_assert(isValidFloat(g));
	yggdrasil_assert(0.0 < g);
	yggdrasil_assert(g < 1.0);

	//Writting the sample
	yggdrasil_assert(isValidFloat(g));
	s->at(0) = g;

	//Return
	return;
}; //End generate a single sample


std::ostream&
yggdrasil_betaDistri_t::dumpToStream(
	std::ostream&	s)
{
	s << m_gammaX << m_gammaY;

	return s;
}; //ENd dump to


std::istream&
yggdrasil_betaDistri_t::loadFromStream(
	std::istream&	s)
{
	s >> m_gammaX >> m_gammaY;

	return s;
};





YGGDRASIL_NS_END(yggdrasil)


