#pragma once
/**
 * \brief	This file contains the main function that
 * 		 deals with the generation of random numbers.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>

//Including std
#include <random>

//INcluding Eigen
#include <Eigen/Core>


YGGDRASIL_NS_START(yggdrasil)

/**
 * \brief	This is the random number gener<ated used in yggdrasil.
 * \typedef	yggdrasil_pRNG_t
 *
 * This class is basically a typefdef of the std::mt19937_64 that
 * is provided by the standard library.
 *
 * This generator is not thread safe.
 */
using yggdrasil_pRNG_t = ::std::mt19937_64;


/**
 * \brief	This function saves the state of the generator
 * 		 to the file.
 *
 * \param  dumpFile	The file the state should written to.
 * \param  geni		The generator to be saved.
 */
extern
void
yggdrasil_pRNG_dump(
	const std::string&		dumpFile,
	const yggdrasil_pRNG_t&		geni);

/**
 * \brief	This fucntion loads the generator from the file
 *
 * \param  dumpFile	The file the state should loaded from.
 * \param  geni		The generator to be written to.
 */
extern
void
yggdrasil_pRNG_load(
	const std::string&	dumpFile,
	yggdrasil_pRNG_t*	geni);


/**
 * \brief	This function estiomates the empirical variance and mean form the data.
 *
 * The function is templated such that any container,
 * that conforms to the uci can be used.
 * A pair is returned, first is the mean and second is the covariance matrix.
 * This function will fail if not at least 2 samples are in the container.
 *
 * \param  samples	The samples that should be estimated.
 *
 * \tparam  Container_t		The container, must conform to UCI.
 */
template<
	class 	Container_t
>
inline
::std::pair<Eigen::VectorXd, Eigen::MatrixXd>
yggdrasil_estinmateStatistic(
	const Container_t&	samples)
{
	//Load the parameter
	const Size_t N = samples.nSamples();
	const Size_t D = samples.nDims();

	//Test if teh numbers of samples is large enough
	if(N < 2)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The number of samples is too small.");
	};

	/*
	 * First we will estimate the mean
	 */
	Eigen::VectorXd estMean = Eigen::VectorXd::Zero(D);

	//Going throgh the scontainer and estimate the mean
	for(Size_t k = 0; k != N; ++k)
	{
		//Load the sample
		const auto s = samples.getSample(k);

		//Check the sample
		yggdrasil_assert((Size_t)s.nDims() == D);
		yggdrasil_assert([D](const yggdrasil_sample_t& s) -> bool
			{for(Size_t i = 0; i != D; ++i)
			  {
			    if(isValidFloat(s[i]) == false)
			      {return false;};
			  };
			  return true;
			}
			(s)	//Call the assert immediatly
		); //End assert

		//Creating a map
		const Eigen::Map<const Eigen::VectorXd> s_Map(s.data(), D);

		//Perform the addition
		estMean += s_Map;

		//Check the mean value
		yggdrasil_assert([D](const Eigen::VectorXd& mean) -> bool
			{for(Size_t i = 0; i != D; ++i)
			  {
			    if(isValidFloat(mean[i]) == false)
			      {return false;};
			  };
			  return true;
			}
			(estMean)	//Call the assert immediatly
		); //End assert
	}; //ENd for(k): going through the samples

	//Scall the mean by the number of samples
	estMean /= (Numeric_t)(N);

	//Test if the sample mean is valid, we enforce this by an exception
	for(Size_t i = 0; i != D; ++i)
	{
		if(isValidFloat(estMean[i]) == false)
		{
			//The mean value is invalid
			throw YGGDRASIL_EXCEPT_RUNTIME("The estimated mean is invalid for component " + std::to_string(i));
		};
	}; //End for(i): test if the mean is correct


	/*
	 * Now we calculating the number covariance matrix
	 * This is a text book implementaion, so do not
	 * expect performance or stability
	 */
	Eigen::MatrixXd estCoVar = Eigen::MatrixXd::Zero(D, D);

	//Going throgh the samples
	for(Size_t k = 0; k != N; ++k)
	{
		//Load the current sample
		const auto s = samples.getSample(k);
		yggdrasil_assert((Size_t)s.nDims() == D);
		//The sample is clean, we know that from the computatioon of the mean

		//Now create an Eigen map from the sample
		const Eigen::Map<const Eigen::VectorXd> s_Map(s.data(), D);

		//Now we subtract the sample mean from the sample
		const Eigen::VectorXd s_ohne_mean = s_Map - estMean;

		for(Size_t i = 0; i != D; ++i)
		{
			//Load the value of s_ohne_mean[i] directly to register
			const Numeric_t s_ohne_mean_i = s_ohne_mean[i];

			for(Size_t j = i; j != D; ++j)	//Only goes through the lower half of the matrix
			{
				//Caluclate the contribution
				const Numeric_t thisContri = s_ohne_mean_i * s_ohne_mean[j];
				yggdrasil_assert(isValidFloat(thisContri));

				//this is the current value in the matrix
				const Numeric_t thisEntry = estCoVar(j, i);	//This is colum-major
				yggdrasil_assert(isValidFloat(thisEntry));

				//Perform the addition
				const Numeric_t newEntry = thisContri + thisEntry;
				yggdrasil_assert(isValidFloat(newEntry));

				//Write the new entry back.
				//doing it thas way frees us from distinguish between i == j
				//and i != j; The reason is the symmetry of the matrix.
				estCoVar(i, j) = newEntry;
				estCoVar(j, i) = newEntry;	//Swapped ordering; ToDo: move out of the loop
								//  This will increase performance
			}; //ENd for(j):
		}; //ENd for(i):
	}; //ENd for(k): samples

	//Scalling the matrix
	estCoVar /= Numeric_t(N - 1);

	//Make a test if the matrix is valid,
	//this is done aby exceptions
	for(Size_t i = 0; i != D; ++i)
	{
		for(Size_t j = 0; j != D; ++j)
		{
			//Test if entry (i, j) is valid
			if(isValidFloat(estCoVar(i, j)) == false)
			{
				//Entry is not valid, so generate an exception
				throw YGGDRASIL_EXCEPT_RUNTIME("The entry "
						"(" + std::to_string(i) + ", " + std::to_string(j) + ") "
						"of the covariance matrix is invalid.");
			}; //End if: throw if invalid
		}; //End for(j):
	}; //End for(i):

	//Returning, mean and covariance
	return std::make_pair(estMean, estCoVar);
}; //End: estimate statistic





YGGDRASIL_NS_END(yggdrasil)


