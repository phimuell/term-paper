#pragma once
/**
 * \brief	This file contains the code that allows to
 * 		 generate multi dimensionals Gaussians.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>


//Including std


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)


/**
 * \brief	This class allows multidimensional Gaussian samples.
 * \class	yggdrasil_multiDimGauss_t
 *
 * This class deals with the generation of multidimensional sampels.
 * That are distributed acording a multidimensional gaussian.
 *
 * Thsi class implements the distribution interface.
 * Thus only three fucntions are implemented.
 *
 * For an idea how to do it see:
 * 	- "https://math.stackexchange.com/questions/2079137/generating-multivariate-normal-samples-why-cholesky"
 * 	- "https://en.wikipedia.org/wiki/Multivariate_normal_distribution#Drawing_values_from_the_distribution"
 */
class yggdrasil_multiDimGauss_t final : public yggdrasil_randomDistribution_i
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using CholDecomp_t 	= ::Eigen::LLT<CoVarMat_t>;			//!< This is the Cholesky decomposition.
	using OneDimGauss_t 	= ::std::normal_distribution<Numeric_t>;	//!< This is the type for a one dimensional normal distribution
	using Super_t 		= yggdrasil_randomDistribution_i;		//!< This is the interface.



	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * It takes the covarient matrix, which must be SPD,
	 * and the mean of the samples should have.
	 *
	 * The fucntion can be restricted such that only
	 * samples inside domain are generated.
	 * If this argument is invalid, then no restriction are applied.
	 *
	 * \param  mean		The mean that should be used.
	 * \param  coVar	The covariant matrix.
	 * \param  domain	Onyl generate in that domain.
	 *
	 * \throw	If an error occured, like the Cholesky decomposition failed.
	 *
	 * \note	The Cholesky is performed using Eigen.
	 * 		 No fiurther tests abnout the applicability are performed.
	 * 		 If eigen goes with YOUR matrix, then *this will go.
	 */
	yggdrasil_multiDimGauss_t(
		const MeanVec_t&	mean,
		const CoVarMat_t&	coVar,
		const HyperCube_t&	domain);


	/**
	 * \brief	Building contructor.
	 *
	 * It takes the covarient matrix, which must be SPD,
	 * and the mean of the samples should have.
	 *
	 * No restriction are applied.
	 *
	 * \param  mean		The mean that should be used.
	 * \param  coVar	The covariant matrix.
	 *
	 * \throw	If an error occured, like the Cholesky decomposition failed.
	 *
	 * \note	The Cholesky is performed using Eigen.
	 * 		 No fiurther tests abnout the applicability are performed.
	 * 		 If eigen goes with YOUR matrix, then *this will go.
	 *
	 * \note	This constructor is the same as callingthe above
	 * 		 with an invalid hypercube.
	 */
	yggdrasil_multiDimGauss_t(
		const MeanVec_t&	mean,
		const CoVarMat_t&	coVar);


	/**
	 * \brief	This is the destructor.
	 *
	 * It is defaulted.
	 */
	virtual
	~yggdrasil_multiDimGauss_t();


	/**
	 * \brief	This is a colne function.
	 *
	 * This function returns a pointer to a copied object of *this.
	 */
	virtual
	Distribution_ptr
	clone()
	 const
	 final;


	/**
	 * \brief	This is teh copy constrructor
	 *
	 * It is defaulted.
	 */
	yggdrasil_multiDimGauss_t(
		const yggdrasil_multiDimGauss_t&);


	/**
	 * \brief	The copy assignment operator.
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiDimGauss_t&
	operator= (
		const yggdrasil_multiDimGauss_t&);


	/**
	 * \brief	The move constructor
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiDimGauss_t(
		yggdrasil_multiDimGauss_t&&);


	/**
	 * \brief	The move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiDimGauss_t&
	operator= (
		yggdrasil_multiDimGauss_t&&);


	/**
	 * \brief	The default constructor.
	 *
	 * Is deleted.
	 */
	yggdrasil_multiDimGauss_t()
	 = delete;


	/*
	 * =====================
	 * Random number functions
	 */
public:
	/*
	 * No functions here, since all of the defaults
	 * are used.
	 *
	 * If we would list them we would need to
	 * explicitly code that the default are used.
	 */


	/*
	 * ======================
	 * Statistical parameters
	 */
public:
	/**
	 * \brief	This function gives the mean of *this.
	 *
	 * This is the mean that the distribution has.
	 */
	virtual
	MeanVec_t
	getMean()
	 const
	 final;


	/**
	 * \brief	This fucntion returns the covariance matrix of *this.
	 *
	 * \note	This is not directly the matrix that was used for construction.
	 * 		 *this only stores the choleskyfactor. THe covariance matrix
	 * 		 is reconstructed from this.
	 */
	virtual
	CoVarMat_t
	getCoVar()
	 const
	 final;


	/*
	 * ====================
	 * Domain info
	 */
public:
	/**
	 * \brief	THis function reetruns the dimension the generated samples has.
	 */
	virtual
	Size_t
	nDims()
	 const
	 final;


	/**
	 * \brief	Returns true if this is undounded.
	 */
	virtual
	bool
	isUnbounded()
	 const
	 final;


	/**
	 * \brief	This function returns the domain, in which the
	 * 		 sampleing is restricted.
	 *
	 * This function returns an invalid domain if the sampling is unbounded.
	 */
	virtual
	HyperCube_t
	getSampleDomain()
	 const
	 final;


	/*
	 * ===================
	 * Workhorse interface
	 */
protected:

	/**
	 * \brief	This function generates one sample that is
	 * 		 distributed as the implementation.
	 *
	 * See in teh description above for how it works.
	 *
	 * \param  generator		This is the random generator
	 * \param  sample		This is the sample that should be generated.
	 */
	virtual
	void
	wh_generateSample(
		pRNGenerator_t&		generator,
		Sample_t*		sample)
	 final;


	/**
	 * \brief	This function return the pdf of the sample.
	 *
	 * This is the normal version of the exponential
	 * we are used.
	 *
	 * \param  sample	The pdf to compute
	 */
	virtual
	Real_t
	wh_pdf(
		const Sample_t&		sample)
	 const
	 final;

	/*
	 * ========================
	 * Dumping ffunction
	 *
	 * As stated in the interface, this only
	 * saves the state of the random distrinbution
	 * that are defined from the standard.
	 * Other parameter are not saved/loaded.
	 */
public:
	/**
	 * \brief	Write to a stream
	 */
	virtual
	std::ostream&
	dumpToStream(
		std::ostream&	dumpStream)
	 final;

	/**
	 * \brief	Load from a stream
	 */
	virtual
	std::istream&
	loadFromStream(
		std::istream&	dumpStream)
	 final;


	/*
	 * =======================
	 * Private Memeber
	 */
private:
	MeanVec_t 	m_mean;		//!< This is the mean of the distribution.
	CholDecomp_t 	m_chol;		//!< This is the Cholesky decomposition.
	OneDimGauss_t 	m_1DGauss;	//!< One D normal distribution.
	Real_t 		m_pdfScale;	//!< This is the scaling coefficient in front of the PDF.
	HyperCube_t 	m_domain;	//!< The domain where we restrinc ourself. If unvalid unrestrcted.
}; //End: class(yggdrasil_multiDimGauss_t)




YGGDRASIL_NS_END(yggdrasil)


