/**
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>


//Including std
#include <algorithm>

//Include Boost
#include <boost/filesystem.hpp>


YGGDRASIL_NS_START(yggdrasil)


//Using the Path
using Path_t = ::boost::filesystem::path;

namespace fs = ::boost::filesystem;


/*
 * =======================
 * FWD of the template functions
 */
template<
	class 	Container_t,
	class 	pdfVector_t
>
extern
void
ygInternal_generateManySamples(
	yggdrasil_randomDistribution_i& 			self,
	yggdrasil_randomDistribution_i::pRNGenerator_t&		generator,
	Size_t 							N,
	Container_t* const 					samples,
	pdfVector_t* const 					pdfs);



template<
	class 	Container_t,
	class 	pdfVector_t
>
extern
pdfVector_t
ygInteranl_evaluateManySamples(
	const yggdrasil_randomDistribution_i& 	self,
	const Container_t& 			samples);


/*
 * ===================
 * Constructors
 */
yggdrasil_randomDistribution_i::yggdrasil_randomDistribution_i(
	const yggdrasil_randomDistribution_i&)
 = default;

yggdrasil_randomDistribution_i::~yggdrasil_randomDistribution_i()
 = default;

yggdrasil_randomDistribution_i::yggdrasil_randomDistribution_i(
	yggdrasil_randomDistribution_i&&)
 = default;

 yggdrasil_randomDistribution_i::yggdrasil_randomDistribution_i()
 = default;


yggdrasil_randomDistribution_i&
yggdrasil_randomDistribution_i::operator= (
	yggdrasil_randomDistribution_i&&)
 = default;

yggdrasil_randomDistribution_i&
yggdrasil_randomDistribution_i::operator= (
	const yggdrasil_randomDistribution_i&)
 = default;



/*
 * =====================
 * Single samples
 */
yggdrasil_randomDistribution_i::Sample_t
yggdrasil_randomDistribution_i::generateSample(
	pRNGenerator_t&		generator)
{
	const Size_t dims = this->nDims();
	if(dims <= 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dimension of the sampel is zero or less.");
	};

	//Get the domain
	const HyperCube_t domain = this->getSampleDomain();

	//Test if the domain is valid
	const bool isDomainValid = domain.isValid();

	//This is for the starvation protection
	const Size_t maxTries = 2000;
	      Size_t tries    = 0;

	//This is the pdf value that is used
	Real_t thisPDF = NAN;

	//This is is the sample that is generated
	Sample_t s(dims);

	do
	{
		//Set the pdf variable to NAN
		thisPDF = NAN;

		//Handle the starvation protection
		tries += 1;
		if(tries >= maxTries)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("A starvation was detected.");
		};

		//Generate the domain
		this->wh_generateSample(generator, &s);

		//Test if the sample is still valid
		yggdrasil_assert(s.isValid());
		yggdrasil_assert(s.size() == dims);

		//Compute the pdf of the sample to check if its probability
		//is also greater than zero in finite arethmetic
		thisPDF = this->wh_pdf(s);

		//For consistency we generate here an error (the reason is that we do the
		//same in the many sample case)
		if(isValidFloat(thisPDF) == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("A non valid pdf value was generated.");
		};

		//Test the pdf values
		yggdrasil_assert(thisPDF >= 0.0);	//Here we allow for zero probability
	}
	while(
		(thisPDF <= 0.0)				//In some  case cancelation can create samples that are numerical unprobable
		||
		(isDomainValid && (domain.isInside(s) == false))//Test if the sample is in teh domain
	); //End while

	//Make some final tests
	yggdrasil_assert(isDomainValid ? domain.isInside(s) : true);
	yggdrasil_assert(isValidFloat(thisPDF));
	yggdrasil_assert(thisPDF > 0.0);

	//Return the smaple
	return s;
}; //End generate a single sample


Real_t
yggdrasil_randomDistribution_i::pdf(
	const Sample_t&		x)
 const
{
	//Get the domain
	const HyperCube_t domain = this->getSampleDomain();

	//Test if the domain is valid
	const bool isDomainValid = domain.isValid();

	//Test if the sample is outside a valid domain
	if(isDomainValid && (domain.isInside(x) == false))
	{
		//The sample is outside the sampling domain,
		//so by definition we return zero

		return Real_t(0.0);
	}; //End if: sample outside the sampling domain

	//Calcualte the pdf of teh sample
	const Real_t pdf_i = this->wh_pdf(x);

	//For consistency we have to generate an exception.
	if(isValidFloat(pdf_i) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("A non bvalid pdf value was generated.");
	};

	//Perform some tests
	yggdrasil_assert(pdf_i >= 0.0);		//We allow here zero

	//Return the podf
	return pdf_i;
}; //End: generate PDF


/*
 * ======================
 * SampleList
 */
yggdrasil_randomDistribution_i::SampleList_t
yggdrasil_randomDistribution_i::generateSamplesList(
	pRNGenerator_t&		generator,
	const Size_t 		N)
{
	if(N <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Zero or less samples where requested.");
	};

	const Size_t dims = this->nDims();
	if(dims <= 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dimension of the sampel is zero or less.");
	};

	//Generate the sample array
	SampleList_t sList(dims, N);

	//Use the generic fucntion to sample
	ygInternal_generateManySamples<SampleList_t, PDFValueArray_t>(
			*this,
			generator,
			N,
			&sList,
			nullptr		//No not request pointers
			);
	yggdrasil_assert(sList.nSamples() == N);
	yggdrasil_assert(sList.nDims() == dims);

	//Return
	return sList;;
}; //End: generate a smaple LIST

yggdrasil_randomDistribution_i::PDFValueArray_t
yggdrasil_randomDistribution_i::pdf(
	const SampleList_t&	sList)
 const
{
	const Size_t lN = sList.nSamples();	//Size of the list
	const Size_t lD = sList.nDims();	//Dimension of the samples in the list
	const Size_t dD = this->nDims();	//Dimension of the diostribution
	if(dD != lD)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the samples in the list and the distribtuib differes.");
	};

	//Handle zero case
	if(lN == 0)
	{
		return PDFValueArray_t();
	};

	PDFValueArray_t res = ygInteranl_evaluateManySamples<SampleList_t, PDFValueArray_t>(*this, sList);
	yggdrasil_assert(res.size() == lN);
	yggdrasil_assert(sList.nSamples() == lN);
	yggdrasil_assert(sList.nDims() == lD);

	return res;
}; //ENd pdf list


::std::pair<yggdrasil_randomDistribution_i::SampleList_t, yggdrasil_randomDistribution_i::PDFValueArray_t>
yggdrasil_randomDistribution_i::generateSamplesListPDF(
	pRNGenerator_t&		generator,
	const Size_t 		N)
{
	if(N <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Zero or less samples where requested.");
	};

	const Size_t dims = this->nDims();
	if(dims <= 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dimension of the sampel is zero or less.");
	};

	//Generate the sample array
	SampleList_t sList(dims, N);

	//This list is for the PDFs
	PDFValueArray_t pdfList(N);

	//Call the templated fucntion
	ygInternal_generateManySamples(*this, generator, N, &sList, &pdfList);

	//Return the array
	return ::std::make_pair(sList, pdfList);
}; //End: generate samples and compute the PDF


/*
 * Sample List Eigen
 */
yggdrasil_randomDistribution_i::PDFValueArrayEigen_t
yggdrasil_randomDistribution_i::pdf_Eigen(
	const SampleList_t&	sList)
 const
{
	const Size_t lN = sList.nSamples();	//Size of the list
	const Size_t lD = sList.nDims();	//Dimension of the samples in the list
	const Size_t dD = this->nDims();	//Dimension of the diostribution
	if(dD != lD)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the samples in the list and the distribtuib differes.");
	};

	//Handle zero case
	if(lN == 0)
	{
		return PDFValueArrayEigen_t();
	};

	PDFValueArrayEigen_t res = ygInteranl_evaluateManySamples<SampleList_t, PDFValueArrayEigen_t>(*this, sList);
	yggdrasil_assert((Size_t)res.size() == lN);
	yggdrasil_assert(sList.nSamples() == lN);
	yggdrasil_assert(sList.nDims() == lD);

	return res;
}; //ENd pdf list


::std::pair<yggdrasil_randomDistribution_i::SampleList_t, yggdrasil_randomDistribution_i::PDFValueArrayEigen_t>
yggdrasil_randomDistribution_i::generateSamplesListPDF_Eigen(
	pRNGenerator_t&		generator,
	const Size_t 		N)
{
	if(N <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Zero or less samples where requested.");
	};

	const Size_t dims = this->nDims();
	if(dims <= 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dimension of the sampel is zero or less.");
	};

	//Generate the sample array
	SampleList_t sList(dims, N);

	//This list is for the PDFs
	PDFValueArrayEigen_t pdfList(N);

	//Call the templated fucntion
	ygInternal_generateManySamples(*this, generator, N, &sList, &pdfList);

	//Return the array
	return ::std::make_pair(sList, pdfList);
}; //End: generate samples and compute the PDF




/*
 * =============================
 * Array based functions
 */

yggdrasil_randomDistribution_i::SampleArray_t
yggdrasil_randomDistribution_i::generateSamplesArray(
	pRNGenerator_t&		generator,
	const Size_t 		N)
{
	if(N <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Zero or less samples where requested.");
	};

	const Size_t dims = this->nDims();
	if(dims <= 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dimension of the sampel is zero or less.");
	};

	//Generate the sample array
	SampleArray_t sArray(dims, N);

	//Use the generic fucntion to sample
	ygInternal_generateManySamples<SampleArray_t, PDFValueArray_t>(
			*this,
			generator,
			N,
			&sArray,
			nullptr		//No not request pointers
			);
	yggdrasil_assert(sArray.nSamples() == N);
	yggdrasil_assert(sArray.nDims() == dims);

	//Return
	return sArray;
}; //ENd: generate sample array


yggdrasil_randomDistribution_i::PDFValueArray_t
yggdrasil_randomDistribution_i::pdf(
	const SampleArray_t&	sArray)
 const
{
	const Size_t lN = sArray.nSamples();	//Size of the list
	const Size_t lD = sArray.nDims();	//Dimension of the samples in the list
	const Size_t dD = this->nDims();	//Dimension of the diostribution
	if(dD != lD)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the samples in the list and the distribtuib differes.");
	};

	//Handle zero case
	if(lN == 0)
	{
		return PDFValueArray_t();
	};

	PDFValueArray_t res = ygInteranl_evaluateManySamples<SampleArray_t, PDFValueArray_t>(*this, sArray);
	yggdrasil_assert(res.size() == lN);
	yggdrasil_assert(sArray.nSamples() == lN);
	yggdrasil_assert(sArray.nDims() == lD);

	return res;
}; //ENd pdf list


::std::pair<yggdrasil_randomDistribution_i::SampleArray_t, yggdrasil_randomDistribution_i::PDFValueArray_t>
yggdrasil_randomDistribution_i::generateSamplesArrayPDF(
	pRNGenerator_t&		generator,
	const Size_t 		N)
{
	if(N <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Zero or less samples where requested.");
	};

	const Size_t dims = this->nDims();
	if(dims <= 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dimension of the sampel is zero or less.");
	};

	//Generate the sample array
	SampleArray_t sArray(dims, N);

	//This list is for the PDFs
	PDFValueArray_t pdfList(N);

	//Call the templated fucntion
	ygInternal_generateManySamples(*this, generator, N, &sArray, &pdfList);

	//Return the array
	return ::std::make_pair(sArray, pdfList);
}; //End: generate samples and compute the PDF


/*
 * Sample Array: Eigen
 */
yggdrasil_randomDistribution_i::PDFValueArrayEigen_t
yggdrasil_randomDistribution_i::pdf_Eigen(
	const SampleArray_t&	sArray)
 const
{
	const Size_t lN = sArray.nSamples();	//Size of the list
	const Size_t lD = sArray.nDims();	//Dimension of the samples in the list
	const Size_t dD = this->nDims();	//Dimension of the diostribution
	if(dD != lD)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the samples in the list and the distribtuib differes.");
	};

	//Handle zero case
	if(lN == 0)
	{
		return PDFValueArrayEigen_t();
	};

	PDFValueArrayEigen_t res = ygInteranl_evaluateManySamples<SampleArray_t, PDFValueArrayEigen_t>(*this, sArray);
	yggdrasil_assert((Size_t)res.size() == lN);
	yggdrasil_assert(sArray.nSamples() == lN);
	yggdrasil_assert(sArray.nDims() == lD);

	return res;
}; //ENd pdf list


::std::pair<yggdrasil_randomDistribution_i::SampleArray_t, yggdrasil_randomDistribution_i::PDFValueArrayEigen_t>
yggdrasil_randomDistribution_i::generateSamplesArrayPDF_Eigen(
	pRNGenerator_t&		generator,
	const Size_t 		N)
{
	if(N <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Zero or less samples where requested.");
	};

	const Size_t dims = this->nDims();
	if(dims <= 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dimension of the sampel is zero or less.");
	};

	//Generate the sample array
	SampleArray_t sArray(dims, N);

	//This list is for the PDFs
	PDFValueArrayEigen_t pdfList(N);

	//Call the templated fucntion
	ygInternal_generateManySamples(*this, generator, N, &sArray, &pdfList);

	//Return the array
	return ::std::make_pair(sArray, pdfList);
}; //End: generate samples and compute the PDF




/*
 * ===============================
 * Collection Functions
 */
yggdrasil_randomDistribution_i::SampleCollection_t
yggdrasil_randomDistribution_i::generateSamplesCollection(
	pRNGenerator_t&		generator,
	const Size_t 		N)
{
	if(N <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Zero or less samples where requested.");
	};

	const Size_t dims = this->nDims();
	if(dims <= 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dimension of the sampel is zero or less.");
	};

	//Generate the sample array
	SampleCollection_t sColl(dims, N);

	//Use the generic fucntion to sample
	ygInternal_generateManySamples<SampleCollection_t, PDFValueArray_t>(
			*this,
			generator,
			N,
			&sColl,
			nullptr		//No not request pointers
			);
	yggdrasil_assert(sColl.nSamples() == N);
	yggdrasil_assert(sColl.nDims() == dims);

	//Return
	return sColl;
}; //ENd: generate sample array


yggdrasil_randomDistribution_i::PDFValueArray_t
yggdrasil_randomDistribution_i::pdf(
	const SampleCollection_t&	sColl)
 const
{
	const Size_t lN = sColl.nSamples();	//Size of the list
	const Size_t lD = sColl.nDims();	//Dimension of the samples in the list
	const Size_t dD = this->nDims();	//Dimension of the diostribution
	if(dD != lD)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the samples in the list and the distribtuib differes.");
	};

	//Handle zero case
	if(lN == 0)
	{
		return PDFValueArray_t();
	};

	PDFValueArray_t res = ygInteranl_evaluateManySamples<SampleCollection_t, PDFValueArray_t>(*this, sColl);
	yggdrasil_assert(res.size() == lN);
	yggdrasil_assert(sColl.nSamples() == lN);
	yggdrasil_assert(sColl.nDims() == lD);

	return res;
}; //ENd pdf list


::std::pair<yggdrasil_randomDistribution_i::SampleCollection_t, yggdrasil_randomDistribution_i::PDFValueArray_t>
yggdrasil_randomDistribution_i::generateSamplesCollectionPDF(
	pRNGenerator_t&		generator,
	const Size_t 		N)
{
	if(N <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Zero or less samples where requested.");
	};

	const Size_t dims = this->nDims();
	if(dims <= 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dimension of the sampel is zero or less.");
	};

	//Generate the sample array
	SampleCollection_t sColl(dims, N);

	//This list is for the PDFs
	PDFValueArray_t pdfList(N);

	//Call the templated fucntion
	ygInternal_generateManySamples(*this, generator, N, &sColl, &pdfList);

	//Return the array
	return ::std::make_pair(sColl, pdfList);
}; //End: generate samples and compute the PDF



/*
 * Array Sample: Eigen
 */
yggdrasil_randomDistribution_i::PDFValueArrayEigen_t
yggdrasil_randomDistribution_i::pdf_Eigen(
	const SampleCollection_t&	sColl)
 const
{
	const Size_t lN = sColl.nSamples();	//Size of the list
	const Size_t lD = sColl.nDims();	//Dimension of the samples in the list
	const Size_t dD = this->nDims();	//Dimension of the diostribution
	if(dD != lD)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the samples in the list and the distribtuib differes.");
	};

	//Handle zero case
	if(lN == 0)
	{
		return PDFValueArrayEigen_t();
	};

	PDFValueArrayEigen_t res = ygInteranl_evaluateManySamples<SampleCollection_t, PDFValueArrayEigen_t>(*this, sColl);
	yggdrasil_assert((Size_t)res.size() == lN);
	yggdrasil_assert(sColl.nSamples() == lN);
	yggdrasil_assert(sColl.nDims() == lD);

	return res;
}; //ENd pdf list


::std::pair<yggdrasil_randomDistribution_i::SampleCollection_t, yggdrasil_randomDistribution_i::PDFValueArrayEigen_t>
yggdrasil_randomDistribution_i::generateSamplesCollectionPDF_Eigen(
	pRNGenerator_t&		generator,
	const Size_t 		N)
{
	if(N <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Zero or less samples where requested.");
	};

	const Size_t dims = this->nDims();
	if(dims <= 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dimension of the sampel is zero or less.");
	};

	//Generate the sample array
	SampleCollection_t sColl(dims, N);

	//This list is for the PDFs
	PDFValueArrayEigen_t pdfList(N);

	//Call the templated fucntion
	ygInternal_generateManySamples(*this, generator, N, &sColl, &pdfList);

	//Return the array
	return ::std::make_pair(sColl, pdfList);
}; //End: generate samples and compute the PDF




/*
 * ================
 * Domain fucntions
 */
Size_t
yggdrasil_randomDistribution_i::nDims()
 const
{
	//Aauiere the domain
	const HyperCube_t dom = this->getSampleDomain();

	//Test if teh domain is valid
	if(dom.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The domain is invalid, so the number of dimensions could not be infered from it. Implement the nDims() function corectly.");
	};
	yggdrasil_assert(dom.nDims() == this->getMean().size());

	//Returns the number of domains
	return dom.nDims();
}; //End: nDims



bool
yggdrasil_randomDistribution_i::isUnbounded()
 const
{
	return this->getSampleDomain().isValid() == false;
};





/*
 * =================
 * Dump Function
 */

void
yggdrasil_randomDistribution_i::dumpToFile(
	const std::string&	dumpFile)
{
	if(dumpFile.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The filename for the dumping was empty.");
	};

	//Create a file
	std::ofstream out;
	out.open(dumpFile.c_str(), std::ofstream::trunc | std::ofstream::out);
	if(out.is_open() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Could not open/create the file " + dumpFile);
	};

	//Write to the file
	this->dumpToStream(out);

	if(out.fail() == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("An error while writeing to the file " + dumpFile + " has occured.");
	};

	//close the file
	out.close();

	return;
}; //End: dumpToFile


void
yggdrasil_randomDistribution_i::loadFromFile(
	const std::string&	dumpFile)
{
	if(dumpFile.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The filename for the dumping was empty.");
	};
	if(fs::exists(Path_t(dumpFile)) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The given file " + dumpFile + " does not exists.");
	};

	//Create a file
	std::ifstream in;
	in.open(dumpFile.c_str(), std::ofstream::in);
	if(in.is_open() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Could not open the file " + dumpFile);
	};

	//Write to the file
	this->loadFromStream(in);

	if(in.fail() == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("An error while writeing to the file " + dumpFile + " has occured.");
	};

	//close the file
	in.close();

	return;
}; //End: load from file


std::ostream&
yggdrasil_randomDistribution_i::dumpToStream(
	std::ostream&	s)
{
	throw YGGDRASIL_EXCEPT_RUNTIME("The function is not implemented.");
	(void)s;
};


std::istream&
yggdrasil_randomDistribution_i::loadFromStream(
	std::istream&	s)
{
	throw YGGDRASIL_EXCEPT_RUNTIME("The function is not implemented.");
	(void)s;
};


/*
 * ===========================
 * Friends
 */

template<
	class 	Container_t,
	class 	pdfVector_t
>
void
ygInternal_generateManySamples(
	yggdrasil_randomDistribution_i& 			self,
	yggdrasil_randomDistribution_i::pRNGenerator_t&		generator,
	Size_t 							N,
	Container_t* const 					samples,
	pdfVector_t* const 					pdfs)
{
	/*
	 * Tests
	 */
	if(samples == nullptr)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The pointer to the samples is invalid.");
	};

	//Det the dimensions of the distribution
	const Size_t distDim = self.nDims();


	/*
	 * Resizing the sample container if needed
	 */
	if(samples->nDims() != distDim)
	{
		/*
		 * Re requiere that the conatiner has at least the correct dimension
		 */
		throw YGGDRASIL_EXCEPT_InvArg("The passed conatiner does not have the correct dimension.");
	}
	else
	{
		/*
		 * The dimension is correct but we have to recreate the space
		 */
		if(samples->nSamples() != N)
		{
			/*
			 * The number of samples are not the same
			 * so we must first clear and then resize it.
			 * This is needed since a resize is only allowed if
			 * the size is zero.
			 */
			samples->clear();
			samples->resize(N);
		}; //ENd if: size was wrong
	}; //ENd: handling of the sample container

	yggdrasil_assert(samples->nSamples() == N      );
	yggdrasil_assert(samples->nDims()    == distDim);

	//This variables stores if the pdf value is also requested.
	const bool pdfIsNeeded = (pdfs != nullptr) ? true : false;

	//Resizeing the pfd container if needed
	if(pdfIsNeeded == true && (pdfs->size() != N))
	{
		pdfs->resize(N);
		yggdrasil_assert(pdfs->size() == N);
	}; //ENd resizing of the pdf values


	/*
	 * Actuall generating of the code
	 */
	//THis is the sample that we will use to write to
	yggdrasil_randomDistribution_i::Sample_t s(distDim);
	yggdrasil_assert(s.size() == distDim);

	//Load the domain
	const yggdrasil_randomDistribution_i::HyperCube_t domain = self.getSampleDomain();

	//Test if the domain is valid
	const bool isDomainValid = domain.isValid();

	//Generating the samples
	for(Size_t i = 0; i != N; ++i)
	{
		yggdrasil_assert(s.nDims() == (Int_t)distDim);

		//This is the variable that stores the pdf
		Real_t thisPDF;

		//This are counter to check if a dead lock occured
		const Size_t maxTries = 10000;
		Size_t tries = 0;

		//The loop will prevent the genration of any sample that has zero probability
		do
		{
			//We try one more
			tries += 1;

			//Test if we are stuck
			if(tries > maxTries)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("The generation of the sample has stuck, an erropr was generated for that.");
			}; //ENd stuck

			//Set the pdf to a value that is safe
			thisPDF = NAN;

			//Generate the sample
			self.wh_generateSample(generator, &s);

			//Check if the sample is valid, only in debug mode
			yggdrasil_assert(s.nDims() == (Int_t)distDim);
			yggdrasil_assert(::std::all_of(s.begin(), s.end(), [](const Numeric_t x) -> bool {return isValidFloat(x);}) == true);

			//Calculating the PDF
			thisPDF = self.wh_pdf(s);
			if(isValidFloat(thisPDF) == false)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("The pdf was invalid.");
			};

			//Paranuid checks
			yggdrasil_assert(s.nDims() == (Int_t)distDim);
		}
		while(
			(isDomainValid == true ? (domain.isInside(s) == false) : false)  //Test if the sample is inside
			||
			(thisPDF <= 0.0)		//The pdf must be greater than zero, this is because of numerical artefacts
		     );

		yggdrasil_assert(isValidFloat(thisPDF));
		yggdrasil_assert(thisPDF > 0.0);	//We requere that the distributio
		yggdrasil_assert(s.nDims() == (Int_t)distDim);

		// Inserting the data into the container
		samples->setSampleTo(s, i);

		//insert the pdf value if requered
		if(pdfIsNeeded == true)
		{
			yggdrasil_assert(pdfs != nullptr);
			yggdrasil_assert(i < pdfs->size());

			(*pdfs)[i] = thisPDF;	//Write it
		}; //End if: pdf is needed
	}; //End for(i):


	//End of the calculation
	return;
}; //End template generating fucntuion



template<
	class 	Container_t,
	class 	pdfVector_t
>
pdfVector_t
ygInteranl_evaluateManySamples(
	const yggdrasil_randomDistribution_i& 	self,
	const Container_t& 			samples)
{

	const Size_t  D = samples.nDims();	//Dimension of the sample container
	const Size_t dD = self.nDims(); 	//Dimension of the distribtuin
	if(D != dD)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The runtime and the samples have the wrong dimensions.");
	};

	const Size_t N = samples.nSamples();
	if(N == 0)
	{
		return pdfVector_t();
	};

	//Create the return vector
	pdfVector_t res(N);
	yggdrasil_assert(Size_t(res.size()) == N);

	//Load the domain
	const yggdrasil_randomDistribution_i::HyperCube_t domain = self.getSampleDomain();

	//Test if the domain is valid
	const bool isDomainValid = domain.isValid();

#if defined(YGGDRASIL_OPENMP_PDF_EVAL_RAND_DIST) && YGGDRASIL_OPENMP_PDF_EVAL_RAND_DIST != 0
	#pragma omp parallel for default(shared)
#endif
	for(Size_t i = 0; i < N; ++i)
	{
		//Load the sample from the container
		const yggdrasil_randomDistribution_i::Sample_t s(samples.getSample(i));

		//Test if the sample lies outside the sampling domain
		//If so then set the sample probability to zero
		if(isDomainValid && (domain.isInside(s) == false))
		{
			//The sample is outside the domain, so we assigne zero to it
			yggdrasil_assert(i < res.size());
			res[i] = 0.0;

			//We have handled this sample, so we can end the current iteration
			continue;
		}; //End if: handle outside of domain

		//Calcualte the probability
		const Real_t pdf_i = self.wh_pdf(s);

		//For consistency we also enfore non nan
		if(isValidFloat(pdf_i) == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("A non valid pdf value was generated.");
		};

		//Tets the probability
		yggdrasil_assert(pdf_i >= 0.0);	//Here we allow zero probability, but no negative one

		//Store the probability in the output vector
		yggdrasil_assert(i < res.size());
		res[i] = (pdf_i);
	}; //End for(i):

	//Return the result
	return res;
}; //End: evaluate many samples





YGGDRASIL_NS_END(yggdrasil)


