/**
 * \brief	This file contains the code that allows to
 * 		 generate multi dimensionals Dirichlet.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_multiDirichle.hpp>


//Including std


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)


yggdrasil_multiDimDirichlet_t::yggdrasil_multiDimDirichlet_t(
	const yggdrasil_multiDimDirichlet_t&)
 = default;

yggdrasil_multiDimDirichlet_t::~yggdrasil_multiDimDirichlet_t()
 = default;


yggdrasil_multiDimDirichlet_t::Distribution_ptr
yggdrasil_multiDimDirichlet_t::clone()
 const
{
	return Distribution_ptr(new yggdrasil_multiDimDirichlet_t(*this));
};

yggdrasil_multiDimDirichlet_t::yggdrasil_multiDimDirichlet_t(
	yggdrasil_multiDimDirichlet_t&&)
 = default;


yggdrasil_multiDimDirichlet_t&
yggdrasil_multiDimDirichlet_t::operator= (
	yggdrasil_multiDimDirichlet_t&&)
 = default;

yggdrasil_multiDimDirichlet_t&
yggdrasil_multiDimDirichlet_t::operator= (
	const yggdrasil_multiDimDirichlet_t&)
 = default;


yggdrasil_multiDimDirichlet_t::Alpha_t
yggdrasil_multiDimDirichlet_t::getAlpha()
 const
{
	//N is the number of parameters
	const Size_t N = m_gammaDist.size();
	yggdrasil_assert(N >= 2);

	//Create the output vector
	Alpha_t Alpha(N);

	//Extract alphas from the distribution
	for(Size_t i = 0; i != N; ++i)
	{
		Alpha[i] = m_gammaDist[i].alpha();
	}; //ENd for(i):

	return Alpha;
}; //ENd getAlpha


yggdrasil_multiDimDirichlet_t::MeanVec_t
yggdrasil_multiDimDirichlet_t::getMean()
 const
{
	//N is the number of parameters
	const Size_t N = m_gammaDist.size();
	yggdrasil_assert(N >= 2);

	//This is the number of dimensions
	const Size_t K = N - 1;

	//This is the Mean, it is important that
	//Its length is one less than the number
	//of parameters the distribution has
	Alpha_t Mean(K);

	//This is the sum
	Real_t normConst = 0.0;

	//Load the first K alpha value, this are all
	//except the last one.
	for(Size_t i = 0; i != K; ++i)
	{
		//Load the alpha from the distribution
		const auto alpha_i = m_gammaDist[i].alpha();
		yggdrasil_assert(alpha_i > 0.0);

		//Store it in the mean
		Mean[i] 	   = alpha_i;

		//Summ up the normalizaing constant
		normConst         += alpha_i;
	}; //ENd for(i):

	//We have to add the last alpha value, for the normalizaion
	yggdrasil_assert(m_gammaDist.back().alpha() > 0.0);
	normConst += m_gammaDist.back().alpha();

	//Normalizing the mean
	Mean /= normConst;

	return Mean;
}; //ENd getMean


yggdrasil_multiDimDirichlet_t::CoVarMat_t
yggdrasil_multiDimDirichlet_t::getCoVar()
 const
{
	//This is the number of parameters we have
	const Size_t N = m_gammaDist.size();
	yggdrasil_assert(N >= 2);

	//This is teh number of dimensions we have
	const Size_t K = N - 1;


	/*
	 * For the variable name, see wikipedia for the terminology
	 *
	 * We apply the samle formula as before but skip the last index.
	 */
	Alpha_t Alpha;	//This vector is already reduced. the last alpha value
				//is only important for the normalization, and will
				//be included below.
	Alpha.resize(K);

	//This is the sum, we initalize it with the last value of
	//Alpha, I had strange memory bugs in other cases
	//This directly includes the handling
	Real_t Alpha_0 = m_gammaDist.back().alpha();

	//Create the alpha vector
	for(Size_t i = 0; i != K; ++i)
	{
		const auto alpha_i = m_gammaDist[i].alpha();
		Alpha[i] 	   = alpha_i;
		Alpha_0		  += alpha_i;
	}; //ENd for(i):

	//Normalizing the Alphas to get a AlphaTilde
	//We must here normalize with the sum off ALL
	//alphas!
	const Alpha_t AlphaTilde = Alpha / Alpha_0;
	yggdrasil_assert((Size_t)AlphaTilde.size() == K);

	//Generate the matrix for the variance
	CoVarMat_t coVar(K, K);


	//I know it would be possible to do it with less flops, but it would be more work
	//for something that will run almost in no time
	for(Size_t i = 0; i != K; ++i)
	{
		for(Size_t j = 0; j != K; ++j)
		{
			if(i == j)
			{
				//Variance
				coVar(i, j) = (AlphaTilde[i] * (1.0 - AlphaTilde[i])) / (Alpha_0 + 1.0);
			}
			else
			{
				//CoVar
				coVar(i, j) = (-1.0) * AlphaTilde[i] * AlphaTilde[j] / (Alpha_0 + 1.0);
			};
		}; //ENd for(j)
	}; //End for(i)

	return coVar;
}; //ENd covar




Size_t
yggdrasil_multiDimDirichlet_t::nDims()
 const
{
	yggdrasil_assert(m_gammaDist.size() > 0);
	yggdrasil_assert(m_domain.isValid());

	//Now for the correction
	return (m_gammaDist.size() - 1);
}; //End getDim


bool
yggdrasil_multiDimDirichlet_t::isUnbounded()
 const
{
	yggdrasil_assert(m_gammaDist.size() >= 2);
	yggdrasil_assert(m_domain.isValid());

	return false;
};

yggdrasil_multiDimDirichlet_t::HyperCube_t
yggdrasil_multiDimDirichlet_t::getSampleDomain()
 const
{
	yggdrasil_assert(m_gammaDist.size() > 0);

	return m_domain;
};




yggdrasil_multiDimDirichlet_t::yggdrasil_multiDimDirichlet_t(
	const Alpha_t& 	Alpha,
	const HyperCube_t&	domain)
 :
  m_gammaDist(),
  m_domain(domain),
  m_invBeta(NAN)
{
	if(Alpha.size() <= 1)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the vector is one or lower.");
	};

	//TEst if the domain must be estimated
	if(domain.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The domain is ionvalid.");
	}; //End if: domain must be estimated

	//Test if the size constraint are vialated
	if(m_domain.size() != Size_t(Alpha.size() - 1))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The size of the dimension and the Alpha vector is not compatible.");
	};



	//This is not the number of dimensions
	//This is the number of paramater we have.
	//This is one more than the number of dimension!
	const Size_t N = (Size_t)Alpha.size();

	//Reserve space
	yggdrasil_assert(m_gammaDist.empty());
	m_gammaDist.reserve(N);

	//Now create the distribution that are needed.
	for(Size_t i = 0; i != N; ++i)
	{
		if(Alpha[i] <= 0.0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The " + std::to_string(i) + "-th Alpha value was negative.");
		};

		m_gammaDist.push_back(
			GammaDistri_t(
				Alpha[i],	//Shape
				1.0		//Scale
			)
		);
		yggdrasil_assert(m_gammaDist.back().alpha() == Alpha[i]);
	}; //End for(i): creating the distributions

	/*
	 * The Normalizing constant is given as
	 * 	\frac{1}{B(\alpha)}
	 *
	 * Where B(\alpha) is given by the following expresion:
	 * 	B(\alpha) := \frac{\prod_{i = 1}^{N} \Gamma(\alpha_i)}{\Gamma(\sum_{i = 1}^{N} \alpha_i)}
	 */

	//THis are helper variables
	Real_t sumOfAlpha  = 0.0;
	Real_t prodOfGamma = 1.0;

	for(Size_t i = 0; i != N; ++i)
	{
		//Load the alpha value
		const Real_t alpha_i = Alpha[i];

		//Sum up the alphas
		sumOfAlpha += alpha_i;

		//Compute the gamma fucntion of that
		const Real_t gammaAlpha_i = ::std::tgamma(alpha_i);
		yggdrasil_assert(isValidFloat(gammaAlpha_i));

		//Compute the product
		prodOfGamma *= gammaAlpha_i;
	}; //End for(i): get the values together

	//Now compute the gama value of the sup of parameters
	const Real_t gammaSumOfAlpha = ::std::tgamma(sumOfAlpha);
	yggdrasil_assert(isValidFloat(gammaSumOfAlpha));

	//Compute the inverse of the B value
	m_invBeta = gammaSumOfAlpha / prodOfGamma;
	yggdrasil_assert(isValidFloat(m_invBeta));
}; //End: building constructor


yggdrasil_multiDimDirichlet_t::yggdrasil_multiDimDirichlet_t(
	const Alpha_t&		Alpha)
 :
  yggdrasil_multiDimDirichlet_t(Alpha, HyperCube_t::MAKE_UNIT_CUBE(Alpha.size() - 1))
{
};



Real_t
yggdrasil_multiDimDirichlet_t::wh_pdf(
	const Sample_t&		x)
 const
{
	yggdrasil_assert(m_gammaDist.size() >= 2);
	yggdrasil_assert(x.isValid());
	yggdrasil_assert(m_domain.isValid());

	//Get the number of parameters and the number of dimensions we have
	const Size_t N = m_gammaDist.size();	//This i steh number of parameters (K+1)
	const Size_t K = N - 1;			//This is the number of dimensions we have

	//The dimension of the sample
	const Size_t xDim = x.size();

	if(xDim != K)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of *this and the passed samples do not agree.");
	};
	yggdrasil_assert(xDim == m_domain.size());

	//Test if we are outside the domain we are support.
	//for technical reaon we will return 0 in that case
	if(m_domain.isInside(x) == false)
	{
		return Real_t(0.0);
	}; //End if: outside domain

	//This is a helper variables
	Real_t prodOfExponents = 1.0;

	/*
	 * We have striped the last sample, this is wy we have K+1 parameters
	 * but just K dimensional samples. However the last component, K+1,
	 * is implicitkly given by the conditon that $\sum_{i = 1}^{K} x_i + x_{K+1} == 1$
	 * must hold.
	 *
	 * After the loop below, this variable will be
	 * 	1 - \sum_{i = 1}^{K}
	 */
	Real_t implicitLastComponent = 1.0;	//Will be reduced

	//Going through the elements
	for(Size_t i = 0; i != xDim; ++i)
	{
		//Load the component
		const Real_t x_i = x[i];
		yggdrasil_assert(isValidFloat(x_i));

		//If we encounter a negative value, we know that the distribution will be zero.
		if(x_i <= 0.0)
		{
			return Real_t(0.0);
			//throw YGGDRASIL_EXCEPT_InvArg("The " + std::to_string(i) + " component is invalid.");
		};

		//Update the last implicit component
		implicitLastComponent -= x_i;

		//Check if we are below or equal zero
		//if this happens we know that we are
		//outside the support
		//Notice: there can be some numerical anaomalies.
		if(implicitLastComponent <= 0.0)
		{
			return Real_t(0.0);
		};

		//Compute the other part of the PDF
		prodOfExponents *= ::std::pow(x_i, Real_t(m_gammaDist.at(i).alpha()) - Real_t(1.0));
		yggdrasil_assert(isValidFloat(prodOfExponents));
	}; //End for(i): computing the value
	yggdrasil_assert(implicitLastComponent > 0.0);

	//Now we must incooperate the the implicit compüonent
	prodOfExponents *= ::std::pow(implicitLastComponent, Real_t(m_gammaDist.back().alpha()) - Real_t(1.0));
	yggdrasil_assert(isValidFloat(prodOfExponents));



	//To get the final result, we have to include the niormalization factor
	const Real_t PDF = prodOfExponents * m_invBeta;

	if(isValidFloat(PDF) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Error, the pdf is invalid.");
	};


	//Return the pdf
	return PDF;
}; //End: generate PDF


void
yggdrasil_multiDimDirichlet_t::wh_generateSample(
	pRNGenerator_t&		generator,
	Sample_t*		s)
{
	/*
	 * We will use the method that is described by
	 * wikipedi for generating the samples.
	 * but we will simply ignore the last one.
	 */
	yggdrasil_assert(m_gammaDist.size() >= 2);
	yggdrasil_assert(m_domain.isValid());

	//Get the number of parameters and the number of dimensions we have
	const Size_t N = m_gammaDist.size();	//This i steh number of parameters (K+1)
	const Size_t K = N - 1;			//This is the number of dimensions we have

	//The dimension of the sample
	const Size_t xDim = s->size();

	//Thest if we have to addapt the dimension of the sample
	//Notice we have K+1 ~ N different parameters, but we only
	//have K dimension.
	if(K != xDim)
	{
		//Addapt the sample
		*s = Sample_t(K);
	}; //End if: have to change the sample
	yggdrasil_assert(s->size() == K);

	//This is the pointer to the data space of the sample
	yggdrasil_assert(s->size() == K);
	Sample_t::value_type* const sp = s->data();
	yggdrasil_assert(sp != nullptr);

	//This bool indicate if a zero was enchountered (special case handling)
	//Must be outside the loop, because otherwhise it is not in the scope.
	//according to ycm
	bool foundZeroComponent = false;

	//This is the pdf of the samples we are generating.
	//There where some probles, that we accidentely generated
	//samples that have zero probability, this was an atefact
	//of the finite numeric.
	//So we monitore them.
	Real_t thisPDF = -1.0;

	//This is the number of tries we have done to generate one samples
	Size_t tries = 0;

	//This is the bound, we use as a threashold.
	//if more tries are needed an exception is generated.
	const Size_t bound = 10 * std::pow(4, N);

	//A loop since we could need more tries to generate a valid sample.
	do
	{
		//Increase the ry count
		tries += 1;

		if(tries >= bound)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Tried " + std::to_string(bound) + " times to generate a sample, buit with no success.");
		}; //ENd tries

		//This is the sum of the component. it is needed for the normalization.
		//It is intialized with the last sample, tat we will discard anyway.
		Real_t sumOfVectors = m_gammaDist.back()(generator);

		//Reset the value of the bool for the zero case
		//An edge case that seams to happen, probably also
		//numerical arefact
		foundZeroComponent = (sumOfVectors == 0.0) ? true : false;

		//Test if the firs attempt already failed
		if(foundZeroComponent == true)
		{
			//Try again
			continue;
		}; //ENd if: maybe already a restart

		//Generate the individual comonents
		//We only generate the first K ones, the last was
		//already generated.
		for(Size_t i = 0; i != K; ++i)
		{
			//Generate the component i
			const Real_t y_i = m_gammaDist[i](generator);

			//Test if we have found an 0, this is because the other test
			//did not recognize this case
			if(y_i == Real_t(0.0))
			{
				foundZeroComponent = true;

				//Exoit the loop and try again
				break;
			}; //End if: handling zero case
			yggdrasil_assert(y_i > Real_t(0.0));

			//Ass the current sample to the normalizing constant
			sumOfVectors += y_i;

			//Save the generated, yet unnormalized component
			//in the sample
#ifdef NDEBUG
			sp[i] = y_i;
#else
			s->at(i) = y_i;
#endif
		}; //ENd for(i): generating the unnormalized components

		//Normalize (only if not a zero was found; Crude error handling)
		if(foundZeroComponent == false)
		{
			const Real_t normalConst = Real_t(1.0) / sumOfVectors;
			for(Size_t i = 0; i != K; ++i)
			{
#ifdef NDEBUG
				sp[i] *= normalConst;
#else
				s->at(i) *= normalConst;
#endif
			}; //End for(i):

			//Evaluate the PDF for preventing some edge cases
			thisPDF = this->wh_pdf(*s);
		}
		else
		{
			thisPDF = -1.0;
		};
	}
	while(
		   (foundZeroComponent == true)		//Test if a component was zero
		|| (m_domain.isInside(*s) == false)	//Test if the sample lies inside the domain we restrict the samples to
		|| (thisPDF <= Real_t(0.0))		//Test if thesample is possible in numerical terms
	); //End do: repet until valid sample was found


	return;
}; //End generate a single sample


std::ostream&
yggdrasil_multiDimDirichlet_t::dumpToStream(
	std::ostream&	s)
{
	for(Size_t i = 0; i != m_gammaDist.size(); ++i)
	{
		s << m_gammaDist.at(i);
	};

	return s;
}; //ENd dump to


std::istream&
yggdrasil_multiDimDirichlet_t::loadFromStream(
	std::istream&	s)
{
	for(Size_t i = 0; i != m_gammaDist.size(); ++i)
	{
		s >> m_gammaDist.at(i);
	};

	return s;
};




YGGDRASIL_NS_END(yggdrasil)


