/**
 * \brief	This file contains the main function that
 * 		 deals with the generation of random numbers.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>

#include <random/yggdrasil_random.hpp>

//Including std
#include <random>
#include <fstream>

//Include Boost
#include <boost/filesystem.hpp>


YGGDRASIL_NS_START(yggdrasil)


//Using the Path
using Path_t = ::boost::filesystem::path;

namespace fs = ::boost::filesystem;


void
yggdrasil_pRNG_dump(
	const std::string&		dumpFile,
	const yggdrasil_pRNG_t&		geni)
{
	if(dumpFile.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dumpfile is empyt.");
	};

	std::ofstream out;
	out.open(dumpFile.c_str(), std::ofstream::out | std::ofstream::trunc);
	if(out.is_open() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("COuld not open the file " + dumpFile + " to save the generator state to.");
	};

	out << geni;

	if(out.fail() == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The writting occured in failing.");
	};

	out.close();

	return;
}; //End dumping function


void
yggdrasil_pRNG_load(
	const std::string&	dumpFile,
	yggdrasil_pRNG_t*	geni)
{
	if(dumpFile.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The filename for the dumping was empty.");
	};
	if(fs::exists(Path_t(dumpFile)) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The given file " + dumpFile + " does not exists.");
	};
	if(geni == nullptr)
	{
		throw YGGDRASIL_EXCEPT_NULL("The generator is the nullptr.");
	};

	//Create a file
	std::ifstream in;
	in.open(dumpFile.c_str(), std::ofstream::in);
	if(in.is_open() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Could not open the file " + dumpFile);
	};

	in >> (*geni);

	if(in.fail() == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("An error while writeing to the file " + dumpFile + " has occured.");
	};

	//close the file
	in.close();

	return;
}; //End loading




YGGDRASIL_NS_END(yggdrasil)


