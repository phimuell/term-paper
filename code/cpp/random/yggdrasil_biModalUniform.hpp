#pragma once
/**
 * \brief	This file contains the code for a bimodal uniform distribution.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>


//Including std


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)


/**
 * \brief	This class allows multidimensional Gaussian samples.
 * \class	yggdrasil_biModalUniform_t
 *
 * This class is a bimodal uniform distribution.
 * the PDF is given as
 * 	p ~ \alpha Uni(a_1, b_1) + (1.0 - \alpha) Uni(a_2, b_2)
 *
 * Also an supperposition of two uniform fucntions.
 * This is like the outlier function, that is allready knwon.
 */
class yggdrasil_biModalUniform_t final : public yggdrasil_randomDistribution_i
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using UniformDist_t	= ::std::uniform_real_distribution<Numeric_t>;
	using Super_t 		= yggdrasil_randomDistribution_i;	//!< This is the interface.



	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This function takes two intervals, that marks the support of
	 * the two moddes of the uniform distribution.
	 * And the coubling factor.
	 *
	 * It also takes a domain, which must be 1D and contain the two
	 * intervals completly.
	 * If the domain is not given then it is estimated.
	 * Interval ne must ly before the second interval.
	 * And no overlap is allowed.
	 *
	 * \param  mode1	The domain of the first interval.
	 * \param  mode2	The domain of the second mode.
	 * \param  alpha	The coubling constant.
	 * \param  domain	Where the twos are supported.
	 */
	yggdrasil_biModalUniform_t(
		const IntervalBound_t&		mode1,
		const IntervalBound_t&		mode2,
		const Numeric_t 		alpha,
		const HyperCube_t&		domain);


	/**
	 * \brief	Building contructor.
	 *
	 * Same as above but estimates the domain based on the
	 * input intervals.
	 *
	 */
	yggdrasil_biModalUniform_t(
		const IntervalBound_t&		mode1,
		const IntervalBound_t&		mode2,
		const Numeric_t 		alpha);


	/**
	 * \brief	This is the destructor.
	 *
	 * It is defaulted.
	 */
	~yggdrasil_biModalUniform_t();


	/**
	 * \brief	This is a colne function.
	 *
	 * This function returns a copy of *this.
	 * It is like the java fucntion.
	 * It allows calling a copy cvonstructor without the need
	 * of knnowing the exact type.
	 */
	virtual
	Distribution_ptr
	clone()
	 const
	 final;


	/**
	 * \brief	This is teh copy constrructor
	 *
	 * It is defaulted.
	 */
	yggdrasil_biModalUniform_t(
		const yggdrasil_biModalUniform_t&);


	/**
	 * \brief	The copy assignment operator.
	 *
	 * Is defaulted.
	 */
	yggdrasil_biModalUniform_t&
	operator= (
		const yggdrasil_biModalUniform_t&);


	/**
	 * \brief	The move constructor
	 *
	 * Is defaulted.
	 */
	yggdrasil_biModalUniform_t(
		yggdrasil_biModalUniform_t&&);


	/**
	 * \brief	The move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_biModalUniform_t&
	operator= (
		yggdrasil_biModalUniform_t&&);


	/**
	 * \brief	The default constructor.
	 *
	 * Is deleted.
	 */
	yggdrasil_biModalUniform_t()
	 = delete;


	/*
	 * =====================
	 * Random number functions
	 */
public:
	/*
	 * No functions here, since all of the defaults
	 * are used.
	 *
	 * If we would list them we would need to
	 * explicitly code that the default are used.
	 */


	/*
	 * ======================
	 * Statistical parameters
	 */
public:
	/**
	 * \brief	This function gives the mean of *this.
	 *
	 * This is the mean that the distribution has.
	 */
	virtual
	MeanVec_t
	getMean()
	 const
	 final;


	/**
	 * \brief	This fucntion returns the covariance matrix of *this.
	 *
	 */
	virtual
	CoVarMat_t
	getCoVar()
	 const
	 final;


	/**
	 * \brief	This function returns <alpha the coupling parameter of the two modes.
	 *
	 * This value is the coupling parameter between the two.
	 * OIt was passed as alpha to the constructor.
	 */
	virtual
	Numeric_t
	getAlpha()
	 const
	 noexcept
	 final;


	/**
	 * \brief	This function returns the inerval of the first mode.
	 *
	 * This is the support domain of the first mode.
	 * Note that this function returns an interval and no hypercube.
	 */
	virtual
	IntervalBound_t
	getSuppDom1()
	 const
	 final;


	/**
	 * \brief	This function returns the support domain of the second mode.
	 *
	 * This functin returns an interval rather than a hyper cube.
	 */
	virtual
	IntervalBound_t
	getSuppDom2()
	 const
	 final;



	/*
	 * ====================
	 * Domain information
	 */
public:
	/**
	 * \brief	THis function reetruns the dimension the generated samples has.
	 *
	 * Since this fucntion is purly one d it always return one.
	 */
	virtual
	Size_t
	nDims()
	 const
	 final;


	/**
	 * \brief	Returns true if this is undounded.
	 *
	 * Since the domain is estimated it allways return false;
	 */
	virtual
	bool
	isUnbounded()
	 const
	 final;


	/**
	 * \brief	This function returns the domain, in which the
	 * 		 sampleing is restricted.
	 *
	 * This function returns an invalid domain if the sampling is unbounded.
	 */
	virtual
	HyperCube_t
	getSampleDomain()
	 const
	 final;


	/*
	 * ===================
	 * Workhorse interface
	 */
protected:

	/**
	 * \brief	This function generates one sample that is
	 * 		 distributed as the implementation.
	 *
	 * See in teh description above for how it works.
	 *
	 * \param  generator		This is the random generator
	 * \param  sample		This is the sample that should be generated.
	 */
	virtual
	void
	wh_generateSample(
		pRNGenerator_t&		generator,
		Sample_t*		sample)
	 final;


	/**
	 * \brief	This function return the pdf of the sample.
	 *
	 * This is the normal version of the exponential
	 * we are used.
	 *
	 * \param  sample	The pdf to compute
	 */
	virtual
	Real_t
	wh_pdf(
		const Sample_t&		sample)
	 const
	 final;

	/*
	 * ========================
	 * Dumping ffunction
	 *
	 * As stated in the interface, this only
	 * saves the state of the random distrinbution
	 * that are defined from the standard.
	 * Other parameter are not saved/loaded.
	 */
public:
	/**
	 * \brief	Write to a stream
	 */
	virtual
	std::ostream&
	dumpToStream(
		std::ostream&	dumpStream)
	 final;

	/**
	 * \brief	Load from a stream
	 */
	virtual
	std::istream&
	loadFromStream(
		std::istream&	dumpStream)
	 final;


	/*
	 * =======================
	 * Private Memeber
	 */
private:
	Numeric_t 		m_alpha;	//!< The coupling
	UniformDist_t 		m_select;	//!< The distribution for selecting the mode
	UniformDist_t 		m_mode1;	//!< The distribution of mode 1.
	UniformDist_t 		m_mode2;	//!< The distribution of mode 2.
	HyperCube_t 		m_domain;	//!< The domain where we restrinc ourself. If unvalid unrestrcted.
}; //End: class(yggdrasil_biModalUniform_t)




YGGDRASIL_NS_END(yggdrasil)


