#pragma once
/**
 * \brief	This function implements an outlier PDF.
 * 		 This is section 3.1.2 in Daniel's paper.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>


//Including std


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)


/**
 * \brief	This class allows outlier pdf.
 * \class	yggdrasil_outlierPDF_t
 *
 * An outlier PDF is described by the following pdf:
 * 	p_{\alpha, \mu, \sigma_1, \sigma_2}(x) :=
 * 		\alpha \cdot \mathcal{N}(\mu, \sigma_1^2) + (1 - \alpha) \cdot \mathcal{N}(\mu, \sigma_2^2)
 *
 * This means it is the supporposition of two gaussians.
 * It is important \mathcal{N}(\mu, \sigma^2) is our notation for the gaussinan,
 * this is different than in the paper and the C++ standard, for god knows why.
 *
 * We also say requere that \sigma_2 < \sigma_1 and \alpha \in ]0, 1[.
 *
 * This is only a one dimensional distribution.
 *
 * \note	This is a one dimensional distribution.
 *
 */
class yggdrasil_outlierPDF_t final : public yggdrasil_randomDistribution_i
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using OneDimGauss_t 	= ::std::normal_distribution<Numeric_t>;	//!< This is the type for a one dimensional normal distribution
	using UniformDistri_t	= ::std::uniform_real_distribution<Numeric_t>;	//!< This is the uniform distribution, for selecting teh gaussians.

	using Super_t 		= yggdrasil_randomDistribution_i;	//!< This is the interface.

	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This function takes the parameter and a restriction domain
	 * and constructs an ozutlier pdf.
	 *
	 * \param  alpha	The contribution of the first gaussion.
	 * 			 i.e. how likely it is to observe gaussian one.
	 * \param  mean		The mean of the two gaussian.
	 * \param  std_1	The standard diviation of the first gaussian.
	 * \param  std_2	The standard deviation of the second gaussian.
	 * \param  domain	Where we allow to sample.
	 *
	 * \note	This fucntion takes the standard deviation as arguments.
	 */
	yggdrasil_outlierPDF_t(
		const Numeric_t 	alpha,
		const Numeric_t 	mean,
		const Numeric_t 	std_1,
		const Numeric_t 	std_2,
		const HyperCube_t&	domain);

	/**
	 * \brief	This version of teh constructor uses an invalid domain.
	 *
	 * \param  alpha	The contribution of the first gaussion.
	 * 			 i.e. how likely it is to observe gaussian one.
	 * \param  mean		The mean of the two gaussian.
	 * \param  std_1	The standard diviation of the first gaussian.
	 * \param  std_2	The standard deviation of the second gaussian.
	 *
	 * \note	This fucntion takes the standard deviation as arguments.
	 */
	yggdrasil_outlierPDF_t(
		const Numeric_t 	alpha,
		const Numeric_t 	mean,
		const Numeric_t 	std_1,
		const Numeric_t 	std_2);



	/**
	 * \brief	This is the destructor.
	 *
	 * It is defaulted.
	 */
	~yggdrasil_outlierPDF_t();


	/**
	 * \brief	This is a colne function.
	 *
	 * This function returns a copy of *this.
	 * It is like the java fucntion.
	 * It allows calling a copy cvonstructor without the need
	 * of knnowing the exact type.
	 */
	virtual
	Distribution_ptr
	clone()
	 const
	 final;


	/**
	 * \brief	This is teh copy constrructor
	 *
	 * It is defaulted.
	 */
	yggdrasil_outlierPDF_t(
		const yggdrasil_outlierPDF_t&);


	/**
	 * \brief	The copy assignment operator.
	 *
	 * Is defaulted.
	 */
	yggdrasil_outlierPDF_t&
	operator= (
		const yggdrasil_outlierPDF_t&);


	/**
	 * \brief	The move constructor
	 *
	 * Is defaulted.
	 */
	yggdrasil_outlierPDF_t(
		yggdrasil_outlierPDF_t&&);


	/**
	 * \brief	The move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_outlierPDF_t&
	operator= (
		yggdrasil_outlierPDF_t&&);


	/**
	 * \brief	The default constructor.
	 *
	 * Is deleted.
	 */
	yggdrasil_outlierPDF_t()
	 = delete;


	/*
	 * =====================
	 * Random number functions
	 */
public:
	/*
	 * No functions here, since all of the defaults
	 * are used.
	 *
	 * If we would list them we would need to
	 * explicitly code that the default are used.
	 */


	/*
	 * ======================
	 * Statistical parameters
	 */
public:
	/**
	 * \brief	This function gives the mean of *this.
	 *
	 * This is the mean that the distribution has.
	 *
	 */
	virtual
	MeanVec_t
	getMean()
	 const
	 final;


	/**
	 * \brief	This fucntion returns the covariance matrix of *this.
	 *
	 * In the one dimensional case this means the VARIANCE.
	 * Rememeber we used the std to construct *thsi.
	 *
	 * \note	This is not directly the matrix that was used for construction.
	 * 		 *this only stores the choleskyfactor. THe covariance matrix
	 * 		 is reconstructed from this.
	 */
	virtual
	CoVarMat_t
	getCoVar()
	 const
	 final;


	/**
	 * \brief	This function returns the standard deviation of the first mode.
	 *
	 * Note that unlike the getCoVar() function, which returns a matrix, due to the
	 * interface contract, this function returns a Numneric_t value.
	 */
	virtual
	Numeric_t
	getStdDev1()
	 const
	 noexcept
	 final;


	/**
	 * \brief	This function returns the standard deviation of the second mode.
	 *
	 * Note that unlike the getCoVar() function, which returns a matrix, due to the
	 * interface contract, this function returns a Numneric_t value.
	 */
	virtual
	Numeric_t
	getStdDev2()
	 const
	 noexcept
	 final;


	/**
	 * \brief	This fucntion returns the alpha value of the coupling.
	 *
	 * The value returned by this function is equivalent to the alpha value,
	 * that was appesed to *this as alpha on the constructor.
	 * It describes the strength of the first mode.
	 */
	virtual
	Numeric_t
	getAlpha()
	 const
	 noexcept
	 final;



	/*
	 * ==================
	 * Domain functions
	 */
public:
	/**
	 * \brief	THis function reetruns the dimension the generated samples has.
	 *
	 * This is trivially always one.
	 */
	virtual
	Size_t
	nDims()
	 const
	 final;


	/**
	 * \brief	Returns true if this is undounded.
	 */
	virtual
	bool
	isUnbounded()
	 const
	 final;


	/**
	 * \brief	This function returns the domain, in which the
	 * 		 sampleing is restricted.
	 *
	 * This function returns an invalid domain if the sampling is unbounded.
	 *
	 * Note that for compability reasons thsi fuction returns a hypercube.
	 */
	virtual
	HyperCube_t
	getSampleDomain()
	 const
	 final;


	/*
	 * ===================
	 * Workhorse interface
	 */
protected:

	/**
	 * \brief	This function generates one sample that is
	 * 		 distributed as the implementation.
	 *
	 * See in teh description above for how it works.
	 *
	 * \param  generator		This is the random generator
	 * \param  sample		This is the sample that should be generated.
	 */
	virtual
	void
	wh_generateSample(
		pRNGenerator_t&		generator,
		Sample_t*		sample)
	 final;


	/**
	 * \brief	This function return the pdf of the sample.
	 *
	 * This is the normal version of the exponential
	 * we are used.
	 *
	 * \param  sample	The pdf to compute
	 */
	virtual
	Real_t
	wh_pdf(
		const Sample_t&		sample)
	 const
	 final;

	/*
	 * ========================
	 * Dumping ffunction
	 *
	 * As stated in the interface, this only
	 * saves the state of the random distrinbution
	 * that are defined from the standard.
	 * Other parameter are not saved/loaded.
	 */
public:
	/**
	 * \brief	Write to a stream
	 */
	virtual
	std::ostream&
	dumpToStream(
		std::ostream&	dumpStream)
	 final;

	/**
	 * \brief	Load from a stream
	 */
	virtual
	std::istream&
	loadFromStream(
		std::istream&	dumpStream)
	 final;


	/*
	 * =======================
	 * Private Memeber
	 */
private:
	Numeric_t 	m_alpha;	//!< Alpha parametzer that we use.
	Numeric_t 	m_mean;		//!< The mean that is used.
	UniformDistri_t m_uniform;	//!< This is the distribution for selecting the first gausssan.
	OneDimGauss_t 	m_gauss1;	//!< This is the first gauss distribution object.
	OneDimGauss_t 	m_gauss2;	//!< This is the secod gauss distribution object.
	IntervalBound_t	m_domain;	//!< We only store an internal
}; //End: class(yggdrasil_outlierPDF_t)




YGGDRASIL_NS_END(yggdrasil)


