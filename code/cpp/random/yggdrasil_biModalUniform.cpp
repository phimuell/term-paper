/**
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_biModalUniform.hpp>


//Including std


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)


yggdrasil_biModalUniform_t::yggdrasil_biModalUniform_t(
	const yggdrasil_biModalUniform_t&)
 = default;

yggdrasil_biModalUniform_t::~yggdrasil_biModalUniform_t()
 = default;

yggdrasil_biModalUniform_t::Distribution_ptr
yggdrasil_biModalUniform_t::clone()
 const
{
	return Distribution_ptr(new yggdrasil_biModalUniform_t(*this));
};

yggdrasil_biModalUniform_t::yggdrasil_biModalUniform_t(
	yggdrasil_biModalUniform_t&&)
 = default;


yggdrasil_biModalUniform_t&
yggdrasil_biModalUniform_t::operator= (
	yggdrasil_biModalUniform_t&&)
 = default;

yggdrasil_biModalUniform_t&
yggdrasil_biModalUniform_t::operator= (
	const yggdrasil_biModalUniform_t&)
 = default;


yggdrasil_biModalUniform_t::MeanVec_t
yggdrasil_biModalUniform_t::getMean()
 const
{
	//Get the middle point of the two
	const Numeric_t middle1 = (m_mode1.b() + m_mode1.a()) * 0.5;
	const Numeric_t middle2 = (m_mode2.b() + m_mode2.a()) * 0.5;

	const Numeric_t wMiddle = m_alpha * middle1 + (1.0 - m_alpha) * middle2;

	MeanVec_t wMeanV(1);
	wMeanV[0] = wMiddle;
	return wMeanV;
}; //End: getMean


yggdrasil_biModalUniform_t::CoVarMat_t
yggdrasil_biModalUniform_t::getCoVar()
 const
{
	/*
	 * See:
	 * 	https://en.wikipedia.org/wiki/Multimodal_distribution#Moments_of_mixtures
	 */
	const Numeric_t var1 = ::std::pow(m_mode1.b() - m_mode1.a(), 2) / 12.0;
	const Numeric_t var2 = ::std::pow(m_mode2.b() - m_mode2.a(), 2) / 12.0;

	const Numeric_t middle1 = (m_mode1.b() + m_mode1.a()) * 0.5;
	const Numeric_t middle2 = (m_mode2.b() + m_mode2.a()) * 0.5;
	const Numeric_t wMiddle = m_alpha * middle1 + (1.0 - m_alpha) * middle2;

	//calcualting the deltas
	const Numeric_t delta_1 = middle1 - wMiddle;
	const Numeric_t delta_2 = middle2 - wMiddle;

	const Numeric_t wVar = m_alpha * (var1 + delta_1 * delta_1) + (1.0 - m_alpha) * (var2 + delta_2 * delta_2);

	CoVarMat_t wVarM(1, 1);
	wVarM(0, 0) = wVar;

	return wVarM;
}; //End: get covar



Numeric_t
yggdrasil_biModalUniform_t::getAlpha()
 const
 noexcept
{
	return m_alpha;
};

yggdrasil_biModalUniform_t::IntervalBound_t
yggdrasil_biModalUniform_t::getSuppDom1()
 const
{
	return IntervalBound_t(m_mode1.a(), m_mode1.b());
};


yggdrasil_biModalUniform_t::IntervalBound_t
yggdrasil_biModalUniform_t::getSuppDom2()
 const
{
	return IntervalBound_t(m_mode2.a(), m_mode2.b());
};

Size_t
yggdrasil_biModalUniform_t::nDims()
 const
{
	yggdrasil_assert(m_domain.isValid());
	yggdrasil_assert(m_domain.nDims() == 1);
	return 1;
}; //End getDim


bool
yggdrasil_biModalUniform_t::isUnbounded()
 const
{
	yggdrasil_assert(m_domain.isValid());
	return false;
};

yggdrasil_biModalUniform_t::HyperCube_t
yggdrasil_biModalUniform_t::getSampleDomain()
 const
{
	return m_domain;
};


yggdrasil_biModalUniform_t::yggdrasil_biModalUniform_t(
	const IntervalBound_t&		mode1,
	const IntervalBound_t&		mode2,
	const Numeric_t 		alpha)
 :
  yggdrasil_biModalUniform_t(mode1, mode2, alpha, HyperCube_t::MAKE_INVALID_CUBE())
{};


yggdrasil_biModalUniform_t::yggdrasil_biModalUniform_t(
	const IntervalBound_t&		mode1,
	const IntervalBound_t&		mode2,
	const Numeric_t 		alpha,
	const HyperCube_t&		domain)
 :
  m_alpha(alpha),
  m_select(0.0, 1.0),
  m_mode1(mode1.lower(), mode1.upper()),
  m_mode2(mode2.lower(), mode2.upper()),
  m_domain(domain)
{
	if(mode1.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Mode one is not valid.");
	};
	if(mode2.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Mode two is not valid.");
	};
	if(!((0.0 < alpha) && (alpha < 1.0)))
	{
		throw YGGDRASIL_EXCEPT_InvArg("Compability conditions on alpha are vialated.");
	};
	if(mode1.isBeforeOther(mode2) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The mode1 is not before the second mode.");
	};
	if(m_domain.isValid() == true)
	{
		if(m_domain.nDims() != 1)
		{
			throw YGGDRASIL_EXCEPT_InvArg("THe domain is wrong.");
		};
		if(mode1.isContainedIn(m_domain.at(0)) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The first mode is not inside the domain.");
		};
		if(mode2.isContainedIn(m_domain.at(0)) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The second mode is not inside the domain.");
		};
	};
}; //End: building constructor



Real_t
yggdrasil_biModalUniform_t::wh_pdf(
	const Sample_t&		x)
 const
{
	if(x.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The sample is not valid.");
	};
	if(x.nDims() != 1)
	{
		throw YGGDRASIL_EXCEPT_InvArg("the samples does not have dim 1");
	};

	if(m_domain.isInside(x) == false)
	{
		return Real_t(0.0);
	};

	const Numeric_t x_0 = x[0];
	const IntervalBound_t m1(m_mode1.a(), m_mode1.b());

	if(m1.isInside(x_0) == true)
	{
		return Real_t(m_alpha / m1.getLength());
	};

	const IntervalBound_t m2(m_mode2.a(), m_mode2.b());
	if(m2.isInside(x_0) == true)
	{
		return Real_t((1.0 - m_alpha) / m2.getLength());
	};

	return Real_t(0.0);
}; //End: generate PDF


void
yggdrasil_biModalUniform_t::wh_generateSample(
	pRNGenerator_t&		generator,
	Sample_t*		s)
{
	const Size_t nDim = 1;

	//Test if the dimension of the argument is correct
	//and change it to the correct value.
	if(s->nDims() != nDim)
	{
		//Not the same numbers of dmensions
		//we have to update it
		*s = Sample_t(nDim);
	}; //End: addapt sample if needed.

	if(m_select(generator) < m_alpha)
	{
		s->at(0) = m_mode1(generator);
	}
	else
	{
		s->at(0) = m_mode2(generator);
	};

	return;
}; //End generate a single sample


std::ostream&
yggdrasil_biModalUniform_t::dumpToStream(
	std::ostream&	s)
{
	s << m_select << m_mode1 << m_mode2;

	return s;
}; //ENd dump to


std::istream&
yggdrasil_biModalUniform_t::loadFromStream(
	std::istream&	s)
{
	s >> m_select >> m_mode1 >> m_mode2;

	return s;
};





YGGDRASIL_NS_END(yggdrasil)


