#we will add the cpp files to the source and the rest as bublic
target_sources(
	Yggdrasil
  PRIVATE
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_random.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_multiGauss.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_interface_distribution.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_multiDirichle.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_stdDistributions.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_outlierPDF.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_biModalUniform.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_gammaPDF.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_betaPDF.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_multiUniform.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_multiModalDistribution.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/pyYggdrasil_randomNumberGenerator.cpp"
  PUBLIC
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_random.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_multiGauss.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_interface_distribution.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_multiDirichle.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_outlierPDF.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_biModalUniform.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_gammaPDF.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_betaPDF.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_multiUniform.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pyYggdrasil_randomNumberGenerator.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_multiModalDistribution.hpp"
  )


