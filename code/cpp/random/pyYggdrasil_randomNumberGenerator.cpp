/**
 * \brief	This file contains a wrapper for the random number generator.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>
#include <random/pyYggdrasil_randomNumberGenerator.hpp>

//Including std
#include <random>

YGGDRASIL_NS_START(yggdrasil)

pyYggdrasil_pRNGenerator_t::pyYggdrasil_pRNGenerator_t(
	const result_type	seed)
 :
  m_geni(seed)
{};

pyYggdrasil_pRNGenerator_t::pyYggdrasil_pRNGenerator_t(
	const result_type 	seed,
	const Size_t 		N)
 :
  m_geni(seed)
{
	m_geni.discard(N);
};


pyYggdrasil_pRNGenerator_t::pyYggdrasil_pRNGenerator_t()
 = default;

pyYggdrasil_pRNGenerator_t::~pyYggdrasil_pRNGenerator_t()
 = default;


pyYggdrasil_pRNGenerator_t::pyYggdrasil_pRNGenerator_t(
	const pyYggdrasil_pRNGenerator_t&)
 = default;


pyYggdrasil_pRNGenerator_t::pyYggdrasil_pRNGenerator_t(
	pyYggdrasil_pRNGenerator_t&&)
 = default;


pyYggdrasil_pRNGenerator_t&
pyYggdrasil_pRNGenerator_t::operator= (
	const pyYggdrasil_pRNGenerator_t&)
 = default;


pyYggdrasil_pRNGenerator_t&
pyYggdrasil_pRNGenerator_t::operator= (
	pyYggdrasil_pRNGenerator_t&&)
 = default;


pyYggdrasil_pRNGenerator_t::pyYggdrasil_pRNGenerator_t(
	const BaseGenerator_t& 		geni)
 :
  m_geni(geni)
{};


pyYggdrasil_pRNGenerator_t::pyYggdrasil_pRNGenerator_t(
	BaseGenerator_t&&		geni)
 :
  m_geni(std::move(geni))
{};


pyYggdrasil_pRNGenerator_t&
pyYggdrasil_pRNGenerator_t::operator= (
	const BaseGenerator_t&		geni)
{
	m_geni = geni;
	return *this;
};


pyYggdrasil_pRNGenerator_t&
pyYggdrasil_pRNGenerator_t::operator= (
	BaseGenerator_t&&		geni)
{
	m_geni = std::move(geni);

	return *this;
};


pyYggdrasil_pRNGenerator_t::operator
pyYggdrasil_pRNGenerator_t::BaseGenerator_t&()
{
	return m_geni;
};


pyYggdrasil_pRNGenerator_t::operator
const pyYggdrasil_pRNGenerator_t::BaseGenerator_t&()
{
	return m_geni;
};

pyYggdrasil_pRNGenerator_t::BaseGenerator_t&
pyYggdrasil_pRNGenerator_t::get()
{
	return m_geni;
};


void
pyYggdrasil_pRNGenerator_t::seed(
	const result_type 	newSeed)
{
	m_geni.seed(newSeed);
	return;
};//End: seed


void
pyYggdrasil_pRNGenerator_t::discard(
		const Size_t 	N)
{
	m_geni.discard(N);
	return;
}; //ENd: discard


pyYggdrasil_pRNGenerator_t::result_type
pyYggdrasil_pRNGenerator_t::generate()
{
	return m_geni();
}; //End: generate



YGGDRASIL_NS_END(yggdrasil)


