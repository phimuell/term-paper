#pragma once
/**
 * \brief	This fucntion implements multidimensional uniform distribution.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>


//Including std


//Including Eigen
#include <Eigen/Dense>


YGGDRASIL_NS_START(yggdrasil)


/**
 * \brief	This class allows multidimensional Uniform distribution.
 * \class	yggdrasil_multiDimUniform_t
 *
 * This is for uniform independent distributions
 */
class yggdrasil_multiDimUniform_t final : public yggdrasil_randomDistribution_i
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using UniformDist_t 	= ::std::uniform_real_distribution<Numeric_t>;
	using UniformDistVec_t 	= ::std::vector<UniformDist_t>;

	using Super_t 		= yggdrasil_randomDistribution_i;	//!< This is the interface.


	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * The building contructor, takes two cubes, one is the smapling domain
	 * and one is the support domeian.
	 * The support domain must be weakly contained inside
	 * the sampling domain.
	 * However it can be invalid.
	 *
	 * \param  supportDom		This is the support domain.
	 * \param  samplingDom		This is the sampling domain.
	 *
	 * \note	This constructor is provided for consistency with the other
	 * 		 Distributions. It should not be used. Instead use the version
	 * 		 That only takes only the support domain.
	 */
	yggdrasil_multiDimUniform_t(
		const HyperCube_t&	supportDom,
		const HyperCube_t&	samplingDom);


	/**
	 * \brief	Building contructor.
	 *
	 * The sampling domain and the support domain are the same.
	 *
	 * \param  supportDom		This is the support domain.
	 */
	yggdrasil_multiDimUniform_t(
		const HyperCube_t&	supportDom);


	/**
	 * \brief	This is the destructor.
	 *
	 * It is defaulted.
	 */
	~yggdrasil_multiDimUniform_t();


	/**
	 * \brief	This is a colne function.
	 *
	 * This function returns a copy of *this.
	 * It is like the java fucntion.
	 */
	virtual
	Distribution_ptr
	clone()
	 const
	 final;


	/**
	 * \brief	This is teh copy constrructor
	 *
	 * It is defaulted.
	 */
	yggdrasil_multiDimUniform_t(
		const yggdrasil_multiDimUniform_t&);


	/**
	 * \brief	The copy assignment operator.
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiDimUniform_t&
	operator= (
		const yggdrasil_multiDimUniform_t&);


	/**
	 * \brief	The move constructor
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiDimUniform_t(
		yggdrasil_multiDimUniform_t&&);


	/**
	 * \brief	The move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiDimUniform_t&
	operator= (
		yggdrasil_multiDimUniform_t&&);


	/**
	 * \brief	The default constructor.
	 *
	 * Is deleted.
	 */
	yggdrasil_multiDimUniform_t()
	 = delete;


	/*
	 * =====================
	 * Random number functions
	 */
public:
	/*
	 * No functions here, since all of the defaults
	 * are used.
	 *
	 * If we would list them we would need to
	 * explicitly code that the default are used.
	 */


	/*
	 * ======================
	 * Statistical parameters
	 */
public:
	/**
	 * \brief	This function gives the mean of *this.
	 *
	 * This is the mean that the distribution has.
	 */
	virtual
	MeanVec_t
	getMean()
	 const
	 final;


	/**
	 * \brief	This fucntion returns the covariance matrix of *this.
	 *
	 * This is a digaonla matrix.
	 */
	virtual
	CoVarMat_t
	getCoVar()
	 const
	 final;


	/*
	 * ===================
	 * Domain functions
	 */
public:
	/**
	 * \brief	THis function reetruns the dimension the generated samples has.
	 */
	virtual
	Size_t
	nDims()
	 const
	 final;


	/**
	 * \brief	Returns true if this is undounded.
	 */
	virtual
	bool
	isUnbounded()
	 const
	 final;


	/**
	 * \brief	This function returns the domain, in which the
	 * 		 sampleing is restricted.
	 *
	 * If given this the cube returned by this fucntion is the second
	 * Argument of the constructor.
	 * Notice that if tone the support domain was given at construction,
	 * i.e. the constrctor that only takes one argument, the support domain
	 * is returned.
	 *
	 */
	virtual
	HyperCube_t
	getSampleDomain()
	 const
	 final;


	/**
	 * \brief	This function returns the pdf support.
	 *
	 * This is the first argument of constructor.
	 * It returns where the sampling is actually possible.
	 *
	 * \note	Not to be confused by the sampling domain.
	 */
	virtual
	HyperCube_t
	getSupportDomain()
	 const
	 final;


	/*
	 * ===================
	 * Workhorse interface
	 */
protected:

	/**
	 * \brief	This function generates one sample that is
	 * 		 distributed as the implementation.
	 *
	 * See in teh description above for how it works.
	 *
	 * \param  generator		This is the random generator
	 * \param  sample		This is the sample that should be generated.
	 */
	virtual
	void
	wh_generateSample(
		pRNGenerator_t&		generator,
		Sample_t*		sample)
	 final;


	/**
	 * \brief	This function return the pdf of the sample.
	 *
	 * This is the normal version of the exponential
	 * we are used.
	 *
	 * \param  sample	The pdf to compute
	 */
	virtual
	Real_t
	wh_pdf(
		const Sample_t&		sample)
	 const
	 final;

	/*
	 * ========================
	 * Dumping ffunction
	 *
	 * As stated in the interface, this only
	 * saves the state of the random distrinbution
	 * that are defined from the standard.
	 * Other parameter are not saved/loaded.
	 */
public:
	/**
	 * \brief	Write to a stream
	 */
	virtual
	std::ostream&
	dumpToStream(
		std::ostream&	dumpStream)
	 final;

	/**
	 * \brief	Load from a stream
	 */
	virtual
	std::istream&
	loadFromStream(
		std::istream&	dumpStream)
	 final;


	/*
	 * =======================
	 * Private Memeber
	 */
private:
	HyperCube_t 		m_suppDom;		//!< This is the sampling domain
	HyperCube_t 		m_sampDom;		//!< This is the sampling domain
	UniformDistVec_t 	m_uniSamp;		//!< This is the sampling object.
	Numeric_t 		m_iVol;			//!< This is the inverse volume, aka the normalization conmstant
}; //End: class(yggdrasil_multiDimUniform_t)




YGGDRASIL_NS_END(yggdrasil)


