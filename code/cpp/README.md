# Info
This folder contains the implementation of Yggdrasil as a C++ project.


# Authorship & License
Yggdrasil was written, with the exception of the external libraries and the assert code, by Philip Müller (phimuell@ethz.ch).
The code of Yggdrasil is published under the terms of the GNU GPL version 3 or newer.


## Other Parts

### Assert Code
The code for the assertion was originally written by Stefano Weidmann (stefanow@ethz.ch).
This code was derived from PGL (separate project of Philip) and Stefano granted all using rights to to Philip.

### Fast Code Project
The code of the Fast Code Course in FS19, is included in the responsibility but was not used.
Although it was not used directly, some aspects of its design inspired Yggdrasil.
See also separate README.md file in the fast code folder.


# Folders
Here is a small description of the folders.

## CORE
The folder ‘core’ contains some basic functionality that can be used in some larger context.
As an example all type definitions and the assert code is located there.


## TREE
The folder ‘tree’ contains all the code that is related to the the actual tree implementation.
However some aspects that are needed/used by the tree, but are clearly not treeish are located in different folder, such as the statistical tests.


## SAMPLES
This folder contains all code that is written for the samples.
This includes the containers as well as a class that models a single sample.

### Universal Container Interface
All containers implements an universal container interface.
This is a set of methods that all containers are required to implement.
This interface allows that the containers can be used interchangeably.
Note that the containers are not implemented as polymorphic class with dynamic dispatch.


## UTIL
The ‘util’ folder contains all general purpose utilities.
It is basically a collection of classes and functionality that does not belong to any other folder, but offers such a great and far reaching functionality that they could be used as building blocks of other code parts.


## INTERFACES
This folder contains the interfaces that were developed for Yggdrasil.
Some aspects like the statistical tests or the parametric model are clearly candidate bo be implemented by polymorphism.
The interfaces allows to add new models and tests to Yggdrasil without much efforts.
See also the ‘factory’ folder.
If Yggdrasil is extended the user should carefully study the description of the interface and the description of the functions.
They all describes in much detail what they expect and what condition must be met.
Also the user should look at the concrete implementation.


## PARA_MODELS
This folder contains the implementation of the parametric models, that are used inside a single domain.


## STAT_TEST
In this folder the implementation for the statistical tests, namely the GOF and the independence test are located.


## SPLIT_SCHEME
This folder contains the implementation for the two different ways of splitting a domain.


## TESTS
This folder contains some simple tests.
The call the building blocks of Yggdrasil with a careful crafted input and check if the result meets the expectation.
Some of the more involved tests are build directly by the CMake file.
Some earlier and very simple tests as a shell script that compile, run and check the output.


## RANDOM
This folder contains the functionality for the generation of random numbers.
In this folder a new interface is defined.
This interface is used to model a random number distribution.
The interface is designed such that concrete distribution/classes only have to provide only have to provide a very small set of functions.
All other functions are then reduced to them.
While this is makes it very easy and fast to add a new distribution to the code base it makes them slow.
They are intended for the verification procedure, but are also provided by pyYggdrasil.

The TreeSampler, which allows to generate samples from a tree is not located here, but in the ‘tree_geni’ folder.


## VERIFICATION
This code contains the functionality for running a scaling experiment.
It also contains auxiliary code like a class for storing and outputting the results.
It is important that this folder only contains the code for performing a scaling experiment.
It does not contain a program for actually doing one, see the ‘progs’ folder for that.


## FACTORY
This folder contains the functionality that is used for creating a tree.
For example it contains the tree builder class.
If Yggdrasil is extended by a new model or test only the code in this folder has to be adapted to include the new functionality into Yggdrasil.
See the manual and the code for more information.
It should be quite clear how to do it.


## PROGS
This folder contains some programs that can be used to test Yggdrasil.
For example it contains the scaling experiment program, which allows to perform a large scale experiment.
Also the plot generator is provided for performing a single experiment.
See the manual for more information on them.


## PYBIND11
This is the library that generates the code for the Python interface.
It is actually a submodule, a copy of the used version is provided.


## PYYGGDRASIL
This is short of "PYthon YGGDRASIL".
Since pybind11 is used to generate the actual bindings between Python and Yggdrasil, the files in this folder only defines the interface.
It is also the location where the doc strings of pyYggdrasil can be changed.
It should be quite clear how they are working.
Note that I make use of a “Cish” behavior.
If the compiler encounters two strings, that are not separated by something, then they are concatenated.
Since pybind11 make heavily use of template metaprogramming this can lead to some funny effects, so keep tracks of the commas.


## TREE_GENI
This folder contains the code that allows to draw samples form a tree.
It also contains the classes that models conditions.





