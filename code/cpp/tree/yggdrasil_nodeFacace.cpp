/**
 * \file	tree/yggdrasil_nodeFacade.cpp
 * \brief 	This is a facade to the node.
 *
 * This file implements the function of the node facade.
 * It will include all the nasty stuff that is needed to realy access it.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_treeNode.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>

YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_nodeFacade_t::yggdrasil_nodeFacade_t(
	cNode_ptr node)
 :
  m_node(node)
{
	if(m_node == nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to construct a node facade with an null ptr.");
	};
	if(m_node->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The source node is not ok.");
	};

	//We not check tif the node is integrity.
	//This would consume to much cycles, so we hope for the best.
};


bool
yggdrasil_nodeFacade_t::isOKNode()
 const
 noexcept
{
	return m_node->isOKNode();
};


bool
yggdrasil_nodeFacade_t::checkIntegrity()
 const
 noexcept
{
	return m_node->checkIntegrity();
};

bool
yggdrasil_nodeFacade_t::isDirty()
 const
 noexcept
{
	return m_node->isDirty();
};

const yggdrasil_nodeFacade_t*
yggdrasil_nodeFacade_t::operator-> ()
 const
 noexcept
{
	return this;
};


yggdrasil_nodeFacade_t*
yggdrasil_nodeFacade_t::operator-> ()
 noexcept
{
	return this;
};

yggdrasil_nodeFacade_t::HyperCube_t
yggdrasil_nodeFacade_t::getDomain()
 const
{
	//Make a copy
	HyperCube_t ret = m_node->getDomain();

	//test if it is valid
	yggdrasil_assert(ret.isValid() == true);

	return ret;
};


const yggdrasil_nodeFacade_t::SampleCollection_t&
yggdrasil_nodeFacade_t::getSampleCollection()
 const
{
	return m_node->getSampleCollection();
};


const yggdrasil_nodeFacade_t::SampleCollection_t&
yggdrasil_nodeFacade_t::getCSampleCollection()
 const
{
	return m_node->getSampleCollection();
};


const yggdrasil_nodeFacade_t::DimArray_t&
yggdrasil_nodeFacade_t::getDimensionalArray(
	const Int_t 	d)
 const
{
	return m_node->getCDim(d);
};


Size_t
yggdrasil_nodeFacade_t::getMass()
 const
{
	return m_node->getMass();
};

Size_t
yggdrasil_nodeFacade_t::getDirtyMass()
 const
{
	return m_node->getDirtyMass();
};


Size_t
yggdrasil_nodeFacade_t::nDims()
 const
{
	return m_node->nDims();
};


Size_t
yggdrasil_nodeFacade_t::nSamples()
 const
{
	return m_node->nSamples();
};


yggdrasil_nodeFacade_t::yggdrasil_nodeFacade_t(
	const yggdrasil_nodeFacade_t&)
 noexcept
 = default;


yggdrasil_nodeFacade_t::yggdrasil_nodeFacade_t(
	yggdrasil_nodeFacade_t&&)
 noexcept
 = default;


bool
yggdrasil_nodeFacade_t::isLeaf()
 const
 noexcept
{
	return m_node->isLeaf();
};


bool
yggdrasil_nodeFacade_t::isTrueSplit()
 const
 noexcept
{
	return m_node->isTrueSplit();
};

bool
yggdrasil_nodeFacade_t::isIndirectSplit()
 const
 noexcept
{
	return m_node->isIndirectSplit();
};

bool
yggdrasil_nodeFacade_t::isFullySplittedLeaf()
 const
{
	return m_node->isFullySplittedLeaf();
};


bool
yggdrasil_nodeFacade_t::hasParModel()
 const
{
	return m_node->hasParModel();
};


bool
yggdrasil_nodeFacade_t::isParModelFitted()
 const
{
	return m_node->isParModelFitted();
};


const yggdrasil_nodeFacade_t::ParModel_i*
yggdrasil_nodeFacade_t::getParModel()
 const
{
	return m_node->getParModel();
};




yggdrasil_nodeFacade_t::~yggdrasil_nodeFacade_t()
 noexcept
 = default;


bool
yggdrasil_nodeFacade_t::hasGOFTest()
 const
{
	return m_node->hasGOFTest();
};

bool
yggdrasil_nodeFacade_t::isGOFTestAssociated()
 const
{
	return m_node->isGOFTestAssociated();
};

const yggdrasil_nodeFacade_t::GOFTest_i*
yggdrasil_nodeFacade_t::getGOFTest()
 const
{
	return m_node->getGOFTest();
};

bool
yggdrasil_nodeFacade_t::isIndepTestAssociated()
 const
{
	return m_node->isIndepTestAssociated();
};

bool
yggdrasil_nodeFacade_t::hasIndepTest()
 const
{
	return m_node->hasIndepTest();
};

const yggdrasil_nodeFacade_t::IndepTest_i*
yggdrasil_nodeFacade_t::getIndepTest()
 const
{
	return m_node->getIndepTest();
};










YGGDRASIL_NS_END(yggdrasil)

