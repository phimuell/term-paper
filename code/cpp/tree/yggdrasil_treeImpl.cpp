/**
 * \file	tree/yggdrasil_treeImpl_priv.cpp
 * \brief 	This file cotains the implementation of the tree.
 *
 * This contains the code for the normal public accessable function of the tree.
 *
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>

#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>

#include <util/yggdrasil_treeDepthMap.hpp>

//Include STD
#include <cmath>
#include <fstream>

YGGDRASIL_NS_BEGIN(yggdrasil)

void
yggdrasil_DETree_t::printToFile(
	const std::string&	file,
	bool 			noS)
 const
{
	std::ofstream out;
	out.open(file.c_str(), std::ofstream::out | std::ofstream::trunc);

	if(out.is_open() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Could not open the file of writting the tree.");
	};

	//Writting the tree
	this->printToStream(out, noS);

	if(out.fail() == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("While writting to the file an error occured.");
	};

	out.close();

	return;
}; //End print to file

std::ostream&
yggdrasil_DETree_t::printToStream(
	std::ostream&	o,
	bool 		noS)
 const
{
	//Print the data domain, the true as arguments means that
	//brackets are used
	o << "Data Domain: " << m_dataSpace.print(true) << "\n";


	//Now printing the rest allready with an intent
	o
		<< "TREE:" << "\n"
		<< "{" << "\n";
		this->m_root->priv_printToStream(o, "\t", "\t", noS);
	o
		<< "}" << "\n";

	return o;
}; //End printToStream



bool
yggdrasil_DETree_t::isValid()
 const
 noexcept
{
	//For the begionnnig the node msust be okay and the domain valid
	if(m_root == nullptr)
	{
		//ONe could define this as an error, but it could be good
		//that it is not
		return false;
	};
	if(m_root->isOKNode() == false)
	{
		return false;
	};
	if(m_root->hasParModel() == false)
	{
		return false;
	};
	if(m_root->hasGOFTest() == false)
	{
		return false;
	};
	if(m_dataSpace.isValid() == false)
	{
		return false;
	};

	//std::cerr << "PLEASE IMPROVE ME" << std::endl;


	return true;
}; //End: isValid


bool
yggdrasil_DETree_t::checkIntegrity()
 const
 noexcept
{
	if(this->isValid() == false)
	{
		yggdrasil_assert(false);
		return false;
	};

	//Now call the integrity function recursively
	if(this->m_root->checkIntegrityRecursively() == false)
	{
		yggdrasil_assert(false);
		return false;
	};

	//All test are passed so we return true
	return true;
}; //End: checkIntegrity on tree


bool
yggdrasil_DETree_t::isFullySplittedTree()
 const
{
	if(this->isValid() == false)
	{
		yggdrasil_assert(false);
		return false;
	};

	if(this->m_root->isFullySplittedNodeRecursively() == false)
	{
		yggdrasil_assert(false);
		return false;
	};

	return true;
}; //ENd: isFullyfitted tree

::std::tuple<Size_t, Size_t, Size_t>
yggdrasil_DETree_t::getTreeComposition()
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The tree is not valid.");
	};

	return m_root->getSubTreeComposition();
}; //End: getTreeDecomposition

yggdrasil_DETree_t::DepthMap_t
yggdrasil_DETree_t::getDepthMap()
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The tree is not valid.");
	};

	NodeFacade_t nf(this->m_root.get());
	DepthMap_t dm(nf);

	return dm;
}; //End getDepthMap

Size_t
yggdrasil_DETree_t::nNodes()
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The tree was not valid.");
	};

	Size_t trueSplitsNodes, indirectSplitNodes, leafNodes;

	//Get the composition
	::std::tie(trueSplitsNodes, indirectSplitNodes, leafNodes) = this->getTreeComposition();

	return (trueSplitsNodes + indirectSplitNodes + leafNodes);
}; //End: getnNodes


YGGDRASIL_NS_END(yggdrasil)


