#pragma once
/**
 * \file	tree/yggdrasil_nodeFacade.hpp
 * \brief 	This is a facade to the node.
 *
 * The node facade is a class that is a facade for the node.
 * It manages the access to the nodes from the user.
 *
 * The main intention is that the number amount of code that has to be included
 * is limited to only the neccessary service code.
 *
 * This class also is able to be used as a sub tree.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_leafIterator.hpp>

//#include <interfaces/yggdrasil_interface_parametricModel.hpp>



YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the node
class yggdrasil_DETNode_t;

//FWD of the iterator
class yggdrasil_leafIterator_t;

//FWD of of the parameteric model interface
class yggdrasil_parametricModel_i;

//FWD the two tests
class yggdrasil_IndepTest_i;
class yggdrasil_GOFTest_i;

/**
 * \class	yggdrasil_nodeFacade_t
 * \brief	A very light weight interface to a node.
 *
 * This is a very small interface to the node.
 * It is like a non owning managed pointer to the node.
 * It will be constructed with a pointer to a node and
 * then be used by the user.
 *
 * This has two effects, first the user is limited in
 * its ability to shoot itself in the foot and also
 * the number of code that has to be included is dramatically limited.
 *
 * The facade models a constant node.
 * This means that no modifing access is allowed with it.
 */
class yggdrasil_nodeFacade_t
{
	/*
	 * =======================
	 * Typedef
	 */
public:
	using Node_t 		  = ::yggdrasil::yggdrasil_DETNode_t;	//!< This is the node type
	using Node_ptr 		  = Node_t*;				//!< Pointer to a node
	using Node_ref 		  = Node_t&;				//!< This is a reference to a node
	using cNode_ref 	  = const Node_t&;			//!< This is a constant reference to a node
	using cNode_ptr 	  = const Node_t*;			//!< This is a pointer to a constant node

	using SampleCollection_t  = yggdrasil_sampleCollection_t;	//!< This is the sample collection
	using DimensionArray_t 	  = SampleCollection_t::DimensionArray_t; //!< This is one diomension of the sample collection
	using DimArray_t 	  = DimensionArray_t;

	using Sample_t 		  = yggdrasil_sample_t;			//!< This is the sample type
	using HyperCube_t 	  = yggdrasil_hyperCube_t;		//!< This is the type for a hyper cube, aka domain bound
	using IntervalBound_t 	  = HyperCube_t::IntervalBound_t;	//!< This is the bound of a single interval

	using ParModel_i 	  = ::yggdrasil::yggdrasil_parametricModel_i;	//!< This is the interface of the parameteric model

	using GOFTest_i 	  = yggdrasil_GOFTest_i;		//!< This is the interface of the gof test.
	using IndepTest_i 	  = yggdrasil_IndepTest_i;		//!< This is the interface for the independence test.

	using LeafIterator_t 	  = yggdrasil_leafIterator_t;		//!< This is the type for the iterator of the leafes



	/*
	 * ========================
	 * Constructors
	 *
	 * This class can be copied and moved.
	 * But it is not possible to construct
	 * without a node poiner.
	 *
	 * Also the building constructor is private.
	 * This means that only friends fo the class
	 * can construct it.
	 *
	 */
private:
	/**
	 * \brief	This is the building constructor.
	 *
	 * It takes a pointer to a node and construct
	 * itself.
	 * If the pointer is the null pointer an exception is trigered.
	 *
	 * \param  node		The node that should be accessed.
	 */
	yggdrasil_nodeFacade_t(
		cNode_ptr);


public:
	/**
	 * \brief	The default constructor.
	 *
	 * Is deleted.
	 */
	yggdrasil_nodeFacade_t()
	 = delete;


	/**
	 * \brief	The copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_nodeFacade_t(
		const yggdrasil_nodeFacade_t&)
	 noexcept;


	/**
	 * \brief	The move constructor.
	 *
	 * Is deleted,
	 */
	yggdrasil_nodeFacade_t(
		yggdrasil_nodeFacade_t&&)
	 noexcept;


	/**
	 * \brief	The copy assignment.
	 *
	 * If deleted, thus modeling a real constant object
	 */
	yggdrasil_nodeFacade_t&
	operator= (
		const yggdrasil_nodeFacade_t&)
	 noexcept
	 = delete;


	/**
	 * \brief	The move assignment.
	 *
	 * Id deleted, thsi si needed for provideding
	 * a real consatnt object.
	 */
	yggdrasil_nodeFacade_t&
	operator= (
		yggdrasil_nodeFacade_t&&)
	 noexcept
	 = delete;


	/**
	 * \brief	The destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_nodeFacade_t()
	 noexcept;


	/*
	 * ===================
	 * ===================
	 * Interface functions
	 */
public:
	/*
	 * ==================
	 * Leaf Access
	 *
	 * These functions allow the access to the
	 * leafs beneeth the root
	 */
public:
	/**
	 * \brief	Returns the the beginning of the containing leaf ranges.
	 *
	 * Thsi function returns the beginning iterator to all leafs that are beneath \c *this.
	 * This allows the iteration over the leafs.
	 *
	 * The order in which the leafs are visited is unspecific.
	 *
	 * Notice that this only gives read only access.
	 *
	 * Notice that there is no end function, since the iterators are able
	 * to determine if the end is reached.
	 */
	LeafIterator_t
	getLeafRange()
	 const;


	/*
	 * ===================
	 * Status functions
	 */
public:

	/**
	 * \brief	This function return if the node is okay.
	 *
	 * Okay is a cheap and quick way to test if a node is consistent or not.
	 */
	bool
	isOKNode()
	 const
	 noexcept;


	/**
	 * \brief	This function returns true if \c *this is integrity.
	 *
	 * A node which meats the integrity constraints, is a node
	 * that which has no obvious problems.
	 *
	 * The isOKNode() function only checks a small part of this aspect.
	 * This function checks the inegrity of the payload.
	 *
	 * \note 	This function does no recursive check on its child.
	 */
	bool
	checkIntegrity()
	 const
	 noexcept;



	/**
	 * \brief	This function checks if the node is dirty
	 */
	bool
	isDirty()
	 const
	 noexcept;


	/*
	 * State functions.
	 *
	 * This deas with what *this realy is
	 */
public:
	/**
	 * \brief	This function returns true if \c *thsi si a leaf.
	 */
	bool
	isLeaf()
	 const
	 noexcept;

	/**
	 * \brief	This returns \c true if *this is a "true split".
	 *
	 * A true split is a split that was created directly from a leaf.
	 */
	bool
	isTrueSplit()
	 const
	 noexcept;

	/**
	 * \brief	Thsi fucntion returns \c true if *this is an "Indirect split".
	 *
	 * An indirect split is a split that is created indirecly, because
	 * we splitted a leaf into more than two parts.
	 */
	bool
	isIndirectSplit()
	 const
	 noexcept;


	/**
	 * \brief	This function returns true if *This is fully splitted leaf.
	 *
	 * A leaf is fully splitted if all the tests are done and and the split sequence
	 * of both are empty.
	 *
	 * It is not considered an error if the tests are not associated and also
	 * not all tests are done, in that case false is returned.
	 *
	 * \throw	If *thsi si nopt a leaf.
	 */
	bool
	isFullySplittedLeaf()
	 const;





	/*
	 * ==================
	 * Accessing function
	 */
public:
	/**
	 * \brief 	This function returns the hypercube of the associated note.
	 *
	 * It is important that this hypercube describes the space that *this
	 * occupies realtive to the rescalled root data space.
	 * This data domain is also known as node data space.
	 */
	HyperCube_t
	getDomain()
	 const;


	/**
	 * \brief	This function returns the mass of the node.
	 *
	 * The mass is the number of samples that are currently used to build
	 * the estimater on it
	 */
	Size_t
	getMass()
	 const;


	/**
	 * \brief	This function returns the dirty mass.
	 *
	 * The dirty mass is the number of samples that
	 * where added to this but not yet used to fit the models.
	 *
	 * It is like the total number of sampels that are stored
	 * in all leafes beneath this.
	 */
	Size_t
	getDirtyMass()
	 const;


	/*
	 * ==================
	 * Sample accessing function
	 *
	 * This function goives access to the samples
	 */
public:

	/**
	 * \brief	This function returns a constant reference to the sample container of the node.
	 *
	 * Note that the samples that are stored inside the collection are expresend by
	 * the RESCALLED node data space, thus lies in the unit hyper cube.
	 */
	const SampleCollection_t&
	getSampleCollection()
	 const;


	/**
	 * \brief	This function returns a constant reference to the sample container of the node.
	 *
	 * This is the explicit constant version.
	 *
	 * Note that the samples that are stored inside the collection are expresend by
	 * the RESCALLED node data space, thus lies in the unit hyper cube.
	 */
	const SampleCollection_t&
	getCSampleCollection()
	 const;


	/**
	 * \brief	Thsi function returns a reference to the dimensional array of teh requested dimension.
	 *
	 * This function does no checking on its own.
	 *
	 * Note that the samples that are stored inside the collection are expresend by
	 * the RESCALLED node data space, thus lies in the unit hyper cube.
	 *
	 * \param  d	The dimension we want to access.
	 *
	 * \throw	This fucntion relies on the underling implementation of the node.
	 * 		 Generally the node must be valid and a leaf.
	 */
	const DimArray_t&
	getDimensionalArray(
		const Int_t 	d)
	 const;


	/**
	 * \brief	This fucntion returns the number of dimensions.
	 *
	 * For this the sample collection is queried.
	 */
	Size_t
	nDims()
	 const;


	/**
	 * \brief	This fucntion returrns the number of samples that are
	 * 		 stored in the underling sample collection.
	 *
	 * For this end, ths sample collection is queried.
	 *
	 * \note 	In theory this fucntion should return the same value as
	 * 		 the getDirtyMass() does.
	 */
	Size_t
	nSamples()
	 const;


	/*
	 * =================
	 * Model access
	 *
	 * These models are jsut wrappers.
	 * They call the respectively node
	 * function directly, without any tests.
	 */
public:
	/**
	 * \brief	This function returns true if *this has a parameteric model.
	 *
	 * \note	All node types, excedpt the indirect splits will have a model.
	 */
	bool
	hasParModel()
	 const;


	/**
	 * \brief	This function returns a pointer to the parameteric model.
	 *
	 * \throw 	If *this does not have one.
	 */
	const ParModel_i*
	getParModel()
	 const;

	/**
	 * \brief	Returns true if the parameteric model is fully fitted.
	 *
	 * \note	That this function test if the parameteric model is fitted.
	 * 		 This function implies that if its value is true,
	 * 		 that the model is associated, the reverse is not true.
	 *
	 * \throw	If *this does not have one.
	 */
	bool
	isParModelFitted()
	 const;


	/*
	 * ====================
	 * Statistical tests
	 *
	 * These fucntions gives access to statistical tests
	 */
public:
	/**
	 * \brief	Returns a pointer to the GOF test object.
	 *
	 * Notioce that this function could potentially
	 * return the nullpointer, in the case that *this is an
	 * indiorect split, but in that case the fucntion will
	 * generate an exception.
	 *
	 * \throw	If no GOF test is present.
	 */
	const GOFTest_i*
	getGOFTest()
	 const;


	/**
	 * \brief	THis fucntion returns true if *this
	 * 		 has an associated GOF test.
	 *
	 * In theory this fucntion is an alias of isIndirectSplitState().
	 * But in my view it is better to provide a fucntion that
	 * is named explicit.
	 */
	bool
	hasGOFTest()
	 const;


	/**
	 * \brief	This function returns true if the GOF Test of
	 * 		 *this is associated.
	 *
	 * \note	In a bvalid tree the user can expect that this
	 * 		 function returns true for all node, excepts
	 * 		 the ones that are indirect splits.
	 *
	 * \throw	If *thsi does not have a GOF test.
	 */
	bool
	isGOFTestAssociated()
	 const;


	/**
	 * \brief	This fucntion returns true if *this has an
	 * 		 independence test.
	 *
	 * *this does not have an independence test, if it is an
	 * indirect split.
	 */
	bool
	hasIndepTest()
	 const;


	/**
	 * \brief	This function returns a pointer to the
	 * 		 independence test of *this.
	 *
	 * This function throws if *this does nbot have an
	 * independence test, this could be the case if
	 * *this is an indirect split
	 *
	 * \throw	If *this does not have an independence test.
	 */
	const IndepTest_i*
	getIndepTest()
	 const;


	/**
	 * \brief	This fucntion returns true if the independence
	 * 		 test of *this is associated.
	 *
	 * \note	In a valid tree it is not neccessaraly the case
	 * 		 that this function returns true for all nodes,
	 * 		 that has one. This function returns ture for all
	 * 		 leafs, but it may return false if the node
	 * 		 is an true split.
	 *
	 * \throw	If this does not have an associated indpoendende
	 * 		 test.
	 */
	bool
	isIndepTestAssociated()
	 const;



	/*
	 * ============
	 * Operators
	 */
public:
	/**
	 * \brief	This function allows the drop in replacement of the node pointer
	 * 		 with a facade object.
	 *
	 * To be blunt, this is a hack
	 */
	const yggdrasil_nodeFacade_t*
	operator-> ()
	 const
	 noexcept;


	/**
	 * \brief	This function allows the drop in replacement of the node pointer
	 * 		 with a facade object.
	 *
	 * To be blunt, this is a hack
	 */
	yggdrasil_nodeFacade_t*
	operator-> ()
	 noexcept;




	/*
	 * =================
	 * Friends
	 */
private:
	//The node itself is a friend
	friend class yggdrasil_DETNode_t;

	//The iterator must be a friend in order to construct it
	friend class yggdrasil_leafIterator_t;

	//The tree also needs to be a friend
	friend class yggdrasil_DETree_t;


	/*
	 * =================
	 * Private Member
	 */
private:
	cNode_ptr 		m_node;		//!< This is the node that will be managed
}; //End class: yggdrasil_nodeFacade_t


YGGDRASIL_NS_END(yggdrasil)







