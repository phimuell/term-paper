/**
 * \file	tree/yggdrasil_nodeFacade_interfaceFkt.cpp
 * \brief 	This is a facade to the node.
 *
 * This file implements the interface functions of the node facade.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

YGGDRASIL_NS_BEGIN(yggdrasil)

yggdrasil_nodeFacade_t::LeafIterator_t
yggdrasil_nodeFacade_t::getLeafRange()
 const
{
	return LeafIterator_t(m_node);
};

YGGDRASIL_NS_END(yggdrasil)







