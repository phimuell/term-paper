/**
 * \file	tree/yggdrasil_treeNode_accessPayload.cpp
 * \brief	This file contains all the functions that have to interact with the payload.
 *
 * Other files can also use the payload, but this one implements the functions
 * that only query the payload and does not operate on them.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>

//Include STD
#include <cmath>




YGGDRASIL_NS_BEGIN(yggdrasil)



void
yggdrasil_DETNode_t::priv_allocPayload(
	const Int_t 	sampleDimension)
{
	if(m_payload != nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The payload is allready allocated.");
	};

	m_payload = new Payload_t(sampleDimension);

	return;
};


const yggdrasil_DETNode_t::HyperCube_t&
yggdrasil_DETNode_t::getDomain()
 const
{
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to access the domain of an invalid (not okay) node.");
	};
	if(m_payload->getDomain().isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The domain of *this is not valid.");
	};

	return m_payload->getDomain();
};

yggdrasil_DETNode_t::SampleCollection_t&
yggdrasil_DETNode_t::getSampleCollection()
{
	//Check if *this is a leaf
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not ok");
	};
	if(m_payload->isLeafState() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Only a leaf has assocciated samples.");
	};

	return m_payload->getSampleCollection();
};


const yggdrasil_DETNode_t::SampleCollection_t&
yggdrasil_DETNode_t::getSampleCollection()
 const
{
	//Check if *this is a leaf
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not ok");
	};
	if(m_payload->isLeafState() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Only a leaf has assocciated samples.");
	};

	return m_payload->getSampleCollection();
};


const yggdrasil_DETNode_t::SampleCollection_t&
yggdrasil_DETNode_t::getCSampleCollection()
 const
{
	//Check if *this is a leaf
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not ok");
	};
	if(m_payload->isLeafState() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Only a leaf has assocciated samples.");
	};

	return m_payload->getSampleCollection();
};


yggdrasil_DETNode_t::DimensionArray_t&
yggdrasil_DETNode_t::getDim(
	const Int_t 	d)
{
	//Check if *this is a leaf
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not ok");
	};
	if(m_payload->isLeafState() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Only a leaf has assocciated samples.");
	};

	return m_payload->getDim(d);
};

const yggdrasil_DETNode_t::DimensionArray_t&
yggdrasil_DETNode_t::getDim(
	const Int_t 	d)
 const
{
	//Check if *this is a leaf
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not ok");
	};
	if(m_payload->isLeafState() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Only a leaf has assocciated samples.");
	};

	return m_payload->getCDim(d);
};

const yggdrasil_DETNode_t::DimensionArray_t&
yggdrasil_DETNode_t::getCDim(
	const Int_t 	d)
 const
{
	//Check if *this is a leaf
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not ok");
	};
	if(m_payload->isLeafState() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Only a leaf has assocciated samples.");
	};

	return m_payload->getCDim(d);
};


Size_t
yggdrasil_DETNode_t::nDims()
 const
{
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not okay.");
	};

	return m_payload->nDims();
}; //End: nDims


Size_t
yggdrasil_DETNode_t::nSamples()
 const
{
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not okay");
	};

	return m_payload->nSamples();
}; //End: nSamples





bool
yggdrasil_DETNode_t::isDirty()
 const
{
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not ok");
	};

	return m_payload->isDirty();
};

Size_t
yggdrasil_DETNode_t::getMass()
 const
{
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not okay.");
	};

	return m_payload->getMass();
};

Size_t
yggdrasil_DETNode_t::getDirtyMass()
 const
{
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not okay.");
	};

	return m_payload->getDirtyMass();
};


bool
yggdrasil_DETNode_t::isFullySplittedLeaf()
 const
{
	//Test are done here, only in debug
	yggdrasil_assert(this->isLeaf());

	//Perform the test
	return (m_payload->isFullySplittedLeaf());
}; //End: is Fully splitted Leaf

bool
yggdrasil_DETNode_t::isFullySplittedNode()
 const
{
	//First call the check integrity fucntion
	if(this->checkIntegrity() == false)
	{
		yggdrasil_assert(false);
		return false;
	};

	//Now call the node test function
	if(m_payload->isFullySplittedNode() == false)
	{
		yggdrasil_assert(false);
		return false;
	};

	//Return true to indicate that everything went well
	return true;
}; //ENd is fully splitted recurively



bool
yggdrasil_DETNode_t::isTrueSplit()
 const
 noexcept
{
	return this->m_payload->isTrueSplitState();
}; //End: is TrueSplit


bool
yggdrasil_DETNode_t::isIndirectSplit()
 const
 noexcept
{
	return this->m_payload->isIndirectSplitState();
}; //End: isIndirectSplit



const yggdrasil_DETNode_t::GOFTest_i*
yggdrasil_DETNode_t::getGOFTest()
 const
{
	//Test if *this has a GOF test
	if(this->m_payload->m_GOFTest == nullptr)
	{
		yggdrasil_assert(this->isIndirectSplit() == true);
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to access a non exoisting GOF test. You most likely called the fucntion on an indirect split.");
	};

	//Make some small tests
	yggdrasil_assert(this->isIndirectSplit() == false);
	yggdrasil_assert(this->isOKNode());

	//Return the pointer
	return m_payload->m_GOFTest.get();
}; //End: getGOFTest

bool
yggdrasil_DETNode_t::hasGOFTest()
 const
{
	if(this->m_payload->m_GOFTest.get() == nullptr)
	{
		//No GOF test, this means *this is an indirect split
		yggdrasil_assert(this->isIndirectSplit() == true);

		return false;
	}; // End: no GOF test

	yggdrasil_assert(this->isIndirectSplit() == false);
	return true;
}; //End: hasGOF


bool
yggdrasil_DETNode_t::isGOFTestAssociated()
 const
{
	if(this->m_payload->m_GOFTest.get() == nullptr)
	{
		//No GOF test, this means *this is an indirect split
		yggdrasil_assert(this->isIndirectSplit() == true);

		throw YGGDRASIL_EXCEPT_LOGIC("Called the function on a node that does not have an GOF test in the first place.");
	}; // End: no GOF test

	yggdrasil_assert(this->isIndirectSplit() == false);


	return (this->m_payload->m_GOFTest->isAssociated());
}; //End gof assoc


bool
yggdrasil_DETNode_t::hasIndepTest()
 const
{
	if(this->m_payload->m_indepTest == nullptr)
	{
		//no indep thes, this must be an indirect split
		yggdrasil_assert(this->isIndirectSplit() == true);

		return false;
	}; //End if: no indep test

	yggdrasil_assert(this->isIndirectSplit() == false);

	return true;
}; //End: has indep test


const yggdrasil_DETNode_t::IndepTest_i*
yggdrasil_DETNode_t::getIndepTest()
 const
{
	if(this->m_payload->m_indepTest == nullptr)
	{
		yggdrasil_assert(this->isIndirectSplit() == true);
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to access the independence test of a node which does not have one. Maybe called on an indirect split?");
	};

	yggdrasil_assert(this->isIndirectSplit() == false);

	return this->m_payload->m_indepTest.get();
}; //End: get indep test


bool
yggdrasil_DETNode_t::isIndepTestAssociated()
 const
{
	if(this->m_payload->m_indepTest == nullptr)
	{
		yggdrasil_assert(this->isIndirectSplit() == true);
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to access the independence test of a node which does not have one. Maybe called on an indirect split?");
	};

	yggdrasil_assert(this->isIndirectSplit() == false);

	return this->m_payload->m_indepTest->isAssociated();
}; //End: is indep assoc


bool
yggdrasil_DETNode_t::hasParModel()
 const
{
	if(this->m_payload->m_parModel == nullptr)
	{
		yggdrasil_assert(this->isIndirectSplit() == true);
		return false;
	};

	yggdrasil_assert(this->isIndirectSplit() == false);

	return true;
}; //End: hasParModel

bool
yggdrasil_DETNode_t::isParModelFitted()
 const
{
	if(this->m_payload->m_parModel == nullptr)
	{
		yggdrasil_assert(this->isIndirectSplit() == true);
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to access the parametric model of a node that does not exists.");
	};

	yggdrasil_assert(this->isIndirectSplit() == false);

	return this->m_payload->m_parModel->allDimensionsFitted();
}; //End: is par model fitted

const yggdrasil_DETNode_t::ParModel_i*
yggdrasil_DETNode_t::getParModel()
 const
{
	if(this->m_payload->m_parModel == nullptr)
	{
		yggdrasil_assert(this->isIndirectSplit() == true);
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to access the parametric model of a node that does not exists.");
	};

	yggdrasil_assert(this->isIndirectSplit() == false);

	return this->m_payload->m_parModel.get();
}; //End: getParameteric model







YGGDRASIL_NS_END(yggdrasil)
