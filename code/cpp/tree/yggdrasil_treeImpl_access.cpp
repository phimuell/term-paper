/**
 * \brief 	This file cotains the implementation of the tree.
 *
 * This contains the code for the normal public accessable function of the tree.
 * It contains the fucntins which are more ore less simply a wrapper
 *
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>

#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>

//Include STD
#include <cmath>

YGGDRASIL_NS_BEGIN(yggdrasil)



yggdrasil_DETree_t::SubLeafIterator_t
yggdrasil_DETree_t::getLeafIterator()
 const
{
	yggdrasil_assert(this->isValid());

	return this->m_root->getLeafIterator();
}; //End: getLeaf iterator


const yggdrasil_DETree_t::HyperCube_t&
yggdrasil_DETree_t::getGlobalDataSpace()
 const
{
	yggdrasil_assert(this->isValid() == true);
	if(this->m_dataSpace.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The global data space of the tree is invalid.");
	};

	return this->m_dataSpace;
}; //End: get the data space


Size_t
yggdrasil_DETree_t::getMass()
 const
{
	yggdrasil_assert(this->isValid());
	yggdrasil_assert((this->isConsistent() == true) ? (m_root->getDirtyMass() == m_root->getMass()) : (m_root->getMass() < m_root->getDirtyMass()));

	return m_root->getMass();
}; //End: getMass


Size_t
yggdrasil_DETree_t::getDirtyMass()
 const
{
	yggdrasil_assert(this->isValid());
	yggdrasil_assert((this->isConsistent() == true) ? (m_root->getDirtyMass() == m_root->getMass()) : (m_root->getMass() < m_root->getDirtyMass()));

	return m_root->getDirtyMass();
}; //ENd: getDirtyMass


bool
yggdrasil_DETree_t::isConsistent()
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to access an invalid tree.");
	};

	//The isDirty property is sytacticaly inverted to the other property.
	return (m_root->isDirty() == false) ? true : false;
}; //End: isConsistent

Size_t
yggdrasil_DETree_t::nDims()
 const
 noexcept
{
	yggdrasil_assert(this->isValid());
	yggdrasil_assert(m_dataSpace.isValid());

	return m_dataSpace.nDims();
}; //End: nDims




YGGDRASIL_NS_END(yggdrasil)


















