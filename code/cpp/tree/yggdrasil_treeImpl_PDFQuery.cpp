/**
 * \brief 	This file cotains the implementation of the tree.
 *
 * This file contains teh functions that deals with the evaluation
 * of the tree.
 *
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>

#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>

//Include STD
#include <cmath>

YGGDRASIL_NS_BEGIN(yggdrasil)

/*
 * This is the fwd of the fucntion that
 * wilol performs the evaluation of the tree.
 * It allows templates to be used, without the need to
 * template the header.
 */
template<
	class 	Container_t,
	class   PDFVector_t,
	bool 	noErr
	>
extern
PDFVector_t
ygInteranl_evaluateContainer(
	const yggdrasil_DETree_t* const 	self,
	const Container_t& 			conatiner);




Numeric_t
yggdrasil_DETree_t::evaluateSamplesAt(
	const Sample_t& 	x)
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};
	if((this->m_dataSpace.nDims()) != x.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the container and the rtree does not agree.");
	};
	if(x.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Passed an invalid sample to the tree.");
	};

	//Now search for the node we want
	cSampleDescentRet_t node = this->priv_findElementOf(x);

	//Test if it lies inside the data range
	if(node.first == nullptr)
	{
		yggdrasil_assert(m_dataSpace.isInside(x) == false);

		throw YGGDRASIL_EXCEPT_LOGIC("Tried to query a node that lies outside the data range.");
	}; //ENd outside the data range


	//Now call the internal evaluation sample
	const Real_t pdfGlobal = this->priv_evaluateSampleGlobal(node);
	yggdrasil_assert(isValidFloat(pdfGlobal) == true);
	yggdrasil_assert(pdfGlobal >= 0.0);

	return pdfGlobal;
}; //End: evaluateSampleAt




Numeric_t
yggdrasil_DETree_t::evaluateSamplesAt_noErr(
	const Sample_t& 	x)
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};
	if((this->m_dataSpace.nDims()) != x.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the container and the rtree does not agree.");
	};
	if(x.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Tried to evaluate an invalid sample.");
	};

	//Now search for the node we want
	cSampleDescentRet_t node = this->priv_findElementOf(x);

	//Test if it lies inside the data range
	if(node.first == nullptr)
	{
		yggdrasil_assert(m_dataSpace.isInside(x) == false);

		return 0.0;
	}; //ENd outside the data range


	//Now call the internal evaluation sample
	const Real_t pdfGlobal = this->priv_evaluateSampleGlobal(node);
	yggdrasil_assert(isValidFloat(pdfGlobal) == true);
	yggdrasil_assert(pdfGlobal >= 0.0);

	return pdfGlobal;
}; //End: evaluateSampleAt


yggdrasil_DETree_t::PDFValueArray_t
yggdrasil_DETree_t::evaluateSamplesAt(
 	const SampleList_t&	X)
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};
	if(Size_t(this->m_dataSpace.nDims()) != X.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the container and the rtree does not agree.");
	};

	//The real check if the samples are valid or not is done inside the eval helper.
	//Hower we will do here a check that the assers provide the early fail feture
	yggdrasil_assert(std::all_of(X.cbegin(), X.cend(),
			[](const Sample_t& s) -> bool {return s.isValid();}));

	//Evaluate the samples using the generic method
	const PDFValueArray_t pdfsGlobal = ygInteranl_evaluateContainer<SampleList_t, PDFValueArray_t, true>(this, X);

	//Now return it
	return pdfsGlobal;
}; //ENd: Evaluate for many samples


yggdrasil_DETree_t::PDFValueArray_t
yggdrasil_DETree_t::evaluateSamplesAt_noErr(
 	const SampleList_t&	X)
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};
	if(Size_t(this->m_dataSpace.nDims()) != X.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the container and the rtree does not agree.");
	};

	//The real check if the samples are valid or not is done inside the eval helper.
	//Hower we will do here a check that the assers provide the early fail feture
	yggdrasil_assert(std::all_of(X.cbegin(), X.cend(),
			[](const Sample_t& s) -> bool {return s.isValid();}));

	//Evaluate the samples using the generic method
	const PDFValueArray_t pdfsGlobal = ygInteranl_evaluateContainer<SampleList_t, PDFValueArray_t, true>(this, X);

	//Now return it
	return pdfsGlobal;
}; //ENd: Evaluate for many samples without error


yggdrasil_DETree_t::PDFValueArray_t
yggdrasil_DETree_t::evaluateSamplesAt(
 	const SampleCollection_t&	X)
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};
	if((this->m_dataSpace.nDims()) != X.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the container and the rtree does not agree.");
	};

	//The real check if the samples are valid or not is done inside the eval helper.
	//Hower we will do here a check that the assers provide the early fail feture
	yggdrasil_assert([](const SampleCollection_t& X) -> bool
			{
			  const Size_t N = X.nSamples(); for(Size_t i = 0; i != N; ++i)
			    {if(X.getSample(i).isValid() == false) {return false;};};
			  return true;
			}(X));


	//Evaluate the samples using the generic method
	const PDFValueArray_t pdfsGlobal = ygInteranl_evaluateContainer<SampleCollection_t, PDFValueArray_t, true>(this, X);

	//Now return it
	return pdfsGlobal;
}; //ENd: Evaluate for many samples


yggdrasil_DETree_t::PDFValueArray_t
yggdrasil_DETree_t::evaluateSamplesAt_noErr(
 	const SampleCollection_t&	X)
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};
	if((this->m_dataSpace.nDims()) != X.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the container and the rtree does not agree.");
	};

	//The real check if the samples are valid or not is done inside the eval helper.
	//Hower we will do here a check that the assers provide the early fail feture
	yggdrasil_assert([](const SampleCollection_t& X) -> bool
			{
			  const Size_t N = X.nSamples(); for(Size_t i = 0; i != N; ++i)
			    {if(X.getSample(i).isValid() == false) {return false;};};
			  return true;
			}(X));

	//Evaluate the samples using the generic method
	const PDFValueArray_t pdfsGlobal = ygInteranl_evaluateContainer<SampleCollection_t, PDFValueArray_t, true>(this, X);

	//Now return it
	return pdfsGlobal;
}; //ENd: Evaluate for many samples without error

yggdrasil_DETree_t::PDFValueArray_t
yggdrasil_DETree_t::evaluateSamplesAt(
 	const SampleArray_t&	X)
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};
	if(Size_t(this->m_dataSpace.nDims()) != X.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the container and the rtree does not agree.");
	};

	//The real check if the samples are valid or not is done inside the eval helper.
	//Hower we will do here a check that the assers provide the early fail feture
	yggdrasil_assert([](const SampleArray_t& X) -> bool
			{
			  const Size_t N = X.nSamples(); for(Size_t i = 0; i != N; ++i)
			    {if(X.getSample(i).isValid() == false) {return false;};};
			  return true;
			}(X));

	//Evaluate the samples using the generic method
	const PDFValueArray_t pdfsGlobal = ygInteranl_evaluateContainer<SampleArray_t, PDFValueArray_t, false>(this, X);

	//Now return it
	return pdfsGlobal;
}; //ENd: Evaluate for many samples


yggdrasil_DETree_t::PDFValueArray_t
yggdrasil_DETree_t::evaluateSamplesAt_noErr(
 	const SampleArray_t&	X)
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};
	if(Size_t(this->m_dataSpace.nDims()) != X.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the container and the rtree does not agree.");
	};

	//The real check if the samples are valid or not is done inside the eval helper.
	//Hower we will do here a check that the assers provide the early fail feture
	yggdrasil_assert([](const SampleArray_t& X) -> bool
			{
			  const Size_t N = X.nSamples(); for(Size_t i = 0; i != N; ++i)
			    {if(X.getSample(i).isValid() == false) {return false;};};
			  return true;
			}(X));

	//Evaluate the samples using the generic method
	const PDFValueArray_t pdfsGlobal = ygInteranl_evaluateContainer<SampleArray_t, PDFValueArray_t, true>(this, X);

	//Now return it
	return pdfsGlobal;
}; //ENd: Evaluate for many samples without error


/*
 * =====================
 * Eigen based functions
 */

yggdrasil_DETree_t::PDFValueArrayEigen_t
yggdrasil_DETree_t::evaluateSamplesAt_Eigen(
	const SampleArray_t&	X,
	const bool 		beQuiet)
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};
	if(Size_t(this->m_dataSpace.nDims()) != X.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the container and the rtree does not agree.");
	};

	//The real check if the samples are valid or not is done inside the eval helper.
	//Hower we will do here a check that the assers provide the early fail feture
	yggdrasil_assert([](const SampleArray_t& X) -> bool
			{
			  const Size_t N = X.nSamples(); for(Size_t i = 0; i != N; ++i)
			    {if(X.getSample(i).isValid() == false) {return false;};};
			  return true;
			}(X));

	//The return value, this allows nrvo
	PDFValueArrayEigen_t pdfsGlobal;

	/*
	 * Make the decision if we use the no error or the error behaviour
	 * of the querry fucntion
	 */
	if(beQuiet == true)
	{
		//This is the no error behaviour, menaing
		//that a sample that lies outsode the domain is set to zero
		pdfsGlobal = ygInteranl_evaluateContainer<SampleArray_t, PDFValueArrayEigen_t, true>(this, X);
	}
	else
	{
		//This is teh error behaviour, meaning that
		//a sample outside the data domain will cause an error
		pdfsGlobal = ygInteranl_evaluateContainer<SampleArray_t, PDFValueArrayEigen_t, false>(this, X);
	};


	//Now return it
	return pdfsGlobal;
}; //End: eigen evaluate on sample array


yggdrasil_DETree_t::PDFValueArrayEigen_t
yggdrasil_DETree_t::evaluateSamplesAt_Eigen(
	const SampleList_t&	X,
	const bool 		beQuiet)
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};
	if(Size_t(this->m_dataSpace.nDims()) != X.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the container and the rtree does not agree.");
	};

	//The real check if the samples are valid or not is done inside the eval helper.
	//Hower we will do here a check that the assers provide the early fail feture
	yggdrasil_assert([](const SampleList_t& X) -> bool
			{
			  const Size_t N = X.nSamples(); for(Size_t i = 0; i != N; ++i)
			    {if(X.getSample(i).isValid() == false) {return false;};};
			  return true;
			}(X));

	//The return value, this allows nrvo
	PDFValueArrayEigen_t pdfsGlobal;

	/*
	 * Make the decision if we use the no error or the error behaviour
	 * of the querry fucntion
	 */
	if(beQuiet == true)
	{
		//This is the no error behaviour, menaing
		//that a sample that lies outsode the domain is set to zero
		pdfsGlobal = ygInteranl_evaluateContainer<SampleList_t, PDFValueArrayEigen_t, true>(this, X);
	}
	else
	{
		//This is teh error behaviour, meaning that
		//a sample outside the data domain will cause an error
		pdfsGlobal = ygInteranl_evaluateContainer<SampleList_t, PDFValueArrayEigen_t, false>(this, X);
	};


	//Now return it
	return pdfsGlobal;
}; //End: eigen evaluate on sample list



yggdrasil_DETree_t::PDFValueArrayEigen_t
yggdrasil_DETree_t::evaluateSamplesAt_Eigen(
	const SampleCollection_t&	X,
	const bool 			beQuiet)
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};
	if(this->m_dataSpace.nDims() != X.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the container and the rtree does not agree.");
	};

	//The real check if the samples are valid or not is done inside the eval helper.
	//Hower we will do here a check that the assers provide the early fail feture
	yggdrasil_assert([](const SampleCollection_t& X) -> bool
			{
			  const Size_t N = X.nSamples(); for(Size_t i = 0; i != N; ++i)
			    {if(X.getSample(i).isValid() == false) {return false;};};
			  return true;
			}(X));

	//The return value, this allows nrvo
	PDFValueArrayEigen_t pdfsGlobal;

	/*
	 * Make the decision if we use the no error or the error behaviour
	 * of the querry fucntion
	 */
	if(beQuiet == true)
	{
		//This is the no error behaviour, menaing
		//that a sample that lies outsode the domain is set to zero
		pdfsGlobal = ygInteranl_evaluateContainer<SampleCollection_t, PDFValueArrayEigen_t, true>(this, X);
	}
	else
	{
		//This is teh error behaviour, meaning that
		//a sample outside the data domain will cause an error
		pdfsGlobal = ygInteranl_evaluateContainer<SampleCollection_t, PDFValueArrayEigen_t, false>(this, X);
	};


	//Now return it
	return pdfsGlobal;
}; //End: eigen evaluate on sample list





/*
 * ====================
 * Internal helper functions
 */



Real_t
yggdrasil_DETree_t::priv_evaluateSampleGlobal(
	const cSampleDescentRet_t& 	dRes)
 const
{
	//Test if it lies inside the data range
	if(dRes.first == nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to query a dRes that lies outside the data range.");
	}; //ENd outside the data range

	//The dRes must be fully splitted
	yggdrasil_assert(dRes.first->isFullySplittedLeaf() == true);
	yggdrasil_assert(dRes.first->hasParModel() == true);
	yggdrasil_assert(dRes.first->isParModelFitted() == true);

	//Ensure consistency of the sample
	yggdrasil_assert(dRes.second.isValid());
	yggdrasil_assert(dRes.second.nDims() == this->m_dataSpace.nDims());

	/*
	 * Evaluate the model at the sample location
	 */
	const ParModel_i* model = dRes.first->getParModel();

	//The model must be fully fitted
	yggdrasil_assert(model->allDimensionsFitted() == true);

	//Now evaluate the model; Note that this is in the rescalled root data space
	const Numeric_t pdfRootDataSpace = model->evaluateModelForSample(dRes.second, dRes.first->getDomain(), m_root->getMass());
	yggdrasil_assert(isValidFloat(pdfRootDataSpace) == true);
	yggdrasil_assert(pdfRootDataSpace >= 0.0);

	//Now we have to transf form it back to the original root data space.
	//for that we must devide by th e volume of the original data space.
	const Real_t volOfDom = m_dataSpace.getVol();
	yggdrasil_assert(isValidFloat(volOfDom) == true);
	yggdrasil_assert(volOfDom > 0.0);

	//Transform the PDF to the globals space
	const Real_t pdfGlobalSpace = pdfRootDataSpace / volOfDom;
	yggdrasil_assert(isValidFloat(pdfGlobalSpace) == true);
	yggdrasil_assert(pdfGlobalSpace >= 0.0);

	//Return it
	return pdfGlobalSpace;
}; //End: evaluate sample internbal Global




Real_t
yggdrasil_DETree_t::priv_evaluateSampleRootDS(
	const cSampleDescentRet_t& 	dRes)
 const
{
	//Test if it lies inside the data range
	if(dRes.first == nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to query a dRes that lies outside the data range.");
	}; //ENd outside the data range

	//The dRes must be fully splitted
	yggdrasil_assert(dRes.first->isLeaf());
	yggdrasil_assert(dRes.first->isFullySplittedLeaf() == true);
	yggdrasil_assert(dRes.first->hasParModel() == true);
	yggdrasil_assert(dRes.first->isParModelFitted() == true);

	//Ensure consistency of the sample
	yggdrasil_assert(dRes.second.isValid());
	yggdrasil_assert(dRes.second.nDims() == this->m_dataSpace.nDims());

	/*
	 * Evaluate the model at the sample location
	 */
	const ParModel_i* model = dRes.first->getParModel();
	yggdrasil_assert(model != nullptr);

	//The model must be fully fitted
	yggdrasil_assert(model->allDimensionsFitted() == true);

	//Now evaluate the model; Note that this is in the rescalled root data space
	const Numeric_t pdfRootDataSpace = model->evaluateModelForSample(dRes.second, dRes.first->getDomain(), m_root->getMass());

	yggdrasil_assert(isValidFloat(pdfRootDataSpace) == true);
	yggdrasil_assert(pdfRootDataSpace >= 0.0);

	return pdfRootDataSpace;
}; //End: evaluate internal for batch


template<
	class 	Container_t,
	class 	PDFVector_t,
	bool 	noErr
	>
PDFVector_t
ygInteranl_evaluateContainer(
	const yggdrasil_DETree_t* const 	self,
	const Container_t& 			X)
{
	using Tree_t = yggdrasil_DETree_t;

	if(self->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not evaluate on an invalid tree.");
	};

	//The numbers of the samples we have to transform
	const Size_t nSamples = X.nSamples();

	//The dimensions
	if(X.nDims() != self->m_dataSpace.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The number of dimensions in the sample and in the tree is not the same.");
	};

	//Test if we have to do something in the first place
	if(nSamples == 0)
	{
		//Nothing to do
		return PDFVector_t();	//Empty matrix, thys sytak is supported for both types
	}; //End if: nothing to do

	//Allocate the array for the computation.
	PDFVector_t pdfsGlobal(nSamples);	//We can not set the value, since eigen does not supports that

	//Test if the allocation has happended and was successull
	if(pdfsGlobal.size() != nSamples)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Allocation of the output array failed.");
	};

	//Compute auxilery value this is for the transforming form the
	//root data space to the global data space.
	const Real_t volOfDom = self->m_dataSpace.getVol(); 	yggdrasil_assert(isValidFloat(volOfDom) == true);
								yggdrasil_assert(volOfDom > 0.0);

	//Comute the inverse of the volume, this is to save some division
	const Real_t iVolOfDom = 1.0 / volOfDom;		yggdrasil_assert(isValidFloat(iVolOfDom));
								yggdrasil_assert(iVolOfDom > 0.0);

	//Now iterate through all the com samples and compute the results
#if defined(YGGDRASIL_OPENMP_PDF_EVAL) && YGGDRASIL_OPENMP_PDF_EVAL != 0
	#pragma omp parallel for default(shared)
#endif
	for(Size_t i = 0; i < nSamples; ++i)
	{
		//Load the sample, using the universal interface
		const Tree_t::Sample_t x = X.getSample(i);

		//This is the real test.
		//The other imput function (query fucntions on the tree will only
		//cgheck the validity of the sample with asserts.
		if(x.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("A sample that was passed is invalid.");
		};

		yggdrasil_assert(Size_t(x.nDims()) == X.nDims());	//This could be violated for some container
		yggdrasil_assert(std::all_of(x.cbegin(), x.cend(), [](const Numeric_t e) -> bool
						{return isValidFloat(e);}));

		//Search for the target domain
		const Tree_t::cSampleDescentRet_t node = self->priv_findElementOf(x);

		if(node.first == nullptr)
		{
			/*
			 * We are outside the domain
			 */
			if(noErr == true)
			{
				/*
				 * We also outside the domain samples
				 * we define them as zero
				 */

				//The case when we allow zero values
				yggdrasil_assert(self->m_dataSpace.isInside(x) == false);

				//The result is 0.0
				pdfsGlobal[i] = 0.0;
			}
			else
			{
				/*
				 * We do not allow samples outside the domain
				 */
				yggdrasil_assert(self->m_dataSpace.isInside(x) == false);

				//THere is a null pointer found, generate an exception
				throw YGGDRASIL_EXCEPT_InvArg("The " + std::to_string(i) + "th sampels lied outside the domain.");
			}; //End else: error handling
		}
		else
		{
			/*
			 * We are inside the domain
			 */

			//
			//Now evaluate the model in rescalled data space
			const Real_t pdfRootDataSpace = self->priv_evaluateSampleRootDS(node);

			//Check if the calculated pdf meets some creteria
			yggdrasil_assert(isValidFloat(pdfRootDataSpace) == true);
			yggdrasil_assert(pdfRootDataSpace >= 0.0);	//We allow zero.

			//Calculate the final value by applying the transformation
			const Real_t finalPDF = pdfRootDataSpace * iVolOfDom;		yggdrasil_assert(isValidFloat(finalPDF));

			//Now store the value, we will make a paranoid safty check
											yggdrasil_assert(i < pdfsGlobal.size());
			pdfsGlobal[i] = finalPDF;
		};//End if: handliong the inside case
	}; //End for(i):

	//Now return it, it should be moved
	return pdfsGlobal;
}; //ENd: Evaluate for many samples without error



YGGDRASIL_NS_END(yggdrasil)

