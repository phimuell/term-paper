/**
 * \file	tree/yggdrasil_treeNode_payload_split.hpp
 * \brief	This file implements the splitting functions of the payload.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>

//Include STD
#include <cmath>
#include <utility>


YGGDRASIL_NS_BEGIN(yggdrasil)


void
yggdrasil_DETNodePayload_t::priv_distributeSamples(
	const SubDomRange_t&		subDomRange,
	const IndexArray_t&		indexArray,
	Node_ptr 			rootNode,
	const Int_t 			nThreads)
{
	if(m_samples.nSamples() != indexArray.size())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The index array has " + std::to_string(indexArray.size()) + " samples, but *this has " + std::to_string(m_samples.nSamples()));
	};

	if(rootNode->m_payload != this)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The payload of the root node and *this are not the same.");
	};

	if(subDomRange.allSubDomainsValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The subranges where not valid.");
	};

	//We requere that the collection is valid
	if(m_samples.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The source collection is invalid.");
	};

	const Size_t numbersOfSamples = m_samples.nSamples();


	/*
	 * We now iterate through the dimensions
	 */
	for(int d = 0; d != m_samples.nDims(); ++d)
	{
		//This is the source array
		const DimensionArray_t& sourceDimArray = m_samples.getCDim(d);

		//This is because m_sample is invalid in later iterations
		if(numbersOfSamples != sourceDimArray.nSamples())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The number of expected samples in dimension " + std::to_string(d) + " is " + std::to_string(numbersOfSamples) + " but we found " + std::to_string(sourceDimArray.nSamples()) + ".");
		};

		//This is the counter for managing the totaly copied sample components
		Size_t thisDim_copiedValues = 0;

		//Now we iterate through the different ranges
		for(Size_t sRange = 0; sRange != subDomRange.nSubRanges(); ++sRange)
		{
			//This is the path to the new leaf
			const SubDomEnum_t thisPath = SubDomRange_t::CREATE_UNSAFE_SUBDOMAIN_ENUM(subDomRange, sRange);

			//this is the leaf
			Node_ptr thisLeaf = rootNode->decendTo(thisPath);

			//Test if this is truly a leaf
			if(thisLeaf->isLeaf() == false)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("There was an error, we do not have a leaf.");
			};

			//We requiere a payload
			if(thisLeaf->m_payload == nullptr)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("The leafe does not have an assoicated payload.");
			};

			//This is the range we currently handle
			const auto thisRange = subDomRange.getSubDomain(thisPath);

			//We do not allow that a new subrange is empty
			if(thisRange.isEmpty() == true)
			{
				//We allow empty ranges
				//throw YGGDRASIL_EXCEPT_LOGIC("One of the subranges was empty.");
			};

			//This is the length of the range we currently handle
			const auto thisRangeLength = thisRange.size();

			//Get the destination array
			DimensionArray_t& destinationDimArray = thisLeaf->m_payload->m_samples.getDim(d);

			//We want to be informed if there is a problem
			if(destinationDimArray.hasValidDimensions() == false)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("The destination array is invalid.");
			};

			//Allocating the array; This function is self protecting
			destinationDimArray.firstUsageInit(thisRangeLength, thisRangeLength + 10);

			if(destinationDimArray.nSamples() != thisRangeLength)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("The destination array has the wrong size. Its size is " + std::to_string(destinationDimArray.nSamples()) + ", but it should be " + std::to_string(thisRangeLength) + ".");
			};

			//This is the index we write to
			Size_t destinationIdx = 0;

			/*
			 * This is the actuall coping.
			 * again we use the unrolling in order to produce
			 * as mucch ILP as we can
			 */
			{
				//This is the range that contains the indexies
				//of the values, that belongs to this range
				const auto srcIdxRange = thisRange;

				//This is the unrolling factr
				const Size_t unrollFactor = 12;

				//Get differrent index
				const Size_t srcIdx_start = srcIdxRange.begin();
				const Size_t srcIdx_end   = srcIdxRange.end();	//This is the pass the end index
				yggdrasil_assert(srcIdx_start <= srcIdx_end);

				//this is the length of the current range
				const Size_t srcIdx_length = srcIdx_end - srcIdx_start;

				if(destinationDimArray.size() != srcIdx_length)
				{
					throw YGGDRASIL_EXCEPT_RUNTIME("The size of the destination array is not correct.");
				};


				//Test if we have something to copy
				if(srcIdx_length != 0)
				{
					yggdrasil_assert(sourceDimArray.getAssociatedDim() == destinationDimArray.getAssociatedDim());

					//Source array
					const auto& uSrcDimArray = sourceDimArray.internal_getInternalArray();
					const Numeric_t* const rawSrcDimArray = uSrcDimArray.data();

					//destination array
					auto& uDstDimArray = destinationDimArray.internal_getInternalArray();
					Numeric_t* const rawDstDimArray = uDstDimArray.data();

					//Rawpointer to the index array
					const Size_t* const rawIndexArray = indexArray.data();

					//We have something to copy
					const Size_t srcIdx_leftOver = srcIdx_length % unrollFactor;

					//Determine the end of the copy iteration with the unrolled lop
					const Size_t srcIdx_unrollEnd = srcIdx_end - srcIdx_leftOver;

					//Now we use an unrolled loop for the coping
					for(Size_t i = srcIdx_start; i != srcIdx_unrollEnd; i += unrollFactor)
					{
						//Load the index
						const Size_t idx_0 = rawIndexArray[i + 0];
						const Size_t idx_1 = rawIndexArray[i + 1];
						const Size_t idx_2 = rawIndexArray[i + 2];
						const Size_t idx_3 = rawIndexArray[i + 3];
						const Size_t idx_4 = rawIndexArray[i + 4];
						const Size_t idx_5 = rawIndexArray[i + 5];
						const Size_t idx_6 = rawIndexArray[i + 6];
						const Size_t idx_7 = rawIndexArray[i + 7];
						const Size_t idx_8 = rawIndexArray[i + 8];
						const Size_t idx_9 = rawIndexArray[i + 9];
						const Size_t idx_10 = rawIndexArray[i + 10];
						const Size_t idx_11 = rawIndexArray[i + 11];
						yggdrasil_assert((i + 11) < indexArray.size());

						//Load the values from the src array
						const Numeric_t x_0 = rawSrcDimArray[idx_0];
						const Numeric_t x_1 = rawSrcDimArray[idx_1];
						const Numeric_t x_2 = rawSrcDimArray[idx_2];
						const Numeric_t x_3 = rawSrcDimArray[idx_3];
						const Numeric_t x_4 = rawSrcDimArray[idx_4];
						const Numeric_t x_5 = rawSrcDimArray[idx_5];
						const Numeric_t x_6 = rawSrcDimArray[idx_6];
						const Numeric_t x_7 = rawSrcDimArray[idx_7];
						const Numeric_t x_8 = rawSrcDimArray[idx_8];
						const Numeric_t x_9 = rawSrcDimArray[idx_9];
						const Numeric_t x_10 = rawSrcDimArray[idx_10];
						const Numeric_t x_11 = rawSrcDimArray[idx_11];

						yggdrasil_assert(idx_0 < sourceDimArray.size());
						yggdrasil_assert(idx_1 < sourceDimArray.size());
						yggdrasil_assert(idx_2 < sourceDimArray.size());
						yggdrasil_assert(idx_3 < sourceDimArray.size());
						yggdrasil_assert(idx_4 < sourceDimArray.size());
						yggdrasil_assert(idx_5 < sourceDimArray.size());
						yggdrasil_assert(idx_6 < sourceDimArray.size());
						yggdrasil_assert(idx_7 < sourceDimArray.size());
						yggdrasil_assert(idx_8 < sourceDimArray.size());
						yggdrasil_assert(idx_9 < sourceDimArray.size());
						yggdrasil_assert(idx_10 < sourceDimArray.size());
						yggdrasil_assert(idx_11 < sourceDimArray.size());

						//Write the data
						rawDstDimArray[destinationIdx + 0] = x_0;
						rawDstDimArray[destinationIdx + 1] = x_1;
						rawDstDimArray[destinationIdx + 2] = x_2;
						rawDstDimArray[destinationIdx + 3] = x_3;
						rawDstDimArray[destinationIdx + 4] = x_4;
						rawDstDimArray[destinationIdx + 5] = x_5;
						rawDstDimArray[destinationIdx + 6] = x_6;
						rawDstDimArray[destinationIdx + 7] = x_7;
						rawDstDimArray[destinationIdx + 8] = x_8;
						rawDstDimArray[destinationIdx + 9] = x_9;
						rawDstDimArray[destinationIdx + 10] = x_10;
						rawDstDimArray[destinationIdx + 11] = x_11;

						//Incrementing the destination index
						destinationIdx += unrollFactor;

						//Test
						//It can also be equal
						yggdrasil_assert(destinationIdx <= destinationDimArray.nSamples());
					}; //End for(i): inrolled loop

					//Handling the left overs
					if(srcIdx_leftOver != 0)
					{
						for(Size_t i = srcIdx_unrollEnd; i != srcIdx_end; ++i)
						{
							//load the index
							yggdrasil_assert(i < indexArray.size());
							const Size_t idx_i = rawIndexArray[i];

							//Load the bvalue we want to copy
							yggdrasil_assert(idx_i < sourceDimArray.size());
							const Numeric_t x_i = rawSrcDimArray[idx_i];

							//Write it back
							yggdrasil_assert(destinationIdx < destinationDimArray.size());
							rawDstDimArray[destinationIdx] = x_i;

							//Increment the index
							destinationIdx += 1;
						}; //ENd for(i): coping
					}; //End if Handle left over
				}; //End if: we have something to copy
			}; //End: coping range

			if(destinationIdx != thisRangeLength)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("The destination index counter has the wrong value. Its value is " + std::to_string(destinationIdx) + ", but it should be " + std::to_string(thisRangeLength) + ".");
			};

			//Increase the counter of copied values
			thisDim_copiedValues += destinationIdx;
		}; //End for(sRange): iterate through the range

		//Test if we have copied the amount we need
		if(thisDim_copiedValues != sourceDimArray.nSamples())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The amount of copied samples in dimenison " + std::to_string(d) + " is wrong. It should be " + std::to_string(sourceDimArray.nSamples()) + " but it was " + std::to_string(thisDim_copiedValues) + ".");
		};


		/*
		 * Now we clear the dimension we have handled
		 */
		SampleCollection_t::DELETE_SAMPLES_FROM_COLLECTION_SINGLE_DIMENSION(m_samples, d);
	}; //End for(d): iterating through the dimension


	/*
	 * Now we perform some validy tests.
	 * Some of them where alredy necessary before, so we do not rewpet them.
	 */
	//Now we iterate through the different ranges
	for(Size_t sRange = 0; sRange != subDomRange.nSubRanges(); ++sRange)
	{
		//This is the path to the new leaf
		const SubDomEnum_t thisPath = SubDomRange_t::CREATE_UNSAFE_SUBDOMAIN_ENUM(subDomRange, sRange);

		//this is the leaf
		Node_ptr thisLeaf = rootNode->decendTo(thisPath);

		//Test if this is truly a leaf
		if(thisLeaf->isLeaf() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("There was an error, we do not have a leaf.");
		};

		//Test if the samples are valid
		if(thisLeaf->m_payload->m_samples.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The samples of the subsplit " + std::to_string(thisPath.getWay()) + " are invalid.");
		};

		/*
		 * Do to the rescalling in a preior function.
		 * it is quite useless to make a test, since it was already in the
		 * unit interval.
		 * We do not rescale the data here, because it would make stuff realy
		 * complicated, and we know thathis code worked before
		 * the intrroduction of teh rescalling.
		 */
	}; //End for(sRange): iterate through the range


	/*
	 * We have now successfully distributed the samples
	 */
	return;
}; //End: ditribute the samples




void
yggdrasil_DETNodePayload_t::priv_populatePayload(
	const yggdrasil_DETNodePayload_t* const 	rootPayload,
	TestResult_ptr 					testResults,
	SubDomEnum_t 					splitThusFar,
	IndexArray_cIt 					indexArray_start,
	IndexArray_cIt 					indexArray_end,
	const HyperCube_t&				thisDomain)
{
	/*
	 * MASS
	 */
	m_mass 		= ::std::distance(indexArray_start, indexArray_end);
	m_dirtyMass 	= m_mass;

	/*
	 * Dirty
	 * By definition a split is not dirty
	 */
	m_isDirty = false;


	/*
	 * Domain
	 */
	if(thisDomain.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The new domain is not valid.");
	};
	this->m_domain = thisDomain;
	yggdrasil_assert(thisDomain.isValid());


	/*
	 * Copy the tests, They will be copied empty
	 */
	if(this->m_indepTest != nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The independcence test of the payload is not null.");
	};

	if(this->m_GOFTest != nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The pointer of the GOF test is not null");
	};
	if(this->m_parModel != nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The pointer to the parameteric model is not empty.");
	};

	return;
}; //End: poipulate Payload


void
yggdrasil_DETNodePayload_t::priv_prepareSplits(
	const SplitSequence_t& 		splitSequence,
	TestResult_ptr 			testResults)
{
	/*
	 * Test
	 */
	// *this must be a leaf
	if(m_nodeState != NodeState_e::Leaf)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this is not a leaf");
	};

	if(m_dirtyMass != m_samples.nSamples())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dirty mass " + std::to_string(m_dirtyMass) + " and the stored samples, " + std::to_string(m_samples.nSamples()) + " are not the same.");
	};

	/*
	 * AFFECTS
	 */


	/*
	 * Update the mass
	 */
	m_dirtyMass = m_samples.nSamples();
	m_mass      = m_dirtyMass;




	/*
	 * NOTICE:
	 * We do not change the state of *this.
	 */

	return;
}; //Ebnd prepare split


void
yggdrasil_DETNodePayload_t::priv_transformToSplit(
	const SingleSplit_t&	sSplit,
	Node_t* 		newSubTree)
{
	/*
	 * Tests
	 */
	if(m_nodeState != NodeState_e::Leaf)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this is not a leaf.");
	};

	//It is not possible to test if the split location lies inside the domain or not
	//The reason is that they both live in different scales.



	/*
	 * Affects
	 */
	//Change the state of this
	m_nodeState = NodeState_e::TrueSplit;

	return;

}; //End transformToSplit


void
yggdrasil_DETNodePayload_t::priv_setToLeaf(
		cNode_ptr 	rootSplitNode)
{
	/*
	 * Tests
	 */
	if(m_nodeState != NodeState_e::INVALID)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The state of the ndoe is not invalid.");
	};

	//It is not possible to test if the split location lies inside the domain or not
	//The reason is that they both live in different scales.


	/*
	 * Actions
	 */

	//Set *this to a leaf
	m_nodeState = NodeState_e::Leaf;

	/*
	 * We must now allocate the model
	 *
	 * Since the tests and the model is only needed in leafs and
	 * true splits it makes sense to copy them here.
	 */
	if(m_parModel != nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model is allready allocated.");
	};
	m_parModel = rootSplitNode->m_payload->m_parModel->creatOffspring();

	if(m_indepTest != nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The independence test not allocated.");
	};
	m_indepTest = rootSplitNode->m_payload->m_indepTest->creatOffspring();

	if(m_GOFTest != nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The GOF test is not allocated.");
	};
	m_GOFTest = rootSplitNode->m_payload->m_GOFTest->creatOffspring();

	return;
}; //End: setToLeaf




void
yggdrasil_DETNodePayload_t::priv_setToIndirectSplit(
	const SingleSplit_t&	sSplit)
{
	/*
	 * Tests
	 */
	if(m_nodeState != NodeState_e::INVALID)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The state of the ndoe is not invalid.");
	};

	//It is not possible to test if the split location lies inside the domain or not
	//The reason is that they both live in different scales.

	/*
	 * Actions
	 */

	//Set *this to a leaf
	m_nodeState = NodeState_e::ImplicitSplit;

	/*
	 * Since this is an implicit node
	 * The payload and the test has not to be allocated.
	 */

	return;
}; //End: setToSplit



YGGDRASIL_NS_END(yggdrasil)

