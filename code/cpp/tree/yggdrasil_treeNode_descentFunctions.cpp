/**
 * \file	tree/yggdrasil_treeNode.cpp
 * \brief	This file implements the general function of the node.
 *
 * At least the one that are not implemented directly inside the header.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>

//Include STD
#include <cmath>
#include <utility>




YGGDRASIL_NS_BEGIN(yggdrasil)

yggdrasil_DETNode_t::Node_ptr
yggdrasil_DETNode_t::decendTo(
	const SubDomEnum_t&	targetDomain)
{
	//Thest if this is valid
	yggdrasil_assert(this->isOKNode());

	//Test if we are a leaf
	if(this->isLeaf() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Called the nextNextNode function on a leaf.");
	}; //End if: Leaf Handling

	Node_ptr currentNode = this;
	for(int d = 0; d != targetDomain.getDepth(); ++d)
	{
		yggdrasil_assert(currentNode != nullptr);

		//We have to test if we can decend further
		if(currentNode->isLeaf() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to decend a leaf.");
		};

		//Decend
		if(targetDomain.wasLeft(d) == true)
		{
			currentNode = currentNode->getLeftChild();
		}
		else
		{
			currentNode = currentNode->getRightChild();
		};
	}; //End for(d): the decend path

	return currentNode;
}; //End: decendTo



yggdrasil_DETNode_t::cNode_ptr
yggdrasil_DETNode_t::decendTo(
	const SubDomEnum_t&	targetDomain)
 const
{
	//Thest if this is valid
	yggdrasil_assert(this->isOKNode());

	//Test if we are a leaf
	if(this->isLeaf() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Called the nextNextNode function on a leaf.");
	}; //End if: Leaf Handling

	cNode_ptr currentNode = this;
	for(int d = 0; d != targetDomain.getDepth(); ++d)
	{
		yggdrasil_assert(currentNode != nullptr);

		//We have to test if we can decend further
		if(currentNode->isLeaf() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to decend a leaf.");
		};

		//Decend
		if(targetDomain.wasLeft(d) == true)
		{
			currentNode = currentNode->getLeftChild();
		}
		else
		{
			currentNode = currentNode->getRightChild();
		};
	}; //End for(d): the decend path

	return currentNode;
}; //End: decendTo



yggdrasil_DETNode_t::cNode_ptr
yggdrasil_DETNode_t::cdecendTo(
	const SubDomEnum_t&	targetDomain)
 const
{
	//Thest if this is valid
	yggdrasil_assert(this->isOKNode());

	//Test if we are a leaf
	if(this->isLeaf() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Called the nextNextNode function on a leaf.");
	}; //End if: Leaf Handling

	cNode_ptr currentNode = this;
	for(int d = 0; d != targetDomain.getDepth(); ++d)
	{
		//We have to test if we can decend further
		if(currentNode->isLeaf() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to decend a leaf.");
		};

		//Decend
		if(targetDomain.wasLeft(d) == true)
		{
			currentNode = currentNode->getLeftChild();
		}
		else
		{
			currentNode = currentNode->getRightChild();
		};
	}; //End for(d): the decend path

	return currentNode;
}; //End: decendTo


yggdrasil_DETNode_t::SampleDescentRet_t
yggdrasil_DETNode_t::decendTo(
	const Sample_t& 	s_)
{
	//Fitrst of all make some tests
	yggdrasil_assert(this->isOKNode());

	//Test if we are a leaf
	if(this->isLeaf() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Called the nextNextNode function on a leaf.");
	}; //End if: Leaf Handling

	if(s_.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The passed sample was invalid.");
	};
	if(s_.nDims() != this->m_payload->m_samples.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The sample and the done does not have the same dimension, the sample has dimension " + std::to_string(s_.nDims()) + " the node has " + std::to_string(this->nDims()));
	};

	if(s_.isInsideUnit() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The position that was passed to the descent function was not inside the unit hypercube.");
	};


	//Now we can make a copy of the argument, this is needed
	//since we have to actively rescalling it while we descnet.
	Sample_t s(s_);

	//This is the current node that we handle
	Node_ptr currentNode = this;
	Node_ptr parentNode  = nullptr;


	//We descend until we found a leaf
	while(currentNode->isLeaf() == false)
	{
		yggdrasil_assert(currentNode != parentNode);
		yggdrasil_assert(currentNode->isLeaf() == false);
		yggdrasil_assert(currentNode->isOKNode() == true);
		yggdrasil_assert(currentNode != nullptr);

		//Now we get the split poisition and the split diemensioon of the current node
		const Int_t     currSplitDim = currentNode->getSplitDimension();
		const Numeric_t currSplitPos = currentNode->getSplitPosition();

		//This is the value of the sample in the dimension
		const Numeric_t samplePosInSplitDim = s.at(currSplitDim);

		//Now we check to whgich side we have to go
		if(samplePosInSplitDim < currSplitPos)
		{
			/*
			 * We have to go to the left side
			 */

			//Remap the sample
			s[currSplitDim] = samplePosInSplitDim / currSplitPos;

			//Make the parent node propagate
			parentNode = currentNode;

			//Get the next sample
			currentNode = currentNode->getLeftChild();

			yggdrasil_assert(currentNode != parentNode);
		}
		else
		{
			/*
			 * We habe to go to the right side
			 */

			//Semap the sample
			s[currSplitDim] = (samplePosInSplitDim - currSplitPos) / (1.0 - currSplitPos);

			//Make the parent node propagate
			parentNode = currentNode;

			//Get the next node
			currentNode = currentNode->getRightChild();

			yggdrasil_assert(currentNode != parentNode);
		}; //End if

		//We make some paranoia test
		yggdrasil_assert(s.isInsideUnit() == true);
	}; //End while: descnet

	//Paranoid
	yggdrasil_assert(currentNode != nullptr);
	yggdrasil_assert(currentNode->isLeaf() == true);
	yggdrasil_assert(currentNode->isOKNode() == true);

	//Now we can return the sample
	return ::std::make_pair(currentNode, ::std::move(s));
}; //End: descend fucntion


yggdrasil_DETNode_t::cSampleDescentRet_t
yggdrasil_DETNode_t::decendTo(
	const Sample_t& 	s_)
 const
{
	//Fitrst of all make some tests
	yggdrasil_assert(this->isOKNode());

	//Test if we are a leaf
	if(this->isLeaf() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Called the nextNextNode function on a leaf.");
	}; //End if: Leaf Handling

	if(s_.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The passed sample was invalid.");
	};
	if(s_.nDims() != this->m_payload->m_samples.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The sample and the done does not have the same dimension, the sample has dimension " + std::to_string(s_.nDims()) + " the node has " + std::to_string(this->nDims()));
	};

	if(s_.isInsideUnit() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The position that was passed to the descent function was not inside the unit hypercube.");
	};


	//Now we can make a copy of the argument, this is needed
	//since we have to actively rescalling it while we descnet.
	Sample_t s(s_);

	//This is the current node that we handle
	cNode_ptr parentNode  = nullptr;
	cNode_ptr currentNode = this;

	//We descend until we found a leaf
	while(currentNode->isLeaf() == false)
	{
		yggdrasil_assert(currentNode != parentNode);
		yggdrasil_assert(currentNode->isLeaf() == false);
		yggdrasil_assert(currentNode->isOKNode() == true);
		yggdrasil_assert(currentNode != nullptr);

		//Now we get the split poisition and the split diemensioon of the current node
		const Int_t     currSplitDim = currentNode->getSplitDimension();
		const Numeric_t currSplitPos = currentNode->getSplitPosition();

		//This is the value of the sample in the dimension
		const Numeric_t samplePosInSplitDim = s.at(currSplitDim);

		//Now we check to whgich side we have to go
		if(samplePosInSplitDim < currSplitPos)
		{
			/*
			 * We have to go to the left side
			 */

			//Remap the sample
			s[currSplitDim] = samplePosInSplitDim / currSplitPos;

			//Make the parent node propagate
			parentNode = currentNode;

			//Get the next sample
			currentNode = currentNode->getLeftChild();

			yggdrasil_assert(currentNode != parentNode);
		}
		else
		{
			/*
			 * We habe to go to the right side
			 */

			//Semap the sample
			s[currSplitDim] = (samplePosInSplitDim - currSplitPos) / (1.0 - currSplitPos);

			//Make the parent node propagate
			parentNode = currentNode;

			//Get the next node
			currentNode = currentNode->getRightChild();

			yggdrasil_assert(currentNode != parentNode);
		}; //End if

		//We make some paranoia test
		yggdrasil_assert(s.isInsideUnit() == true);
	}; //End while: descnet

	//Paranoid
	yggdrasil_assert(currentNode != nullptr);
	yggdrasil_assert(currentNode->isLeaf() == true);
	yggdrasil_assert(currentNode->isOKNode() == true);

	//Now we can return the sample
	return ::std::make_pair(currentNode, ::std::move(s));
}; //End: descend fucntion


YGGDRASIL_NS_END(yggdrasil)
