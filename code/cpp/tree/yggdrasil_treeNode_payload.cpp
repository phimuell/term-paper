/**
 * \file	tree/yggdrasil_treeNode_payload.cpp
 * \brief	This file implements the non splitting functions of the payload.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>

//Include STD
#include <cmath>
#include <utility>


YGGDRASIL_NS_BEGIN(yggdrasil)

//The destructor is defaulted
yggdrasil_DETNodePayload_t::~yggdrasil_DETNodePayload_t() = default;


yggdrasil_DETNodePayload_t::yggdrasil_DETNodePayload_t(
	const Int_t 	dims) :
	m_samples(dims),
	m_domain(dims),
	m_mass(0),
	m_dirtyMass(0),
	m_isDirty(false),
	m_nodeState(NodeState_e::INVALID),
	m_indepTest(nullptr),
	m_GOFTest(nullptr),
	m_parModel(nullptr)
{};

std::ostream&
yggdrasil_DETNodePayload_t::priv_printToStream(
	std::ostream&	o,
	std::string 	prefix,
	std::string 	inten,
	bool 		noS)
 const
{
	if(this->isLeafState() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can only print if a leaf.");
	};

	//Printing the domain, the true as arguments will use brackets
	o << prefix << "Domain = " << m_domain.print(true) << "\n";

	//Print the model
	o << prefix << m_parModel->print(prefix) << "\n";

	if(m_samples.nSamples() != 0)
	{

		o << prefix << "nSamples = " << m_samples.nSamples() << "\n";
		o << prefix << "Samples = [";

		if(noS == false)
		{
			for(Int_t i = 0; i != m_samples.nSamples(); ++i)
			{
				if(i != 0)
				{
					o << ", ";
				};

				o << "(";

				for(Int_t d = 0; d != m_samples.nDims(); ++d)
				{
					if(d != 0)
					{
						o << ",";
					};

					o << m_samples[d][i];
				}; //End for(d): going throgh the simension

				o << ")";
			}; //End for(i): iterate through samples
		}
		else
		{
			o << "...";
		};

		o << "]\n";
	}
	else
	{
		o << prefix << "Samples = [EMPTY]\n";
	};

	return o;
}; //End: priv_printToStream


void
yggdrasil_DETNodePayload_t::loadSamplesAndDomain(
	      SampleCollection_t&	samples,
	      HyperCube_t* const 	domain,
	const bool 			moveSamples)
{
	/*
	 * The model and test are set by the node object.
	 */

	//Test if teh samples are valid
	if(samples.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The samples to loads are invalid.");
	};

	//Test if the condition to load are met
	if(m_samples.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("THe samples of the payload are not valid.");
	};

	if(m_domain.isValid() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The domain of the payload is not invalid.");
	};

	if(m_samples.nDims() != m_domain.nDims())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The numbers of dimension are not the same inside the payload.");
	};
	if(m_samples.nDims() != samples.nDims())
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Wrong number of dimensions.");
	};

	if(m_samples.nSamples() != 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The payload has samples.");
	};

	if(samples.nSamples() == 0)
	{
		//We allow empty collections
		//throw YGGDRASIL_EXCEPT_LOGIC("The collection to load from are empty.");
	};

	if(samples.nDims() != m_samples.nDims())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The collection has the wrong dimension.");
	};

	if(isLeafState() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The payload is not in the leaf state.");
	};


	//Thi sbool saves if we have to estimate the bounds
	const bool estimateDomain = domain->isValid() ? false : true;

	/*
	 * It can happens that the domain is inavlid and the number of dimensiosn
	 * is also wrong, for that we create first an invalid domain, with the
	 * corrent dimensions, iff we have to estimate the domain
	 */
	if(estimateDomain == true)
	{
		//Create one that has the correct diomensions
		*domain = HyperCube_t(samples.nDims());
	}
	else
	{
		// In the case that the domain is valid, we have to check that it has the right
		// numbers of diomensions
		if(domain->nDims() != samples.nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The number of the dimension is incorrect. The domain has " + std::to_string(domain->nDims()) + ", but the samples has " + std::to_string(samples.nDims()));
		};
	};

	//Now copy
	for(Int_t d = 0; d != m_samples.nDims(); ++d)
	{
		/*
		 * Aquiere the samples
		 */
		if(moveSamples == false)
		{
			//Make a copy of the samples
			m_samples.getDim(d).copyAndLoadDimArray(samples.getDim(d));
		}
		else
		{
			//Move in the samples
			m_samples.getDim(d).moveLoadDimArray(samples.getDim(d));
		};

		/*
		 * Handle the domain
		 *
		 * This is either estimate the domain or check if the
		 * samples lies inside the domain.
		 */
		if(estimateDomain == true)
		{
			/*
			 * The domain must be estimated
			 */

			//The domain has to be invalid in the beginning
			yggdrasil_assert(domain->at(d).isValid() == false);

			//We now find the min and max element
			DimensionArray_t::const_iterator minElemIT, maxElemIT;
			std::tie(minElemIT, maxElemIT) = std::minmax_element(m_samples[d].cbegin(), m_samples[d].cend());

			const Numeric_t minElem = *minElemIT;
			const Numeric_t maxElem = *maxElemIT;

			//Now generate the interval, make the domain sligthly larger
			IntervalBound_t newBound(minElem, ::std::nextafter(maxElem, maxElem * (1 + 100 * Constants::EPSILON)));

			//Exchange the new bound
			domain->setIntervalTo(d, newBound);

			//After the excahnge the domain must be valid
			yggdrasil_assert(domain->at(d).isValid() == true);
		}
		else
		{
			// We do not have to estimate the domain
			// but this means we must check the data
			const IntervalBound_t thisIntterval = domain->at(d);

			//Perform the checking
			if(thisIntterval.isInside(m_samples.getDim(d).internal_getInternalArray()) == false)
			{
				throw YGGDRASIL_EXCEPT_InvArg("There are some samples which are not insdie teh given domain.");
			}; //End if: error
		}; //End if:
	}; //End for(d): coping the dimension

	//Now the domain is valid for sure
	if(domain->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The domain was not valid after estimating it. This si serious trouble.");
	};

	/*
	 * Rescalling
	 */
	for(Int_t d = 0; d != m_samples.nDims(); ++d)
	{
		//This is the domain we use
		const IntervalBound_t& thisInterval = domain->at(d);

		//Get a reference to the underling array
		auto& uDimArray = m_samples.getDim(d).internal_getInternalArray();

		//This is the function to contract it to the unit interval
		thisInterval.contractToUnitInPlace(uDimArray);
	}; //End for(d): mapping

	/*
	 * Set the domain of *this.
	 *
	 * As we havbe stated bfore this means that we have to set
	 * it to the unit cube
	 */
	m_domain = HyperCube_t::MAKE_UNIT_CUBE(m_samples.nDims());


	/*
	 * Set other values that are connected to the samples
	 */
	m_mass      = m_samples.nSamples();
	m_dirtyMass = m_mass;			//The dirty mass has to be the same as the normal mass
	m_isDirty   = false;

	return;
}; //End load smaple and domain





bool
yggdrasil_DETNodePayload_t::isValid()
 const
 noexcept
{
	//General tests
	if(m_isDirty == true)
	{
		if(m_dirtyMass < m_mass)
		{
			yggdrasil_assert(false);
			return false;
		};
	};

	if(NodeState_e::INVALID == m_nodeState)
	{
		yggdrasil_assert(false);
		return false;
	};

	//The samples must be valid
	if(m_samples.isValid() == false)
	{
		yggdrasil_assert(false);
		return false;
	};

	if(m_domain.isValid() == false)
	{
		yggdrasil_assert(false);
		return false;
	};

	if(m_domain.nDims() != m_samples.nDims())
	{
		yggdrasil_assert(false);
		return false;
	};


	/*
	 * Specific tests
	 */
	if(m_nodeState == NodeState_e::ImplicitSplit)
	{
		/*
		 * Implicit split
		 */

		if(m_samples.nSamples() != 0)
		{
			yggdrasil_assert(false);
			return false;
		};

		if(m_parModel != nullptr)
		{
			yggdrasil_assert(false);
			return false;
		};

		if(m_GOFTest != nullptr)
		{
			yggdrasil_assert(false);
			return false;
		};

		if(m_indepTest != nullptr)
		{
			yggdrasil_assert(false);
			return false;
		};
	}
	else if(m_nodeState == NodeState_e::TrueSplit)
	{
		/*
		 * True Split
		 */
		if(m_samples.nSamples() != 0)
		{
			yggdrasil_assert(false);
			return false;
		};

		if(m_parModel == nullptr)
		{
			yggdrasil_assert(false);
			return false;
		};

		if(m_GOFTest == nullptr)
		{
			yggdrasil_assert(false);
			return false;
		};

		if(m_indepTest == nullptr)
		{
			yggdrasil_assert(false);
			return false;
		};

	}
	else
	{
		/*
		 * Leaf
		 */
		yggdrasil_assert(this->isLeafState() == true);

		if(m_samples.nSamples() != m_dirtyMass)
		{
			yggdrasil_assert(false);
			return false;
		};

		if(m_parModel == nullptr)
		{
			yggdrasil_assert(false);
			return false;
		};

		if(m_GOFTest == nullptr)
		{
			yggdrasil_assert(false);
			return false;
		};

		if(m_indepTest == nullptr)
		{
			yggdrasil_assert(false);
			return false;
		};

#if 0
		//This test can not be done here, because it can be called from the
		//fitting function
		if(m_parModel->nSampleBase() != m_samples.nSamples())
		{
			yggdrasil_assert(false);
			return false;
		};
#endif

	}; //End test if:


	return true;
}; //End: isValid



bool
yggdrasil_DETNodePayload_t::isFullySplittedLeaf()
 const
{
	//Test if *this is a leaf
	if(this->isLeafState() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Called a leaf only method on a split.");
	};

	yggdrasil_assert(m_parModel  != nullptr);
	yggdrasil_assert(m_GOFTest   != nullptr);
	yggdrasil_assert(m_indepTest != nullptr);

	//Model must be associated and fully tested
	if(this->m_parModel->isAssociated() == false)
	{
		yggdrasil_assert(false);
		return false;
	};
	if(this->m_parModel->allDimensionsFitted() == false)
	{
		yggdrasil_assert(false);
		return false;
	};
	if(this->m_parModel->nSampleBase() != m_samples.nSamples())
	{
		yggdrasil_assert(false);
		return false;
	};

#if 0
	//We must have that both test are assoiated
	if(this->m_GOFTest->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The GOF-Test of teh node is not associated.");
	};
	if(this->m_indepTest->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The Indep-Test of teh node is not associated.");
	};
#else
	//We must have that both test are assoiated
	if(this->m_GOFTest->isAssociated() == false)
	{
		return false;
	};
	if(this->m_indepTest->isAssociated() == false)
	{
		return false;
	};
#endif

	//Make some test, they are actually alredy done in the splitting code,
	//but I like it that way
	yggdrasil_assert(this->m_GOFTest->allTestsDone() == true);
	yggdrasil_assert(this->m_indepTest->allTestsDone() == true);

	//Test if teh split sequences are empty
	if(this->m_GOFTest->areSplitsToPerform() == true)
	{
		//There are splits to perform so "this can not be fully splitted yet
		return false;
	};
	yggdrasil_assert(this->m_GOFTest->computeSplitSequence().isEmpty());

	if(this->m_indepTest->areSplitsToPerform() == true)
	{
		//There are splits to do, so it can not be complete
		return false;
	};
	yggdrasil_assert(this->m_indepTest->computeSplitSequence().isEmpty());

	//Now we can return true, since all is done
	return true;
}; //End: isFullySplittedLeaf


bool
yggdrasil_DETNodePayload_t::isFullySplittedNode()
 const
{
	//Test if we are a node
	if(this->isLeafState() == true)
	{
		return this->isFullySplittedLeaf();
	};

	//Test if this is an indirect solit
	if(this->isIndirectSplitState() == true)
	{
		//Actually handled in the check integrity fucntion
		//but we will ensure that there is no allocated model and tests
		if(this->m_parModel != nullptr)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("An indirect split has a parameteric model.");
		};
		if(this->m_GOFTest != nullptr)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("An indirect split has a GOF test.");
		};
		if(this->m_indepTest != nullptr)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("An indirect split has an independence test.");
		};
		if(this->m_samples.nSamples() != 0)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("An indirect split has samples.");
		};
		if(this->m_domain.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("An indirect split has an invalid domain.");
		};

		return true;
	};//End: indirect spliot

	if(this->isTrueSplitState() == true)
	{
		if(this->m_parModel == nullptr)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME(" true split does not have a parameteric model.");
		};
		if(this->m_GOFTest == nullptr)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("A true split does not have a parameteric model.");
		};
		if(this->m_indepTest == nullptr)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("A true split does not habe an indpendence test.");
		};
		if(this->m_samples.nSamples() != 0)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("A true split has some samples.");
		};
		if(this->m_domain.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("A true split does not have an invalid domain");
		};

		//The model must be associated and full fuitted
		if(this->m_parModel->isAssociated() == false)
		{
			//This could be considered an error
			yggdrasil_assert(false);
			return false;
		};
		if(this->m_parModel->allDimensionsFitted() == false)
		{
			//This could be considered an error
			yggdrasil_assert(false);
			return false;
		};
		if(this->m_GOFTest->isAssociated() == false)
		{
			return false;
		};
		if(this->m_GOFTest->allTestsDone() == false)
		{
			//This is also a serious error
			yggdrasil_assert(false);
			return false;
		};

		if(this->m_GOFTest->areSplitsToPerform() == false)
		{
			/*
			 * The GOF test has accepted the NULL,
			 * so the independence test is now done.
			 */
			if(this->m_indepTest->isAssociated() == false)
			{
				//This could be considered an error
				yggdrasil_assert(false);
				return false;
			};
			if(this->m_indepTest->allTestsDone() == false)
			{
				//This could be considered an error
				yggdrasil_assert(false);
				return false;
			};

			//It is important that we have a problem if there are no splits to perform
			//In that case *this should not be a leaf
			if(this->m_indepTest->areSplitsToPerform() == false)
			{
				//This could be considered an error
				yggdrasil_assert(false);
				return false;
			};
		}; //End: The GOF test has accepted

		/*
		 * Everything is correct so we return true
		 */
		return true;
	}; //End: true split state


	/*
	 * If we are here, the we have serious problems
	 */
	throw YGGDRASIL_EXCEPT_RUNTIME("Reached a part of the code that should be unreachable. This indicate that *this has an unkown sate.");
}; //End: isFullySplittedNode()





YGGDRASIL_NS_END(yggdrasil)

