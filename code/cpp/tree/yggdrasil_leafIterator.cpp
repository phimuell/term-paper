/**
 * \file	tree/yggdrasil_leafIterator.cpp
 * \brief 	This function is a leaf iterator, it allows the iteration over all the leafs.
 *
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <util/yggdrasil_taggedPointer.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>
#include <tree/yggdrasil_treeNode.hpp>

// Include STD
#include <deque>

YGGDRASIL_NS_BEGIN(yggdrasil)


/*
 * This enum is internal and is used only for encoding the directions
 */
enum class YgInternal_Direction_e : unsigned char
{
	LEFT   = 1,
	BLATT  = 2,
	RIGHT  = 3,
};

yggdrasil_leafIterator_t::yggdrasil_leafIterator_t(
	cIntNode_ptr root)
 :
  m_path()
{
	//Test if the node is valid
	if(root == nullptr)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The root for construction is a nullptr.");
	};

	//We only allow valid nodes
	if(root->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The root node is not ok.");
	};

	if(root->isLeaf() == true)
	{
		/*
		 * The root is a leaf
		 */
		IntPathElement_t pathElem(root);
		pathElem.setTag(static_cast<unsigned char>(YgInternal_Direction_e::BLATT));

		//Inserting
		m_path.push_back(pathElem);

		//We are now finished
		//See: https://stackoverflow.com/questions/5255777/what-if-i-write-return-statement-in-constructor/5255839#5255839
		return;
	}
	else
	{
		/*
		 * We are not at a leaf so we must decend
		 */

		//Insert the root of the iterator into the path
		//DO not yet set the tag
		m_path.push_back(IntPathElement_t(root));

		while(m_path.back().get()->isLeaf() == false)
		{
			//It seams that the last descend operation did not bring us
			//to a leafe, so we must note that we have descended to the
			//left and do it again.
			m_path.back().setTag(static_cast<unsigned char>(YgInternal_Direction_e::LEFT));

			//Now we descend to the left
			//Not yet mark a tag
			IntPathElement_t nextStep(m_path.back().get()->cgetLeftChild());

			//insert it into the way
			m_path.push_back(nextStep);
		}; //End while: descend until we have found a leaf

		//When we are here, we must have a leaf and no tag
		yggdrasil_assert(m_path.back().get()->isLeaf() == true);
		yggdrasil_assert(m_path.back().hasTag() == false);

		//Now we set the tag to a leaf
		m_path.back().setTag(static_cast<unsigned char>(YgInternal_Direction_e::BLATT));

#ifndef NDEBUG
		for(int i = int(m_path.size()) - 2; i >= 0; --i)
		{
			if(static_cast<YgInternal_Direction_e>(m_path[i].getTag()) != YgInternal_Direction_e::LEFT)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("We have not descended to the left.");
			};
		}; //End for(i)

		if(static_cast<YgInternal_Direction_e>(m_path.back().getTag()) != YgInternal_Direction_e::BLATT)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The last piece in the path is not a leaf.");
		};
#endif
	}; //End else: we have to descend
}; //End building constructor:w



yggdrasil_leafIterator_t::yggdrasil_leafIterator_t()
 :
  m_path()
{};


yggdrasil_leafIterator_t::yggdrasil_leafIterator_t(
	const yggdrasil_leafIterator_t&)
 = default;


yggdrasil_leafIterator_t::yggdrasil_leafIterator_t(
	yggdrasil_leafIterator_t&&)
 = default;


yggdrasil_leafIterator_t&
yggdrasil_leafIterator_t::operator= (
	const yggdrasil_leafIterator_t&)
 = default;


yggdrasil_leafIterator_t&
yggdrasil_leafIterator_t::operator= (
	yggdrasil_leafIterator_t&&)
 = default;


yggdrasil_leafIterator_t::~yggdrasil_leafIterator_t()
 = default;

yggdrasil_leafIterator_t&
yggdrasil_leafIterator_t::nextLeaf()
{
	/*
	 * This is teh function that strongly requieres that
	 * the current head is a leaf so we will enforce that.
	 */
	if(m_path.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to increment leaf iterator which has reached the end.");
	};
	if(m_path.back().get()->isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The leaf iterator was supposed to be at a leaf, but the current head of the path is not a leaf.");
	};

	//Cal the safe implementation, we have checked the requirements
	return this->priv_safeIncrement();
}; //End nextLeaf

yggdrasil_leafIterator_t&
yggdrasil_leafIterator_t::priv_safeIncrement()
{
	if(isEnd() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to increment an invalid iterator.");
	};

	//It is guaranteed that we are always
	yggdrasil_assert(static_cast<YgInternal_Direction_e>(m_path.back().getTag()) == YgInternal_Direction_e::BLATT);

	//This is the safe increment function, so it is not guaranteed that this is a leaf
	//so the check is dangerous,
	//it is sufficent to test that it was once a leaf
	//yggdrasil_assert(m_path.back().get()->isLeaf() == true);

	//We now pop the head and then start to look at the parent
	//of the old head.
	m_path.pop_back();

	//It could be that oly a leaf was present, so m_path could now be empty
	if(m_path.empty() == true)
	{
		//We are already finished, we have reached the end
		return *this;
	};

	/*
	 * We have removed the head already, so we are not at a
	 * leaf, we must now accend until we find a
	 * node were we went to the left.
	 * There we must go to the right and descend left
	 * until we find a leaf.
	 */
	while(m_path.empty() == false)
	{
		bool stopAscend = false;

		yggdrasil_assert(m_path.back().get()->isLeaf() == false);

		// Now check how we must descend
		switch(static_cast<YgInternal_Direction_e>(m_path.back().getTag()))
		{
		  case YgInternal_Direction_e::LEFT:
			//We have found what we want, a node where we descended to the left.
			//We must now stop the ascending and then follow to the right.
		  	stopAscend = true;
		  break; //END: LEFT

		  case YgInternal_Direction_e::RIGHT:
		  	//We have found a node that points to the right so we must
		  	//accend one more

		  	//We must now pop the current head
		  	//This is like ascending
		  	m_path.pop_back();

		  	//We set it to false to make clear that we have to ascend further
		  	stopAscend = false;

		  break; //END: RIGHT

		  case YgInternal_Direction_e::BLATT:
			//This will not happens and indicates an error
			throw YGGDRASIL_EXCEPT_LOGIC("The ascending found a leaf, this is a serious error.");

		  break; //END: BLATT

		  default:
			throw YGGDRASIL_EXCEPT_LOGIC("An unknown value was found.");
		  break; //END: Default
		}; //End switch

		//Test if we must stop the ascending
		//This is equal to stop running the while loop.
		if(stopAscend == true)
		{
			break;
		};
	}; //End while: ascend until we find a left junction

	/*
	 * Now we have ascended until we have found a split where we turned to the left.
	 * Now we must there switch to the right and descend.
	 *
	 * However it could also be that we have completly emptied the path.
	 * In that case we must exited
	 * And *this has turned into the empty iterator
	 */
	if(m_path.empty() == true)
	{
		//We are finished, ince we have gotten to the end iterator
		return *this;
	}; //End if: we have emptieed the path


	/*
	 * Now we must switch to the right
	 */
	yggdrasil_assert(static_cast<YgInternal_Direction_e>(m_path.back().getTag()) == YgInternal_Direction_e::LEFT);
	yggdrasil_assert(m_path.back().get()->isLeaf() == false);

	//now we set the tag to the right
	m_path.back().setTag(static_cast<unsigned char>(YgInternal_Direction_e::RIGHT));

	/*
	 * We must make the first descend operation outside of the loop.
	 * The reason is that otherwhise we would again descend in the
	 * left subtree
	 *
	 * It is important that we do the classification,
	 * meaning setting the tag is done in the loop below
	 */
	{
		//This is the root of the right subtree
		IntPathElement_t rootOfRightSubtree(m_path.back().get()->cgetRightChild());

		//inserting it
		m_path.push_back(rootOfRightSubtree);
	}; //End of right descending


	/*
	 * Now we will descend to the left until we found a leaf
	 *
	 * It is important that we handle the classification in the loop.
	 */
	while(m_path.back()->isLeaf() == false)
	{
		yggdrasil_assert(m_path.back().hasTag() == false);
		yggdrasil_assert(m_path.back().get()->isLeaf() == false);

		//We must mark that in the last iteration we
		//have descended to the left, this is the default.
		m_path.back().setTag(static_cast<unsigned char>(YgInternal_Direction_e::LEFT));
		yggdrasil_assert(static_cast<YgInternal_Direction_e>(m_path.back().getTag()) == YgInternal_Direction_e::LEFT);

		//Now descend to the left, but bot set the tag yet
		//This is done in the next iteration, or in the
		//case of a leaf after the leaf
		IntPathElement_t newElement = m_path.back()->cgetLeftChild();
		m_path.push_back(newElement);

		//When we are here then there should be no tag at all on the back
		//this will be set in the next iteration of this loop or outside
		yggdrasil_assert(m_path.back().hasTag() == false);
	}; //End while: descend to the left until a leaf is found

	/*
	 * We are now done with the left descending, sicne we have found a leaf.
	 */

	//Now we must set the tag to the leaf
	yggdrasil_assert(m_path.back()->isLeaf() == true);
	yggdrasil_assert(m_path.back().hasTag() == false);

	//now set the tag to a leaf
	m_path.back().setTag(static_cast<unsigned char>(YgInternal_Direction_e::BLATT));


	/*
	 * Now return the itewrator
	 */
	yggdrasil_assert(m_path.back().get()->isLeaf() == true);
	return *this;
}; //End save increment


bool
yggdrasil_leafIterator_t::isEnd()
 const
 noexcept
{
	/*
	 * The node is at the ned, if it is not possible to increment it
	 */
	return m_path.empty() == true ? true : false;
}; //End: isEnd


yggdrasil_leafIterator_t::Node_t
yggdrasil_leafIterator_t::access()
 const
{
	if(m_path.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to access an invalid iterator.");
	};
	yggdrasil_assert(static_cast<YgInternal_Direction_e>(m_path.back().getTag()) == YgInternal_Direction_e::BLATT);
	yggdrasil_assert(m_path.back().get()->isLeaf() == true);


	return Node_t(m_path.back().get());
}; //End: access



Size_t
yggdrasil_leafIterator_t::getDepth()
 const
{
	if(m_path.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to get the depth of an invalid iterator.");
	};

	//The Dept is one minus the length of the path.
	return (m_path.size() - 1);
};



/*
 * ==========================
 * This are the functions requered by boost
 */

yggdrasil_leafIterator_t::Node_t
yggdrasil_leafIterator_t::dereference()
 const
{
	if(m_path.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to access an invalid iterator.");
	};
	yggdrasil_assert(static_cast<YgInternal_Direction_e>(m_path.back().getTag()) == YgInternal_Direction_e::BLATT);
	if(m_path.back().get()->isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Leaf iterator tried to dereference a node which is not a leaf.");
	};

	//Initalize a facade
	return Node_t(m_path.back().get());
}; //End: dereference



void
yggdrasil_leafIterator_t::increment()
{
	/*
	 * This function is equivalent with the nextNode function.
	 *
	 * This means no safe increment is done.
	 */

	//Call the increemting function on this
	this->nextLeaf();

	//Return void
	return;
}; //End nextLeaf


bool
yggdrasil_leafIterator_t::equal(
	const yggdrasil_leafIterator_t&		other)
 const
 noexcept
{
	/*
	 * We could use operator== that is provided by the
	 * deque, but we will rool out our own.
	 * Since the tagged pointer only conpares
	 * the the address if operator== is applied.
	 */

	//Get the number of elements in *this and other
	//Notice here we have signed int
	const sSize_t thisPathLength  = this->m_path.size();
	const sSize_t otherPathLength = other.m_path.size();
	yggdrasil_assert(0 <= thisPathLength);
	yggdrasil_assert(0 <= otherPathLength);

	//THey must be equal, in order that the path can be the same
	if(thisPathLength != otherPathLength)
	{
		//They are not the same, so the paths, must be different
		return false;
	};

	/*
	 * We now have to go throgh the path and ompare if the tags and
	 * addresses are the same
	 * We will walk through the path from the end,
	 * sionce we have a rooted tree, it is very likely that
	 * at the beginning we will have the same nodes and tags
	 */
	for(sSize_t i = thisPathLength - 1; i != -1; --i)
	{
		yggdrasil_assert(i >= 0);

		//Load the two path elements form the path
		const auto& thisPathElement  = this->m_path.at(i);
		const auto& otherPathElement = other.m_path.at(i);

		yggdrasil_assert(otherPathElement.hasTag() == true);
		yggdrasil_assert(thisPathElement.hasTag() == true);
		yggdrasil_assert((i == (thisPathLength - 1)) ?
					static_cast<YgInternal_Direction_e>(otherPathElement.getTag()) == YgInternal_Direction_e::BLATT :
					(
					   static_cast<YgInternal_Direction_e>(otherPathElement.getTag()) == YgInternal_Direction_e::LEFT ||
					   static_cast<YgInternal_Direction_e>(otherPathElement.getTag()) == YgInternal_Direction_e::RIGHT
					)
				);
		yggdrasil_assert((i == (thisPathLength - 1)) ?
					static_cast<YgInternal_Direction_e>(thisPathElement.getTag()) == YgInternal_Direction_e::BLATT :
					(
					   static_cast<YgInternal_Direction_e>(thisPathElement.getTag()) == YgInternal_Direction_e::LEFT ||
					   static_cast<YgInternal_Direction_e>(thisPathElement.getTag()) == YgInternal_Direction_e::RIGHT
					)
				);


		//Compare if the two address are the same
		if(thisPathElement.get() == otherPathElement.get())
		{
			/*
			 * The two addresses are the same, so we must now compare
			 * the tags
			 */
			if(thisPathElement.getTag() != otherPathElement.getTag())
			{
				/*
				 * The two eleemts are different, so the two paths
				 * can not be the same
				 */
				return false;
			}; //End tag controll
		}
		else
		{
			/*
			 * The two address are not the same, so the path must
			 * be different at all.
			 */
			return false;
		} //End else: the addresses are different
	}; //ENd for(i):


	/*
	 * If we are here, this must mean that the two paths are the same.
	 * So we return true, since they are equal at all
	 */
	return true;
}; //End: equal











YGGDRASIL_NS_END(yggdrasil)







