/**
 * \brief	This file contains the code for the scallar rescalling.
 *
 * This code is heavaly unrolled to enable the pipeline.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_subDomainEnum.hpp>
#include <util/yggdrasil_splitSequence.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <interfaces/yggdrasil_interface_splitStrategy.hpp>

//Include STD
#include <cmath>
#include <tuple>
#include <algorithm>

/*
 * Aufklärung ist der Ausgang des Menschen aus seiner selbstverschuldeten Unmündigkeit.
 * Unmündigkeit ist das Unvermögen, sich seines Verstandes ohne Leitung eines anderen zu bedienen.
 * Selbstverschuldet ist diese Unmündigkeit, wenn die Ursache derselben nicht am Mangel des Verstandes,
 * sondern der Entschließung und des Muthes liegt, sich seiner ohne Leitung eines anderen zu bedienen.
 * Sapere aude! Habe Muth, dich deines eigenen Verstandes zu bedienen! ist also der Wahlspruch der Aufklärung.
 *
 * 		-- Kant; Beantwortung der Frage: Was ist Aufklärung?
 */



YGGDRASIL_NS_BEGIN(yggdrasil)


typedef yggdrasil_DETNode_t Node_t;

void
ygInternal_rescaleData_scalar(
	const Node_t::IndexArray_t::iterator 	indexArray_start,
	const Node_t::IndexArray_t::iterator 	indexArray_end,
	const Node_t::IndexArray_t::iterator 	indexArray_split,
	Node_t::DimensionArray_t&		dimArray,
	const Numeric_t 			splitPoint)
{
	/*
	 * Sorting for the cache
	 *
	 * We will now sort the left side of the array in accending order
	 * This will make it cache friendly, at least it makes it a bit less unfriendly.
	 *
	 * The right side will be sorted in ascending order.
	 * This is because we can hope that then something is already in the cache
	 */

	yggdrasil_assert(indexArray_start <= indexArray_split);
	yggdrasil_assert(indexArray_split <= indexArray_end);
	yggdrasil_assert(::std::all_of(indexArray_start, indexArray_split, [&dimArray, splitPoint](const Size_t x) -> bool
		{ const Numeric_t v = dimArray.at(x); return ((-0.001 < v) && (v < splitPoint));}));
	yggdrasil_assert(::std::all_of(indexArray_split, indexArray_end, [&dimArray, splitPoint](const Size_t x) -> bool
		{ const Numeric_t v = dimArray.at(x); return ((splitPoint <= v) && (v < 1.001));}));



	//Sort in accending order
	//::std::sort(indexArray_start, indexArray_split);

	//The right is now sorted in descending order
	//::std::sort(indexArray_split, indexArray_end, ::std::greater<Size_t>());

	//This is the underling dimensional array
	auto& underlingDimArray = dimArray.internal_getInternalArray();

	//This is the unrolling factor that we use
	const Size_t unrollFactor = 12;

#ifndef NDEBUG
	const yggdrasil_intervalBound_t unitInterval = yggdrasil_intervalBound_t::CREAT_UNIT_INTERVAL();
#endif

	/*
	 * Left Case
	 *
	 * They are allready at the correct start,
	 * they just need to be rescalled.
	 *
	 * If x_s is the splitting point, the new sample y_i,
	 * can be computed from the old sample by:
	 * 	y_i :=	\frac{x_i}{x_s}
	 *
	 * Since divisions are terible slow, we will
	 * use the following formula:
	 * 	y_i :=	a * x_i
	 *
	 * where a is given as
	 * 	s :=	\frac{1}{x_s}
	 */
	{

	//This is the rescalling factor, it is done in long double.
	const ::yggdrasil::Real_t left_reScallingFactor = ::yggdrasil::Real_t(1.0) / splitPoint;
	yggdrasil_assert(left_reScallingFactor >= 0.0);
	yggdrasil_assert(isValidFloat(left_reScallingFactor));

	//Calculating the number of samples in the left domain
	const Size_t left_nSamples = ::std::distance(indexArray_start, indexArray_split);

	//Now calculate the iteration bounds
	const Size_t left_leftOvers      = left_nSamples % unrollFactor;
	const Size_t left_unrolledIters  = left_nSamples - left_leftOvers; 	//This is not the number of iterations, but the bound

	//We must test if we have any samples to translate
	if(left_nSamples != 0)
	{

		//Get a pointer for the beginning of the indexing
		Size_t* left_idxArrayBase = &(*indexArray_start);


		for(Size_t i = 0; i != left_unrolledIters; i += unrollFactor)
		{
			//Load all the index of that iteration
			const Size_t idx_0  = left_idxArrayBase[i  + 0];
			const Size_t idx_1  = left_idxArrayBase[i  + 1];
			const Size_t idx_2  = left_idxArrayBase[i  + 2];
			const Size_t idx_3  = left_idxArrayBase[i  + 3];
			const Size_t idx_4  = left_idxArrayBase[i  + 4];
			const Size_t idx_5  = left_idxArrayBase[i  + 5];
			const Size_t idx_6  = left_idxArrayBase[i  + 6];
			const Size_t idx_7  = left_idxArrayBase[i  + 7];
			const Size_t idx_8  = left_idxArrayBase[i  + 8];
			const Size_t idx_9  = left_idxArrayBase[i  + 9];
			const Size_t idx_10 = left_idxArrayBase[i  + 10];
			const Size_t idx_11 = left_idxArrayBase[i  + 11];

			//Load the data
			const Numeric_t x_0 = underlingDimArray[idx_0];
			const Numeric_t x_1 = underlingDimArray[idx_1];
			const Numeric_t x_2 = underlingDimArray[idx_2];
			const Numeric_t x_3 = underlingDimArray[idx_3];
			const Numeric_t x_4 = underlingDimArray[idx_4];
			const Numeric_t x_5 = underlingDimArray[idx_5];
			const Numeric_t x_6 = underlingDimArray[idx_6];
			const Numeric_t x_7 = underlingDimArray[idx_7];
			const Numeric_t x_8 = underlingDimArray[idx_8];
			const Numeric_t x_9 = underlingDimArray[idx_9];
			const Numeric_t x_10 = underlingDimArray[idx_10];
			const Numeric_t x_11 = underlingDimArray[idx_11];

			//Perform checks
			yggdrasil_assert(underlingDimArray.at(idx_0) < splitPoint);
			yggdrasil_assert(underlingDimArray.at(idx_1) < splitPoint);
			yggdrasil_assert(underlingDimArray.at(idx_2) < splitPoint);
			yggdrasil_assert(underlingDimArray.at(idx_3) < splitPoint);
			yggdrasil_assert(underlingDimArray.at(idx_4) < splitPoint);
			yggdrasil_assert(underlingDimArray.at(idx_5) < splitPoint);
			yggdrasil_assert(underlingDimArray.at(idx_6) < splitPoint);
			yggdrasil_assert(underlingDimArray.at(idx_7) < splitPoint);
			yggdrasil_assert(underlingDimArray.at(idx_8) < splitPoint);
			yggdrasil_assert(underlingDimArray.at(idx_9) < splitPoint);
			yggdrasil_assert(underlingDimArray.at(idx_10) < splitPoint);
			yggdrasil_assert(underlingDimArray.at(idx_11) < splitPoint);

			//Perform the computation
			const Numeric_t y_0 = left_reScallingFactor * x_0;
			const Numeric_t y_1 = left_reScallingFactor * x_1;
			const Numeric_t y_2 = left_reScallingFactor * x_2;
			const Numeric_t y_3 = left_reScallingFactor * x_3;
			const Numeric_t y_4 = left_reScallingFactor * x_4;
			const Numeric_t y_5 = left_reScallingFactor * x_5;
			const Numeric_t y_6 = left_reScallingFactor * x_6;
			const Numeric_t y_7 = left_reScallingFactor * x_7;
			const Numeric_t y_8 = left_reScallingFactor * x_8;
			const Numeric_t y_9 = left_reScallingFactor * x_9;
			const Numeric_t y_10 = left_reScallingFactor * x_10;
			const Numeric_t y_11 = left_reScallingFactor * x_11;

			//Make tests
			yggdrasil_assert(unitInterval.isInside(y_0) || unitInterval.isCloseToUpperBound(y_0));
			yggdrasil_assert(unitInterval.isInside(y_1) || unitInterval.isCloseToUpperBound(y_1));
			yggdrasil_assert(unitInterval.isInside(y_2) || unitInterval.isCloseToUpperBound(y_2));
			yggdrasil_assert(unitInterval.isInside(y_3) || unitInterval.isCloseToUpperBound(y_3));
			yggdrasil_assert(unitInterval.isInside(y_4) || unitInterval.isCloseToUpperBound(y_4));
			yggdrasil_assert(unitInterval.isInside(y_5) || unitInterval.isCloseToUpperBound(y_5));
			yggdrasil_assert(unitInterval.isInside(y_6) || unitInterval.isCloseToUpperBound(y_6));
			yggdrasil_assert(unitInterval.isInside(y_7) || unitInterval.isCloseToUpperBound(y_7));
			yggdrasil_assert(unitInterval.isInside(y_8) || unitInterval.isCloseToUpperBound(y_8));
			yggdrasil_assert(unitInterval.isInside(y_9) || unitInterval.isCloseToUpperBound(y_9));
			yggdrasil_assert(unitInterval.isInside(y_10) || unitInterval.isCloseToUpperBound(y_10));
			yggdrasil_assert(unitInterval.isInside(y_11) || unitInterval.isCloseToUpperBound(y_11));

			//Write the computation back
			underlingDimArray[idx_0] = y_0;
			underlingDimArray[idx_1] = y_1;
			underlingDimArray[idx_2] = y_2;
			underlingDimArray[idx_3] = y_3;
			underlingDimArray[idx_4] = y_4;
			underlingDimArray[idx_5] = y_5;
			underlingDimArray[idx_6] = y_6;
			underlingDimArray[idx_7] = y_7;
			underlingDimArray[idx_8] = y_8;
			underlingDimArray[idx_9] = y_9;
			underlingDimArray[idx_10] = y_10;
			underlingDimArray[idx_11] = y_11;
		}; //End for(it): handle left domain


		/*
		* Handling the left left over
		*/
		if(left_leftOvers != 0)
		{
			for(Size_t i = left_unrolledIters; i != left_nSamples; ++i)
			{
				//Load the index we have to rescalle
				const auto idx_i = left_idxArrayBase[i];

				//Get the value
				const Numeric_t x_i = underlingDimArray[idx_i];

				//Make a small test
				yggdrasil_assert(underlingDimArray.at(idx_i) < splitPoint);

				//Rescalling
				const Numeric_t y_i = left_reScallingFactor * x_i;

				//Writing back
				yggdrasil_assert(unitInterval.isInside(y_i) || unitInterval.isCloseToUpperBound(y_i));
				underlingDimArray[idx_i] = y_i;
			}; //End for(it): handle left domain
		}; //End if: left leftover handling
	}; //ENd: handling left side
	}; //End scope of rescalling


	/*
	 * Now comes the right rescalling.
	 * Again let be x_i the samples that is in the right domain
	 * and let x_s be the split point.
	 * then the new sample value y_i can be computed in the following way.
	 * 	y_i :=	\frac{x_i - x_s}{1 - x_s}
	 *
	 * It is technically possible to write this as an fma.
	 * However I think that this will lower the numerical
	 * quality. Instead we will use the following form:
	 * 	y_i :=	\alpha * (x_i - x_s)
	 *
	 * where we set \alpha to:
	 * 	\alpha :=	\frac{1}{1 - x_s}
	 */
	{

	//Calculating the parameters
	const Numeric_t right_reScallingFactor = 1.0 / (1.0 - splitPoint);

	//Now we again calculate the iterations for the unrolling
	const Size_t right_nSamples      = ::std::distance(indexArray_split, indexArray_end);
	const Size_t right_leftOvers     = right_nSamples % unrollFactor;
	const Size_t right_unrolledIters = right_nSamples - right_leftOvers;


	//Test if there are any samples to handle
	if(right_nSamples != 0)
	{
		//This is the indexing array we need
		Size_t* right_idxArrayBase = &(*indexArray_split);

		for(Size_t i = 0; i != right_unrolledIters; i += unrollFactor)
		{
			//Load all the index of that iteration
			const Size_t idx_0  = right_idxArrayBase[i  + 0];
			const Size_t idx_1  = right_idxArrayBase[i  + 1];
			const Size_t idx_2  = right_idxArrayBase[i  + 2];
			const Size_t idx_3  = right_idxArrayBase[i  + 3];
			const Size_t idx_4  = right_idxArrayBase[i  + 4];
			const Size_t idx_5  = right_idxArrayBase[i  + 5];
			const Size_t idx_6  = right_idxArrayBase[i  + 6];
			const Size_t idx_7  = right_idxArrayBase[i  + 7];
			const Size_t idx_8  = right_idxArrayBase[i  + 8];
			const Size_t idx_9  = right_idxArrayBase[i  + 9];
			const Size_t idx_10 = right_idxArrayBase[i  + 10];
			const Size_t idx_11 = right_idxArrayBase[i  + 11];

			//Load the data
			const Numeric_t x_0 = underlingDimArray[idx_0];
			const Numeric_t x_1 = underlingDimArray[idx_1];
			const Numeric_t x_2 = underlingDimArray[idx_2];
			const Numeric_t x_3 = underlingDimArray[idx_3];
			const Numeric_t x_4 = underlingDimArray[idx_4];
			const Numeric_t x_5 = underlingDimArray[idx_5];
			const Numeric_t x_6 = underlingDimArray[idx_6];
			const Numeric_t x_7 = underlingDimArray[idx_7];
			const Numeric_t x_8 = underlingDimArray[idx_8];
			const Numeric_t x_9 = underlingDimArray[idx_9];
			const Numeric_t x_10 = underlingDimArray[idx_10];
			const Numeric_t x_11 = underlingDimArray[idx_11];

			//Make tests
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_0));
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_1));
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_2));
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_3));
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_4));
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_5));
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_6));
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_7));
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_8));
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_9));
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_10));
			yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_11));

			//First compute the x_i - x_s
			const Numeric_t z_0 = x_0 - splitPoint;
			const Numeric_t z_1 = x_1 - splitPoint;
			const Numeric_t z_2 = x_2 - splitPoint;
			const Numeric_t z_3 = x_3 - splitPoint;
			const Numeric_t z_4 = x_4 - splitPoint;
			const Numeric_t z_5 = x_5 - splitPoint;
			const Numeric_t z_6 = x_6 - splitPoint;
			const Numeric_t z_7 = x_7 - splitPoint;
			const Numeric_t z_8 = x_8 - splitPoint;
			const Numeric_t z_9 = x_9 - splitPoint;
			const Numeric_t z_10 = x_10 - splitPoint;
			const Numeric_t z_11 = x_11 - splitPoint;

			//Transform it
			const Numeric_t y_0 = z_0 * right_reScallingFactor;
			const Numeric_t y_1 = z_1 * right_reScallingFactor;
			const Numeric_t y_2 = z_2 * right_reScallingFactor;
			const Numeric_t y_3 = z_3 * right_reScallingFactor;
			const Numeric_t y_4 = z_4 * right_reScallingFactor;
			const Numeric_t y_5 = z_5 * right_reScallingFactor;
			const Numeric_t y_6 = z_6 * right_reScallingFactor;
			const Numeric_t y_7 = z_7 * right_reScallingFactor;
			const Numeric_t y_8 = z_8 * right_reScallingFactor;
			const Numeric_t y_9 = z_9 * right_reScallingFactor;
			const Numeric_t y_10 = z_10 * right_reScallingFactor;
			const Numeric_t y_11 = z_11 * right_reScallingFactor;

			//Make tests
			yggdrasil_assert(unitInterval.isInside(y_0) || unitInterval.isCloseToUpperBound(y_0));
			yggdrasil_assert(unitInterval.isInside(y_1) || unitInterval.isCloseToUpperBound(y_1));
			yggdrasil_assert(unitInterval.isInside(y_2) || unitInterval.isCloseToUpperBound(y_2));
			yggdrasil_assert(unitInterval.isInside(y_3) || unitInterval.isCloseToUpperBound(y_3));
			yggdrasil_assert(unitInterval.isInside(y_4) || unitInterval.isCloseToUpperBound(y_4));
			yggdrasil_assert(unitInterval.isInside(y_5) || unitInterval.isCloseToUpperBound(y_5));
			yggdrasil_assert(unitInterval.isInside(y_6) || unitInterval.isCloseToUpperBound(y_6));
			yggdrasil_assert(unitInterval.isInside(y_7) || unitInterval.isCloseToUpperBound(y_7));
			yggdrasil_assert(unitInterval.isInside(y_8) || unitInterval.isCloseToUpperBound(y_8));
			yggdrasil_assert(unitInterval.isInside(y_9) || unitInterval.isCloseToUpperBound(y_9));
			yggdrasil_assert(unitInterval.isInside(y_10) || unitInterval.isCloseToUpperBound(y_10));
			yggdrasil_assert(unitInterval.isInside(y_11) || unitInterval.isCloseToUpperBound(y_11));

			//Write the computation back
			underlingDimArray[idx_0] = y_0;
			underlingDimArray[idx_1] = y_1;
			underlingDimArray[idx_2] = y_2;
			underlingDimArray[idx_3] = y_3;
			underlingDimArray[idx_4] = y_4;
			underlingDimArray[idx_5] = y_5;
			underlingDimArray[idx_6] = y_6;
			underlingDimArray[idx_7] = y_7;
			underlingDimArray[idx_8] = y_8;
			underlingDimArray[idx_9] = y_9;
			underlingDimArray[idx_10] = y_10;
			underlingDimArray[idx_11] = y_11;
		}; //ENd for(i): unrolled right transfere

		//Now handeling possible left overs
		if(right_leftOvers != 0)
		{
			for(Size_t i = right_unrolledIters; i != right_nSamples; ++i)
			{
				//get the current index
				const Size_t idx_i = right_idxArrayBase[i];

				//load the sample
				const Numeric_t x_i = underlingDimArray[idx_i];
				yggdrasil_assert(splitPoint <= underlingDimArray.at(idx_i));

				//Perform the computation
				const Numeric_t z_i = x_i - splitPoint;
				const Numeric_t y_i = right_reScallingFactor * z_i;

				//Write back
				yggdrasil_assert(unitInterval.isInside(y_i) || unitInterval.isCloseToUpperBound(y_i));
				underlingDimArray[idx_i] = y_i;
			}; //End for(i): left over
		}; //End: right left over
	}; //End: handle the right side
	}; //End scope: right rescalling

	return;
}; //ENd rescalling







YGGDRASIL_NS_END(yggdrasil)
