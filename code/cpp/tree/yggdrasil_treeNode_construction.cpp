/**
 * \file	tree/yggdrasil_treeNode_construction.cpp
 * \brief	This file contains all the code that deals with the construction of the nodes.
 *
 * This are mainly the constructors.
 *
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>

//Include STD
#include <cmath>




YGGDRASIL_NS_BEGIN(yggdrasil)

yggdrasil_DETNode_t::yggdrasil_DETNode_t(
	      SampleCollection_t&	Samples,
	const ParModel_i*		parModel,
	const GOFTest_i*		gofTest,
	const IndepTest_i*		indepTest,
	      HyperCube_t* const	domain,
	const bool 			moveSamples)
 :
  yggdrasil_DETNode_t()
{
	if(Samples.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not construct a leaf from invalid samples.");
	};

	/*
	 * Note we can not check if the domain is valid.
	 * Since this constructer also handles the case where the domain size is estimated from data
	 */

	const Int_t Dim       = Samples.nDims();
	const Size_t nSamples = Samples.nSamples();

	//Allocating the payload
	this->priv_allocPayload(Dim);

	//Set the split to NAN to indicate that the node is a leaf
	m_splitPos = NAN;

	//Set the payload to a leaf
	m_payload->m_nodeState = Payload_t::NodeState_e::Leaf;

	//Clone the data
	m_payload->m_parModel  = parModel->creatOffspring();
	m_payload->m_GOFTest   = gofTest->creatOffspring();
	m_payload->m_indepTest = indepTest->creatOffspring();

	//COpy the data
	//Domain is estimated
	//It is sligthly larger than the data to ensure that all data is inside.
	//It will also do the rescalling into the unit interval.
	//
	//The domain is modified and this is propagated outwards
	m_payload->loadSamplesAndDomain(Samples, domain, moveSamples);

	/*
	 * We ensure that we have the unit interval
	 */
	yggdrasil_assert(Dim == m_payload->m_domain.nDims());
	yggdrasil_assert(m_payload->m_domain.isValid());
#ifndef NDEBUG
	for(const IntervalBound_t& b : m_payload->m_domain)
	{
		if(b.getLowerBound() != 0.0)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The lower bound is not the expected one.");
		};
		if(b.getUpperBound() != 1.0)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The upper bound is not the expected one.");
		};
	}; //End for(b): testing the domain
#endif
}; //End: building constructor





yggdrasil_DETNode_t::~yggdrasil_DETNode_t()
{
	//Only act if *this is valid
	if(this->isOKNode() == true)
	{
		/*
			* Deleting the pointer to the children.
			* Deleting the nullpointer does not have any sideefects
			*/
		delete [] m_child.get();

		//Seting it to zero
		//Remember that the set function also clears the tag
		m_child.setPointerTo(nullptr);


		/*
			* Delete the payload
			* This shouild always be allocated
			*/
		delete m_payload;
		m_payload = nullptr;

		//Since *this has now become invalid, we will set this to invalid
		//It is a bit useless.
		m_splitPos = INFINITY;
	}; //End if: isValid
}; //End destructor

yggdrasil_DETNode_t::yggdrasil_DETNode_t()
 noexcept :
  m_splitPos(INFINITY),	//By definition invalid
  m_child(nullptr),
  m_payload(nullptr)
{};


yggdrasil_DETNode_t::yggdrasil_DETNode_t(
	yggdrasil_DETNode_t&& o) :
  m_splitPos(o.m_splitPos),	//By definition invalid
  m_child(o.m_child),
  m_payload(o.m_payload)
{
	/*
	 * Now we have to invalidate the source node.
	 * This also involves of setting its member to zero.
	 */
	//Innvalidate
	o.m_splitPos = INFINITY;

	//Delte the pointers
	o.m_child.setPointerTo(nullptr);	//This will also reset the payload

	o.m_payload = nullptr;
}; //End move constructor

yggdrasil_DETNode_t&
yggdrasil_DETNode_t::operator= (
	yggdrasil_DETNode_t&& o)
{
	if(this->isOKNode() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to move a node into a vald one.");
	};
	yggdrasil_assert(m_child.isNull() == true);
	yggdrasil_assert(m_payload == nullptr);

	//Make copies of the source into *this
	this->m_splitPos = o.m_splitPos;
	this->m_child    = std::move(o.m_child);
	this->m_payload  = o.m_payload;


	//Invalidate the source
	o.m_splitPos = INFINITY;
	o.m_child.setPointerTo(nullptr);	//This will also reset the payload
	o.m_payload = nullptr;


	//Return *this
	return *this;
}; //End move assignment



YGGDRASIL_NS_END(yggdrasil)
