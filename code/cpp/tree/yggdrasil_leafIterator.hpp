#pragma once
/**
 * \file	tree/yggdrasil_leafIterator.hpp
 * \brief 	This function is a leaf iterator, it allows the iteration over all the leafs.
 *
 * The set of leaf that this iterator is able ot iterate over, is defined by the root.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <util/yggdrasil_taggedPointer.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>

// Include STD
#include <deque>

//Include boost
#include <boost/iterator/iterator_facade.hpp>

YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the node
class yggdrasil_DETNode_t;

class yggdrasil_nodeFacade_t;

/**
 * \class	yggdrasil_leafIterator_t
 * \brief	A class that allows the iteration over a set of leafs.
 *
 * This class exposed the set of leafs to the user.
 * It is important to note, that the class follows the spirit
 * of teh node yggdrasil_nodeFacade_t class.
 * This menas that it does not expose all the internal classes to the user.
 * This also means that the iterator only provides read-only access.
 *
 * The set of leaf that are accessable is defined by the node that is given
 * upon construction to the class.
 * It is also important that the order in which the leafs are accessed/visited
 * is unspecific.
 *
 * The iterator tries to mimic \e some asspect of the forward iterator.
 * Meaning that it only supports iteration in one direction.
 *
 * Notice that by designe, the end iterator is a read dead end, and
 * if you have reached that point, the iterator has outlifed his usefullness.
 *
 * Notice that this class is relatively big.
 *
 */
class yggdrasil_leafIterator_t : public ::boost::iterator_facade<
				yggdrasil_leafIterator_t,	//Derived
				yggdrasil_nodeFacade_t,		//Value
				::std::forward_iterator_tag,	//The category
				yggdrasil_nodeFacade_t,		//The reference type
				::std::ptrdiff_t		//Pointer difference type
			>
{
	/*
	 * =======================
	 * Typedef
	 */
	using IntNode_t 	  = ::yggdrasil::yggdrasil_DETNode_t;	//!< This is the internal node type
	using cIntNode_ptr 	  = const IntNode_t*;
	using IntPathElement_t 	  = yggdrasil_taggedPointer_t<const IntNode_t>;	//!< This is one ellemnt of the path in the tree
	using IntTreePath_t 	  = ::std::deque<IntPathElement_t>;		//!< This is the path we have done in the node

public:
	using Node_t 		  = yggdrasil_nodeFacade_t;		//!< This is the type we expose to the user.

	using SampleCollection_t  = yggdrasil_sampleCollection_t;	//!< This is the sample collection
	using DimensionArray_t 	  = SampleCollection_t::DimensionArray_t; //!< This is one diomension of the sample collection

	using Sample_t 		  = yggdrasil_sample_t;			//!< This is the sample type
	using HyperCube_t 	  = yggdrasil_hyperCube_t;		//!< This is the type for a hyper cube, aka domain bound
	using IntervalBound_t 	  = HyperCube_t::IntervalBound_t;	//!< This is the bound of a single interval



	/*
	 * ========================
	 * Constructors
	 *
	 * This class can be copied and moved.
	 * But it is not possible to construct
	 * without a node poiner.
	 *
	 * Also the building constructor is private.
	 * This means that only friends fo the class
	 * can construct it.
	 *
	 * The default constructor will construct an end iterator.
	 *
	 */
private:
	/**
	 * \brief	This is the building constructor.
	 *
	 * It takes a pointer to a node and construct
	 * itself.
	 * If the pointer is the null pointer an exception is trigered.
	 *
	 * \param  root		The node that is the root where the iterator starts to decend.
	 */
	yggdrasil_leafIterator_t(
		cIntNode_ptr);


public:
	/**
	 * \brief	The default constructor.
	 *
	 * This function will create an end iterator.
	 * Notice that it is pretty much useless.
	 */
	yggdrasil_leafIterator_t();


	/**
	 * \brief	The copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_leafIterator_t(
		const yggdrasil_leafIterator_t&);


	/**
	 * \brief	The move constructor.
	 *
	 * Is defaulted
	 */
	yggdrasil_leafIterator_t(
		yggdrasil_leafIterator_t&&);


	/**
	 * \brief	The copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_leafIterator_t&
	operator= (
		const yggdrasil_leafIterator_t&);


	/**
	 * \brief	The move assignment.
	 *
	 * Is allowed and defaulted.
	 */
	yggdrasil_leafIterator_t&
	operator= (
		yggdrasil_leafIterator_t&&);


	/**
	 * \brief	The destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_leafIterator_t();


	/*
	 * =====================
	 * Iterator fucntions
	 *
	 * This functions are the one that are making the iterator usefull.
	 */
public:
	/**
	 * \brief	This is the increment function.
	 *
	 * This function will advance \c *this to the next leaf.
	 *
	 * \return	It will return a reference to \c *this.
	 *
	 * \throw	If \c *this is the end iterator, or if an underling function throws.
	 */
	yggdrasil_leafIterator_t&
	nextLeaf();


	/**
	 * \brief	This function checks if \c *this is an end iterator.
	 *
	 * An end iterator can not be incremented any further.
	 * Also access the underling leaf is impossible.
	 */
	bool
	isEnd()
	 const
	 noexcept;


	/**
	 * \brief	This function returns the current leaf \c *this is resting on.
	 *
	 * This function does not return a reference to a real node or a pointer.
	 * Instead it returns a facade node to the user.
	 *
	 * \throw	This function throws if \c *this is the end iterator.
	 *
	 *
	 */
	Node_t
	access()
	 const;



	/*
	 * =========================
	 * Utility functions
	 */
public:
	/**
	 * \brief	Thsi function returns the depth of the current leaf, relative from the root.
	 *
	 * This function counts the number of descends that where performed.
	 * This means that the root was a leaf then the depth is zero.
	 *
	 * \throw 	If \c *this is invalid, the end iterator an exception is raised.
	 */
	Size_t
	getDepth()
	 const;


	/*
	 * =======================
	 * Interbal Functions
	 */
public:
	/**
	 * \brief	This function allows to increment an iterator that has become invalid.
	 *
	 * This function is provided to handle very special situations, that only apears
	 * in the inernal fitting code, thus it is private.
	 * When the fitting recursively descend it can happens that leafs are
	 * turned into splits, since they have to be splitted.
	 * In that case the iterator moints to a node which was once a leaf and
	 * for the iterator should be a leaf then it becomes invalid.
	 * For this situation this fucntion is provided.
	 * It ignores the case that it is not a leaf.
	 *
	 */
	yggdrasil_leafIterator_t&
	priv_safeIncrement();



	/*
	 * =======================
	 * Bost Facade functions
	 *
	 * These functions are requered to be implemented
	 * by the facade iterator implementation.
	 * They will be private.
	 */
private:
	//Make the core a friend, needed for accessing them
	friend class boost::iterator_core_access;


	/*
	 * \brief	This is the dereference fucntion.
	 *
	 * It is basically the same as the access fucntion
	 */
	Node_t
	dereference()
	 const;


	/**
	 * \brief	Thsi si the increment function.
	 *
	 * Notice this function does not perform the sve increment.
	 */
	void
	increment();


	/**
	 * \brief	This function tests if *this and other are the same.
	 *
	 * A compare operation is done by comparing the address and the tag
	 * of the tree path.
	 *
	 * This function is no except, but it will enforce and checks with assert.
	 */
	bool
	equal(
		const yggdrasil_leafIterator_t&		other)
	 const
	 noexcept;



	/*
	 * =================
	 * Friends
	 */
private:
	//The node itself is a friend
	friend class yggdrasil_DETNode_t;

	//The facade is also a friend
	friend class yggdrasil_nodeFacade_t;

	/*
	 * =================
	 * Private Member
	 */
private:
	IntTreePath_t 		m_path;		//!< This is the path we have done in the tree.
}; //End class: yggdrasil_leafIterator_t


YGGDRASIL_NS_END(yggdrasil)







