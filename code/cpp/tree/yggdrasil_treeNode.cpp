/**
 * \brief	This file implements the general function of the node.
 *
 * At least the one that are not implemented directly inside the header.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>

//Include STD
#include <cmath>




YGGDRASIL_NS_BEGIN(yggdrasil)



std::ostream&
yggdrasil_DETNode_t::priv_printToStream(
	std::ostream&	o,
	std::string 	prefix,
	std::string 	inten,
	bool 		noS)
 const
{
	//Test if we are a leaf
	if(this->isLeaf())
	{
		/*
		 * Handle the leaf stuff
		 */
		this->m_payload->priv_printToStream(o, prefix, inten, noS);
	}
	else
	{
		/*
		 * We are an inner node
		 */
		o << prefix
			<< (this->m_payload->isTrueSplitState() == true ? "True Split" : "Indirect Split") << " "
			<< "at x = " << m_splitPos << ", dim = " << (Int_t)m_child.getTag() << "; "	//Otherwhise casted to array
			<< "Mass = " << m_payload->m_mass << ", "
			<< "DirtyMass = " << m_payload->m_dirtyMass << ", "
			<< "isDirty = " << (m_payload->isDirty() == true ? "True" : "False")
			<< "\n";

		//Print the model only if one is there
		if(this->m_payload->m_parModel != nullptr)
		{
			o << prefix
				<< this->m_payload->m_parModel->print(prefix) << "\n";
		}
		else
		{
			if(this->m_payload->isIndirectSplitState() != true)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("A node that is not an indirect split does not won a model.");
			};
		};

		o
			<< prefix << "LEFT:\n"
			<< prefix << "{\n";
		//(m_child.get() + IDX_LEFT)->priv_printToStream(o, prefix + inten, inten);
		m_child[IDX_LEFT].priv_printToStream(o, prefix + inten, inten, noS);
		o
			<< prefix << "}\n";

		o
			<< prefix << "RIGHT:\n"
			<< prefix << "{\n";
		m_child[IDX_RIGHT].priv_printToStream(o, prefix + inten, inten, noS);
		o
			<< prefix << "}\n";
	};

	return o;
}; //End priv_printToStream


std::ostream&
yggdrasil_DETNode_t::printToStream(
	std::ostream&	o,
	bool 		noS)
 const
{
	return this->priv_printToStream(o, "", "\t", noS);
}; //End printToStream


bool
yggdrasil_DETNode_t::checkIntegrity()
 const
 noexcept
{
	//First *this must be ok
	if(this->isOKNode() == false)
	{
		std::abort();
		return false;
	};

	//*this must have a payload
	if(m_payload == nullptr)
	{
		yggdrasil_assert(false);
		return false;
	};

	//Tests that only applies to split
	if(this->isLeaf() == false)
	{
		//this is the dimesions to split
		const Int_t splitDim = m_child.getTag();

		//*thsi must satisfy some domensionional propoerties
		if(splitDim == Constants::YG_INVALID_DIMENSION)
		{
			yggdrasil_assert(false);
			return false;
		};
		if(splitDim >= Constants::YG_MAX_DIMENSIONS)
		{
			yggdrasil_assert(false);
			return false;
		};

		if(this == this->getLeftChild())
		{
			yggdrasil_assert(false);
			return false;
		};

		if(this == this->getRightChild())
		{
			yggdrasil_assert(false);
			return false;
		};


	}; //End if: Split checks

	//Now check the payload
	if(m_payload->isValid() == false)
	{
		//Note that this time the function is again called 'isValid'.
		yggdrasil_assert(false);
		return false;
	};

	return true;
}; //End check integrirty



bool
yggdrasil_DETNode_t::checkIntegrityRecursively()
 const
 noexcept
{
	//First check this
	if(this->checkIntegrity() == false)
	{
		//This is not integer
		yggdrasil_assert(false);
		return false;
	};

	//If this is not a leaf then go deeper
	if(this->isLeaf() == false)
	{
		if(this->getLeftChild()->checkIntegrityRecursively() == false)
		{
			yggdrasil_assert(false);
			return false;
		};
		if(this->getRightChild()->checkIntegrityRecursively() == false)
		{
			yggdrasil_assert(false);
			return false;
		};
	}; //End is not leaf


	//If we are here then everything must have returned true
	return true;
}; //ENd recursively node check


bool
yggdrasil_DETNode_t::isFullySplittedNodeRecursively()
 const
{
	//First call the node check function
	if(this->isFullySplittedNode() == false)
	{
		yggdrasil_assert(false);
		return false;
	};

	//If this is not a leaf we have to descent
	if(this->isLeaf() == false)
	{
		if(this->getLeftChild()->isFullySplittedNodeRecursively() == false)
		{
			yggdrasil_assert(false);
			return false;
		};
		if(this->getRightChild()->isFullySplittedNodeRecursively() == false)
		{
			yggdrasil_assert(false);
			return false;
		};
	}; //End: if: is not a leaf

	return true;
}; //ENd: fully splitted node recur


::std::tuple<Size_t, Size_t, Size_t>
yggdrasil_DETNode_t::getSubTreeComposition()
 const
{
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Called a function on an invalid node");
	};

	//In the case *this is a leaf, the result is trivial
	if(this->isLeaf() == true)
	{
		return ::std::make_tuple(0, 0, 1);
	};


	/*
	 * If this is not a leaf wemust first
	 * collect the information from our childrens
	 */
	Size_t trueSplitsNodes    = 0;
	Size_t indirectSplitNodes = 0;
	Size_t leafeNodes         = 0;

	//Handle the left side
	{
		Size_t trueSplitsNodesL    = 0;
		Size_t indirectSplitNodesL = 0;
		Size_t leafeNodesL         = 0;

		std::tie(trueSplitsNodesL, indirectSplitNodesL, leafeNodesL) = this->getLeftChild()->getSubTreeComposition();

		//Adding it together
		trueSplitsNodes    += trueSplitsNodesL;
		indirectSplitNodes += indirectSplitNodesL;
		leafeNodes         += leafeNodesL;
	}; //ENd left

	//Handle the right side
	{
		Size_t trueSplitsNodesR    = 0;
		Size_t indirectSplitNodesR = 0;
		Size_t leafeNodesR         = 0;

		std::tie(trueSplitsNodesR, indirectSplitNodesR, leafeNodesR) = this->getRightChild()->getSubTreeComposition();

		//Adding it together
		trueSplitsNodes    += trueSplitsNodesR;
		indirectSplitNodes += indirectSplitNodesR;
		leafeNodes         += leafeNodesR;
	}; //ENd left


	//Now we have to add what we are
	if(this->isTrueSplit() == true)
	{
		trueSplitsNodes += 1;
	};
	if(this->isIndirectSplit() == true)
	{
		indirectSplitNodes += 1;
	};
	yggdrasil_assert(this->isLeaf() == false);


	//Return the result
	return ::std::make_tuple(trueSplitsNodes, indirectSplitNodes, leafeNodes);
}; //ENd get Subtree composition


YGGDRASIL_NS_END(yggdrasil)

