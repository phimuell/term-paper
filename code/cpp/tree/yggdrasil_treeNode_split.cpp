/**
 * \brief	This file contains the implementation of the node, at least part of them
 * \file	tree/yggdrasil_treeNode_split.cpp
 *
 * Tis file contains the code that performs the split.
 * At least the driver code is located the functionality
 * is also distributed over the sub classes
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_subDomainEnum.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <util/yggdrasil_util.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <interfaces/yggdrasil_interface_splitStrategy.hpp>

//Include STD
#include <cmath>
#include <tuple>
#include <algorithm>

/*
 * "I am the Bone of my Sword
 *  Steel is my Body and Fire is my Blood.
 *  I have created over a Thousand Blades,
 *  Unknown to Death,
 *  Nor known to Life.
 *  Have withstood Pain to create many Weapons
 *  Yet those Hands will never hold Anything.
 *  So, as I Pray--
 *  Unlimited Blade Works"
 *
 * See zsh sourcecode
 *  Src/exec.c:348 (8ce9e20c4963b288cee9a76523c41579880aeeb1)
 */



YGGDRASIL_NS_BEGIN(yggdrasil)

typedef yggdrasil_DETNode_t Node_t;

/*
 * ========================
 * Free functions
 */


/**
 * \brief	This is a fucntion that performs the rescalling.
 *
 * The function is heavely unrolled to enable maximal throughput computation.
 * However it does perform sorting on its input which meight be not so good.
 * The reason is that the array can come in any order.
 * So loading is a problem, by first sorting it we enable the CPU to load the data.
 *
 * \param  indexArray_start	The start of the index array.
 * \param  indexArray_end	The past the end pointer th the index array.
 * \param  indeyArray_split	The pointer to the split position, also the first member of the right side.
 * \param  dimArray		The dimensional array.
 * \param  splitPoint		The numerical value where we split.
 */
extern
void
ygInternal_rescaleData_scalar(
	const Node_t::IndexArray_t::iterator 	indexArray_start,
	const Node_t::IndexArray_t::iterator 	indexArray_end,
	const Node_t::IndexArray_t::iterator 	indexArray_split,
	Node_t::DimensionArray_t&		dimArray,
	const Numeric_t 			splitPoint);


/**
 * \brief	This is a recursive function that performs the splitting.
 *
 * This function performs a descending operation while creating the new subtree.
 * It uses the splitThusFar member to keep track of its position on the tree.
 * It will populate the sub domain ranges.
 * It will also perfo
 * The parent node, is transformed into a split.
 *
 * \param  indexArray_start 	This is the start of the index array that the functions works on.
 * \param  indexArray_end 	This is the past the end iterator of the index array we work on.
 * \param  indexArray_base 	This is the base pointer of the index array.
 * \param  splitThusFar 	The list off all splits that are created thus far.
 * \param  splitSeq 		The list of splits that has to be performed; Consumed from the [0].
 * \param  testResult 		The pointer to the test result
 * \param  rootPayload 		The payload of the root; will be used to create the payloads of the under functions
 * \param  thisSingleSplit_OUT	This is the split that was produced by splitting the presented range.
 * 				 Is invalid in the case of a final node
 * \param  SplitDeterminer 	The object that implements the spliting
 * \param  parentDomain 	This is the size of the domain of the parent node.
 */
extern
Node_t*
ygInternal_createNewSubTree(
	Node_t::IndexArray_t::iterator		indexArray_start,
	Node_t::IndexArray_t::iterator		indexArray_end,
	const Node_t::IndexArray_t::iterator	indexArray_base,
	Node_t::SubDomEnum_t 			splitThusFar,
	Node_t::SubDomRange_t* 			subDomainRanges,
	const Node_t::SplitSequence_t&		splitSeq,
	Node_t::TestResult_ptr			testResult,
	Node_t::cPayload_ptr 			rootPayload,
	Node_t::cNode_ptr 			rootNodeWhereSplit,
	Node_t::cSplitDevice_ptr		splitDeterminer,
	const Node_t::HyperCube_t&		parentDomain);





/*
 * =========================
 * Class functions
 */

void
yggdrasil_DETNode_t::priv_hypothesisTesterAndSplitter_serial(
	cSplitDevice_ptr		splitDeterminer)
{
	//test if the node id okay
	if(this->isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not perform initial fitting on a not leaf.");
	};
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not okay.");
	};

	const Payload_t& tPl   = *(m_payload);
	ParModel_i* model      = tPl.m_parModel.get();
	GOFTest_i*  gofTest    = tPl.m_GOFTest.get();
	IndepTest_i* indepTest = tPl.m_indepTest.get();

	if(model == nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model is not allocated.");
	};
	if(model->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model is empty.");
	};
	if(model->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model is not fitted.");
	};
	if(gofTest == nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The GOF tester is not allocated.");
	};
	if(gofTest->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The GOF tester is not empty.");
	};
	if(indepTest == nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The independence test object is a nullpointer.");
	};
	if(indepTest->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The independence test is allready associated.");
	};


	/*
	 * Now we perform the tests
	 */

	//This is the split sequence that is used by the split code above.
	SplitSequence_t finalSplitSequence;
	yggdrasil_assert(finalSplitSequence.isEmpty());

	/*
	 * GOF-Tests
	 */
	{

		//Call the init function
		gofTest->preInitialHookInit(this);

		//We are now associated
		if(gofTest->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The initialization of the gof tester, did not associate it with *This.");
		};

		//Iterate over all dimension and test individually
		const GOFTest_i::DimList_t gofDims = gofTest->getAllTestKeys();
		const Size_t nTest = gofDims.size();

#if defined(YGGDRASIL_OPENMP_GOF) && YGGDRASIL_OPENMP_GOF != 0
		#pragma omp parallel for default(shared)
#endif
		for(Size_t i = 0; i < nTest; ++i)
		{
			const auto d = gofDims[i];

			if(gofTest->isDimensionTested(d) == true)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("The dimension " + std::to_string(d) + " was allready tested.");
			};

			//Perform the initial test
			const bool testSucc = gofTest->performInitailTest(model, this, d);

			if(testSucc == false)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("The gof test of dimension " + std::to_string(d) + " failed.");
			};

			if(gofTest->isDimensionTested(d) == false)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("Was not able to test dimension " + std::to_string(d));
			};

		}; //End for(d):

		//At this poiint all diomensions should be tested
		if(gofTest->allTestsDone() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The test if all dimensions where fitted, failed but each individual test has succeeded. This is strange.");
		};

		//Now finialize the test by calling the end fucntion
		gofTest->endOfTestProcess();
		yggdrasil_assert(gofTest->allTestsDone() == true);
	}; //End of scope: PERFORMING the GOF test, not the evaluation


	/*
	 * Compute the split sequence that is generated by the GOF test
	 *
	 * If the test is acepted then goes to the independence test
	 */
	if(gofTest->areSplitsToPerform() == true)
	{
		/*
		 * Theere are splits to perform, so we must write
		 * the sequnce to the final one
		 */
		finalSplitSequence = gofTest->computeSplitSequence();
		yggdrasil_assert(finalSplitSequence.isEmpty() == false);
	}
	else
	{
		/*
		 * The independence test has to be performed
		 * Since the the gof test was accepted
		 */

		//Set up the independence test
		indepTest->preInitialHookInit(this);
		if(indepTest->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The inizializationn of teh indepoendence test, did not bound it to the node.");
		};


		//Perform the precomputing
		const auto precompList = indepTest->getAllPrecomputeKeys();
		const Size_t nPreComp  = precompList.size();

#if defined(YGGDRASIL_OPENMP_INDEP_PRE) && YGGDRASIL_OPENMP_INDEP_PRE != 0
		#pragma omp parallel for default(shared)
#endif
		for(Size_t i = 0; i < nPreComp; ++i)
		{
			const auto d = precompList[i];

			yggdrasil_assert(indepTest->isAssociated() == true);
			indepTest->preComputeDimension(d, model, this);
			yggdrasil_assert(indepTest->isAssociated() == true);

		}; //ENd for(d): precomputing the dimension

		yggdrasil_assert(indepTest->isAssociated() == true);


		//Now perform the testing on all the interactions
		const auto toTestList    = indepTest->getAllTestKeys();
		const Size_t nToTestList = toTestList.size();

#if defined(YGGDRASIL_OPENMP_INDEP_TESTS) && YGGDRASIL_OPENMP_INDEP_TESTS != 0
		#pragma omp parallel for default(shared)
#endif
		for(Size_t i = 0; i < nToTestList; ++i)
		{
			const auto dimPair = toTestList[i];

			yggdrasil_assert(indepTest->isAssociated() == true);

			if(indepTest->isPairTested(dimPair) == true)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("The test of the pair " + dimPair.print() + " was already done.");
			};

			yggdrasil_assert(indepTest->isAssociated() == true);

			//Perform the test
			const bool testSucc = indepTest->performInitailTest(dimPair, this);
			yggdrasil_assert(indepTest->isAssociated() == true);

			if(testSucc == false)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("During the initial testing of pair " + dimPair.print() + " there was an error.");
			};
			yggdrasil_assert(indepTest->isAssociated() == true);

			if(indepTest->isPairTested(dimPair) == false)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("The test of the pair " + dimPair.print() + " did not fail, but it was not registered as done.");
			};
		}; //end for(dimPar): going through all the interactions

		yggdrasil_assert(indepTest->isAssociated() == true);


		//Now we have done everything so we must end the computation
		indepTest->endOfTestProcess();

		//If there are splits that have to be done, then do it now
		if(indepTest->areSplitsToPerform() == true)
		{
			yggdrasil_assert(finalSplitSequence.isEmpty() == true);

			//THere are splits to do, so we write them to the final destimation
			finalSplitSequence = indepTest->computeSplitSequence();
			yggdrasil_assert(finalSplitSequence.isEmpty() == false);
		}; //End: set the split sequance if needed
	}; //End else: indel has to be performed


	/*
	 * Perform the split if the final split sequence is not empty
	 */
	if(finalSplitSequence.isEmpty() == false)
	{
		//We have to apply the sequence
		this->priv_splitNode(finalSplitSequence, nullptr, splitDeterminer);

		/*
		 * Recursively go down.
		 */

		//Construct a leaf iterator
		uInt_t counter = 0;

		//This is the expected amount of children
		const uInt_t expectedNChilds = 1 << (finalSplitSequence.size());

		for(SubLeafIterator_t childs = this->getLeafIterator(); childs.isEnd() == false; )
		{
			yggdrasil_assert(counter < expectedNChilds);

			//Get the node; This is very dangerous
			yggdrasil_assert(childs.m_path.empty() == false);

			yggdrasil_DETNode_t* chiNode = const_cast<yggdrasil_DETNode_t*>(childs.m_path.back().get());

			yggdrasil_assert(chiNode->isLeaf() == true);
			yggdrasil_assert(childs.access().isLeaf());

			//We habe now to perform the fitting
			chiNode->priv_initialFitting_serial();
			yggdrasil_assert(childs.access().isLeaf());
			yggdrasil_assert(chiNode->isLeaf() == true);

			/*
			 * Now perform the testing (hypothesis).
			 *
			 * There is something very impotant here.
			 * It is possible that the the following function
			 * does split the node that is pointed to by childs
			 * this means that it is not a leaf anylonger.
			 *
			 * This technically invalidates the iterator
			 * after the next call returned.
			 */
			chiNode->priv_hypothesisTesterAndSplitter_serial(splitDeterminer);

			//Add the counter
			counter += 1;

			/*
			 * Since we may have invalidated the iterator
			 * we must call the super unsafe function to increment the node
			 */
			childs.priv_safeIncrement();
		}; //End for(childs)


		if(counter != expectedNChilds)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The number of children is not correct.");
		};
	}; //End else: case where we have to split more



	/*
	 * We perform a final check
	 */
	if(this->checkIntegrity() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("A node has lost its integrity.");
	};



	/*
	 * End
	 */
	return;
}; //End: Hypotheis tester and Splitter serial





void
yggdrasil_DETNode_t::priv_initialFitting_serial()
{
	//test if the node id okay
	if(this->isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not perform initial fitting on a not leaf.");
	};
	if(this->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not okay.");
	};

	const Payload_t& tPl = *(m_payload);
	ParModel_i* model = tPl.m_parModel.get();

	if(model == nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model is not allocated.");
	};
	if(model->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model is not empty.");
	};

	/*
	 * Associate the model and the node.
	 */
	model->preFittingHookInit(this);
	yggdrasil_assert(model->isAssociated() == true);

	/*
	 * Fit all the dimensions.
	 *
	 * Get the listz of all the dimensions that needs fitting.
	 */
	const ParModel_i::DimList_t modelDims = model->getAllDimensionKeys();
	const Size_t nModelDims = modelDims.size();

#if defined(YGGDRASIL_OPENMP_FIT) && YGGDRASIL_OPENMP_FIT != 0
	#pragma omp parallel for default(shared)
#endif
	for(Size_t i = 0; i < nModelDims; ++i)
	{
		const auto d = modelDims[i];

		if(model->isDimensionFitted(d) == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimension " + std::to_string(d) + " was already fitted.");
		};

		const bool fitResult = model->fitModelInDimension(this, d);
		if(fitResult != true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Serious truble, the fitting of dimension " + std::to_string(d) + " did not return true. This is serious.");
		};

		if(model->isDimensionFitted(d) == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Dimension " + std::to_string(d) + " was not fitted correctly.");
		};

		//A small test, I had some stack smashing bugs
		yggdrasil_assert(model->isAssociated() == true);
	}; //End for(d)

	//A small test, I had some stack smashing bugs
	yggdrasil_assert(model->isAssociated() == true);

	//All dimensions should now be fitted
	if(model->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("All tests claim to be fitted, but the overall test at the end was negative.");
	};


	/*
	 * Now inform the model that the fitting ends now
	 */
	model->endOfFittingProcess();

	//Make some safty checks
	yggdrasil_assert(model->isAssociated() == true);
	yggdrasil_assert(model->allDimensionsFitted() == true);
	yggdrasil_assert(model->isConsistent() == true);


	/*
	 * Return
	 */
	return;
}; //End: priv_initialFitting_serial



void
yggdrasil_DETNode_t::priv_splitNode(
	const SplitSequence_t&		splitSeq,
	TestResult_ptr			testResult,
	cSplitDevice_ptr		splitDeterminer)
{
	if(this->checkIntegrity() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not split an invalid node.");
	};

	if(this->isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can only split a leaf.");
	};

	if(splitSeq.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The split sequence is not valid.");
	};

	if(splitSeq.size() == 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("At least one split must be performed.");
	};

	if(m_child.isNull() != true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The childpointer is not zero.");
	};

	/*
	 * Notice that this test is not flawless, since it can
	 * happen that the tag is zero
	 */
	if(m_child.hasTag() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The child has a non zero payload; " + std::to_string(m_child.getTag()));
	};

	//
	//End: Tests

	/*
	 * This now prepares the payload of the split
	 */
	this->m_payload->priv_prepareSplits(splitSeq, testResult);


	/*
	 * Create the new nodes
	 */

	//First of all we have to prepare

	//Index array
	const Size_t nSample = m_payload->nSamples();
	IndexArray_t indexArray = IndexArray_t(nSample);
	for(Size_t i = 0; i != nSample; ++i)
	{
		indexArray[i] = i;
	}; //End for(i): filling the index array

	//This is the subdomain enum; we will start with the empty split
	SubDomEnum_t splitThusFar;	//No splits where done yet

	//This is the parent domain
	HyperCube_t parentDomain = m_payload->getDomain();

	//This is the sub domain ranges
	SubDomRange_t subDomainRanges(splitSeq.size());

	//This is the split that was generated when splitting *this
	//Is set to hand out the data
	SingleSplit_t thisSplit = SingleSplit_t::CONSTRUCT_INVALID_SINGLE_SPLIT();

	//Call the function
	Node_t* newChildren = ygInternal_createNewSubTree(
			indexArray.begin(),
			indexArray.end(),
			indexArray.begin(), 	//This is the base
			splitThusFar,
			&subDomainRanges,	//This is the subdomain ranges
			splitSeq,
			testResult,
			this, 			//This is the ndoe where the split occiures
			&thisSplit, 		//This is generated by this node; It is in rescalled node coordinate of *this node
			splitDeterminer,
			parentDomain);

	//Check if the partitioning worked
	if(subDomainRanges.allSubDomainsValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Not all subdomain of the ranges are valid.");
	};

	//Test if the interval is partioned correctly
	const Size_t foundSamples = subDomainRanges.findUnionRange();
	if(foundSamples != m_payload->nSamples())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Partitioning did not work. Found only " + std::to_string(foundSamples) + ", but there are " + std::to_string(m_payload->nSamples()) + " present.");
	};

	/*
	 * Modify *this such that it is a new internal node
	 * Make set it from the leaf to a split.
	 * This also loads the subtree that was created into *this
	 *
	 * For certain reasons this has to occure before we
	 * distribute the samples.
	 */
	this->priv_transformToSplit(thisSplit, newChildren);

	/*
	 * Copy the samples;
	 * Dimensions by dimensions
	 */

	//Perform an optimization on the index array
	subDomainRanges.finalizeArrayStructure(indexArray);

	/*
	 * This function does the redistribution of the smaples.
	 * This means it distributes the samples onto the new leafes that
	 * are created during the descending process.
	 *
	 * It is important that this function does not do the scalling.
	 * It only distributes the samples.
	 * The scalling was done in in the seach process.
	 *
	 */
	this->m_payload->priv_distributeSamples(subDomainRanges, indexArray, this, 1);



	/*
	 * Perform consistency tests on *this and the subdomains and splits
	 */
	const bool finalTestRes = this->priv_finalTestsAfterSplit(splitSeq);

	if(finalTestRes == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The newly created subtree is not valid.");
	};


	/*
	 * End of splitting process
	 */
	return;
}; //End: split node




bool
yggdrasil_DETNode_t::priv_finalTestsAfterSplit(
	const SplitSequence_t&		splitSeq)
 const
{
	//At least one split has to be performed
	if(splitSeq.isEmpty() == true)
	{
		yggdrasil_assert(false && "The split sequence at the top is empty.");
		return false;
	};

	//First we test if the payload is valid
	if(this->checkIntegrity() == false)
	{
		yggdrasil_assert(false && "Payload is not valid.");
		return false;
	};

	if(m_payload->isTrueSplitState() == false)
	{
		yggdrasil_assert(false && "The node is not a true split");
		return false;
	};

	if(m_child.getTag() != splitSeq.getHeadSplit())
	{
		yggdrasil_assert(false && "The split location is wrong.");
		return false;
	};

	if(m_payload->m_isDirty == true)
	{
		yggdrasil_assert(false && "The node is dirty.");
		return false;
	};


	/*
	 * Perform the checking recursive
	 */
	Size_t leftMass = 0;
	const bool leftResult = priv_performRecursiveChecking(
			m_child.get() + IDX_LEFT,
			splitSeq.copyAndRemoveHeadSplit(),
			&leftMass);
	if(leftResult == false)
	{
		yggdrasil_assert(false && "Left split failed.");
		return false;
	};
	if(leftMass == Size_t(-1))
	{
		yggdrasil_assert(false && "The left mass could not be estimated.");
		return false;
	};

	Size_t rightMass = 0;
	const bool rightResult = priv_performRecursiveChecking(
			m_child.get() + IDX_RIGHT,
			splitSeq.copyAndRemoveHeadSplit(),
			&rightMass);
	if(rightResult == false)
	{
		yggdrasil_assert(false && "Right splt faile .");
		return false;
	};
	if(rightMass == Size_t(-1))
	{
		yggdrasil_assert(false && "The mass is invalid.");
		return false;
	};


	/*
	 * Check the mass
	 */
	if((leftMass + rightMass) != m_payload->m_mass)
	{
		yggdrasil_assert(false && "Mass is not compatible.");
		return false;
	};


	return true;
};



void
yggdrasil_DETNode_t::priv_setToLeaf(
	const yggdrasil_DETNode_t*	rootNode)
{
	if(this->isOKNode() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can only set an invalid node to a leafe.");
	};

	//TODO: TEST PAYLOAD

	//Test if payload is allocated and child is nulll
	if(m_payload == nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can only transform a node into a leaf that has an allocated payload.");
	};

	if(m_child.isNull() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node to transfer into a leaf has children.");
	};

	//This test is niot 100% secure
	if(m_child.hasTag() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The noe has a tag.");
	};

	//We transform this to a leaf
	m_splitPos = NAN;

	//Set the payload
	m_payload->priv_setToLeaf(rootNode);


	return;
}; //End: setToLeaf


void
yggdrasil_DETNode_t::priv_setToSplit(
	const SingleSplit_t&	sSplit)
{
	if(this->isOKNode() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can only set an invalid node to a split.");
	};

	//TODO: TEST PAYLOAD

	//Test if payload is allocated and child is nulll
	if(m_payload == nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can only transform a node into a split that has an allocated payload.");
	};

	if(m_child.isNull() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node to transfer into a split does not has children.");
	};

	//This test is niot 100% secure
	if(m_child.hasTag() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The noe has a tag.");
	};

	if(sSplit.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The split is not valid; Pos = " + std::to_string(sSplit.getSplitPos()) + ", Dim = " + std::to_string(sSplit.getSplitDim()));
	};

	//Now we must this into a split
	//The value that we have computed is inside the always rescalled domain
	//so we can not check it , if it is lies inside the domain.
	m_splitPos = sSplit.getSplitPos();
	m_child.setTag(sSplit.getSplitDim());

	//Call the payload
	m_payload->priv_setToIndirectSplit(sSplit);

	return;
}; //End: setToSplit


void
yggdrasil_DETNode_t::priv_transformToSplit(
	const SingleSplit_t&	sSplit,
	Node_t*			newSubTree)
{
	if(this->isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can only set an invalid node to a split.");
	};

	//TODO: TEST PAYLOAD

	//Test if payload is allocated and child is nulll
	if(m_payload == nullptr)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can only transform a node into a split that has an allocated payload.");
	};

	if(m_child.isNull() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node to transfer into a split has already children.");
	};

	//This test is niot 100% secure
	if(m_child.hasTag() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The noe has a tag.");
	};

	if(sSplit.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The split is not valid; Pos = " + std::to_string(sSplit.getSplitPos()) + ", Dim = " + std::to_string(sSplit.getSplitDim()));
	};

#if 0
	/*
	 * This test is obsolete, since we now work with the
	 * rescalled data and the split is in the rescalled node
	 * data space, but the domain is in the rescalled root
	 * node data space.
	 */
	if(m_payload->isInside(sSplit) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The split is not inside the domain.");
	};
#endif

#ifndef NDEBUG
	const IntervalBound_t unitInterval(IntervalBound_t::CREAT_UNIT_INTERVAL());
	yggdrasil_assert(unitInterval.isInside(sSplit.getSplitPos()) || unitInterval.isCloseToUpperBound(sSplit.getSplitPos()));
#endif

	//Now we must this into a split
	m_splitPos = sSplit.getSplitPos();	//The splöit position is relative to the [0, 1[ interbal
	m_child.setTag(sSplit.getSplitDim());
	m_child.exchangePointer(newSubTree);

	//Transform the paylod
	m_payload->priv_transformToSplit(sSplit, newSubTree);

	yggdrasil_assert(m_child.getTag() == sSplit.getSplitDim());

	return;
};

/*
 * ====================================
 * Helper functions
 */


/*
 * This function performs the checking in the deeper levels
 */
bool
yggdrasil_DETNode_t::priv_performRecursiveChecking(
	const yggdrasil_DETNode_t*			nodeToCheck,
	const yggdrasil_DETNode_t::SplitSequence_t& 	spitSeq,
	Size_t* 					massIn)
{
	//Mark that we are jet inavlid
	*massIn = Size_t(-1);

	/*
	 * When this function is called, we are either at a
	 * leafe or at an indirect split.
	 */
	if(nodeToCheck->isOKNode() == false)
	{
		yggdrasil_assert(false && "The node is not valid.");
		return false;
	};

	//The node must be non dirty
	if(nodeToCheck->m_payload->m_isDirty == true)
	{
		yggdrasil_assert(false && "The node is dirty.");
		return false;
	};

	//this is the mass we have found
	Size_t foundMass = Size_t(-1);

	if(spitSeq.isEmpty() == false)
	{
		//The we are at an inner node
		if(nodeToCheck->m_payload->isIndirectSplitState() == false)
		{
			yggdrasil_assert(false && "The split is not an indirect split");
			return false;
		};


		/*
		 * NOTE that the statistisc are empty
		 */
		if(nodeToCheck->m_payload->m_GOFTest != nullptr)
		{
			yggdrasil_assert(false && "The GOF Test pointer is not null.");
			return false;
		};
		if(nodeToCheck->m_payload->m_indepTest != nullptr)
		{
			yggdrasil_assert(false && "An indirect split has an associated statistic.");
			return false;
		};

		/*
		 * The model and the two test must be nullpointers.
		 * This is ensured by the verify fucntion
		 */


		/*
		* Perform the checking recursive
		*/
		Size_t leftMass = 0;
		const bool leftResult = priv_performRecursiveChecking(
				nodeToCheck->m_child.get() + IDX_LEFT,
				spitSeq.copyAndRemoveHeadSplit(),
				&leftMass);
		if(leftResult == false)
		{
			yggdrasil_assert(false && "Left split failed.");
			return false;
		};
		if(leftMass == Size_t(-1))
		{
			yggdrasil_assert(false && "The left mass could not be estimated.");
			return false;
		};

		Size_t rightMass = 0;
		const bool rightResult = priv_performRecursiveChecking(
				nodeToCheck->m_child.get() + IDX_RIGHT,
				spitSeq.copyAndRemoveHeadSplit(),
				&rightMass);
		if(rightResult == false)
		{
			yggdrasil_assert(false && "Right splt faile .");
			return false;
		};
		if(rightMass == Size_t(-1))
		{
			yggdrasil_assert(false && "The mass is invalid.");
			return false;
		};

		//Set the found mass
		foundMass = leftMass + rightMass;

	}
	else
	{
		//We are a leaf
		if(nodeToCheck->m_payload->isLeafState() == false)
		{
			yggdrasil_assert(false && "The node is not a leaf");
			return false;
		};

		//The found mass is the one we have
		foundMass = nodeToCheck->m_payload->m_samples.nSamples();

		/*
		 * The model and the two test must be allocated,
		 * but they must be empty at this stage.
		 */
		if(nodeToCheck->m_payload->m_GOFTest->isAssociated() == true)
		{
			yggdrasil_assert(false && "The GOF test is not empty.");
			return false;
		};
		if(nodeToCheck->m_payload->m_indepTest->isAssociated() == true)
		{
			yggdrasil_assert(false && "The independence test is not empty.");
			return false;
		};
		if(nodeToCheck->m_payload->m_parModel->isAssociated() == true)
		{
			yggdrasil_assert(false && "The parametric model is not empty.");
			return false;
		};
	}; //End

	//Now check if the mass is the one we expect
	if(foundMass != nodeToCheck->m_payload->m_mass)
	{
		yggdrasil_assert(false && "The mass is wrong.");
		return false;
	};

	if(nodeToCheck->m_payload->m_mass != nodeToCheck->m_payload->m_dirtyMass)
	{
		yggdrasil_assert(false && "The dirty mass is not the same as the mass.");
		return false;
	};

	//propagate the mass outside
	*massIn = foundMass;


	//End checking
	return true;
}; //End Perform checking



Node_t*
ygInternal_createNewSubTree(
	Node_t::IndexArray_t::iterator		indexArray_start,
	Node_t::IndexArray_t::iterator		indexArray_end,
	const Node_t::IndexArray_t::iterator	indexArray_base,
	Node_t::SubDomEnum_t 			splitThusFar,
	Node_t::SubDomRange_t* 			subDomainRanges,
	const Node_t::SplitSequence_t&		splitSeq,
	Node_t::TestResult_ptr			testResult,
	Node_t::cNode_ptr 			rootNodeWhereSplit,
	Node_t::SingleSplit_t* 			thisSingleSplit_out,
	Node_t::cSplitDevice_ptr		splitDeterminer,
	const Node_t::HyperCube_t&		parentDomain)
{
	yggdrasil_assert(parentDomain.isValid());

	/*
	 * BASE CASE:
	 * We are finisched with construction; So we must make the clean up
	 *
	 * The base case is, when all splits are performed.
	 * This means that we are called on a leaf.
	 */
	if(splitSeq.isEmpty() == true)
	{
		//Filling the range
		subDomainRanges->exchangeSubDomain(
				splitThusFar,
				Node_t::SubDomRange_t::SingleRange_t(
					::std::distance(indexArray_base, indexArray_start),
					::std::distance(indexArray_base, indexArray_end)
					)
				);

		////We allow empty ranges.
		//yggdrasil_assert(subDomainRanges->getSubDomain(splitThusFar).isEmpty() == false);

		//Setting the this split to something useless
		*thisSingleSplit_out = Node_t::SingleSplit_t::CONSTRUCT_INVALID_SINGLE_SPLIT();

		return nullptr;
	}; //End basecase check

	/*
	 * Non Basecase
	 */

	//Which split do we have to handle
	const Int_t thisSplit = splitThusFar.getDepth(); 	//This has to do with the counting

	//Which dimension we have to split now
	const auto thisSplitDim = splitSeq.getHeadSplit();

	//Get the array we have to handle
	const Node_t::DimensionArray_t& thisSampleDim = rootNodeWhereSplit->m_payload->getCDim(thisSplitDim);

	/*
	 * This call will determine the split location.
	 *
	 * It is very  I M P O R T A N T  that this call computes the bound
	 * in the rescalled data space of the node that is splitted.
	 *
	 * It is also important to note that this are different coordinates
	 * than the domain uses!
	 */
	const yggdrasil_splitFinder_i::SplitCommand_t splitRes = splitDeterminer->determineSplitLocation(
			parentDomain, 				//The 1D interval that we split; In rescalled  R O O T  data space coordinate
			splitSeq, 				//The dimension we split
			indexArray_start, 			//Start of the array
			indexArray_end, 			//Past the end of the array
			thisSampleDim);				//Reference to the sample array


	//Save the single split of the range in the output vcariable
	//A assigne it before the test in order to propagate
	//it out, it could be good for debugging
	//It is important that the value is in the rescalled range.
	//So it must not be inside the domain.
	*thisSingleSplit_out = splitRes;

	//Test if the split is valid
	if(splitRes.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The split has failed.");
	};

	/*
	 * This is the spliot position, it is important that it is not in the
	 * rescalled root data range, but in the rescalled data range of this node.
	 * Thsi emans it isallways in the interval [0, 1[.
	 */
	const Numeric_t newBound = splitRes.getSplitPos();

	if(splitRes.getSplitDim() != thisSplitDim)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Dimension has changed.");
	};

	//Now we partition the samples; We do not use a stable version
	//It could be that depending on the implementation of the split computation
	//object this is not needed, however it is good paractice to do it
	//also it will be very fast.
	auto beginOfRightDomain = yggdrasil_argPartition(
			indexArray_start, indexArray_end, thisSampleDim, newBound
			);

	/*
	 * Rescalling the memory
	 *
	 * Here we rescale the samples.
	 * We only have to do one dimension
	 *
	 * We do this in a sperate scope.
	 */
	{
		//this is a mutable reference to the dimensional array
		//that we are currently handle
		Node_t::DimensionArray_t& thisSampleDimMutable = rootNodeWhereSplit->m_payload->getDim(thisSplitDim);

		//Perform the rescalling
		ygInternal_rescaleData_scalar(
			indexArray_start,
			indexArray_end,
			beginOfRightDomain,
			thisSampleDimMutable,
			newBound);
	}; //ENd rescalling

	/*
	 * Create the memory
	 */

	/*
	 * Going down
	 */

	//This are the new domains
	//Before we can split them we must transfere the split position from the
	//node specific data range to the rescalled root data space
	Node_t::HyperCube_t
		leftDomain  = Node_t::HyperCube_t::MAKE_INVALID_CUBE(),
		rightDomain = Node_t::HyperCube_t::MAKE_INVALID_CUBE();
	{
		//We must trasform it, from that we use the map fucntion of the hypercube
		const auto rescalledSplitPosition = parentDomain[thisSplitDim].mapFromUnit(newBound);

		//Perform the spliting
		std::tie(leftDomain, rightDomain) = parentDomain.doSplit(thisSplitDim, rescalledSplitPosition);
	}; //Endscope to not contaminate the domain
	yggdrasil_assert(leftDomain.isValid());
	yggdrasil_assert(rightDomain.isValid());

	//Allocate the memory for the new nodes
	Node_t* newChilds = new Node_t[2]();
	yggdrasil_assert(newChilds != nullptr);

	//This is the next level for the split

	//LEFT
	{
		//This is the next split
		const Node_t::SubDomEnum_t leftSplitThusFar = splitThusFar.decendToLeft();

		//Test if the original is Valid
		yggdrasil_assert(leftDomain.isValid());

		//Make a copy of the left domain
		const Node_t::HyperCube_t leftDomainPass = leftDomain;

		//Test if the two domains are valid
		yggdrasil_assert(leftDomainPass.isValid());
		yggdrasil_assert(leftDomain.isValid());

		//Pop the first one
		const Node_t::SplitSequence_t leftCuttedSplitSeq = splitSeq.copyAndRemoveHeadSplit();

		//Test if the two are still valid
		yggdrasil_assert(leftDomainPass.isValid());
		yggdrasil_assert(leftDomain.isValid());



		//This is the split that is generated by spliting the left samples
		Node_t::SingleSplit_t leftSplitResult = Node_t::SingleSplit_t::CONSTRUCT_INVALID_SINGLE_SPLIT();

		//Now Allocate the payload of the left child
		newChilds[Node_t::IDX_LEFT].priv_allocPayload(rootNodeWhereSplit->m_payload->nDims());

		// Populate the payload of the left
		newChilds[Node_t::IDX_LEFT].m_payload->priv_populatePayload(
				rootNodeWhereSplit->m_payload,
				testResult,
				leftSplitThusFar,	//This is not necessaraly a final sub domain
				indexArray_start,	//This is still the same beginning
				beginOfRightDomain,	//This must be the result of the partitioning
				leftDomain);


		//Perform the split recursively
		newChilds[Node_t::IDX_LEFT].m_child = ygInternal_createNewSubTree(
				indexArray_start, 	//Is still the same
				beginOfRightDomain,	//Is different, because the domain is now partitioned
				indexArray_base, 	//This is the base
				leftSplitThusFar, 	//Must be left split
				subDomainRanges,	//This is the subdomain ranges; will be andled in the base case
				leftCuttedSplitSeq,	//This is the one shorter sequence
				testResult,		//Still the same results
				rootNodeWhereSplit,		//Rootpayload
				&leftSplitResult,	//This is the result of the split, of the left samples
				splitDeterminer,	//Sill the same
				leftDomainPass);		//Is now the left domain

		//Test if we have reached the end
		if(newChilds[Node_t::IDX_LEFT].m_child.isNull() == true)
		{
			//This is a leaf, so we must do transfer *this inot a leaf
			newChilds[Node_t::IDX_LEFT].priv_setToLeaf(rootNodeWhereSplit);
		} //End if: left is a leaf
		else
		{
			//This is not a leaf
			if(leftSplitResult.isValid() == false)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("The split of the left subdomain did not work. Split Sequence thus far " + std::to_string(splitThusFar.getWay()));
			};

			//We now set the left node to a split
			newChilds[Node_t::IDX_LEFT].priv_setToSplit(leftSplitResult);
		}; //End else: Is not a leaf

		//final Check
		if(newChilds[Node_t::IDX_LEFT].isOKNode() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The left child is invalid.");
		};
	}; //End LEFT

	//RIGHT
	{
		//Descend tio the right
		const Node_t::SubDomEnum_t rightSplitThusFar = splitThusFar.decendToRight();

		//This is the split that is generated by spliting the right samples
		Node_t::SingleSplit_t rightSplitResult = Node_t::SingleSplit_t::CONSTRUCT_INVALID_SINGLE_SPLIT();

		//Allocate the payload
		newChilds[Node_t::IDX_RIGHT].priv_allocPayload(rootNodeWhereSplit->m_payload->nDims());

		//Populate the payload on the right
		newChilds[Node_t::IDX_RIGHT].m_payload->priv_populatePayload(
				rootNodeWhereSplit->m_payload,
				testResult,
				rightSplitThusFar,
				beginOfRightDomain, 	//The right domain starts here
				indexArray_end, 	//The right end is still the classical end
				rightDomain);

		//Peform the split recursively
		newChilds[Node_t::IDX_RIGHT].m_child = ygInternal_createNewSubTree(
				beginOfRightDomain, 	//The start was determined by the partitioning
				indexArray_end, 	//End is still the same
				indexArray_base, 	//Base is allways the base
				rightSplitThusFar, 	//splits that leads to the domain at the right
				subDomainRanges, 	//Subdomain ranges; Handled in the base case
				splitSeq.copyAndRemoveHeadSplit(), 		//The full split sequence
				testResult, 		//The test results
				rootNodeWhereSplit, 		//The root payload
				&rightSplitResult, 	//This is the split that is generated for this
				splitDeterminer, 	//Object to find the split
				rightDomain);		//The right domain

		//Test if we have reached the end
		if(newChilds[Node_t::IDX_RIGHT].m_child.isNull() == true)
		{
			//The right child is a leafe
			newChilds[Node_t::IDX_RIGHT].priv_setToLeaf(rootNodeWhereSplit);
		} //End if: right is a leaf
		else
		{
			//This is not a leaf
			if(rightSplitResult.isValid() == false)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("The split of the right subdomain did not work. Split Sequence thus far " + std::to_string(splitThusFar.getWay()));
			};

			//We now set the right node to be a split (indirect)
			newChilds[Node_t::IDX_RIGHT].priv_setToSplit(rightSplitResult);
		}; //End else: right is a split


		//Final check
		if(newChilds[Node_t::IDX_RIGHT].isOKNode() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The right child is invalid.");
		};
	}; //End: RIGHT


	//Return

	//The split location was alredy returned

	//Return the poiinter
	return newChilds;
}; //End: ygInternal_createNewSubTree




YGGDRASIL_NS_END(yggdrasil)
