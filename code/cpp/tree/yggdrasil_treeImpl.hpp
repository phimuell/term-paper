#pragma once
/**
 * \file	tree/yggdrasil_treeImpl.hpp
 * \brief 	This file cotains the implementation of the tree.
 *
 * The tree basically serves as the container of the nodes and interface for them.
 * It stores also the global data, that is associated to it.
 *
 * This class can not be copied.
 *
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>
#include <core/yggdrasil_PDFValueArray.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <interfaces/yggdrasil_interface_splitStrategy.hpp>
#include <interfaces/yggdrasil_interface_parametricModel.hpp>
#include <interfaces/yggdrasil_interface_GOFTest.hpp>
#include <interfaces/yggdrasil_interface_IndepTest.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>

#include <util/yggdrasil_treeDepthMap.hpp>

#include <factory/yggdrasil_treeBuilder.hpp>


//Include STD
#include <cmath>
#include <iostream>
#include <memory>
#include <utility>

// Include Eigen
#include <Eigen/Core>


YGGDRASIL_NS_BEGIN(yggdrasil)

//Forward declaration of the node type
class yggdrasil_DETNode_t;

/**
 * \brief	This class reoresents a tree
 * \class 	yggdrasil_DETree_t
 *
 * This class does own the a tree.
 * It is the central interface of managing a tree object.
 * However it does not allow coping a tree.
 *
 * Its implementation makes use of the pinpl idiom.
 * This means that only the service code is exposed
 * when this file is included.
 *
 * There is no central object that collects all, becaue
 * the majority of stuff \c *thsi needs is service code anyway.
 *
 * Notice that there were several version of this class.
 * Mainly the exact meaning of the m_domain member that
 * is located inside the payload.
 * It changed its meaning several time, but currently
 * its meaning is, that it describes the space occuped
 * by a node relative to the unit cube.
 *
 * This means that the nodes don't know how the original domain
 * looks like. This is the reason why the tree must store
 * this information.
 *
 */
class yggdrasil_DETree_t
{
	/*
	 * =======================
	 * Typedef
	 */
public:
	using Node_t 		  = ::yggdrasil::yggdrasil_DETNode_t;	//!< This is the node type
	using Node_ptr 		  = Node_t*;				//!< Pointer to a node
	using Node_ref 		  = Node_t&;				//!< This is a reference to a node
	using cNode_ref 	  = const Node_t&;			//!< This is a constant reference to a node
	using cNode_ptr 	  = const Node_t*;			//!< This is a pointer to a constant node

	using NodeFacade_t 	  = yggdrasil_nodeFacade_t;		//!< This is the node facade.

	using TreeBuilder_t 	  = yggdrasil_treeBuilder_t;		//!< This is the class for building the tree.

	using SampleCollection_t  = yggdrasil_sampleCollection_t;	//!< This is the sample collection
	using DimensionArray_t 	  = SampleCollection_t::DimensionArray_t; //!< This is one diomension of the sample collection
	using SampleArray_t 	  = SampleCollection_t::SampleArray_t;	//!< Space efficient representation of many samples.
	using SampleList_t 	  = SampleCollection_t::SampleList_t;

	using Sample_t 		  = yggdrasil_sample_t;			//!< This is the sample type
	using HyperCube_t 	  = yggdrasil_hyperCube_t;		//!< This is the type for a hyper cube, aka domain bound
	using IntervalBound_t 	  = HyperCube_t::IntervalBound_t;	//!< This is the bound of a single interval

	using DomainSplitter_i 	  = yggdrasil_splitFinder_i;		//!< This is the interface that represent the ability to split
	using DomainSplitter_ptr  = DomainSplitter_i*;			//!< This is the pointer of the splitter

	//This are the interfaces
	using GOFTest_i 	= yggdrasil_GOFTest_i;				//!< This is the interface to the GOF test
	using GOFTest_ptr 	= std::unique_ptr<GOFTest_i>;	//!< This is the interface of the GOF test
	using IndepTest_i 	= yggdrasil_IndepTest_i;		//!< This is the interface for the indepenence test
	using IndepTest_ptr 	= std::unique_ptr<IndepTest_i>; //!< This is the interface for the independence test
	using ParModel_i 	= yggdrasil_parametricModel_i;	//!< This is the interface of the parameteric model
	using ParModel_ptr 	= std::unique_ptr<ParModel_i>;	//!< This is the test for the parameteric model
	using ValueArray_t 	= ParModel_i::ValueArray_t;	//!< THis is a result type.

	using PDFValueArray_t 		= ::yggdrasil::yggdrasil_PDFValueArray_t<Numeric_t>;	//!< This type is for storing the results of PDF queries, which involves many samples.
	using PDFValueArrayEigen_t	= ::Eigen::VectorXd;					//!< This is the return type for the pdf fcntions that operate on Eigen types.

	using DepthMap_t 	= yggdrasil_treeDepthMap_t;	//!< This is the depth map

	/**
	 * \using 	SubLeafIterator_t
	 * \brief	This allows teh iteration of all leafs beneath *this.
	 *
	 * This iterator allows the access of all leafes that are stored below *this.
	 * This functionality is allways present.
	 * If *this is a leaf one can only iterate over it.
	 */
	using SubLeafIterator_t		= yggdrasil_leafIterator_t;	//Iterator for accessing everything benith this

	/**
	 * \brief	Return type of some of the complexer descent functions.
	 *
	 * This is the return type of the descent fucntions that operates on a sample.
	 * Instead of simply returning the node pointer they also return the sample,
	 * in rescalled node specific data range, (unit interval).
	 */
	using SampleDescentRet_t = ::std::pair<Node_ptr, Sample_t>;

	/**
	 * \brief	This is the retunr type for the constant descend fucntions
	 */
	using cSampleDescentRet_t = ::std::pair<cNode_ptr, Sample_t>;



	/*
	 * ========================
	 * Constructors
	 *
	 * This class is unmovable and it is also not possible to copy it.
	 */
public:
	/**
	 * \brief	This is a building constructor.
	 *
	 * This function uses the sample collection and the builder,
	 * to set *this up. It is possible to move the underling data
	 * form the collection into *this.
	 * The result is that the collection becomes empty.
	 *
	 * The domain can be invalid, in that case the domain
	 * is estimated from the data.
	 * Notice that due to the definition of an interval, the domain will
	 * be larger than the largest coordinate.
	 *
	 * \param  sampelCol	This is the sample collection.
	 * \param  domain	This is the domain that this spans.
	 * \param  builder	This is the builder that is used for creating
	 * 			 the implementation.
	 * \param  mvSamples	This moves the samples, is defaulted to true.
	 *
	 * \throw 	Throws is an underling fucntion throws.
	 */
	yggdrasil_DETree_t(
		SampleCollection_t&	sampelCol,
		const HyperCube_t&	domain,
		const TreeBuilder_t&	treeBuilder,
		const bool 		moveSamples = false);

	/**
	 * \brief	This is a building constructor.
	 *
	 * This function operates on sample array.
	 *
	 * The domain can be invalid, in that case the domain
	 * is estimated from the data.
	 * Notice that due to the definition of an interval, the domain will
	 * be larger than the largest coordinate.
	 *
	 * \param  arraySamples This is the array where we load the data from.
	 * \param  domain	This is the domain that this spans.
	 * \param  builder	This is the builder that is used for creating
	 * 			 the implementation.
	 *
	 * \throw 	Throws is an underling fucntion throws.
	 *
	 * \note	This function constructs a collection out of teh sample array.
	 */
	yggdrasil_DETree_t(
		const SampleArray_t&	arraySample,
		const HyperCube_t&	domain,
		const TreeBuilder_t&	treeBuilder);


	/**
	 * \brief	This is a building constructor.
	 *
	 * This function operates on sample list.
	 *
	 * The domain can be invalid, in that case the domain
	 * is estimated from the data.
	 * Notice that due to the definition of an interval, the domain will
	 * be larger than the largest coordinate.
	 *
	 * \param  listSamples	This is the array where we load the data from.
	 * \param  domain	This is the domain that this spans.
	 * \param  builder	This is the builder that is used for creating
	 * 			 the implementation.
	 *
	 * \throw 	Throws is an underling fucntion throws.
	 *
	 * \note	This function constructs a collection out of teh sample list.
	 */
	yggdrasil_DETree_t(
		const SampleList_t&	listSamples,
		const HyperCube_t&	domain,
		const TreeBuilder_t&	treeBuilder);



	/*
	 * =============================
	 * Internal constructors
	 */



	/**
	 * \brief	This is the building constructor.
	 *
	 * It takes the main strageties of this and constructs itself.
	 * The domain must not be valid.
	 * If the domain is not valid, that data will be used to determine
	 * the domain.
	 * Notice that due to the definition of an interval, the domain will
	 * be larger than the largest coordinate.
	 *
	 * This constructor reads in the data.
	 * It also rescales the data.
	 *
	 * After everything is loaded it will fit the initial model on the whole data.
	 * After that it will perform the GOF and the independence test and
	 * if they failed it will goes down.
	 *
	 * \param  samples 		This is the samples that will be absorbed.
	 * \param  domain		This is the domain we operate on.
	 * \param  parModel		This is the parameteric model
	 * \param  splitFinder 		This is the object that is able to determine the split.
	 * \param  gofTester 		This is the object for performing the statistical tests.
	 * \param  indepTester 		THis is the object for testing the independence hypothesis.
	 * \param  moveSamples 		If set to true the samples are not copied but moved.
	 *
	 * \note	This is an internal constructor.
	 */
	yggdrasil_DETree_t(
		SampleCollection_t&				samples,
		const HyperCube_t& 				domain,
		const std::unique_ptr<DomainSplitter_i>&	splitFinder,
		const std::unique_ptr<ParModel_i>&		parModel,
		const std::unique_ptr<GOFTest_i>&		gofTester,
		const std::unique_ptr<IndepTest_i>&		indepTester,
		const bool 					moveSample);

	/**
	 * \brief	This is the building constructor.
	 *
	 * It takes the main strageties of this and constructs itself.
	 * The domain must not be valid.
	 * If the domain is not valid, that data will be used to determine
	 * the domain.
	 * Notice that due to the definition of an interval, the domain will
	 * be larger than the largest coordinate.
	 *
	 * This constructor reads in the data.
	 * It also rescales the data.
	 *
	 * After everything is loaded it will fit the initial model on the whole data.
	 * After that it will perform the GOF and the independence test and
	 * if they failed it will goes down.
	 *
	 * \param  samples 		This is the samples that will be absorbed.
	 * \param  domain		This is the domain we operate on.
	 * \param  parModel		This is the parameteric model
	 * \param  splitFinder 		This is the object that is able to determine the split.
	 * \param  gofTester 		This is the object for performing the statistical tests.
	 * \param  indepTester 		THis is the object for testing the independence hypothesis.
	 *
	 * \note	This is an internal constructor.
	 */
	yggdrasil_DETree_t(
		SampleCollection_t				samples,
		const HyperCube_t& 				domain,
		const std::unique_ptr<DomainSplitter_i>&	splitFinder,
		const std::unique_ptr<ParModel_i>&		parModel,
		const std::unique_ptr<GOFTest_i>&		gofTester,
		const std::unique_ptr<IndepTest_i>&		indepTester);



	/*
	 * Normal constructors
	 */



	/**
	 * \brief	The default constructor is deleted
	 */
	yggdrasil_DETree_t() = delete;


	/**
	 * \brief	The copy constructor is deleted
	 */
	yggdrasil_DETree_t(
		const yggdrasil_DETree_t&) = delete;


	/**
	 * \brief	The move constructor is deleted
	 */
	yggdrasil_DETree_t(
		yggdrasil_DETree_t&&) = delete;


	/**
	 * \brief	The copy assignment is deleted
	 */
	yggdrasil_DETree_t&
	operator= (
		const yggdrasil_DETree_t&) = delete;


	/**
	 * \brief	The move assignment is deleted
	 */
	yggdrasil_DETree_t&
	operator= (
		yggdrasil_DETree_t&&) = delete;


	/**
	 * \brief	The destructor is not trivial.
	 *
	 * It will dealocate the root and other pointers.
	 */
	~yggdrasil_DETree_t();


	/*
	 * =====================
	 * PDF Query functions
	 *
	 * This function allows to query the estimator
	 */
public:
	/**
	 * \brief	This fucntion eqvaluaes the PDF at the given location \c x.
	 *
	 * This function desent into the tree until it reaces the leaf
	 * where the given sample \c x is located in.
	 * It then will evaluate the PDF value of the sample and return it.
	 *
	 * If the sample lies outside the given support this function
	 * raises an error. If this fact should be ignored, meaning that you
	 * want that zero is returned, then use evaluateSampleAt_noErr() function.
	 *
	 * \param  x	The smaple location where we evaluate the tree.
	 * 		 Must be in original data space.
	 *
	 * \throw 	This function throws if \c x is outside the data range.
	 * 		 Also internal fucntion could throw.
	 *
	 * \note 	\c x must be in original data space, this means no rescalling
	 * 		 what so ever has to be applied before the fucntion call.
	 *
	 * \note 	The name of this functionindicates that it is able to handle
	 * 		 many samples. But this is not true. The plural of sample
	 * 		 was used to provide the same name as for the other cases.
	 */
	Numeric_t
	evaluateSamplesAt(
		const Sample_t& 	x)
	 const;



	/**
	 * \brief	This function evaluates the tree at the given location \c x.
	 *
	 * This function is basically the same as evaluateSampleAt() function.
	 * But this function returns zero if \c x lies outside the data domain.
	 *
	 * \param  x	The smaple location where we evaluate the tree.
	 * 		 Must be in original data space.
	 *
	 * \throw 	This function throws if an internal fucntion could throw.
	 *
	 * \note 	\c x must be in original data space, this means no rescalling
	 * 		 what so ever has to be applied before the fucntion call.
	 *
	 * \note 	The name of this functionindicates that it is able to handle
	 * 		 many samples. But this is not true. The plural of sample
	 * 		 was used to provide the same name as for the other cases.
	 */
	Numeric_t
	evaluateSamplesAt_noErr(
		const Sample_t& 	x)
	 const;


	/**
	 * \brief	This function evaluates the model for many samples at once.
	 *
	 * This fucntion is a bit more efficient than calling the evaluateSampleAt() function
	 * many times, since it computes some auxilery values only once.
	 *
	 * As the evaluateSampleAt() fucntion, this functions generate an exception if
	 * the sample lies outside the data range.
	 *
	 * Also the samples are to be exopected to be in the original data space, this means
	 * no scalling what so ever has to be applied.
	 *
	 * \param  X	The smaple locations where we evaluate the tree.
	 * 		 Must be in original data space.
	 *
	 * \throw 	This function throws if an internal fucntion could throw.
	 *
	 * \note 	\c x must be in original data space, this means no rescalling
	 * 		 what so ever has to be applied before the fucntion call.
	 */
	PDFValueArray_t
	evaluateSamplesAt(
	 	const SampleList_t&	X)
	 const;


	/**
	 * \brief	This function evaluates the model for many samples at once.
	 *
	 * This fucntion is a bit more efficient than calling the evaluateSampleAt_noErr()
	 * function many times, since it computes some auxilery values only once.
	 *
	 * As the evaluateSampleAt_noErr() fucntion, this functions generates no exception
	 * if the sample lies outside the data range, but it simply sets the value to zero.
	 *
	 * Also the samples are to be exopected to be in the original data space, this means
	 * no scalling what so ever has to be applied.
	 *
	 * \param  X	The smaple locations where we evaluate the tree.
	 * 		 Must be in original data space.
	 *
	 * \throw 	This function throws if an internal fucntion could throw.
	 *
	 * \note 	\c x must be in original data space, this means no rescalling
	 * 		 what so ever has to be applied before the fucntion call.
	 */
	PDFValueArray_t
	evaluateSamplesAt_noErr(
	 	const SampleList_t&	X)
	 const;



	/**
	 * \brief	This function evaluates the model for many samples at once.
	 *
	 * This fucntion is a bit more efficient than calling the evaluateSampleAt() function
	 * many times, since it computes some auxilery values only once.
	 *
	 * As the evaluateSampleAt() fucntion, this functions generate an exception if
	 * the sample lies outside the data range.
	 *
	 * Also the samples are to be exopected to be in the original data space, this means
	 * no scalling what so ever has to be applied.
	 *
	 * \param  X	The smaple locations where we evaluate the tree.
	 * 		 Must be in original data space.
	 *
	 * \throw 	This function throws if an internal fucntion could throw.
	 *
	 * \note 	\c x must be in original data space, this means no rescalling
	 * 		 what so ever has to be applied before the fucntion call.
	 *
	 * \note 	This fucntion operates on a sample array.
	 */
	PDFValueArray_t
	evaluateSamplesAt(
		const SampleArray_t&	X)
	 const;

	/**
	 * \brief	This function evaluates the model for many samples at once.
	 *
	 * This fucntion is a bit more efficient than calling the evaluateSampleAt_noErr()
	 * function many times, since it computes some auxilery values only once.
	 *
	 * As the evaluateSampleAt_noErr() fucntion, this functions generates no exception
	 * if the sample lies outside the data range, but it simply sets the value to zero.
	 *
	 * Also the samples are to be exopected to be in the original data space, this means
	 * no scalling what so ever has to be applied.
	 *
	 * \param  X	The smaple locations where we evaluate the tree.
	 * 		 Must be in original data space.
	 *
	 * \throw 	This function throws if an internal fucntion could throw.
	 *
	 * \note 	\c x must be in original data space, this means no rescalling
	 * 		 what so ever has to be applied before the fucntion call.
	 *
	 * \note	This function operates on a smaple array.
	 */
	PDFValueArray_t
	evaluateSamplesAt_noErr(
	 	const SampleArray_t&	X)
	 const;


	/**
	 * \brief	This function evaluates the model for many samples at once.
	 *
	 * This fucntion is a bit more efficient than calling the evaluateSampleAt() function
	 * many times, since it computes some auxilery values only once.
	 *
	 * As the evaluateSampleAt() fucntion, this functions generate an exception if
	 * the sample lies outside the data range.
	 *
	 * Also the samples are to be exopected to be in the original data space, this means
	 * no scalling what so ever has to be applied.
	 *
	 * \param  X	The smaple locations where we evaluate the tree.
	 * 		 Must be in original data space.
	 *
	 * \throw 	This function throws if an internal fucntion could throw.
	 *
	 * \note 	\c x must be in original data space, this means no rescalling
	 * 		 what so ever has to be applied before the fucntion call.
	 *
	 * \note 	This fucntion operates on a sample array.
	 */
	PDFValueArray_t
	evaluateSamplesAt(
		const SampleCollection_t&	X)
	 const;


	/**
	 * \brief	This function evaluates the model for many samples at once.
	 *
	 * This fucntion is a bit more efficient than calling the evaluateSampleAt_noErr()
	 * function many times, since it computes some auxilery values only once.
	 *
	 * As the evaluateSampleAt_noErr() fucntion, this functions generates no exception
	 * if the sample lies outside the data range, but it simply sets the value to zero.
	 *
	 * Also the samples are to be exopected to be in the original data space, this means
	 * no scalling what so ever has to be applied.
	 *
	 * \param  X	The smaple locations where we evaluate the tree.
	 * 		 Must be in original data space.
	 *
	 * \throw 	This function throws if an internal fucntion could throw.
	 *
	 * \note 	\c x must be in original data space, this means no rescalling
	 * 		 what so ever has to be applied before the fucntion call.
	 *
	 * \note	This function operates on a smaple array.
	 */
	PDFValueArray_t
	evaluateSamplesAt_noErr(
	 	const SampleCollection_t&	X)
	 const;


	/*
	 * ==================
	 * Eigen querring functions
	 *
	 * These tpye operates on Eigen types.
	 * This functions does not support
	 * the multible names of the above functions.
	 * Instead they use a second argument
	 * to indicate which behaviour should be choosen.
	 */
public:
	/**
	 * \brief	This function operates on a sample array.
	 *
	 * It evaluates the pdf, definded by the tree estimater for all samples
	 * inside the sample array. The parameter beQuiet, which is by default set
	 * to false controls how the fucntion reacts if a sample is found that is
	 * outside the data space.
	 *
	 * If beQuite is set to false, default, an exception will be raised if
	 * a sample outside the data domain is found.
	 * If beQuiet is set to true, the value will be set to zero instead.
	 *
	 * \param  samples 	The samples that should be evaluated.
	 * \param  beQuiet	Controlls how samples outside the domain are handled.
	 *
	 * \note	This is afunction that is primarly written for the python wrapper.
	 */
	PDFValueArrayEigen_t
	evaluateSamplesAt_Eigen(
		const SampleArray_t&	samples,
		const bool 		beQuiet = false)
	 const;


	/**
	 * \brief	This function operates on a sample list.
	 *
	 * It evaluates the pdf, definded by the tree estimater for all samples
	 * inside the sample list. The parameter beQuiet, which is by default set
	 * to false controls how the fucntion reacts if a sample is found that is
	 * outside the data space.
	 *
	 * If beQuite is set to false, default, an exception will be raised if
	 * a sample outside the data domain is found.
	 * If beQuiet is set to true, the value will be set to zero instead.
	 *
	 * \param  samples 	The samples that should be evaluated.
	 * \param  beQuiet	Controlls how samples outside the domain are handled.
	 *
	 * \note	This is afunction that is primarly written for the python wrapper.
	 */
	PDFValueArrayEigen_t
	evaluateSamplesAt_Eigen(
		const SampleList_t&	samples,
		const bool 		beQuiet = false)
	 const;


	/**
	 * \brief	This function operates on a sample collection.
	 *
	 * It evaluates the pdf, definded by the tree estimater for all samples
	 * inside the sample collection. The parameter beQuiet, which is by default set
	 * to false controls how the fucntion reacts if a sample is found that is
	 * outside the data space.
	 *
	 * If beQuite is set to false, default, an exception will be raised if
	 * a sample outside the data domain is found.
	 * If beQuiet is set to true, the value will be set to zero instead.
	 *
	 * \param  samples 	The samples that should be evaluated.
	 * \param  beQuiet	Controlls how samples outside the domain are handled.
	 *
	 * \note	This is afunction that is primarly written for the python wrapper.
	 */
	PDFValueArrayEigen_t
	evaluateSamplesAt_Eigen(
		const SampleCollection_t&	samples,
		const bool 			beQuiet = false)
	 const;




	/*
	 * ===================
	 * Tree access function
	 */
public:
	/**
	 * \brief	THis function returns a leaf iterator of the tree.
	 *
	 * This function basically allows the iteration of all leafes
	 * of *this.
	 *
	 */
	SubLeafIterator_t
	getLeafIterator()
	 const;


	/**
	 * \brief	This function return the orginal data space.
	 *
	 * This is the data space that was estimated, or given at the
	 * creation of *this.
	 */
	const HyperCube_t&
	getGlobalDataSpace()
	 const;



	/*
	 * ===================
	 * Status functions
	 *
	 * this contains all the functions that
	 * informs about the status
	 */
public:
	/**
	 * \brief	Returns true if *this is a valid tree
	 *
	 * This function only performs some basic tests, without
	 * recursively descending the tree.
	 * if you whant to that then unse the checkIntegrity() function.
	 */
	bool
	isValid()
	 const
	 noexcept;

	/**
	 * \brief	Checks the tree recursevely.
	 *
	 * This function first check if *this is valid, by calling
	 * isValid(), then it will cals the checkIntegrityRecursively()
	 * fucntion that will descend to the tree and check the
	 * integrity.
	 *
	 * Note that depending on the tree this function can take a while
	 * to compute.
	 */
	bool
	checkIntegrity()
	 const
	 noexcept;


	/**
	 * \brief	This function test if teh tree is fully fitted.
	 *
	 * This function calls the isFullySplittedNOdeRecursively()
	 * function on the root node.
	 *
	 * This function should only return true after a fitt.
	 */
	bool
	isFullySplittedTree()
	 const;



	/**
	 * \brief	This function returns true if *this is consistent.
	 *
	 * *This is consistent, if the node at the top is not dirty.
	 * This means that no samples are inside the tree that where not yet
	 * incooperated into the estimator.
	 *
	 * \note	This fucntion quiereis the root node if it is dirty.
	 */
	bool
	isConsistent()
	 const;


	/**
	 * \brief	This function returns the mass of *this.
	 *
	 * The mass is the number of sampels that were used to construct
	 * *this. It is important that the number of actual samples
	 * that are located in here can be higher.
	 *
	 * To get the number of the samples that are totally stored inside
	 * *This use the getDirtyMass() function.
	 */
	Size_t
	getMass()
	 const;


	/**
	 * \brief	This function returns the dirty mass of *this.
	 *
	 * The dirty mass is the number of total samples that
	 * are stored inside *this. It is allways greater equal than
	 * the regular mass.
	 * If this si consistent they are equal.
	 */
	Size_t
	getDirtyMass()
	 const;


	/*
	 * =====================
	 * Topological fucntions
	 *
	 * These are functions that deals wioth the topology of the
	 * tree.
	 */
public:
	/**
	 * \brief	This function returns the number of nodes inside this.
	 *
	 * Please notice that this function performs a descent operation.
	 *
	 * \note	This function is implemented in terms of the composition fucntion.
	 */
	Size_t
	nNodes()
	 const;


	/**
	 * \brief	This function returns a depth map of *this.
	 *
	 * This function constructs a depth map, constructed with
	 * the root not owned by *this.
	 */
	DepthMap_t
	getDepthMap()
	 const;


	/**
	 * \brief	This function returns a tuble that encodes
	 * 		 the distribution of the different node types
	 * 		 of *this.
	 *
	 * - 1:	The number of true splits.
	 * - 2: The number of indirect splits.
	 * - 3: The number of leafes.
	 */
	::std::tuple<Size_t, Size_t, Size_t>
	getTreeComposition()
	 const;


	/**
	 * \brief	This function returns the dimension
	 * 		 of teh underling samples.
	 */
	Size_t
	nDims()
	 const
	 noexcept;




	/*
	 * =====================
	 * Priniting
	 */
public:
	/**
	 * \brief	This function print the constent of \c *this to the stream \c o.
	 *
	 * This outputing is done in a recursive way.
	 * All samples are outputed and then the two childeren are applied.
	 *
	 * \param  o	The stream that the function should write to.
	 * \param  noS	Setting this to true the samples are omitted; default to false.
	 *
	 * \return 	The stream is returned.
	 */
	std::ostream&
	printToStream(
		std::ostream&	o,
		bool 		noS = false)
	 const;


	/**
	 * \brief	This function prints *This to the file.
	 *
	 * This function is equivalent to the printToStream function
	 * with a file stream, but ios more convenient.
	 *
	 * \param  o	The stream that the function should write to.
	 * \param  noS	Setting this to true the samples are omitted; default to false.
	 */
	void
	printToFile(
		const std::string&	file,
		bool 			noS = false)
	 const;


	/*
	 * ======================
	 * Interal Functions
	 */
private:
	/**
	 * \brief	This function searches the leaf at which x is located in.
	 *
	 * This function takes a sample, in global data spcace and descent into the tree.
	 * It descends until it finds a leaf.
	 * The fucntion then returns a pointer to the leaf that was found (first) and
	 * the sample (second), the sample was rescalled such that it lies in
	 * the rescalled node data space, it describes the sample relative to
	 * the node, also in unit coordinates.
	 *
	 * It the sample lies outside the nullpointer is returned.
	 * The value of the sample is unspecific.
	 *
	 * \param  x	The sample locatation we search for; this must be in global data sapce.
	 *
	 * \throw 	The underling traversing functions tests and they may throw.
	 *
	 * \note 	This is an internal function
	 */
	cSampleDescentRet_t
	priv_findElementOf(
		const Sample_t&		x)
	 const;


	/**
	 * \brief	The same function as priv_findElementOf, but in a non const version
	 *
	 * \param  x	The sample locatation we search for
	 *
	 * \throw 	The underling traversing functions tests and they may throw.
	 *
	 * \note 	This is an internal function
	 */
	SampleDescentRet_t
	priv_findElementOf(
		const Sample_t&		x);


	/**
	 * \brief	This is an internal function that is used to evaluate the model.
	 *
	 * Given an sampleDescentRet_t object, this function evaluates the model
	 * that is associated to this function.
	 *
	 * This fucntion applies the global rescalling of the result.
	 * Meaning that the result is not in the rescalled root data space,
	 * but on the global data space.
	 *
	 * \param  dRes		The result of the descending operation.
	 *
	 * \throw	If dRes.first is the nullpointer.
	 *
	 * \note 	Despite its name this function is implemented in the
	 * 		 "tree/yggdrasil_treeImpl_PDFQuery.cpp" file.
	 */
	Real_t
	priv_evaluateSampleGlobal(
		const cSampleDescentRet_t& 	dRes)
	 const;


	/**
	 * \brief	This is an internal function that is used to evaluate the model.
	 *
	 * The main difference between this function and the priv_evaluateSampleGlobal()
	 * function is, that this function operates on the rescalled root data space.
	 * This means that no global correction is applied to the data.
	 *
	 * The intentiaon of thsi function is that this allow a better batch processing.
	 * It also takes the result of a descend operation.
	 *
	 * \param  dRes		The result of the descending operation.
	 *
	 * \throw	If dRes.first is the nullpointer.
	 *
	 * \note 	Despite its name this function is implemented in the
	 * 		 "tree/yggdrasil_treeImpl_PDFQuery.cpp" file.
	 */
	Real_t
	priv_evaluateSampleRootDS(
		const cSampleDescentRet_t& 	dRes)
	 const;


	/**
	 * \brief	This is an friend function, that delas with the evaluation of samples.
	 *
	 * Since all containers provide the UCI we can use templates to write one functions for all.
	 *
	 * \param  self		This is a pointer to the tree insatnce.
	 * \param  container	This is the container that should be used.
	 *
	 * \tparam Container_t	This is the type of the container.
	 * \tparam PDFVectot_t	This is the type of the pdf vector, it is basically the return type.
	 * \tparam noErr 	Bool use to activate the no error behaviour.
	 *
	 * \note	The type PDFVector_t must support operator[](Size_t i), size() and resize(newSize)
	 * 		 operation, meaning shgould be clear. As a side note it is not expected from the
	 * 		 resize operation to preserve the content.
	 * 		 As a constructor it must support PDFVector_t(size), which creates a vector instance of
	 * 		 size size, the content is unspecificed.
	 * 		 This restriction allows to use Eigen::Vector and std::vector at the same time.
	 */
	template<
		class 	Container_t,
		class   PDFVector_t,
		bool 	noErr
		>
	friend
	PDFVector_t
	ygInteranl_evaluateContainer(
		const yggdrasil_DETree_t* const 	self,
		const Container_t& 			conatiner);




	/*
	 * ======================
	 * Private Members
	 */
private:
	std::unique_ptr<Node_t> 		m_root;			//!< This is the root of the tree
	std::unique_ptr<DomainSplitter_i>	m_domainSplitter;	//!< This is the object that will perform the splitting
	HyperCube_t 				m_dataSpace;		//!< This is the domain that the tree was defined on.
									 //!< It is very important that this is the real data space and not a rescalled version.
									 //!< This is important to fully restore the rescalled values.
}; //End class: yggdrasil_DETree_t


YGGDRASIL_NS_END(yggdrasil)


