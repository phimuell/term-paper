/**
 * \file	tree/yggdrasil_treeImpl_priv.cpp
 * \brief 	This file cotains the implementation of the tree.
 *
 * This functions are for the descending.
 *
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>

#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>

//Include STD
#include <cmath>

YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_DETree_t::cSampleDescentRet_t
yggdrasil_DETree_t::priv_findElementOf(
	const Sample_t& 	sample)
 const
{
	//Ensure consistency of *this
	yggdrasil_assert(this->isValid() == true);
	yggdrasil_assert(this->m_dataSpace.isValid() == true);

	//Test if the sample is valid
	yggdrasil_assert(sample.isValid());
	yggdrasil_assert(sample.nDims() == m_dataSpace.nDims());

	//Test if the samples lies inside the data domain
	if(m_dataSpace.isInside(sample) == false)	//This will implicitly also checj the dimension
	{
		//The sample lies not inside the domain so we return the nullptr
		return ::std::make_pair(nullptr, Sample_t());
	}; //End lies outside of the domain

	//Now contract the sample to the root data space
	const Sample_t contractedSample = m_dataSpace.contractToUnit(sample);

	//Now we must test if we the root is a leaf,
	//for safty reasons the node refuses to check it itself.
	if(m_root->isLeaf() == true)
	{
		// We are allready at the leaf, so we can now return
		// We have to contract the sample
		return std::make_pair(cNode_ptr(m_root.get()), contractedSample);
	}; //ENd this is already a leaf

	//Now we use the standard descend function to fiund the leaf
	cNode_ptr foundLeaf = nullptr;
	Sample_t  nodeSample;

	//Search for it
	::std::tie(foundLeaf, nodeSample) = this->m_root->decendTo(contractedSample);

	//We will never found a ullpointer
	yggdrasil_assert(foundLeaf != nullptr);
	yggdrasil_assert(nodeSample.isInsideUnit());

	//We expect that leafs are fully splitted
	if(foundLeaf->isFullySplittedLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("A leaf was encountered, that is not fully fitted.");
	};

	//Return the found results
	return std::make_pair(foundLeaf, ::std::move(nodeSample));
}; //End: find the element


yggdrasil_DETree_t::SampleDescentRet_t
yggdrasil_DETree_t::priv_findElementOf(
	const Sample_t& 	sample)
{
	//Ensure consistency of this
	yggdrasil_assert(this->isValid() == true);
	yggdrasil_assert(this->m_dataSpace.isValid() == true);

	//ENsure consistency of teh sample
	yggdrasil_assert(sample.nDims() == m_dataSpace.nDims());
	yggdrasil_assert(sample.isValid());

	//Test if the samples lies inside the data domain
	if(m_dataSpace.isInside(sample) == false)	//This implicitly also checks the dimension
	{
		//The sample lies not inside the domain so we return the nullptr
		return ::std::make_pair(nullptr, Sample_t());
	}; //End lies outside of the domain

	//Now contract the sample to the root data space
	const Sample_t contractedSample = m_dataSpace.contractToUnit(sample);

	//Now we must test if we the root is a leaf,
	//for safty reasons the node refuses to check it itself.
	if(m_root->isLeaf() == true)
	{
		// We are allready at the leaf, so we can now return
		// We have to contract the sample
		return std::make_pair(Node_ptr(m_root.get()), contractedSample);
	}; //ENd this is already a leaf

	//Now we use the standard descend function to fiund the leaf
	Node_ptr foundLeaf = nullptr;
	Sample_t  nodeSample;

	//Search for it
	::std::tie(foundLeaf, nodeSample) = this->m_root->decendTo(contractedSample);

	//We will never found a ullpointer
	yggdrasil_assert(foundLeaf != nullptr);
	yggdrasil_assert(nodeSample.isInsideUnit());

	//We expect that leafs are fully splitted
	if(foundLeaf->isFullySplittedLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("A leaf was encountered, that is not fully fitted.");
	};


	//Return the found results
	return std::make_pair(foundLeaf, ::std::move(nodeSample));
}; //End: find the element


YGGDRASIL_NS_END(yggdrasil)

