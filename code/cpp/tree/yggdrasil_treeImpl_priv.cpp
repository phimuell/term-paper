/**
 * \file	tree/yggdrasil_treeImpl_priv.cpp
 * \brief 	This file cotains the implementation of the tree.
 *
 * This contains the code for the internal/private function of the tree.
 *
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>

#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeImpl.hpp>

//Include STD
#include <cmath>

YGGDRASIL_NS_BEGIN(yggdrasil)







YGGDRASIL_NS_END(yggdrasil)


















