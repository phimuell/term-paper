/**
 * \brief 	This file cotains the implementation of the tree.
 *
 * This file contains the fnctions that are concerned with the creation of the tree.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>

#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_treeNode_payload.hpp>

//Include STD
#include <cmath>

YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_DETree_t::~yggdrasil_DETree_t()
{
	//Currently only the root need to be deleted
	//delete m_root;	Is done by the unque pointer
	//m_root = nullptr;

}; //End: destructor



yggdrasil_DETree_t::yggdrasil_DETree_t(
	SampleCollection_t&	sampelCol,
	const HyperCube_t&	domain,
	const TreeBuilder_t&	treeBuilder,
	const bool 		moveSamples)
 :
  yggdrasil_DETree_t(
  	sampelCol,
  	domain,
  	treeBuilder.creatSplitStragety(),
  	treeBuilder.creatParModel(),
  	treeBuilder.creatGOFTester(),
  	treeBuilder.creatIndepTester(),
  	moveSamples)
{
	/*
	 * Nothing to do
	 */
};


yggdrasil_DETree_t::yggdrasil_DETree_t(
	const SampleArray_t&	arraySample,
	const HyperCube_t&	domain,
	const TreeBuilder_t&	treeBuilder)
 :
  yggdrasil_DETree_t(
  	SampleCollection_t(arraySample),
  	domain,
  	treeBuilder.creatSplitStragety(),
  	treeBuilder.creatParModel(),
  	treeBuilder.creatGOFTester(),
  	treeBuilder.creatIndepTester())
{
	/*
	 * Nothing to do
	 */
};


yggdrasil_DETree_t::yggdrasil_DETree_t(
	const SampleList_t&	sampleList,
	const HyperCube_t&	domain,
	const TreeBuilder_t&	treeBuilder)
 :
  yggdrasil_DETree_t(
  	SampleCollection_t(sampleList),
  	domain,
  	treeBuilder.creatSplitStragety(),
  	treeBuilder.creatParModel(),
  	treeBuilder.creatGOFTester(),
  	treeBuilder.creatIndepTester())
{
	/*
	 * Nothing to do
	 */
};



yggdrasil_DETree_t::yggdrasil_DETree_t(
	SampleCollection_t&				samples,
	const HyperCube_t& 				domain,
	const std::unique_ptr<DomainSplitter_i>&	splitFinder,
	const std::unique_ptr<ParModel_i>&		parModel,
	const std::unique_ptr<GOFTest_i>&		gofTester,
	const std::unique_ptr<IndepTest_i>&		indepTester,
	const bool 					moveSample)
 :
  m_root(nullptr),
  m_domainSplitter(splitFinder->creatOffspring()),
  m_dataSpace(domain)	//Make a copy of this
{
	//Save some data
	const auto nSamples = samples.nSamples();
	const auto nDims    = samples.nDims();

	//Now we call the constructor
	m_root = std::unique_ptr<yggdrasil_DETNode_t>(
		new yggdrasil_DETNode_t(
			samples,
			parModel.get(),
			gofTester.get(),
			indepTester.get(),
			&m_dataSpace,
			moveSample)
		);

	//Perform the initial fitting
	//This only performs the initial fitting on the root element
	m_root->priv_initialFitting_serial();

	//Perform the testing and splitting
	//This function goes doen and will also fit the newly created domain.
	m_root->priv_hypothesisTesterAndSplitter_serial(m_domainSplitter.get() );

#ifndef NDEBUG
	yggdrasil_assert(m_root->getMass() == nSamples);
	yggdrasil_assert(m_root->getDirtyMass() == nSamples);
	yggdrasil_assert(m_root->isDirty() == false);
	yggdrasil_assert(m_root->isOKNode() == true);
	yggdrasil_assert(m_root->checkIntegrity() == true);
	yggdrasil_assert(m_root->getDomain().isValid() == true);
	yggdrasil_assert(m_root->getDomain().nDims() == nDims);
	yggdrasil_assert(this->m_dataSpace.isValid() == true);
	yggdrasil_assert(this->m_dataSpace.nDims() == nDims);
	for(int i = 0; i != nDims; ++i)
	{
		yggdrasil_assert(m_root->getDomain().getUpperBound(i) == 1.0);
		yggdrasil_assert(m_root->getDomain().getLowerBound(i) == 0.0);
	}; //End for(i)

	//Test if everything if fully splitted
	for(SubLeafIterator_t it = this->getLeafIterator(); it.isEnd() == false; it.nextLeaf())
	{
		//Access the node
		const auto nF = it.access();

		//test if integer
		if(nF.checkIntegrity() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("After the construction of the tree found a leaf that is not okay.");
		};
		if(nF.isFullySplittedLeaf() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Found a leaf that is not fully splitted.");
		};
	}; //End for(it):
#endif
}; //End building constructor



yggdrasil_DETree_t::yggdrasil_DETree_t(
	SampleCollection_t				samples,
	const HyperCube_t& 				domain,
	const std::unique_ptr<DomainSplitter_i>&	splitFinder,
	const std::unique_ptr<ParModel_i>&		parModel,
	const std::unique_ptr<GOFTest_i>&		gofTester,
	const std::unique_ptr<IndepTest_i>&		indepTester)
 :
  m_root(nullptr),
  m_domainSplitter(splitFinder->creatOffspring()),
  m_dataSpace(domain)	//Make a copy of this
{
	//Save some data
	const auto nSamples = samples.nSamples();
	const auto nDims    = samples.nDims();

	//Now we call the constructor
	m_root = std::unique_ptr<yggdrasil_DETNode_t>(
		new yggdrasil_DETNode_t(
			samples,
			parModel.get(),
			gofTester.get(),
			indepTester.get(),
			&m_dataSpace,
			true)	//Allways move the imput
		);

	//Perform the initial fitting
	//This only performs the initial fitting on the root element
	m_root->priv_initialFitting_serial();

	//Perform the testing and splitting
	//This function goes doen and will also fit the newly created domain.
	m_root->priv_hypothesisTesterAndSplitter_serial(m_domainSplitter.get() );

#ifndef NDEBUG
	yggdrasil_assert(m_root->getMass() == nSamples);
	yggdrasil_assert(m_root->getDirtyMass() == nSamples);
	yggdrasil_assert(m_root->isDirty() == false);
	yggdrasil_assert(m_root->isOKNode() == true);
	yggdrasil_assert(m_root->checkIntegrity() == true);
	yggdrasil_assert(m_root->getDomain().isValid() == true);
	yggdrasil_assert(m_root->getDomain().nDims() == nDims);
	yggdrasil_assert(this->m_dataSpace.isValid() == true);
	yggdrasil_assert(this->m_dataSpace.nDims() == nDims);
	for(int i = 0; i != nDims; ++i)
	{
		yggdrasil_assert(m_root->getDomain().getUpperBound(i) == 1.0);
		yggdrasil_assert(m_root->getDomain().getLowerBound(i) == 0.0);
	}; //End for(i)

	//Test if everything if fully splitted
	for(SubLeafIterator_t it = this->getLeafIterator(); it.isEnd() == false; it.nextLeaf())
	{
		//Access the node
		const auto nF = it.access();

		//test if integer
		if(nF.checkIntegrity() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("After the construction of the tree found a leaf that is not okay.");
		};
		if(nF.isFullySplittedLeaf() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Found a leaf that is not fully splitted.");
		};
	}; //End for(it):
#endif
}; //End building constructor




YGGDRASIL_NS_END(yggdrasil)


















