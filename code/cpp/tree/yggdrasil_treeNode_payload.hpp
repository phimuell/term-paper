#pragma once
/**
 * \brief	This file contains the implementation of the payload that is used inside the node.
 * \file	tree/yggdrasil_treeNode_payload.hpp
 *
 * The payload is essentially the state variables that are to large and to infrequently used
 * to store them inside the node itself.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitCommand.hpp>

#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeNode_state.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>
#include <interfaces/yggdrasil_interface_GOFTest.hpp>
#include <interfaces/yggdrasil_interface_IndepTest.hpp>

//Include STD
#include <cmath>
#include <utility>
#include <memory>
#include <iostream>


YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \class	yggdrasil_DETNodePayload_t
 * \brief	This class implements the payload functionality for the node.
 *
 * The node was designed to be small and only contains the neccessary values.
 * everything that is very specific and is not needed at the nodes often is implemented
 * here in the payload.
 *
 * The payload should be considered the actual implmenetation of the node functionality.
 * However both have to work together.
 *
 * Please also see the discussion about the "m_dmain" member in the introduction
 * to the node object.
 *
 * The samples that are stored inside the collection of *this are expresed
 * in a rescalled node data space. Thus they lies inside the unit hyper cube.
 *
 *
 * \note 	The payload is niot meant to be used by the user.
 */
class yggdrasil_DETNodePayload_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Node_t 			= ::yggdrasil::yggdrasil_DETNode_t;		//!< This is the node type
	using Node_ptr 			= Node_t::Node_ptr;			//!< Pointer to a node
	using cNode_ptr 		= Node_t::cNode_ptr;			//!< Pointer to a constant node
	using SampleCollection_t 	= Node_t::SampleCollection_t;			//!< This is the class that stores the samples
	using DimensionArray_t 		= Node_t::DimensionArray_t;		//!< This is the class that stores the sperate dimension
	using Sample_t 			= Node_t::Sample_t;			//!< This is the class that represents a sample
	using HyperCube_t 		= Node_t::HyperCube_t;			//!< This is the hypercube type
	using IntervalBound_t 		= HyperCube_t::IntervalBound_t;
	using Payload_ptr 		= Node_t::Payload_ptr;			//!< This is the pointer to a paiload
	using NodeState_e 		= ::yggdrasil::yggdrasil_treeNode_e;	//!< This is the type for encoding the state

	using SplitDevice_t 	= Node_t::SplitDevice_t;		//!< This is the type of oject that is used to determine the spli
	using SplitDevice_ptr 	= Node_t::SplitDevice_ptr;		//!< Since it is an interface, only pointers are usefull
	using cSplitDevice_ptr 	= Node_t::cSplitDevice_ptr;		//!< Since it is an interface, only pointers are usefull

	using SingleSplit_t 	= Node_t::SingleSplit_t;		//!< This is a single split
	using SplitSequence_t 	= Node_t::SplitSequence_t;		//!< This is the sequence of splits that has to be performed

	using SubDomEnum_t 	= Node_t::SubDomEnum_t;			//!< This is for navigating in the splits
	using SubDomRange_t 	= Node_t::SubDomRange_t;		//!< This is for the subdomain ranges.
	using IndexArray_t 	= Node_t::IndexArray_t;			//!< This is the index array
	using IndexArray_cIt 	= IndexArray_t::const_iterator;		//!< This are constant iterators of the index array

	//This are the interfaces
	using GOFTest_ptr 	= std::unique_ptr<yggdrasil_GOFTest_i>;	//!< This is the interface of the GOF test
	using IndepTest_ptr 	= std::unique_ptr<yggdrasil_IndepTest_i>; //!< This is the interface for the independence test
	using ParModel_ptr 	= std::unique_ptr<yggdrasil_parametricModel_i>;	//!< This is the test for the parameteric model


	/**
	 * \brief	This is the test result that leads to the split
	 * \using 	TestResult_t
	 *
	 * It is either the test iof an independence test or a gof test.
	 */
	using TestResult_t 	= Node_t::TestResult_t;		//!< This is the test result
	using TestResult_ptr 	= Node_t::TestResult_ptr;	//!< This is the pointer to a test result.

	/**
	 * \using 	SampleIterator_t
	 * \brief	This iterator allows the iteration of all samples in a partuicular dimension.
	 *
	 * If one imagines the samples stored in a matrix such that each colum is a sample.
	 * Then this is an iterator to traverse the rows.
	 */
	using SampleIterator_t		= DimensionArray_t::SampleIterator_t;
	using cSampleIterator_t 	= DimensionArray_t::cSampleIterator_t; 	//!< Constant version of dimension iterator

	/**
	 * \using 	DimIterators_t
	 * \brief	This allows to iterate over the single dimensions.
	 *
	 * They gives access to the individual DimensionArray_ts
	 */
	using DimIterator_t 		= SampleCollection_t::DimIterator_t;
	using cDimIterator_t 		= SampleCollection_t::cDimIterator_t;		//!< This is the constant version

	//TODO: Iterator
	//TODO: Tests & Results

	/*
	 * ====================
	 * Constructors
	 *
	 * We only allow the building constructor and the destroctur.
	 *
	 * The other constructors are deleted.
	 */
public:
	/**
	 * \brief	This is the building constructor
	 *
	 * This function constructs *this in a semi valid state.
	 * It is basically empty
	 *
	 * \param  dims 	This is the diomension of the samples
	 */
	yggdrasil_DETNodePayload_t(
		const Int_t 	dims);


	/**
	 * \brief	This is the destructor of *this.
	 *
	 */
	~yggdrasil_DETNodePayload_t();


	/*
	 * ==================
	 * Delteted constructors
	 *
	 * We make them public to generate explicit warnings.
	 */
public:

	/**
	 * \brief	Copy constructor
	 *
	 * Is deleted
	 */
	yggdrasil_DETNodePayload_t(
		const yggdrasil_DETNodePayload_t&) = delete;


	/**
	 * \brief	Move constructor
	 *
	 * Is deleted
	 */
	yggdrasil_DETNodePayload_t(
		yggdrasil_DETNodePayload_t&&) = delete;


	/**
	 * \brief	Copy assignment
	 *
	 * Is deleted
	 */
	yggdrasil_DETNodePayload_t&
	operator= (
		const yggdrasil_DETNodePayload_t&) = delete;


	/**
	 * \brief	Move assignment
	 *
	 * Is deleted
	 */
	yggdrasil_DETNodePayload_t&
	operator= (
		yggdrasil_DETNodePayload_t&&) = delete;

	/**
	 * \brief	Default constructor.
	 *
	 * Is deleted
	 */
	yggdrasil_DETNodePayload_t()
	 = delete;



	/*
	 * ========================
	 * Samples Access
	 */
public:
	/**
	 * \brief	Get a reference to the Sample collection
	 */
	SampleCollection_t&
	getSampleCollection()
	 noexcept
	{
		return m_samples;
	};


	/**
	 * \brief	Get a const reference to the sample collection
	 */
	const SampleCollection_t&
	getSampleCollection()
	 const
	 noexcept
	{
		return m_samples;
	};


	/**
	 * \brief	Get a const reference to the sample collection; Explicit version
	 */
	const SampleCollection_t&
	cgetSampleCollection()
	 const
	 noexcept
	{
		return m_samples;
	};


	/**
	 * \brief	Get a reference to the ith dimensional array
	 */
	DimensionArray_t&
	getDim(
		const Int_t 	i)
	{
		return m_samples.getDim(i);
	};


	/**
	 * \brief	Get a constant reference to the ith dimensional array
	 */
	const DimensionArray_t&
	getDim(
		const Int_t 	i)
	 const
	{
		return m_samples.getDim(i);
	};


	/**
	 * \brief	Get a constant reference to the ith dimenisonal array; Explicit version
	 */
	const DimensionArray_t&
	getCDim(
		const Int_t 	i)
	 const
	{
		return m_samples.getCDim(i);
	};


	/**
	 * \brief	Rerturns the number of samples in *this
	 *
	 * Please note that this function is only usefull if the payload
	 * belongs to a leaf.
	 * In the case of a split this function returns zero.
	 *
	 * You can use the mass function to optain the numbers of samples that are below *this.
	 *
	 * \note 	This function does not take into account the update state of *this.
	 * 		 This means that this returns allways the number of stored samples inside *this.
	 * 		 Not the number of samples that where used to build the estimator.
	 * 		 If you want that number use the mass function.
	 */
	Size_t
	nSamples()
	 const
	 noexcept
	{
		return m_samples.nSamples();
	};


	/**
	 * \brief	Returns the bnumbers of the diomensions of the samples
	 *
	 * Please note that this function is only usefull if the payload
	 * belongs to a leaf.
	 */
	Size_t
	nDims()
	 const
	 noexcept
	{
		return m_samples.nDims();
	};


	/**
	 * \brief	Returns the mass of *this.
	 *
	 * The mass is the number of all samples below *this.
	 * For a leaf it is just the number of samples that are stored ionside *this.
	 * For a split it is the sum of the masses off all the leafe that are below *this.
	 *
	 * \note	The mass value is only updated if the update function is called.
	 */
	Size_t
	getMass()
	 const
	 noexcept
	{
		yggdrasil_assert(this->isDirty() ? (m_mass < m_dirtyMass) : (m_mass == m_dirtyMass));
		return m_mass;
	};


	/**
	 * \brief 	This function returns the dirty mass of *this.
	 *
	 * Since we allow inserting without imediatly updating the tree,
	 * the tree and thus *this can be in a dirty state.
	 * This fucntion returns the mass of the samples that are realy
	 * inside the tree and below *this.
	 *
	 * If the update functionis called this will be the new mass.
	 *
	 * \note 	If no insertion has been taken place since the
	 * 		 last updating/construction, this function returns
	 * 		 the same as the ordinary mass function.
	 */
	Size_t
	getDirtyMass()
	 const
	 noexcept
	{
		yggdrasil_assert(this->isDirty() ? (m_mass < m_dirtyMass) : (m_mass == m_dirtyMass));
		return m_dirtyMass;
	};


	/*
	 * =====================
	 * Sample manipulations
	 *
	 * Here are functions that modify the samples of *this
	 */
public:
	/**
	 * \brief	This function loads a sample collection into \c *this.
	 *
	 * Thius function acquieres the data and the domain of the node.
	 * The other memebers, like the model and the tests are handled
	 * in other functions, this function only deals with the samples.
	 *
	 * Depending in the value of the argument \c moveSamples, the samples
	 * are moved, meaning the argument will be empty afterwards
	 * or they are copied.
	 *
	 * If the domain is a valid domain, it is checked if the sampels
	 * lies inside this domain. For that the isInside() function
	 * is used, and no consideration of the upper bound is made.
	 *
	 * If the domain is invalid, the data is scanned and the size
	 * of the domain is estimated. In that case the estimated domain
	 * is also written back to the pointer location.
	 *
	 * This function then performs the rescalling of the data into
	 * the rescalled ropot data space.
	 *
	 * It is also important to note that the m_domain memeber of this
	 * is not set to the domain that was given/estimated, but to the
	 * unit cube.
	 *
	 *
	 * \param  samples 	The samples that are loaded.
	 * \param  domain 	The domain that is aquiered.
	 * \param  moveSamples 	If set to true the samples are moved and not copied.
	 */
	void
	loadSamplesAndDomain(
		      SampleCollection_t&	samples,
		      HyperCube_t* const 	domain,
		const bool 			moveSamples);





	/*
	 * =====================
	 * Access to the domain
	 */
public:
	/**
	 * \brief	This function returns a reference to the hypercube that
	 * 		 represents the domain of \c *this.
	 *
	 * The domain that is returned by this function is relative to the
	 * rescalled root data space. This means that no information
	 * about the original data space can be optained by this function.
	 * This is consitent with the R-Code but leads to a non consistant
	 * use as tiebreaker, that may not be important anyway.
	 *
	 * \throw	If this is a not okay node and the domain is not valid.
	 *
	 * \note	In previous incraation of this fucntion, there it
	 * 		 was different. The data was relative to the original
	 * 		 data space. Now This is not the case anymore.
	 */
	const HyperCube_t&
	getDomain()
	 const
	 noexcept
	{
		yggdrasil_assert(m_domain.isValid());
		return m_domain;
	};


	/**
	 * \brief	After the introduction of automatic rescalling, this fucntion
	 * 		 was demean obsolete and deleted.
	 */
	bool
	isInside(
		const Sample_t&)
	 const
	 = delete;


	/**
	 * \brief	After the introduction of automatic rescalling, this fucntion
	 * 		 was demean obsolete and deleted.
	 */
	bool
	isInside(
		const Int_t,
		const Numeric_t)
	 const
	 = delete;


	/**
	 * \brief	Deleted versiobn, increases safty
	 */
	bool
	isInside(
		const Numeric_t 	x,
		const Int_t 		d)
	 const
	 = delete;


	/**
	 * \brief	Was demean obsolete.
	 */
	bool
	isCloseToUpperBound(
		const Sample_t&,
		const Numeric_t)
	 const
	 = delete;


	/**
	 * \brief	Returns true if the domain of *this is valid
	 */
	bool
	isDomainValid()
	 const
	 noexcept
	{
		return m_domain.isValid();
	};


	/**
	 * \brief	Returns true if the split position is inside *this
	 *
	 * This function is technically also obsolete.
	 * But it was kept.
	 *
	 * \param  splitCom 	The split command
	 */
	bool
	isInside(
		const SingleSplit_t&	splitCom)
	 const
	{
		return m_domain.isInside(splitCom);
	};


	/**
	 * \brief	This function allows to exchange the domain of *this with newDomain.
	 *
	 * This is also a semiinternal function.
	 * That should not be used by the user.
	 * The exchange is only applied if the domain of *this is completly invalid.
	 *
	 * \throw 	If the domain is not completly invalid.
	 * 		 The new domain must be valid
	 *
	 * \param  newDomain 		The new domaion
	 */
	void
	exchangeDomain(
		const HyperCube_t& 	newDomain)
	{
		if(m_domain.allDimsInvalid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can only exchange a completly invalid domain.");
		};

		if(newDomain.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The new domain is not valid.");
		};

		//Exhange the domain
		m_domain = newDomain;

		return;
	}; //End: exchangeDomain




	/*
	 * ========================
	 * Payload query function
	 */
public:
	/**
	 * \brief	This function returns true if *this is valid.
	 *
	 * This fucntion checks if the payload is valid.
	 * Meaning that the objects are allocated that should be there.
	 * And also some further consistency tests are applied.
	 *
	 * Note that this function does not check if the samples inside this,
	 * in the case of a leafe, are inside the domain, since this
	 * is not realy possible.
	 *
	 * \note	Despite its name, this function does similar things as the
	 * 		 yggdrasil_DETNode_t::checkIntegrity() function.
	 */
	bool
	isValid()
	 const
	 noexcept;


	/**
	 * \brief	This function returns true if *this is dirty.
	 *
	 * A payload is dirty if, since the last uopdate insertions
	 * happened.
	 * This is like the mass and the dirty mass are not the same.
	 */
	bool
	isDirty()
	 const
	 noexcept
	{
		return m_isDirty;
	};


	/**
	 * \brief	This function returns true if *this is not dirty.
	 *
	 * This function is the inverse of the isDirty function.
	 * It is provided for syntactical reasons.
	 *
	 * \note	This is also the name of an OS developed by IBM.
	 */
	bool
	isCoherent()
	 const
	 noexcept
	{
		return !(this->isDirty());
	};



	/*
	 * =====================
	 * Node state functions
	 */
public:
	/**
	 * \brief	Returns true if \c *this is a true split.
	 */
	bool
	isTrueSplitState()
	 const
	 noexcept
	{
		return (m_nodeState == NodeState_e::TrueSplit) ? true : false;
	};


	/**
	 * \brief 	Returns true if \c *this is an indirect split.
	 */
	bool
	isIndirectSplitState()
	 const
	 noexcept
	{
		return (m_nodeState == NodeState_e::ImplicitSplit) ? true : false;
	};


	/**
	 * \brief	Return true if \c *this is in the state of a eaf
	 */
	bool
	isLeafState()
	 const
	 noexcept
	{
		return (m_nodeState == NodeState_e::Leaf) ? true : false;
	};


	/**
	 * \brief	Return true if \c *this has an Nodestate of INvalid
	 */
	bool
	isINVALIDState()
	 const
	 noexcept
	{
		return (m_nodeState == NodeState_e::INVALID) ? true : false;
	};


	/**
	 * \brief	This function tests if *thsi si fully splitted.
	 *
	 * *this is fully splitted if it is a leaf and the two tests,
	 * gof and independence test both hafe zero length split sequences.
	 *
	 * Note that this can change if more samples are added.
	 *
	 * Notice that if one of the tests is not associated
	 * also false is returned.
	 *
	 * \throw	If *this is not a leaf.
	 *
	 * \note	There was some kind of discussion if this should cause an
	 * 		 error or not. It was then descided that an exception is
	 * 		 a bit extreme for that case.
	 */
	bool
	isFullySplittedLeaf()
	 const;


	/**
	 * \brief	This function is conceptual similar to the isFullySplittedLeaf() fucntion.
	 * 		 But allows a broader range of applicability.
	 *
	 * If *this is a leaf, then it is the same as the isFullySPlittedLeaf().
	 *
	 * If *this is not a leaf it is tested if this node is plausible.
	 * This means that the test are made and splits are present.
	 *
	 * It is important that this function does not descend recurlively into the tree
	 * rooted at *this. for that use the recursive version
	 *
	 * This fucntion does not call the isVAlid() function.
	 *
	 * \note	The version of the node extends this functionality.
	 */
	bool
	isFullySplittedNode()
	 const;






	/*
	 * ===============
	 * Spliting functions
	 *
	 * These functions are devoted to perform the splitting.
	 */
private:
	/**
	 * \brief	This function has to be called before the actual split takes place.
	 *
	 * This function prepares *this for the split.
	 * It deletes all the components that should be deleted.
	 * Also the test results must be saved for the insertion
	 * This function is only called for the top split, meaning that the node
	 * is guarantted a leaf.
	 *
	 * This function will set *this to a true split.
	 * The mass is set to the dirty mass.
	 *
	 * This function does not chane the state of *this.
	 * Meaning that *this is still a leaf after the function returns.
	 *
	 * \param  testResults 		This is the test results that leaded to the split.
	 * \param  splitSeq 		This is the sequences of splits that have to be applied.
	 *
	 * \throw 	If the dirty mass and the number of samples are not the same.
	 * 		 Also if this is not a leaf.
	 */
	void
	priv_prepareSplits(
		const SplitSequence_t& 		splitSequence,
		TestResult_ptr 			testResults);


	/**
	 * \brief	This function is used to populate *this during the splitting process
	 *
	 * The payload must allready be allocated.
	 *
	 * Notice that *this is also called for splits.
	 * The function is not allowed to copy the samples.
	 * The samples are loaded in a later step.
	 *
	 * However the mass is updated.
	 * The mass is estimated from the size of the index array
	 * range that is passed to the function.
	 * The dirty mass is set to the same value.
	 * *this is also set to non dirty.
	 *
	 * Thi sfunction also dioes not copy the tests and the model.
	 * Since this are only needed in a leaf, they will be populated in the leaf.
	 *
	 * \param  rootPayload 		This is the payload of the root
	 * \param  testResults 		This is the test results
	 * \param  splitThusFar		This is the way of the splits
	 * \param  indexArray_start	Start of the index array
	 * \param  indexArray_end 	This is the (past the) end of the index array
	 * \param  thisDomain 		This is the domain of this
	 */
	void
	priv_populatePayload(
		const yggdrasil_DETNodePayload_t* const 	rootPayload,
		TestResult_ptr 					testResults,
		SubDomEnum_t 					splitThusFar,
		IndexArray_cIt 					indexArray_start,
		IndexArray_cIt 					indexArray_end,
		const HyperCube_t&				thisDomain);


	/**
	 * \brief	This function transforms *this from a leaf into a true split.
	 *
	 * This function is called by the Node_t::priv_transformToSplit function
	 * and marks the end of the splitting process.
	 *
	 * It will not update the mass, since this has been done already in the prepare function.
	 * But it will update the stet.
	 * This will set this to a true split node.
	 * \c *this must be a leaf.
	 *
	 * \param  sSplit 	This is the split that was computed.
	 * \param  newSubTree	This is the new subree rooted at *this.
	 *
	 * \throw 	If *this is not invalid.
	 * 		 payload must be valid.
	 * 		 if the arguments exceeds the range
	 */
	void
	priv_transformToSplit(
		const SingleSplit_t&	sSplit,
		Node_t* 		newSubTree);


	/**
	 * \brief	This function is used for the splitting.
	 *
	 * It will set set the state of *this to a leaf.
	 * It will also copy (creatOffsplring()) the gof and independence tests
	 * and the parameteric model.
	 *
	 * \param  spliTRoot 	The node that is the root of the splitting process.
	 * 			 This is the leaf taht was splitted
	 *
	 *
	 * \note 	This function is called by the function of the same name
	 * 		 inside the node.
	 *
	 * \throw 	*this must be a invalid state.
	 */
	void
	priv_setToLeaf(
		cNode_ptr 	splitRoot);


	/**
	 * \brief	This function trandforms *this into a split.
	 *
	 * This function will change the state of *this to an indirect split.
	 *
	 * \param  sSplit 	This is the split that was computed.
	 *
	 * \note 	This function is called by the function of the same name
	 * 		 inside the node.
	 *
	 * \note 	This function does not cehcks the arguments, it expects
	 * 		 that this has been done by the calling function.
	 *
	 * \throw	*this must be in an invalid state.
	 */
	void
	priv_setToIndirectSplit(
		const SingleSplit_t&	sSplit);


	/**
	 * \brief	This function copies the samples from the root payload into the payloads of the newly subtrees.
	 *
	 * This function is the finalizing of the spliting process.
	 * It will operate at one dimension at a time.
	 * This will ensure that, the storage requierement is keept low.
	 *
	 * This function will also perform some test on the newly created collections.
	 * If N_DEBUG is not defined, it is tested if the samples belongs to the right dimennions.
	 * This function only tests the samples.
	 * The validity of the payload is postponed.
	 *
	 * This function onyl distributes the samples from the big collection onto
	 * the sub collections that were created by the splitting.
	 * The rescalling was done by the function function of the that searches for the split.
	 *
	 * \param  subDomRanges		This is the subdomain ranges that where partitioned.
	 * \param  indexArray 		This is the index array that was used to partitioning the dimensions.
	 * \param  rootNode 		This is the root node, this is the node that conatins *this.
	 * \param  nThreads 		How many threads that are used; Currently ignored.
	 *
	 * \note 	This function will empty the sample collection of *this
	 *
	 * \throw 	If inconsistencies arrieses.
	 */
	void
	priv_distributeSamples(
		const SubDomRange_t&		subDomRange,
		const IndexArray_t&		indexArray,
		Node_ptr 			rootNode,
		const Int_t 			nThreads);


	/*
	 * =====================
	 * Private Printing function
	 */
private:
	/**
	 * \brief	This function is the private printing function of \c *this.
	 *
	 * It will print the content of the payload to the provided stream.
	 * This function assumes that \c *this is a leaf.
	 *
	 * \param  o 		The sream that is written to.
	 * \param  prefix	This is the prefix (accumulated intention).
	 * \param  inten	The intention character.
	 * \param  noS		Setting this to true, the samples are omitted.
	 *
	 * \return 	The stream is returned.
	 */
	std::ostream&
	priv_printToStream(
		std::ostream&	o,
		std::string 	prefix,
		std::string 	inten,
		bool 		noS)
	 const;





	/*
	 * ======================
	 * Iterator access functions
	 *
	 * This functions turns *this into a sample collection
	 */
public:
	/**
	 * \brief	Get an iterator to the first dimension
	 */
	DimIterator_t
	beginDim()
	  noexcept
	{
		return m_samples.beginDim();
	};


	/**
	 * \brief	Returns a constant iterator to the first dimension
	 */
	cDimIterator_t
	beginDim()
	  const
	  noexcept
	{
		return m_samples.cbeginDim();
	};


	/**
	 * \brief	Returns a constant iterator to the first diemnsion; explicit verison
	 */
	cDimIterator_t
	cbeginDim()
	  const
	  noexcept
	{
		return m_samples.cbeginDim();
	};


	/**
	 * \brief	Get an iterator to the past the end dimension
	 */
	DimIterator_t
	endDim()
	  noexcept
	{
		return m_samples.endDim();
	};


	/**
	 * \brief	Returns a constant iterator to the past the end dimension
	 */
	cDimIterator_t
	endDim()
	  const
	  noexcept
	{
		return m_samples.cendDim();
	};


	/**
	 * \brief	Returns a constant iterator to the past the end dimension; explicit verison
	 */
	cDimIterator_t
	cendDim()
	  const
	  noexcept
	{
		return m_samples.cendDim();
	};


	/*
	 * =====================
	 * 	Access to the samples
	 */
public:
	/**
	 * \brief	Returns an iterator the the first sample of the ith dimension
	 *
	 * \param  i 	The dimension to query
	 *
	 * \throw	If the dimenimension does not exists
	 */
	SampleIterator_t
	beginSample(
		const Int_t 	i)
	{
		return m_samples.beginSample(i);
	};


	/**
	 * \brief	Returns a constant iterator to the first sample of the ith dimension
	 *
	 * \param  i 	The dimension to query
	 *
	 * \throw 	If the dimension does not exists
	 */
	cSampleIterator_t
	beginSample(
		const Int_t 	i)
	  const
	{
		return m_samples.cbeginSample(i);
	};


	/**
	 * \brief	Returns a constant iterator to the first sample of the ith dimension; Explicit version
	 *
	 * \param  i 	The dimension to query
	 *
	 * \throw 	If the dimension does not exists
	 */
	cSampleIterator_t
	cbeginSample(
		const Int_t 	i)
	  const
	{
		return m_samples.cbeginSample(i);
	};


	/**
	 * \brief	Returns an iterator the the first sample of the ith dimension
	 *
	 * \param  i 	The dimension to query
	 *
	 * \throw	If the dimenimension does not exists
	 */
	SampleIterator_t
	endSample(
		const Int_t 	i)
	{
		return m_samples.endSample(i);
	};


	/**
	 * \brief	Returns a constant iterator to the first sample of the ith dimension
	 *
	 * \param  i 	The dimension to query
	 *
	 * \throw 	If the dimension does not exists
	 */
	cSampleIterator_t
	endSample(
		const Int_t 	i)
	  const
	{
		return m_samples.cendSample(i);
	};


	/**
	 * \brief	Returns a constant iterator to the first sample of the ith dimension; Explicit version
	 *
	 * \param  i 	The dimension to query
	 *
	 * \throw 	If the dimension does not exists
	 */
	cSampleIterator_t
	cendSample(
		const Int_t 	i)
	  const
	{
		return m_samples.cendSample(i);
	};



	/*
	 * ==========================
	 * Friend
	 *
	 * The node is the fiorend of this.
	 * This is natural, since the payload is just an extension of the node.
	 * However, this should be used only for certain cases
	 */
	friend class yggdrasil_DETNode_t;

	//friend class yggdrasil_DETree_t;

	friend
	Node_t*
	ygInternal_createNewSubTree(
		Node_t::IndexArray_t::iterator		indexArray_start,
		Node_t::IndexArray_t::iterator		indexArray_end,
		const Node_t::IndexArray_t::iterator	indexArray_base,
		Node_t::SubDomEnum_t 			splitThusFar,
		Node_t::SubDomRange_t* 			subDomainRanges,
		const Node_t::SplitSequence_t&		splitSeq,
		Node_t::TestResult_ptr			testResult,
		Node_t::cNode_ptr 			rootNodeWhereSplit,
		Node_t::SingleSplit_t* 			thisSingleSplit_out,
		Node_t::cSplitDevice_ptr		splitDeterminer,
		const Node_t::HyperCube_t&		parentDomain);

	/*
	 * ========================
	 * Private Variable
	 */
private:
	SampleCollection_t 	m_samples;	//!< This variable stores
	HyperCube_t 		m_domain;	//!< This is the domain of the element, relative to the root data space, which is rescalled.
	Size_t 			m_mass;		//!< This is the number of samples that are below *this; If this is a leaf, it is equal to nSamples()
	Size_t 			m_dirtyMass;	//!< This is the dirty mass of *this.
	bool 			m_isDirty;	//!< Indicates if *this is dirty.
	NodeState_e 		m_nodeState;	//!< This is the state of *this.

	//Statistics
	IndepTest_ptr 		m_indepTest;	//!< This is the pointer to the independence test
	GOFTest_ptr 		m_GOFTest;	//!< This is the GOF test
	ParModel_ptr 		m_parModel;	//!< This is the parameteric model

}; //End class: yggdrasil_DETNodePayload_t


YGGDRASIL_NS_END(yggdrasil)

