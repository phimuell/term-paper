#pragma once
/**
 * \brief	This file contains the implementation of the node, at least part of them
 * \file	tree/yggdrasil_treeNode.hpp
 *
 * It does not contains the full node.
 * For example the implementation of the sample container and the payload as a whole is missing.
 * These parts are implemented in an other part.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_taggedPointer.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitCommand.hpp>
#include <util/yggdrasil_subDomainRanges.hpp>
#include <util/yggdrasil_subDomainEnum.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_leafIterator.hpp>
#include <tree/yggdrasil_nodeFacace.hpp>

#include <interfaces/yggdrasil_interface_splitStrategy.hpp>
#include <interfaces/yggdrasil_interface_parametricModel.hpp>
#include <interfaces/yggdrasil_interface_GOFTest.hpp>
#include <interfaces/yggdrasil_interface_IndepTest.hpp>

//Include STD
#include <cmath>
#include <iostream>
#include <utility>


extern
bool
ygInternal_testSplitRoutine();


YGGDRASIL_NS_BEGIN(yggdrasil)

//Forward declaration of the payload
class yggdrasil_DETNodePayload_t;

//FWD of the tree
class yggdrasil_DETree_t;

/**
 * \class 	yggdrasil_DETNode_t
 * \brief	This class implements a node of the tree
 *
 * The node is implemented as a lightwight proxy.
 * It is designed such that it does not occupy much memory
 * and can thus be loaded quick.
 * If more content is needed the payload must be loaded.
 * The payload is not implemented here
 *
 * Since this class is allocated dynamically, it is not possible to
 * move or copy it.
 *
 * This is something like the third incranation of this class.
 * The data (samples) are now keept inside the range [0, 1[, if
 * this is a leaf.
 *
 * Each node, actually the payload maintains a hypercube
 * that defines the space that is occupied by the node.
 * However this hypercube is not with respect to the
 * original data space, but with respect to the rescalled
 * version, that is called root data space.
 * This is the space that is created when we map the
 * original data to the unit cube.
 *
 * This is consistent with the R-Code and I asked Daniel about
 * it and he told me to do it that way.
 * However doing that way there is a small problem that arises
 * when we use it as tioebreaker.
 * The problem is that due to the rescalling to the unit interval
 * we have lost the geometrical information. This means that
 * a length of lets say 0.001 in dimension i may not be the same
 * as the same distance along dimension j.
 * Since this is only used as a tie breaker, the influence will
 * not be that sevire, but it is not fully consistent
 * with the idea of the tie breaker that we prefere to cut longer
 * dimensions. It is true that the cut is now along the longest
 * dimension, but thius is done in the rescalled root data space
 * and not in the original data space.
 *
 * And the domain that is stored by the payload is exactly that.
 * The size the domain occupies in the rescalled data space, aka.
 * the (rescalled root data space), and not the original space.
 */
class yggdrasil_DETNode_t
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using Numeric_t 	= ::yggdrasil::Numeric_t;	//!< This is the type that is used for numeric
	using Node_t 		= ::yggdrasil::yggdrasil_DETNode_t; //!< This is the node type

	using Node_ptr 		= Node_t*;		//!< This is the pointer to a node
	using cNode_ptr 	= const Node_t*;	//!< This is a pointer to a constant node

	using NodeFacade_t 	= yggdrasil_nodeFacade_t;	//!< THis is the node facade

	using SampleCollection_t= ::yggdrasil::yggdrasil_sampleCollection_t; 	//!< This is the sample collection
	using DimensionArray_t  = SampleCollection_t::DimensionArray_t;		//!< This is a single dimension

	using HyperCube_t 	= ::yggdrasil::yggdrasil_hyperCube_t;	//!< This is the type for hypercubes, aka domain bounds
	using IntervalBound_t   = HyperCube_t::IntervalBound_t;		//!< This is the type to reprensent one interval

	using SplitDevice_t 	= yggdrasil_splitFinder_i;		//!< This is the type of oject that is used to determine the spli
	using SplitDevice_ptr 	= SplitDevice_t*;			//!< Since it is an interface, only pointers are usefull
	using cSplitDevice_ptr 	= const SplitDevice_t*;			//!< Since it is an interface, only pointers are usefull

	using SplitSequence_t 	= yggdrasil_splitSequence_t;		//!< This is the sequence of splits that has to be performed
	using SubDomRange_t	= yggdrasil_subDomainRanges_t;		//!< This are all the sequences that are build
	using IndexArray_t 	= ::yggdrasil::yggdrasil_indexArray_t;	//!< This is the inex array
	using SubDomEnum_t 	= yggdrasil_subDomainEnum_t;		//!< This are the enums to encodes the split

	using SingleSplit_t 	= yggdrasil_singleSplit_t;		//!< This is a single split.

	//This are the interfaces
	using GOFTest_i 	= yggdrasil_GOFTest_i;				//!< This is the interface to the GOF test
	using GOFTest_ptr 	= std::unique_ptr<GOFTest_i>;	//!< This is the interface of the GOF test
	using IndepTest_i 	= yggdrasil_IndepTest_i;		//!< This is the interface for the indepenence test
	using IndepTest_ptr 	= std::unique_ptr<IndepTest_i>; //!< This is the interface for the independence test
	using ParModel_i 	= yggdrasil_parametricModel_i;	//!< This is the interface of the parameteric model
	using ParModel_ptr 	= std::unique_ptr<ParModel_i>;	//!< This is the test for the parameteric model

	/**
	 * \brief	This is the test result that leads to the split
	 * \using 	TestResult_t
	 *
	 * It is either the test iof an independence test or a gof test.
	 */
	using TestResult_t 	= void*;
	using TestResult_ptr 	= TestResult_t*;	//!< Pointer to the test result


	//Payload
	//Never make the payload pointer to a unique pointer.
	using Payload_t 	= yggdrasil_DETNodePayload_t;	//!< This is the type for the payload; Note we generaly work with pointer
	using cPayload_t 	= const Payload_t;		//!< This is the const version of the payload
	using Payload_ptr 	= Payload_t*;			//!< This is the pointer type for the payload
	using Payload_ref 	= Payload_t&;			//!< This is the reference type of the payload
	using cPayload_ptr 	= const Payload_t*;		//!< This is a poninter to a constant opayload
	using cPayload_ref 	= const Payload_t&;		//!< This is a reference to a constant payload

	/**
	 * \brief	This is the type that is used to store the pointer of the two childs
	 */
	using ChildPtr_t 	= yggdrasil_taggedPointer_t<yggdrasil_DETNode_t, true>;

	using Sample_t 		= yggdrasil::yggdrasil_sample_t;//!< This is the type of the sample that is used

	/**
	 * \brief	Return type of some of the complexer descent functions.
	 *
	 * This is the return type of the descent fucntions that operates on a sample.
	 * Instead of simply returning the node pointer they also return the sample,
	 * in rescalled node specific data range, (unit interval).
	 */
	using SampleDescentRet_t = ::std::pair<Node_ptr, Sample_t>;

	/**
	 * \brief	This is the retunr type for the constant descend fucntions
	 */
	using cSampleDescentRet_t = ::std::pair<cNode_ptr, Sample_t>;


	//
	//Iterators

 	/**
 	 * \using 	DimIterator_t
 	 * \brief	Allows access to a dimension of all stored samples.
 	 *
 	 * This allows the iteration over a dimension of all stored samples.
 	 * Notice that this functionality id only prensent if *tis is a leafe.
 	 */
	using DimIterator_t 	= void*;

	using cDimIterator_t 	= void*;	//!< Const version of the dim iterator

	/**
	 * \using 	SubLeafIterator_t
	 * \brief	This allows teh iteration of all leafs beneath *this.
	 *
	 * This iterator allows the access of all leafes that are stored below *this.
	 * This functionality is allways present.
	 * If *this is a leaf one can only iterate over it.
	 */
	using SubLeafIterator_t		= yggdrasil_leafIterator_t;	//Iterator for accessing everything benith this


	/*
	 * ==================
	 * Private static variable
	 */
private:
	static const Size_t 	IDX_LEFT  = 0;	//!< This is the index for the left  node
	static const Size_t 	IDX_RIGHT = 1;	//!< This is the index for the right node

	/*
	 * ======================
	 * Constructors
	 *
	 * This class is unmovable, but we allow default constructor, which makes an invalid node
	 * A node can only be constructed as a leaf.
	 * A split can only be generated by spliting a leaf node.
	 *
	 * However there are _internal_ ways of constructing a split.
	 */
public:
	/**
	 * \brief	The default constructor.
	 *
	 * It creats an invalid node
	 */
	yggdrasil_DETNode_t()
	 noexcept;


	/**
	 * \brief	This is the building constructor.
	 *
	 * This construction is ment to construct the tree.
	 * It is very important that this constructor is mainly used for
	 * internal testing, the user should never use it.
	 *
	 * Nost of the arguments of this fucntion are slef
	 * explaining, but there are some that have a special meaning
	 * and intention.
	 *
	 * First of all the domain memeber which is a pointer.
	 * If the object that is pointer to by the pointer represents
	 * a valid hypercube this cube is used as bounding domain
	 * of the tree. In that case a test is performed to veryfy
	 * if the data that is given lies inside the domain.
	 * Notice that this check is dome by the isInside() fucntion
	 * of the hypercube and no correction for the "close to
	 * upper bound" is applied.
	 * If the domain is invalid, then the bounding domain
	 * of the data is estimated from the data itslef.
	 * It is the smallest domain which contains all the data.
	 *
	 * After the domain has been determined the found domain is
	 * written to the pointed location, however this will only
	 * happens in the case the domain had to be estimated from
	 * the data, in the case of a valid domain nothing happens.
	 *
	 * It is importaqnt that the domain membr that is associated
	 * to the constructed node (*this), is not the one that is
	 * returned/passed, but it is allways the unit cube.
	 * This is needed to be able to reach compability with R.
	 *
	 * In a second step the data is loaded. If the bool member is
	 * set to true, the data is moved in and not copied.
	 * However this will empty the given collection.
	 *
	 * Then a rescalling is apllied, which maps the data into the
	 * unit hyper cube.
	 *
	 * Then the model is applied and splitting and refitting
	 * untoill the model is accepted.
	 *
	 * The model are copied with the creatOffspring() fucntion.
	 *
	 *
	 * \param  Samples 	A sample collection that is accessed.
	 * \param  parModel	This is the stragety of the parameteric model.
	 * \param  gofTest	This is the test object for the gof tests.
	 * \param  indepTest	This is the independence tests.
	 * \param  domain	The domain or the support of the estimater; If invalid the domain is estimated;
	 * 			 This variable may also be modified.
	 * \param  moveSamples  Indicates if the data should be moved or copied.
	 *
	 * \throw	This function may throw if some compability conditions
	 *		 are violated or if an internal function throws.
	 */
	yggdrasil_DETNode_t(
		      SampleCollection_t&	Samples,
		const ParModel_i*		parModel,
		const GOFTest_i*		gofTest,
		const IndepTest_i*		indepTest,
		      HyperCube_t* const	domain,
		const bool 			moveSamples = false);


	/**
	 * \brief	The destructor is non trivaial
	 *
	 * The destructor only has an effect if *this is valid.
	 * It will then call the destructor of the child, then it will call
	 * the destructor of the payload in that order.
	 *
	 */
	~yggdrasil_DETNode_t();


	/**
	 * \brief	Copy constructor.
	 *
	 * This is deleted
	 */
	yggdrasil_DETNode_t(
		const yggdrasil_DETNode_t&) = delete;



	/**
	 * \brief	Copy assignment
	 *
	 * Is also deleted
	 */
	yggdrasil_DETNode_t&
	operator= (
		const yggdrasil_DETNode_t&) = delete;


	/**
	 * \brief	Move constructor
	 *
	 * Defined for internal use, thus it is private.
	 *
	 * The source is invalidated
	 */
private:
	yggdrasil_DETNode_t(
		yggdrasil_DETNode_t&& o);


	/**
	 * \brief	Move assignment
	 *
	 * For internal use, this assignment is implemented.
	 * The source o, is invalidated.
	 *
	 * This operator will throw an exception if *this is valid
	 */
	yggdrasil_DETNode_t&
	operator= (
		yggdrasil_DETNode_t&& o);



	/*
	 * =======================
	 * General function
	 */
public:
	/**
	 * \brief	Returns true if \c *this is ok node.
	 *
	 * A node that is ok is either a split or a leaf.
	 * This means that the split posiition is not infinity.
	 *
	 * This function is a light wight test that does not need much
	 * power to complete, but it is not able to detect some
	 * serious problems.
	 * Use the checkIntegrity() function for that.
	 *
	 *
	 * \note 	This function was previously named 'isValid()'.
	 * 		 However this leads to collitions with the check function
	 */
	bool
	isOKNode()
	 const
	 noexcept
	{
		return (::std::isinf(m_splitPos) == true) ? false : true;
	}; //End: isValid


	/**
	 * \brief	This function returns true if \c *this is integrity.
	 *
	 * A node which meats the integrity constraints, is a node
	 * that which has no obvious problems.
	 *
	 * The isOKNode() function only checks a small part of this aspect.
	 * This function checks the inegrity of the payload.
	 *
	 * \note 	This function does no recursive check on its child.
	 */
	bool
	checkIntegrity()
	 const
	 noexcept;


	/**
	 * \brief	This function applies the check integrity recorsively.
	 *
	 * This function realy goes down. A node passes this test, if itself,
	 * and all of its children pass the itegrity test.
	 * For a leaf this function is equivalent to the checkIntegrity() function.
	 */
	bool
	checkIntegrityRecursively()
	 const
	 noexcept;


	/**
	 * \brief	Returns true if *this indicates a leaf.
	 *
	 * As a remember a node is a leaf, iff m_splitPos is a NAN.
	 *
	 * The payload suports more powerfull state information.
	 */
	bool
	isLeaf()
	 const
	 noexcept
	{
		if(::std::isnan(m_splitPos) == true)
		{
			//A leaf does not have any childs
			yggdrasil_assert(m_child.isNull() == true);
			yggdrasil_assert(m_payload != nullptr);
			return true;
		}

		//We are not a leaf
		return false;
	}; //End: isLeaf


	/**
	 * \brief	This returns \c true if *this is a "true split".
	 *
	 * A true split is a split that was created directly from a leaf.
	 */
	bool
	isTrueSplit()
	 const
	 noexcept;


	/**
	 * \brief	Thsi fucntion returns \c true if *this is an "Indirect split".
	 *
	 * An indirect split is a split that is created indirecly, because
	 * we splitted a leaf into more than two parts.
	 */
	bool
	isIndirectSplit()
	 const
	 noexcept;


	/**
	 * \brief	This function returns true if \c *this is dirty.
	 *
	 * For this test the payload is checked if the associated dirty flag is set.
	 * A node is dirty if it needs an update.
	 * This happens after data was added to the tree and no update was
	 * perforemd yet.
	 *
	 * \throw 	If \c *this is not okay.
	 */
	bool
	isDirty()
	 const;


	/**
	 * \brief	This function returns true if *this is full splitted.
	 *
	 * This function can only be called on leafs, otherwhise an error is returned.
	 * This fucntion is a consistency test, but it does not throw if the
	 * test is not passed.
	 *
	 * Its functionality is supper simple. A node is fully splitted it is a leaf.
	 * This leaf must have zereo split sequence in both kind of tests.
	 * If this is the case true is returned.
	 * If thuis is not the case false is returned.
	 *
	 * Note that this can change if more samples are added.
	 *
	 * Notice that if one of the tests is not associated
	 * also false is returned.
	 *
	 * \throw	If *this is not a leaf.
	 *
	 * \note	There was some kind of discussion if this should cause an
	 * 		 error or not. It was then descided that an exception is
	 * 		 a bit extreme for that case.
	 */
	bool
	isFullySplittedLeaf()
	 const;


	/**
	 * \brief	This function is conceptual similar to the isFullySplittedLeaf() fucntion.
	 * 		 But allows a broader range of applicability.
	 *
	 * If *this is a leaf, then it is the same as the isFullySPlittedLeaf() with the addition
	 * that first the checkIntegrity() function is called.
	 *
	 * If *this is not a leaf it is tested if this node is plausible.
	 * This means that the test are made and splits are present.
	 *
	 * It is important that this function does not descend recurlively into the tree
	 * rooted at *this. for that use the recursive version
	 */
	bool
	isFullySplittedNode()
	 const;


	/**
	 * \brief	This is the recursive version of the isFullySplittedNode() function.
	 *
	 * It works by first calling the isFullySplittedNode() function on itself.
	 * The it calls the fucntion on its two children.
	 *
	 * \note	THis function is implemented in the generall cpp file of this class.
	 */
	bool
	isFullySplittedNodeRecursively()
	 const;


	/*
	 * ====================
	 * Topological function
	 */
public:
	/**
	 * \brief	This function returns the split possition.
	 *
	 * It is importnat that the split position is with respect to the
	 * hypercube [0, 1[^d, that si occupied by this and not the
	 * original spliting position!
	 *
	 * \throws	If this is not a split.
	 */
	Numeric_t
	getSplitPosition()
	 const
	{
		yggdrasil_assert(this->isOKNode() == true);

		if(this->isLeaf() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A leaf does not have a split position.");
		};

		return m_splitPos;
	}; //End get split position


	/**
	 * \brief	This function returns the doimension in which the split is.
	 *
	 * This function is only defined if *this is a split.
	 */
	Int_t
	getSplitDimension()
	 const
	{
		yggdrasil_assert(this->isOKNode() == true);

		if(this->isLeaf() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A leaf does not have a split position.");
		};


		//Extract it
		const Int_t splitDim = m_child.getTag();

		return splitDim;
	}; //End: getSPlitDimension



	/*
	 * ===================
	 * Get subtree topological functions
	 *
	 * THe fucntions operates more
	 * on *This as a subtree.
	 */
public:
	/**
	 * \brief	This function returns a tuble that encodes
	 * 		 the distribution of the different node types
	 * 		 of *this.
	 *
	 * - 1:	The number of true splits.
	 * - 2: The number of indirect splits.
	 * - 3: The number of leafes.
	 */
	::std::tuple<Size_t, Size_t, Size_t>
	getSubTreeComposition()
	 const;



	/*
	 * =====================
	 * Priniting
	 */
public:
	/**
	 * \brief	This function print the constent of \c *this to the stream \c o.
	 *
	 * This outputing is done in a recursive way.
	 * All samples are outputed and then the two childeren are applied.
	 *
	 * \param  o	The stream that the function should write to.
	 * \param  noS	Setting this to true the samples are omitted; default to false.
	 *
	 * \return 	The stream is returned.
	 */
	std::ostream&
	printToStream(
		std::ostream&	o,
		bool 		noS = false)
	 const;


	/**
	 * \brief	This is a private helper function for the printing.
	 *
	 * This function is mainly used to provided the intention of the output.
	 */
private:
	std::ostream&
	priv_printToStream(
		std::ostream&	o,
		std::string 	prefix,
		std::string 	inten,
		bool 		noS)
	 const;



	/*
	 * =================
	 * Descending funtion.
	 *
	 * These functions allow the descending into the tree
	 */
public:
	/**
	 * \brief	This is a decent function that takes
	 * 		 a subdomain enumeration type for the job.
	 *
	 * The enum is a way to encode a dewscend operation into the tree.
	 * It is limited in the way that it is only able to descent at
	 * most 30 levels down.
	 * A further limitation is that one can only go left or right.
	 *
	 * This fucntion is mainly used internaly when the samples
	 * are redistributed when a split happens.
	 *
	 * We allow that a descend operation of depth zero, meaning
	 * that we do not descent at all, in which case *this
	 * is returned.
	 * However we never allow that this function is called on
	 * a leaf, even in the case that we have a zero descent.
	 *
	 * \param  targetDom	This is the target domain
	 *
	 * \throw 	If an error occured, or *this is a leaf.
	 *
	 * \note 	Note descend is not a spelling mistake it is a feature.
	 */
	Node_ptr
	decendTo(
		const SubDomEnum_t&	targetDomain);



	/**
	 * \brief	This is a decent function that takes
	 * 		 a subdomain enumeration type for the job.
	 *
	 * This is the constant version.
	 *
	 * the enum is a way to encode a dewscend operation into the tree.
	 * it is limited in the way that it is only able to descent at
	 * most 30 levels down.
	 * a further limitation is that one can only go left or right.
	 *
	 * this fucntion is mainly used internaly when the samples
	 * are redistributed when a split happens.
	 *
	 * we allow that a descend operation of depth zero, meaning
	 * that we do not descent at all, in which case *this
	 * is returned.
	 * however we never allow that this function is called on
	 * a leaf, even in the case that we have a zero descent.
	 *
	 * \param  targetdom	this is the target domain
	 *
	 * \throw 	if an error occured, or *this is a leaf.
	 *
	 * \note 	note descend is not a spelling mistake it is a feature.
	 */
	cNode_ptr
	decendTo(
		const SubDomEnum_t&	targetDomain)
	 const;



	/**
	 * \brief	This is a decent function that takes
	 * 		 a subdomain enumeration type for the job.
	 *
	 * This is the explicit constant version.
	 *
	 * the enum is a way to encode a dewscend operation into the tree.
	 * it is limited in the way that it is only able to descent at
	 * most 30 levels down.
	 * a further limitation is that one can only go left or right.
	 *
	 * this fucntion is mainly used internaly when the samples
	 * are redistributed when a split happens.
	 *
	 * we allow that a descend operation of depth zero, meaning
	 * that we do not descent at all, in which case *this
	 * is returned.
	 * however we never allow that this function is called on
	 * a leaf, even in the case that we have a zero descent.
	 *
	 * \param  targetdom	this is the target domain
	 *
	 * \throw 	if an error occured, or *this is a leaf.
	 *
	 * \note 	note descend is not a spelling mistake it is a feature.
	 */
	cNode_ptr
	cdecendTo(
		const SubDomEnum_t&	targetDomain)
	 const;


	/**
	 * \brief	This function performs the descending into the tree.
	 *
	 * It expects that its argument is inside the unit hypercube.
	 * The function will apply the rescalling needed for
	 * the descending automatically.
	 *
	 * It is considered an error to call this function on a leaf.
	 *
	 * \return 	It will return a pointer to the leaf which contains the original sample.
	 *
	 * \param  s	The position that is examined.
	 *
	 * \throw	If the sample is not inside the unit hypercube.
	 * 		 Also if *this happens to be a leaf.
	 */
	SampleDescentRet_t
	decendTo(
		const Sample_t& 	s);


	/**
	 * \brief	This function performs the descending into the tree.
	 *
	 * It is the constant version.
	 *
	 * It expects that its argument is inside the unit hypercube.
	 * The function will apply the rescalling needed for
	 * the descending automatically.
	 *
	 * It is considered an error to call this function on a leaf.
	 *
	 * \return 	It will return a pointer to the leaf which contains the original sample.
	 *
	 * \param  s	The position that is examined.
	 *
	 * \throw	If the sample is not inside the unit hypercube.
	 * 		 Also if *this happens to be a leaf.
	 */
	cSampleDescentRet_t
	decendTo(
		const Sample_t& 	s)
	 const;


	/*
	 * =====================
	 * Accesser function
	 *
	 * This function allows some access to this
	 */
public:

	/**
	 * \brief	Get a pointer to the right child
	 *
	 * \thorw 	Throws if *this is a leaf
	 */
	Node_ptr
	getRightChild()
	{
		if(this->isLeaf() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A leaf does not have a right child.");
		};

		return m_child.get() + IDX_RIGHT;
	};


	/**
	 * \brief	Get a pointer to the right child, the child is constant
	 *
	 * \thorw 	Throws if *this is a leaf
	 */
	cNode_ptr
	getRightChild()
	 const
	{
		if(this->isLeaf() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A leaf does not have a right child.");
		};

		return m_child.get() + IDX_RIGHT;
	};


	/**
	 * \brief	Get a pointer to the right child, the child is constant
	 *
	 * This is the explicit veriosn
	 *
	 * \thorw 	Throws if *this is a leaf
	 */
	cNode_ptr
	cgetRightChild()
	 const
	{
		if(this->isLeaf() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A leaf does not have a right child.");
		};

		return m_child.get() + IDX_RIGHT;
	};




	/**
	 * \brief	Get a pointer to the left child
	 *
	 * \thorw 	Throws if *this is a leaf
	 */
	Node_ptr
	getLeftChild()
	{
		if(this->isLeaf() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A leaf does not have a left child.");
		};

		return m_child.get() + IDX_LEFT;
	};


	/**
	 * \brief	Get a pointer to the left child, the child is constant
	 *
	 * \thorw 	Throws if *this is a leaf
	 */
	cNode_ptr
	getLeftChild()
	 const
	{
		if(this->isLeaf() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A leaf does not have a left child.");
		};

		return m_child.get() + IDX_LEFT;
	};


	/**
	 * \brief	Get a pointer to the left child, the child is constant
	 *
	 * This is the explicit veriosn
	 *
	 * \thorw 	Throws if *this is a leaf
	 */
	cNode_ptr
	cgetLeftChild()
	 const
	{
		if(this->isLeaf() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A leaf does not have a left child.");
		};

		return m_child.get() + IDX_LEFT;
	};



	/*
	 * ===========================
	 * ==========================
	 * Payload functions
	 *
	 * Here are all the functions that are associated
	 * with the payload.
	 */

	/*
	 * ===========================
	 * Domain Functions
	 */
public:
	/**
	 * \brief	This function was declared obsolet and removed.
	 *
	 * This function was removed, when the rescalling was introduced to the node.
	 * Since it is not clear anymore what this fucntion should test.
	 * Is it inside the real subspace that is saved inside or not.
	 */
	bool
	isInside(
		const Sample_t&)
	 const
	 = delete;


	/**
	 * \brief	This function was declared obsolet and removed.
	 *
	 * This function was removed, when the rescalling was introduced to the node.
	 * Since it is not clear anymore what this fucntion should test.
	 * Is it inside the real subspace that is saved inside or not.
	 */
	bool
	isCloseToUpperBound(
		const Sample_t&,
		const Numeric_t)
	 const
	 = delete;


	/**
	 * \brief	This function returns a reference to the hypercube that
	 * 		 represents the domain of \c *this, this domain is
	 * 		 also known as node data space.
	 *
	 * The domain that is returned by this function is relative to the
	 * rescalled root data space (~ unit cube). This means that no information
	 * about the original data space can be optained by this function.
	 * This is consitent with the R-Code but leads to a non consistant
	 * use as tiebreaker, that may not be important anyway.
	 *
	 * \throw	If this is a not okay node and the domain is not valid.
	 *
	 * \note	In previous incraation of this fucntion, there it
	 * 		 was different. The data was relative to the original
	 * 		 data space. Now This is not the case anymore.
	 */
	const HyperCube_t&
	getDomain()
	 const;



	/*
	 * =================
	 * Sample functions
	 */
public:
	/**
	 * \brief	This function returns a reference to the underling sample container.
	 *
	 * This function returns a non constant reference.
	 * *this must be a leaf.
	 *
	 * Note that the samples that are stored in the collection are in the
	 * rescalled node data space, which means that they are rescalled to
	 * lie inside the unit cube.
	 *
	 * \throw 	If \c *this is not a leaf.
	 */
	SampleCollection_t&
	getSampleCollection();


	/**
	 * \brief	This function returns a constant reference to the underling sample container.
	 *
	 * This function is the constant version of the getSampleCollection() function.
	 * \c *this must be a leaf.
	 *
	 * Note that the samples that are stored in the collection are in the
	 * rescalled node data space, which means that they are rescalled to
	 * lie inside the unit cube.
	 *
	 * \throw 	If \c *this is not a leaf.
	 */
	const SampleCollection_t&
	getSampleCollection()
	 const;


	/**
	 * \brief	This function returns a constant reference to the underling sample container.
	 *
	 * This function is the explicit version of the getSampleCollection() function.
	 * \c *this must be a leaf.
	 *
	 * Note that the samples that are stored in the collection are in the
	 * rescalled node data space, which means that they are rescalled to
	 * lie inside the unit cube.
	 *
	 * \throw 	If \c *this is not a leaf.
	 */
	const SampleCollection_t&
	getCSampleCollection()
	 const;


	/**
	 * \brief	This function returns a reference to the dimensional
	 * 		 array of dimension \c d of the sample collection of *this.
	 *
	 * Note that the samples that are stored are expresed in the rescalled node
	 * data space, thus lies in a unit hyper cube.
	 *
	 * \param  d	The dimension we want.
	 *
	 * \throw 	If *this is not a leaf, or the dimension does not ecists.
	 */
	DimensionArray_t&
	getDim(
		const Int_t 	d);


	/**
	 * \brief	This function returns a reference to the dimensional
	 * 		 array of dimension \c d of the sample collection of *this.
	 *
	 * This is the implicit const version.
	 *
	 * Note that the samples that are stored are expresed in the rescalled node
	 * data space, thus lies in a unit hyper cube.
	 *
	 * \param  d	The dimension we want.
	 *
	 * \throw 	If *this is not a leaf, or the dimension does not ecists.
	 */
	const DimensionArray_t&
	getDim(
		const Int_t 	d)
	 const;


	/**
	 * \brief	This function returns a reference to the dimensional
	 * 		 array of dimension \c d of the sample collection of *this.
	 *
	 * This is the explicit const version.
	 *
	 * Note that the samples that are stored are expresed in the rescalled node
	 * data space, thus lies in a unit hyper cube.
	 *
	 * \param  d	The dimension we want.
	 *
	 * \throw 	If *this is not a leaf, or the dimension does not ecists.
	 */
	const DimensionArray_t&
	getCDim(
		const Int_t 	d)
	 const;


	/**
	 * \brief	This fucntion returns the number of dimensions.
	 *
	 * For this the sample collection is queried.
	 */
	Size_t
	nDims()
	 const;


	/**
	 * \brief	This fucntion returrns the number of samples that are
	 * 		 stored in the underling sample collection.
	 *
	 * For this end, ths sample collection is queried.
	 *
	 * \note 	In theory this fucntion should return the same value as
	 * 		 the getDirtyMass() does.
	 */
	Size_t
	nSamples()
	 const;




	/**
	 * \brief	This functin returns the mass of \c *this .
	 *
	 * The mass is the number of samples, that was used to build \c *this.
	 * To put it differently, this many samples where used to fit and test \c *this.
	 *
	 * If *this is not dirty, then this number is the same as the number of
	 * samples that are stored in the scample collections, that are beneath
	 * *this.
	 *
	 * \throw	If *this is not okay.
	 */
	Size_t
	getMass()
	 const;


	/**
	 * \brief	This returns the dirty mass of \c *this.
	 *
	 * The dirty mass is simillar, but different meaning than the
	 * the normal mass, that can be accessed by getMass().
	 *
	 * As stated in the description of getMass() that value is not necessaraly
	 * equal to the number of samples beneath *this.
	 * But for the dirty mass this is allways the case.
	 *
	 * If *this is not dirty the two masses are the same.
	 *
	 * The meaning of the dirty mass is the number of samples that passes through
	 * *this but where not used to build the estimator.
	 *
	 * the dirty mass will become the new mass once the update has been done.
	 */
	Size_t
	getDirtyMass()
	 const;


	/*
	 * ====================
	 * Statistical tests
	 *
	 * These fucntions gives access to statistical tests
	 */
public:
	/**
	 * \brief	Returns a pointer to the GOF test object.
	 *
	 * Notioce that this function could potentially
	 * return the nullpointer, in the case that *this is an
	 * indiorect split, but in that case the fucntion will
	 * generate an exception.
	 *
	 * \throw	If no GOF test is present.
	 */
	const GOFTest_i*
	getGOFTest()
	 const;


	/**
	 * \brief	THis fucntion returns true if *this
	 * 		 has an associated GOF test.
	 *
	 * In theory this fucntion is an alias of isIndirectSplitState().
	 * But in my view it is better to provide a fucntion that
	 * is named explicit.
	 */
	bool
	hasGOFTest()
	 const;


	/**
	 * \brief	This function returns true if the GOF Test of
	 * 		 *this is associated.
	 *
	 * \note	In a bvalid tree the user can expect that this
	 * 		 function returns true for all node, excepts
	 * 		 the ones that are indirect splits.
	 *
	 * \throw	If *thsi does not have a GOF test.
	 */
	bool
	isGOFTestAssociated()
	 const;


	/**
	 * \brief	This fucntion returns true if *this has an
	 * 		 independence test.
	 *
	 * *this does not have an independence test, if it is an
	 * indirect split.
	 */
	bool
	hasIndepTest()
	 const;


	/**
	 * \brief	This function returns a pointer to the
	 * 		 independence test of *this.
	 *
	 * This function throws if *this does nbot have an
	 * independence test, this could be the case if
	 * *this is an indirect split
	 *
	 * \throw	If *this does not have an independence test.
	 */
	const IndepTest_i*
	getIndepTest()
	 const;


	/**
	 * \brief	This fucntion returns true if the independence
	 * 		 test of *this is associated.
	 *
	 * \note	In a valid tree it is not neccessaraly the case
	 * 		 that this function returns true for all nodes,
	 * 		 that has one. This function returns ture for all
	 * 		 leafs, but it may return false if the node
	 * 		 is an true split.
	 *
	 * \throw	If this does not have an associated indpoendende
	 * 		 test.
	 */
	bool
	isIndepTestAssociated()
	 const;


	/*
	 * ========================
	 * Parameteric model
	 *
	 * This functions allows access to the parameteric model
	 */
public:
	/**
	 * \brief	This function returns true if *this has a parameteric model.
	 *
	 * \note	All node types, excedpt the indirect splits will have a model.
	 */
	bool
	hasParModel()
	 const;


	/**
	 * \brief	This function returns a pointer to the parameteric model.
	 *
	 * \throw 	If *this does not have one.
	 */
	const ParModel_i*
	getParModel()
	 const;

	/**
	 * \brief	Returns true if the parameteric model is fully fitted.
	 *
	 * \note	That this function test if the parameteric model is fitted.
	 * 		 This function implies that if its value is true,
	 * 		 that the model is associated, the reverse is not true.
	 *
	 * \throw	If *this does not have one.
	 */
	bool
	isParModelFitted()
	 const;








	/*
	 * ===================
	 * Chilren functions
	 */
public:
	/**
	 * \brief	Thsi function creates an leaf iterator with \c *thsi as root.
	 *
	 * Thsi function requieres only that *thsi si valid.
	 */
	SubLeafIterator_t
	getLeafIterator()
	 const
	{
		if(this->isOKNode() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not construct a leaf iterator from a node that is not OKAY.");
		};

		return SubLeafIterator_t(this);
	}; //End getLeafIterator


	/**
	 * \brief	This function returns a node facade that points to *this.
	 *
	 * This function is a handy conversion function.
	 *
	 */
	NodeFacade_t
	makeFacade()
	 const
	{
		if(this->isOKNode() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not construct a nodeFacde from a non okay node.");
		};

		return NodeFacade_t(this);
	}; //ENd: makeFacade





	/*
	 * ===================
	 * ====================
	 * Split function
	 *
	 * These functions are not defined in the header but in the cpp file.
	 * These are mainly internal function the user should never use them.
	 */
private:
	/**
	 * \brief	This function performs the splittig.
	 *
	 * After a test is performed and some of them where negative this function,
	 * takes a split sequence and performs the splitting.
	 *
	 * It will modify *this such that it is now an internal node.
	 *
	 * It is important that the estimators are not recalculated.
	 * This process must be initated by the caller himself.
	 *
	 * The split locations are determined by the splitting interface
	 *
	 * \param  splitSeq 	This is the sequence (list of dimensions) of splits that has to be performed.
	 * \param  testResults 	This are the results that leaded to the splits of *this.
	 * \param  splitDeterminer 	This is the object that determines the split location
	 *
	 * \throw	This function may throw.
	 *
	 * \note 	This function does not rebuild the estimateors.
	 * 		 This is only a first version, where we only perform one
	 * 		 split and not two, as we should do
	 */
	void
	priv_splitNode(
		const SplitSequence_t&		splitSeq,
		TestResult_ptr			testResult,
		cSplitDevice_ptr		splitDeterminer);


	/**
	 * \brief	This function is used for the splitting.
	 *
	 * It only works on invalid nodes.
	 * It takes the payload and adopts it.
	 * It will turn itself into a leafe, however the estimator is not
	 * calculated yet, this is postponed.
	 *
	 * \param rootNode 	The node where the split originates.
	 *
	 * \note 	This function changes its argument to the null pointer
	 *
	 * \throw 	The payload must be valid.
	 * 		 Also *this must be invalid.
	 */
	void
	priv_setToLeaf(
		const yggdrasil_DETNode_t*	rootNode);


	/**
	 * \brief	This function trandforms *this into a split.
	 *
	 * The payload must be set.
	 * This function is only for invalid nodes.
	 * On a technical level, this function transforms *this into
	 * an indirect split.
	 *
	 * \param  sSplit 	This is the split that was computed.
	 *
	 * \throw 	If *this is not invalid.
	 * 		 payload must be valid
	 * 		 if the arguments exceeds the range
	 */
	void
	priv_setToSplit(
		const SingleSplit_t&	sSplit);


	/**
	 * \brief	This function transforms a leaf into a split
	 *
	 * It is important that *this must be a leaf.
	 * This function must be called bfore the samples are distributed.
	 *
	 * It is important that the Split location that is passed in the
	 * variable \c sSplit is in rescalled node coordinates.
	 * This means it is in the interval [0, 1[, relative
	 * to the domain *this occupies.
	 *
	 * \param  sSplit 	This is the split that was computed;
	 * 			 in rescalled node coordinates
	 * \param  newSubTree	This is the new subree rooted at *this.
	 *
	 * \throw 	If *this is not invalid.
	 * 		 payload must be valid.
	 * 		 if the arguments exceeds the range
	 */
	void
	priv_transformToSplit(
		const SingleSplit_t&	sSplit,
		Node_t* 		newSubTree);



	/**
	 * \brief	This function allocates the payload of *this
	 *
	 * This function is for internal use.
	 * It allocates the memory for the payload
	 * This is an internal function.
	 *
	 * \param  sampleDimension 	This is the dimeneion of the samples
	 */
	void
	priv_allocPayload(
		const Int_t 	sampleDimension);


	/**
	 * \brief	Thif function performs a final consistency tests.
	 *
	 * It is very importnat that this fuction dies not throw if the test fails.
	 * This is only reflects in the return value.
	 *
	 * \param  splitSeq	This is teh sequences of split that was performed.
	 *
	 * \return 	If the test fails, then false is returned.
	 *
	 * \throw 	This fucntion may throw because of some underling functions.
	 */
	bool
	priv_finalTestsAfterSplit(
		const SplitSequence_t&		splitSeq)
	 const;


	/*
	 * ====================
	 * Hypothesis Test functions
	 *
	 * This functions performs the hypothesis test function
	 * and drives the verfication process.
	 *
	 * They are internal.
	 */
private:
	/**
	 * \brief	This function performs the hypotesis test on \c *thsi.
	 *
	 * This function will recursive descend and start to split until the tests are accepted.
	 *
	 * \param  splitDeterminer 	This is a pointer to the object that determines the splittiing.
	 */
	void
	priv_hypothesisTesterAndSplitter_serial(
		cSplitDevice_ptr		splitDeterminer);


	/*
	 * ==============================
	 * Model Fitter
	 *
	 * This functions controles the fitting of the model
	 */
private:
	/**
	 * \brief	This function performs the initial fitting on the node.
	 */
	void
	priv_initialFitting_serial();





	/*
	 * ===================
	 * Private static functions
	 */
private:
	/**
	 * \brief	This function performs the recursive testing of the node
	 */
	static
	bool
	priv_performRecursiveChecking(
		const yggdrasil_DETNode_t*			nodeToCheck,
		const yggdrasil_DETNode_t::SplitSequence_t& 	spitSeq,
		Size_t* 					massIn);


	/*
	 * ==================
	 * Friend functions
	 */
private:
	friend
	Node_t*
	ygInternal_createNewSubTree(
		Node_t::IndexArray_t::iterator		indexArray_start,
		Node_t::IndexArray_t::iterator		indexArray_end,
		const Node_t::IndexArray_t::iterator	indexArray_base,
		Node_t::SubDomEnum_t 			splitThusFar,
		Node_t::SubDomRange_t* 			subDomainRanges,
		const Node_t::SplitSequence_t&		splitSeq,
		Node_t::TestResult_ptr			testResult,
		cNode_ptr 				rootNode,
		Node_t::SingleSplit_t*			thisSplit,
		Node_t::cSplitDevice_ptr		splitDeterminer,
		const Node_t::HyperCube_t&		parentDomain);

	//This is only for testing
	//We have to explicitly say that we are in global scope
	//Also needed to fwd the function at the beginning of the file
	friend
	bool
	::ygInternal_testSplitRoutine();

	//The mayload also must be a firiend
	//Not nice.
	friend class yggdrasil_DETNodePayload_t;

	//The tree is a firiend
	friend class yggdrasil_DETree_t;

	/*
	 * =====================
	 * Private Variable
	 */
private:
	Numeric_t 	m_splitPos;		//!< This is the split location.
						//!<  A NAN indicates a leaf, a infinity indicates that *this is not valid
	ChildPtr_t 	m_child;		//!< This is a pointer (array length 2) that stores the two child.
						//!<  The tag it carries is also used as the spliting dimeniosn.
	Payload_ptr 	m_payload;		//!< This is the pointer for the payload
}; //End class: yggdrasil_DETNode_t




YGGDRASIL_NS_END(yggdrasil);












