#pragma once
/**
 * \file	tree/yggdrasil_treeNode_state.hpp
 * \brief	This file defines enum that is used inside the payload to define the state of it.
 */

// Including Core of Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>



YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \brief	This enum encodes the state of a node.
 * \enum 	yggdrasil_treeNode_e
 *
 * This enum is used to name states a node can be.
 * This enum is stored inside the payload.
 *
 * Some of the functionality like split or so,
 * is allready stored in the node (value of split)
 * But this enum allows a more fine grained state.
 */
enum class yggdrasil_treeNode_e : yggdrasil::uInt_t
{
	INVALID, 	//!< The node is invalid
	Leaf,		//!> The node is a leaf
	TrueSplit, 	//!< The node is a true split, this means it
			//!<  was once a leaf but was splited, and is the top split
	ImplicitSplit, 	//!< An implicit split is a split that is not the top level split.
};


YGGDRASIL_NS_END(yggdrasil)
