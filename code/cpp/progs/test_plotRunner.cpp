/**
 * \brief	This is the plotter runner.
 *
 * It runs the experiment with and then dump the results
 * to a file.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_util.hpp>

#include <tree/yggdrasil_treeImpl.hpp>

#include <factory/yggdrasil_treeBuilder.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>
#include <random/yggdrasil_multiGauss.hpp>
#include <random/yggdrasil_multiDirichle.hpp>

#include <verification/yggdrasil_singleExperimentResult.hpp>
#include <verification/yggdrasil_scallingExperimentResult.hpp>
#include <verification/yggdrasil_verificationDriver.hpp>

//INcluide std
#include <iostream>
#include <memory>

#include <getopt.h>

//Include Boost
#include <boost/filesystem.hpp>


#define PRINT_OUTPUT

#ifndef NDEBUG
#define yggdrasil_throw(what) yggdrasil_assert(what)
#else
#define yggdrasil_throw(what) if(!(what)) { throw YGGDRASIL_EXCEPT_RUNTIME(std::string("Condition was violated: ") + #what);}
#endif



/*
 * ========================
 * Helper structes
 *
 */

using Size_t 	        = ::yggdrasil::Size_t;
using Int_t 		= ::yggdrasil::Int_t;
using Real_t 		= ::yggdrasil::Real_t;
using Numeric_t 	= ::yggdrasil::Numeric_t;
using Distribution_t    = ::yggdrasil::yggdrasil_randomDistribution_i;
using Gauss_t 		= ::yggdrasil::yggdrasil_multiDimGauss_t;
using HyperCube_t 	= Gauss_t::HyperCube_t;
using IntervalBound_t 	= HyperCube_t::IntervalBound_t;
using Dirichlet_t 	= ::yggdrasil::yggdrasil_multiDimDirichlet_t;
using Builder_t 	= ::yggdrasil::yggdrasil_treeBuilder_t;
using Path_t 		= ::boost::filesystem::path;

using ScallingResult_t 	= yggdrasil::yggdrasil_scallingExperimentResult_t;
using SingleResult_t 	= ScallingResult_t::SingleExperiment_t;
using CompositionPack_t = SingleResult_t::CompositionPack_t;
using DepthMap_t 	= SingleResult_t::DepthMap_t;
using TimeBase_t 	= SingleResult_t::TimeBase_t;
using Tree_t 		= ::yggdrasil::yggdrasil_DETree_t;
using Sample_t 		= Tree_t::Sample_t;
using SampleArray_t 	= Distribution_t::SampleArray_t;
using SampleList_t 	= Distribution_t::SampleList_t;
using PDFValues_t 	= Distribution_t::ValueArray_t;
using yggdrasil::eDistiType;

namespace fs = ::boost::filesystem;


/**
 * \brief	This fucntion returns the a pointer to
 * 		 the distribution that was requested.
 *
 * \param  distriType	The type of the distribution.
 * \param  dim 		The number of dimensions
 */
extern
yggdrasil::yggdrasil_randomDistribution_i::FactoryRet_t
getDistribution(
	const eDistiType 	dType,
	const Int_t 		dim);


extern
std::string
printDistribution(
	const eDistiType 	dType,
	const Int_t 		dim);


using yggdrasil::isValidFloat;

int
main(
	int  	argc,
	char**	argv)
{
	::yggdrasil::yggdrasil_pRNG_t geni(42);

	/*
	 * ====================
	 * Parameters
	 */

	//Distribution
	eDistiType dType = eDistiType::None;
	Int_t 	   dDim  = 0;
	bool isDirichlet = false;
	bool writeOutPut = true;

	//State; they are optional
	Path_t     generatorPath;
	Path_t     distributionPath;

	//This is the path where we write the result to.
	//Also the internal storage; needed for HDF5 and better organizaion of the dtaa
	//They are requered
	Path_t saveFolder;
	Path_t expLocation;

	//Build object
	Builder_t builder;
	builder.setGOFLevel(0.0001).setIndepLevel(0.0001);

	//Size for fitting
	Size_t fitSize   = 0;

	//This is the point sizes
	Size_t gPoint1 = 0;
	Size_t gPoint2 = 0;

	//Printing the tree
	bool printTheTree = false;
	Path_t fileForPTreePrinting;	//If empty the cout is used.

	//This bool indicates if the grid should be used
	bool useGrid = false;


	/*
	 * Read the arguments
	 */
	//Prepare for get ops
	struct option longopts[] =
	{
		{"save-to",	required_argument,	nullptr,	'f'},	//The folder to save to
		{"save-dir",	required_argument,	nullptr,	'f'},	//The folder to save to (alias)
		{"save",	required_argument,	nullptr,	'f'},	//The folder to save to (alias)
		{"exp-name",	required_argument,	nullptr,	'F'},	//The name of the experiment (Can be empty)
		{"stateFile",	required_argument, 	nullptr, 	's'},	//The general file pattern for the state
		{"fileG",	required_argument,	nullptr,	'g'},	//Explicit generator file
		{"fileD",	required_argument,	nullptr,	'd'},	//Explicit distribution state
		{"fitN",	required_argument, 	nullptr,	'n'},	//Thge size of the sampels to fit
		{"gPoint1",	required_argument,	nullptr, 	'p'},	//Gridpoints for the first dimension
		{"gPoint2",	required_argument,	nullptr,	'P'},	//Gridpoints in the second dimension (may be optional)
		{"distri",	required_argument,	nullptr, 	'T'},	//The type of the distribution
		{"dim",		required_argument, 	nullptr, 	'D'},	//The dimension of the distribution
		{"kind",	required_argument,	nullptr,	'D'},	//An alias for dimension
		{"print",	optional_argument,	nullptr,	'z'},	//This will print the fitted tree
		{"grid",	no_argument,		nullptr,	'G'},	//this will use the grid mode for the eval, if possible
		{"no-write",	no_argument,		nullptr,	'w'},	//Disable the wrtting of the output
		{"Median",	no_argument,		nullptr,	'M'},	//Use the median splitter
		{"Size",	no_argument,		nullptr,	'H'},	//Use the constant splitter
		{"LinMod",	no_argument,		nullptr,	'L'},	//Use the linear model
		{"ConstMod",	no_argument,		nullptr,	'C'},	//use the constant model
		{"gauss",	no_argument,		nullptr,	'a'},	//Selecting gauss
		{"dirichlet",	no_argument,		nullptr,	'A'},	//Selecting dirichlet
		{"outlier",	no_argument,		nullptr,	'o'},	//Selecting the outlier
		{"uniform",	no_argument,		nullptr,	'b'},	//Using bimodal
		{"gamma",	no_argument,		nullptr,	'q'},	//Use the gamma distrinbution.
		{"beta",	no_argument,		nullptr,	'Q'},	//Use the Beta distribution
		{0, 0, 0, 0}
	};

	//See "http://www.informit.com/articles/article.aspx?p=175771&seqNum=3"
	char shortCommands[] = ":f:F:s:g:d:n:p:P:T:D:MHLCbw";

	/*
	 * Actual parsing
	 */
	int c;
	while((c = getopt_long(argc, argv, shortCommands, longopts, nullptr)) != -1)
	{
		switch(c)
		{
			//Save to location
		  case 'f':
		  {
		  	yggdrasil_assert(optarg != nullptr);
		  	saveFolder = std::string(optarg);
		  };
		  break;

		  	//The experiment location
		  case 'F':
		  {
		  	yggdrasil_assert(optarg != nullptr);
		  	expLocation = std::string(optarg);
		  };
		  break;


			//Only one file idenetity is given
		  case 's':
		  {
			yggdrasil_assert(optarg != nullptr);
			std::string pathBone = optarg;
			if(pathBone.empty() == true)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The given file was empty.");
			};
			generatorPath    = pathBone + (pathBone.back() == '.' ? "geni" : ".geni");
			distributionPath = pathBone + (pathBone.back() == '.' ? "dist" : ".dist");
		  };
		  break;

		  	//Only the generator is given
		  case 'g':
		  {
			yggdrasil_assert(optarg != nullptr);
			std::string pathBone = optarg;
			if(pathBone.empty() == true)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The given file was empty.");
			};
			generatorPath    = (pathBone.back() == '.' ? "geni" : ".geni");
		  };
		  break;

		  	//Only the distribution is given
		  case 'd':
		  {
			yggdrasil_assert(optarg != nullptr);
			std::string pathBone = optarg;
			if(pathBone.empty() == true)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The given file was empty.");
			};
			distributionPath = (pathBone.back() == '.' ? "dist" : ".dist");
		  };
		  break;

		  	//Fit size is given
		  case 'n':
		  {
			yggdrasil_assert(optarg != nullptr);
			fitSize = ::std::stoull(optarg);
			if(fitSize <= 0)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The fit size is too small.");
			};
		  };
		  break;

		  	//The numbers of grid points in one direction
		  case 'p':
		  {
		  	yggdrasil_assert(optarg != nullptr);
		  	gPoint1 = std::stoull(optarg);
		  	if(gPoint1 <= 0)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The number of gird points in the first direction is to small.");
			};
		  };
		  break;

		  	//The numbers of grid points in the second direction
		  case 'P':
		  {
		  	yggdrasil_assert(optarg != nullptr);
		  	gPoint2 = std::stoull(optarg);
		  	if(gPoint2 <= 0)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The number of grid points in the second dimension is too small.");
			};
		  };
		  break;

		  case 'w':
		  {
		  	  writeOutPut = false;
		  };
		  break;

		  	//Prionting to a file
		  case 'z':
		  {
		  	printTheTree = true;
		  	if(optarg != nullptr)
			{
				fileForPTreePrinting = optarg;
			};
		  };
		  break;

		  	//Use the grid for the sample generation if possible
		  case 'G':
		  {
		  	useGrid = true;
		  };
		  break;


		  	//Distribution type
		  case 'T':
		  {
			yggdrasil_assert(optarg != nullptr);
			std::string tString = optarg;

			if(tString == "gauss" || tString == "normal")
			{
				dType = eDistiType::Gauss;
				break;
			};
			if(tString == "dirichlet")
			{
				dType = eDistiType::Dirichlet;
				break;
			};

			throw YGGDRASIL_EXCEPT_InvArg("The distributiuon \"" + tString + "\" is unkonw.");
		  };
		  break;

		  	//Direct seletcing Gauss
		  case 'a':
		  {
		  	dType = eDistiType::Gauss;
		  };
		  break;

		  	//Use gamma
		  case 'q':
		  {
		  	dType = eDistiType::Gamma;
		  };
		  break;

		  	//Direct selecting Dirichlet
		  case 'A':
		  {
		  	dType = eDistiType::Dirichlet;
		  };
		  break;

		  	//Use beta direct
		  case 'Q':
		  {
		  	dType = eDistiType::Beta;
		  };
		  break;

		  case 'o':
		  {
		  	  dType = eDistiType::Outlier;
		  };
		  break;

		  case 'b':
		  {
		  	  dType = eDistiType::Uniform;
		  };
		  break;

			//Dimension of distribution
		  case 'D':
		  {
			yggdrasil_assert(optarg != nullptr);
			dDim = ::std::stoi(optarg);
		  };
		  break;

		  case 'M':
		  {
		  	  builder.useMedianSplitter();
		  };
		  break;

		  case 'H':
		  {
		  	  builder.useSizeSplitter();
		  };
		  break;

		  case 'L':
		  {
		  	  builder.useLinearModel();
		  };
		  break;

		  case 'C':
		  {
		  	  builder.useConstantModel();
		  };
		  break;



		  /*
		   * Error
		   */

		  //Missing option
		  case ':':
		  {
			  std::cerr << "Error:\n"
			  	  << "The option \"" << ((char)optopt) << "\" needs an argument.\n"
			  	  << "Abord"
			  	  << std::endl;
			  std::abort();
			  break;
		  };

		  //Invalid commend
		  case '?':
		  default:
		  {
			  std::cerr << "Error:\n"
			  	  << "The command \"" << argv[optind] << "\" is unknown.\n"
			  	  << "Abord"
			  	  << std::endl;
			  std::abort();
			  break;
		  };
		}; //End switch(c)
	}; //End parsing

	/*
	 * Restoring some default values
	 */
	isDirichlet = (dType == eDistiType::Dirichlet) ? true : false;


	/*
	 * ===============
	 * Checking of the arguments
	 * some are used automatically
	 */
	if(generatorPath.empty() == false)
	{
		if(fs::is_regular_file(generatorPath) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The filepath \"" + generatorPath.string() + "\" for the generator state is invalid.");
		};
	};
	if(distributionPath.empty() == false)
	{
		if(fs::is_regular_file(distributionPath) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The filepath \"" + distributionPath.string() + "\" for the distribution state is invalid.");
		};
	};

	if(writeOutPut == true)
	{
		if(saveFolder.empty() == true)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Nop path for the storage of the data is given.");
		};
	};

	//Create a distribution
	auto DistributionPoint = getDistribution(dType, dDim);

	if(expLocation.empty() == true)
	{
		expLocation = DistributionPoint.second;
	};

	//In the case of a one D distribution the second number of grid punt is set to 1
	if(DistributionPoint.first->nDims() == 1)
	{
		gPoint2 = 1;
	}
	else
	{
		//In the case of a non 1d distribution
		//and if the second number of grid point is not
		//given copy the one of the first.
		if(gPoint2 == 0)
		{
			gPoint2 = gPoint1;
		};
	};

	/*
	 * ==================
	 * Give some output
	 */
	std::cout
		<< "Restored Informations:\n"
		<< "Generator State:     " << generatorPath 			<< "\n"
		<< "Distroibution State: " << distributionPath 			<< "\n"
		<< "Distribution:        " << DistributionPoint.second 		<< "\n"
		<< "Fitting Size:        " << fitSize 				<< "\n"
		<< "Grid Enabled:        " << (useGrid ? "Yes" : "No") 		<< "\n"
		<< "Grid point dim 1:    " << gPoint1 				<< "\n"
		<< "Grid point dim 2:    " << gPoint2 				<< "\n";

	//Only write the output paths if a writewas requested
	if(writeOutPut == true)
	{
		std::cout
			<< "Savefolder:          " << saveFolder 			<< "\n"
			<< "exp name:            " << expLocation 			<< "\n";
	};

	std::cout
		<< "\n"
		<< "The bilder is:\n"
		<< builder.print()
		<< "\n" << std::endl;

	/*
	 * ==================
	 * State reloading
	 */


	if(distributionPath.empty() == false)
	{
		//Load the distribution
		DistributionPoint.first->loadFromFile(distributionPath.string());
	};
	if(generatorPath.empty() == false)
	{
		//Load the generator
		yggdrasil::yggdrasil_pRNG_load(generatorPath.string(), &geni);
	};


	/*
	 * Perform the running
	 */
	{
		//Get the destribution as reference
		Distribution_t& distribution = *DistributionPoint.first;

		//Get the domain from the distribution
		const HyperCube_t domain = distribution.getSampleDomain();

		if(domain.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The domain is not valid.");
		};

		//Get the dimension
		const Size_t nDimensions = domain.nDims();

		/*
		 * Create the samples for the fitting
		 */
		SampleArray_t sampleArray(distribution.generateSamplesArray(geni, fitSize));


		/*
		 * Fitting the tree
		 */
		Tree_t currentTree(sampleArray, domain, builder);


		/*
		 * Make some tests
		 */
		if(ygInternal_isTreeFullyValid(currentTree) == false)
		{
			std::cerr << " --  " << "Tree did not pass the tests." << std::endl;
			std::abort();
		};


		DepthMap_t treeDepthMap    = currentTree.getDepthMap();
		std::cout
			<< "Statistic:\n"
			<< "  Mean Depth: " << treeDepthMap.getDepthMean() << "\n"
			<< "  Std Deth:   " << treeDepthMap.getDepthStd()  << "\n"
			<< "  Max Depth:  " << treeDepthMap.getMaxDepth()  << "\n"
			<< "  Leafs:      " << treeDepthMap.getLeafCount() << "\n"
			<< std::endl;

		//Write the tree
		if(printTheTree == true)
		{
			if(fileForPTreePrinting.empty() == true)
			{
				//Directly to the cout
				currentTree.printToStream(std::cout, true);
			}
			else
			{
				//Writting to a file
				currentTree.printToFile(fileForPTreePrinting.string(), true);
			};
		}; //End if: writting the tree


		/*
		 * ============
		 * 	MISE
		 */

		/*
		 * Generating the samples for plotting
		 */

		//Variable for holding them
		SampleArray_t querySamples(nDimensions);
#ifdef PRINT_OUTPUT
		std::cout << " >>  Starting the generation of the samples." << std::endl;
#endif

		//The dimension of the samples
		const Size_t sampDim = distribution.nDims();

		//This bool indicates which method was used in the end
		bool griddingWasUsed;

		if(useGrid && (sampDim <= 2))
		{
			/*
			 * Use the grid for generation
			 */
			querySamples = ::yggdrasil::yggdrasil_generateGrid(
					distribution.getSampleDomain(),
					gPoint1, gPoint2,
					isDirichlet);

			griddingWasUsed = true;
		}
		else
		{
			/*
			 * Use the sampler for the generation
			 */

			if(useGrid == true)
			{
				std::cout
					<< " >>  The grid generator was requested, but was not possible using.\n"
					<< "        Using the distribution to generate samples."
					<< std::endl;
			}; //ENd if: grid was requested

			//No gridding was used
			griddingWasUsed = false;

			//It must be that way to accomodate all possibilities
			//Also Dirichlet may produce smaller grids.
			const Size_t querySize = gPoint1 * (gPoint2 == 0 ? 1 : gPoint2);
			querySamples = distribution.generateSamplesArray(geni, querySize);
		}; //ENd else: using the sampler

		//THe final size of the quering
		const Size_t querySize = querySamples.size();

#ifdef PRINT_OUTPUT
		std::cout << " >>  Finished the generation of the samples." << std::endl;
#endif

		/*
		 * Evaluate the samples for the true distribution
		 */
		const PDFValues_t pdf_ex = distribution.pdf(querySamples);
#ifdef PRINT_OUTPUT
		std::cout << " >>  Finished the computations of the PDFs." << std::endl;
		if(writeOutPut == false)
		{
			std::cout << "\n";
		};
#endif

		/*
		 * Evaluate the same samples on the tree
		 */
#ifdef USE_REGULAR_GRID
		const PDFValues_t pdf_est = currentTree.evaluateSamplesAt_noErr(querySamples);
#else
		const PDFValues_t pdf_est = currentTree.evaluateSamplesAt(querySamples);
#endif

		/*
		 * Dumping it to a file
		 */
		if(writeOutPut == true)
		{
			yggdrasil::yggdrasil_dumpDistribution(saveFolder, expLocation,
				querySamples, pdf_est, &pdf_ex);
#ifdef PRINT_OUTPUT
			std::cout << " >>  Finished writing the samples." << std::endl;
			std::cout << "\n";
#endif
		};


		/*
		 * Evaluating the MISE
	 	 */

		//This is the variable for summing up the Values
		::std::vector<Real_t> pMISE(16, 0.0);

		//This is for integrating the two probabilities to check if they are one
		::std::vector<Real_t> pTree(16, 0.0);
		::std::vector<Real_t> pEx(16, 0.0);

		Numeric_t maxAbsDiff = -1;

		//Terate over the two samples and iterate over the
		//the two value ranges.
		//using equation (9) of the paper
		for(Size_t q = 0; q != querySize; ++q)
		{
			//Load the two values
			const auto pEst  = pdf_est[q];	//This is the estimated value
			const auto pTrue = pdf_ex[q];	//This is the true value

			const auto absDiff = std::abs(pEst - pTrue);
			maxAbsDiff = std::max(absDiff, maxAbsDiff);

			//These are the variables
			Real_t term1, term2, term3;

			if(griddingWasUsed == true)
			{
				term1 = (pEst - pTrue) * (pEst - pTrue);
				term2 = 0;
				term3 = 0;
			}
			else
			{
				if(isValidFloat(pEst) == false)
				{
					throw YGGDRASIL_EXCEPT_RUNTIME("pEst was not valid, it was: " + std::to_string(pEst));
				};
				yggdrasil_throw(isValidFloat(pEst) == true);
				yggdrasil_throw(pEst >= 0.0);
				yggdrasil_throw(isValidFloat(pTrue) == true);
				yggdrasil_throw(pTrue >= 0.0);	//Must be strigtly greater than zero


				//Compute the local MISE, We will never have a division by zero

				//Calculating the three individual terms

				//First term \frac{\hat{p}(x_q)^2}{p(x_q)}
				term1 = pEst * pEst / pTrue;
				yggdrasil_throw(isValidFloat(term1));

				// Second term -2.0 \cdot p(x_q)
				term2 = -2.0 * pEst;

				//Third term
				term3 = pTrue;
			}; //End else: no griding was used

			//Sum up the value and store it
			pMISE.at(q % 16) += (term1 + term2 + term3);
			pTree.at(q % 16) += pEst;
			pEx.at  (q % 16) += pTrue;
		}; //End for(q): evaluating the tree

		//Makeing the reduction
		for(Size_t k = 1; k != pMISE.size(); ++k)
		{
			pMISE[0] += pMISE[k];
			pTree[0] += pTree[k];
			pEx[0]   += pEx[k];
		}; //ENd for(k)

		//It is extremly unlikely that we have 0, but it can happen
		yggdrasil_assert(pMISE[0] >= 0.0);


		//Now make the true reduction
		if(griddingWasUsed == true)
		{
			Real_t miseVol;
			miseVol = distribution.getSampleDomain().getVol();
			const Real_t trueMISE = pMISE[0] * (miseVol / querySize);
			const Real_t intPDF_true = pEx[0] * (miseVol / querySize);
			const Real_t intPDT_tree = pTree[0] * (miseVol / querySize);
			std::cout
				<< "Regular Grid is activated.\n"
				<< "The MISE was estimated as:  " << trueMISE << "\n"
				<< "The Itegrated exact PDF is: " << intPDF_true << "\n"
				<< "The integrated tree PDF is: " << intPDT_tree << "\n"
				<< "MaxAbsDiff:                 " << maxAbsDiff << "\n"
				<< "Mass of Tree:               " << currentTree.getMass() << "\n"
				<< std::endl;
		}
		else
		{
			const Real_t trueMISE = pMISE[0] / querySize;
			const Real_t intPDF_true = pEx[0] / querySize;
			const Real_t intPDT_tree = pTree[0] / querySize;

			/*
			* In this mode we can not calculat ethe intgram, because
			* actually we must sum up ones and then divide by theier
			* number, thius is not so usefull, so we define a new quntitiey.
			* We define that the mass of the exat pdf is one
			*/
			const Real_t fracMass = intPDT_tree / intPDF_true;
			std::cout
				<< "The MISE was estimated as:  " << trueMISE << "\n"
				<< "MaxAbsDiff:                 " << maxAbsDiff << "\n"
				<< "Tree mass was:              " << intPDT_tree << "\n"
				<< "Exact mass was:             " << intPDF_true << "\n"
				<< "Quotient is:                " << fracMass	<< "\n"
				<< "Mass of Tree:               " << currentTree.getMass() << "\n"
				<< std::endl;
		}; //End: gridding was not used for output
	}; //ENd running the experiment


	std::cout
		<< "\n"
		<< "Your wish has surpassed entropy." << "\n"
		<< "    -- Kyubey" << "\n"
		<< std::endl;


	/*
	 * ===============
	 * Returning
	 */
	return 0;
}; //End main


/*
 * ====================
 * Impel Helperfunction
 */

yggdrasil::yggdrasil_randomDistribution_i::FactoryRet_t
getDistribution(
	const eDistiType 	dType,
	const Int_t 		dim)
{
	return yggdrasil::yggdrasil_randomDistribution_i::FACTORY(dType, dim);
}; //End get Distri

std::string
printDistribution(
	const eDistiType 	dType,
	const Int_t 		dim)
{
	std::string what;
	if(dType == eDistiType::Gauss)
	{
		what = "Gauss ";
	}
	else if(dType == eDistiType::Dirichlet)
	{
		what = "Dirichlet ";
	}
	else if(dType == eDistiType::Outlier)
	{
		what = "Outlier";
	}
	else if(dType == eDistiType::Uniform)
	{
		what = "Uniform";
	}
	else
	{
		what = "Unknown ";
	};


	what += std::to_string(dim) + "D";

	return what;
}; //End print

