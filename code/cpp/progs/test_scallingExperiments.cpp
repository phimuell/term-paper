/**
 * \brief	This si the main file for performing the
 * 		 reproduction of the experiments.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <tree/yggdrasil_treeImpl.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>
#include <random/yggdrasil_multiGauss.hpp>
#include <random/yggdrasil_multiDirichle.hpp>

#include <verification/yggdrasil_singleExperimentResult.hpp>
#include <verification/yggdrasil_verificationDriver.hpp>

//INcluide std
#include <iostream>

#include <getopt.h>

//Include Boost
#include <boost/filesystem.hpp>

using Path_t = ::yggdrasil::Path_t;
using FactoryRet_t = ::yggdrasil::yggdrasil_randomDistribution_i::FactoryRet_t;
using ::yggdrasil::eDistiType;
using Int_t = ::yggdrasil::Int_t;

static
FactoryRet_t
distFactory(
	const eDistiType 	dType,
	const int 		dim)
{
	return yggdrasil::yggdrasil_randomDistribution_i::FACTORY(dType, dim);
};



int
main(
	int  	argc,
	char**	argv)
{
	/*
	 * Controll variables
	 */
	yggdrasil::yggdrasil_pRNG_t geni(42);

	//These variable controlles if a test is done or not
	bool do_gauss     = true;
	bool do_dirichlet = true;
	bool do_uniform   = true;
	bool do_outlier   = true;
	bool do_gamma     = true;
	bool do_beta      = true;

	//This is the variable that controls where to save the result
	Path_t saveFolder = "./scallingExp";


	/*
	 * ====================
	 * Read the parameter
	 */
	struct option longopts[] =
	{
		{"dir",			required_argument, 	nullptr,	'F'},	//Where to save the data
		{"save-dir",		required_argument, 	nullptr,	'F'},	//Where to save the data (alias)
		{"none",		no_argument,		nullptr,	'a'},	//Disable all tests
		{"all",			no_argument, 		nullptr,	'A'},	//Enable all tests
		{"no-gauss",		no_argument,		nullptr,	'g'},	//Deactivate all gauss distributions
		{"gauss",		no_argument,		nullptr,	'G'},	//Activate all gauss distributions
		{"no-dirichlet",	no_argument, 		nullptr,	'd'},	//Deactivate all dirichlet
		{"dirichlet",		no_argument,		nullptr,	'D'},	//Activate all dirichlet
		{"no-outlier",		no_argument,		nullptr,	'o'},	//No 1D outlier
		{"outlier",		no_argument,		nullptr,	'O'},	//Outlier one D
		{"no-uniform",		no_argument,		nullptr,	'u'},	//No bimod
		{"uniform",		no_argument,		nullptr,	'U'},	//Do bimod
		{"no-gamma",		no_argument,		nullptr,	'c'},	//no gamma function
		{"gamma",		no_argument, 		nullptr,	'C'},	//Endable gamma
		{"no-beta",		no_argument,		nullptr,	'b'},	//Disable all beta
		{"beta",		no_argument,		nullptr,	'B'},	//Enalble Beta
		{0, 0, 0, 0}
	};


	char shortCommands[] = ":F:aAgGdDoOuUcCbB";

	/*
	 * Actual parsing
	 */
	int c;
	while((c = getopt_long(argc, argv, shortCommands, longopts, nullptr)) != -1)
	{
		switch(c)
		{
		  case 'F':
		  {
		  	yggdrasil_assert(optarg != nullptr);
		  	saveFolder = optarg;
		  };
		  break;

		  	//None
		  case 'a':
		  {
		  	  do_outlier 	= false;
		  	  do_gauss 	= false;
		  	  do_uniform 	= false;
		  	  do_dirichlet 	= false;
		  	  do_gamma 	= false;
		  	  do_beta 	= false;
		  };
		  break;

		  	//All
		  case 'A':
		  {
		  	  do_outlier 	= true;
		  	  do_gauss 	= true;
		  	  do_uniform 	= true;
		  	  do_dirichlet 	= true;
		  	  do_gamma 	= true;
		  	  do_beta 	= true;
		  };
		  break;


#define CASE(ON, OFF, VAR) case ON : {VAR = true;}; break; case OFF : {VAR = false;}; break
		  CASE('U', 'u', do_uniform);
		  CASE('G', 'g', do_gauss);
		  CASE('C', 'c', do_gamma);
		  CASE('D', 'd', do_dirichlet);
		  CASE('O', 'o', do_uniform);
		  CASE('B', 'b', do_beta);
#undef CASE

		  /*
		   * Error
		   */

		  //Missing option
		  case ':':
		  {
			  std::cerr << "Error:\n"
			  	  << "The option \"" << ((char)optopt) << "\" needs an argument.\n"
			  	  << "Abord"
			  	  << std::endl;
			  std::abort();
			  break;
		  };

		  //Invalid commend
		  case '?':
		  default:
		  {
			  std::cerr << "Error:\n"
			  	  << "The command \"" << argv[optind] << "\" is unknown.\n"
			  	  << "Abord"
			  	  << std::endl;
			  std::abort();
			  break;
		  };
		}; //End switch(c):
	}; //End: parser while



	/*
	 * =========================
	 * Perform the tests
	 */

	/*
	 * Perform Gauss
	 */
	if(do_gauss == true)
	{
		using Gauss_t 		= ::yggdrasil::yggdrasil_multiDimGauss_t;
		using HyperCube_t 	= Gauss_t::HyperCube_t;
		using IntervalBound_t 	= HyperCube_t::IntervalBound_t;

		const Int_t knownTests[] = {-2, 2, 4, 7};

		for(const Int_t t : knownTests)
		{
			//get the distribution
			FactoryRet_t distPack = distFactory(eDistiType::Gauss, t);

			//Where to same
			Path_t sFolder = saveFolder / "Gauss";

			//Perform the experiment
			yggdrasil_performStandardExperiment(
				*distPack.first,
				geni,
				distPack.first->getSampleDomain(),
				distPack.second,
				distPack.second,
				sFolder
				);
		}; //End for(t)
	}; //End: 2D Simple Gauss test


	/*
	 * Dirichlet
	 *
	 * Notice:
	 * The paper (page 20) talks about a 4D distribution.
	 * But gives 5 parameters, these 5 Parameters are now taken.
	 *
	 */
	if(do_dirichlet == true)
	{
		using Dirichlet_t 	= ::yggdrasil::yggdrasil_multiDimDirichlet_t;
		using HyperCube_t 	= Dirichlet_t::HyperCube_t;
		using IntervalBound_t 	= HyperCube_t::IntervalBound_t;

		const Int_t knownTests[] = {2, 4, 7};

		for(const Int_t t : knownTests)
		{

			//Generate the distribution
			FactoryRet_t distPack = distFactory(eDistiType::Dirichlet, t);

			//Where to same
			Path_t sFolder = saveFolder / "Dirichlet";

			//Perform the experiment
			yggdrasil_performStandardExperiment(
				*distPack.first,
				geni,
				distPack.first->getSampleDomain(),
				distPack.second,
				distPack.second,
				sFolder
				);
		}; //End for(t)
	}; //End: 3D Dirichlet


	/*
	 * Outlier distribution
	 */
	if(do_outlier == true)
	{
		const Int_t knownTests[] = {1};

		for(const Int_t t : knownTests)
		{
			FactoryRet_t distPack = distFactory(eDistiType::Outlier, t);

			//WHere to same
			Path_t sFolder = saveFolder / "Outlier";

			//Perform the experiment
			yggdrasil_performStandardExperiment(
				*distPack.first,
				geni,
				distPack.first->getSampleDomain(),
				distPack.second,
				distPack.second,
				sFolder
				);
		}; //End for(t)
	}; //End: 1D outlier


	/*
	 * BiMod / uniform
	 *
	 * Nagative values are bimodal
	 */
	if(do_uniform == true)
	{
		Int_t knownTests[] = {-1, -2, 1, 2, 4, 7};

		for(const Int_t t : knownTests)
		{
			FactoryRet_t distPack = distFactory(eDistiType::Uniform, t);

			//WHere to same
			Path_t sFolder = saveFolder / "Uniform";

			//Perform the experiment
			yggdrasil_performStandardExperiment(
				*distPack.first,
				geni,
				distPack.first->getSampleDomain(),
				distPack.second,
				distPack.second,
				sFolder
				);
		}; //End for(t):
	}; //End bimodal


	/*
	 * Gamma Distribution
	 */
	if(do_gamma == true)
	{
		Int_t knownTests[] = {-1, -3, -4};

		for(const Int_t t : knownTests)
		{
			FactoryRet_t distPack = distFactory(eDistiType::Gamma, t);

			//WHere to same
			Path_t sFolder = saveFolder / "Gamma";

			//Perform the experiment
			yggdrasil_performStandardExperiment(
				*distPack.first,
				geni,
				distPack.first->getSampleDomain(),
				distPack.second,
				distPack.second,
				sFolder
				);
		}; //End for(t):
	}; //ENd gamma


	/*
	 * Beta distribution
	 */
	if(do_beta == true)
	{
		Int_t knownTests[] = {1, -1, -2, -3};

		for(const Int_t t : knownTests)
		{
			FactoryRet_t distPack = distFactory(eDistiType::Beta, t);

			//WHere to same
			Path_t sFolder = saveFolder / "Beta";

			//Perform the experiment
			yggdrasil_performStandardExperiment(
				*distPack.first,
				geni,
				distPack.first->getSampleDomain(),
				distPack.second,
				distPack.second,
				sFolder
				);
		}; //End for(t):


	}; //End: Beta


	return 0;
}; //End main




