/**
 * \brief	This is the dump loader.
 *
 * It is possoble to lad a dumped state of the scalling experiment
 * and then run it.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <tree/yggdrasil_treeImpl.hpp>

#include <factory/yggdrasil_treeBuilder.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>
#include <random/yggdrasil_multiGauss.hpp>
#include <random/yggdrasil_multiDirichle.hpp>

#include <verification/yggdrasil_singleExperimentResult.hpp>
#include <verification/yggdrasil_scallingExperimentResult.hpp>
#include <verification/yggdrasil_verificationDriver.hpp>

//INcluide std
#include <iostream>
#include <memory>

#include <getopt.h>

//Include Boost
#include <boost/filesystem.hpp>


/*
 * ========================
 * Helper structes
 *
 */

using Size_t 	        = ::yggdrasil::Size_t;
using Int_t 		= ::yggdrasil::Int_t;
using Real_t 		= ::yggdrasil::Real_t;
using Numeric_t 	= ::yggdrasil::Numeric_t;
using Distribution_t    = ::yggdrasil::yggdrasil_randomDistribution_i;
using Gauss_t 		= ::yggdrasil::yggdrasil_multiDimGauss_t;
using HyperCube_t 	= Gauss_t::HyperCube_t;
using IntervalBound_t 	= HyperCube_t::IntervalBound_t;
using Dirichlet_t 	= ::yggdrasil::yggdrasil_multiDimDirichlet_t;
using Builder_t 	= ::yggdrasil::yggdrasil_treeBuilder_t;
using Path_t 		= ::boost::filesystem::path;

using ScallingResult_t 	= yggdrasil::yggdrasil_scallingExperimentResult_t;
using SingleResult_t 	= ScallingResult_t::SingleExperiment_t;
using CompositionPack_t = SingleResult_t::CompositionPack_t;
using DepthMap_t 	= SingleResult_t::DepthMap_t;
using TimeBase_t 	= SingleResult_t::TimeBase_t;
using Tree_t 		= ::yggdrasil::yggdrasil_DETree_t;
using Sample_t 		= Tree_t::Sample_t;
using SampleArray_t 	= Distribution_t::SampleArray_t;
using SampleList_t 	= Distribution_t::SampleList_t;
using PDFValues_t 	= Distribution_t::ValueArray_t;
using yggdrasil::eDistiType;

namespace fs = ::boost::filesystem;


/**
 * \brief	This fucntion returns the a pointer to
 * 		 the distribution that was requested.
 *
 * \param  distriType	The type of the distribution.
 * \param  dim 		The number of dimensions
 */
extern
std::unique_ptr<Distribution_t>
getDistribution(
	const eDistiType 	dType,
	const Int_t 		dim);


extern
std::string
printDistribution(
	const eDistiType 	dType,
	const Int_t 		dim);


using yggdrasil::isValidFloat;

int
main(
	int  	argc,
	char**	argv)
{
	::yggdrasil::yggdrasil_pRNG_t geni(42);

	/*
	 * ====================
	 * Parameters
	 */

	//Distribution
	Path_t     generatorPath;
	Path_t     distributionPath;
	eDistiType dType = eDistiType::None;
	Int_t 	   dDim  = 0;

	//Build object
	Builder_t builder;

	//Size parameters
	Size_t fitSize   = 0;
	Size_t querySize = 0;


	/*
	 * Read the arguments
	 */
	//Prepare for get ops
	struct option longopts[] =
	{
		{"file",	required_argument, 	nullptr, 	'f'},	//The general file pattern for the state
		{"fileG",	required_argument,	nullptr,	'g'},	//Explicit generator file
		{"fileD",	required_argument,	nullptr,	'd'},	//Explicit distribution state
		{"fitN",	required_argument, 	nullptr,	'n'},	//Thge size of the sampels to fit
		{"queryN",	required_argument,	nullptr, 	'q'},	//The size of the query
		{"distri",	required_argument,	nullptr, 	'T'},	//The type of the distribution
		{"dim",		required_argument, 	nullptr, 	'D'},	//The dimension of the distribution
		{"Median",	no_argument,		nullptr,	'M'},	//Use the median splitter
		{"Size",	no_argument,		nullptr,	'H'},	//Use the constant splitter
		{"LinMod",	no_argument,		nullptr,	'L'},	//Use the linear model
		{"ConstMod",	no_argument,		nullptr,	'C'},	//use the constant model
		{"gauss",	no_argument,		nullptr,	'a'},	//Selecting gauss
		{"dirichlet",	no_argument,		nullptr,	'A'},	//Selecting dirichlet
		{"outlier",	no_argument,		nullptr,	'b'},	//Selecting the outlier
		{"uniform",	no_argument,		nullptr,	'B'},	//Using bimodal
		{"gamma",	no_argument,		nullptr,	'c'},	//Use the gamma distrinbution.
		{"beta",	no_argument,		nullptr,	'Z'},	//Use the beta distribution
		{0, 0, 0, 0}
	};

	//See "http://www.informit.com/articles/article.aspx?p=175771&seqNum=3"
	char shortCommands[] = ":f:g:d:n:q:T:D:MHLC";

	/*
	 * Actual parsing
	 */
	int c;
	while((c = getopt_long(argc, argv, shortCommands, longopts, nullptr)) != -1)
	{
		switch(c)
		{
			//Only one file idenetity is given
		  case 'f':
		  {
			yggdrasil_assert(optarg != nullptr);
			std::string pathBone = optarg;
			if(pathBone.empty() == true)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The given file was empty.");
			};
			generatorPath    = pathBone + (pathBone.back() == '.' ? "geni" : ".geni");
			distributionPath = pathBone + (pathBone.back() == '.' ? "dist" : ".dist");
		  };
		  break;

		  	//Only the generator is given
		  case 'g':
		  {
			yggdrasil_assert(optarg != nullptr);
			std::string pathBone = optarg;
			if(pathBone.empty() == true)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The given file was empty.");
			};
			generatorPath    = (pathBone.back() == '.' ? "geni" : ".geni");
		  };
		  break;

		  	//Only the distribution is given
		  case 'd':
		  {
			yggdrasil_assert(optarg != nullptr);
			std::string pathBone = optarg;
			if(pathBone.empty() == true)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The given file was empty.");
			};
			distributionPath = (pathBone.back() == '.' ? "dist" : ".dist");
		  };
		  break;

		  	//Fit size is given
		  case 'n':
		  {
			yggdrasil_assert(optarg != nullptr);
			fitSize = ::std::stoull(optarg);
			if(fitSize <= 0)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The fit size is too small.");
			};
		  };
		  break;

		  	//Query size is given
		  case 'q':
		  {
			yggdrasil_assert(optarg != nullptr);
			querySize = ::std::stoull(optarg);
			if(querySize <= 0)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The fit size is too small.");
			};
		  };
		  break;

		  	//Distribution type
		  case 'T':
		  {
			yggdrasil_assert(optarg != nullptr);
			std::string tString = optarg;

			if(tString == "gauss" || tString == "normal")
			{
				dType = eDistiType::Gauss;
				break;
			};
			if(tString == "dirichlet")
			{
				dType = eDistiType::Dirichlet;
				break;
			};
			if(tString == "outlier" || tString == "out-lier")
			{
				dType = eDistiType::Outlier;
				break;
			}
			if(tString == "uni" || tString == "uniform")
			{
				dType = eDistiType::Uniform;
				break;
			};
			if(tString == "gamma")
			{
				dType = eDistiType::Gamma;
			};


			throw YGGDRASIL_EXCEPT_InvArg("The distributiuon \"" + tString + "\" is unkonw.");
		  };
		  break;

		  	//Direct seletcing Gauss
		  case 'a':
		  {
		  	dType = eDistiType::Gauss;
		  };
		  break;

		  	//Direct selecting Dirichlet
		  case 'A':
		  {
		  	dType = eDistiType::Dirichlet;
		  };
		  break;

		  	//Direct select outlier
		  case 'b':
		  {
		  	dType = eDistiType::Outlier;
		  };
		  break;

		  	//Direct select uniform
		  case 'B':
		  {
		  	dType = eDistiType::Uniform;
		  };
		  break;

		  	//Direct selecting Gamma
		  case 'c':
		  {
		  	dType = eDistiType::Gamma;
		  };
		  break;

		  	//Use the beta distribution
		  case 'Z':
		  {
		  	dType = eDistiType::Beta;
		  };
		  break;

			//Dimension of distribution
		  case 'D':
		  {
			yggdrasil_assert(optarg != nullptr);
			dDim = ::std::stoi(optarg);	//The dimension can also be negative
							//This indicates a special kind
		  };
		  break;

		  case 'M':
		  {
		  	  builder.useMedianSplitter();
		  };
		  break;

		  case 'H':
		  {
		  	  builder.useSizeSplitter();
		  };
		  break;

		  case 'L':
		  {
		  	  builder.useLinearModel();
		  };
		  break;

		  case 'C':
		  {
		  	  builder.useConstantModel();
		  };
		  break;

		  /*
		   * Error
		   */

		  //Missing option
		  case ':':
		  {
			  std::cerr << "Error:\n"
			  	  << "The option \"" << ((char)optopt) << "\" needs an argument.\n"
			  	  << "Abord"
			  	  << std::endl;
			  std::abort();
			  break;
		  };

		  //Invalid commend
		  case '?':
		  default:
		  {
			  std::cerr << "Error:\n"
			  	  << "The command \"" << argv[optind] << "\" is unknown.\n"
			  	  << "Abord"
			  	  << std::endl;
			  std::abort();
			  break;
		  };
		}; //End switch(c)
	}; //End parsing


	/*
	 * ===============
	 * Checking of the arguments
	 * some are used automatically
	 */
	if(fs::is_regular_file(generatorPath) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The filepath \"" + generatorPath.string() + "\" for the generator state is invalid.");
	};
	if(fs::is_regular_file(distributionPath) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The filepath \"" + distributionPath.string() + "\" for the distribution state is invalid.");
	};

	//Create a distribution
	auto DistributionPoint = getDistribution(dType, dDim);


	/*
	 * ==================
	 * Give some output
	 */
	std::cout
		<< "Restored Informations:\n"
		<< "Generator State:     " << generatorPath 			<< "\n"
		<< "Distroibution State: " << distributionPath 			<< "\n"
		<< "Distribution:        " << printDistribution(dType, dDim) 	<< "\n"
		<< "Fitting Size:        " << fitSize 				<< "\n"
		<< "Query Size:          " << querySize 			<< "\n";
	std::cout
		<< "\n"
		<< "The bilder is:\n"
		<< builder.print()
		<< "\n" << std::endl;

	/*
	 * ==================
	 * State reloading
	 */

	//Load the distribution
	DistributionPoint->loadFromFile(distributionPath.string());

	//Load the generator
	yggdrasil::yggdrasil_pRNG_load(generatorPath.string(), &geni);


	/*
	 * Perform the running
	 */
	{
		//Get the destribution as reference
		Distribution_t& distribution = *DistributionPoint;

		//Get the domain from the distribution
		const HyperCube_t domain = distribution.getSampleDomain();

		if(domain.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The domain is not valid.");
		};

		//Get the dimension
		const Size_t nDimensions = domain.nDims();

		/*
		 * Create the samples for the fitting
		 */
		SampleArray_t sampleArray(distribution.generateSamplesArray(geni, fitSize));


		/*
		 * Fitting the tree
		 */
		Tree_t currentTree(sampleArray, domain, builder);


		/*
		 * Make some tests
		 */
		if(ygInternal_isTreeFullyValid(currentTree) == false)
		{
			std::cerr << " --  " << "Tree did not pass the tests." << std::endl;
			std::abort();
		};

		/*
			* Gather the statistics
			*/
		CompositionPack_t treeComposition;
		DepthMap_t        treeDepthMap;

		treeComposition = currentTree.getTreeComposition();
		treeDepthMap    = currentTree.getDepthMap();
		yggdrasil_assert(treeDepthMap.nDiffDepth() > 0);

		/*
			* ============
			* 	MISE
			*/

		/*
			* Generating the samples for the MISE
			*/

		//Variable for holding them
		SampleList_t querySamples(nDimensions);
		PDFValues_t   trueQueryPDFs;

		//Actuall generation
		::std::tie(querySamples, trueQueryPDFs) = distribution.generateSamplesListPDF(geni, querySize);

		yggdrasil_assert(querySamples.size() == querySize);
		yggdrasil_assert(querySamples.size() == trueQueryPDFs.size());
		yggdrasil_assert(::std::all_of(
				querySamples.cbegin(), querySamples.cend(),
				[](const Sample_t& s) -> bool {return s.isValid();}));
		yggdrasil_assert(::std::all_of(
				trueQueryPDFs.cbegin(), trueQueryPDFs.cend(),
				[](const Real_t x) -> bool {return ((x >= 0.0) && isValidFloat(x));}));


		/*
			* Evaluating the Tree at the query points
			*/

		//Prepare the results
		PDFValues_t estQueryPDF;

		//Evaluating the PDF of the estimator
		estQueryPDF = currentTree.evaluateSamplesAt(querySamples);

		yggdrasil_assert(estQueryPDF.size() == querySize);
		yggdrasil_assert(::std::all_of(
				estQueryPDF.cbegin(), estQueryPDF.cend(),
				[](const Real_t x) -> bool {return ((x >= 0.0) && isValidFloat(x));}));




		/*
		 * Evaluating the MISE
	 	 */

		//This is the variable for summing up the Values
		::std::vector<Real_t> pMISE(16, 0.0);

		//Terate over the two samples and iterate over the
		//the two value ranges.
		//using equation (9) of the paper
		for(Size_t q = 0; q != querySize; ++q)
		{
			//Load the two values
			const auto pEst  = estQueryPDF[q];	//This is the estimated value
			const auto pTrue = trueQueryPDFs[q];	//This is the true value

			yggdrasil_assert(isValidFloat(pEst) == true);
			yggdrasil_assert(pEst >= 0.0);
			yggdrasil_assert(isValidFloat(pTrue) == true);
			yggdrasil_assert(pTrue > 0.0);	//Must be strigtly greater than zero

			//Compute the local MISE, We will never have a division by zero

			//Calculating the three individual terms

			//First term \frac{\hat{p}(x_q)^2}{p(x_q)}
			const Real_t term1 = pEst * pEst / pTrue;
			yggdrasil_assert(isValidFloat(term1));

			// Second term -2.0 \cdot p(x_q)
			const Real_t term2 = -2.0 * pEst;

			//Third term
			const Real_t term3 = pTrue;

			//Sum up the value and store it
			pMISE[q % 16] += (term1 + term2 + term3);
		}; //End for(q): evaluating the tree

		//Makeing the reduction
		Size_t RedCoef[] = {8, 4, 2, 1};
		for(const Size_t ende : RedCoef)
		{
			for(Size_t k = 0; k != ende; ++k)
			{
				pMISE[k] += pMISE[k + ende];
				pMISE[k + ende] = Real_t(0.0);
			}; //ENd for(k)
		}; //End for(ende)

		yggdrasil_assert(pMISE[0] > 0.0);
		yggdrasil_assert(::std::all_of(
			pMISE.cbegin() + 1, pMISE.cend(),
			[](const Real_t x) -> bool {return (x == Real_t(0.0));}));


		//Now make the true reduction
		const Real_t trueMISE = pMISE[0] / querySize;
		yggdrasil_assert(isValidFloat(trueMISE));
		(void)trueMISE;
	}; //ENd running the experiment


	std::cout
		<< "\n"
		<< "Your wish has surpassed entropy." << "\n"
		<< "    -- Kyubey" << "\n"
		<< std::endl;


	/*
	 * ===============
	 * Returning
	 */
	return 0;
}; //End main


/*
 * ====================
 * Impel Helperfunction
 */

std::unique_ptr<Distribution_t>
getDistribution(
	const eDistiType 	dType,
	const Int_t		dim)
{
	return yggdrasil::yggdrasil_randomDistribution_i::FACTORY(dType, dim).first;
}; //End get Distri

std::string
printDistribution(
	const eDistiType 	dType,
	const Int_t 		dim)
{
	return yggdrasil::yggdrasil_randomDistribution_i::FACTORY(dType, dim).second;
}; //End print



