/**
 * \brief	This file contains the code for the constant model that deals with the evaluation of the model.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_constModel.hpp>

//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)


Numeric_t
yggdrasil_constantModel_t::evaluateModelInSingleDim(
	const Numeric_t 	x,
	const Int_t 		d,
	const IntervalBound_t&	domain)
 const
{
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d < m_dims);
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(Size_t(m_dims) == m_volFactor.size());
	yggdrasil_assert(domain.isValid());
	yggdrasil_assert(isValidFloat(m_transFactor));
	yggdrasil_assert(0.0 < m_transFactor);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an empty model.");
	};
	if(this->isDimensionFitted(d) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension " + std::to_string(d) + " is not fitted.");
	};

#ifndef NDEBUG
	yggdrasil_assert(domain.isValid());
	const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
	yggdrasil_assert(unitInterval.isInside(x) || unitInterval.isCloseToUpperBound(x));
#endif

	//This is the pdf that we have would have in the rescaled Space
	const Numeric_t PDF_rescaled = 1.0;

	//We have to rescale it such that we have the value
	//inside the original data space. for that we have to divide by the volume
	const Numeric_t PDF_global = PDF_rescaled * m_volFactor[d];	//We have applied the division already
	yggdrasil_assert(PDF_global >= 0.0);
	yggdrasil_assert(isValidFloat(PDF_global) == true);

	//We do not have to apply the correction for the samples

	//However we have to keep in mind that we must return zero if no samples are inside the domain.
	return (m_nSamples == 0 ? 0.0 : PDF_global);
}; //End: Evaluate in single dimension



Numeric_t
yggdrasil_constantModel_t::evaluateModelForSample(
	const Sample_t&		s,
	const HyperCube_t&	domain,
	const Size_t 		nTotSamples)
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an empty model.");
	};
	if(this->allDimensionsFitted() == false)	//Inmplies all association
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Trioed to evaluate the samples, but not all dimensions where fitted.");
	};

	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(m_dims == s.nDims());
	yggdrasil_assert(Size_t(m_dims) == m_volFactor.size());
	yggdrasil_assert(domain.isValid());
	yggdrasil_assert(s.isInsideUnit());
	yggdrasil_assert(isValidFloat(m_transFactor));
	yggdrasil_assert(0.0 < m_transFactor);

	//Now evaluate the sample
	//Test if the answer is 0 because of no samples
	if(m_nSamples == 0)
	{
		return 0.0; //No samples in it, so zero.
	};

	//This is the correction value that is due to the
	//number of samples
	Numeric_t res = (Numeric_t(m_nSamples)) / nTotSamples;
	yggdrasil_assert(isValidFloat(res) == true);
	yggdrasil_assert(res >= 0.0);

#if 1
	/*
	 * Siche we have calculated the volume factor allready
	 * we just have to incooperate it into the res value
	 */
	res = res * m_transFactor;
#else

	//THis is the unroling factor
	const Size_t unrollFactor  = 4;
	const Size_t leftOvers     = m_dims % unrollFactor;
	const Size_t unrolledLoops = m_dims - leftOvers;

	//These are the accumulators
	Numeric_t res1 = 1.0;
	Numeric_t res2 = 1.0;
	Numeric_t res3 = 1.0;
	Numeric_t res4 = 1.0;
	for(Size_t i = 0; i != unrolledLoops; i += unrollFactor)
	{
		//Load the data
		const Numeric_t v1 = m_volFactor[i + 0];
		const Numeric_t v2 = m_volFactor[i + 1];
		const Numeric_t v3 = m_volFactor[i + 2];
		const Numeric_t v4 = m_volFactor[i + 3];

		//Perform the multiplication
		res1 = res1 * v1;
		res2 = res2 * v2;
		res3 = res3 * v3;
		res4 = res4 * v4;

		//Make tests
		yggdrasil_assert(res1 >= 0.0);
		yggdrasil_assert(isValidFloat(res1) == true);
		yggdrasil_assert(res2 >= 0.0);
		yggdrasil_assert(isValidFloat(res2) == true);
		yggdrasil_assert(res3 >= 0.0);
		yggdrasil_assert(isValidFloat(res3) == true);
		yggdrasil_assert(res4 >= 0.0);
		yggdrasil_assert(isValidFloat(res4) == true);
	}; //End for(v)

	//Handling the left over
	if(leftOvers != 0)
	{
		for(Int_t i = unrolledLoops; i != m_dims; ++i)
		{
			res = res * m_volFactor[i];
			yggdrasil_assert(res >= 0);
			yggdrasil_assert(isValidFloat(res) == true);
		}; //End for(i): left overs
	}; //ENd if: handling left overs

	//Compute the final value
	res1 = res1 * res2;
	res3 = res3 * res4;
	res1 = res1 * res3;
	res  = res  * res1;
#endif
	//Some final tests
	yggdrasil_assert(isValidFloat(res) == true);
	yggdrasil_assert(res >= 0.0);

	//Return teh vbalue
	return res;
}; //End: Evaluate for one sample



yggdrasil_constantModel_t::ValueArray_t
yggdrasil_constantModel_t::evaluateModelInSingleDim(
	const ValueArray_t&	xSamples,
	const Int_t 		d,
	const IntervalBound_t&	domain)
	const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(d < m_dims);
	yggdrasil_assert((Size_t)m_dims == m_volFactor.size());
	yggdrasil_assert(domain.isValid());
	yggdrasil_assert(isValidFloat(m_transFactor));

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an empty model.");
	};
	if(this->isDimensionFitted(d) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension " + std::to_string(d) + " is not fitted.");
	};


#ifndef NDEBUG
	const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
	//Here make a test
	yggdrasil_assert(::std::all_of(
		xSamples.cbegin(),
		xSamples.cend(),
		[unitInterval](const Numeric_t x) -> bool {return (unitInterval.isInside(x) || unitInterval.isCloseToUpperBound(x)) ? true : false;}
			)  //End: all_of
			); //End: assert
#endif
	//Number of samples that we have to compute
	const auto nXSamples = xSamples.size();

	/*
	 * CASE:
	 * 	There are no samples inside this,
	 * 	so the PDF is zero
	 */
	if(m_nSamples == 0)
	{
		//NMake the return value
		const ValueArray_t ret(nXSamples, 0.0);

		//Return the value
		return ret;
	}; //End if: there are no samples in this, so we have to return zero

	/*
	 * CASE:
	 * 	The normal case, we have to compute the value.
	 * 	Since they are independend of the samples we can no it very easaly
	 */


	//This is the slope we have to use
	//We check here if some  samples are here
	const Numeric_t  f = m_volFactor.at(d);

	//All the same
	ValueArray_t res(nXSamples, f);

	//return the value
	return res;
}; //End: evaluate model at many location in one dimension





YGGDRASIL_NS_END(yggdrasil)
