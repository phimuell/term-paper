/**
 * \brief	This file contains the code that is used during the fitting process of the tree.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_linModel.hpp>

//INcluide std
#include <memory>
#include <cmath>
#include <vector>

YGGDRASIL_NS_BEGIN(yggdrasil)



yggdrasil_linearModel_t::ValueArray_t
yggdrasil_linearModel_t::getExpectedCountsInBins(
	const Int_t 		d,
	const BinEdgeArray_t& 	binEdges,
	const IntervalBound_t&	domain)
	const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert((Size_t)m_dims == m_dimParam.size());
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d < m_dims);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an empty model.");
	};
	if(this->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use a model with not all dimension fitte.");
	};
	if(binEdges.isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The ein edges is not associated.");
	};
	if(m_nSampleBase == 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("There are no samples in the doamin.");
	};
	if(domain.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The domain is invalid.");
	};

	/*
	 * For finding the expected number in each bin, we compute the CDF in each bin
	 * and then multiply by the number of samples.
	 * It is important that we can completely ignore the volFactor here.
	 *
	 * The PDF fucntion, we have to integrate is:
	 * 	p(x)  := \left( x - \frac{1}{2} \right) \cdot \theta + 1 = \theta \cdot x + \beta,
	 * 	\beta := 1 - \frac{\theta}{2}
	 *
	 * Integrating results in
	 * 	P(x)  := \frac{\theta}{2} \cdot x^2 + \beta \cdot x + \gamma
	 * where \gamma is the integration constant.
	 * The requierement  P(x <- 0) == 0, leads to the conslusion that \gamma := 0.
	 * When also testing P(x <- 1), we see that this is indeed 1, as it should be.
	 *
	 * So we have now:
	 * 	P(x)   := \alpha \cdot x^2 + \beta \cdot x
	 * 	\alpha := \frac{\theta}{2}
	 *
	 * In the following we also assume that the full interval of *thsi concide
	 * with the interval that was binned.
	 */

	//The number of bins
	const Size_t nBins = binEdges.nBins();
	yggdrasil_assert(nBins > 0);

	//Handle the situiation when only one bin is present
	if(nBins == 1)
	{
		//Only one bin, so all should be in that bin
		ValueArray_t result(nBins, (Numeric_t)m_nSampleBase);

		return result;
	}; //End handle the case of one bin

	//thios is teh result array
	ValueArray_t expectedCounts(nBins, 0.0);
	yggdrasil_assert(expectedCounts.size() == nBins);

	//These are the paramater
	yggdrasil_assert(m_dimParam.at(d).hasValidParameters());
	const rNumeric_t Theta = m_dimParam[d].getTheta();

	//Calculating the parameters of the CDF
	const rNumeric_t alpha = Theta * 0.5;
	const rNumeric_t beta  = 1.0 - Theta * 0.5;

	//This is the old value that was computed
	//Technically it is the CDF evaluated at the upper bound of
	//the prevous bin, By definition it is zero
	rNumeric_t oldCDF = 0.0;

	//Iterating through the bins and evaluate the values
	//at the upper bound of the cuurent interval
	//The last one is handled because it is known
	for(Size_t b = 0; b != nBins; ++b)
	{
		//Thi is the upper bound of the current interval
		const rNumeric_t xUpperBound = binEdges.getBinInterval(b).getUpperBound();

		//Now evaluate the CDF there
		//using Horrnet; Handle the last bin directly
		const rNumeric_t CDFUpperBound = (b == (nBins - 1)) ? 1.0 : (xUpperBound * ( alpha * xUpperBound + beta));
		yggdrasil_assert(oldCDF < CDFUpperBound);

		//Now subtract it frrm the last value, to get the CDF inside the bin
		//rather than from the x == 0
		const rNumeric_t CDFInBin = CDFUpperBound - oldCDF;

		//We clamp it to zero
		if(CDFInBin <= BIG_EPSILON)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("There was an error in the calculation of bin " + std::to_string(b));
		};
		const rNumeric_t CDFInBinClamp = std::max<rNumeric_t>(CDFInBin, 0.0);

		//Now calculate the expected counts
		expectedCounts.at(b) = CDFInBinClamp * m_nSampleBase;

		//Now propagate the value we must propagete the CDF value at the upper bound
		//not the value in the bin
		oldCDF = CDFUpperBound;
	}; //End for(b): calculating the CDF VAlue in each bin

	//Return the data
	return expectedCounts;
}; //End: expected counts




yggdrasil_linearModel_t::BinEdgeArray_t
yggdrasil_linearModel_t::generateBins(
	const Int_t 		d,
	const Int_t 		nBins,
	const IntervalBound_t&	domain)
 const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert((Size_t)m_dims == m_dimParam.size());
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d < m_dims);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an empty model.");
	};
	if(this->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use a model with not all dimension fitte.");
	};
	if(this->isConsistent() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an inconsistent model.");
	};
	if(nBins <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("There where zero or less bins requested, it was " + std::to_string(nBins));
	};
	if(m_nSampleBase == 0)
	{
		/*
		 * Note:
		 * The evaluating function of the object should be able to handle the case
		 * with 0 sample. This function could be patched to allow the handling of
		 * this case, however I consider it more of an error, calling this function
		 * in such a case in the first place.
		 * Since the test is trivial.
		 *
		 * Thus I will not patch the function top handle this case.
		 * And the check remains in effect.
		 */
		throw YGGDRASIL_EXCEPT_RUNTIME("There are no samples in the doamin.");
	};
	if(domain.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The domain is invalid.");
	};


	/*
	 * For finding the expected number in each bin, we compute the CDF in each bin
	 * and then multiply by the number of samples.
	 * It is important that we can completely ignore the volFactor here.
	 *
	 * The PDF fucntion, we have to integrate is:
	 * 	p(x)  := \left( x - \frac{1}{2} \right) \cdot \theta + 1 = \theta \cdot x + \beta,
	 * 	\beta := 1 - \frac{\theta}{2}
	 *
	 * Integrating results in
	 * 	P(x)  := \frac{\theta}{2} \cdot x^2 + \beta \cdot x + \gamma
	 * where \gamma is the integration constant.
	 * The requierement  P(x <- 0) == 0, leads to the conslusion that \gamma := 0.
	 * When also testing P(x <- 1), we see that this is indeed 1, as it should be.
	 *
	 * So we have now:
	 * 	P(x)   := \alpha \cdot x^2 + \beta \cdot x
	 * 	\alpha := \frac{\theta}{2}
	 *
	 * We now eant equaly spaced quantiles,
	 * so we must solve the equation:
	 * 	q == \alpha \cdot x^2 + \beta \cdot x
	 * for x.
	 * Which results in the quadratioc equation:
	 * 	0 == \alpha \cdot x^2 + \beta \cdot x - q
	 *
	 * Appling the formula leads us to
	 * 	x_q := \frac{-\beta \pm \sqrt{\beta^2 + 4 \cdot \alpha \cdot q}}{2 \alpha}
	 * Technicla considerations gives us that only the positive solution is the one
	 * we are after so
	 * 	x_q := \frac{-\beta + \sqrt{\beta^2 + 4 \cdot \alpha \cdot q}}{2 \alpha}
	 *
	 * This equation s solved using the method that is proposed in Numerical Recipies,
	 * See second issue, formula (5.6.4) and (5.6.5).
	 */

	//The domain must be used is the unit interval
	const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();

	//We first handle the case where we have only one bin to build
	if(nBins == 1)
	{
		//This are the inner points which are empty
		ValueArray_t innerPoints;

		//COnstruct is use the building constructor directly
		BinEdgeArray_t binEdges(unitInterval, innerPoints);
		yggdrasil_assert(binEdges.isAssociated() == true);
		yggdrasil_assert((Int_t)binEdges.nBins() == nBins);

		//Return the bins
		return binEdges;
	}; //End if: only one bin

	/*
	 * Handle of the many bin case
	 */

	//This is the value of how far appart each quantile is
	const Real_t quantileSpread = Real_t(1.0) / nBins;
	yggdrasil_assert(isValidFloat(quantileSpread));

	//This is the array that saves the inenr edges
	ValueArray_t innerEdges;
	innerEdges.reserve(nBins - 1);

	//Get the coefficient
	const Real_t Theta = m_dimParam.at(d).getTheta();

	//This are the coefficient fromt the equation
	const Real_t Alpha = Theta * 0.5;
	const Real_t Beta  = Real_t(1.0) - Theta * 0.5;

	/*
	 * It can be that alpha is extremly small
	 * Meaning that we have essentially a linear
	 * function.
	 * Since thsi will result in some numerical difficulties
	 * There is a trap door to handle that case
	 */

	//Test if Alpha is too small
	const bool alphaIsTooSmall = (std::abs(Alpha) < 200 * Constants::EPSILON) ? true : false;

	if(alphaIsTooSmall == false)
	{
		/*
		 * We are not in teh degenerated case
		 */
		for(Int_t i = 1; i != nBins; ++i)
		{
			//This is the quantile we want to generate
			//It would be actually i * quantileSpread + lB ; but lB is zero
			const Real_t qValue = i * quantileSpread;

			//Now solve the quadratic equation
			const Real_t solvedBinEdge = internal::ygInternal_quadraticSolve(
					Alpha,
					Beta,
					-qValue		//It is important that the minus is there, see the equation above
					);

			//Save the point
			innerEdges.push_back(solvedBinEdge);
		}; //ENd for(i): filling the inner points
	}
	else
	{
		/*
		 * We are in the linear case that needs a handling
		 * as it was in the constant case
		 */
		for(Int_t i = 1; i != nBins; ++i)
		{
			//This is the quantile we want to generate
			//It would be actually i * quantileSpread + lB ; but lB is zero
			const Real_t qValue = i * quantileSpread;

			//It is a linear equation, so it is directly the quantile

			//Save the point
			innerEdges.push_back(qValue);
		}; //ENd for(i): filling the inner points
	}; //End if:

	//make a small test
	if(!(Int_t(innerEdges.size()) == (nBins - 1)))
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The number of bins is not correct");
	};
#ifndef NDEBUG
	if(!(::std::is_sorted(innerEdges.cbegin(), innerEdges.cend(), [](const Real_t lhs, const Real_t rhs) -> bool {return (lhs < rhs) ? true : false;})))
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The edges are not sorted, but they should.");
	};
#endif

	/*
	 * Creating the binn edge container
	 */
	BinEdgeArray_t binEdges(unitInterval, innerEdges);

	//Tests
	yggdrasil_assert((Int_t)binEdges.nBins() == nBins);
	yggdrasil_assert(binEdges.isAssociated() == true);

	//Return
	return binEdges;
}; //ENd: generate bins



YGGDRASIL_NS_END(yggdrasil)





