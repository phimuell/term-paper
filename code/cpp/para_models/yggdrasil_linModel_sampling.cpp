/**
 * \brief	This finle implements the functions of the linear model, that deals woith the generation of the sampling
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_linModel.hpp>

//INcluide std
#include <memory>
#include <cmath>
#include <vector>

YGGDRASIL_NS_BEGIN(yggdrasil)


void
yggdrasil_linearModel_t::drawSampleOnThis(
		Sample_t* const   		s,
		const MultiCondition_t& 	reNodeDSCondition,
		const HyperCube_t& 		domain)
{
	//First of all test if the null pointer is given
	if(s == nullptr)
	{
		throw YGGDRASIL_EXCEPT_NULL("The sample that is given is the null pointer.");
	};

	//Test if the dimensiuon matches
	if(Size_t(s->nDims()) != m_dimParam.size())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension of the source sample is " + std::to_string(s->nDims()) + " but the dimnension of the model is " + std::to_string(m_dimParam.size()));
	};

	//Test if this is full fitted
	if(this->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The model is not completly fitted.");
	};
	yggdrasil_assert(this->isAssociated());

	//This is teh dimension
	const Size_t nDims = m_dimParam.size();

	/*
	 * If no restriction is done
	 * we will use a more efficient version as
	 * when restriction is in place
	 */
	if(reNodeDSCondition.noConditions() == true)
	{
		/*
		 * No conditions are in place
		 */

		//We will now go through the different dimension and calculate the sample
		//During that we will do the test
		for(Size_t i = 0; i != nDims; ++i)
		{
			//Load the current value
			const Real_t srcX = (*s)[i];

			//Test if teh sample is valid
			yggdrasil_assert(isValidFloat(srcX));

			//Test if the sample lies inside the domain
			yggdrasil_assert((0.0 <= srcX) && (srcX <= 1.0));

			//Load the slope parameter
			const Real_t theta = m_dimParam[i].getTheta();

			//Now inverse the value
			const Real_t invX = internal::ygInternal_quadraticInverse(theta, srcX);

			//Write the value back to the sample
			(*s)[i] = invX;
		}; //ENd for going through the data
	}
	else
	{
		/*
		 * Restricctions are in place
		 *
		 * This means we must use the inversion method use the
		 * value that is provided by the condition
		 */

		// Going through the loop dimensions and apply inversion
		// pr load the value from the condition
		for(Size_t i = 0; i != nDims; ++i)
		{
			//Get the condition of the current dimension.
			//Note that this can be invalid
			const auto sCondi = reNodeDSCondition.getConditionFor(i);

			//Test if we are on condition or not
			//We can do that by checking if the condition is invalid or not
			if(sCondi.isValid() == true)
			{
				/*
				 * We have to apply a condition
				 */
				//Test the condition
				yggdrasil_assert((Size_t)sCondi.getConditionDim() == i);
				yggdrasil_assert(isValidFloat(sCondi.getConditionValue()));

				//Load the value into the sample
				(*s)[i] = sCondi.getConditionValue();
			}
			else
			{
				/*
				 * We have to apply real inversiuon
				 */

				//The condition must be invalid
				yggdrasil_assert(sCondi.isValid() == false);

				//Load the current value
				const Real_t srcX = (*s)[i];

				//Test if teh sample is valid
				yggdrasil_assert(isValidFloat(srcX));

				//Test if the sample lies inside the domain
				yggdrasil_assert((0.0 <= srcX) && (srcX <= 1.0));

				//Load the slope parameter
				const Real_t theta = m_dimParam[i].getTheta();

				//Now inverse the value
				const Real_t invX = internal::ygInternal_quadraticInverse(theta, srcX);

				//Write the value back to the sample
				(*s)[i] = invX;
			}; //End else: real inversion
		}; //End for(i): going through the dimension and handling them
	}; //End else: restrictions are in place


	//Now we are done
	return;

	(void)domain;
}; //End: draw sample on this


YGGDRASIL_NS_END(yggdrasil)


