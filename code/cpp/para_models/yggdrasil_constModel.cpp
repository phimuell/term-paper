/**
 * \brief	Thhis file contains the fucntions of the constant model, that can not be sorted into an other category.
 *
 * YOu can see them as left overs.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_constModel.hpp>

//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)


Numeric_t
yggdrasil_constantModel_t::getBoundDOF()
 const
{
	return 0.0;
};


std::string
yggdrasil_constantModel_t::print(
	const std::string&	intent)
 const
{
	std::string resString;

	//Beginning of the string
	//The first time the intend did not need to be applied, since it is
	//applied extern.
	resString = "MODEL(Const) = ";

	//Test if associated and made some more info
	if(this->isAssociated() == false)
	{
		//The model is not associated, so we have no parameter to plot
		//so only this information can be given
		resString += "Associated: NO";

		//Return the string
		return resString;
	}; //End if: not associated

	/*
	 * This code handles the case, wehere
	 * we have something to print.
	 */

	resString += "Associated: YES;";

	/*
	 * Now it comes the parameter, we put them on a new line
	 */

	//the new line
	resString += "\n";

	//Now the intent again and some more space to make it clear
	resString += intent
		+ "     "
		+ "Param = [";

	//Loop over the diomension
	bool firstLoop = true;
	const auto dimList = this->getAllDimensionKeys();
	for(const auto i : dimList)
	{
		if(firstLoop == false)
		{
			resString += ", ";
		}; //End if: first iteration

		//Compose the string
		if(this->isDimensionFitted(i) == true)
		{
			resString += "{" + std::to_string(i) + ": " + std::to_string(m_volFactor.at(i)) + "}";
		}
		else
		{
			resString += "{" + std::to_string(i) + ": NOT FITTED}";
		};

		//We have done the first loop
		firstLoop = false;
	}; //End for(i)

	//End of the string
	resString += "]";

	//Return the string
	return resString;

}; //End print



bool
yggdrasil_constantModel_t::isDimensionFitted(
	const Int_t 	d)
 const
{
	//No empty chack can be performed, since this tests if something is empty
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("A non associated model, can not be quieried if a doimension is fitted or not.");
	};
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d < m_dims); //Implies that d < MAX_DIMS
	yggdrasil_assert(Size_t(m_dims) == m_volFactor.size());

	return (::std::isnan(m_volFactor[d]) == true) ? false : true;
}; //End: test if single dimension is fitted


bool
yggdrasil_constantModel_t::allDimensionsFitted()
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("It is undefined if a dimension is fitted if the model is not associated.");
	};
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(m_volFactor.size() == (Size_t)m_dims);

	//We test if all dimensions are tested
	//We do nto have to check if *this is bound to data
	for(const Numeric_t& v : m_volFactor)
	{
		if(::std::isnan(v) == true)
		{
			//Some elements still have nan, so not all are fitted
			return false;
		};
	}; //End for(v):

	//All dimensions have a valid parameter, so return it
	return true;
}; //end: allDimsnionTested



bool
yggdrasil_constantModel_t::isAssociated()
 const
{
	if(m_isAssocsiated == true)
	{
		yggdrasil_assert(m_dims > 0);
		yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
		if((Size_t)m_dims != m_volFactor.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The underling structure of the model has th wrong dimension.");
		};

		return true;
	}; //End if: is associated true

	return false;
}; // End: isEmpty


Int_t
yggdrasil_constantModel_t::nDifferentFittings()
 const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(Size_t(m_dims) == m_volFactor.size());

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model must be associated to data.");
	};

	return m_dims;
}; //End get the number of fittings



yggdrasil_constantModel_t::DimList_t
yggdrasil_constantModel_t::getAllDimensionKeys()
 const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(Size_t(m_dims) == m_volFactor.size());

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("For returning the list of diomensions, the node needs to be associated.");
	};


	//Create the list and fill it
	DimList_t keys(m_dims);

	for(Int_t d = 0; d != m_dims; ++d)
	{
		keys[d] = d;
	}; //End for(d)

	return keys;
}; //End: getAllDimensionsKEys


Size_t
yggdrasil_constantModel_t::nSampleBase()
 const
 noexcept(false)
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this is not initalized.");
	};
	if(this->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Not all dimensions where fitted.");
	};
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(Size_t(m_dims) == m_volFactor.size());


	return m_nSamples;
}; //End: nSampleBase





bool
yggdrasil_constantModel_t::isConsistent()
	const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert((Size_t)m_dims == m_volFactor.size());
	yggdrasil_assert(isValidFloat(m_transFactor));
	yggdrasil_assert(0.0 < m_transFactor);

	//If empyt, then one can not define a consistency
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("An empty constant model can not be consistent or inconsistent. It is undefined.");
	};
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);

	/*
	 * We can not uphold the guarantee we made
	 * so we do the best we can.
	 */
	return (m_nAddedSamples == 0) ? true : false;
};



YGGDRASIL_NS_END(yggdrasil)
