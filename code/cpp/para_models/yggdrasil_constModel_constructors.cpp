/**
 * \brief	This file contains the constructors for the constant model.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_constModel.hpp>

//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_constantModel_t::yggdrasil_constantModel_t(
	const Int_t 	d) :
  yggdrasil_parametricModel_i(),
  m_volFactor(),
  m_nSamples(0),
  m_nAddedSamples(0),
  m_isAssocsiated(false),
  m_dims(d),
  m_transFactor(NAN)
{
	if(d == Constants::YG_INVALID_DIMENSION)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is invalid.");
	};
	if(d < 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is negative.");
	};
	if(d >= Constants::YG_MAX_DIMENSIONS)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is larger than the maximum.");
	};

	/*
	 * Previously the allocation of the internal structres happened here.
	 * But because of the introduction of the preFittingHookInit() function,
	 * this was moved to the duty of that function.
	 *
	 * We also allow a dimention of zero.
	 * This indicates that the dimension will then be estimated from the
	 * node that is passed to the init fucntion.
	 */
}; //End constructor


yggdrasil_constantModel_t::yggdrasil_constantModel_t(
	std::string&	modDescr)
 :
  yggdrasil_constantModel_t(modDescr.empty() == true ? 0 : ::std::stoi(modDescr))
{
}; //End universal constructor



yggdrasil_constantModel_t::ParametricModel_ptr
yggdrasil_constantModel_t::creatOffspring()
 const
{
	return ParametricModel_ptr(new yggdrasil_constantModel_t(m_dims));
};


yggdrasil_constantModel_t::~yggdrasil_constantModel_t()
 noexcept = default;

yggdrasil_constantModel_t::yggdrasil_constantModel_t(
	const yggdrasil_constantModel_t&)
 = default;


yggdrasil_constantModel_t::yggdrasil_constantModel_t(
	yggdrasil_constantModel_t&&)
 noexcept = default;


yggdrasil_constantModel_t&
yggdrasil_constantModel_t::operator= (
	const yggdrasil_constantModel_t&)
 = default;

yggdrasil_constantModel_t&
yggdrasil_constantModel_t::operator= (
	yggdrasil_constantModel_t&&)
 noexcept = default;


yggdrasil_constantModel_t::ParametricModel_ptr
yggdrasil_constantModel_t::clone()
 const
{
	return ParametricModel_ptr(new yggdrasil_constantModel_t(*this));
}; //ENd cloneing








YGGDRASIL_NS_END(yggdrasil)
