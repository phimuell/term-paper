/**
 * \brief	This finle implements the functions of the linear model, that could not be sorted in into a category.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_linModel.hpp>

//INcluide std
#include <memory>
#include <cmath>
#include <vector>

YGGDRASIL_NS_BEGIN(yggdrasil)


YGGDRASIL_NS_BEGIN(internal)

/**
 * \brief	This is a small fucntion that computes the solution of a quadratic equation.
 *
 * It uses the stable unmerical scheme that is outlied in §5.6, second edioton of numerical recipies.
 *
 * This function returns the solution which lies in the sought range.
 *
 * \param  a 	Coefficient in fromt of x^2
 * \param  b	Coefficient in front of x
 * \param  c	Constant coefficient
 */
Real_t
ygInternal_quadraticSolve(
	const Real_t 	a,
	const Real_t 	b,
	const Real_t 	c)
{
	yggdrasil_assert(isValidFloat(a));
	yggdrasil_assert(isValidFloat(b));
	yggdrasil_assert(isValidFloat(c));

	if(std::abs(a) < 100 * Constants::EPSILON)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The value of a, " + std::to_string(a) + ", is to small to be used.");
	};

	/*
	 * q := \frac{1}{2} \left[ b + \sgn(b) \sqrt{b^2 - 4 a c } \right]
	 *
	 * x_1 := \frac{q}{a}
	 * x_2 := \frac{c}{q}
	 */

	//First lets compute the determinante
	const Real_t Det2 = b * b - 4.0  * a * c;	//This is the square of teh actual determinante

	//We require that the determinante is not negative
	//for accounting for small errors we permitt some thrash value
	if(Det2 <= (-0.0001))
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The determinanet iwas to small.");
	};

	//Now we clamp the determinante, we know that it is not too small
	const Real_t Det2Clamp = std::max<Real_t>(0.0, Det2);

	//This is the root of the determinante
	const Real_t Det  = ::std::sqrt(Det2Clamp);

	//Now we compute the value in the square braket
	//in the equation above, this is 2 times q
	Real_t TwoTimesQ;
	if(0.0 <= b)
	{
		//B is positive, also \sgn(b) == +1
		TwoTimesQ = b + Det;
	}
	else
	{
		//B is negative, also sig(b) == -1
		//It looks like an subtraction, but its more like an addition
		TwoTimesQ = b - Det;
	};

	//Now we compute the real Q
	//we must multiply by (-1/2); See equation (5.6.4)
	//Numerical Reciepice Volume 2
	const Real_t Q = TwoTimesQ * (-0.5);


	//Now we can compute the two solutions
	const Real_t X1 = Q / a;
	const Real_t X2 = c / Q;

	//Test if they are good
	if(isValidFloat(X1) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The first solution has become invalid");
	};
	if(isValidFloat(X2) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The second solution has become invalid");
	};

	//We will now sort the two solutions such that they
	const Real_t X_min = ::std::min(X1, X2);
	const Real_t X_max = ::std::max(X1, X2);

	/*
	 * After doing some math
	 * This is a nioce euphemism for using Mathematica and realize
	 * how much one has forgotten.
	 *
	 * One sees that there are two cases:
	 * 	\item[\theta > 0]
	 * 	In this case the correct solution is the larger one.
	 *
	 * 	\item[\theta < 0]
	 * 	In this case the correct solution is the smaller one.
	 */

	//Get the correct sollution
	const Real_t X_correct = (a > 0.0) ? (X_max) : (X_min);

	//We make some testing also ensuring that we clamp the result appropriate

	//Tests
	if(X_correct < (-0.0001))
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The computed value " + std::to_string(X_correct) + " is too small");
	};
	if(X_correct > (1.0001))
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The computed value " + std::to_string(X_correct) + " is too large.");
	};

	//Const clamp the result
	const Real_t X_correct_c1 = std::max<Real_t>(0.0, X_correct   );
	const Real_t X_correct_fi = std::min<Real_t>(1.0, X_correct_c1);

	//Return the value
	return X_correct_fi;
}; //End: quandratic solve


Real_t
ygInternal_quadraticInverse(
	const Real_t 	theta,
	const Real_t 	rhs)
{
	//This is teh unit interval it is used for testing
	const auto unitInterval(yggdrasil_intervalBound_t::CREAT_UNIT_INTERVAL());

	//Check if the rhs lies inside the unit intervals
	if((unitInterval.isInside(rhs) == false) && (unitInterval.isCloseToUpperBound(rhs) == false))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The rhs value " + std::to_string(rhs) + " does not liy inside the unit interval.");
	};

	//Now we check if the theta value is too small
	if(std::abs(theta) < 200 * Constants::EPSILON)
	{
		/*
		 * The theta value is so small that we should consider
		 * it zero, thus we only have a linear equation, with slope
		 * one. Thus the inversion value is rhs.
		 */
		return rhs;
	}; //ENd if: we have a linear equation to solve

	//Now compute the parameters
	const Real_t a = theta * 0.5;			//This is the coefficient in front of the quadratic term
	const Real_t b = Real_t(1.0) - theta * 0.5;	//This is the coefficient in front of the linear term
	const Real_t c = -rhs;				//This is the constant term

	//We now call the stable solve fucntion
	const Real_t invValue = ygInternal_quadraticSolve(a, b, c);

	//We check if the value is corect, thsi only has to be done by an assert
	yggdrasil_assert(unitInterval.isInside(invValue) || unitInterval.isCloseToUpperBound(invValue));

	//Now we return the value back to the fucntion
	return invValue;
}; //End: quadratic inverse

YGGDRASIL_NS_END(internal)


const Numeric_t yggdrasil_linearModel_t::BIG_EPSILON = -0.001;

Numeric_t
yggdrasil_linearModel_t::getBoundDOF()
 const
{
	return 1.0;
};




std::string
yggdrasil_linearModel_t::print(
	const std::string& 	intent)
 const
{
	std::string resString;
	resString.reserve(50 + m_dimParam.size() * 40); // This bound is completly ridiuculus

	//Beginning of the string
	//The first intent is already applied externaly.
	resString = "MODEL(Lin) = ";

	//Test if associated and made some more info
	if(this->isAssociated() == false)
	{
		//The model is not associated, so we have no parameter to plot
		//so only this information can be given
		resString += "Associated: NO";

		//Return the string
		return resString;
	}; //End if: not associated

	/*
	 * This code handles the case, wehere
	 * we have something to print.
	 */

	resString += "Associated: YES;";

	/*
	 * Now it comes the parameter, we put them on a new line
	 */

	//the new line
	resString += "\n";

	//Now the intent again and some more space to make it clear
	resString += intent
		+ "     "
		+ "Param = [";

	//Loop over the diomension
	bool firstLoop = true;
	const auto dimList = this->getAllDimensionKeys();
	for(const auto i : dimList)
	{
		//Load the parameter
		const Numeric_t theta = m_dimParam.at(i).getTheta();

		if(firstLoop == false)
		{
			resString += ", ";
		}; //End if: first iteration

		//Compose the string
		if(this->isDimensionFitted(i) == true)
		{
			resString += "{" + std::to_string(i) + ": " + std::to_string(theta) + "}";
		}
		else
		{
			resString += "{" + std::to_string(i) + ": NOT FITTED}";
		};

		//We have done the first loop
		firstLoop = false;
	}; //End for(i)

	//End of the string
	resString += "]";

	//Return the string
	return resString;
}; //End print




bool
yggdrasil_linearModel_t::isDimensionFitted(
	const Int_t 	d)
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model is not associated.");
	};
	yggdrasil_assert(0 <= d);
	yggdrasil_assert(0 < m_dims);
	yggdrasil_assert(d < m_dims);
	yggdrasil_assert((Size_t)m_dims == m_dimParam.size());

	//We can do this by simply testing if the slope of that dimension is NAN
	//We use here the light weight function
	return (m_dimParam.at(d).isInitialFitted() == true) ? true : false;
}; //End: is dimensiuon fitted


bool
yggdrasil_linearModel_t::allDimensionsFitted()
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model is not associated.");
	};
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert((Size_t)m_dims == m_dimParam.size());

	for(const SingleDimState_t& s : m_dimParam)
	{
		if(s.isInitialFitted() == false)
		{
			//One of the samples did not have valid doimensions
			//so not all are fitted
			return false;
		}; //End if:
	}; //End for(s): going through the dimensions

	//All of the dinensions have valid parameters, so all are fitted
	return true;
}; //End: allDimensionFitted


Int_t
yggdrasil_linearModel_t::nDifferentFittings()
 const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert((Size_t)m_dims == m_dimParam.size());

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model must be associated to data.");
	};

	return m_dims;
}; //End get the number of fittings


yggdrasil_linearModel_t::DimList_t
yggdrasil_linearModel_t::getAllDimensionKeys()
 const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert((Size_t)m_dims == m_dimParam.size());

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model is not associated.");
	};

	//Create the list and fill it
	DimList_t keys(m_dims);
	yggdrasil_assert(keys.size() == (Size_t)m_dims);

	for(Int_t d = 0; d != m_dims; ++d)
	{
		keys[d] = d;
	}; //End for(d)

	return keys;
}; //End: getAllDimensionsKEys


bool
yggdrasil_linearModel_t::isAssociated()
 const
{
	if(m_isAssocsiated == true)
	{
		yggdrasil_assert(m_dims > 0);
		yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
		if((Size_t)m_dims != m_dimParam.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The model is associated, but the underling structure is not.");
		};

		return true;
	}; //ENd if: this is associated

	return false;
}; //End: isAssociated






bool
yggdrasil_linearModel_t::isConsistent()
	const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("By definition the consistency of an empty model is undefined.");
	};

	//We are consistent if the number of samples and added samples is the same
	return (m_nSampleBase == m_nAddedSamples) ? true : false;
}; //End is consistent




Size_t
yggdrasil_linearModel_t::nSampleBase()
 const
 noexcept(false)
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Quering the sample base of an empyt model is considered an error.");
	};

	return m_nSampleBase;
}; //End get Sample base






YGGDRASIL_NS_END(yggdrasil)
