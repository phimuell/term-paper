/**
 * \brief	This file conatins the code that deals with the evaluation of the model.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_linModel.hpp>

//INcluide std
#include <memory>
#include <cmath>
#include <vector>

YGGDRASIL_NS_BEGIN(yggdrasil)



Numeric_t
yggdrasil_linearModel_t::evaluateModelInSingleDim(
	const Numeric_t 	x,
	const Int_t 		d,
	const IntervalBound_t&	domain)
 const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert((Size_t)m_dims == m_dimParam.size());
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d < m_dims);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an empty model.");
	};
	if(this->isDimensionFitted(d) == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Error: The dimension " + std::to_string(d) + " is not fitted.");
	};

#ifndef NDEBUG
	yggdrasil_assert(domain.isValid());
	const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
	yggdrasil_assert(unitInterval.isInside(x) || unitInterval.isCloseToUpperBound(x));
#endif
	yggdrasil_assert(0.0 <= x);
	yggdrasil_assert(x <= 1.0);

	//If no sample is inside the doamin, we have to return zero
	if(m_nSampleBase == 0)
	{
		return rNumeric_t(0.0);
	};

	//Loading the parameter
	yggdrasil_assert(m_dimParam.at(d).hasValidParameters());
	const rNumeric_t Theta     = m_dimParam[d].getTheta();
	const rNumeric_t volFactor = m_dimParam[d].getVolFactor();

	//Now calulate the value in the rescalled, element specific domain
	const rNumeric_t elementValue = (x - 0.5) * Theta + 1.0;

	//Test if we are to negative
	if(elementValue < BIG_EPSILON)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The value that was computed was to small, it was " + std::to_string(elementValue));
	};

	//Apply the correction value
	const rNumeric_t rootSpaceValue = elementValue * volFactor;

	//Evaluate
	//We will not clap a value that is very negative, since we have cheked above that it is positive
	return std::max<Numeric_t>(rootSpaceValue, 0.0);
}; //End: evaluate single value in single dimension





yggdrasil_linearModel_t::ValueArray_t
yggdrasil_linearModel_t::evaluateModelInSingleDim(
	const ValueArray_t&	xSamples,
	const Int_t 		d,
	const IntervalBound_t&	domain)
 const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert((Size_t)m_dims == m_dimParam.size());
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d < m_dims);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an empty model.");
	};
	if(this->isDimensionFitted(d) == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Error: The dimension " + std::to_string(d) + " is not fitted.");
	};

#ifndef NDEBUG
	yggdrasil_assert(domain.isValid());
	const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
#endif

	//This is the nunmber of samples
	const Size_t N = xSamples.size();

	//Handle the case that no samples are present
	//In my view this should be an error
	if(N == 0)
	{
		return ValueArray_t();
	}; //End if: no samples where given

	//In the case of zero samples in the domain, we must return zero
	if(m_nSampleBase == 0)
	{
		ValueArray_t result;
		result.assign(N, rNumeric_t(0.0));

		return result;
	}; //End if: domain is empty


	//Loading the parameter
	yggdrasil_assert(m_dimParam.at(d).hasValidParameters());
	const rNumeric_t Theta     = m_dimParam[d].getTheta();
	const rNumeric_t volFactor = m_dimParam[d].getVolFactor();

	//These are some helper values that we need inside the code
	const rNumeric_t ThetaHalf 	   = Theta * 0.5;
	const rNumeric_t mThetaHalfPlusOne = 1.0 - ThetaHalf;

	//This is the result vector
	ValueArray_t result(N);
	yggdrasil_assert(result.size() == N);

	//This is the unrole factor, actually one should unrole more
	const Size_t unrollFactor = 4;
	const Size_t leftOvers    = N % unrollFactor;
	const Size_t unrolledIter = N - leftOvers;

	//Iterating over all the samples for evaluating them
	for(Size_t i = 0; i != unrolledIter; i += unrollFactor)
	{
		yggdrasil_assert(i < N);

		//Load the value
		const Numeric_t x1 = xSamples[i + 0];
		const Numeric_t x2 = xSamples[i + 1];
		const Numeric_t x3 = xSamples[i + 2];
		const Numeric_t x4 = xSamples[i + 3];

		//Make tests
		yggdrasil_assert(unitInterval.isInside(x1) || unitInterval.isCloseToUpperBound(x1));
		yggdrasil_assert(unitInterval.isInside(x2) || unitInterval.isCloseToUpperBound(x2));
		yggdrasil_assert(unitInterval.isInside(x3) || unitInterval.isCloseToUpperBound(x3));
		yggdrasil_assert(unitInterval.isInside(x4) || unitInterval.isCloseToUpperBound(x4));

		//Now calulate the value in the rescalled, element specific domain
		const rNumeric_t elementValue1 = x1 * Theta + mThetaHalfPlusOne;	//This is a more fma friendly way
		const rNumeric_t elementValue2 = x2 * Theta + mThetaHalfPlusOne;	//This is a more fma friendly way
		const rNumeric_t elementValue3 = x3 * Theta + mThetaHalfPlusOne;	//This is a more fma friendly way
		const rNumeric_t elementValue4 = x4 * Theta + mThetaHalfPlusOne;	//This is a more fma friendly way

		//Test if we are to negative
#ifndef NDEBUG
		if((elementValue1 < BIG_EPSILON))
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The value that was computed was to small,");
		};
		if((elementValue2 < BIG_EPSILON))
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The value that was computed was to small,");
		};
		if((elementValue3 < BIG_EPSILON))
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The value that was computed was to small,");
		};
		if((elementValue4 < BIG_EPSILON))
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The value that was computed was to small,");
		};
#endif

		//Apply the correction value
		const rNumeric_t rootSpaceValue1 = elementValue1 * volFactor;
		const rNumeric_t rootSpaceValue2 = elementValue2 * volFactor;
		const rNumeric_t rootSpaceValue3 = elementValue3 * volFactor;
		const rNumeric_t rootSpaceValue4 = elementValue4 * volFactor;

		//Evaluate and save
		result[i + 0] = std::max<Numeric_t>(rootSpaceValue1, 0.0);
		result[i + 1] = std::max<Numeric_t>(rootSpaceValue2, 0.0);
		result[i + 2] = std::max<Numeric_t>(rootSpaceValue3, 0.0);
		result[i + 3] = std::max<Numeric_t>(rootSpaceValue4, 0.0);
	}; //End for(i): evaluate at all position

	if(leftOvers != 0)
	{
		for(Size_t i = unrolledIter; i != N; i += 1)
		{
			//Load the value
			const Numeric_t x = xSamples[i];

			//Make tests
			yggdrasil_assert(unitInterval.isInside(x) || unitInterval.isCloseToUpperBound(x));

			//Now calulate the value in the rescalled, element specific domain
			//const rNumeric_t elementValue = (x - 0.5) * Theta + 1.0;
			const rNumeric_t elementValue = x * Theta + mThetaHalfPlusOne;	//This is a more fma friendly way

#ifndef NDEBUG
			//Test if we are to negative
			if(elementValue < BIG_EPSILON)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("The value that was computed was to small, it was " + std::to_string(elementValue));
			};
#endif

			//Apply the correction value
			const rNumeric_t rootSpaceValue = elementValue * volFactor;

			//Evaluate and save
			result[i] = std::max<Numeric_t>(rootSpaceValue, 0.0);
		}; //ENd for(i):
	}; //End if: handle the left overs


	//Teturn the vactor
	return result;
}; //End: evaluate in single dimension but at many locations


Numeric_t
yggdrasil_linearModel_t::evaluateModelForSample(
	const Sample_t&		s,
	const HyperCube_t&	domain,
	const Size_t 		nTotSamples)
	const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert((Size_t)m_dims == m_dimParam.size());
	yggdrasil_assert(s.nDims() == m_dims);
	yggdrasil_assert(domain.isValid());
	yggdrasil_assert(s.isInsideUnit() == true);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an empty model.");
	};
	if(m_dimParam.size() != s.size())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the sample is " + std::to_string(s.nDims()) + ", but the dimension of the parametric model is " + std::to_string(m_dimParam.size()));
	};
	if(this->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use a model with not all dimension fitte.");
	};

	//if no samples are inside the domain then we have to return palin zero
	if(m_nSampleBase == 0)
	{
		return rNumeric_t(0.0);
	};

	//This is the final result, initalized to the scaling factor
	rNumeric_t res = Numeric_t(m_nSampleBase) / Numeric_t(nTotSamples);

	//This is for the unrolling
	const Size_t unrollFactor = 4;
	const Size_t leftOvers    = m_dims % unrollFactor;
	const Size_t unrolledIter = m_dims - leftOvers;


	//These are the accumulators
	rNumeric_t res1 = 1.0;
	rNumeric_t res2 = 1.0;
	rNumeric_t res3 = 1.0;
	rNumeric_t res4 = 1.0;

	for(Size_t i = 0; i != unrolledIter; i += unrollFactor)
	{
		yggdrasil_assert(i < (Size_t)m_dims);

		//Check if the parameters are valid
		yggdrasil_assert(m_dimParam[i + 0].hasValidParameters());
		yggdrasil_assert(m_dimParam[i + 1].hasValidParameters());
		yggdrasil_assert(m_dimParam[i + 2].hasValidParameters());
		yggdrasil_assert(m_dimParam[i + 3].hasValidParameters());

		//Load the value of teh sample of dimension i
		const rNumeric_t x1 = s[i + 0];
		const rNumeric_t x2 = s[i + 1];
		const rNumeric_t x3 = s[i + 2];
		const rNumeric_t x4 = s[i + 3];

		//Load the parameter
		const rNumeric_t Theta1     = m_dimParam[i + 0].getTheta();
		const rNumeric_t volFactor1 = m_dimParam[i + 0].getVolFactor();

		const rNumeric_t Theta2     = m_dimParam[i + 1].getTheta();
		const rNumeric_t volFactor2 = m_dimParam[i + 1].getVolFactor();

		const rNumeric_t Theta3     = m_dimParam[i + 2].getTheta();
		const rNumeric_t volFactor3 = m_dimParam[i + 2].getVolFactor();

		const rNumeric_t Theta4     = m_dimParam[i + 3].getTheta();
		const rNumeric_t volFactor4 = m_dimParam[i + 3].getVolFactor();

		/*
		 * Compuet the diffrent values
		 *
		 * We will transform the equation
		 * 	(x - 0.5) * Theta + 1
		 * Into a more compute friendly form of
		 *  	x * a	+ b
		 *
		 * Where
		 * 	a :=	Theta
		 * 	b := 	-0.5 * Theta + 1
		 */

		//Calculate b
		const rNumeric_t b1 = -0.5 * Theta1 + 1.0;
		const rNumeric_t b2 = -0.5 * Theta2 + 1.0;
		const rNumeric_t b3 = -0.5 * Theta3 + 1.0;
		const rNumeric_t b4 = -0.5 * Theta4 + 1.0;

		//Now compute the contribution
		const rNumeric_t elementValue1 = x1 * Theta1 + b1;
		const rNumeric_t elementValue2 = x2 * Theta2 + b2;
		const rNumeric_t elementValue3 = x3 * Theta3 + b3;
		const rNumeric_t elementValue4 = x4 * Theta4 + b4;

		//Rescale the probability to the root data space
		const rNumeric_t rescalDataValue1 = elementValue1 * volFactor1;
		const rNumeric_t rescalDataValue2 = elementValue2 * volFactor2;
		const rNumeric_t rescalDataValue3 = elementValue3 * volFactor3;
		const rNumeric_t rescalDataValue4 = elementValue4 * volFactor4;

		yggdrasil_assert(BIG_EPSILON < rescalDataValue1);
		yggdrasil_assert(BIG_EPSILON < rescalDataValue2);
		yggdrasil_assert(BIG_EPSILON < rescalDataValue3);
		yggdrasil_assert(BIG_EPSILON < rescalDataValue4);


		//Now inccoperate it to the tentative value
		//one have to apply the max here, othervise negative negative will result in plus
		res1 *= std::max<rNumeric_t>(rescalDataValue1, 0.0);
		res2 *= std::max<rNumeric_t>(rescalDataValue2, 0.0);
		res3 *= std::max<rNumeric_t>(rescalDataValue3, 0.0);
		res4 *= std::max<rNumeric_t>(rescalDataValue4, 0.0);
	}; //End for(i)

	if(leftOvers != 0)
	{
		const Size_t D = (Size_t)m_dims;
		for(Size_t i = unrolledIter; i != D; ++i)
		{
			yggdrasil_assert(m_dimParam[i + 0].hasValidParameters());

			//Load the value of teh sample of dimension i
			const rNumeric_t x1 = s[i + 0];

			//Load the parameter
			const rNumeric_t Theta1     = m_dimParam[i + 0].getTheta();
			const rNumeric_t volFactor1 = m_dimParam[i + 0].getVolFactor();

			const rNumeric_t b1 = -0.5 * Theta1 + 1.0;

			//Now compute the contribution
			const rNumeric_t elementValue1 = x1 * Theta1 + b1;

			//Rescale the probability to the root data space
			const rNumeric_t rescalDataValue1 = elementValue1 * volFactor1;

			yggdrasil_assert(BIG_EPSILON < rescalDataValue1);

			//Now inccoperate it to the tentative value
			//one have to apply the max here, othervise negative negative will result in plus
			res *= std::max<rNumeric_t>(rescalDataValue1, 0.0);
		}; //End for(i):
	}; //End if: handling left over

	//Now make the reduction
	//The reduction is only needed if no loop iterations where needed
	//Because then the resX are one
	if(unrolledIter != 0)
	{
		res1 = res1 * res2;
		res3 = res3 * res4;
		res  = (res * res1) * res3;
	};

	//Retrunr the result
	return res;
}; //END: Evaluate for sample






YGGDRASIL_NS_END(yggdrasil)
