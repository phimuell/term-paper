/**
 * \brief	This file implements all the code taht is needed fro the testing process in the tree.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_constModel.hpp>

//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)



yggdrasil_constantModel_t::ValueArray_t
yggdrasil_constantModel_t::getExpectedCountsInBins(
	const Int_t 		d,
	const BinEdgeArray_t& 	binEdges,
	const IntervalBound_t&	domain)
	const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(d < m_dims);
	yggdrasil_assert((Size_t)m_dims == m_volFactor.size());
	yggdrasil_assert(domain.isValid());

	/*
	 * We do this tests here again to make sure that the error is
	 * raised at the correct location.
	 * We only do this for the debug mode.
	 */
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an empty model.");
	};
	if(this->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Not all dimensions are fitted.");
	};
	if(this->isConsistent() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried using an inconsistent model for the estimation test.");
	};
	if(isValidFloat(m_transFactor) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The transformation factor is not valid.");
	};
	if(m_transFactor <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The transformation factor is zero or negative.");
	};


	/*
	 * As we have stated we simply have to compuzte the average number of samples in each bin
	 * We allow for fractional count.
	 */

	//The number of bins
	const Int_t nBins = binEdges.nBins();

	if(nBins <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The number of bins was " + std::to_string(nBins) + ", this means it is negative or zero.");
	};

	//Thi is the average number of samples in each bin
	const Numeric_t avgSamplesPerBin = Numeric_t(m_nSamples) / nBins;
	yggdrasil_assert(avgSamplesPerBin >= 0.0);
	yggdrasil_assert(isValidFloat(avgSamplesPerBin) == true);

	//Now create the output vector
	const ValueArray_t expeCoutsInBins(nBins, avgSamplesPerBin);
	yggdrasil_assert(expeCoutsInBins.size() == (Size_t)nBins);

	//Return the count
	return expeCoutsInBins;
}; //End: Evaluate inside bins




yggdrasil_constantModel_t::BinEdgeArray_t
yggdrasil_constantModel_t::generateBins(
	const Int_t 		d,
	const Int_t 		nBins,
	const IntervalBound_t&	domain)
 const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(d < m_dims);
	yggdrasil_assert((Size_t)m_dims == m_volFactor.size());

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an empty model.");
	};
	if(this->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use a model with not all dimension fitte.");
	};
	if(this->isConsistent() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an inconsistent model.");
	};
	if(nBins <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("There where zero or less bins requested, it was " + std::to_string(nBins));
	};
	if(domain.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The domain is invalid.");
	};
	if(isValidFloat(m_transFactor) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The transformation factor is not valid.");
	};
	if(m_transFactor <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The transformation factor is zero or negative.");
	};

	/*
	 * As we have stated the bins are spreded equadistance.
	 * The reason is the that we have a constant PDF and thus a linear CDF.
	 * With solope 1, which is due to the rescalling of the data.
	 */

	//The domain must be used is the unit interval
	const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();

	//We first handle the case where we have only one bin to build
	if(nBins == 1)
	{
		//This are the inner points which are empty
		ValueArray_t innerPoints;


		//COnstruct is use the building constructor directly
		BinEdgeArray_t binEdges(unitInterval, innerPoints);
		yggdrasil_assert(binEdges.isAssociated() == true);
		yggdrasil_assert(binEdges.nBins() == 1);

		//Return the bins
		return binEdges;
	}; //End if: only one bin

	/*
	 * Handle of the many bin case
	 */

	//this is the spreading, or the width of each of bins
	const Real_t binLength = Real_t(1.0) / nBins;
	yggdrasil_assert(isValidFloat(binLength));

	//This is the array that saves the inenr edges
	ValueArray_t innerEdges;
	innerEdges.reserve(nBins - 1);

	//Fill the inner edges
	for(Int_t i = 1; i != nBins; ++i)
	{
		//It would be actually i * binLength + lB
		//but lB is zero
		innerEdges.push_back(i * binLength);
	}; //ENd for(i): filling the inner points
	yggdrasil_assert(Int_t(innerEdges.size()) == (nBins - 1));

	//Construct the bin
	BinEdgeArray_t binEdges(unitInterval, innerEdges);

	//Test the results
	yggdrasil_assert(binEdges.isAssociated());
	yggdrasil_assert((Int_t)binEdges.nBins() == nBins);

	//Return the BinEdges
	return binEdges;
}; //End: generate bins



YGGDRASIL_NS_END(yggdrasil)
