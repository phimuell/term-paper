/**
 * \brief	THis file conatins all the functions fdor the constant model that deas with the updateting.
 * 		 Note that thius functionality is acutally not used.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_constModel.hpp>

//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)


void
yggdrasil_constantModel_t::addNewSample(
	const Sample_t&		nSample,
	const HyperCube_t&	domain)
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert((Size_t)m_dims == m_volFactor.size());
	yggdrasil_assert(domain.isValid());

	/*
	 * Adding a sample does not have any effect
	 * on *this, so we simply increase the counter.
	 *
	 * Notice that this is guaranteed to be serial, so
	 * no race condtion will occure and all update take place
	 */

	//We have to check if *this is empty
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not add samples to an empty contant model. An initial fit has to be performed at least.");
	};
	if(nSample.nDims() != (Int_t)m_volFactor.size())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The domension of the sample is " + std::to_string(nSample.nDims()) + ", but the dimension of the function is just " + std::to_string(m_volFactor.size()));
	};
	if(nSample.isInsideUnit() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The smaple " + nSample.print() + ", is not inside the unit cube.");
	};
	if(isValidFloat(m_transFactor) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The transformation factor is not valid.");
	};
	if(m_transFactor <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The transformation factor is zero or negative.");
	};


	m_nAddedSamples += 1; 	//Update the count of the added sample since the last update

	return;
}; // End: Add new sample


void
yggdrasil_constantModel_t::preCommitHookInit()
{
	throw YGGDRASIL_EXCEPT_illMethod("Called a not implemented function.");
};


bool
yggdrasil_constantModel_t::commitChanges(
	const Int_t 	d,
	NodeFacade_t 	subTreeRootNode)
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(d < m_dims);
	yggdrasil_assert((Size_t)m_dims == m_volFactor.size());

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use update an empty model. Before an update can be performed an initial fit must be done. From dimesions " + std::to_string(d));
	};
	if(d == Constants::YG_INVALID_DIMENSION)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is invalid.");
	};
	if(d < 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is negative.");
	};
	if(d >= Constants::YG_MAX_DIMENSIONS)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is larger than the maximum.");
	};
	if(d >= (Int_t)m_volFactor.size())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Dimension d = " + std::to_string(d) + " exceeds the model dimension of " + std::to_string(m_volFactor.size()));
	};
	if(this->isConsistent() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to commit the changes of an consistent model. This is considered an error. From dimension " + std::to_string(d));
	};
	if(subTreeRootNode.isDirty() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node that was passed to the commit fucntion is not dirty.");
	};
	if(isValidFloat(m_transFactor) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The transformation factor is not valid.");
	};
	if(m_transFactor <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The transformation factor is zero or negative.");
	};

	/*
	 * As we have stated it, only the last dimesion does the updating
	 */

	if(d != m_dims)
	{
		//Not the last dimension is updated, but another one
		//so we simply return
		return true;
	}; //End if: not the last thread

	/*
	 * Only the last dimension will execute the code below.
	 */

	//We first make somne tests
	yggdrasil_assert(m_nAddedSamples == (subTreeRootNode.getDirtyMass() - subTreeRootNode.getMass()));

	/*
	 * Perform the updateing.
	 *
	 * Only the count of the added sample has to be added to
	 * the count of the allready added samples.
	 *
	 * Note: In the case there were not any samples in the domain.
	 * Now they are. thsi will enable the code to
	 * use the parameter of the model.
	 */
	m_nSamples += m_nAddedSamples;
	m_nAddedSamples = 0;		//Reset the counter

	if(this->isConsistent() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The updateing of the constant model was not successful.");
		return false;	//It is useless, but to indicate that was unsuccessfull
	};

	//Return true, to indicate that all is correct
	return true;
}; //End commit changes




YGGDRASIL_NS_END(yggdrasil)
