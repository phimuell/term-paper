/**
 * \brief	This file contains the code for generating random samples on the cnstant model.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_constModel.hpp>

//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)


void
yggdrasil_constantModel_t::drawSampleOnThis(
	Sample_t* const 		s,
	const MultiCondition_t& 	reNodeDSCondition,
	const HyperCube_t& 		domain)
{
	/*
	 * We only have to make a foew checks, since s is allready
	 * a valid sample.
	 */
	if(s == nullptr)
	{
		throw YGGDRASIL_EXCEPT_NULL("The sample that was passed to the drawing function is the null pointer.");
	};
	if(s->nDims() != m_dims)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the sample and the model did not agree.");
	};
	yggdrasil_assert(s->isValid());
	yggdrasil_assert(s->isInsideUnit());

	/*
	 * We just have to override the the conditions that are given.
	 */
	if(reNodeDSCondition.noConditions() == false)
	{
		//We just have to iterate over all the components that are
		//prescribed and set them approptiate
		for(const auto& sCondi : reNodeDSCondition)
		{
			//Load the data
			const Size_t idx = sCondi.getConditionDim();
			const auto   val = sCondi.getConditionValue();
			yggdrasil_assert(0.0 <= val);
			yggdrasil_assert(val <= 1.0);

			//Setting the value
			(*s)[idx] = val;
		}; //End for(sCondi): appling the conditions
	}; //End if: there are conditions

	return;

	(void)domain;
}; //ENd: drawing sample







YGGDRASIL_NS_END(yggdrasil)
