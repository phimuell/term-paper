/**
 *
 * \brief	This file conatines the code for fitting the model.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_linModel.hpp>

//INcluide std
#include <memory>
#include <cmath>
#include <vector>

YGGDRASIL_NS_BEGIN(yggdrasil)

void
yggdrasil_linearModel_t::preFittingHookInit(
	const NodeFacade_t 	nodeToAssociate)
{
	yggdrasil_assert(m_dimParam.empty());

	//Thest if *This is associated
	if(this->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model was allready associated.");
	};
	if(nodeToAssociate.isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node to associate is not a leaf.");
	};
	if(nodeToAssociate.isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node to associate is not okay.");
	};
	if(nodeToAssociate.isDirty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node to associate to is dirty.");
	};

	//Get the dimensions of the sample space
	const Int_t nDims = nodeToAssociate.getCSampleCollection().nDims();
	if(nDims <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The numbers of dimensions is too small.");
	};

	//Test if we must set the number of dimensions of this or not
	if(m_dims == 0)
	{
		//The numbers of dimensions is zero, so we have to load it
		m_dims = nDims;
	}
	else
	{
		//The number of dimension is not zero, so we must check if they are the same
		if(m_dims != nDims)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The number of set dimensions, " + std::to_string(m_dims) + " and the number of dimensions of the sampel collection, " + std::to_string(nDims) + " differs.");
		};
		if(m_dims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimension is larger than the maximum.");
		};
	}; //End else: the number of dimensions is set

	yggdrasil_assert(m_dims > 0);

	//Create the model place
	//All will be default constructed, which is exactly what we want.
	m_dimParam.resize(m_dims);

	//Make this associate
	m_isAssocsiated = true;

	yggdrasil_assert(this->isAssociated() == true);
	yggdrasil_assert(this->allDimensionsFitted() == false);	//This test requier associativity

	/*
	 * Returning
	 */
	return;
}; //ENd: preFittingHookInit()



bool
yggdrasil_linearModel_t::fitModelInDimension(
	NodeFacade_t 	leaf,
	const Int_t 	d)
{
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(d < m_dims);
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert((Size_t)m_dims == m_dimParam.size());

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model is not yetr associated.");
	};
	if(this->isDimensionFitted(d) == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Dimension " + std::to_string(d) + " was tried to fit twice.");
	};
	if(leaf.isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The provided leaf is not a leaf.");
	};
	if(leaf.checkIntegrity() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The leafe to fit on is not valid.");
	};
	if(leaf.isDirty() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The leaf to fit is dirty.");
	};

	//Now we perform the fitting
	SingleDimState_t state;

	//Get the dimensional array
	const DimensionArray_t& dimArray = leaf->getCSampleCollection().getCDim(d);

	//This is the number of samples that will be added of *this
	const Size_t N = dimArray.nSamples();

	//This is the domain of the node.
	//Notice that this is in the rescalled root data space
	const IntervalBound_t domain = leaf->getDomain().at(d);
	yggdrasil_assert(domain.isValid());

#ifndef NDEBUG
	/*
	 * This interval is used only for the assert test.
	 * it is needed sionce the imput data is in rescalled data space.
	 * This means it is inside theinterval [0, 1[.
	 */
	const IntervalBound_t unitInterval(IntervalBound_t::CREAT_UNIT_INTERVAL());
#endif

	//This is the counter we use
	Size_t c = 0;

	//Now starting to add the samples
	for(Size_t i = 0; i != N; ++i)
	{
		//Load the sample
		const Numeric_t x = dimArray[i];

		//Test if the samples is inside the domain, but only by an assert
		yggdrasil_assert(unitInterval.isInside(x) || unitInterval.isCloseToUpperBound(x));

		//Inclrease the counter and update the state
		c += 1;
		state.addNewSample(x, c);
	}; //End for(i)

	//Now perform the initial fitting
	state.updateTheParameters(domain, N);

	//Writing back the state to the vector
	m_dimParam[d] = state;

	//Test if the model was fitted successfully
	if(this->isDimensionFitted(d) == false)
	{
		//The model is not fitted so we must raise an exception
		throw YGGDRASIL_EXCEPT_RUNTIME("Could not fit dimension " + std::to_string(d));

		//It is not usefull but we return false, since we specified that.
		return false;
	}; //End if: error while fitting.

	//Test if we have valid parameters
	if(m_dimParam[d].hasValidParameters() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The fitting of dimension " + std::to_string(d) + " failed, because the paramaters were not valid.");

		//It is not usefull but we return false, since we specified that.
		return false;
	}; //End if: has valid paramater


	//The last thread has to write the counters
	if(d == Int_t(m_dimParam.size() - 1))
	{
		m_nSampleBase   = N;
		m_nAddedSamples = N;	//Different than in the constant model
	}; //End if: last thread

	//We return true to indicate that the model
	//was successfully fitted, but we are paranouid
	return m_dimParam[d].hasValidParameters();
}; //end: initial fitting


void
yggdrasil_linearModel_t::endOfFittingProcess()
{
	//Check
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The end of fitting process was called, but *this was not associated.");
	};
	if(this->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The end of fitting process was called, but not all dimensions of this were fitted");
	};

	/*
	 * We have to do nothing.
	 */


	/*
	 * Make this consistent
	 */


	if(this->isConsistent() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model was not consistent in the end.");
	};


	/*
	 * Return
	 */
	return;
}; //End: endOfFittingProcess




YGGDRASIL_NS_END(yggdrasil)
