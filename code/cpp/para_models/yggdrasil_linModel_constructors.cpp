/**
 * \brief	This file implements all the constructing of the linear model.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_linModel.hpp>

//INcluide std
#include <memory>
#include <cmath>
#include <vector>

YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_linearModel_t::ParametricModel_ptr
yggdrasil_linearModel_t::clone()
 const
{
	return ParametricModel_ptr(new yggdrasil_linearModel_t(*this));
};



yggdrasil_linearModel_t::yggdrasil_linearModel_t(
	const Int_t 	d) :
  yggdrasil_parametricModel_i(),
  m_dimParam(),
  m_nSampleBase(0),
  m_nAddedSamples(0),
  m_isAssocsiated(false),
  m_dims(d)
{
	if(d == Constants::YG_INVALID_DIMENSION)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is invalid.");
	};
	if(d < 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is negative.");
	};
	if(d >= Constants::YG_MAX_DIMENSIONS)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is larger than the maximum.");
	};

	//In earlier versions the allocation of space happened here
	//but this was moved to the pre fucntion
};


yggdrasil_linearModel_t::yggdrasil_linearModel_t(
	const std::string& 	modDescr)
 :
  yggdrasil_linearModel_t((modDescr.empty() == true ? 0 : ::std::stoi(modDescr)))
{
}; //End universal constructor.



yggdrasil_linearModel_t::ParametricModel_ptr
yggdrasil_linearModel_t::creatOffspring()
 const
{
	return ParametricModel_ptr(new yggdrasil_linearModel_t(m_dims));
};


yggdrasil_linearModel_t::~yggdrasil_linearModel_t()
 noexcept = default;


yggdrasil_linearModel_t::yggdrasil_linearModel_t(
	const yggdrasil_linearModel_t&)
 = default;


yggdrasil_linearModel_t::yggdrasil_linearModel_t(
	yggdrasil_linearModel_t&&)
 noexcept = default;


yggdrasil_linearModel_t&
yggdrasil_linearModel_t::operator= (
	const yggdrasil_linearModel_t&)
 = default;

yggdrasil_linearModel_t&
yggdrasil_linearModel_t::operator= (
	yggdrasil_linearModel_t&&)
 noexcept = default;








YGGDRASIL_NS_END(yggdrasil)
