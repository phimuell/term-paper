#pragma once
/**
 * \brief	This file implements a helper struct that is used
 * 		 to implement the model in a single dimension.
 *
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_hyperCube.hpp>

//#include <para_models/yggdrasil_linModel.hpp>


//Incluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)

YGGDRASIL_NS_BEGIN(internal)


/**
 * \class	ygInternal_linModelDimPack_t
 * \brief	This is a class that implements the functionality in one dimension.
 *
 * This class implements online algorithm of the data and also the estimation of the paramter.
 * It is designed to work in conjunction with the linear model obect.
 *
 * It is not so space efficient as it could be.
 * The reason is that exploiting this efficiency would increase the complexity
 * and also the need to provide some form of sychronisation.
 *
 * This is also the second version where we have switched to the new form of the equation
 * This means the PDF now looks like equation (4) in the DET paper.
 * But all the domain specific stuff was removed since it is all one.
 */
class ygInternal_linModelDimPack_t
{
	/*
	 * ==================
	 * Typedefs
	 */
public:
	using IntervalBound_t 	= ::yggdrasil::yggdrasil_intervalBound_t;		//!< This is the type that reprensents an interval
	using rNumeric_t   	= ::yggdrasil::Real_t;				//!< This is a higher resolution type

	/*
	 * ===================
	 * Accesser function
	 */
public:
	/**
	 * \brief	This function returns the estimate of theta.
	 *
	 * Notice that this is the estimate of the slope since the last update.
	 * This function can also return NAN.
	 *
	 * This is the theta form the paper.
	 */
	inline
	rNumeric_t
	getTheta()
	 const
	 noexcept
	{
		yggdrasil_assert(::std::isnan(m_model_theta) ? true : ((-2.0 <= m_model_theta) && (m_model_theta <= 2.0)));
		return m_model_theta;
	};


	/**
	 * \brief	This number is the vol factor.
	 *
	 * The volume factor is the scalling factor that has to be applied to the pdf
	 * to transform it to the rescalled data space of the current element,
	 * back to the rescalled root data space.
	 *
	 * It is important that the transformation does not leed to the
	 * original sample data space.
	 *
	 * This function may return NAN.
	 *
	 * Technicaly this funciton is allowed to return zero.
	 * This is the case if the dimension is relatively big.
	 * however this is not verly likely and it should be considered an error.
	 */
	inline
	rNumeric_t
	getVolFactor()
	 const
	 noexcept
	{
		yggdrasil_assert(::std::isnan(m_model_volFactor) ? true : (m_model_volFactor > 0.0));
		return m_model_volFactor;
	};




	/**
	 * \brief 	This function tests if all parameters have valid values.
	 */
	inline
	bool
	hasValidParameters()
	 const
	 noexcept
	{
		//Actually we could test more here, but we rely on the fact that the theta and vol are
		//updated together so if one is valid the other is too.
		yggdrasil_assert(isValidFloat(m_model_theta) == isValidFloat(m_model_volFactor));
		yggdrasil_assert(::std::isnan(m_model_theta) ? true : ((-2.0 <= m_model_theta) && (m_model_theta <= 2.0)));
		yggdrasil_assert(::std::isnan(m_model_volFactor) ? true : m_model_volFactor >= 0.0);

		return (isValidFloat(m_model_theta) && isValidFloat(m_model_volFactor) && (m_model_volFactor >= 0.0));
	};


	/**
	 * \brief	This function performs the update of \c *this.
	 *
	 * It will update the slope, intersection and the integration constant.
	 * It excpects the number of added samples as an argument as well as the
	 * interval bound.
	 *
	 * \return 	True if the model was able to be fitted.
	 *
	 * \param  domain	This is the interval the model should be fitted on.
	 * \param  nCount 	The number of samples that where added to \c *this in total.
	 */
	inline
	bool
	updateTheParameters(
		const IntervalBound_t&	domain,
		const Size_t 		nCount)
	{
		yggdrasil_assert(domain.isValid());

		//Test if the vol factor is set allready, if not set it
		if(::std::isnan(m_model_volFactor) == true)
		{
			//The vol factor is not set so we have to do it
			const rNumeric_t thisLength = domain.getLength();
			yggdrasil_assert(isValidFloat(thisLength));

			//The volFactor is the inverse of the length
			m_model_volFactor = 1.0 / thisLength;
			yggdrasil_assert(isValidFloat(m_model_volFactor));
			yggdrasil_assert(m_model_volFactor >= 0.0);
		}; //End if: Set vol Factor


		if(nCount == 0)
		{
			/*
			 * No Samples
			 */
			// In this case everything is zero
			m_model_theta = 0.0;

		} //End if: there are zero samples
		else if(nCount == 1)	//One could also put it above, but I do not like the idea
		{
			/*
			 * In this case the variance can not be determined.
			 * So we set theta to zero.
			 * This will lead degenerate *this to a constant model.
			 */
			m_model_theta = 0.0;
		}
		else
		{
			//This is the theta variable, is here for verbosity
			rNumeric_t Theta_ik = NAN;

			//First we compute the values we need
			const rNumeric_t Mean = m_wa_mean;
			const rNumeric_t Var  = m_wa_M2 / (nCount - 1);	//I asked Daniel (16-06-2019)
			yggdrasil_assert(isValidFloat(Var));
			yggdrasil_assert(isValidFloat(Mean));

			//THere is a special case that is handled in the R-Code.
			//See testing.R:58.
			//This can lead in the case of VAR == 0 to a division by zero
			//The R-Code does this very strictly.
			//I am not sure if one also had to test if the variance is zero?
			if(std::abs(Mean - 0.5) >= 100 * Constants::EPSILON)
			{
				//THere is no risk for a division by zero

				//We now calculate the s value form the paper
				const rNumeric_t S_i = 6.0 * ( 2 * Mean - 1 );

				const rNumeric_t N_k = nCount;			//This is only done for verbosity

				const rNumeric_t S_i2 = S_i * S_i;		//This is the square

				//COmpute the unrestricted value of theta
				const Numeric_t unrestrictedTheta = (N_k * S_i2 * S_i) / (N_k * S_i2 + 144.0 * Var);
				yggdrasil_assert(isValidFloat(unrestrictedTheta));

				//this is the value of Theta
				//In order to ensure non negativity requierements of a PDF we must clamp
				//this value between [-2, 2]
				Theta_ik = ::std::max<Numeric_t>(::std::min<Numeric_t>(unrestrictedTheta, 2.0), -2.0);
				yggdrasil_assert(isValidFloat(Theta_ik));
			}
			else
			{
				//Special case,w e assign zero to the theta value
				//This comes from the R code
				Theta_ik = 0.0;
			};

			//Save the theta value
			m_model_theta = Theta_ik;
			yggdrasil_assert(isValidFloat(m_model_theta));
		}; //End else: More than one

		if(isValidFloat(m_model_theta) == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The estimation of theta failed.");
		};
		if(isValidFloat(m_model_volFactor) == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The estimation of the volume factor failed.");
		};

		return true;
	}; //End: update Parameters


	/**
	 * \brief	This adds the data point \c xNewdata to \c *this
	 *
	 * \param  xNewData			This is the new data that will be added to this.
	 * \param  nSamplesIncludingNew		The number of samples \c *this has seen; including the new one.
	 */
	inline
	void
	addNewSample(
		const Numeric_t 	xNewData,
		const Size_t 		nSamplesIncludingNew)
	 noexcept
	{
		//This implementation based on:
		//  https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm
		const rNumeric_t delta = xNewData - m_wa_mean;

		m_wa_mean += delta / nSamplesIncludingNew;

		const rNumeric_t delta2 = xNewData - m_wa_mean;

		m_wa_M2 += delta * delta2;

		return;
	};


	/**
	 * \brief	Returns true if \c *this was initially fitted.
	 *
	 * \c *this is fitted if the slope is not NAN.
	 */
	inline
	bool
	isInitialFitted()
	 const
	 noexcept
	{
		//We relay on the fact that the two are synced
		yggdrasil_assert(isValidFloat(m_model_theta) == isValidFloat(m_model_volFactor));
		yggdrasil_assert(::std::isnan(m_model_theta) ? true : ((-2.0 <= m_model_theta) && (m_model_theta <= 2.0)));
		yggdrasil_assert(::std::isnan(m_model_volFactor) ? true : m_model_volFactor >= 0.0);

		return (::std::isnan(m_model_theta) == false) ? true : false;
	};



	/*
	 * ====================
	 * Constructor
	 */
public:
	/**
	 * \brief	Default constructor
	 *
	 */
	inline
	ygInternal_linModelDimPack_t()
	 noexcept
	:
	  m_wa_mean(0.0),
	  m_wa_M2(0.0),
	  m_model_theta(NAN),
	  m_model_volFactor(NAN)
	{};


	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	inline
	ygInternal_linModelDimPack_t(
		const ygInternal_linModelDimPack_t&)
	 noexcept
	 = default;


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	inline
	ygInternal_linModelDimPack_t(
		ygInternal_linModelDimPack_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	inline
	ygInternal_linModelDimPack_t&
	operator= (
		const ygInternal_linModelDimPack_t&)
	 noexcept
	 = default;

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	inline
	ygInternal_linModelDimPack_t&
	operator= (
		ygInternal_linModelDimPack_t&&)
	 noexcept
	 = default;

	/**
	 * \brief	The destructor.
	 *
	 * It is defaulted.
	 */
	inline
	~ygInternal_linModelDimPack_t()
	  noexcept
	  = default;


	/*
	 * ===================
	 * Private member
	 */
private:
	rNumeric_t 	m_wa_mean;	//!< This is the mean from the online algorithm
	rNumeric_t 	m_wa_M2;	//!< This is the M2 from the online algorithm
	//Size_t 		m_wa_count;	//!< This is the count of the online algorithm.
	//The count needed is stored inside the linear model itself.

	rNumeric_t 	m_model_theta;	//!< This is the slope of the model
	rNumeric_t 	m_model_volFactor;	//!< This is the volume factor, it is defined as 1/length of the assigned dimension.
					//!<  Relative to the rescalled root data space
}; //End class: ygInternal_linModelDimPack_t




YGGDRASIL_NS_END(internal)

YGGDRASIL_NS_END(yggdrasil)


