/**
 * \brief	This file contains the ccode that delas with the fitting of the constant model.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_constModel.hpp>

//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)


void
yggdrasil_constantModel_t::preFittingHookInit(
	const NodeFacade_t 	nodeToAssociate)
{
	yggdrasil_assert(m_volFactor.empty());

	//We test if the node is okay and a leaf
	if(this->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model is allready associated.");
	};
	if(nodeToAssociate.isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node to associate is not a leaf.");
	};
	if(nodeToAssociate.isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node to associate is not okay.");
	};
	if(nodeToAssociate.isDirty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node to associate to is dirty.");
	};
	if(isValidFloat(m_transFactor) == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The transformation factor was already set.");
	};


	//Get the dimensions of the sample space
	const Int_t nDims = nodeToAssociate.getCSampleCollection().nDims();

	//Test if we must set the number of dimensions of this or not
	if(m_dims == 0)
	{
		//The numbers of dimensions is zero, so we have to load it
		m_dims = nDims;
	}
	else
	{
		//The number of dimension is not zero, so we must check if they are the same
		if(m_dims != nDims)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The number of set dimensions, " + std::to_string(m_dims) + " and the number of dimensions of the sampel collection, " + std::to_string(nDims) + " differs.");
		};
		if(m_dims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimension is larger than the maximum.");
		};
	};



	/*
	 * Make this associated
	 */
	yggdrasil_assert(this->isAssociated() == false);
	m_isAssocsiated = true;
	//The flag is set, but *this is currently inconsistent


	/*
	 * Since we allways operate on the interval [0, 1[,
	 * the PDF is for all arguments _and_ dimensions of the domain,
	 * one, since we are always in teh unit hyper cube.
	 *
	 * This means that the only thing we need for mapping this value
	 * to the global data space is to divide by the size of that dimension.
	 *
	 * It is a bit more complicated since there is also teh correction due to
	 * the number of samples and so.
	 *
	 * We set the value to NAN to indicate that it is not fitted yet.
	 */
	m_volFactor.resize(nDims, NAN);

	//Make a final test
	yggdrasil_assert(this->isAssociated() == true);
	yggdrasil_assert(this->allDimensionsFitted() == false);

	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(m_dims == nDims);
	yggdrasil_assert((Size_t)m_dims == m_volFactor.size());


	/*
	 * We are done
	 */
	return;
}; //End preFittingHookInit



void
yggdrasil_constantModel_t::endOfFittingProcess()
{
	//Tests if the conditions are meet
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The end of pfitting fucntin was called, but the model is not associated.");
	};
	if(this->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The end of fitting function was called, but not all dimensions are fitted.");
	};
	if(std::isnan(m_transFactor) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The transformation factor was allready set.");
	};
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert((Size_t)m_dims == m_volFactor.size());

	/*
	 * We have to calculate the transformation factor
	 *
	 * This could be done more stable, by recursively
	 * computing the factor.
	 */
	Real_t iVolume = 1;	//This is the inverse of the volume
	for(const auto& volSingleDim : m_volFactor)
	{
		if(isValidFloat(volSingleDim) == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Detected an invalid volume factor.");
		};
		if(volSingleDim <= 0.0)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Detected a negative or zero volume factor.");
		};

		//Incooperated this dimension
		iVolume *= volSingleDim;

		if(isValidFloat(iVolume) == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The calculation of the trans factor resulted in an invalid float.");
		};
		if(iVolume <= 0.0)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The calculation of the transformation factor, resuted in a negative or invalid float.");
		};
	}; //End for(volSingleDim)
	yggdrasil_assert(::std::isnan(m_transFactor) == true);
	yggdrasil_assert(isValidFloat(iVolume));

	//Now assine it
	m_transFactor = iVolume;

	return;
}; //ENd: endOfFittingProcess



bool
yggdrasil_constantModel_t::fitModelInDimension(
	NodeFacade_t 	leaf,
	const Int_t 	d)
{
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d <= Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(leaf.checkIntegrity());
	yggdrasil_assert(leaf.isDirty() == false);
	yggdrasil_assert(leaf.getDomain().nDims() == m_dims);
	yggdrasil_assert((Size_t)m_dims == m_volFactor.size());

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not fit a model, that is not associated.");
	};
	if(this->isDimensionFitted(d) == true)	//This technically implies is associateioon, but I think it is nicer that way.
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to fit the dimension " + std::to_string(d) + " twiche.");
	};
	if(d >= m_dims)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Dimension d = " + std::to_string(d) + " exceeds the model dimension of " + std::to_string(m_volFactor.size()));
	};
	if(leaf.isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The provided leaf is not a leaf.");
	};
	if(isValidFloat(m_transFactor) == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The transformation fgactor is already valid.");
	};

	//Now we compute the constant that we need
	const IntervalBound_t thisInterval = leaf.getDomain()[d];
	yggdrasil_assert(thisInterval.isValid());

	//Now we get the length of the domain
	const Numeric_t lengthOfInterval = thisInterval.getLength();

	if(thisInterval.hasFiniteLength() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Dimension " + std::to_string(d) + " has a non finite length of " + std::to_string(lengthOfInterval));
	};


	//The inverse of the lenght is the correction factor we have to apply to get to the
	// RESCALLED ROOT DATA SPACE
	yggdrasil_assert(::std::isnan(m_volFactor.at(d)) == true);
	m_volFactor.at(d) = Real_t(1.0) / lengthOfInterval;

	if(isValidFloat(m_volFactor[d]) == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The fitting of dimension " + std::to_string(d) + " with value " + std::to_string(m_volFactor[d]) + " was not valid.");
	};

	//Test if teh dimension was correctly fitted
	if(this->isDimensionFitted(d) == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension " + std::to_string(d) + " could not be fitted. Model value was " + std::to_string(m_volFactor[d]) + ".");
		return false;
	};


	/*
	 * Writing the sample count.
	 * As for the updating, this is only done by the last dimension
	 */
	if(d == (m_dims - 1))
	{
		m_nSamples = leaf->getMass();  //We requiere that the leaf is clean, so this is correct
		m_nAddedSamples = 0;
	};

	//We return true to indicate that all went well
	return true;
}; //End: initial fit





YGGDRASIL_NS_END(yggdrasil)
