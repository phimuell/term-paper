/**
 * \brief	This file implements the update functionality of the linear model.
 *
 * Note that this functionality is not supüported.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_linModel.hpp>

//INcluide std
#include <memory>
#include <cmath>
#include <vector>

YGGDRASIL_NS_BEGIN(yggdrasil)



void
yggdrasil_linearModel_t::addNewSample(
	const Sample_t&		nSample,
	const HyperCube_t&	domain)
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Before a smaple can be added an initial fit has to be performed.");
	};
	if(this->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Not all dimensions are fitted.");
	};

	//make some tests to ensure compability
	if(nSample.size() != m_dimParam.size())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The sample has a dimension of " + std::to_string(nSample.size()) + ", but the model has a dimsnion of " + std::to_string(m_dimParam.size()));
	};

	yggdrasil_assert(domain.isValid());
	yggdrasil_assert(nSample.isInsideUnit() == true);

	/*
	 * By assumtion thsi function will not be called concurently
	 */

	//Wirst we have to update the sample count, but not the productive one
	m_nAddedSamples += 1;

	//Update the dimension
	//This will not change the slope
	const Size_t N = m_dimParam.size();
	for(Size_t d = 0; d != N; ++d)
	{
		m_dimParam[d].addNewSample(nSample[d], m_nAddedSamples);
	}; //End for(d): updating

	return;
}; //End add new sample



void
yggdrasil_linearModel_t::preCommitHookInit()
{
	throw YGGDRASIL_EXCEPT_illMethod("Called a not implemented function.");
};


bool
yggdrasil_linearModel_t::commitChanges(
		const Int_t 	d,
		NodeFacade_t 	subTreeRootNode)
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not update an empty model.");
	};
	if(d == Constants::YG_INVALID_DIMENSION)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is invalid.");
	};
	if(d < 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is negative.");
	};
	if(d >= Constants::YG_MAX_DIMENSIONS)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is larger than the maximum.");
	};
	if(d >= (Int_t)m_dimParam.size())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Dimension d = " + std::to_string(d) + " exceeds the model dimension of " + std::to_string(m_dimParam.size()));
	};
	if(this->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Not all dimensions are fitted.");
	};
	if(subTreeRootNode.isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node that was given is not okay.");
	};
	if(subTreeRootNode.isDirty() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node is not dirty");
	};

	//At this point all calls must be inconsistent
	if(this->isConsistent() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("This is consistent.");
	};


	//Now we update the dimension pack
	const IntervalBound_t thisINterval = subTreeRootNode.getDomain().at(d);

	//Update the samples
	const bool suc = m_dimParam.at(d).updateTheParameters(thisINterval, m_nAddedSamples);

	if(suc == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The updateing of the parameter failed.");

		return false;
	};

	//Now only doing the stuff the last thread does
	if(d == Int_t(m_dimParam.size() - 1))
	{
		m_nSampleBase = m_nAddedSamples;
	}; //End if: only doing the last job

	return true;
}; //End commit changes







YGGDRASIL_NS_END(yggdrasil)


