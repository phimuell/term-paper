# CMake file for the core folder

#we will add the cpp files to the source and the rest as bublic
target_sources(
	Yggdrasil
  PRIVATE
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_constModel.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_constModel_eval.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_constModel_constructors.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_constModel_fitting.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_constModel_testing.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_constModel_update.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_constModel_sampling.cpp"

	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_linModel.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_linModel_eval.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_linModel_constructors.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_linModel_fitting.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_linModel_testing.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_linModel_update.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_linModel_sampling.cpp"
  PUBLIC
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_constModel.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_linModel.hpp"
  )


