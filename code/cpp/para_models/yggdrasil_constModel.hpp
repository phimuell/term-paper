#pragma once
/**
 * \brief	This file implements the functionality of the constant parameteric model.
 *
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_generalBinEdges.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>


#include <tree/yggdrasil_nodeFacace.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>


//Incluide std
#include <memory>
#include <vector>
#include <atomic>
#include <string>



YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the node
class yggdrasil_DETNode_t;

/**
 * \class 	yggdrasil_constantModel_t
 * \brief	A constant parameteric model.
 *
 * This function impements the constant model.
 * Thi is equation (3) and the one below from the paper.
 *
 * As we have stated in the interface, we omitt the \f$ n(C_k) / n \f$ factor,
 * when only one dimension is invloved.
 * The main reason for that is that it is hard, lets say inconvenient, to propagate,
 * the information down.
 * So the descission was to exclude the factor, when only one dimensin is involved.
 * This also means that the user has to take care of that.
 *
 * It is also important that the input of this class is allways considered
 * to life in the interval [0, 1[, of the rescalled element space.
 * Its output however is mostly considered to be expresend on
 * the rescalled root data space.
 */
class yggdrasil_constantModel_t final : public yggdrasil_parametricModel_i
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using ValueArray_t 		= yggdrasil_valueArray_t<Numeric_t>; 	//!< This is the value array class.
	using Sample_t 			= yggdrasil_sample_t;			//!< This is the type of a single sample
	using SampleCollection_t	= yggdrasil_sampleCollection_t;		//!< This is the sample collection
	using DimensionArray_t 		= SampleCollection_t::DimensionArray_t;	//!< This is the dimeions array
	using IntervalBound_t 		= yggdrasil_intervalBound_t;		//!< This is the type that reprensents an interval
	using HyperCube_t 		= yggdrasil_hyperCube_t;		//!< This is the type that represents a domain.
	using ParametricModel_ptr	= std::unique_ptr<yggdrasil_parametricModel_i>;	//!< This is the managed pointer of the clone function
	//using cNode_ptr 		= const yggdrasil_DETNode_t*;			//!< This is the node type
	using NodeFacade_t 		= yggdrasil_nodeFacade_t;		//!< This is the class that models an interface to a node.
	using ModelVector_t 		= ::std::vector<Real_t>;		//!< This is the type that stores the model parameter
	using BinEdgeArray_t 		= yggdrasil_genBinEdges_t;		//!< This is a class for the bins.


	/*
	 * ===================================
	 * Constructor
	 *
	 * All constructrs are defaulted and protected
	 * The destructor is public and virtual,
	 * but an implementation is provided.
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * Since the model is not populated with data upon construction.
	 * It is sufficent to only provide a constructor that
	 * informs the model about the size.
	 * This constructor does not bind *this to any data.
	 *
	 * This constructor also accpets zero as number of dimensions.
	 * In this case there is no binding that happens.
	 *
	 * \param  d 	Dimension of the samples.
	 */
	yggdrasil_constantModel_t(
		const Int_t 	d);


	/**
	 * \brief	Universal constructor.
	 *
	 * This is the universal constructor mandated by the
	 * parameteric model interface.
	 * It expects to contain only one positive integer, that is
	 * interpreted as the number of dimensions.
	 * If the string is empty, the effect is the same as when
	 * constructing with zero as number of dimensions.
	 *
	 * \param  modDescr	The string that describes the model
	 *
	 * \throws	This constructo calls the normal bnuilding
	 * 		 constructor.
	 */
	yggdrasil_constantModel_t(
		std::string&	modDescr);


	/**
	 * \brief	Destructor
	 *
	 * It is defaulted.
	 * But it is virtual
	 */
	virtual
	~yggdrasil_constantModel_t()
	 noexcept;


	/**
	 * \brief	This function creates an empty copy of \c *this.
	 *
	 * See the super call for aq description of this function.
	 *
	 * \note 	From all spelling errors you encountered here, this one is intended.
	 */
	virtual
	ParametricModel_ptr
	creatOffspring()
	 const
	 override;


	/**
	 * \brief	This fucntion creates a copy of *this.
	 *
	 * See supper class for a description of this function
	 */
	virtual
	ParametricModel_ptr
	clone()
	 const
	 override;



	/*
	 * ====================
	 * ====================
	 * Interface functions
	 *
	 * Here are the functions that defines the interface
	 */

	/*
	 * ==================
	 * Fitting function
	 */
public:
	/**
	 * \brief	This is the precompute function for the fitting.
	 *
	 * See the super class for more information about this function.
	 *
	 * \throw 	If the node is not valid or this is allready associated.
	 */
	virtual
	void
	preFittingHookInit(
		const NodeFacade_t 	nodeToAssociate)
	 override;



	/**
	 * \brief	This function fits the parameteric model of \c *this.
	 *
	 * See the super class for a description of this function.
	 *
	 * \param  leaf 	The leaf used to build the model.
	 * \param  d 		The dimensionsion that should be builded.
	 *
	 * \throw 	If the model is unable to fit, or underling functions.
	 * 		 Implementations can permitted to strengthen the requierements.
	 * 		 Also if *thsi is allready associated.
	 */
	virtual
	bool
	fitModelInDimension(
		NodeFacade_t 	leaf,
		const Int_t 	d)
	 override;


	/**
	 * \brief	This function informs *thsi that the fitting has ended.
	 *
	 * See the super class for a descrption of this function.
	 *
	 * \throw	If not all dimensions are fitted or this is not associated at all
	 * 		 Also if internal consistency chekks fail
	 */
	virtual
	void
	endOfFittingProcess()
	 override;


	/*
	 * =======================
	 * State functions
	 *
	 * These are the state fucntions that may change during the
	 * computation as they can beppends if updaztes are running or not.
	 */
public:
	/**
	 * \brief	Thsi function returns true if dimension \c d of \c *this is fitted.
	 *
	 * See the super class for more information about this function.
	 *
	 * \param  d 	The dimension to check.
	 *
	 * \throw 	If d exceeds the dimensions.
	 *
	 * \note	Earlier versions of the interface also allowed a negativ
	 * 		 value of the dimension. This feature was removed and is now considered
	 * 		 an error.
	 */
	virtual
	bool
	isDimensionFitted(
		const Int_t 	d)
	 const
	 override;



	/**
	 * \brief	This function returns \c true if \c *this
	 * 		 is consistent.
	 *
	 * See the super class for more information about this function.
	 *
	 * \throw 	If \c *this is not associated to data, meaning no
	 * 		 fit was ever performed, then this function throws.
	 */
	virtual
	bool
	isConsistent()
	 const
	 override;



	/**
	 * \brief	THis function tests if all tests where performed.
	 *
	 * See the super class for more information about this function.
	 *
	 * \throw	If an underling fucntion throws
	 */
	virtual
	bool
	allDimensionsFitted()
	 const
	 override;




	/*
	 * ====================
	 * Information functions
	 *
	 * This functions give access to the static information of *this.
	 * This are informations that are not depends on the update state of *this.
	 * Like the association, once it is established, it will continue to
	 * exists.
	 */
public:
	/**
	 * \brief	This function returns true if \c *this is not associated to any data.
	 *
	 * See the super class for more information about this function.
	 *
	 * \note	This fucntion was previously called 'isEmpty()'.
	 * 		 The name wead unsuitable for that, alning was
	 * 		 inverse to the one it had now.
	 * 		 The 'isEmpty()' fucntion was removed.
	 */
	virtual
	bool
	isAssociated()
	 const
	 override;


	/**
	 * \brief	This function return a string that can be used to inform the user.
	 *
	 * The string to identify has the form
	 *      MODEL(Const) = [R]
	 *
	 * Without the leading tab.
	 * R is expanded to "{dim_id: value}".
	 */
	virtual
	std::string
	print(
		const std::string& 	intent)
	 const
	 override;


	/**
	 * \brief	This function returns the number of different dimensions.
	 *
	 * See the super class for more information about the function.
	 *
	 * \throw 	If the return value is zero.
	 */
	virtual
	Int_t
	nDifferentFittings()
	 const
	 override;


	/**
	 * \brief	This fucntion returns a vector that contains the
	 * 		 keys (dimension \c d) that are accepted by the
	 * 		 fitting fucntion as dimensional argument.
	 *
	 * See the super class for more information about this function.
	 *
	 * \throw 	This fucntion throws if the list is empty.
	 */
	virtual
	DimList_t
	getAllDimensionKeys()
	 const
	 override;


	/**
	 * \brief	This function returns the numbers of samples the estimate is based on.
	 *
	 * See the super class for more infoprmation about this class.
	 *
	 * \throw 	If \c *this was not yet fitted an exception should be raised.
	 */
	virtual
	Size_t
	nSampleBase()
	 const
	 noexcept(false)
	 override;



	/*
	 * ===================
	 * PDF Function
	 *
	 * These functions allows to query the density.
	 * Some of them operate on just one dimension, so they do not
	 * need the number of samples in the full tree.
	 * These function who need them will need them as an argument.
	 *
	 * So keep the discription of the function short the factor,
	 * \f$ n(C_k) / n \f$, form the paper, will henceforce only be
	 * called the scalling factor.
	 *
	 * There is the other factor that is introduced by the
	 * rescalling of the data, such that it allways stays inside
	 * the interval [0, 1[, function must allways expect that the data
	 * is inside that range, the data is then interpreted to lie
	 * inside the domain they where fitted on, relative to the
	 * rescalled root data space.
	 *
	 * The value that are returned are also relative to the full
	 * rescalled root data space.
	 */
public:
	/**
	 * \brief	This function evalutes a the model of dimension
	 * 		 \c d at location \c x, in that dimension.
	 *
	 * See the super class for more information about this function.
	 *
	 * \param  d 		This is the dimension we evaluate the model for
	 * \param  x 		This is the location of the model we evaluate the model for.
	 * \param  domain	This is the domain bound form the hypercube \c *this is defined on.
	 * 			 The value is relative to the rescalled root data space.
	 *
	 * \throw 	The implementation is free to generate exceptions.
	 */
	virtual
	Numeric_t
	evaluateModelInSingleDim(
		const Numeric_t 	x,
		const Int_t 		d,
		const IntervalBound_t&	domain)
	 const
	 override;


	/**
	 * \brief	This function is used to evaluate the model
	 * 		 in a single dimension for many values.
	 *
	 * See the super class for more information about this fucntion.
	 *
	 *
	 * \param  X		These are the sample positins where the model should be evaluated.
	 * \param  d		The dimension we are interested in.
	 * \param  domain	This is the domain \c *this was fitted on only the corresponding interval.
	 * 			 The value is relative to the rescalled root data space.
	 *
	 * \throw	It is unspecific if the implementation throws, hower some sanity tests are recomended.
	 */
	virtual
	ValueArray_t
	evaluateModelInSingleDim(
		const ValueArray_t&	xSamples,
		const Int_t 		d,
		const IntervalBound_t&	domain)
	 const
	 override;


	/**
	 * \brief	This function is able to evaluate the model at sample location \c x.
	 *
	 * See the super class for more information abozut this function.
	 *
	 * \param  x		This is the sample that we want to evaluate.
	 * \param  domain 	This is the hypercube, \c *this was defined on.
	 * 			 The value is relative to the rescalled root data space.
	 * \param  nTotSamples	The number of total samples in the enteier tree.
	 *
	 * \throw 	The conditions on which this function generates an exception
	 * 		 are not specified.
	 * 		 The implementation is encurrated to perform some simple checking.
	 */
	virtual
	Numeric_t
	evaluateModelForSample(
		const Sample_t&		s,
		const HyperCube_t&	domain,
		const Size_t 		nTotSamples)
	 const
	 override;



	/*
	 * =========================
	 * Test Functions
	 *
	 * Thsi function are the one that are used inside the test environemnt.
	 *
	 * It is important that these functions does not operate on the
	 * original data space, nor on the rescalled root data space.
	 * Insted they operate directly on exclusively on the rescalled domain
	 * this is defined on.
	 */
public:
	/**
	 * \brief	This function returns the expected counts inside the bins.
	 *
	 * See the super class for more information about this function.
	 *
	 * \throw 	Generally unspecific. However implementations should make some
	 * 		 compability tests. Also memory allocation could throw.
	 *
	 * \note	The value returned by this function must not be scalled by any factor.
	 * 		 This means this is not just the CDF value in each bin, but actually
	 * 		 the CDF value scalled by the number of the samples iside teh domain managed
	 * 		 by the node.
	 */
	virtual
	ValueArray_t
	getExpectedCountsInBins(
		const Int_t 		d,
		const BinEdgeArray_t& 	binEdge,
		const IntervalBound_t&	domain)
	 const
	 override;


	/**
	 * \brief	This function is used for the quantiles.
	 *
	 * See the super class for more information about this function.
	 *
	 * \param  d		The dimension we opewrate on.
	 * \param  nBins	The number of bins that should be created.
	 * \param  domain	The domain *This was fitted on.
	 * 			 The value is relative to the rescalled root data space.
	 *
	 * \throws	Generally unspeciffic, however implementations
	 * 		 should make some compability tests.
	 * 		 If *this is not consistent an exception should be raised.
	 */
	virtual
	BinEdgeArray_t
	generateBins(
		const Int_t 		d,
		const Int_t 		nBins,
		const IntervalBound_t&	domain)
	 const
	 override;


	/**
	 * \brief	This function returns the number of bounds DOF of this model.
	 *
	 * This fucntion returns 0.0, since there is no parameter that is bound.
	 */
	virtual
	Numeric_t
	getBoundDOF()
	 const
	 final;


	/*
	 * ===========================
	 * Sampling
	 */
public:
	/**
	 * \brief	This function draws a sample.
	 *
	 * This function expects a sample that is drawn uniformly from the unit cube.
	 * This function uses inversion method to do this.
	 * Since we are in the constant case, the values are not modified.
	 *
	 * If conditions needed to be applied the respective components are modified.
	 *
	 * Note that the generated sample will ly on the rescalled node data space.
	 *
	 * See also the interface function for a more detailed description.
	 *
	 * \param  s 			The unit sample that is used to generate the sample.
	 * \param  reNodeDSCondition	This is teh condition object, in rescalled node data
	 * 				 space, that restricts the sampling.
	 * \param  domain 		This is the domain where the owning leaf is defined on.
	 *
	 * \throw 	If compability conditions are violated.
	 */
	void
	drawSampleOnThis(
		Sample_t* const   		s,
		const MultiCondition_t& 	reNodeDSCondition,
		const HyperCube_t& 		domain)
	 override;



	/*
	 * =================================
	 * Update functions
	 *
	 * These fucntions deals with the updating of teh estimator.
	 */
public:

	/**
	 * \brief	This is a function that informs *this that the update process starts.
	 *
	 * See the super class for more information.
	 *
	 * \throw	If *this is not associated or not fitted at all.
	 * 		 Also if it is not consistent.
	 */
	virtual
	void
	preCommitHookInit()
	 override;


	/**
	 * \brief	This function adds the new sample \c nSample to \c *this.
	 *
	 * See the super class for more information about this function.
	 *
	 * \param  nSample	The new sample that should be added to \c *this.
	 * \param  domain 	This is the domain \c *this is reponsible for.
	 * 			 The value is relative to the rescalled root data space.
	 *
	 * \throw 	It is not defined when an exception is raised.
	 * 		 However it is recomended that some sanaty checs are performed.
	 *
	 * \note 	This function must not have an effect if the model can not be made updatable.
	 */
	virtual
	void
	addNewSample(
		const Sample_t&		nSample,
		const HyperCube_t&	domain)
	 override;



	/**
	 * \brief	Commit the changes of \c *this in dimension \c d .
	 *
	 * See the super class for more information about this function.
	 *
	 * \param  d 			The dimension we update.
	 * \param  subTreeRootNode	The node where the subtree is rooted.
	 *
	 * \return 	This function returns true if the fitting was successfull.
	 * 		 If false is returned yggdrasil will gnerate an exception.
	 *
	 * \throw 	It is not defined when an exception is raised.
	 * 		 However it is recomended that some sanaty checs are performed.
	 */
	virtual
	bool
	commitChanges(
		const Int_t 	d,
		NodeFacade_t 	subTreeRootNode)
	 override;






	/*
	 * =================================
	 * Protected constructors
	 */
protected:
	/**
	 * \brief	Default constructor
	 *
	 * Is Deleted.
	 */
	yggdrasil_constantModel_t()
	 noexcept
	 = delete;


	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_constantModel_t(
		const yggdrasil_constantModel_t&);


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_constantModel_t(
		yggdrasil_constantModel_t&&)
	 noexcept;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_constantModel_t&
	operator= (
		const yggdrasil_constantModel_t&);

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_constantModel_t&
	operator= (
		yggdrasil_constantModel_t&&)
	 noexcept;


	/*
	 * ===================
	 * Private memeber
	 */
private:
	ModelVector_t 		m_volFactor;	//!< This is the correction factor that has to be applied to the computed WK to get the rescalled data space.
	Size_t 			m_nSamples;	//!< This is the number of samples the estimate is based on
	Size_t 			m_nAddedSamples;//!< This is the number of added samples since the last update.
	bool 			m_isAssocsiated;//!< This variable indicates if *this is assocaiated or not.
	Int_t 			m_dims;		//!< This is the number of dimensions
	Real_t 			m_transFactor;	//!< This is the transformation factor.
						//!<  It is the inverse of the product of all vomume factors.
						//!<  It is set in the end of fitting process.
}; //End class: yggdrasil_constantModel_t


YGGDRASIL_NS_END(yggdrasil)


