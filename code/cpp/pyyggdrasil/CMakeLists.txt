#
# Thsi is teh cmake file for the wrapper code of Yggdrasil
#


# Create the module
# And add all the source files to the library
pybind11_add_module(pyYggdrasil MODULE
		main_bindings.cpp 			#This is the man part
		  pyYggdrasil_sampleContainers.cpp
		  pyYggdrasil_utilityWrapper.cpp
		  pyYggdrasil_treeBuilderWrapper.cpp
		  pyYggdrasil_treeWrapper.cpp
		  pyYggdrasil_treeHelperWrapper.cpp
		  pyYggdrasil_randomWrapper.cpp
		  pyYggdrasil_randomWrapper_distributions.cpp
		  pyYggdrasil_randomWrapper_util.cpp
		  pyYggdrasil_randomWrapper_distributions_impl.cpp
		  pyYggdrasil_randomWrapper_treeSampler.cpp
		  )

# Link yggdrasil against interface
#
# For some reason I do not understand (maybe I should read the manual)
# The command needs the PRIVATE keyword.
# See: https://github.com/pybind/pybind11/issues/387#issuecomment-244692002
target_link_libraries(pyYggdrasil PRIVATE Yggdrasil)



