/**
 * \brief	This fucntion implements the wrapping of the random fucntions
 */

//Include the yggdrasil code that is needed
#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>
#include <random/yggdrasil_multiGauss.hpp>
#include <random/yggdrasil_multiDirichle.hpp>
#include <random/yggdrasil_outlierPDF.hpp>
#include <random/yggdrasil_biModalUniform.hpp>
#include <random/yggdrasil_gammaPDF.hpp>
#include <random/yggdrasil_multiUniform.hpp>
#include <random/yggdrasil_betaPDF.hpp>

//Wrapper to the random number generator
#include <random/pyYggdrasil_randomNumberGenerator.hpp>


//Include Eigen
#include <Eigen/Core>

//Include the bindings
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>


//Make a namespace alias
namespace py = pybind11;



/**
 * \brief	This is a helper function it contains all the distribution code.
 *
 * This function generates the code for binding the distributions.
 */
extern
void
pyYggdrasil_randomWrapper_distri(
	py::module&	m);

/**
 * \brief	This is a helper fucntion that contaians all the helper code/utility.
 *
 * This is like the helper code that is related with.
 */
extern
void
pyYggdrasil_randomWrapper_util(
	py::module&	m);


/**
 * \brief	This is the function that writes the bindings for the tree sampler.
 */
extern
void
pyYggdrasil_randomWrapper_treeSampler(
	py::module& 	m);



void
pyYggdrasil_randomWrapper(
	py::module&	m)
{
	/*
	 * =======================
	 * USINGS
	 */
	using Int_t 		 = ::yggdrasil::Int_t;
	using Numeric_t 	 = ::yggdrasil::Numeric_t;
	using Size_t 		 = ::yggdrasil::Size_t;
	using pRNGenerator_t 	 = ::yggdrasil::pyYggdrasil_pRNGenerator_t;
	using result_type 	 = pRNGenerator_t::result_type;
	using Distribution_t 	 = ::yggdrasil::yggdrasil_randomDistribution_i;
	using Gauss_t 		 = ::yggdrasil::yggdrasil_multiDimGauss_t;
	using Dirichlet_t 	 = ::yggdrasil::yggdrasil_multiDimDirichlet_t;
	using Uniform_t 	 = ::yggdrasil::yggdrasil_multiDimUniform_t;
	using Outlier_t 	 = ::yggdrasil::yggdrasil_outlierPDF_t;
	using BiModalUniform_t 	 = ::yggdrasil::yggdrasil_biModalUniform_t;
	using Gamma_t 		 = ::yggdrasil::yggdrasil_gammaDistri_t;
	using Beta_t 		 = ::yggdrasil::yggdrasil_betaDistri_t;
	using HyperCube_t 	 = Distribution_t::HyperCube_t;
	using IntervalBound_t 	 = Distribution_t::IntervalBound_t;
	using PDFValueArray_t 	 = Distribution_t::PDFValueArray_t;

	/*
	 * ==================
	 * Utility
	 */
	pyYggdrasil_randomWrapper_util(m);


	/*
	 * ===================
	 * Distributions
	 */
	pyYggdrasil_randomWrapper_distri(m);


	/*
	 * ==================
	 * Tree Sampler
	 */
	pyYggdrasil_randomWrapper_treeSampler(m);






}; //End: wrapperint utility function








