/**
 * \brief	This file implements the main function for the bindings.
 *
 * As it was described in the FAQ of the pybind project, the bindings are
 * spread over several files, for reduceing the complexity.
 *
 * See also:
 * 	- "https://pybind11.readthedocs.io/en/master/faq.html#how-can-i-reduce-the-build-time"
 * 	- "http://yyc.solvcon.net/drafts.html"
 *
 */

//Include the bindings
#include <pybind11/pybind11.h>

//Make a namespace alias
namespace py = pybind11;


/*
 * \brief	This function implements the wrapping for the sample containers.
 *
 * THis funciton is implemented in the file "pyYggdrasil_sampleConatiners.cpp".
 * It contains wrapper for the sample array, sample collection as well as the sample.
 *
 * They all implement the universal sample interface.
 */
extern
void
pyYggdrasil_sampleContainersWrapper(
	py::module&	m);


/**
 * \brief	This function implements the wrapping of the utility code.
 *
 * The utility code are the interval the domaian and similar stuff.
 * Remember the C++ interface is larger.
 */
extern
void
pyYggdrasil_utilWrapper(
	py::module&	m);


/**
 * \brief	This class implements the builder class.
 *
 * This is the thing that allows to reduce the number of arguments in
 * the tree construction face.
 */
extern
void
pyYggdrasil_treeBuilderWrapper(
	py::module&	m);


/**
 * \brief	This fucntion generates the wrapper for the helper structs of the tree.
 *
 */
extern
void
pyYggdrasil_treeHelperWrapper(
	py::module&	m);


/**
 * \brief	This function implements the wrapping of the tree fucntion.
 *
 * This is basically the tree with its different function
 * But also some helper functions, like the phacade are supported.
 * I think I will need to make a better iterator.
 */
extern
void
pyYggdrasil_treeWrapper(
	py::module&	m);


/**
 * \brief	This function creates the binding code for the random part of yggdrasil.
 *
 * This functin delclares the main interface of the random fucntion and also
 * the individual random distributions.
 * It also allows the canonical distributions.
 */
extern
void
pyYggdrasil_randomWrapper(
	py::module&	m);








/**
 * \brief	We declare the module, that is the pythen package here.
 *
 * The following code will generate the module or package "pyYggdrasil".
 */
PYBIND11_MODULE(pyYggdrasil, m)
{
	/*
	 * ============
	 * Documentation of the module
	 */
	m.doc() =
		"The module pyYggdrasil is a wrapper that allows accessing the Yggdrasil library from within Python."
		" The wrapper was written with pybind11, a very good library."
		" It offers the main classes that are aviable in C++ to Python, but with an reduced interface."
	; //End doc codule

	/*
	 * =============
	 * Sample containers
	 */
	pyYggdrasil_sampleContainersWrapper(m);


	/*
	 * =============
	 * Utility code
	 */
	pyYggdrasil_utilWrapper(m);


	/*
	 * ==================
	 * Tree builder code
	 */
	pyYggdrasil_treeBuilderWrapper(m);


	/*
	 * ==================
	 * The actual tree code
	 */
	pyYggdrasil_treeWrapper(m);


	/*
	 * ==================
	 * Helper structes for the tree
	 */
	pyYggdrasil_treeHelperWrapper(m);


	/*
	 * ======================
	 * Random wrapper
	 */

	//We will create a new submodule
	py::module pyYggdrasil_random_submodule = m.def_submodule(
		"Random",	//Name of the submodule
		"This submodule contains the distribution generators that are offered by yggdrasil."
		" They operrate directly on the sample containers."
		" Thus offer a more direct way of creating samples, than using the numpy distributions."
	); //End random Submodule

	//Create the wrapping code
	pyYggdrasil_randomWrapper(pyYggdrasil_random_submodule);


	return;
}; //End module(pyYggdrasil)









