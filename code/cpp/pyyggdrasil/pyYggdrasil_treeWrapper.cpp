/**
 * \brief	This fucntion implements the wrapping of the tree functionality.
 *
 */

//Include the yggdrasil code that is needed
#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

//Include the bindings
#include <pybind11/pybind11.h>

//For the convertions of the return types that are standard vector
#include <pybind11/stl.h>
#include <pybind11/eigen.h>

//Make a namespace alias
namespace py = pybind11;


void
pyYggdrasil_treeWrapper(
	py::module&	m)
{
	/*
	 * =======================
	 * USINGS
	 */
	using Int_t 		 = ::yggdrasil::Int_t;
	using Numeric_t 	 = ::yggdrasil::Numeric_t;
	using Real_t 		 = ::yggdrasil::Real_t;
	using Size_t 		 = ::yggdrasil::Size_t;
	using IntervalBound_t 	 = ::yggdrasil::yggdrasil_intervalBound_t;
	using HyperCube_t 	 = ::yggdrasil::yggdrasil_hyperCube_t;
	using Sample_t 		 = HyperCube_t::Sample_t;
	using DepthMap_t 	 = ::yggdrasil::yggdrasil_treeDepthMap_t;
	using Tree_t 		 = ::yggdrasil::yggdrasil_DETree_t;
	using Tree_ptr 		 = ::std::unique_ptr<Tree_t>;
	using ValueArray_t 	 = Tree_t::ValueArray_t;
	using PDFValueArray_t 	 = Tree_t::PDFValueArray_t;
	using PDFValueArrayEigen_t = Tree_t::PDFValueArrayEigen_t;	//This is an alias for an Eigen type that can be converted into a NumPy array
	using NodeFacade_t 	 = ::yggdrasil::yggdrasil_nodeFacade_t;
	using LeafIterator_t 	 = ::yggdrasil::yggdrasil_leafIterator_t;
	using Builder_t 	 = ::yggdrasil::yggdrasil_treeBuilder_t;
	using SampleCollection_t = ::yggdrasil::yggdrasil_sampleCollection_t;
	using SampleArray_t      = ::yggdrasil::yggdrasil_arraySample_t;
	using SampleList_t 	 = ::yggdrasil::yggdrasil_sampleList_t;


	/*
	 * ================
	 * The full tree
	 */
	py::class_<Tree_t>(m, "DETree",
		"This is the densitzy estimation tree that is implemented by Yggdrasil."
		" It fits itself upon construction and cna then be used."
		" It offers fucntionality to check its own integrity."
		" It accepts all three containers, however using the sample collection is the most efficient way."
		" Since this is also used in its internal structure, it can be moved into the tree and no coping is needed."
		" It also offers the interface to perform the query operations."
		" The ree is currently not able to be copied."
		)
		/*
		 * Constructors
		 */
		.def(py::init([](SampleCollection_t& sc, const HyperCube_t& domain, const Builder_t& builder) -> Tree_ptr
					{
					  if(sc.nSamples() == 0) {throw YGGDRASIL_EXCEPT_InvArg("The sample container is empty, can not fit a tree.");};
					  return Tree_ptr(new Tree_t(sc, domain, builder, false));
					}),
				"This constructor constructs a tree out of a sammple collection."
				" If the domain is invalid the domain will be estimated."
				" The tree will be build acording to the passed builder object.",
				py::arg("samples"),
				py::arg("domain"),
				py::arg("builder")
		)
		.def(py::init([](SampleCollection_t& sc, const HyperCube_t& domain, const Builder_t& builder, const bool load) -> Tree_ptr
					{
					  if(sc.nSamples() == 0) {throw YGGDRASIL_EXCEPT_InvArg("The sample container is empty, can not fit a tree.");};
					  return Tree_ptr(new Tree_t(sc, domain, builder, load));
					}),
				"This constructor constructs a tree out of a sammple collection."
				" If the domain is invalid the domain will be estimated."
				" The tree will be build acording to the passed builder object."
				" This constructor offers to load the samples from the sample collection into *this."
				" This avoids a coping operation, however the collection will be empyty afterwards.",
				py::arg("samples"),
				py::arg("domain"),
				py::arg("builder"),
				py::arg("load")
		)
		.def(py::init([](const SampleList_t& sl, const HyperCube_t& domain, const Builder_t& builder) -> Tree_ptr
					{
					  if(sl.nSamples() == 0) {throw YGGDRASIL_EXCEPT_InvArg("The sample container is emoty, can not fit a tree.");};
					  return Tree_ptr(new Tree_t(sl, domain, builder));
					}),
				"This constructor constructs a tree out of a sammple list."
				" If the domain is invalid the domain will be estimated."
				" The tree will be build acording to the passed builder object.",
				py::arg("samples"),
				py::arg("domain"),
				py::arg("builder")
		)
		.def(py::init([](const SampleArray_t& sa, const HyperCube_t& domain, const Builder_t& builder) -> Tree_ptr
					{
					  if(sa.nSamples() == 0) {throw YGGDRASIL_EXCEPT_InvArg("THe sample container is empty, can not fit a tree.");};
					  return Tree_ptr(new Tree_t(sa, domain, builder));
					}),
				"This constructor constructs a tree out of a sammple array."
				" If the domain is invalid the domain will be estimated."
				" The tree will be build acording to the passed builder object.",
				py::arg("samples"),
				py::arg("domain"),
				py::arg("builder")
		)


		/*
		 * Data spce function
		 */
		.def("getGlobalDataSpace", [](const Tree_t& t) -> HyperCube_t
					{HyperCube_t hc(t.getGlobalDataSpace()); return hc;},
				"This function returns a copy of the data sapce of the sample."
				" This is either the domain that was passed upon construction, or the estimated one."
		)

		/*
		 * Iterator fucntion
		 * allows the iteration over the tree
		 */
		.def("__iter__", [](const Tree_t& t)
					{
					   	Tree_t::SubLeafIterator_t ende;	//Default constructed, will result in end iterator
						Tree_t::SubLeafIterator_t anfa = t.getLeafIterator();	//Begining
						return py::make_iterator(anfa, ende);
					},
				"This function allows the iteration over all leafs in the tree."
				" The returned iterator will dereference to a node facade, this is a read only wrapper around an iterator."
				" It provides some functions that allows accessing the associated node."
				" It only gives access to the leaf and writes will have no effect.",
				py::keep_alive<0, 1>() /* Essential: keep object alive while iterator exists */
		)
		.def("getLeafIterator", [](const Tree_t& t) -> LeafIterator_t
					{return t.getLeafIterator();},
				"This function returns a leaf iterator."
				" This is basically equivalent to the __iter__ function, but allows a more direct controll over the processor.",
				py::keep_alive<0, 1>() /* Essential: keep object alive while iterator exists */
		)





		/*
		 * Status functions
		 */
		.def("isValid", &Tree_t::isValid,
				"Test if the tree is valid, this only performs a small and quick check."
		)
		.def("checkIntegrity", &Tree_t::checkIntegrity,
				"This function makes a more involved check, it involves a descend operation."
		)
		.def("isFullySplittedTree", &Tree_t::isFullySplittedTree,
				"This function performs an even more extended test, than the checkIntegrity test."
				" It basically tests, if the tree is fully splitted."
		)

		/*
		 * Sample related informations
		 */
		.def("getMass", &Tree_t::getMass,
				"This function returns the mass of the tree."
				" The mass is tne number of samples that are stored inside the tree."
		)
		.def("nSamples", &Tree_t::getMass,
				"This function returns the number of samples that are stored inside the tree."
				" It is an alias of the getMass() fucntion."
		)

		/*
		 * Tree structure
		 */
		.def("getDepthMap", [](const Tree_t& t) -> DepthMap_t
					{return t.getDepthMap();},
				"This fucntion returns a depth map of the tree."
				" A depth map is a structurure that describes the leafs of the tree."
		)
		.def("nNodes", &Tree_t::nNodes,
				"This function returns the number of nodes inside the tree."
		)
		.def("getTreeComposition", [](const Tree_t& t) -> ::std::tuple<Size_t, Size_t, Size_t>
					{const auto comp = t.getTreeComposition(); return comp;},
				"This function returns the tree composition, this is a tuple with tree elements."
				" The first element is the number of true splits."
				" The second number is the number of indirect splits."
				" The third is the number of leafs inside the tree."
		)
		.def("nLeafs", [](const Tree_t& t) -> Size_t
					{const auto comp = t.getTreeComposition(); return std::get<2>(comp);},
				"This function returns the number of leafs inside the tree."
		)


		/*
		 * ====================
		 * 	Query Functions
		 */

		/*
		 * Evaluate at a single location
		 *  	All variants
		 */
		.def("evaluateSamplesAt", [](const Tree_t& t, const Sample_t& s, const bool beQuiet) -> Real_t
					{return (beQuiet == true) ? t.evaluateSamplesAt_noErr(s) : t.evaluateSamplesAt(s);},
				"This function evaluates the tree at a single location x."
				" The sample must be in the not rescalled data space, rescalling is done by the tree."
				" If the optional parameter beBuiet is set to false, the default, this fucntion generates an error, if the samples does not lie inside the data space of the tree."
				" If the parameter is set to true, samples that are outside the data domain are set to zero.",
				py::arg("x"),
				py::arg_v("beQuiet", false, "If set to true, no error is generated if x is outside the domain.")
		)
		.def("evaluateSamplesAt_noErr", [](const Tree_t& t, const Sample_t& x) -> Real_t
					{return t.evaluateSamplesAt_noErr(x);},
				"This function evaluates the probability given by the tree."
				" If the sample lies outside the domain, its value is zero."
				" No error is generated.",
				py::arg("x")
		)
		.def("evaluateSamplesAt_err", [](const Tree_t& t, const Sample_t& x) -> Real_t
					{return t.evaluateSamplesAt(x);},
				"This function evaluates the sample x."
				" If the sample lies ouside the domain an error is generated.",
				py::arg("x")
		)


		/*
		 * Evaluate at many locations
		 * 	Choose between error behaviour
		 */
		.def("evaluateSamplesAt", [](const Tree_t& t, const SampleArray_t& sa, const bool beQuiet) -> PDFValueArrayEigen_t
					{return t.evaluateSamplesAt_Eigen(sa, beQuiet);},
				"This function evaluates the pdf at the given locations."
				" The samples are stored inside a sample array."
				" The handling of samples outside the domain can be selected with the optional parameter.",
				py::arg("x"),
				py::arg_v("beQuiet", false, "If set to true, values outside will have probability zero.")
		)
		.def("evaluateSamplesAt", [](const Tree_t& t, const SampleList_t& sl, const bool beQuiet) -> PDFValueArrayEigen_t
					{return t.evaluateSamplesAt_Eigen(sl, beQuiet);},
				"This function evaluates the pdf at the given locations."
				" The samples are stored inside a sample list."
				" The handling of samples outside the domain can be selected with the optional parameter.",
				py::arg("x"),
				py::arg_v("beQuiet", false, "If set to true, values outside will have probability zero.")
		)
		.def("evaluateSamplesAt", [](const Tree_t& t, const SampleCollection_t& sc, const bool beQuiet) -> PDFValueArrayEigen_t
					{return t.evaluateSamplesAt_Eigen(sc, beQuiet);},
				"This function evaluates the pdf at the given locations."
				" The samples are stored inside a sample collection."
				" The handling of samples outside the domain can be selected with the optional parameter.",
				py::arg("x"),
				py::arg_v("beQuiet", false, "If set to true, values outside will have probability zero.")
		)



		/*
		 * Evaluate at many locations
		 * 	generate errors if outside
		 */
		.def("evaluateSamplesAt_err", [](const Tree_t& t, const SampleArray_t& sa) -> PDFValueArrayEigen_t
					{return t.evaluateSamplesAt_Eigen(sa, false);},
				"This function evaluates the pdf at the given locations."
				" The samples are stored inside a sample array."
				" If a sample is not inside the domain, an error is generated.",
				py::arg("x")
		)
		.def("evaluateSamplesAt_err", [](const Tree_t& t, const SampleList_t& sl) -> PDFValueArrayEigen_t
					{return t.evaluateSamplesAt_Eigen(sl, false);},
				"This function evaluates the pdf at the given locations."
				" The samples are stored inside a sample list."
				" If a sample is not inside the domain, an error is generated.",
				py::arg("x")
		)
		.def("evaluateSamplesAt_err", [](const Tree_t& t, const SampleCollection_t& sc) -> PDFValueArrayEigen_t
					{return t.evaluateSamplesAt_Eigen(sc, false);},
				"This function evaluates the pdf at the given locations."
				" The samples are stored inside a sample collection."
				" If a sample is not inside the domain, an error is generated.",
				py::arg("x")
		)

		/*
		 * Evaluate at many locations
		 * 	if outside the domain, then quietly set to zero
		 */
		.def("evaluateSamplesAt_noErr", [](const Tree_t& t, const SampleArray_t& sa) -> PDFValueArrayEigen_t
					{return t.evaluateSamplesAt_Eigen(sa, true);},
				"This function evaluates the pdf at the given locations."
				" The samples are stored inside a sample array."
				" If the sample lies outside the domain, then its value is set to zero.",
				py::arg("x")
		)
		.def("evaluateSamplesAt_noErr", [](const Tree_t& t, const SampleList_t& sl) -> PDFValueArrayEigen_t
					{return t.evaluateSamplesAt_Eigen(sl, true);},
				"This function evaluates the pdf at the given locations."
				" The samples are stored inside a sample list."
				" If a sample is not inside the domain, its pdf value is set to zero.",
				py::arg("x")
		)
		.def("evaluateSamplesAt_noErr", [](const Tree_t& t, const SampleCollection_t& sc) -> PDFValueArrayEigen_t
					{return t.evaluateSamplesAt_Eigen(sc, true);},
				"This function evaluates the pdf at the given locations."
				" The samples are stored inside a sample collection."
				" If a sample is not inside the domain, its pdf value is set to zero.",
				py::arg("x")
		)
	; //End: tree wrapper

	return;
}; //End: wrapperint utility function








