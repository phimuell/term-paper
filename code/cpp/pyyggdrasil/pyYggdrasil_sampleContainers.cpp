/**
 * \brief	This fucntion implements the wrapping of the sample containers.
 *
 */

//Include the yggdrasil code that is needed
#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_matrixOrder.hpp>

#include <core/yggdrasil_eigen.hpp>


//Include Eigen
#include <Eigen/Core>

//Include the bindings
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

//Make a namespace alias
namespace py = pybind11;


void
pyYggdrasil_sampleContainersWrapper(
	py::module&	m)
{
	/*
	 * =======================
	 * USINGS
	 */
	using Sample_t           = ::yggdrasil::yggdrasil_sample_t;
	using SampleCollection_t = ::yggdrasil::yggdrasil_sampleCollection_t;
	using SampleArray_t      = ::yggdrasil::yggdrasil_arraySample_t;
	using SampleList_t	 = ::yggdrasil::yggdrasil_sampleList_t;
	using Int_t 		 = ::yggdrasil::Int_t;
	using Numeric_t 	 = ::yggdrasil::Numeric_t;
	using Size_t 		 = ::yggdrasil::Size_t;
	using DimArrayEigen_t	 = ::Eigen::VectorXd;	//This is used as substitute for the dimensinal array in the interface
	using PyVector_t 	 = ::yggdrasil::yggdrasil_eigenRowVector_t;	//This is a vector that is tailored for functions that takes a vector as argument.

	using eMatrixViewRow     = ::yggdrasil::eMatrixViewRow;			//Will have other meaning in Python
	using Matrix_t 		 = SampleCollection_t::RowMatrix_t;		//This is the only matrix order we support
	using MatrixRef_t 	 = ::yggdrasil::yggdrasil_eigenRef_t<const Matrix_t>;	//This is a better reference type

	/*
	 * =======================
	 * Sample
	 */
	py::class_<Sample_t>(m, "Sample",
		"This class models a single sample."
		" It is basically an array with some syntactic sugar such that it is easy to use."
		" It is not used in the invernals of Yggdrasil, but serves as an important interface component."
		)
		/*
		 * Constructors
		 */
		.def(py::init([](const Size_t d) -> Sample_t
					{
					  if(d == 0) {throw YGGDRASIL_EXCEPT_InvArg("A sample with dimension 0 was tried to construct.");};
					  return Sample_t(d);
					}),
				"A constructor that constructs a sample of the given dimension d."
				" All components are set to zero.",
				py::arg("d")
		)
		.def(py::init([](const Size_t d, const Numeric_t v) -> Sample_t
					{
					  if(d == 0) {throw YGGDRASIL_EXCEPT_InvArg("Tried to construct a sample with zero dimensions.");};
					  if(yggdrasil::isValidFloat(v) == false) {throw YGGDRASIL_EXCEPT_InvArg("An invalid value as sample component was used to construct the sample.");};
					  return Sample_t(d, v);
					}),
				"A constructor that construct a sample of the given dimension d, all dimension are set to the given value v.",
				py::arg("d"),
				py::arg("v")
		)
		.def(py::init([](const py::list& l)
					{
					  Sample_t s(l.size());
					  if(s.nDims() == 0) {throw YGGDRASIL_EXCEPT_InvArg("Tried to construct a sample of dimension zero.");};
					  yggdrasil::Size_t i = 0;
					  for(const auto c : l) {s.at(i++) = py::cast<Numeric_t>(c);}
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("The construction from a list resulted in an invalid sample.");};
					  return s;
					}),
				"A constructor that builds a sample from a list/vector.",
				py::arg("list")
		)
		.def(py::init([](const Sample_t& src)
					{
					  if(src.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Tried to xopy an invalid sample.");};
					  Sample_t dolly(src);
					  return dolly;
					}),
				"This constructor construct a sample from the passed sample src."
				" This call will generate a deep copy."
				" It is an error if an invalid sample is passed.",
				py::arg("src")
		)

		/*
		 * Operatros
		 */
		.def("__repr__", &Sample_t::print,
				"Prints the sample."
		)
		.def("__str__", &Sample_t::print,
				"Transform this into a string, the same as the printing function."
		)
		.def("__getitem__", [](const Sample_t& s, const Size_t i) -> double
					{return s.at(i);},
				"Accessing the sample at the ith component.",
				py::arg("i")
		)
		.def("__setitem__", [](Sample_t& s, const Size_t i, const Numeric_t v) -> void
					{
					  if(yggdrasil::isValidFloat(v) == false) {throw YGGDRASIL_EXCEPT_InvArg("Tried to assign an invalid value to a sample.");};
					  s.at(i) = v;
					  return;
					},
				"Setting the ith component of the sample to the new value v."
		)
		.def("__len__", &Sample_t::nDims,
				"Get the dimension of the sample."
		)
		/*
		 * Again strannge behaviour.
		 * We can iterate and assigne to, but there is no error generated and the
		 * assignment, has no effect at all, so we disallow it.
		 */
		.def("__iter__", [](const Sample_t& s)
				{throw YGGDRASIL_EXCEPT_illMethod("The iterate method for the sample is not implemented in Python."); (void)s;}
		)


		/*
		 * Other Functions
		 */
		.def("size", &Sample_t::nDims,
				"The dimension of the sample."
		)
		.def("nDims", &Sample_t::nDims,
				"The dimension of the sample."
		)
		.def("isValid", &Sample_t::isValid,
				"Tests if the sample is considered valid."
		)
		.def("isInsideUnit", &Sample_t::isInsideUnit,
				"Tests if the sample is inside the unit cube."
		)
	;//End of sample bindings

	m.def("CREAT_INVALID_SAMPLE", [](const Size_t d) -> Sample_t
				{return Sample_t(d, NAN);},
			"This function is the only way of creating an invalid sample in Python."
			" The component will be set to NAN, the dimension of the sample can be selected by d."
			" This fucntion is mostly for ilustrative prupose, and you should not use it.",
			py::arg("d")
	);

	/*
	 * Matrix View
	 *
	 * This enum controls how the matrix is interpreted.
	 * Note that on C++ the enum encode what a row contains.
	 * In python we swithced to a different, but equivalent virew,
	 * where we encode if a smaple is on the row or the column.
	 */
	py::enum_<eMatrixViewRow>(m, "eMatrixViewSample",
			"This enum encode how we see a matrix."
			" It encodes where a sample lies in the matrix.")
	.value(
		"Row",
		eMatrixViewRow::Sample,
		"Each row conatins one sample."
	)
	.value(
		"Col",
		eMatrixViewRow::Dimension,
		"Each column conatins one sample."
	)


	; //End: enum(eMatrixViewRow)



	/*
	 * Sample Array
	 *
	 * This class supports the universal interface
	 */
	py::class_<SampleArray_t>(m, "SampleArray",
		"This is one of the three disfferent containers to store many samples."
		" This is one of the most efficient methods to store a large amount of samples."
		" Internaly it stores a single array, where the samples are placed consecutive."
		" Like all container it implements the 'Universal Container Interface'."
		)
		/*
		 * Constructors
		 */
		.def(py::init<Size_t>(),
				"Constructor to construct an empty array for samples of dimension d.",
				py::arg("d")
		)
		.def(py::init<Size_t, Size_t>(),
				"Constructopr to construct an array of dimension d with space for N samples.",
				py::arg("d"),
				py::arg("N")
		)
		.def(py::init([](const SampleArray_t& sa) -> SampleArray_t
					{SampleArray_t dolly(sa); return dolly;}),
				"This constructor constructs a sample array by deep copying the sourc array src.",
				py::arg("src")
		)
		.def(py::init([](const SampleList_t& sl) -> SampleArray_t
					{return SampleArray_t(sl);}),
				"This constructor performs a deep copy of the sample list src.",
				py::arg("src")
		)
		.def(py::init([](const SampleCollection_t sc) -> SampleArray_t
					{return SampleArray_t(sc);}),
				"This constructor performs a deep copy of the sample collection src.",
				py::arg("src")
		)
		.def(py::init([](const MatrixRef_t& mat, eMatrixViewRow view) -> SampleArray_t
					{
					  //This will also check for infinity; according to the Eigen manual
					  if(mat.allFinite() == false) {throw YGGDRASIL_EXCEPT_InvArg("Tried to construct a sample array out of a matrix that contained NaN or inf");};
					  SampleArray_t sa(mat, view);
					  return sa;
					}),
				"This constructor constructs a sample array out of the matrix mat."
				" The matrix is interpreted using the view argument."
				" If set to Row, each row is interpreted as one sample."
				" If set to Col, each colum is interpreted as one sample."
				" Setting it to Row is most efficient."
				" If mat conatins infinity or has nan an error is generated.",
				py::arg("mat"),
				py::arg("view")
		)
		.def("clone", [](const SampleArray_t& sa) -> SampleArray_t
					{return sa.clone();},
				"This function performs a deep copy of *this."
				" This function allows to copy a container without the need of knowing its type."
				" This is interesting when exploiting duck typing."
		)

		/*
		 * Operators
		 *
		 * The operators are able to perform operations that the C++ class
		 * can not, the reason is there is an implicit proxy type
		 */
		.def("__len__", &SampleArray_t::nSamples,
				"Returns the numbers of samples that are inside the container."
		)
		.def("__getitem__", [](const SampleArray_t& a, const Size_t i) -> Sample_t
					{
					  Sample_t b(a.at(i));
					  yggdrasil_assert((Size_t)b.nDims() == a.nDims());
					  return b;
					},
				"Returns a copy of the i-th sample of the container.",
				py::arg("i")
		)
		.def("__setitem__", [](SampleArray_t& a, const Size_t i, const Sample_t& s) -> void
					{
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Can not write an invalid sample to the container.");};
					  a.loadSample(s, i);  //Size check is enforced
					  return;
					},
				"Setting the ith sample of the array to the sample s, the sample is copied."
				" Note that if s is invalid an exception is generated."
				" Also if there is an dimension missmatch.",
				py::arg("i"),
				py::arg("s")
		)
		.def("__iter__", [](const SampleArray_t& sa)
				{throw YGGDRASIL_EXCEPT_illMethod("The iterate method for the sample array is not implemented in Python."); (void)sa;}
		)


		/*
		 * Other function
		 */
		.def("nDims", &SampleArray_t::nDims,
				"Returns the dimensionality of the underling samples."
		)
#if defined(YGGDRASIL_PYYGGDRASIL_PROVIDE_SIZEFKT) && YGGDRASIL_PYYGGDRASIL_PROVIDE_SIZEFKT == 1
		.def("size", &SampleArray_t::size,
				"Returns the number of stored samples."
		)
#endif
		.def("nSamples", &SampleArray_t::nSamples,
				"Returns the number of stored sample."
		)
		.def("resize", &SampleArray_t::resize,
				"Resizing the sample array to newSize, this only works if the array has currently zero samples.",
				py::arg("newSize")
		)
		.def("reserve", [](SampleArray_t& sa, const Size_t newCapacity) -> void
					{sa.reserve(newCapacity); return;},
				"This function preallocate space, this allows faster inserting if the number of samples is known in advance."
				" This function does not have an effect if the size of the container is set, thus only empty container.",
				py::arg("newCapacity")
		)
		.def("addNewSample", [](SampleArray_t& sa, const Sample_t& s) -> void
					{
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Can not write an invalid sample to the container.");};
					  sa.addNewSample(s);
					  return;
					},
				"This function adds the sample s at the end of the container. This can cause a reallocation."
				" This fucntion does not allows to store invalid samples in the containers."
				" Also an error will be generated if s does not have the roight dimensions.",
				py::arg("s")
		)
		.def("setSampleTo", [](SampleArray_t& sa, const Sample_t& s, const Size_t i) -> void
					{
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Can not write an invalid sample to the container.");};
					  sa.setSampleTo(s, i); //The size constraint are handled in teh container
					  return;
					},
				"This function exchanges the sample with index i with the provided sample s.",
				py::arg("s"),
				py::arg("i")
		)
		.def("getSample", [](const SampleArray_t& sa,  const Size_t i) -> Sample_t
					{
					  Sample_t s(sa.getSample(i));
					  yggdrasil_assert(s.isValid());
					  return s;
					},
				"Returns a copy of the sample with index i.",
				py::arg("i")
		)
		.def("clear", &SampleArray_t::clear,
				"Set the number of samples of the array zo zero and free the storage. The size of the dimension is not changed."
		)
		.def("loadSample", [](SampleArray_t& sa, const Sample_t& s, const Size_t idx) -> void
					{
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Troed to store an invalid sample in the conatiner.");};
					  sa.loadSample(s, idx);  //Size constraint is handled by the container itseld
					  return;
					},
				"Load the sample s into the array, it will become the ith sample.",
				py::arg("s"),
				py::arg("i")
		)


		/*
		 * This function is for accessing the dimension
		 */
		.def("getDimensionArray", [](const SampleArray_t& sa, const Size_t d) -> DimArrayEigen_t
					{
					  DimArrayEigen_t dimArray(sa.nSamples());	//Allocate space for the output
					  sa.internal_getDimensionArray<DimArrayEigen_t>(d, &dimArray);
					  return dimArray;
					},
				"This function returns a single component, dimension d, but of all samples."
				" The order inside the returned Eigen vector is the same order as would accessing be.",
				py::arg("d")
		)

		.def("getMatrix", [](const SampleArray_t& sa, const eMatrixViewRow view) -> Matrix_t
					{ Matrix_t kl = sa.getRowMatrix(view); return kl;},
				"This function return a matrix representation of *this back."
				" The encoding of the matrix is controlled with the view argument."
				" If set to Row, each row will conatin one sample."
				" If set to Col, each column will contain one sample.",
				py::arg("view")
		)


		.def("setSampleComponent", [](SampleArray_t& sa,  const Size_t i,  const Size_t j,  const Numeric_t v) -> void
					{ sa.setSampleComponent(i, j, v); return;},
				"This function allows to set a single component of a single sample to a specifiv value v."
				" This function is equivalent in first loading the ith sample."
				" Then setting jts ith component to v and then store it back in the container."
				" But this does not need to load the sample from teh container since it happens internaly.",
				py::arg("i"),
				py::arg("j"),
				py::arg("v")
		)
		.def("getSampleComponent", [](const SampleArray_t& sa,  const Size_t i,  const Size_t j) -> Numeric_t
					{ return sa.getSampleComponent(i, j); },
				"This function returns the value of the jth component of the ith sample.",
				py::arg("i"),
				py::arg("j")
		)
		.def("setDimensionTo", [](SampleArray_t& sa,  const Size_t j,  const Numeric_t v) -> void
					{ sa.setDimensionTo(j, v); return; },
				"This function will set the value of the jth component of _all_ samples to v."
				" This function is like calling the setSampleComponent() function for each sample, but more efficiently."
				" The value is the same for all samples.",
				py::arg("j"),
				py::arg("v")
		)
		.def("setDimensionArray", [](SampleArray_t& sa,  const Size_t j,  const ::yggdrasil::yggdrasil_eigenRef_t<const PyVector_t>& dArr) -> void
					{ sa.setDimensionArray(j, dArr); return; },
				"This function copies the values of the array given as dArr into the component j."
				" This fucntion allwos to set the value of the sample in dimension j to an individual value.",
				py::arg("j"),
				py::arg("dArr")
		)
	; //End od sample array



	/*
	 * =========================
	 * Sample list
	 */
	py::class_<SampleList_t>(m, "SampleList",
		"This is the second sample container that is provided by Yggdrasil."
		" Like the array it implements the 'Universal Container Interface'."
		" It is not as much space efficient as the array, to be honest it is much worser."
		" Instead of storing a single array, this class stores an array of samples."
		" Thus we have a lot of overhead, that is generated by the samples itself."
		" However it is supported for convenience reasons."
		)
		/*
		 * Constructors
		 */
		.def(py::init<Int_t>(),
				"Constructing an empty sample collection of dimension d.",
				py::arg("d")
		)
		.def(py::init<Size_t, Size_t>(),
				"Construct a list with samples of dimension d, the list has size n.",
				py::arg("d"),
				py::arg("n")
		)
		.def(py::init([](const SampleList_t& src) -> SampleList_t
					{SampleList_t dolly(src); return dolly;}),
				"Construct a sample list by deep coping the source list src.",
				py::arg("src")
		)
		.def(py::init([](const SampleArray_t& src) -> SampleList_t
					{return SampleList_t(src);}),
				"Performs a deep copy of the sample array src.",
				py::arg("src")
		)
		.def(py::init([](const SampleCollection_t& src) -> SampleList_t
					{return SampleList_t(src);}),
				"Performs a deep copy of the sample collection src.",
				py::arg("src")
		)
		.def(py::init([](const MatrixRef_t& mat, eMatrixViewRow view) -> SampleList_t
					{
					  if(mat.allFinite() == false) {throw YGGDRASIL_EXCEPT_InvArg("Tried to construct a sample list from a matrix that contains Nan and/or inf values.");};
					  SampleList_t sl(mat, view);
					  return sl;
					}),
				"This constructor constructs a sample List out of a matrix."
				" How the matrix is interpreted is determined by the view parameter."
				" If view is set to Row, each row, will be interpreted as a single sample."
				" If set to Col, ewach column will be interpreted as single sample."
				" Setting it to Row is most efficient."
				" If the matrix contains NaN or non finite values an exception is generated.",
				py::arg("mat"),
				py::arg("view")
		)
		.def("clone", [](const SampleList_t& sl) -> SampleList_t
					{return sl.clone();},
				"Perfroms a deep copy of *this."
				" This function is usefull to copy the container without thee need of knowing its type."
				" This is usefull when duck typing is exploited."
		)


		/*
		 * Operators
		 */
		.def("__len__", [](const SampleList_t& sc) -> Size_t
					{return sc.nSamples();},
				"Return the nunmber of samples stored inside the scample collection."
		)
		.def("__getitem__", [](const SampleList_t& sl, const Size_t i) -> Sample_t
					{Sample_t s(sl.getSample(i)); return s;}, //Size constraint are fullfilled automatically
				"Returns a copy of the ith sample.",
				py::arg("i")
		)
		.def("__setitem__", [](SampleList_t& sl, const Size_t i, const Sample_t& s) -> void
					{
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Can not write an invalid sample to the container.");};
					  sl.setSampleTo(s, i);  //Size is enforced by the container
					  return;
					},
				"Set the ith sample of the list to the given sample s."
				" Note that only valid samples and samples with the same numbers of dimensios are accepted.",
				py::arg("i"),
				py::arg("s")
		)
		.def("__iter__", [](const SampleList_t& sl)
				{throw YGGDRASIL_EXCEPT_illMethod("The iterate method for the sample list is not implemented in Python."); (void)sl;}
		)



		/*
		 * Functions
		 */
		.def("nSamples", [](const SampleList_t& sl) -> Size_t
					{return sl.nSamples();},
				"Return the nunmber of samples stored inside the sample list."
		)
#if defined(YGGDRASIL_PYYGGDRASIL_PROVIDE_SIZEFKT) && YGGDRASIL_PYYGGDRASIL_PROVIDE_SIZEFKT == 1
		.def("size", [](const SampleList_t& sl) -> Size_t
					{return sl.size();},
				"Return the size of the sample list, which is equivalent to the number of samples."
		)
#endif
		.def("nDims", [](const SampleList_t& sl) -> Size_t
					{return sl.nDims();},
				"Return the number of dimensions of the sample list."
		)
		.def("setSampleTo", [](SampleList_t& sl, const Sample_t& s, const Size_t idx) -> void
					{
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Can not write an invalid sample to the container.");};
					  sl.setSampleTo(s, idx);  //SIze is enforced by the container
					  return;
					},
				"Load the sample s into the list, it will become the ith sample."
				" Note that only samples with the same numbers of dimensions and that are valid are accepted.",
				py::arg("s"),
				py::arg("i")
		)
		.def("addNewSample", [](SampleList_t& sl, const Sample_t& s) -> void
					{
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Can not write an invalid sample to the container.");};
					  sl.addNewSample(s);  //Size is enforced by the container
					  return;
					},
				"Add the sample s at the end of the current list."
				" Only samples that are valid and have the same nuber of dimensions are accepted.",
				py::arg("s")
		)
		.def("reserve", [](SampleList_t& sa, const Size_t newCapacity) -> void
					{sa.reserve(newCapacity); return;},
				"This function preallocate space, this allows faster inserting if the number of samples is known in advance."
				" This function does not have an effect if the size of the container is set, thus only empty container.",
				py::arg("newCapacity")
		)
		.def("getSample", [](const SampleList_t& sl, const Size_t i) -> Sample_t
					{return sl.getSample(i);}, //Size is enforced by the container
				"Get a copy of the ith sample of the sample list.",
				py::arg("i")
		)
		.def("resize", [](SampleList_t& sl, const Size_t newSize) -> void
					{sl.resize(newSize); return;},
				"Set the number of samples of the list to newSize. This operation is only allowed if the list is empty.",
				py::arg("newSize")
		)
		.def("clear", &SampleList_t::clear,
				"This function sets the size of the sample list to zero and deallocates all internal structures."
		)

		/*
		 * This function is for accessing the dimension
		 */
		.def("getDimensionArray", [](const SampleList_t& sl, const Size_t d) -> DimArrayEigen_t
					{
					  DimArrayEigen_t dimArray(sl.nSamples());	//Allocate space for the output
					  sl.internal_getDimensionArray<DimArrayEigen_t>(d, &dimArray);
					  return dimArray;
					},
				"This function returns a single component, dimension d, but of all samples."
				" The order inside the returned Eigen vector is the same order as would accessing be.",
				py::arg("d")
		)

		.def("getMatrix", [](const SampleList_t& sl, const eMatrixViewRow view) -> Matrix_t
					{
					  Matrix_t ma = sl.getRowMatrix(view);
					  return ma;
					},
				"This function will convert *this into a matrix."
				" How the matrix can be interpreted is controlled by view."
				" If view is set to Row, each row will contains one sample."
				" If set to Col, each column will contain one sample.",
				py::arg("view")
		)


		.def("setSampleComponent", [](SampleList_t& sl,  const Size_t i,  const Size_t j,  const Numeric_t v) -> void
					{ sl.setSampleComponent(i, j, v); return;},
				"This function allows to set a single component of a single sample to a specifiv value v."
				" This function is equivalent in first loading the ith sample."
				" Then setting its jth component to v and then store it back in the container."
				" But this does not need to load the sample from teh container since it happens internaly.",
				py::arg("i"),
				py::arg("j"),
				py::arg("v")
		)
		.def("getSampleComponent", [](const SampleList_t& sl,  const Size_t i,  const Size_t j) -> Numeric_t
					{ return sl.getSampleComponent(i, j); },
				"This function returns the value of the jth component of the ith sample.",
				py::arg("i"),
				py::arg("j")
		)
		.def("setDimensionTo", [](SampleList_t& sl,  const Size_t j,  const Numeric_t v) -> void
					{ sl.setDimensionTo(j, v); return; },
				"This function will set the value of the jth component of _all_ samples to v."
				" This function is like calling the setSampleComponent() function for each sample, but more efficiently."
				" The value is the same for all samples.",
				py::arg("j"),
				py::arg("v")
		)
		.def("setDimensionArray", [](SampleList_t& sl,  const Size_t j,  const ::yggdrasil::yggdrasil_eigenRef_t<const PyVector_t>& dArr) -> void
					{ sl.setDimensionArray(j, dArr); return; },
				"This function copies the values of the array given as dArr into the component j."
				" This fucntion allows to set the value of the sample in dimension j to an individual value.",
				py::arg("j"),
				py::arg("dArr")
		)

	; //End: wrapping for sample list


	/*
	 * A sample collection
	 *
	 * This is a very reduced set of functions that is provided
	 */
	py::class_<SampleCollection_t>(m, "SampleCollection",
		"This is one of the three container to store samples."
		" It is sligthly less efficent than the array but much better than the sample list."
		" As the sample list it does maintain an array of arrays."
		" However instead of storing the samples, it stores the dimensions."
		" This class allows fast access to a whole dimension but slow access to a complete sample."
		" However it is the internal format of Yggdrasil, so when you want to fit a tree unse this container."
		)
		/*
		 * Constructors
		 */
		.def(py::init<Int_t>(),
				"Constructing an empty sample collection of dimension d.",
				py::arg("d")
		)
		.def(py::init<Size_t, Size_t>(),
				"Construct a collection with dimension d, allocate space for n samples.",
				py::arg("d"),
				py::arg("n")
		)
		.def(py::init<const SampleArray_t&>(),
				"Construct a sample collection from a sample array src, a deep copy is performed.",
				py::arg("src")
		)
		.def(py::init<const SampleList_t&>(),
				"Construct a sample collection from a sample list src, a deep copy is performed.",
				py::arg("src")
		)
		.def(py::init([](const SampleCollection_t& src) -> SampleCollection_t
					{SampleCollection_t dolly(src); return dolly;}),
				"Construct a sample collection by deepcoping the collection that is given as src.",
				py::arg("src")
		)
		.def(py::init([](const MatrixRef_t& mat, const eMatrixViewRow view) -> SampleCollection_t
					{
					  if(mat.allFinite() == false) {throw YGGDRASIL_EXCEPT_InvArg("Tried to construct a sample collection from a matrix that contains NaN and/or inf values.");};
					  SampleCollection_t sc(mat, view);
					  return sc;
					}),
				"Constructs a sample collection out of a matrix."
				" How the matrix is interpreted is selected by the view parameter."
				" Set it to Row means that each row is interpreted as a single sample."
				" Set it to Col means each column is interpreted as a single sample."
				" This constructor works bests if view is set to Col, this is different from the other two."
				" An error is generated if the matrix contained NaN or inf values.",
				py::arg("mat"),
				py::arg("view")
		)
		.def("clone", [](const SampleCollection_t& sc) -> SampleCollection_t
					{ return sc.clone();},
				"This function performs a deep copy of *this."
				" This function is usefull when writing generic code."
		)


		/*
		 * Operators
		 */
		.def("__len__", [](const SampleCollection_t& sc) -> Size_t
					{return sc.nSamples();},
				"Return the nunmber of samples stored inside the scample collection."
		)
		.def("__getitem__", [](const SampleCollection_t& sc, const Size_t i) -> Sample_t
					{Sample_t s(sc.getSample(i)); return s;},  //Sioze is enfored by constrctuoin
				"Returns a copy of the ith sample.",
				py::arg("i")
		)
		.def("__setitem__", [](SampleCollection_t& sc, const Size_t i, const Sample_t& s) -> void
					{
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Can not write an invalid sample to the container.");};
					  sc.setSampleTo(s, i); //Size is enforced by construction
					  return;
					},
				"Set the ith sample of the collection to the given sample s."
				" The sample must be valid and have the correct dimension.",
				py::arg("i"),
				py::arg("s")
		)
		.def("__iter__", [](const SampleCollection_t& sc)
				{throw YGGDRASIL_EXCEPT_illMethod("The iterate method for the sample collection is not implemented in Python."); (void)sc;}
		)



		/*
		 * Functions
		 */
		.def("nSamples", [](const SampleCollection_t& sc) -> Size_t
					{return sc.nSamples();},
				"Return the nunmber of samples stored inside the scample collection."
		)
#if defined(YGGDRASIL_PYYGGDRASIL_PROVIDE_SIZEFKT) && YGGDRASIL_PYYGGDRASIL_PROVIDE_SIZEFKT == 1
		.def("size", [](const SampleCollection_t& sc) -> Size_t
					{return sc.nSamples();},	//Note this is fundamental different than the C++ code
				"This function returns the number of samples inside the collection. Note this is different from the C++ code, in which it returns the number of dimensions."
		)
#endif
		.def("nDims", [](const SampleCollection_t& sc) -> Size_t
					{return sc.nDims();},
				"Return the number of dimensions of the collection."
		)
		.def("setSampleTo", [](SampleCollection_t& sa, const Sample_t& s, const Size_t idx) -> void
					{
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Can not write an invalid sample to the container.");};
					  sa.setSampleTo(s, idx); //Size is enforced by the container
					  return;
					},
				"Load the sample s into the collection, it will become the ith sample."
				" The smaple must be valid and have the correct dimension.",
				py::arg("s"),
				py::arg("i")
		)
		.def("addNewSample", [](SampleCollection_t& sc, const Sample_t& s) -> void
					{
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Tried to add an invalid sample to the container.");};
					  sc.addNewSample(s); //Size is enforced by the container
					  return;
					},
				"Add the sample s at the end of the current collection.",
				py::arg("s")
		)
		.def("reserve", [](SampleCollection_t& sa, const Size_t newCapacity) -> void
					{sa.reserve(newCapacity); return;},
				"This function preallocate space, this allows faster inserting if the number of samples is known in advance."
				" This function does not have an effect if the size of the container is set, thus only empty container.",
				py::arg("newCapacity")
		)
		.def("getSample", [](const SampleCollection_t& sc, const Size_t i) -> Sample_t
					{return sc.getSample(i);},
				"Get a copy of the ith sample of the collection.",
				py::arg("i")
		)
		.def("resize", [](SampleCollection_t& sc, const Size_t newSize) -> void
					{sc.resize(newSize); return;},
				"Set the number of samples of the collection to newSize. This operation is only allowed if the collection is empty.",
				py::arg("newSize")
		)
		.def("clear", &SampleCollection_t::clear,
				"This function sets the size of the collection ot zero and deallocates all internal structures."
		)

		/*
		 * This function is for accessing the dimension
		 */
		.def("getDimensionArray", [](const SampleCollection_t& sc, const Size_t d) -> DimArrayEigen_t
					{
					  const Size_t N = sc.nSamples();
					  DimArrayEigen_t dimArray(N);	//Allocate space for the output
					  const yggdrasil::yggdrasil_dimensionArray_t& arr = sc.getDimensionArray(d);
					  for(Size_t i = 0; i != N; ++i)
					    {dimArray[i] = arr[i];};
					  return dimArray;
					},
				"This function returns a single component, dimension d, but of all samples."
				" The order inside the returned Eigen vector is the same order as would accessing be.",
				py::arg("d")
		)

		.def("getMatrix", [](const SampleCollection_t& sc, const eMatrixViewRow view) -> Matrix_t
					{ Matrix_t m = sc.getRowMatrix(view); return m;},
				"This function transforms the sample container into a matrix."
				" How the matrix is interpreted depends on the view parameter."
				" If set to Col each colum conatins one sample, this is also the most efficient choice."
				" If set to Row, each row will contain one sample.",
				py::arg("view")
		)

		.def("setSampleComponent", [](SampleCollection_t& sl,  const Size_t i,  const Size_t j,  const Numeric_t v) -> void
					{ sl.setSampleComponent(i, j, v); return;},
				"This function allows to set a single component of a single sample to a specifiv value v."
				" This function is equivalent in first loading the ith sample."
				" Then setting its jth component to v and then store it back in the container."
				" But this does not need to load the sample from teh container since it happens internaly.",
				py::arg("i"),
				py::arg("j"),
				py::arg("v")
		)
		.def("getSampleComponent", [](const SampleCollection_t& sl,  const Size_t i,  const Size_t j) -> Numeric_t
					{ return sl.getSampleComponent(i, j); },
				"This function returns the value of the jth component of the ith sample.",
				py::arg("i"),
				py::arg("j")
		)
		.def("setDimensionTo", [](SampleCollection_t& sl,  const Size_t j,  const Numeric_t v) -> void
					{ sl.setDimensionTo(j, v); return; },
				"This function will set the value of the jth component of _all_ samples to v."
				" This function is like calling the setSampleComponent() function for each sample, but more efficiently."
				" The value is the same for all samples.",
				py::arg("j"),
				py::arg("v")
		)
		.def("setDimensionArray", [](SampleCollection_t& sl,  const Size_t j,  const ::yggdrasil::yggdrasil_eigenRef_t<const PyVector_t>& dArr) -> void
					{ sl.setDimensionArray(j, dArr); return; },
				"This function copies the values of the array given as dArr into the component j."
				" This fucntion allows to set the value of the sample in dimension j to an individual value.",
				py::arg("j"),
				py::arg("dArr")
		)
	; //End of sample collection


	return;
}; //End: wrapperint sample container













