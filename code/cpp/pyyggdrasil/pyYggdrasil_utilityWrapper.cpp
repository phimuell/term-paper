/**
 * \brief	This fucntion implements the wrapping of the utility functions.
 *
 */

//Include the yggdrasil code that is needed
#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_treeDepthMap.hpp>
#include <util/yggdrasil_util.hpp>

#include <core/yggdrasil_eigen.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_sampleList.hpp>

#include <tree_geni/yggdrasil_multiDimCondition.hpp>

//Include the bindings
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

// Include Eigen
#include <Eigen/Core>

//Make a namespace alias
namespace py = pybind11;


void
pyYggdrasil_utilWrapper(
	py::module&	m)
{
	/*
	 * =======================
	 * USINGS
	 */
	using Int_t 		 = ::yggdrasil::Int_t;
	using Numeric_t 	 = ::yggdrasil::Numeric_t;
	using Size_t 		 = ::yggdrasil::Size_t;
	using IntervalBound_t 	 = ::yggdrasil::yggdrasil_intervalBound_t;
	using HyperCube_t 	 = ::yggdrasil::yggdrasil_hyperCube_t;
	using Sample_t 		 = HyperCube_t::Sample_t;
	using SampleArray_t 	 = ::yggdrasil::yggdrasil_arraySample_t;
	using SampleCollection_t = ::yggdrasil::yggdrasil_sampleCollection_t;
	using SampleList_t	 = ::yggdrasil::yggdrasil_sampleList_t;
	using DepthMap_t 	 = ::yggdrasil::yggdrasil_treeDepthMap_t;
	using EigenVector_t 	 = Eigen::VectorXd;
	using MultiCondition_t	 = ::yggdrasil::yggdrasil_multiCondition_t;


	/*
	 * =====================
	 * Interval
	 */
	py::class_<IntervalBound_t>(m, "Interval",
		"This class models the concept of an interval."
		" It is used throughtout Yggdrasil if at some place in the documentation an interval is mentioned, such an interval is ment."
		" It is a mathematically right open interval, this means that the right end point is not included in the interval."
		)
		.def(py::init<Numeric_t, Numeric_t>(),
				"Construct an interval, notice the interval is roight open.",
				py::arg_v("lower", 0.0, "The lower bound of the interval, is included; defaulted to 0.0"),
				py::arg_v("upper", 1.0, "The upper bound of the interval, is not included; defaulted to 1.0")
		)
		.def(py::init<::std::pair<Numeric_t, Numeric_t> >(),
				"Construct an interval from a pair, first is interpreted as the lower and second as upper bound.",
				py::arg("p")
		)
		.def(py::init<IntervalBound_t>(),
				"Construct a new interval by deep coping src.",
				py::arg("src")
		)

		/*
		 * Operators
		 */
		.def("__repr__", [](const IntervalBound_t& i) -> std::string
					{
					  if(i.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("The interval is invalid, can not print it.");};
					  return i.print(true);
					},
				"Textual representation of the interval."
		)
		.def("__str__", [](const IntervalBound_t& i) -> std::string
					{
					  if(i.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("The interval is invalid, can not print it.");};
					  return i.print(true);
					},
				"Textual representation of the interval."
		)


		/*
		 * Other functions
		 */
		.def("isValid", &IntervalBound_t::isValid,
				"Cehck if the interval is valid."
				" Generally speaking an interval is valid, if it does not have any funny values."
		)
		.def("getLength", [](const IntervalBound_t& i) -> Numeric_t
					{
					  if(i.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Tried to get the length of an invalid domain.");};
					  return i.getLength();
					},
				"Return the lenght of the interval."
				" This is the upper minus the lower bound. Can be zero."
		)
		.def("lower", [](const IntervalBound_t& i) -> Numeric_t
					{
					  if(i.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an invalid interval.");};
					  return i.lower();
					},
				"Get the lower bound of the interval."
		)
		.def("upper", [](const IntervalBound_t& i) -> Numeric_t
					{
					  if(i.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an invalid interval.");};
					  return i.upper();
					},
				"Get the upper bound of the interval."
		)
		.def("isInside", [](const IntervalBound_t& i, const Numeric_t x) -> bool  //Not we only support the scallar one
					{
					  if(i.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an invalid interval.");};
					  return i.isInside(x);
					},
				"Thest if the given value x is inside the interval, this test $lower <= x < upper$.",
				py::arg("x")
		)
		.def("isCloseToUpperBound", [](const IntervalBound_t& i, const Numeric_t x, const Numeric_t n) -> bool
					{
					  if(i.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an invalid interval.");};
					  return i.isCloseToUpperBound(x, n);
					},
				"Test if the value x is close to the upper bound."
				" It checks if abs(x - upper) < n * e, where e is the machine precision and n is a safty factor.",
				py::arg("x"),
				py::arg_v("n", 100.0, "The safty factor that is used.")
		)

		/*
		 * Wrapping fucntions
		 */
		.def("mapFromUnit", [](const IntervalBound_t& i, const Numeric_t x) -> Numeric_t
					{ return i.mapFromUnit(x);},
				"This function mapps the value x, that is assumed to lie in the unit interval to the interval described by *this."
				" It basically executes $(b - a) \\cdot x + a$, where $a$ is the lower and $b$ the upper bound of the interval.",
				py::arg("x")
		)
		.def("contractToUnit", [](const IntervalBound_t& i, const Numeric_t x) -> Numeric_t
					{return i.contractToUnit(x);},
				"This function performs the inversion of the mapFromUnit() function.",
				py::arg("x")
		)
	; //End wrapper interval


	/*
	 * =================
	 * Hypercube
	 */
	py::class_<HyperCube_t>(m, "HyperCube",
		"This class models a hyper cube of dimension n."
		" A hypercupe is the composition of n intervals."
		" It offers similary functions as the interval, but extended to many dimensions."
		" It is used to describe a portion of space."
		" Intervals are technically always valid, however for several reasons it is possible to create an invbalid interval."
		)
		/*
		 * Constructors
		 */
		.def(py::init([](const Size_t n) -> HyperCube_t
					{
					   if(n == 0){throw YGGDRASIL_EXCEPT_InvArg("In python you can not construct a hypercube with dimension zero.");};
					   HyperCube_t hc(n, IntervalBound_t::CREAT_UNIT_INTERVAL());
					   return hc;
					}),
				"Creates an n dimensional unit hyper cube."
				" Note that this is different with respect to C++, where this constructor constructs an n dimensiona hyper cube with n dimension, where all intervals are invalid.",
				py::arg("n")
		)
		.def(py::init([](const Size_t n, const IntervalBound_t& i) -> HyperCube_t
					{
					   if(n == 0) {throw YGGDRASIL_EXCEPT_InvArg("You can not construct a hypercube with zero dimensions.");};
					   if(i.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Can not a hyper cube with an invalid interval.");};
					   HyperCube_t hc(n, i);
					   return hc;
					}),
				"Creates an n dimensional hypercube, the underling interval is the given interval interval."
				" Note that n must be greater than zero and the interval must be valid."
				" This is a difference to C++.",
				py::arg("n"),
				py::arg("interval")
		)
		.def(py::init([](const std::vector<IntervalBound_t>& b) -> HyperCube_t
					{
					  if(b.empty()) {throw YGGDRASIL_EXCEPT_InvArg("The passed list had not any bounds.");};
					  HyperCube_t hc(b); yggdrasil_assert(hc.isValid());
					  return hc;
					}),
				"This function creats a hyper cube out of the intervals that are given by the list bounds.",
				py::arg("bounds")
		)
		.def(py::init([](const HyperCube_t& src) -> HyperCube_t
					{
					  if(src.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Tried to copy an invalid hyper cube.");};
					  HyperCube_t dolly(src);
					  return src;
					}),
				"Construct a new hyper cube by deep coping the cube passed as src.",
				py::arg("src")
		)

		/*
		 * Operators
		 */
		/*
		 * I have removed the iterator support since it allows some
		 * strange effects that are not nice and nobody will understand.
		 *
		 * The main cause is that an assignment to an interval does not
		 * produce an error, but the interval is not assined.
		 *
		 * I realy had to implement it, since othervise the method
		 * would be generated automatically, maybe because the cube provides
		 * begin and end.
		 */
		.def("__iter__", [](const HyperCube_t& hc)
					{ throw py::stop_iteration("This calls does not support iterators.");},
				py::keep_alive<0, 1>() /* Essential: keep object alive while iterator exists */,
				"Allowing iterate over the different intervals that composing the hypercube."
				" Due to some problems this feature is disabled."
				" Tring to iterate will reŝult in an exception."
		)
		.def("__len__", [](const HyperCube_t& hc) -> Size_t
					{return hc.nDims();},	//This function is not restricted to valid cubes, because it is needed to create custom cubes
				"Returns the number of dimension of the hypercube."
		)
		.def("__str__", [](const HyperCube_t& hc) -> std::string
					{
					  if(hc.isValid() == false) {throw YGGDRASIL_EXCEPT_LOGIC("Can not print an invalid hypercube.");};
					  return hc.print(true);
					},
				"Converts the hypercube into a textual representation."
		)
		.def("__repr__", [](const HyperCube_t& hc) -> std::string
					{
					  return hc.print(true);
					  if(hc.isValid() == false) {throw YGGDRASIL_EXCEPT_LOGIC("Can not print an invalid hypercube.");};
					},
				"Converts the hypercube into a textual representation."
		)
		.def("__getitem__", [](const HyperCube_t& hc, const Size_t d) -> IntervalBound_t
					{
					  IntervalBound_t i(hc.at(d));
					  if(i.isValid() == false){throw YGGDRASIL_EXCEPT_InvArg("Tried to access a dimension of the hyper cube, which is invalid.");};
					  return i;
					},
				"Returns a copy of the interval that forms the dimension d."
				" If the interval of the dimensions happens to be invalid, an exception is generated.",
				py::arg("d")
		)
		.def("__contains__", [](const HyperCube_t& hc, const Sample_t& s) -> bool
					{
					  if(hc.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Tried to access an invalid hyper cube.");};
					  if(s.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("Can not test an invalid sample.");};
					  return hc.isInside(s);
					},
				"This function test if the sample lies inside the domain that is described by *this."
				" This function is a short hand of the isInside() function."
		)


		/*
		 * Other functions
		 */
		.def("setInvalidDimension", [](HyperCube_t& hc, const Size_t d, const IntervalBound_t& i) -> void
					{
					  yggdrasil_assert(i.isValid());
					  hc.setIntervalTo(d, i);
					  yggdrasil_assert(hc.isValid(d));
					  return;
					},
				"Set the interval of dimension d to the supplied interval interv."
				" The ionterval of that dimension must be invalid.",
				py::arg("d"),
				py::arg("interv")
		)
		.def("isValid", [](const HyperCube_t&  hc) -> bool
					{return hc.isValid();},
				"Test if all dimensions of the hypercube are valid."
		)
		.def("isValid", [](const HyperCube_t&  hc, const Size_t  d) -> bool
					{return hc.isValid(d);},
				"Test if the interval of dimension d is valid."
				" Note that because of the definition of an invalid cube, it could also be the case that each dimension on its own is valid, but the cube as a whole is not."
				" This is a very rare case, but could in principe happens.",
				py::arg("d")
		)
		.def("getVol", [](const HyperCube_t&  hc) -> Numeric_t
					{
					  if(hc.isValid() == false) {throw YGGDRASIL_EXCEPT_LOGIC("Can not print an invalid hypercube.");};
					  return hc.getVol();
					},
				"Get the volume that is occupied by the hypercube."
		)
		.def("size", &HyperCube_t::size,
				"Get the number of dimensions of the cube."
		)
		.def("nDims", &HyperCube_t::nDims,
				"Get the number of dimensions of the cube."
		)
		.def("lower", [](const HyperCube_t& hc,  const Size_t d) -> Numeric_t
					{
					  if(hc.isValid() == false) {throw YGGDRASIL_EXCEPT_LOGIC("Can not print an invalid hypercube.");};
					  return hc.getLowerBound(d);
					},
				"Get the lower bound of dimension d.",
				py::arg("d")
		)
		.def("upper", [](const HyperCube_t& hc,  const Size_t d) -> Numeric_t
					{
					  if(hc.isValid() == false) {throw YGGDRASIL_EXCEPT_LOGIC("Can not print an invalid hypercube.");};
					  return hc.getUpperBound(d);
					},
				"Get the upper bound of dimension d.",
				py::arg("d")
		)

		/*
		 * Inside functions
		 */
		.def("isInside", [](const HyperCube_t&  hc, const Sample_t&  s) -> bool
					{
					  if(hc.isValid() == false) {throw YGGDRASIL_EXCEPT_LOGIC("Can not print an invalid hypercube.");};
					  if(s.isValid() == false)  {throw YGGDRASIL_EXCEPT_InvArg("The sample that was passed to the cube is invalid.");};
					  return hc.isInside(s);
					},
				"Test if the sample s is inside of the cube. The interval rules are used.",
				py::arg("s")
		)
		.def("isInside", [](const HyperCube_t& hc,  const SampleCollection_t& samples) -> bool
					{
					  if(hc.isValid() == false) {throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an invalid hyper cube.");};
					  return hc.isInside(samples);
					},
				"Test if all samples in the sample collection samples are inside *this.",
				py::arg("samples")
		)
		.def("isInside", [](const HyperCube_t& hc,  const SampleArray_t& samples) -> bool
					{
					  if(hc.isValid() == false) {throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an invalid hyper cube.");};
					  return hc.isInside(samples);
					},
				"Test if all samples in the sample array samples are inside *this.",
				py::arg("samples")
		)
		.def("isInside", [](const HyperCube_t& hc,  const SampleList_t& samples) -> bool
					{
					  if(hc.isValid() == false) {throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an invalid hyper cube.");};
					  return hc.isInside(samples);
					},
				"Test if all samples in the sample list samples are inside *this.",
				py::arg("samples")
		)
		.def("intersectsWithCondition", [](const HyperCube_t& hc,  const MultiCondition_t& mc) -> bool
					{
					  if(hc.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("The hyper cube is not valid.");};
					  if(mc.isValid() == false) {throw YGGDRASIL_EXCEPT_InvArg("The multi condition is not valid.");};
					  return hc.intersectsWithCondition(mc);
					},
				"This function tests if the *this, the hyper cube, meet the conditions that are described by the multicondition object condi.",
				py::arg("condi")
		)

		/*
		 * Mapping fucntion
		 */
		.def("mapFromUnit", [](const HyperCube_t& hc, const Sample_t& s) -> Sample_t
					{ return hc.mapFromUnit(s);},
				"This function assumes that the sample s is scalled such that it is inside a unit cube."
				" This function then computes where it is inside *this."
				" This function performs the transformation $(b_i - a_i) \\cdot s_i + a_i$, where $a_i$ is the lower and $b_i$ the upper bound of the interval of dimension $i$."
				" This function can be used map a sample that exists in the recalled NODE data space back to the rescalled root data space.",
				py::arg("s")
		)
		.def("contractToUnit", [](const HyperCube_t& hc, const Sample_t& s) -> Sample_t
					{return hc.contractToUnit(s);},
				"This function contarcts the sample s to the unit cube."
				" This fucntion is basically the inverse function to the mapFromUnit(s)."
				" This function can for example be used to map a sample from the rescalled root data space back to the original data sapce.",
				py::arg("s")
		)
	; //End wrapper hypercube

	/*
	 * This is a helper fucntion for creating an invalid hyper cube.
	 */
		m.def("CREAT_INVALID_CUBE", [](const Size_t  d) -> HyperCube_t
					{ return HyperCube_t::CREAT_INVALID_CUBE(d);},
				"This fucntion creates an invalid hyper cube of dimension d."
				" The dimension can also be zero, which is the default."
				" This is the only possiblility in Python to obtain an invalid hyper cube.",
				py::arg_v("d", 0, "This is the dimension of the cube that should be generated.")
		);


	/*
	 * =======================
	 * Depth map
	 */
	py::class_<DepthMap_t>(m, "DepthMap",
		"This is a depth map."
		" It stores the structore of the leafs of a tree."
		" It is proimarly presented for inspection prupose."
		)
		/*
		 * Constructors
		 */
		.def(py::init<>(),
				"Construct an empty depth map."
		)


		/*
		 * Operators
		 */
		.def("__len__", &DepthMap_t::nDiffDepth,
				"Get the number of different depths"
		)
		.def("__getitem__", [](const DepthMap_t&  dm, const Size_t  d) -> Size_t
					{return dm.getDepthCount(d);},
				"Get the number of leafs that have a depth of d.",
				py::arg("d")
		)
#if 1
		/*
		 * It compiles but it does not run.
		 * So it is not supported.
		 */
		.def("getDict", [](const DepthMap_t& dm) -> py::dict
					{
					  py::dict d;
					  auto ende = dm.end();
					  for(auto it = dm.begin(); it != ende; ++it)
					  {
					    const auto keyStr = std::to_string(it->first);
					    const auto valStr = std::to_string(it->second);
					    d[py::str(keyStr.c_str())] = py::str(valStr.c_str());
					  }; //ENd for(it):
					  return d;
					},
				"Convert the depth map into a dict. The keys are the different sizes and the values are the number of times such a depth was observed."
		)
#endif

		/*
		 * We explicitly forbid the iteration
		 */
		.def("__iter__", [](const DepthMap_t& dm)
					{ throw py::stop_iteration("This calls does not support iterators. Transform this into a dict if you need the iteration.");},
				py::keep_alive<0, 1>() /* Essential: keep object alive while iterator exists */,
				"Not implemented function."
		)
		.def("__conatins__", [](const DepthMap_t& dm, const Size_t depth) -> bool
					{return dm.getDepthCount(depth) != 0;},
				"Test if the depth map has a leaf at depth depth.",
				py::arg("depth")
		)


		/*
		 * Normal function
		 */
		.def("getDepthCount", &DepthMap_t::getDepthCount,
				"Return the number of leafs that have a depth of depth.",
				py::arg("depth")
		)
		.def("getLeafCount", &DepthMap_t::getLeafCount,
				"Return the number of different leafs that are recorded."
		)
		.def("isEmpty", [](const DepthMap_t& dm) -> bool
					{ return (dm.nDiffDepth() == 0) ? true : false;},
				"Test if the depth map is empty."
		)
		.def("getDepthMean", &DepthMap_t::getDepthMean,
				"Get the mean of the depth of all known leafs."
		)
		.def("getDepthStd", &DepthMap_t::getDepthStd,
				"Get the standard deviation of the depths of the depth map."
		)
		.def("getDepthMax", &DepthMap_t::getMaxDepth,
				"Returns the depth of the deepest leaf"
		)
		.def("size", &DepthMap_t::nDiffDepth,
				"Get the number of different depths."
		)
		.def("nDiffDepth", &DepthMap_t::nDiffDepth,
				"Get the number of distincts depths that where observed."
		)
		.def("getDiffDepth", &DepthMap_t::getDiffDepth,
				"Get a list (vector) that contains all depths that were observed."
				" If the entries of this list are used as keys to __getitem__ a non zero value is returned."
		)
		.def("getIthDepth", &DepthMap_t::getIthDepth,
				"This returns teh ith depth that was observed."
				" The result is a poijnter, first is the depth that was observed"
				" and second is the number of times the depth was observed.",
				py::arg("i")
		)
		.def("setDepthSequence", [](const DepthMap_t& dm) -> EigenVector_t
					{
					  EigenVector_t ser(dm.getLeafCount());
					  Size_t c = 0;
					  for(auto it = dm.begin(); it != dm.end(); ++it)
					  {
					    const auto Depth = it->first; const auto dCount = it->second;
					    for(Size_t i = 0; i != dCount; ++i) {ser[c++] = Depth;};
					  };
					  return ser;
					},
				"This fucntion returns a serialized version of *this."
				" It returns an Eigen vector of the length getLeafCount()."
				" The returned sequence is equivalent, altought in different order, that generated *this."
		)
	; //End of depth map wrapper


	/*
	 * =====================
	 * Free functions
	 */
	m.def("generateGrid", [](const HyperCube_t& domain, const Size_t p1, const Size_t p2, const bool woLowest) -> SampleArray_t
				{return yggdrasil_generateGrid(domain, p1, p2, woLowest);},
			"This function generates a gird of the domain."
			" Currently only grids up to two dimensions are supported."
			" As argument this fucntion takes a domain and a nuber of points."
			" If the domain has two dimension and the second number of points is not specified, then the same as the first one is used."
			" With teh optional value woLowwest, the lowest value, which is the same as the lowwer boundery is not included.",
			py::arg("domain"),
			py::arg("p1"),
			py::arg_v("p2", 0, "The number of points in teh second dimension. if needed and not specified, zero, then the value of the firstdimension, p1, is used."),
			py::arg_v("woLowest", false, "Exclude the lowest grid point.")
	); //End generate grid



	return;
}; //End: wrapperint utility function








