/**
 * \brief	This fucntion implements the wrapping of the tree helper functionality.
 *
 */

//Include the yggdrasil code that is needed
#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

//Include the bindings
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

//Make a namespace alias
namespace py = pybind11;


void
pyYggdrasil_treeHelperWrapper(
	py::module&	m)
{
	/*
	 * =======================
	 * USINGS
	 */
	using Int_t 		 = ::yggdrasil::Int_t;
	using Numeric_t 	 = ::yggdrasil::Numeric_t;
	using Size_t 		 = ::yggdrasil::Size_t;
	using IntervalBound_t 	 = ::yggdrasil::yggdrasil_intervalBound_t;
	using HyperCube_t 	 = ::yggdrasil::yggdrasil_hyperCube_t;
	using Sample_t 		 = HyperCube_t::Sample_t;
	using DepthMap_t 	 = ::yggdrasil::yggdrasil_treeDepthMap_t;
	using Tree_t 		 = ::yggdrasil::yggdrasil_DETree_t;
	using Tree_ptr 		 = ::std::unique_ptr<Tree_t>;
	using ValueArray_t 	 = Tree_t::ValueArray_t;
	using PDFValueArray_t 	 = Tree_t::PDFValueArray_t;
	using NodeFacade_t 	 = ::yggdrasil::yggdrasil_nodeFacade_t;
	using LeafIterator_t 	 = ::yggdrasil::yggdrasil_leafIterator_t;
	using Builder_t 	 = ::yggdrasil::yggdrasil_treeBuilder_t;
	using SampleCollection_t = ::yggdrasil::yggdrasil_sampleCollection_t;
	using SampleArray_t      = ::yggdrasil::yggdrasil_arraySample_t;
	using SampleList_t 	 = ::yggdrasil::yggdrasil_sampleList_t;


	/*
	 * ================
	 * Node facade
	 *
	 * The node facade models a constant reference to a node.
	 * Technically the node it wrapps can be of any type.
	 * But on the python level, some designe choices
	 * restrict this access to leafs.
	 *
	 * As a side node user code will only see non leaf facades if
	 * something like a model or test is implemented.
	 * And the update functionality is also implemented.
	 */
#define ENFORCE_LEAF(nf) {const bool res = nf.isLeaf(); if(res == false){throw YGGDRASIL_EXCEPT_RUNTIME("A facade that points to a non leaf wad encountered.");}}

	py::class_<NodeFacade_t>(m, "NodeFacade",
		"This is a node facade, this is abasically a proxy that exposes the internal structure of the tree to the user in a controlled mannor."
		" Its main goal is to keep the amount of code that has to be included by user codse to a minimum."
		" It also serves as a way of restricting the access to the internal structure of the tree."
		" It serves as the stable interface of to the tree."
		" In principle this class models a constant pointer to a node, with a restricted sets of fucntions."
		" It is important that this class only allow read access to the node that is wrapped."
		)
		/*
		 * Constructors
		 */
		//None since they models consatnt references

		/*
		 * Accessors
		 */
		.def("isLeaf", [](const NodeFacade_t& nf) -> bool
					{const bool res = nf.isLeaf(); if(res == false){throw YGGDRASIL_EXCEPT_RUNTIME("A facade that points to a non leaf wad encountered.");} return res;},
				"This function returns true if the facade is leaf."
				" This is technically allways the case so this funtion returns true or throws an exception."
		)


		/*
		 * Sample access
		 */
		.def("getSampleCollection", [](const NodeFacade_t& nf) -> SampleCollection_t
					{ENFORCE_LEAF(nf); SampleCollection_t sc(nf.getCSampleCollection()); return sc;},
				"This function returns a copy of the underling sample collection."
				" It is important that this involves a copy of the samples, which meight be expensive."
				" It is also important that the samples that are returned by this function are expressed in the RESCALLED NODE DATA SPACE, thus they are scalled such that they lie inside the unit cube."
		)
		.def("getSample", [](const NodeFacade_t& nf, const Size_t i) -> Sample_t
					{ENFORCE_LEAF(nf); Sample_t s(nf.getCSampleCollection().getSample(i)); return s;},
				"This function returns a copy of the ith sample of the underling conllection."
				" It is important that the sample is a copy and is rescalled such that it lies in the unit hyper cube.",
				py::arg("i")
		)
		.def("nSamples", [](const NodeFacade_t& nf) -> Size_t
					{ENFORCE_LEAF(nf); return nf.nSamples();},
				"This function returns the number of samples that are stored inside the underling collection."
		)
		.def("nDims", [](const NodeFacade_t& nf) -> Size_t
					{ENFORCE_LEAF(nf); return nf.nDims();},
				"This fuction returns the number of diemnsions the underling sample has."
		)
		.def("getMass", [](const NodeFacade_t& nf) -> Size_t
					{ENFORCE_LEAF(nf); return nf.getMass();},
				"This function returns the mass of the node."
				" Which is defined as the number of sample beneith the node."
				" Since we operate on leafs thsi si the same as the number of samples."
		)


		/*
		 * Other Informations
		 */
		.def("getDomain", [](const NodeFacade_t& nf) -> HyperCube_t
					{ENFORCE_LEAF(nf); HyperCube_t hc(nf.getDomain()); return hc;},
				"This fucntion returns a copy of the hyper cube that stores the NODE DATA SPACE of *this."
				" This is the fraction of space that is occupied by the current node, realtive to the root data space, which is equivalent to the unit cube."
		)
	; //End wrapping: node facade
#undef ENFORCE_LEAF


	/*
	 * ======================
	 * Node iterator.
	 * Is provided for accessing thge depth and performing mnual iteration
	 */
	py::class_<LeafIterator_t>(m, "LeafIterator",
		"This class offers a leaf iterator."
		" It can be constructed from any node, since pyYggdrasil, restricts the access to nodes, it is only possible to optain one from the root node."
		" This means it only allows to iterate over all nodes and just a special subset."
		" The order in which the leafs are visited are unspecific."
		" It is alo important that this class only allow read access, thus it models a constant iterator."
		)
		/*
		 * Constructors
		 * Are not needed, this is handled by the tree."
		 */

		/*
		 * Other fucntion
		 */
		.def("access", [](const LeafIterator_t& it) -> NodeFacade_t
					{if(it.isEnd() == true) { throw YGGDRASIL_EXCEPT_RUNTIME("Tried to access an invalid leaf iterator.");} return it.access();},
				"This function returns a node facade of the node that is currently at the head location of the iterator."
				" As it was outlined in the section about the node facade, they only allow read access."
		)
		.def("isEnd", [](const LeafIterator_t& it) -> bool
					{return it.isEnd();},
				"This function returns true if the iterator has reaced the end."
				" The node can not be further incremented nor accessed."
		)
		.def("getDepth", [](const LeafIterator_t& it) -> Size_t
					{return it.getDepth();},
				"This fucntion returns the depth of the leaf."
		)
		.def("nextLeaf", [](LeafIterator_t& it) -> void
					{yggdrasil_assert(it.isEnd() == false); it.nextLeaf(); return;},
				"This function advances the iterator to the next leaf."
		)
	; //End wrapper of leaf iterator


	return;
}; //End: wrapperint utility function








