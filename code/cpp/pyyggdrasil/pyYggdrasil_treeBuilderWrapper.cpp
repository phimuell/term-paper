/**
 * \brief	This fucntion implements the wrapping of the tree builder fucntionality.
 *
 */

//Include the yggdrasil code that is needed
#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>
#include <factory/yggdrasil_treeBuilder_Enums.hpp>
#include <factory/yggdrasil_treeBuilder.hpp>

//Include the bindings
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>

//Make a namespace alias
namespace py = pybind11;


void
pyYggdrasil_treeBuilderWrapper(
	py::module&	m)
{
	/*
	 * =======================
	 * USINGS
	 */
	using Int_t 		 = ::yggdrasil::Int_t;
	using Numeric_t 	 = ::yggdrasil::Numeric_t;
	using Size_t 		 = ::yggdrasil::Size_t;
	using Builder_t 	 = ::yggdrasil::yggdrasil_treeBuilder_t;

	using ::yggdrasil::eParModel;
	using ::yggdrasil::eGOFTester;
	using ::yggdrasil::eIndepTester;

	/*
	 * =====================
	 * eParModel enum
	 */
	py::enum_<eParModel>(m, "eParModel",
		"This enum is used to encode the model."
		)
		.value(
			"NoModel",
			eParModel::NoModel,
			"The null model, this is an illegal value, that does not represent any model."
		)
		.value(
			"LinModel",
			eParModel::LinModel,
			"The liner model."
		)
		.value(
			"ConstModel",
			eParModel::ConstModel,
			"The constant model."
		)
	; //End enum: eParModel

	/*
	 * The other two enums are not set,
	 * since they have only one, Chi2, valid value anyway.
	 */

	/*
	 * =====================
	 * Builder
	 */
	py::class_<Builder_t>(m, "TreeBuilder",
		"This class offers a easy and convenient way to set up a tree."
		" It does not construct the tree by itself, but it stores the options that should be used to construct it."
		" For that named fucntions that sets or unsets an options are provided."
		" This fucntions can be conveniently combined by chaining them together."
		" This class is a remanent from the C++ heritage of Yggdrasil."
		" It basic intention is to provide named arguments for the constructor."
		" It was keep and even made requiered on the Pyton side to be consistent."
		)
		/*
		 * Constructors
		 */
		.def(py::init<eParModel>(),
				"Constructs a builder with the given parameteric model.",
				py::arg("parModel")
		)
		.def(py::init<>(),
				"Construct a builder, which does not have a valid parameteric model."
		)


		/*
		 * Operators
		 */
		.def("__str__", &Builder_t::print,
				"Tranform this into a textual representation."
		)
		.def("__repr__", &Builder_t::print,
				"Transform the builder into a textual representation."
		)


		/*
		 * =================
		 * 	S E T T E R S
		 *
		 * These functions change the interbnal state of *this/self.
		 * They return a reference to themself.
		 */

		//
		//Parameterc model
		.def("setParModel", [](Builder_t&  b,   const eParModel&  parModel) -> Builder_t&
					{return b.setParModel(parModel);},
				"Set the parameteric model of the builder to parModel.",
				py::arg("parModel")
		)
		.def("useLinearModel", [](Builder_t&  b) -> Builder_t&
					{return b.useLinearModel();},
				"Set the parameteric model to the linear model."
		)
		.def("useConstantModel", [](Builder_t& b) -> Builder_t&
					{return b.useConstantModel();},
				"Set the parameteric model to the constant model."
		)

		//
		//GOF Tester
		.def("setGOFLevel", [](Builder_t& b, const Numeric_t sigLevel) -> Builder_t&
					{return b.setGOFLevel(sigLevel);},
				"Set the significance level of the gof tester.",
				py::arg("sigLevel")
		)

		//
		//Indep Tester
		.def("setIndepLevel", [](Builder_t& b,  const Numeric_t sigLevel) -> Builder_t&
					{return b.setIndepLevel(sigLevel);},
				"Set the significance level of the independence tester.",
				py::arg("sigLevel")
		)

		//
		//Splitter
		.def("useSizeSplitter", &Builder_t::useSizeSplitter,
				"Activate the size based splitter, which cuts the domain simply in half."
		)
		.def("useMedianSplitter", &Builder_t::useMedianSplitter,
				"Activate the median splitter, which splits the domain such, that each subdomain contains half of the sample."
		)
		.def("useScoreSplitter", &Builder_t::useScoreBasedSplitter,
				"This is an alias for the median splitter."
		)


		/*
		 * =================
		 * 	G E T T E R S
		 *
		 * This functions query the state of *this.
		 * They do not support method cahning, since theier return value
		 * indicates the result
		 */

		//
		//Par Model
		.def("getParModel", &Builder_t::getParModel,
				"This function returns the parameteric model that is currently active. The returned type is an enum."
		)
		.def("isLinearModel", &Builder_t::isLinearModel,
				"This fucntion returns true if the parametric model of the builder is the linear model."
		)
		.def("isConstantModel", &Builder_t::isConstantModel,
				"This function returns true if the constant model is selcted."
		)
		.def("hasParModel", &Builder_t::hasModel,
				"This fucntion returns true if a parameteric model is slected."
		)

		//
		//GOF Tester
		.def("getGOFLevel", &Builder_t::getGOFSigLevel,
				"This function returns the significance level of the GOF tester."
		)

		//
		//Independence tester
		.def("getIndepLevel", &Builder_t::getIndepSigLevel,
				"This function returns the significance level of the independence tester."
		)

		//
		//Splitter
		.def("isMedianSplitter", &Builder_t::isMedianSplitter,
				"This function returns true if the median splitter is selected."
		)
		.def("isSizeSplitter", &Builder_t::isSizeSplitter,
				"This fucntion returns true if the size splitter is selected."
		)
	; //End wrapper of tree builder


	return;
}; //End: wrapperint utility function








