/**
 * \brief	This fucntion implements the wrapping of the tree sampler.
 * 		 This is the random distribuition, that is able to sample from the tree.
 */

//Include the yggdrasil code that is needed
#include <random/yggdrasil_random.hpp>

#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>
#include <tree_geni/yggdrasil_treeSampler.hpp>

//Wrapper to the random number generator
#include <random/pyYggdrasil_randomNumberGenerator.hpp>


//Include Eigen
#include <core/yggdrasil_eigen.hpp>

//Include the bindings
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>


//Make a namespace alias
namespace py = pybind11;



void
pyYggdrasil_randomWrapper_treeSampler(
	py::module&	m)
{
	/*
	 * =======================
	 * USINGS
	 */
	using Int_t 		 = ::yggdrasil::Int_t;
	using Numeric_t 	 = ::yggdrasil::Numeric_t;
	using Size_t 		 = ::yggdrasil::Size_t;
	using Real_t 		 = ::yggdrasil::Real_t;
	using pRNGenerator_t 	 = ::yggdrasil::pyYggdrasil_pRNGenerator_t;
	using result_type 	 = pRNGenerator_t::result_type;

	using Tree_t 		 = ::yggdrasil::yggdrasil_DETree_t;
	using Tree_ptr 		 = ::std::unique_ptr<Tree_t>;
	using HyperCube_t 	 = Tree_t::HyperCube_t;

	using Sample_t 		 = Tree_t::Sample_t;
	using SampleCollection_t = ::yggdrasil::yggdrasil_sampleCollection_t;
	using SampleArray_t      = ::yggdrasil::yggdrasil_arraySample_t;
	using SampleList_t 	 = ::yggdrasil::yggdrasil_sampleList_t;

	using MultiCondition_t 	 = ::yggdrasil::yggdrasil_multiCondition_t;
	using MultiCondition_ptr = ::std::unique_ptr<MultiCondition_t>;
	using SingleCondition_t  = MultiCondition_t::SingleCondition_t;
	using ConditionVector_t  = MultiCondition_t::ConditionVector_t;
	using ConditionMap_t 	 = MultiCondition_t::ConditionMap_t;
	using DimensionList_t 	 = MultiCondition_t::DimensionList_t;

	using TreeSampler_t 	 = ::yggdrasil::yggdrasil_treeSampler_t;
	using TreeSampler_ptr 	 = ::std::unique_ptr<TreeSampler_t>;
	using PDFValueArrayEigen_t  = TreeSampler_t::PDFValueArrayEigen_t;	//This is teh Eigen tyope we use as a return value for the pdf values


	/*
	 * Single condition
	 */
	py::class_<SingleCondition_t>(m, "SingleCondition",
		"This class represents one condition."
		" A condition is pair of an integer, which represent the dimension where this condition should be applied to."
		" And a value, the value that should be used for that restriction."
		" This class is not used directly but is unsed as a building block, similar to the interval."
		)
		/*
		 * Constructors
		 */
		.def(py::init([](const Int_t dim, const Numeric_t val) -> SingleCondition_t
					{return SingleCondition_t(dim, val);}),
				"This is the building constructor, it takes a dimension, dim and the value that the conditin should have."
				" The value is interpreted on the original/global data space.",
				py::arg("dim"),
				py::arg("value")
		)

		/*
		 * Other functions
		 */
		.def("__str__", &SingleCondition_t::print,
				"This function produces a textual representation of this."
				" It outputs a string with (dim: val), where dim is the dimension and val is the value that is applied."
		)
		.def("__repr__", &SingleCondition_t::print,
				"Generates a textual represenation of this, is the same function as __str__."
				" It outputs a string with (dim: val), where dim is the dimension and val is the value that is applied."
		)

		/*
		 * Query
		 */
		.def("getDim", &SingleCondition_t::getConditionDim,
				"This fucntion returns the dimension in which the condition should be applied to."
		)
		.def("getValue", &SingleCondition_t::getConditionValue,
				"This function returns the value that the condition has."
		)

		.def("isValid", &SingleCondition_t::isValid,
				"This function returns true id *this is valid."
				" In Yggdrasil invalid conditions could occure, but they will not surface in pyYggdrasil."
				" This fucntion should return always true, if not am internal error has manifested."
		)
	; //End single condition


	/*
	 * ====================
	 * Multi Condition
	 */
	py::class_<MultiCondition_t>(m, "MultiCondition",
		"This class represents many conditions at once."
		" It can be seen as a list of SingleCondition objects."
		" It is needed to instruct the sampler to restrict certain dimensions."
		" There are several ways for constructing it."
		)
		/*
		 * Constructor
		 */
		.def(py::init([](const Int_t nDims) -> MultiCondition_ptr
					{return MultiCondition_ptr(new MultiCondition_t(nDims));}),
				"This function constructs a MultiCondition object that does not have any restrictions."
				" However it needs the dimension of the underling sample space to be passed to it."
				" This is needed for internal operations.",
				py::arg("nDims")
		)
		.def(py::init([](const Int_t nDims, const ConditionMap_t& cMap) -> MultiCondition_ptr
					{return MultiCondition_ptr(new MultiCondition_t(nDims, cMap));}),
				"This constructor constructs the MultiCondition from a map."
				" The map is interpreted that the key of a record is interpreted as the dimension in which the condition should be applied."
				" The associated value of the key is the value that is used for restriction."
				" The value is interpreted on the global/original data space."
				" Also the dimension of the underling sample space, nDims, has to be passed.",
				py::arg("nDims"),
				py::arg("cMap")
		)
		.def(py::init([](const Int_t nDims, const ConditionVector_t& cVector) -> MultiCondition_ptr
					{return MultiCondition_ptr(new MultiCondition_t(nDims, cVector));}),
				"This constructor constructs a MultiCondition out of a list of several SingleConditions."
				" The values are interpreted on the global/original data space."
				" Also the dimension of the underling sample space, nDims, has to be passed."
				" The order of the condition is irrelevant.",
				py::arg("nDims"),
				py::arg("cVec")
		)
		.def(py::init([](const MultiCondition_t& src) -> MultiCondition_ptr
					{return MultiCondition_ptr(new MultiCondition_t(src));}),
				"This constructor performs a copy construction, by deep coping src and stores it in *this.",
				py::arg("src")
		)


		/*
		 * Special functions
		 */
		.def("__str__", &MultiCondition_t::print,
				"This fucntion transforms the MultiCollection into a string."
				" The format is {COND1, ... | nDims}, where COND1 is the result of a print operation on the underling SingleCondition object."
				" nDims is the dimension of the underling sample sapce."
		)
		.def("__repr__", &MultiCondition_t::print,
				"This function transforms *this in a textual representation."
				" The format is the same as when using __str__."
		)


		/*
		 * Quering/Status fucntion
		 */
		.def("nDims", [](const MultiCondition_t& mt) -> Size_t
					{
					  if(mt.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not query an invalid multi condition.");};
					  return mt.nDims();
					},
				"Return the dimension of the underling sample space."
		)
		.def("noRestrictions", [](const MultiCondition_t& mt) -> bool
					{
					  if(mt.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an invalid multi condition.");};
					  return mt.noConditions();
					},
				"This function returns true if *this does not have any restriction."
				" It is an efficient test for nConditions == 0."
				" Note on C++ this fucntion is called noConditions(), but it was renamed on the interface to minimize confusion."
		)
		.def("nConditions", [](const MultiCondition_t& mt) -> Size_t
					{
					  if(mt.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an invalid multi condition.");};
					  return mt.nConditions();
					},
				"This function returns the number of constions that *this has."
				" Note that 0 indicates that no condition is in place."
		)
		.def("isValid", &MultiCondition_t::isValid,
				"This function returns true if *this is valid."
				" This should always be the case and if an invalid condition is found then there are trubles."
		)

		/*
		 * Condition access
		 */
		.def("hasConditionFor", [](const MultiCondition_t& mc, const Size_t d) -> bool
					{
					  if(mc.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an invalid multi condition.");};
					  return mc.hasConditionFor(d);
					},
				"The function returns true if *this has a condition that is applied to dimension dim."
				" Note that if dim is larger (or equal, since we have zero based indexing) an out of bound exception is generated.",
				py::arg("dim")
		)
		.def("getConditionFor", [](const MultiCondition_t& mc, const Size_t dim) -> SingleCondition_t
					{
					  if(mc.isValid() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an invalid multi condition.");};
					  const SingleCondition_t ret(mc.getConditionFor(dim));
					  if(ret.isValid() == false) {throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(dim) + " does not have a restriction that is applied to it.");};
					  return ret;
					},
				"This function returns the condition that is applied to dimension dim."
				" If this dimension does not have a condition that is applied to it and exception is generated."
		)

		/*
		 * Get the underlying conditions
		 */
		.def("getConditionVector", &MultiCondition_t::getConditionVector,
				"This function returns the underling conditions inside a vector."
				" The order is unspecific."
				" The condition is expressed in the original data space."
		)
		.def("getConditionMap", &MultiCondition_t::getConditionMap,
				"This function returns the underling conditions inside a map/dict."
				" The format is such that the keys are the dimensions of the conditions, and the associated values are the values of the restriction."
				" The conditions are expressed in tzhe original data space."
		)

		/*
		 * Get the dimensions
		 */
		.def("getFreeDimensions", [](const MultiCondition_t& mc) -> DimensionList_t
					{
					  if(mc.nDOF() == 0) {throw YGGDRASIL_EXCEPT_RUNTIME("The condition does not have a degree of freedom.");};
					  return mc.getFreeDimensions();
					},
				"This functions returns the free dimensions of the multi conditions."
				" The free dimensions are all dimensions where no restriction is applied to."
				" This means there are nDOF() many of them."
		)
		.def("getConditionedDimensions", [](const MultiCondition_t& mc) -> DimensionList_t
					{
					  return mc.getConditionedDimensions();
					},
				"This function returns the dimensions in which we apply conditions to."
				" The difference between this and the getConditionVector() is, that this function only returns the dimensions and not the values."
		)


		/*
		 * Restrict or expand a sample.
		 */
		.def("expandSample", [](const MultiCondition_t& mc,  const Sample_t& s) -> Sample_t
					{ return mc.expandSample(s); },
				"This function expands the (restricted) sample s to its full dimension and applies the condition to it."
				" The sample must have dimension nDOF(), the returned sample will have dimensin nDims().",
				py::arg("s")
		)
		.def("restrictSample", [](const MultiCondition_t& mc,  const Sample_t& s) -> Sample_t
					{ return mc.restrictSample(s); },
				"This function restricts the sample s, by removing all dimensions that are conditioned."
				" The sample must have dimension nDims() and the returned sample will have dimension nDOF().",
				py::arg("s")
		)

		.def("expandSample", [](const MultiCondition_t& mc,  const SampleList_t& s) -> SampleList_t
					{ return mc.expandSample(s); },
				"This function expands all (restricted) samples that are stored in the sample list s to their full dimension and applies the condition to them."
				" The samples in the sample list must have dimension nDOF(), the samples list that is returned will have dimension nDims().",
				py::arg("s")
		)
		.def("restrictSample", [](const MultiCondition_t& mc,  const SampleList_t& s) -> SampleList_t
					{ return mc.restrictSample(s); },
				"This function restricts all samples that are stored in the sample list s, by removing all dimensions that are conditioned."
				" The sample list must have dimension nDims(), the sample list that is returned will have dimension nDOF().",
				py::arg("s")
		)

		.def("expandSample", [](const MultiCondition_t& mc,  const SampleArray_t& s) -> SampleArray_t
					{ return mc.expandSample(s); },
				"This function expands all (restricted) samples that are stored in the sample array s to their full dimension and applies the condition to them."
				" The sample array must have dimension nDOF(), the returned sample array will have dimension nDims().",
				py::arg("s")
		)
		.def("restrictSample", [](const MultiCondition_t& mc,  const SampleArray_t& s) -> SampleArray_t
					{ return mc.restrictSample(s); },
				"This function restricts all samples that are stored in the sample array s, by removing all dimensions that are conditioned."
				" The sample array must have dimension nDims(), the returned sample array will have dimension nDOF().",
				py::arg("s")
		)

		.def("expandSample", [](const MultiCondition_t& mc,  const SampleCollection_t& s) -> SampleCollection_t
					{ return mc.expandSample(s); },
				"This function expands all (restricted) samples that are stored in the sample collection s to their full dimension and applies the condition to them."
				" The sample collection must have dimension nDOF(), the returned sample collection will have dimension nDims().",
				py::arg("s")
		)
		.def("restrictSample", [](const MultiCondition_t& mc,  const SampleCollection_t& s) -> SampleCollection_t
					{ return mc.restrictSample(s); },
				"This function restricts all samples that are stored in the sample collection s, by removing all dimensions that are conditioned."
				" The sample collection must have dimension nDims(), the returned sample collection will have dimension nDOF().",
				py::arg("s")
		)


		/*
		 * Apply the conditions
		 */
		.def("applyConditions", [](const MultiCondition_t& mc, Sample_t& s) -> void
					{(void)mc.applyConditions(s); return;},
				"This function applies the conditions of *this to s."
				" The values of the components of s that names restricted dimensions are set to the restricting values."
				" This function is equivalent to first restricting the sample and then expanding the result, but more efficient.",
				py::arg("s")
		)
		.def("applyConditions", [](const MultiCondition_t& mc, SampleList_t& s) -> void
					{(void)mc.applyConditions(s); return;},
				"This function applies the conditions of *this to all samples inside s."
				" The values of the components of the samples that names restricted dimensions are set to the restricting values."
				" This function is equivalent to first restricting the container and then expanding the result, but more efficient.",
				py::arg("s")
		)
		.def("applyConditions", [](const MultiCondition_t& mc, SampleArray_t& s) -> void
					{(void)mc.applyConditions(s); return;},
				"This function applies the conditions of *this to all samples inside s."
				" The values of the components of the samples that names restricted dimensions are set to the restricting values."
				" This function is equivalent to first restricting the container and then expanding the result, but more efficient.",
				py::arg("s")
		)
		.def("applyConditions", [](const MultiCondition_t& mc, SampleCollection_t& s) -> void
					{(void)mc.applyConditions(s); return;},
				"This function applies the conditions of *this to all samples inside s."
				" The values of the components of the samples that names restricted dimensions are set to the restricting values."
				" This function is equivalent to first restricting the container and then expanding the result, but more efficient.",
				py::arg("s")
		)
	; //End multicondition restriction


	/*
	 * ======================
	 * Tree Sampler
	 */
	py::class_<TreeSampler_t>(m, "TreeSampler",
		"This class allows the sampling from a valid fitted tree."
		" It processed the tree and copies the important/needed part, thus the tree is not needed to exist longer."
		" There are some limitations in the implementation."
		" For example the structure that is generated an stored inside *this is not suited to compute pdf values."
		" The pdf of samples that are generated by this can be computed as a byproduct of the sampling, but no pdf of a sample can be computed, for this the tree is needed."
		" Hower it supports the same sampling functions as the random distributions does."
		" Note that the probability that the generating functions returns are conditioned pdfs."
		" This means that the probability that is returned is not the same as the one computed by the tree, in the case of conditioned samples."
		)
		/*
		 * Constructors
		 */
		.def(py::init([](const Tree_t& tree) -> TreeSampler_ptr
					{return TreeSampler_ptr(new TreeSampler_t(tree));}),
				"This constructor will construct a sampler that is not restricted."
				" This means unconditional sampling is applied.",
				py::arg("tree")
		)
		.def(py::init([](const Tree_t& tree, const MultiCondition_t& mc) -> TreeSampler_ptr
					{return TreeSampler_ptr(new TreeSampler_t(tree, mc));}),
				"This constructor will build a tree sampler that is restricted as described by the MultiCondition condi."
				" This is the canonical way of generating a conditioned sampler.",
				py::arg("tree"),
				py::arg("condi")
		)
		.def(py::init([](const Tree_t& tree, const ConditionVector_t& cv) -> TreeSampler_ptr
					{return TreeSampler_ptr(new TreeSampler_t(tree, cv));}),
				"This will create a tree sampler out of a tree and a condition vector."
				" Internaly a MultiCondition is constructed using the supplied cVector and then the sampler is constructed."
				" This constructor is provided for convenience.",
				py::arg("tree"),
				py::arg("cVector")
		)
		.def(py::init([](const Tree_t& tree, const ConditionMap_t& cm) -> TreeSampler_ptr
					{return TreeSampler_ptr(new TreeSampler_t(tree, cm));}),
				"This will create a conditioned tree sampler."
				" The conditions are extracted from the provided map/dict in the same way as a MultiCondition would be constructed.",
				py::arg("tree"),
				py::arg("cMap")
		)
		.def(py::init([](const TreeSampler_t& ts) -> TreeSampler_ptr
					{return TreeSampler_ptr(new TreeSampler_t(ts));}),
				"This will construct a tree sampler by deep coping src.",
				py::arg("src")
		)

		/*
		 * Status functions
		 */
		.def("nDims", &TreeSampler_t::nDims,
				"This function returns the dimension of the underlying sample space."
		)
		.def("nConditions", [](const TreeSampler_t& ts) -> Size_t
					{
					  if(ts.notOverConstrained() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a tree sampler that is over constrained.");};
					  return ts.nConditions();
					},
				"This function returns the number of conditions that are used in the sampler."
		)
		.def("noRestrictions", [](const TreeSampler_t& ts) -> bool
					{
					  if(ts.notOverConstrained() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a tree sampler that is over constrained.");};
					  return ts.noConditions();
					},
				"This function returns true, the sampler does not have any conditions that are applied."
				" This is like testing if nConditions() == 0, but is a bit more efficient."
		)
		.def("notOverConstrained", &TreeSampler_t::notOverConstrained,
				"This fucntion returns true if at least some leaf in the tree was able to meet the condition that was given upon constructing."
				" This function is used to indicate that the sampler can be used."
				" If this function returns false, then some of the fucntion will genertae errors."
				" This fucntion is like testing nProxy() != 0, but more efficient."
		)
		.def("nProxy", [](const TreeSampler_t& ts) -> Size_t
					{
					  if(ts.notOverConstrained() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an over constraint tree sampler.");};
					  return ts.nProxies();
					},
				"This function returns the number of leafs that where able to meet the conditions."
				" The name Proxy comes from the C++ side and does not bear any meaning in pyYggdrasil."
		)
		.def("getProxyDomain", [](const TreeSampler_t& ts, const Size_t i) -> HyperCube_t
					{
					const auto& pI = ts[i];	//Will throw if out of bound
					return pI.getDomain();
					},
				"This function will return the hyper cube of the ith leaf."
				" There are two very important things here."
				" First the domain is relative to the root data space."
				" Second the order of the proxy is arbitraraly, but fix during the lifetime of *this."
				" The index ranges from 0 to nProxies() - 1.",
				py::arg("i")
		)
		.def("getInvPxC", [](const TreeSampler_t& ts) -> Real_t
					{
					  if(ts.notOverConstrained() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an over constarined tree sampler.");};
					  return ts.get_iPxc();
					},
				"This function will return the scalling constant of the probability."
				" This factor is used to transform unconditioned probability to conditioned probability."
		)

		/*
		 * Parameter access
		 */
		.def("getGlobalDataSpace", &TreeSampler_t::getGlobalDataSpace,
				"This fucntion returns the domain where the tree is defined on, also known as the global/original data space."
				" This variable was copied from the tree during the constuction."
		)
		.def("getConditions", [](const TreeSampler_t& ts) -> MultiCondition_t
					{
					  if(ts.notOverConstrained() == false) {throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an over constrained tree sampler.");};
					  return ts.getGlobalConditions();
					},
				"This fucntion returns the conditions that where used to select the leafes that enters the tree."
				" Note that this is not what was passed upon the construction to the sampler."
				" The conditions that were passed to the constructor, is mapped to the root data space and stored."
				" This function will first apply a transformation to get back to the original data space."
				" If *this is over constrained an error will be generated."
		)


		/*
		 * Sampling Functions
		 *
		 * The sampling functions are presented in two different ways.
		 * First with the pdf and second only the samples.
		 *
		 * They will throw if they are over constrained.
		 */

		/*
		 * Single sample
		 */
		.def("generateSamples", [](TreeSampler_t& ts, pRNGenerator_t& geni) -> Sample_t
					{return ts.generateSamples(geni);},
				"This function generates a single sample from the tree sampler."
				" geni is used to generate the needed randomness.",
				py::arg("g")
		)
		.def("generateSamplesPDF", [](TreeSampler_t& ts, pRNGenerator_t& geni) -> std::pair<Sample_t, Real_t>
					{return ts.generateSamplesPDF(geni);},
				"This function generates a pair."
				" first is the generated sample and second is the pdf of that sample."
				" The randomness is drawn from geni.",
				py::arg("g")
		)

		/*
		 * Sample Array
		 */
		.def("generateSamplesArray", [](TreeSampler_t& ts, pRNGenerator_t& geni, const Size_t N) -> SampleArray_t
					{return ts.generateSamplesArray(geni, N);},
				"This function generates N samples and store them in a SampleArray container."
				" The randomness is drawn from the geni object.",
				py::arg("g"),
				py::arg("N")
		)
		.def("generateSamplesArrayPDF", [](TreeSampler_t& ts, pRNGenerator_t& geni, const Size_t N) -> std::pair<SampleArray_t, PDFValueArrayEigen_t>
					{return ts.generateSamplesArrayPDF_Eigen(geni, N);},
				"This function generates N samples and also its associatd pdf values, they are stored in a pair."
				" The first element of the returned pair is a SampleArray, where the samples are stored."
				" The second element is an Eigen vector, that can be used like a NumPy array, that contains the pdf values."
				" The randomness is drawn from geni.",
				py::arg("g"),
				py::arg("N")
		)

		/*
		 * Sample List
		 */
		.def("generateSamplesList", [](TreeSampler_t& ts, pRNGenerator_t& geni, const Size_t N) -> SampleList_t
					{return ts.generateSamplesList(geni, N);},
				"This function generates N samples and stores them in a SampleList object."
				" The randomness generated by geni.",
				py::arg("g"),
				py::arg("N")
		)
		.def("generateSamplesListPDF", [](TreeSampler_t& ts, pRNGenerator_t& geni, const Size_t N) -> std::pair<SampleList_t, PDFValueArrayEigen_t>
					{return ts.generateSamplesListPDF_Eigen(geni, N);},
				"This function generates N samples and their associated pdf values."
				" The fucntion returns a pair, its first element is a SampleList that stores the generated samples."
				" The second element is a NumPy vector with the pdf values."
				" The randomness that is needed is generated by geni.",
				py::arg("g"),
				py::arg("N")
		)

		/*
		 * Sample Collection
		 */
		.def("generateSamplesCollection", [](TreeSampler_t& ts, pRNGenerator_t& geni, const Size_t N) -> SampleCollection_t
					{return ts.generateSamplesCollection(geni, N);},
				"This function generates N samples and stores them inside a SampleCollection."
				" The randomness is generated by geni.",
				py::arg("g"),
				py::arg("N")
		)
		.def("generateSamplesCollectionPDF", [](TreeSampler_t& ts, pRNGenerator_t& geni, const Size_t N) -> std::pair<SampleCollection_t, PDFValueArrayEigen_t>
					{return ts.generateSamplesCollectionPDF_Eigen(geni, N);},
				"This function generates N samples, using geni as random source and the associated pdf values of the generated samples."
				" The function returns a pair, the first element contains the SampleCollection, where the samplesa are stored in."
				" The second element is a NumPy compatible array that contains the probabilities of the samples.",
				py::arg("g"),
				py::arg("N")
		)
	; //End Tree Sample




	return;
}; //End: wrapperint utility function








