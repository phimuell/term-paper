/**
 * \brief	This fucntion implements the wrapping of the random fucntions mainly the utility stuff.
 */

//Include the yggdrasil code that is needed
#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>
#include <random/yggdrasil_multiGauss.hpp>
#include <random/yggdrasil_multiDirichle.hpp>
#include <random/yggdrasil_outlierPDF.hpp>
#include <random/yggdrasil_biModalUniform.hpp>
#include <random/yggdrasil_gammaPDF.hpp>
#include <random/yggdrasil_multiUniform.hpp>
#include <random/yggdrasil_betaPDF.hpp>

#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

//Wrapper to the random number generator
#include <random/pyYggdrasil_randomNumberGenerator.hpp>


//Include Eigen
#include <Eigen/Core>

//Include the bindings
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>


//Make a namespace alias
namespace py = pybind11;



void
pyYggdrasil_randomWrapper_util(
	py::module&	m)
{
	/*
	 * =======================
	 * USINGS
	 */
	using Int_t 		 = ::yggdrasil::Int_t;
	using Numeric_t 	 = ::yggdrasil::Numeric_t;
	using Size_t 		 = ::yggdrasil::Size_t;
	using pRNGenerator_t 	 = ::yggdrasil::pyYggdrasil_pRNGenerator_t;
	using result_type 	 = pRNGenerator_t::result_type;
	using Distribution_t 	 = ::yggdrasil::yggdrasil_randomDistribution_i;
	using Gauss_t 		 = ::yggdrasil::yggdrasil_multiDimGauss_t;
	using Dirichlet_t 	 = ::yggdrasil::yggdrasil_multiDimDirichlet_t;
	using Uniform_t 	 = ::yggdrasil::yggdrasil_multiDimUniform_t;
	using Outlier_t 	 = ::yggdrasil::yggdrasil_outlierPDF_t;
	using BiModalUniform_t 	 = ::yggdrasil::yggdrasil_biModalUniform_t;
	using Gamma_t 		 = ::yggdrasil::yggdrasil_gammaDistri_t;
	using Beta_t 		 = ::yggdrasil::yggdrasil_betaDistri_t;
	using HyperCube_t 	 = Distribution_t::HyperCube_t;
	using IntervalBound_t 	 = Distribution_t::IntervalBound_t;
	using PDFValueArray_t 	 = Distribution_t::PDFValueArray_t;
	using MeanVarRet_t 	 = ::std::pair<Distribution_t::MeanVec_t, Distribution_t::CoVarMat_t>;
	using SampleCollection_t = ::yggdrasil::yggdrasil_sampleCollection_t;
	using SampleArray_t      = ::yggdrasil::yggdrasil_arraySample_t;
	using SampleList_t	 = ::yggdrasil::yggdrasil_sampleList_t;

	/*
	 * ==================0
	 * Generator
	 */
	py::class_<pRNGenerator_t>(m, "pRNG",
		"This is the implementation of a (pseudo) random number generator."
		" It is a wrapper arround the Mersen Twister Engine that is implemented by the C++ standard."
		" It is needed to generate random numbers from the Distributions that are offered by py/Yggdrasil."
		)
		/*
		 * Constructor
		 */
		.def(py::init(),
				"This is the default constructor, it will generate a default seeded random number generator."
		)
		.def(py::init<result_type>(),
				"This constructor constructs a pRNG and uses the provided seed.",
				py::arg("seed")
		)
		.def(py::init<result_type, Size_t>(),
				"This construct a pRNG and seeds it with the provided seed."
				" But it will advance the internal state of the genrator by N."
				" This is equivalent with first constructing a generator and then calling discard.",
				py::arg("seed"),
				py::arg("N")
		)

		/*
		 * Functions
		 */
		.def("discard", [](pRNGenerator_t& geni, const Size_t N) -> void
					{geni.discard(N); return;},
				"This function advances the internal state of *this by N."
				" It is equivalent by calling the sample fucntion N times, but is more efficient.",
				py::arg("N")
		)
		.def("seed", [](pRNGenerator_t& geni, const result_type newSeed) -> void
					{geni.seed(newSeed); return;},
				"This function reinitalizes *this with the provided seed."
				" This function is as reconstructing *this, but with new seed as argument.",
				py::arg("newSeed")
		)
		.def("sample", [](pRNGenerator_t& geni) -> result_type
					{return geni.generate();},
				"This function generate a new sample form the random numnber generation."
		)
	; //End wrapper pRNG


	/*
	 * =======================
	 * Functions
	 *
	 * These functions returns the statistic
	 */
	m.def("estMeanCovar", [](const SampleArray_t& sa) -> MeanVarRet_t
					{return ::yggdrasil::yggdrasil_estinmateStatistic(sa);},
				"This function calculates the mean and the covariance of the samples that are stored inside the sample array."
				" The fucntion returns a pair, the first memeber contains the mean and the second contains the covariance."
				" Notice the type is always an Eigen::Matrix type, this means it will be converted to a numpy array."
				" Also notice that this holds true even for one dimensional samples.",
				py::arg("x")
	);

	m.def("estMeanCovar", [](const SampleList_t& sl) -> MeanVarRet_t
					{return ::yggdrasil::yggdrasil_estinmateStatistic(sl);},
				"This function calculates the mean and the covariance of the samples that are stored inside the sample list."
				" The fucntion returns a pair, the first memeber contains the mean and the second contains the covariance."
				" Notice the type is always an Eigen::Matrix type, this means it will be converted to a numpy array."
				" Also notice that this holds true even for one dimensional samples.",
				py::arg("x")
	);

	m.def("estMeanCovar", [](const SampleCollection_t& sc) -> MeanVarRet_t
					{return ::yggdrasil::yggdrasil_estinmateStatistic(sc);},
				"This function calculates the mean and the covariance of the samples that are stored inside the sample collection."
				" The fucntion returns a pair, the first memeber contains the mean and the second contains the covariance."
				" Notice the type is always an Eigen::Matrix type, this means it will be converted to a numpy array."
				" Also notice that this holds true even for one dimensional samples.",
				py::arg("x")
	);


	return;
}; //End: wrapperint utility function








