/**
 * \brief	This fucntion implements the wrapping of the random fucntions mainly the distribution stuff
 *
 * It contains mainly the implementation of the concrete distributions
 */

//Include the yggdrasil code that is needed
#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>
#include <random/yggdrasil_multiGauss.hpp>
#include <random/yggdrasil_multiDirichle.hpp>
#include <random/yggdrasil_outlierPDF.hpp>
#include <random/yggdrasil_biModalUniform.hpp>
#include <random/yggdrasil_gammaPDF.hpp>
#include <random/yggdrasil_multiUniform.hpp>
#include <random/yggdrasil_betaPDF.hpp>
#include <random/yggdrasil_multiModalDistribution.hpp>

#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

//Wrapper to the random number generator
#include <random/pyYggdrasil_randomNumberGenerator.hpp>


//Include Eigen
#include <Eigen/Core>

//Include the bindings
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>


//Make a namespace alias
namespace py = pybind11;


void
pyYggdrasil_randomWrapper_distri_impl(
	py::module&	m)
{
	/*
	 * =======================
	 * USINGS
	 */
	using Int_t 		 = ::yggdrasil::Int_t;
	using Numeric_t 	 = ::yggdrasil::Numeric_t;
	using Real_t 		 = ::yggdrasil::Real_t;
	using Size_t 		 = ::yggdrasil::Size_t;
	using pRNGenerator_t 	 = ::yggdrasil::pyYggdrasil_pRNGenerator_t;
	using result_type 	 = pRNGenerator_t::result_type;
	using Distribution_t 	 = ::yggdrasil::yggdrasil_randomDistribution_i;
	using Gauss_t 		 = ::yggdrasil::yggdrasil_multiDimGauss_t;
	using Dirichlet_t 	 = ::yggdrasil::yggdrasil_multiDimDirichlet_t;
	using Alpha_t 	 	 = Dirichlet_t::Alpha_t;
	using Uniform_t 	 = ::yggdrasil::yggdrasil_multiDimUniform_t;
	using Outlier_t 	 = ::yggdrasil::yggdrasil_outlierPDF_t;
	using BiModalUniform_t 	 = ::yggdrasil::yggdrasil_biModalUniform_t;
	using Gamma_t 		 = ::yggdrasil::yggdrasil_gammaDistri_t;
	using Beta_t 		 = ::yggdrasil::yggdrasil_betaDistri_t;
	using MultiModal_t 	 = ::yggdrasil::yggdrasil_multiModalDistribution_t;
	using WeightVector_t 	 = MultiModal_t::WeightVector_t;
	using ModeVector_t 	 = MultiModal_t::ModeVector_t;
	using HyperCube_t 	 = Distribution_t::HyperCube_t;
	using IntervalBound_t 	 = Distribution_t::IntervalBound_t;
	using PDFValueArray_t 	 = Distribution_t::PDFValueArray_t;
	using MeanVec_t 	 = Distribution_t::MeanVec_t;
	using CoVarMat_t 	 = Distribution_t::CoVarMat_t;
	using Distribution_ptr 	 = Distribution_t::Distribution_ptr;
	using FactoryRet_t 	 = Distribution_t::FactoryRet_t;
	using eDistiType 	 = ::yggdrasil::eDistiType;
	using Sample_t 		 = ::yggdrasil::yggdrasil_sample_t;

	using SampleCollection_t = ::yggdrasil::yggdrasil_sampleCollection_t;
	using SampleArray_t      = ::yggdrasil::yggdrasil_arraySample_t;
	using SampleList_t	 = ::yggdrasil::yggdrasil_sampleList_t;


	/*
	 * =====================
	 * Gauss
	 */
	py::class_<Gauss_t, Distribution_t>(m, "Gauss",
			"This class is a implementaion of the distribution interface."
			" It implements a multi dimensional Gauss distribution."
			" It uses Eigen matrices, which should be compatible to numpy matrices."
		)
		/*
		 * Constructors
		 * We only need to define the constructiors.
		 * The rest is done because of the base class
		 */
		.def(py::init<const MeanVec_t&, const CoVarMat_t&, const HyperCube_t&>(),
				"This constructor constructs a Gaussian distribution, with has the median mu and the covariance matrix Sigma, which is something like the VARIANCE (this is different from many other interface, also from the C++ standard."
				" There are no direct check if Sigma is symmertric, it is feeded to the Cholescky decomposition routine of Eigen, if it is fine with it, then its good."
				" Not that only one half of the matrix is used."
				" The sampling is restricted to the hyper cube hc."
				" If it is invalid, then no restrictions are applied."
				" The class works with Eigen types, they will be automatically converted from numpy arrays.",
				py::arg("mu"),
				py::arg("Sigma"),
				py::arg("domain")
		)
		.def(py::init<const MeanVec_t&, const CoVarMat_t&>(),
				"This constructor constructs a Gauss distribution."
				" The sampling domain is invalid, which means that there are no restrictions applied."
				" For a discussion of the arguments see the other constructor.",
				py::arg("mu"),
				py::arg("Sigma")
		)
	; //End gauss implementation


	/*
	 * ====================
	 * Dirichlet
	 */
	py::class_<Dirichlet_t, Distribution_t>(m, "Dirichlet",
			"This class implements the Dirichlet distribution."
			" The class follows the convention of the paper and Mathematica, note that for example Wikipedia uses a different convention."
			" A K-1 dimensional distribution is described by K many parameters, \\alpha_i > 0 for all i = 1, ... , K, parameters."
			" Furter we requier that the sample vector x, x_i > 0 for all i = 1, ..., K-1, and further \\sum_{i = 1}^{K - 1} x_i < 1, has to hold."
			" By default the sampling domain is uded as the unit hyper cube of the coresponding dimension."
		)
		/*
		 * Constructors
		 */
		.def(py::init<const Alpha_t&, const HyperCube_t&>(),
				"This constructor constructs a dirichlet distribution with the given parameter."
				" Some checks on the validity of the parameter is performed."
				" Note that the dimension of the hyper cube domain, must be one less than the one of the parameter vector."
				" Also note that if this constructor is used, the domain must be valid, this is the _only_ requierement that is imposed."
				" This means that it is not checked if the domain actually contains the support domain."
				" This is generally true for the distributions with finite support.",
				py::arg("alpha"),
				py::arg("domain")
		)
		.def(py::init<const Alpha_t&>(),
				"This constructor constructs a dirichlet distribution."
				" If the parameter vector alpha has K komponents a K - 1 dimensional distribution is constructed."
				" The sample domain that is used is the unit hyper cube, which is also the full supported domain.",
				py::arg("alpha")
		)


		/*
		 * Other functions
		 */
		.def("getAlpha", [](const Dirichlet_t& d) -> Alpha_t
					{return d.getAlpha();},
				"This fucntion returns a copy of the parameter vector that was used to construct the distribution."
		)
	; //End multi dirichlet wrapper


	/*
	 * ======================
	 * Uniform
	 */
	py::class_<Uniform_t, Distribution_t>(m, "Uniform",
			"This class allows to sample multidimensional uniform distributions."
			" Note that all dimensions are independend form each other."
			" This class is mainly described by the support domain, this is the domain in which samples are generated."
			" This class also supports the concept of a sampling domain."
			" For historical reaons the two of them needs not to be the same, however it is strongly recomended to not exploit that future."
		)
		/*
		 * Constructors
		 */
		.def(py::init<const HyperCube_t&>(),
				"This constructor constuct a multidimensional uniform distribution on the domain described by suppDom."
				" This is the domain where samples can be drawn."
				" All dimensions are independend from each other.",
				py::arg("suppDom")
		)
		.def(py::init<const HyperCube_t&, const HyperCube_t&>(),
				"This is a constructor that takes as first argument the support domain, suppDom, meaning is the same as in the other supported constructor."
				" As second argument it takes another domain, the sampling domain, which is called for considtency simply domain, which needs to be at least as big as the support domain, it can be invalid."
				" In this setting it does not cary any meaning, it is only provided for consistency reasons."
				" It is strongly advised to not use it.",
				py::arg("suppDom"),
				py::arg("domain")
		)


		/*
		 * ===================
		 * Additional function
		 */
		.def("getSupportDomain", [](const Uniform_t& u) -> HyperCube_t
					{return u.getSupportDomain();},
				"This function returns the support domain."
				" This is the first argument of the constructor."
		)
	; //End multi uniform wrapper


	/*
	 * ====================
	 * Outlier
	 */
	py::class_<Outlier_t, Distribution_t>(m, "Outlier",
			"This is an outlier distribution, it is basically a bimodal one dimensional gaussian distribution."
			" Both modes have the same mean, but different standard deviantions."
			" The parameter alpha, that must be inside the range 0 to 1, describes the strength of the first mode."
		)
		/*
		 * Constructors
		 */
		.def(py::init<Numeric_t, Numeric_t, Numeric_t, Numeric_t, const HyperCube_t&>(),
				"This is the full constructor, which takes the alpha the strength of the first mode, the mean of both modes."
				" It also takes the standard deviation of the first mode sigma1 and the standard deviation of the second mode, sigma2."
				" Note that the other Gauss distribution takes the variance instead."
				" As last argument A hypercube, which must have dimension one is used."
				" The sampling generation is restricted to this domain.",
				py::arg("alpha"),
				py::arg("mean"),
				py::arg("sigma1"),
				py::arg("sigma2"),
				py::arg("domain")
		)
		.def(py::init<Numeric_t, Numeric_t, Numeric_t, Numeric_t>(),
				"This is the full constructor, which takes the alpha the strength of the first mode, the mean of both modes."
				" It also takes the standard deviation of the first mode sigma1 and the standard deviation of the second mode, sigma2."
				" Note that the other Gauss distribution takes the variance instead."
				" This version does nto take a domain argument, thus the sampling is not restricted.",
				py::arg("alpha"),
				py::arg("mean"),
				py::arg("sigma1"),
				py::arg("sigma2")
		)


		/*
		 * Additional functions
		 */
		.def("getStdDev1", [](const Outlier_t& o) -> Numeric_t
					{return o.getStdDev1();},
				"This function returns the standard deviation of the first mode."
				" Note that unlike the getCoVar() function, which returns a matrix, this function returns a scalar."
		)
		.def("getStdDev2", [](const Outlier_t& o) -> Numeric_t
					{return o.getStdDev2();},
				"This function returns the standard deviation of the second mode."
				" Note that unlike the getCoVar() function, which returns a matrix, this function returns a scalar."
		)
		.def("getAlpha", [](const Outlier_t& o) -> Numeric_t
					{return o.getAlpha();},
				"This fucntion returns the coupling parameter alpha, which describes the influence of the first mode."
		)
	; //End outlier wrapper


	/*
	 * ====================
	 * Bimodal uniform
	 */
	py::class_<BiModalUniform_t, Distribution_t>(m, "BiUniform",
			"This class is the implementation of a bimodal (one dimensional) distribution."
			" It has two supports domain and and a coupling factor alpha, that describs the strength of the first mode."
			" In the paper this distribution was known as spiky uniform."
		)
		/*
		 * Constructor
		 */
		.def(py::init<IntervalBound_t, IntervalBound_t, Numeric_t, HyperCube_t>(),
				"This constructor constructs the uniform distribution."
				" It takes two intrvals (suppDom1 and subDom2) which describes the support of the two modes."
				" It also takes a copupling factor alpha, which describes the strength of the first mode."
				" It also accepts a domain, which must be a one dimensional hypercube, that serves as sampling domain."
				" This must be valid, as for all uniform distribution, it is kind of useless."
				" It is advised to use the other constructor.",
				py::arg("suppDom1"),
				py::arg("suppDom2"),
				py::arg("alpha"),
				py::arg("domain")
		)
		.def(py::init<IntervalBound_t, IntervalBound_t, Numeric_t>(),
				"This constructor constructs the uniform distribution."
				" It takes two intrvals (suppDom1 and subDom2) which describes the support of the two modes."
				" It also takes a copupling factor alpha, which describes the strength of the first mode."
				" This is the constructor that does not take a sampling domain argument."
				" The sampling domain is estimated from the two modes.",
				py::arg("suppDom1"),
				py::arg("suppDom2"),
				py::arg("alpha")
		)

		/*
		 * Additional functions
		 */
		.def("getSuppDom1", [](const BiModalUniform_t& b) -> IntervalBound_t
					{return b.getSuppDom1();},
				"This function returns the support domain of the first mode."
		)
		.def("getSuppDom2", [](const BiModalUniform_t& b) -> IntervalBound_t
					{return b.getSuppDom2();},
				"This function returns the support domain of the second mode."
		)
		.def("getAlpha", [](const BiModalUniform_t& b) -> Numeric_t
					{return b.getAlpha();},
				"This function returns the coupling value alpha."
		)
	; //End wrapper for BiModal Uniform


	/*
	 * =====================
	 * Gamma distribution
	 */
	py::class_<Gamma_t, Distribution_t>(m, "Gamma",
			"This is the class that generates gamma distributed samples."
			" As every one dimensional distribution that interface seams a bit odd."
			" There are many convention about this distribution."
			" We use the one that is also usedby the C++ standard, which calls Alpha the shape and Beta the scale."
			" This class also supports the sampling domain restriction."
		)
		/*
		 * Constructors
		 */
		.def(py::init<Numeric_t, Numeric_t, const HyperCube_t&>(),
				"This constructor constructs a gamma distribution object."
				" The scale parameter of the distribution is set to Alpha."
				" The rate parameter is set to Beta."
				" If the domain is given the sampling is restricted to it."
				" If invalid no restriction is done.",
				py::arg("Alpha"),
				py::arg("Beta"),
				py::arg("domain")
		)
		.def(py::init<Numeric_t, Numeric_t>(),
				"This constructor constructs a gamma distribution object."
				" The scale parameter of the distribution is set to Alpha."
				" The rate parameter is set to Beta."
				" The domain parameter is implicitly set to the invalid domain, thus no restriction is applied.",
				py::arg("Alpha"),
				py::arg("Beta")
		)

		/*
		 * Other fucntions
		 */
		.def("getAlpha", [](const Gamma_t& g) -> Numeric_t
					{return g.getAlpha();},
				"This fucntion returns the shape value, that is also known as Alpha, of the distribution."
		)
		.def("getBeta", [](const Gamma_t& g) -> Numeric_t
					{return g.getBeta();},
				"This function returns the rate value, that is also known as Beta, of the distribution."
		)
	; //End gamma distribution wrapper


	/*
	 * ===================
	 * Beta distribution
	 */
	py::class_<Beta_t, Distribution_t>(m, "Beta",
			"This is the beta distribution."
			" It is described by two parameters, Alpha and Beta."
			" It can also be restricted to a certain part of the domain."
		)
		/*
		 * Constructors
		 */
		.def(py::init<Numeric_t, Numeric_t, const HyperCube_t&>(),
				"This constructor constructs a beta distribution, that is described by the parameters Alpha and Beta."
				" The doamain restricts the sampling to a certain part."
				" It can be invalid in that case the unit interval is used."
				" Some checks are applied to see if the distribution is correct.",
				py::arg("Alpha"),
				py::arg("Beta"),
				py::arg("domain")
		)
		.def(py::init<Numeric_t, Numeric_t>(),
				"This constructor constructs a beta distribution, that is described by the parameters Alpha and Beta."
				" This constructor uses the unit interval as sampling domain.",
				py::arg("Alpha"),
				py::arg("Beta")
		)


		/*
		 * Other functions
		 */
		.def("getAlpha", [](const Beta_t& b) -> Numeric_t
					{return b.getAlpha();},
				"This function returns the Alpha parameter of the distribution."
		)
		.def("getBeta", [](const Beta_t& b) -> Numeric_t
					{return b.getBeta();},
				"This function returns the Beta value of the distribution."
		)
	; //End wrappong of beta



	/*
	 * ===================
	 * Multimodal distribution
	 */
	py::class_<MultiModal_t, Distribution_t>(m, "MultiModal",
			"This is a class for a multi modal distribution."
			" A multimodal distribution is a linear combination of different other distributon."
			" It is simmilar to the BiUniform and the outlier distribution, but way more general."
			" It supports an arbitrary number of and kind of distribution."
		)
		/*
		 * Constructors
		 * They are useless since they are only meaningfull in C++
		 */

		/*
		 * Other functions
		 */
		.def("getWeights", [](const MultiModal_t& m) -> MeanVec_t	//We will directly return it as eigen type
					{return m.getWeightsEigen();},
				"This function returns the weights of the modes."
				" The type is an Eigen Vector, thus it will be converted to a numpy array automatically."
		)
	; //End: Multimodal wrapper


	/*
	 * Helper fucntions for the multimodal distribution.
	 *
	 * This is because the interactioon is not so good between C++ and Python.
	 * But I think it is more that I do not know how to do it correctly.
	 *
	 * These functions are here to mimik a constructior.
	 */
	m.def("creatMultiModal", [](
			const Numeric_t  	w1,	//The weight of the first mode
			const Distribution_t&	m1,	//The first mode
			const Distribution_t&	m2,	//The second mode
			const HyperCube_t&	hc)	//The domain, will be defaulted to invalid
		-> Distribution_ptr
		{
			WeightVector_t W = {w1};
			ModeVector_t M;
			M.push_back(m1.clone());
			M.push_back(m2.clone());

			return Distribution_ptr(new MultiModal_t(M, W, hc));
		}, //End impl
		"This function creates a bimodal distribution out of the given modes."
		"The weight of the first mode is w1, the weight of the second mode is implicitly given by 1-w1."
		" The domain is optional and defaults to the invalid domain."
		" This means that it is tried to estimate a domain from the underling distributions.",
		py::arg("w1"),
		py::arg("m1"),
		py::arg("m2"),
		py::arg_v("domain", HyperCube_t::MAKE_INVALID_CUBE(), "The domain that is used to restrict the sample range, if invalid, the domain is estimated.")
	); //End bimodal

	m.def("creatMultiModal", [](
			const Numeric_t  	w1,	//The weight of the first mode
			const Distribution_t&	m1,	//The first mode
			const Numeric_t 	w2,	//The weight of the second mode
			const Distribution_t&	m2,	//The second mode
			const Distribution_t& 	m3,	//The third mode
			const HyperCube_t&	hc)	//The domain, will be defaulted to invalid
		-> Distribution_ptr
		{
			WeightVector_t W = {w1, w2};
			ModeVector_t M;
			M.push_back(m1.clone());
			M.push_back(m2.clone());
			M.push_back(m3.clone());

			return Distribution_ptr(new MultiModal_t(M, W, hc));
		}, //End impl
		"This function creates a trimodal distribution out of the given modes."
		"The weight of the first mode is w1, the weight of the second mode is w2, the third weight is estimated by 1-w1-w2."
		" The domain is optional and defaults to the invalid domain."
		" This means that it is tried to estimate a domain from the underling distributions.",
		py::arg("w1"),
		py::arg("m1"),
		py::arg("w2"),
		py::arg("m2"),
		py::arg("m3"),
		py::arg_v("domain", HyperCube_t::MAKE_INVALID_CUBE(), "The domain that is used to restrict the sample range, if invalid, the domain is estimated.")
	); //End trimodal

	m.def("creatMultiModal", [](
			const Numeric_t  	w1,	//The weight of the first mode
			const Distribution_t&	m1,	//The first mode
			const Numeric_t 	w2,	//The weight of the second mode
			const Distribution_t&	m2,	//The second mode
			const Numeric_t 	w3,	//The weight of the third mode
			const Distribution_t& 	m3,	//The third mode
			const Distribution_t& 	m4,	//The fourth mode
			const HyperCube_t&	hc)	//The domain, will be defaulted to invalid
		-> Distribution_ptr
		{
			WeightVector_t W = {w1, w2, w3};
			ModeVector_t M;
			M.push_back(m1.clone());
			M.push_back(m2.clone());
			M.push_back(m3.clone());
			M.push_back(m4.clone());

			return Distribution_ptr(new MultiModal_t(M, W, hc));
		}, //End impl
		"This function creates a distribution that is composed out of four given modes."
		" The last weight is given implicitly such that the weights sum up to one."
		" The domain is optional and defaults to the invalid domain."
		" This means that it is tried to estimate a domain from the underling distributions.",
		py::arg("w1"),
		py::arg("m1"),
		py::arg("w2"),
		py::arg("m2"),
		py::arg("w3"),
		py::arg("m3"),
		py::arg("m4"),
		py::arg_v("domain", HyperCube_t::MAKE_INVALID_CUBE(), "The domain that is used to restrict the sample range, if invalid, the domain is estimated.")
	); //End four mode


	m.def("creatMultiModal", [](
			const Numeric_t  	w1,	//The weight of the first mode
			const Distribution_t&	m1,	//The first mode
			const Numeric_t 	w2,	//The weight of the second mode
			const Distribution_t&	m2,	//The second mode
			const Numeric_t 	w3,	//The weight of the third mode
			const Distribution_t& 	m3,	//The third mode
			const Numeric_t 	w4,	//The fourth weight
			const Distribution_t& 	m4,	//The fourth mode
			const Distribution_t& 	m5,	//The fifth mode
			const HyperCube_t&	hc)	//The domain, will be defaulted to invalid
		-> Distribution_ptr
		{
			WeightVector_t W = {w1, w2, w3, w4};
			ModeVector_t M;
			M.push_back(m1.clone());
			M.push_back(m2.clone());
			M.push_back(m3.clone());
			M.push_back(m4.clone());
			M.push_back(m5.clone());

			return Distribution_ptr(new MultiModal_t(M, W, hc));
		}, //End impl
		"This function creates a distribution that is composed out of five given modes."
		" The last weight is given implicitly such that the weights sum up to one."
		" The domain is optional and defaults to the invalid domain."
		" This means that it is tried to estimate a domain from the underling distributions.",
		py::arg("w1"),
		py::arg("m1"),
		py::arg("w2"),
		py::arg("m2"),
		py::arg("w3"),
		py::arg("m3"),
		py::arg("w4"),
		py::arg("m4"),
		py::arg("m5"),
		py::arg_v("domain", HyperCube_t::MAKE_INVALID_CUBE(), "The domain that is used to restrict the sample range, if invalid, the domain is estimated.")
	); //End five mode



	return;
}; //End: wrapperint utility function










