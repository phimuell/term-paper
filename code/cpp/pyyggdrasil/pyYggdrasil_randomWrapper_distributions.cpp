/**
 * \brief	This fucntion implements the wrapping of the random fucntions mainly the distribution stuff
 */

//Include the yggdrasil code that is needed
#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>
#include <random/yggdrasil_multiGauss.hpp>
#include <random/yggdrasil_multiDirichle.hpp>
#include <random/yggdrasil_outlierPDF.hpp>
#include <random/yggdrasil_biModalUniform.hpp>
#include <random/yggdrasil_gammaPDF.hpp>
#include <random/yggdrasil_multiUniform.hpp>
#include <random/yggdrasil_betaPDF.hpp>

#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

//Wrapper to the random number generator
#include <random/pyYggdrasil_randomNumberGenerator.hpp>


//Include Eigen
#include <Eigen/Core>

//Include the bindings
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>


//Make a namespace alias
namespace py = pybind11;

/**
 * \brief	This is a function that computes the wrapper for the distribution implementations
 */
extern
void
pyYggdrasil_randomWrapper_distri_impl(
	py::module&	m);



void
pyYggdrasil_randomWrapper_distri(
	py::module&	m)
{
	/*
	 * =======================
	 * USINGS
	 */
	using Int_t 		 = ::yggdrasil::Int_t;
	using Numeric_t 	 = ::yggdrasil::Numeric_t;
	using Real_t 		 = ::yggdrasil::Real_t;
	using Size_t 		 = ::yggdrasil::Size_t;
	using pRNGenerator_t 	 = ::yggdrasil::pyYggdrasil_pRNGenerator_t;
	using result_type 	 = pRNGenerator_t::result_type;
	using Distribution_t 	 = ::yggdrasil::yggdrasil_randomDistribution_i;
	using Gauss_t 		 = ::yggdrasil::yggdrasil_multiDimGauss_t;
	using Dirichlet_t 	 = ::yggdrasil::yggdrasil_multiDimDirichlet_t;
	using Uniform_t 	 = ::yggdrasil::yggdrasil_multiDimUniform_t;
	using Outlier_t 	 = ::yggdrasil::yggdrasil_outlierPDF_t;
	using BiModalUniform_t 	 = ::yggdrasil::yggdrasil_biModalUniform_t;
	using Gamma_t 		 = ::yggdrasil::yggdrasil_gammaDistri_t;
	using Beta_t 		 = ::yggdrasil::yggdrasil_betaDistri_t;
	using HyperCube_t 	 = Distribution_t::HyperCube_t;
	using IntervalBound_t 	 = Distribution_t::IntervalBound_t;
	using PDFValueArray_t 	 = Distribution_t::PDFValueArrayEigen_t;	//This is for compability reason
	using MeanVec_t 	 = Distribution_t::MeanVec_t;
	using CoVarMat_t 	 = Distribution_t::CoVarMat_t;
	using Distribution_ptr 	 = Distribution_t::Distribution_ptr;
	using FactoryRet_t 	 = Distribution_t::FactoryRet_t;
	using eDistiType 	 = ::yggdrasil::eDistiType;
	using Sample_t 		 = ::yggdrasil::yggdrasil_sample_t;

	using SampleCollection_t = ::yggdrasil::yggdrasil_sampleCollection_t;
	using SampleArray_t      = ::yggdrasil::yggdrasil_arraySample_t;
	using SampleList_t	 = ::yggdrasil::yggdrasil_sampleList_t;

	/*
	 * ==================0
	 * Base class
	 */
	py::class_<Distribution_t>(m, "RNDistribution",
			"This is the base class of all random distribution inside Yggdrasil."
			" All of them operate on the random number generator object that is provided by pyYggdrasil."
			" The distribution used inside Yggdrasil are pretty much what you expect."
			" However they have an additional property, the 'sampleing domain'."
			" In a sense this is the domain where samples will be generated."
			" This is usefull if you want to restrict the sampling in different regions."
			" The pdf ios not addaped to map that change, this means that when you integrate the PDF of that distribution, will not neccessaraly result in one."
			" An invalid hyper cube means that no restriction is applied."
			" All implementation supports a construction whiout a domain."
			" In that case a good domain is selected, there is no guarantee if the domain is valid or not."
			" The Vector this class returns are Eigen types, thus they will be transaprently mapped to NumPy Arrays."
		)
		/*
		 * Constructors
		 * There are none
		 */

		/*
		 * Domain fucntions
		 */
		.def("getSampleDomain", [](const Distribution_t& d) -> HyperCube_t
					{HyperCube_t hc(d.getSampleDomain()); return hc;},
				"This fucntion returns the sampling domain of *this."
				" The sampling domain is the domain where in principle samples could occure."
				" If this domain is infinite, aka. unbounded, then an invalid cube is returned."
		)
		.def("isUnbounded", &Distribution_t::isUnbounded,
				"This function returns true if *this is undbounded."
				" This means that are no restriction, beside some hardware ones, where the samples could lie."
		)
		.def("nDims", &Distribution_t::nDims,
				"Return the dimensions of the samples that are generated."
		)

		/*
		 * Statistical information
		 */
		.def("getMean", [](const Distribution_t& d) -> MeanVec_t
					{MeanVec_t mean(d.getMean()); return mean;},
				"This function returns the mean of the underling distribution."
				" The value is an Eigen::VectorXd, pybind11 should convert it into an numpy array automatically."
				" Note that this involves coping."
		)
		.def("getCoVar", [](const Distribution_t& d) -> CoVarMat_t
					{CoVarMat_t var(d.getCoVar()); return var;},
				"This function returns the theoretical covariance of the distribution."
				" This is an Eigen matrix, that should be converted into a numpy matrix."
		)


		/*
		 * Sampling fucntions
		 */

		//
		//Single sample
		.def("generateSamples", [](Distribution_t& d, pRNGenerator_t& geni) -> Sample_t
					{return d.generateSample(geni);},
				"This function generates one sample from the underling distribution."
				" The probqability is guaranteed to be positive and non zero."
				" This guarantees comes from \"nomerical zeros\" that are effects of the fiunite size arethmetic.",
				py::arg("g")
		)
		.def("pdf", [](const Distribution_t& d, const Sample_t& s) -> Real_t
					{return d.pdf(s);},
				"Calculate the pdf of the given sample s."
				" Note that the result can be zero in finite arethmentic, but in infinite arethmetic the sample could have a positive value.",
				py::arg("s")
		)

		//
		//Array samples
		.def("generateSamplesArray", [](Distribution_t& d, pRNGenerator_t& g, const Size_t N) -> SampleArray_t
					{return d.generateSamplesArray(g, N);},
				"Generate N samples that are drawn from the distribution."
				" The samples are stored inside a sample array."
				" It is guaranteed tha the pdf fucntion of all samples inside the return value will be greater than zero.",
				py::arg("g"),
				py::arg("N")
		)
		.def("pdf", [](const Distribution_t& d, const SampleArray_t& sa) -> PDFValueArray_t
					{return d.pdf_Eigen(sa);},
				"This function calculates the pdf of all samples that are inside the sample array.",
				py::arg("s")
		)
		.def("generateSamplesArrayPDF", [](Distribution_t& d, pRNGenerator_t& g, const Size_t N) -> std::pair<SampleArray_t, PDFValueArray_t>
					{return d.generateSamplesArrayPDF_Eigen(g, N);},
				"Generate N samples that are drawn from the distribution."
				" This fucntion returns a pair, the first element is a sample array which contains the drawn sample."
				" The second elements are the pdf values of those samples.",
				py::arg("g"),
				py::arg("N")
		)


		//
		//Array List
		.def("generateSamplesList", [](Distribution_t& d, pRNGenerator_t& g, const Size_t N) -> SampleList_t
					{return d.generateSamplesList(g, N);},
				"Drawn N sample from the underling distribution."
				" The samples are stored inside a sample list."
				" Not that the pdf of all samples will be greater than zero.",
				py::arg("g"),
				py::arg("N")
		)
		.def("pdf", [](const Distribution_t& d, const SampleList_t& sl) -> PDFValueArray_t
					{return d.pdf_Eigen(sl);},
				"Calcualte the pdf of the samples that are stored inside the sample array.",
				py::arg("s")
		)
		.def("generateSamplesListPDF", [](Distribution_t& d, pRNGenerator_t& g, const Size_t N) -> std::pair<SampleList_t, PDFValueArray_t>
					{return d.generateSamplesListPDF_Eigen(g, N);},
				"Drawn N samples from the underling distribution."
				" This function returns a pair, the first memeber contains a sample list, with the drawn samples."
				" The second member is an array that stores the associated pdfs.",
				py::arg("g"),
				py::arg("N")
		)

		//
		//Sample Collection
		.def("generateSamplesCollection", [](Distribution_t& d, pRNGenerator_t& g, const Size_t N) -> SampleCollection_t
					{return d.generateSamplesCollection(g, N);},
				"Drawn N samples from the underling distribution."
				" The samples are stored inside a sample colelction.",
				py::arg("g"),
				py::arg("N")
		)
		.def("pdf", [](const Distribution_t& d, const SampleCollection_t& sc) -> PDFValueArray_t
					{return d.pdf_Eigen(sc);},
				"This fucntion calculates the probability of the samples that are inside the given sample collection.",
				py::arg("s")
		)
		.def("generateSamplesCollectionPDF", [](Distribution_t& d, pRNGenerator_t& g, const Size_t N) -> std::pair<SampleCollection_t, PDFValueArray_t>
					{return d.generateSamplesCollectionPDF_Eigen(g, N);},
				"This function drawns N samples from the distribution."
				" It returns a pair, where the first memeber is a sample collection, which the drawn samples."
				" The second memeber is a vector that contains the probabilioty of said memeber.",
				py::arg("g"),
				py::arg("N")
		)
	; //End wrapper distribution


	/*
	 * Factory functions
	 */

	//Register the enum
	py::enum_<eDistiType>(m, "eDistiType",
		"This is an enum to encode the kind of distribution should be generated, when a canonical distribution is requested."
		)

		.value(
			"NoDistri",	//Called differently because "None" is a python keyword
			eDistiType::None,
			"This encodes no distribution, this is a useless value."
		)
		.value(
			"Gauss",
			eDistiType::Gauss,
			"This is value for requesting Gaussian distributions."
		)
		.value(
			"Dirichlet",
			eDistiType::Dirichlet,
			"Type for encodeing Dirichlet distributed samples (convention of the paper and Mathematica)."
		)
		.value(
			"Outlier",
			eDistiType::Outlier,
			"Type for encouding an outlier distribution."
			" An outlier distribution is a binomial Gauss distrbution, where both Gauss distributions have the same mean."
		)
		.value(
			"Uniform",
			eDistiType::Uniform,
			"Type for encoding uniform distributions."
		)
		.value(
			"Gamma",
			eDistiType::Gamma,
			"Type for encoding a gamma distribution."
		)
		.value(
			"Beta",
			eDistiType::Beta,
			"Type for encoding Beta distributions."
		)
	; //End enum_<eDistriType>


	//Function for requesting a canonical distribution
	m.def("canDistribution", [](const eDistiType dType, const Int_t dKind) -> Distribution_ptr
				{FactoryRet_t ret(Distribution_t::FACTORY(dType, dKind)); return std::move(ret.first);}, //An explicit move is needed here, otherwhise the copy constructor is called.
			"This function returns the canonical distribution."
			" The canonical distributions are the ones that where used in the paper."
			" dType encodes which kind of distribution is requested, this value is of type eDistriType."
			" dKind is used to select the distribution inside the clsss."
			" The intention of dKind was once to encode the dimension, but it has long lost that meaning, also negative values are alloed."
			" See the manual for a list with supported distributions."
			" If the distribution does not exists, an error is generated.",
			py::arg("dType"),
			py::arg("dKind")
	);

	//The naming function
	m.def("canDistributionName", [](const eDistiType dType, const Int_t dKind) -> std::string
				{const FactoryRet_t ret(Distribution_t::FACTORY(dType, dKind)); return ret.second;},
			"This fucntion gives a name to the distribution."
			" This name is small and does not contain any funny charactor like space."
			" It can thus be used as filename."
			" See the canDistribution() fucntion for a discussion of the argumenst.",
			py::arg("dType"),
			py::arg("dKind")
	);


	/*
	 * ===================
	 * Call the concrete implementation
	 */
	pyYggdrasil_randomWrapper_distri_impl(m);

	return;
}; //End: wrapperint utility function








