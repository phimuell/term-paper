#pragma once
/**
 * \brief	This function implements a leaf proxy that is needed by the tree sampler.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>

#include <samples/yggdrasil_sample.hpp>


//INcluide std
#include <memory>


YGGDRASIL_NS_BEGIN(yggdrasil)


/**
 * \brief	This class implements a proxy for a leaf.
 * \class 	ygInternal_leafProxy_t
 *
 * This class implements a proxy of the leaf, to be more precice it
 * not a proxy but a copy. It contains all essential data that is
 * needed to emoulate a tree node for the parameteric model.
 * It is mostly a node substitute for the parameteric model.
 *
 * It maintains a copy of the parameteric model.
 *
 */
class ygInternal_leafProxy_t
{
	/*
	 * =====================
	 * Typedefs
	 */
public:
	using ParModel_t 		= yggdrasil_parametricModel_i;		//!< This is the type of the paramteric model.
	using ParModel_ptr 		= ParModel_t::ParametricModel_ptr;	//!< This is a managed pointer of the model.
	using HyperCube_t 		= ParModel_t::HyperCube_t;		//!< This is the hypercube
	using MultiConditon_t 		= yggdrasil_multiCondition_t;		//!< This is the multi condition object.
	using SingleCondition_t 	= MultiConditon_t::SingleCondition_t;	//!< This is the object for representing a sionlge conduitin.

	using NodeFacade_t 		= yggdrasil_nodeFacade_t;		//!< This is the facade for the node.
	using LeafIterator_t 		= yggdrasil_leafIterator_t;		//!< This is the leaf iterator.

	using Sample_t 			= yggdrasil_sample_t;			//!< This is the sample that is used.


	/*
	 * ========================
	 * Building Constructors
	 */
public:
	/**
	 * \breif	This function constructs *this out
	 * 		 of a node facade.
	 *
	 * This function copies the model, which is cloned,
	 * and the hyper cube into this.
	 * It also needs the condition that should be applied.
	 * The conditions are expressed in root data space.
	 *
	 * \param  node 	This is the node that should be proxied.
	 * \param  rootDSCondi	The conditions that are applied.
	 *
	 * \throw 	In cases of errors.
	 */
	ygInternal_leafProxy_t(
		const NodeFacade_t& 	node,
		const MultiConditon_t&	rootDSCondi);



	/*
	 * ======================
	 * Other constructors.
	 *
	 * These are the one that could be made defaultl.
	 */
public:
	/**
	 * \brief	Default constructor.
	 *
	 * Is deleted
	 */
	ygInternal_leafProxy_t()
	 = delete;


	/**
	 * \brief 	Copy constructor.
	 *
	 * Uses the clone function of the model.
	 */
	ygInternal_leafProxy_t(
		const ygInternal_leafProxy_t&);



	/**
	 * \brief	Copy assignment.
	 *
	 * Copies the argument and moves it to *this.
	 */
	ygInternal_leafProxy_t&
	operator= (
		const ygInternal_leafProxy_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	ygInternal_leafProxy_t(
		ygInternal_leafProxy_t&& )
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	ygInternal_leafProxy_t&
	operator= (
		ygInternal_leafProxy_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Defaulted
	 */
	~ygInternal_leafProxy_t()
	 = default;


	/*
	 * ======================
	 * Query fucntions
	 */
public:
	/**
	 * \brief	This function returns the numbers
	 * 		 of dimension of the cube.
	 */
	Size_t
	nDims()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nodeDomain.isValid());
		yggdrasil_assert(m_parModel != nullptr);

		return m_nodeDomain.nDims();
	}; //ENd: nDims


	/**
	 * \brief	This function returns a constant
	 * 		 reference to the cube of *this.
	 */
	const HyperCube_t&
	getDomain()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nodeDomain.isValid());
		yggdrasil_assert(m_parModel != nullptr);

		return m_nodeDomain;
	}; //End: getDomain


	/**
	 * \brief	Get the numbers of samples of the model.
	 *
	 * This will return the sample nase of *this.
	 */
	Size_t
	nSampleBase()
	 const
	{
		yggdrasil_assert(m_nodeDomain.isValid());
		yggdrasil_assert(m_parModel != nullptr);

		return m_parModel->nSampleBase();
	}; //End: nSampleBase


	/**
	 *  \brief	This fuction returns a constant reference
	 *  		 to the parameteric model of this.
	 */
	const ParModel_t&
	getParModel()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nodeDomain.isValid());
		yggdrasil_assert(m_parModel != nullptr);

		return (*m_parModel);
	}; //End: getParModel


	/**
	 * \brief	This function returns a mutable reference
	 * 		 to the parameteric model
	 *
	 */
	ParModel_t&
	getMutableParModel()
	 noexcept
	{
		yggdrasil_assert(m_nodeDomain.isValid());
		yggdrasil_assert(m_parModel != nullptr);

		return (*m_parModel);
	}; //End: getMutable ref


	/**
	 * \brief	This function generates a sample.
	 *
	 * This function is a convenient wrapper arround
	 * the fucntionaliyt that is needed.
	 * It will return a sample and the probability,
	 * both expressed in the rescalled root data space.
	 *
	 * \param  s		A pointer to the sample.
	 * \param  nTot		The total amounts of samples in teh tree.
	 *
	 * \retrurns 	The PDF expressed on rescalled root ds.
	 */
	Real_t
	sampleLeaf(
		Sample_t* const 	s,
		const Size_t 		nTot);


	/**
	 * \brief	This fucntion computes the probability
	 * 		 of the sample.
	 *
	 * The sample is interpreted on the node data space, not on the
	 * rescalled data space, this is is very important and
	 * different from the interface function.
	 *
	 * Also note that the pdf that is returned is unconditioned.
	 *
	 * \param  s		A pointer to the sample.
	 * \param  nTot		The total amounts of samples in teh tree.
	 */
	Real_t
	computeUnCondPDF(
		const Sample_t& 	s,
		const Size_t 		nTot)
	 const;




	/*
	 * =======================
	 * Private Memebbers
	 */
public:
	HyperCube_t 		m_nodeDomain;		//!< This is the domain of teh leaf, it is expressed in root data space.
	ParModel_ptr 		m_parModel;		//!< This is the managed pointer object of the model.
	MultiConditon_t 	m_reNDSCondi;		//!< This are the conditions expressed in rescalled node data space.
}; //End class(ygInternal_leafProxy_t)



YGGDRASIL_NS_END(yggdrasil)



