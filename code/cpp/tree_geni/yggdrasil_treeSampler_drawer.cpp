/**
 * \brief	This file implements the drawing fucntions of the tree.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>

#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>
#include <tree_geni/ygInternal_leafProxy.hpp>
#include <tree_geni/yggdrasil_treeSampler.hpp>

#include <samples/yggdrasil_sample.hpp>

#include <random/yggdrasil_random.hpp>

//INcluide std
#include <vector>
#include <random>


YGGDRASIL_NS_BEGIN(yggdrasil)


std::pair<yggdrasil_treeSampler_t::Sample_t, Real_t>
yggdrasil_treeSampler_t::wh_generateSample(
		yggdrasil_pRNG_t& 	geni)
{
	//We enforce that we can do smoething
	yggdrasil_assert(this->notOverConstrained() == true);
	yggdrasil_assert(this->m_leafList.size() > 0);

	//These is the starvation protecten.
	const Size_t maxTries = 1000;
	      Size_t tries    = 0;	//Now many times did we try it

	//This is teh dimension of the problem
	const Size_t nDims = m_globDataSpace.nDims();

	//This is teh result sample
	Sample_t res(nDims);

	//This is the pdf variable that we return.
	//After the loop it will contains the unconditioned probability
	//of the generated sample
	Real_t thisPDF = NAN;


	//This is for the resmapling
	do
	{
		/*
		 * Starvation prtection
		 */
		if(tries >= maxTries)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The starvation protection was trigered.");
		};

		//Increase the count
		tries += 1;


		/*
		 * Generate the samples inside the unit cube
		 */
		yggdrasil_assert(res.size() == nDims);
		for(Size_t i = 0; i != nDims; ++i)
		{
			res[i] = m_uniformSampler(geni);
		}; //ENd for(i): filling res

		//Now we select a lead
		const Size_t leafIdx = m_leafSelecter(geni);

		//Now get the proxy there
		LeafProxy_t& leafProxy = m_leafList.at(leafIdx);

		//Now sample the value
		thisPDF = leafProxy.sampleLeaf(&res, m_nSamples);

		//Perfoorm tests
		yggdrasil_assert(isValidFloat(thisPDF));
		yggdrasil_assert(thisPDF >= 0.0);

		/*
		 * The pdf and sample are already expressed on the rescalled root domain.
		 */

		//Pull the pdf back, but not the sample
		thisPDF *= m_trafoRootToGlob;

		//Perform tests
		yggdrasil_assert(isValidFloat(thisPDF));
		yggdrasil_assert(thisPDF >= 0.0);
	}
	while(
		(thisPDF <= 0.0)		//The pdf has to be strictly greater than zero
	); //ENd while

	//This is a final test
	if(isValidFloat(thisPDF) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("An invalid number aspdf was computed.");
	};

	/*
	 * We have to pull back the sample from the rescalled root data space
	 * to the global data sapce.
	 */
	m_globDataSpace.mapFromUnit_inPlace(res);

	/*
	 * We have now to scale the probability.
	 * This is needed such that we incooperate the condition
	 */
	thisPDF *= m_ipxc;	//Now the PDF is conditioned

	//Return
	return std::make_pair(std::move(res), thisPDF);
}; //End: workhorse function



YGGDRASIL_NS_END(yggdrasil)



