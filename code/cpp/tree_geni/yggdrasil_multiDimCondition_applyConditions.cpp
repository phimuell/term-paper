/**
 * \brief	This file contains the functions that deals with the appling of the conditions.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>

//INcluide std
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <sstream>



YGGDRASIL_NS_BEGIN(yggdrasil)



//This is teh fucntion that operates on a single sample
//It is implemented directly
yggdrasil_multiCondition_t::Sample_t&
yggdrasil_multiCondition_t::applyConditions(
	Sample_t& 	s)
 const
{
	//Parameter
	const Size_t NDim_sample = s.nDims();
	const Size_t NDim_uSpace = this->nDims();

	//Make tests
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Tried to use an invalid condition object.");
	};

	if(NDim_sample != NDim_uSpace)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Error in the dimension, the dimension of the sample is " + std::to_string(NDim_sample) + ", but the underling sample space has dimension " + std::to_string(NDim_uSpace));
	};

	//Now we modifing the sample, by iterating over it
	for(const SingleCondition_t& c : m_conditions)
	{
		//Load the paramters of teh condition
		const Size_t    cDim = c.getConditionDim();		//Dimension
		const Numeric_t cVal = c.getConditionValue();		//Value that has to be applied

		//Setting the value
		s[cDim] = cVal;
	}; //End for(c): iterating over the conditions

	return s;
}; //End: applyCondition(Sample)


//This is teh fucntion that operates on a sample list
//It is implemented directly and exploits the sample list specialties.
yggdrasil_multiCondition_t::SampleList_t&
yggdrasil_multiCondition_t::applyConditions(
	SampleList_t& 	s)
 const
{
	//Parameter
	const Size_t NDim_sample = s.nDims();
	const Size_t NDim_uSpace = this->nDims();

	//Make tests
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Tried to use an invalid condition object.");
	};

	if(NDim_sample != NDim_uSpace)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Error in the dimension, the dimension of the sample is " + std::to_string(NDim_sample) + ", but the underling sample space has dimension " + std::to_string(NDim_uSpace));
	};

	//Load the conditions
	DimensionList_t cDims = this->getConditionedDimensions();	//Dimensions where the restrictions works
	ValueArray_t    cVals = this->getConditionedValues();		//Value of the restrictions
	const Size_t    nCond = cVals.size();				//The numbers of conditions that are in place.

	//A samall test
	yggdrasil_assert(cDims.size() == cVals.size());

	//Shortcut if no restriction is in place
	if(cDims.size() == 0)
	{
		//No restriction is in place, so no restriction is needed and so no modification
		return s;
	}; //End if: no restriction


	/*
	 * Applying the conditions
	 * We will iterate to the samples and apply the conditions to them.
	 * This is easy because the list allows iteration over the samples
	 */
	for(Sample_t& s_i : s)
	{
		//It can happens that the samples are wrong, since this is not checked
		yggdrasil_assert((Size_t)s_i.nDims() == NDim_sample);

		//Now modify the samples
		for(Size_t j = 0; j != nCond; ++j)
		{
			//Load the values that we mofify
			const Size_t    cDim = cDims[j];
			const Numeric_t cVal = cVals[j];

			//Test if no out of bound access happens
			yggdrasil_assert(cDim < NDim_sample);

			//Modifing the sample
			s_i[cDim] = cVal;
		}; //End for(j): modifing the sample
	}; //End for(s_i): hanbdling the samples

	return s;
}; //End: applyCondition(List)



//This is teh fucntion that operates on a sample array
//It is implemented directly
yggdrasil_multiCondition_t::SampleArray_t&
yggdrasil_multiCondition_t::applyConditions(
	SampleArray_t& 	s)
 const
{
	//Parameter
	const Size_t NDim_sample = s.nDims();
	const Size_t NDim_uSpace = this->nDims();
	const Size_t NSamples 	 = s.nSamples();

	//Make tests
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Tried to use an invalid condition object.");
	};

	if(NDim_sample != NDim_uSpace)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Error in the dimension, the dimension of the sample is " + std::to_string(NDim_sample) + ", but the underling sample space has dimension " + std::to_string(NDim_uSpace));
	};

	//Load the conditions
	DimensionList_t cDims = this->getConditionedDimensions();	//Dimensions where the restrictions works
	ValueArray_t    cVals = this->getConditionedValues();		//Value of the restrictions
	const Size_t    nCond = cVals.size();				//The numbers of conditions that are in place.

	//A samall test
	yggdrasil_assert(cDims.size() == cVals.size());

	//Shortcut if no restriction is in place
	if(cDims.size() == 0)
	{
		//No restriction is in place, so no restriction is needed and so no modification
		return s;
	}; //End if: no restriction


	/*
	 * Applying the conditions
	 * We will iterate through the array and apply them one by one.
	 * This is we will operate on the memeory directly.
	 * Thsi could be a bit efficient, but not much.
	 */
	for(Size_t i = 0; i != NSamples; ++i)
	{
		//Load the start of the sample
		Numeric_t* const s_i = s.beginSample(i);

		//Now modify the components of the sample
		for(Size_t j = 0; j != nCond; ++j)
		{
			//Load the values that we mofify
			const Size_t    cDim = cDims[j];
			const Numeric_t cVal = cVals[j];

			//Test if no out of bound access happens
			yggdrasil_assert(cDim < NDim_sample);
			yggdrasil_assert((s_i + cDim) < s.endSample(i));

			//Modifing the sample
			s_i[cDim] = cVal;
		}; //End for(j): modifing the sample
	}; //End for(i): iterating over all samples

	return s;
}; //End: applyCondition(Array)


//This is teh fucntion that operates on a sample collection
//It is implemented directly by exploiting the container interface
yggdrasil_multiCondition_t::SampleCollection_t&
yggdrasil_multiCondition_t::applyConditions(
	SampleCollection_t& 	s)
 const
{
	//Parameter
	const Size_t NDim_sample = s.nDims();
	const Size_t NDim_uSpace = this->nDims();
	const Size_t NSamples 	 = s.nSamples();

	//Make tests
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Tried to use an invalid condition object.");
	};

	if(NDim_sample != NDim_uSpace)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Error in the dimension, the dimension of the sample is " + std::to_string(NDim_sample) + ", but the underling sample space has dimension " + std::to_string(NDim_uSpace));
	};

	//Shortcut if no restriction is in place
	if(m_conditions.size() == 0)
	{
		//No restriction is in place, so no restriction is needed and so no modification
		return s;
	}; //End if: no restriction


	/*
	 * Apply conditions
	 * We iterate throug the conditions and use the setDimensionTo() function
	 * that is specified by the container interface.
	 * It is the most efficient way anbyway.
	 */
	for(const SingleCondition_t& cond : m_conditions)
	{
		//Load the parameter
		yggdrasil_assert(cond.isValid());
		const Numeric_t cVal = cond.getConditionValue();
		const Size_t    cDim = cond.getConditionDim();

		//Set modify the value
		s.setDimensionTo(cDim, cVal);
	}; //End for(cond): appyling the condition

	return s;
}; //End: applyCondition(Collection)




YGGDRASIL_NS_END(yggdrasil)



