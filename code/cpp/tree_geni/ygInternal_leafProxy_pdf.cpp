/**
 * \brief	This function implements the pdf computation.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>
#include <tree_geni/ygInternal_leafProxy.hpp>

#include <samples/yggdrasil_sample.hpp>


//INcluide std
#include <memory>


YGGDRASIL_NS_BEGIN(yggdrasil)


Real_t
ygInternal_leafProxy_t::computeUnCondPDF(
	const Sample_t& 	s,
	const Size_t 		nTot)
 const
{
	//Test the imput
	yggdrasil_assert(s.isValid());
	yggdrasil_assert(nTot > 0);
	yggdrasil_assert(m_nodeDomain.isValid());

	//Create a new variable
	Sample_t nrvo(s);


	//We have to transform the sample to the
	//rescalled data space, since the modell needs it.
	m_nodeDomain.contractToUnit_inPlace(&nrvo);

	//Test if the sample is inside teh unit interval
	yggdrasil_assert(nrvo.isInsideUnit());

	//Compute the probability on the root data space
	const auto rootDSpdf = m_parModel->evaluateModelForSample(nrvo, m_nodeDomain, nTot);

	//TEst the probability
	yggdrasil_assert(isValidFloat(rootDSpdf));
	yggdrasil_assert(rootDSpdf >= 0.0);	//Can be zero

	//Return the probability
	return rootDSpdf;
}; //End: computeUnCond


YGGDRASIL_NS_END(yggdrasil)



