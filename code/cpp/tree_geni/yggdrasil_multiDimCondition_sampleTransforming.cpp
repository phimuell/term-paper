/**
 * \brief	This file implements the functions that derals with the transforming of
 * 		 The samples from teh restricted to the non restricted subspace.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>

//INcluide std
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <sstream>



YGGDRASIL_NS_BEGIN(yggdrasil)

/*
 * ======================
 * FWD
 * Functions that are used to implement the transformation
 * for the sample containers
 */
template<
	class 	Conatiner_t
>
extern
Conatiner_t
ygInternal_restrictSampleContainer(
	const yggdrasil_multiCondition_t& 	self,
	const Conatiner_t& 			samples);


template<
	class 	Conatiner_t
>
extern
Conatiner_t
ygInternal_expandSampleContainer(
	const yggdrasil_multiCondition_t& 	self,
	const Conatiner_t& 			samples);


/*
 * ======================
 * Singe sample transformation
 */
yggdrasil_multiCondition_t::Sample_t
yggdrasil_multiCondition_t::expandSample(
	const Sample_t& 	s)
 const
{
	//Performs some tests
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an invalid condition.");
	};
	yggdrasil_assert(s.isValid());

	//Get the dimension of the sample; caching for later usage
	//This is teh restricted dimension
	const Size_t restrictedSampleDim = s.nDims();

	//The restricted sample dimension must be the same as the dof
	if(restrictedSampleDim != this->nDOF())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The restricted sample had a dimension of " + std::to_string(restrictedSampleDim) + ", but the DOF of the condition was " + std::to_string(this->nDOF()));
	};

	/*
	 * Expanding the sample, this is not particular efficient.
	 */

	//Construct the output sample
	Sample_t expandedSample(m_nDims);

	//This is the counter for accessing the restricted sample
	Size_t idxRestrictedSample = 0;

	for(Size_t i = 0; i != m_nDims; ++i)
	{
		//Get the condition that governs the current
		//dimension; if invalid this is a free dimension
		const SingleCondition_t thisCondit = this->getConditionFor(i);

		//This is the variable that contains the value of the new dimension
		Numeric_t thisDimValue = NAN;

		//Make the distinction between condition or not
		if(thisCondit.isValid() == true)
		{
			//The dimension i is governed by a condition, so we use the condition
			thisDimValue = thisCondit.getConditionValue();
			yggdrasil_assert(i == (Size_t)thisCondit.getConditionDim());
		}
		else
		{
			//The current dimension is a free dimension, so we must use the restricted sample
			thisDimValue = s[idxRestrictedSample];

			//We must increment the idx counter for the next time
			idxRestrictedSample += 1;
			yggdrasil_assert(idxRestrictedSample <= (Size_t)s.nDims());
		};

		/*
		 * We write now the sample
		 */
		if(isValidFloat(thisDimValue) == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The expanding of the sample failed in dimension " + std::to_string(i));
		};

		//Write the value
		expandedSample[i] = thisDimValue;
	}; //End for(i): iterating over the dimension of teh sample space

	//Return teh sample
	return expandedSample;
}; //End: expand sample.


yggdrasil_multiCondition_t::Sample_t
yggdrasil_multiCondition_t::restrictSample(
	const Sample_t& 	s)
 const
{
	//This is an partial valid test
	if(this->m_nDims == 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The used condstion is invalid.");
	};

	//test if the dimensions are correct
	if(this->m_nDims != (Size_t)s.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("Can not restirct the sample of dimension " + std::to_string(s.nDims()) + ", the underlying sample space is " + std::to_string(m_nDims));
	};

	//Get the degrees of freedoms
	const Size_t nDOFs = this->nDOF();

	//Create the restricted sample
	Sample_t restrictedSample(nDOFs);

	//This is an index that is used to write to the restricted sample
	Size_t idxRestricted = 0;

	//Iterating through all the dimensions
	for(Size_t i = 0; i != m_nDims; ++i)
	{
		//get the condition for the dimension i
		//if invalid then we know that there are none
		const SingleCondition_t thisCondi = this->getConditionFor(i);

		//If the condition is valid then dimension i is
		//a free one and belongs to the restricted sample
		if(thisCondi.isValid() == false)
		{
			/*
			 * The restriction is invalid, so dimension
			 * i is a free dimension, that must join the sample.
			 */
			restrictedSample[idxRestricted] = s[i];

			//Increment the indx into the sample
			idxRestricted += 1;
		}
		else
		{
			/*
			 * The condition is invalid so we have nothing to do
			 * sonce this is a restricted dimension.
			 *
			 * We ignore the value the sample has in this dimension.
			 */
		}; //ENd else:
	}; //End for(i): going through the dimension

	if(idxRestricted != (Size_t)restrictedSample.nDims())
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The restriction failed.");
	}; //End if: test if restrictionw as successfull

	//Return the restricted sample
	return restrictedSample;
}; //ENd: restric sample


/*
 * =========================
 * Implementation of the the container version
 */

yggdrasil_multiCondition_t::SampleList_t
yggdrasil_multiCondition_t::expandSample(
	const SampleList_t& 	s)
 const
{
	return ygInternal_expandSampleContainer(*this, s);
}; //End expanding on SampleList_t


yggdrasil_multiCondition_t::SampleList_t
yggdrasil_multiCondition_t::restrictSample(
	const SampleList_t& 	s)
 const
{
	return ygInternal_restrictSampleContainer(*this, s);
}; //End restrictiong on SampleList_t


yggdrasil_multiCondition_t::SampleArray_t
yggdrasil_multiCondition_t::expandSample(
	const SampleArray_t& 	s)
 const
{
	return ygInternal_expandSampleContainer(*this, s);
}; //End expanding on SampleArray_t


yggdrasil_multiCondition_t::SampleArray_t
yggdrasil_multiCondition_t::restrictSample(
	const SampleArray_t& 	s)
 const
{
	return ygInternal_restrictSampleContainer(*this, s);
}; //End restrictiong on SampleArray_t


yggdrasil_multiCondition_t::SampleCollection_t
yggdrasil_multiCondition_t::expandSample(
	const SampleCollection_t& 	s)
 const
{
	return ygInternal_expandSampleContainer(*this, s);
}; //End expanding on SampleCollection_t


yggdrasil_multiCondition_t::SampleCollection_t
yggdrasil_multiCondition_t::restrictSample(
	const SampleCollection_t& 	s)
 const
{
	return ygInternal_restrictSampleContainer(*this, s);
}; //End restrictiong on SampleCollection_t

/*
 * ==========================
 * Implementation of the sample driver
 */
template<
	class 	Conatiner_t
>
Conatiner_t
ygInternal_restrictSampleContainer(
	const yggdrasil_multiCondition_t& 	self,
	const Conatiner_t& 			samples)
{
	//Test if the dimension are agreeing
	if(self.nDims() != samples.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the samples (" + std::to_string(samples.nDims()) + ") and the one of the condition (" + std::to_string(self.nDims()) + ") is not the same.");
	};

	//This is the degree of freedomes that is the new dimension of teh samples
	const Size_t nDOFs = self.nDOF();

	//This is the dimension of teh underlying sample sapce
	const Size_t D = self.nDims();

	//This is a list of the free dimensions, this are all dimensions that
	//does not have a restriction applied to them
	const yggdrasil_multiCondition_t::DimensionList_t freeDims = self.getFreeDimensions();
	yggdrasil_assert(nDOFs == freeDims.size());

	// THis is the number of samples that are inside the container
	const Size_t N = samples.nSamples();

	//Create an empty container for storing the restricted sampels
	Conatiner_t restrictedSamples(nDOFs);
	restrictedSamples.reserve(nDOFs);	//Preallocate the space

	//This sample is the working variable
	//it is a smaple of the appropriate length
	yggdrasil_multiCondition_t::Sample_t restrictedSample(nDOFs);

	/*
	 * Iterating through all the the samples and apply the restrictions to them
	 */
	for(Size_t i = 0; i != N; ++i)
	{
		// Load the current sample
		const auto fullSample = samples.getSample(i);
		yggdrasil_assert(fullSample.nDims() == D);
		yggdrasil_assert(fullSample.isValid());

		//Apply copy the free dimensions into the restricted sample
		for(Size_t j = 0; j != nDOFs; ++j)
		{
			//Load the dimension that is currently handled
			const Size_t freeDim = freeDims[j];
			yggdrasil_assert(freeDim < D);

			//Load the value of the free dimension
			const Numeric_t freeDimValue = fullSample[freeDim];

			//Write the value into the restricted sample
			restrictedSample[j] = freeDimValue;
		}; //End for(j):

		//Store the restricted sample in the sample container of the restricted samples
		restrictedSamples.addNewSample(restrictedSample);
	}; //ENd for(i): going through the samples

	//Return the container
	return restrictedSamples;
}; //ENd: restricting


template<
	class 	Conatiner_t
>
extern
Conatiner_t
ygInternal_expandSampleContainer(
	const yggdrasil_multiCondition_t& 	self,
	const Conatiner_t& 			samples)
{
	//This is the number of free dimension
	const Size_t nDOFs = self.nDOF();

	//This is the dimension of the underlying sample space
	const Size_t D = self.nDims();

	//This is the number of samples that are handled
	const Size_t N = samples.nSamples();

	//Test if the dimensions are correct
	if(nDOFs != samples.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("Dimension missmatch in the expanding of the samples, the degree of freedom is " + std::to_string(nDOFs) + " but the samples have " + std::to_string(samples.nDims()));
	};

	//Load the free dimensions (this are the dimensions that have no condition applied to them
	const auto freeDims = self.getFreeDimensions();
	yggdrasil_assert(freeDims.size() == nDOFs);

	//Create the the sample container for the expanded samples
	Conatiner_t expandedSamples(D);	//Dimension of teh underlying sample space
	expandedSamples.reserve(N);	//Preallocate space for the insertion

	//This is the working sample
	yggdrasil_multiCondition_t::Sample_t expandedSample(D);

	//Iterating over all the samples and construct a smaples
	for(Size_t i = 0; i != N; ++i)
	{
		//Loading the restricted samples
		const auto restrictedSample = samples.getSample(i);

		//Writing the free dimensions of teh expanded samples
		for(Size_t j = 0; j != nDOFs; ++j)
		{
			//Perform some safty checks
			yggdrasil_assert(j < freeDims.size());
			yggdrasil_assert(freeDims[j] < (Size_t)expandedSample.nDims());
			yggdrasil_assert(j < (Size_t)restrictedSample.nDims());

			//Write the free dimensions
			expandedSample[freeDims[j]] = restrictedSample[j];
		}; //End for(j): writting the samples

		//Applying the conditons
		for(const yggdrasil_multiCondition_t::SingleCondition_t& condi : self.m_conditions)
		{
			//Performs some cheks
			yggdrasil_assert(condi.isValid());
			yggdrasil_assert(condi.getConditionDim() < expandedSample.nDims());

			//Write the sample
			expandedSample[condi.getConditionDim()] = condi.getConditionValue();
		}; //End for(condi): applying them

		//Store the sample in the container
		expandedSamples.addNewSample(expandedSample);
	}; //ENd for(i): iterating over the samples


	// Return the container
	return expandedSamples;
}; //End: expainging samples internal




YGGDRASIL_NS_END(yggdrasil)



