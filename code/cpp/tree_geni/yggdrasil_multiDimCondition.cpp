/**
 * \brief	This file implements a condition for several dimension.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>

//INcluide std
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <sstream>



YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_multiCondition_t::yggdrasil_multiCondition_t()
 :
  m_nDims(0),		//This will make something invalid
  m_conditions()
{};


yggdrasil_multiCondition_t::yggdrasil_multiCondition_t(
	const Int_t 		nDims)
 :
  m_nDims(nDims),
  m_conditions()
{
	//Test if the dimension is valid
	if(nDims == 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the underling sample space is zero.");
	};
	if(nDims < 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the underling samplöe spaceis negative, what are you doing.");
	};
	if(nDims == Constants::YG_INVALID_DIMENSION)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension is invalid.");
	};
	if(nDims >= Constants::YG_MAX_DIMENSIONS)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The numbers of diemnsion is too large.");
	};
}; //End: only dimnension is needed


yggdrasil_multiCondition_t::yggdrasil_multiCondition_t(
	const Int_t 				nDims,
	const ConditionMap_t& 			condi)
 :
  yggdrasil_multiCondition_t(nDims)		//This one performs the test on the nDim arg
{

	//Get the size of the restrictions
	const Size_t nCondi = condi.size();

	//Test if we do not restrict too much
	if(nCondi >= m_nDims)	//It must be > for obvious reasons, but also ==, because then no DOF are there
	{
		throw YGGDRASIL_EXCEPT_InvArg("The number of restrictions " + std::to_string(nCondi) + ", is larger or equal the number of dimensions of the underling sample space " + std::to_string(m_nDims));
	};

	//Preallocate space for the conditions
	m_conditions.reserve(nCondi);

	//Now we can go through the map and inserting them to the conditions.
	//They are ordered so no check in that regard is needed
	for(const auto& cCondi : condi)
	{
		//Unpackl it; the auto will actually increase type safty
		const auto cDim = cCondi.first;	 //The dimension of the condition
		const auto cVal = cCondi.second; //The value of the condition

		//Test if a dimension is too large
		if(cDim >= m_nDims)	//Zero based indexiong
		{
			throw YGGDRASIL_EXCEPT_InvArg("one of teh restriction dimension is invalid, the restricted dimension is " + std::to_string(cDim) + ", but the underling sample space is of dimension " + std::to_string(m_nDims));
		};

		//Innsert the single condition at the end
		m_conditions.emplace_back(cDim, cVal);

		//Make some tests
		yggdrasil_assert(m_conditions.back().isValid());
		yggdrasil_assert(m_conditions.back().getConditionDim() == cDim);
		yggdrasil_assert(m_conditions.back().getConditionValue() == cVal);
	}; //ENd for(cCondi): going through the condition

	//The conditions must be sorted
	yggdrasil_assert(std::is_sorted(m_conditions.cbegin(), m_conditions.cend()));

	//Test if this is valid
	yggdrasil_assert(this->isValid());

	//
	//End constructor
}; //ENd building constructor from map

yggdrasil_multiCondition_t::yggdrasil_multiCondition_t(
	const Int_t 				nDims,
	const std::vector<SingleCondition_t>&	condi)
 :
  yggdrasil_multiCondition_t(nDims)		//This one performs the test on the nDim arg
{
	//Get the number of different conditions
	const Size_t nCondi = condi.size();

	//Test if we do not restrict too much
	if(nCondi >= m_nDims)	//It must be > for obvious reasons, but also ==, because then no DOF are there
	{
		throw YGGDRASIL_EXCEPT_InvArg("The number of restrictions " + std::to_string(nCondi) + ", is larger or equal the number of dimensions of the underling sample space " + std::to_string(m_nDims));
	};

	//Reserve space for the conditions
	m_conditions.reserve(nCondi);

	//Iterate through the conditions and test them
	for(Size_t i = 0; i != nCondi; ++i)
	{
		//Load the current condition
		const SingleCondition_t& cCondi = condi[i];

		//Test the condition
		if(cCondi.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Tried to construct a multi condition out of an invalid condition.");
		};

		//Test if the dimension is right; dimension negative and so on are already tested
		if(Size_t(cCondi.getConditionDim()) >= m_nDims)	//Zero basied
		{
			throw YGGDRASIL_EXCEPT_InvArg("one of teh restriction dimension is invalid, the restricted dimension is " + std::to_string(cCondi.getConditionDim()) + ", but the underling sample space is of dimension " + std::to_string(m_nDims));
		};

		//Test if the dimension is already known
		if(this->internal_knownCondition(cCondi) == true)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The condition " + cCondi.print() + " is already knwon.");
		};

		/*
		 * When we are here, we can insiert the consition, since we accepted it.
		 */
		m_conditions.push_back(cCondi);
	}; //End for(i):

	//Test if all conditions whewre inserted
	yggdrasil_assert(nCondi == m_conditions.size());

	/*
	 * Now we sert the conditions
	 * such that lower dimensions comes first
	 */
	std::sort(m_conditions.begin(), m_conditions.end());

	//Test if this is valid
	yggdrasil_assert(this->isValid());

	//
	//END
}; //ENd buiolding from vector


bool
yggdrasil_multiCondition_t::internal_knownCondition(
	const SingleCondition_t&	 condi)
 const
{
	//Test imput argument
	if(condi.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Tried to search for an invalid argument.");
	};

	//In the case of an empty condition list,
	//The knondition is not known
	if(m_conditions.empty() == true)
	{
		return false;
	}; //End if: empty list

	//Using the find fucntion, it does not requere the range to be sorted
	const auto fIT = std::find(m_conditions.cbegin(), m_conditions.cend(), condi);

	//We know the condition, if the iterator is not the end iterator.
	//if so the condition is not known
	if(fIT == m_conditions.cend())
	{
		//The condition is not known
		return false;
	}; //End if: condi notz known

	//The condition is knonw, so we return true
	return true;
}; //ENd: internal search for condition


bool
yggdrasil_multiCondition_t::isValid()
 const
 noexcept
{
	//We requiere at least one dimension in the underling space
	if(m_nDims <= 0)
	{
		return false;
	};

	//We can not have more dimension than the maximum
	if(m_nDims >= Size_t(Constants::YG_MAX_DIMENSIONS))
	{
		return false;
	};


	//Test if there are something to tests
	if(m_conditions.empty() == true)
	{
		//Wea re empty, so we are now valid
		return true;
	};


	/*
	 * Now we perform some more tests
	 */
	for(Size_t i = 0; i != m_conditions.size(); ++i)
	{
		//Test if teh split itself is valid
		if(m_conditions[i].isValid() == false)
		{
			return false;
		};
		yggdrasil_assert(isValidFloat(m_conditions[i].getConditionValue()));

		//test if dimension does not exeed something
		if(!(Size_t(m_conditions[i].getConditionDim()) < m_nDims))	//Indexing
		{
			return false;
		};

		if(i != 0)	//Needed for existing of index i - 1
		{
			//Sorted condition
			if(!(m_conditions[i - 1].getConditionDim() < m_conditions[i].getConditionDim()))
			{
				return false;
			};
		}; //End if: we are not in the first iteration, so we have a predecessor
	}; //End goung for a check

	/*
	 * If we are here we have detected nothing that
	 * would make *thsi invalid, so we must conclude
	 * that *this is valid.
	 */
	return true;
}; //End: isValid


std::string
yggdrasil_multiCondition_t::print()
 const
{
	//This is teh variable that will hold the to composing string
	std::stringstream s;

	//This is the beginning of the output.
	s << "{";

	if(this->isValid() == false)
	{
		s << "INVALID";
	}
	else if(m_conditions.empty() == true)
	{
		/*
		 * The condition is empty
		 */
		s << "EMPTY";
	}
	else
	{
		/*
		 * The conditions ar not empyty, so print them
		 */

		//This indicates if this is the first run
		bool isFirstRun = true;

		//Going throughh the condition and print them
		for(const SingleCondition_t& c : m_conditions)
		{
			//If we are not in the first loop, output a ", " for seperation
			if(isFirstRun == false)
			{
				s << ", ";
			}; //ENd  if: seperation needed

			//Output the condition
			s << c.print();

			//The first loop is over
			isFirstRun = false;
		}; //End for(c):
	}; //End else: the condition is not empty


	//Writting the dimension of the problem
	s << " | " << m_nDims;

	//Writting the closing bracet
	s << "}";


	//Convert everything into a string and return
	return s.str();
}; //End print


yggdrasil_multiCondition_t::ConditionVector_t
yggdrasil_multiCondition_t::getConditionVector()
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not transfer an invalid condition.");
	};

	return m_conditions;
}; //End get condition vector.


yggdrasil_multiCondition_t::ConditionMap_t
yggdrasil_multiCondition_t::getConditionMap()
 const
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not transfer an invalid condition.");
	};

	//Create the map
	ConditionMap_t cMap;

	//Loop over all the conditions and insert them
	for(const SingleCondition_t& c : m_conditions)
	{
		cMap[c.getConditionDim()] = c.getConditionValue();
	}; //End for(c):

	//Return the map
	return cMap;
}; //End: condition map




yggdrasil_multiCondition_t::DimensionList_t
yggdrasil_multiCondition_t::getFreeDimensions()
 const
{
	yggdrasil_assert(this->isValid());	//For the sake of debugging.
	if(this->m_nDims == 0) 			//Only a partial valid test is needed
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Tried to use an invalid condition.");
	};

	//Get the number of free dimensions
	const Size_t nDOFs = this->nDOF();

	//Shortcut if we have zero free dimensions
	if(nDOFs == 0)
	{
		return DimensionList_t();
	}; //End if: zero free dimms short cut.

	//Shortcut for the case of no restriction
	if(m_nDims == nDOFs)
	{
		yggdrasil_assert(m_conditions.empty() == true);		//This has to hold

		/*
		 * No restrictions are in place, so the result is trivial
		 * a list of all dimensions
		 */
		DimensionList_t freeDimensions(m_nDims);

		for(Size_t i = 0; i != m_nDims; ++i)
		{
			freeDimensions[i] = i;
		}; //End for(i): filling consecutive list

		return freeDimensions;
	}; //End if: no restrictions

	//This must be the case
	yggdrasil_assert(m_conditions.empty() == false);

	//This is the output list
	DimensionList_t freeDimensions;
	freeDimensions.reserve(nDOFs);

	//Fill the list
	for(Size_t i = 0; i != m_nDims; ++i)
	{
		if(this->hasConditionFor(i) == false)
		{
			/*
			 * The current dimension i has no restriction applied to it.
			 * This meqans it is a free dimension so we add it to the free list
			 */
			freeDimensions.push_back(i);
			yggdrasil_assert(freeDimensions.size() <= nDOFs);	//This has always to hold
		};
	}; //End for(i): filling the list

	//Test if the number of dimension is correct
	if(nDOFs != freeDimensions.size())
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The number of free dimensions is not correct.");
	};

	//Return the free list
	return freeDimensions;
}; //End: getFreeDimensions


yggdrasil_multiCondition_t::DimensionList_t
yggdrasil_multiCondition_t::getConditionedDimensions()
 const
{
	yggdrasil_assert(this->isValid());	//For the sake of debugging
	if(this->m_nDims == 0)			//Only partial test id needed.
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an invalid condition object.");
	};

	//Shortcut of no condition
	if(m_conditions.size() == 0)
	{
		//No conditions are in place, so the returned array must be empty
		return DimensionList_t();
	}; //End if: shortcut no condition.

	//Create and allocate the result variable
	DimensionList_t conditionedDimensions;
	conditionedDimensions.reserve(m_conditions.size());	//Reserve space of later fast insertion

	//Going through the conditikons and get the dimension where they are applied to
	for(const SingleCondition_t& condi : m_conditions)
	{
		//Test if the condition is valid
		yggdrasil_assert(condi.isValid());

		//add the the dimension to teh list
		conditionedDimensions.push_back(condi.getConditionDim() );
	}; //End for(condi): going through the condi

	//If this is okay it should be ordered (this si only an internal guarantee)
	yggdrasil_assert(std::is_sorted(
				conditionedDimensions.cbegin(),
				conditionedDimensions.cend()
				));

	//Return the list o samples with a condition
	return conditionedDimensions;
}; //End: get dimensions with a conditions

yggdrasil_multiCondition_t::ValueArray_t
yggdrasil_multiCondition_t::getConditionedValues()
 const
{
	yggdrasil_assert(this->isValid());	//For the sake of debugging
	if(this->m_nDims == 0)			//Only partial test id needed.
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to use an invalid condition object.");
	};

	//Shortcut of no condition in place
	if(m_conditions.size() == 0)
	{
		//No condition is in place, so we return the empty vector
		return ValueArray_t();
	}; //End if: no condition is in place

	//Create and allocate the result variable
	ValueArray_t conditionedValues;
	conditionedValues.reserve(m_conditions.size());	//Reserve space of later fast insertion

	//Load the list of the condirtioned dimensions
	//This is needed for guaranteeing that the order is the same
	const DimensionList_t conDims = this->getConditionedDimensions();

	//Now we iterate through the dimensions and load the conditions of them
	for(const auto cDim : conDims)
	{
		//Get the condition of teh dimension
		const auto cond = this->getConditionFor(cDim);
		yggdrasil_assert(cond.isValid()); 	//this has to hold

		//Now insert the dimension value
		conditionedValues.push_back(cond.getConditionValue());
	}; //End for(cDim): iterating over the restricted dimensions

	yggdrasil_assert(conditionedValues.size() == m_conditions.size());

	//Return the list o samples with a condition
	return conditionedValues;
}; //End: get dimensions with a conditions


YGGDRASIL_NS_END(yggdrasil)



