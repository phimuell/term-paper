#pragma once
/**
 * \brief	This file implements a condition in one dimension.
 *
 * This is a building block of the tree generator.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>


//INcluide std
#include <string>


YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \brief	This class encodes a single condition.
 * \class 	yggdrasil_singleDimCondition_t
 *
 * This class is a condition.
 * A cxondition is like given in dimension n a value of x.
 * Thus this class is a pair of an int and a double.
 * The int encodes the dimension where the condition has to be applied,
 * and the float encodes the location/value of the component in that dimension.
 *
 * Note that this class does not store if the condition is in global
 * data space or in the root data space or some other space.
 *
 * It is a building block that, on its own is rather useless.
 *
 * Note that we here use zero based indexing.
 * This means the first dimension is zero.
 *
 */
class yggdrasil_singleDimCondition_t
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using Numeric_t 	= ::yggdrasil::Numeric_t;	//!< This is the numeric type

	/*
	 * =================
	 * Constructors
	 *
	 * All are defaulted
	 */
public:
	/**
	 * \brief	Building constructor
	 *
	 * This is the building constructor.
	 *
	 * \param  condValue 	The value that the condition should have.
	 * \param  condDim 	The dimension in which the condition should be applied.
	 *
	 * \throw 	If the arguments results in an invalid condition
	 *
	 * \note	It was decided that the order is stupid and it was changed.
	 */
	yggdrasil_singleDimCondition_t(
		const Int_t 		condDim,
		const Numeric_t 	condValue)
	 :
	  m_condValue(condValue),
	  m_condDim(condDim)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The arguments for the consdition resulted in an invalid object. They where " + std::to_string(condValue) + " and " + std::to_string(condDim));
		};
	};


	/**
	 * \brief	Building constructor 2.
	 *
	 * In order to avoid implicit casting.
	 * The constructor with the swaped arguments is deleted.
	 */
	yggdrasil_singleDimCondition_t(
		const Numeric_t 	condValue,
		const Int_t 		condDim)
	 = delete;



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleDimCondition_t(
		const yggdrasil_singleDimCondition_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleDimCondition_t(
		yggdrasil_singleDimCondition_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleDimCondition_t&
	operator= (
		const yggdrasil_singleDimCondition_t&) = default;


	/**
	 * \brief 	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleDimCondition_t&
	operator= (
		yggdrasil_singleDimCondition_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_singleDimCondition_t() = default;


	/*
	 * ====================
	 * Private Constructors
	 */
private:
	/**
	 * \brief	The default constructor.
	 *
	 * This constructor will make an invalid condition.
	 * It is prrivate, thus can not be called ditectly
	 */
	yggdrasil_singleDimCondition_t()
	 noexcept
	 :
	  m_condValue(NAN),
	  m_condDim(Constants::YG_INVALID_DIMENSION)
	{
		yggdrasil_assert(this->isValid() == false);
	}; //End: default



	/*
	 * =====================
	 * Functions
	 */
public:
	/**
	 * \brief	Returns true if *this is valid.
	 *
	 * A valid conndition is what you generally expects.
	 * Note that beside the fact that the value can not
	 * have any funny values there are no restrictions
	 * that could be applied to it.
	 *
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		if(isValidFloat(m_condValue) == false)
		{
			return false;
		};


		if(m_condDim == Constants::YG_INVALID_DIMENSION)
		{
			return false;
		};

		if(m_condDim < 0)
		{
			return false;
		};

		if(m_condDim >= Constants::YG_MAX_DIMENSIONS)
		{
			return false;
		};

		return true;
	}; //End: isValid


	/**
	 * \brief	Returns the dimension in which the condition should be applied.
	 *
	 * Note that we use zero based indexing.
	 * This means that the counting starts at zero and not at one.
	 */
	Int_t
	getConditionDim()
	 const
	 noexcept
	{
		yggdrasil_assert(this->isValid());
		return m_condDim;
	};


	/**
	 * \brief	Returns the value of teh condition.
	 */
	Numeric_t
	getConditionValue()
	 const
	 noexcept
	{
		yggdrasil_assert(this->isValid());
		return m_condValue;
	};


	/**
	 * \brief	This fucntion returns a string ot *this.
	 *
	 * The string will have the following from
	 * 	(dim: val)
	 * where val is the value of the condition and dim is
	 * the dimension in which we apply the restriction
	 */
	std::string
	print()
	 const
	{
		//Test if it is invalid
		if(this->isValid() == false)
		{
			return std::string("(INVALID)");
		}; //ENd if: handling teh invalid case

		//Now handling the not invalid part
		return std::string("(" + std::to_string(m_condDim) + ": " + std::to_string(m_condValue) + ")");
	}; //ENd print




	/*
	 * ====================
	 * Operators
	 *
	 * are needed for some algorithm
	 */
public:
	/**
	 * \brief	This function allows sorting with
	 * 		 respeect to the dimensions.
	 *
	 * This function returns true if the dimension of
	 * *this is less than the dimension of rhs.
	 *
	 * \param  rhs 	The other condition
	 */
	bool
	operator< (
		const yggdrasil_singleDimCondition_t& 	rhs)
	 const
	{
		//Test if the argunments are valid
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(rhs.isValid());

		//Make the comarison
		return (this->m_condDim < rhs.m_condDim) ? true : false;
	}; //End: operator<


	/**
	 * \brief	Tests if the dimension of *this this and
	 * 		 rhs are the same.
	 *
	 * \param  rhs		The other condition.
	 */
	bool
	operator== (
		const yggdrasil_singleDimCondition_t& 	rhs)
	 const
	{
		//Test if the argunments are valid
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(rhs.isValid());

		//Make the comarison
		return (this->m_condDim == rhs.m_condDim) ? true : false;
	}; //End: operator==



	/*
	 * ==================
	 * Static functions
	 */
public:
	/**
	 * \brief	This fucntion will create an invalid condition.
	 *
	 * It basically calls the default constructor.
	 * However there are no guarantees about the values.
	 * The only guarantee that is given is that isValid() returns false
	 */
	static
	yggdrasil_singleDimCondition_t
	CREAT_INVALID_CONDITION()
	{
		return yggdrasil_singleDimCondition_t();
	}; //ENd: make invalid condi



	/*
	 * =================
	 * Private Memebers
	 */
private:
	Numeric_t 	m_condValue;		//!< This is the value that the dimension must have
	Int_t 		m_condDim;		//!< This is the dimension where the condition must be applied.
}; //End class: yggdrasil_singleDimCondition_t



YGGDRASIL_NS_END(yggdrasil)



