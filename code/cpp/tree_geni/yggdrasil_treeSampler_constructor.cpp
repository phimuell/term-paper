/**
 * \brief	This file implements the constructors for the tree sampler.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>

#include <util/yggdrasil_util.hpp>

#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>
#include <tree_geni/ygInternal_leafProxy.hpp>
#include <tree_geni/yggdrasil_treeSampler.hpp>

#include <random/yggdrasil_random.hpp>

//INcluide std
#include <vector>
#include <random>


YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_treeSampler_t::yggdrasil_treeSampler_t(
	const Tree_t& 			tree,
	const MultiCondition_t 		condi)
 :
  m_nSamples(tree.getMass()),			//Mass is also the number of nodes
  m_leafList(),					//Not yet possible to fill
  m_globDataSpace(tree.getGlobalDataSpace()),	//Get the data space
  m_rootConditions(tree.nDims()),		//We can not fill them yet
  						//This function will also do the tests on the dimension
  m_leafSelecter(),				//A defaulted selecter
  m_uniformSampler(0.0, 1.0),			//Sampler needed for the generation of the inversion.
  m_trafoRootToGlob(NAN)
{
	/*
	 * Make some tests
	 */
	if(m_nSamples == 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The tree has zero samples in it.");
	};
	if(condi.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The condition passed to *this is not valid.");
	};
	if(m_globDataSpace.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The global data space is not valid.");
	};
	yggdrasil_assert(tree.isFullySplittedTree() == true);

	/*
	 * Setting the transformation value.
	 * This is the inverse of the volume and will be set
	 */
	m_trafoRootToGlob = Real_t(1.0) / m_globDataSpace.getVol();


	/*
	 * If the condition does not intersect with the
	 * global data space it will also not intersect with
	 * the cube of the leafs, so we test it here.
	 *
	 * As we have said this will not cause an error, but the
	 * leaf list will be empty, previously the conditions was
	 * set to the empty condition. Then it was the transformed
	 * condition. Since this caused problem we defined it to the
	 * the invalid condition.
	 */
	if(m_globDataSpace.intersectsWithCondition(condi) == false)
	{
		/*
		 * Conditions are too tight
		 */

		//We set the condition to the root condition
		m_rootConditions = MultiCondition_t::CREAT_INVALID_CONDITION();

		if(this->notOverConstrained() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("No intersection between the conditions and the root data space, but this was not selected as overconstrained.");
		};

		//The leaf list must be empty
		yggdrasil_assert(m_leafList.empty());

		//We are done
		return;
	}; //End if: no intersection of conditions with global data space

	/*
	 * We must transform the root conditions
	 * from the global data space to the
	 * root data space.
	 *
	 * The transformation can only be made here.
	 * The reason is that a condition can only be transfered.
	 * if it lies inside the unit cube.
	 * So we must first check that.
	 */
	m_rootConditions = m_globDataSpace.contractToUnit(condi);


	/*
	 * Iterate over all the leafs to find the ones that
	 * fullfill teh conditions.
	 *
	 * We also calculated their probability weights.
	 * At this point we will conceptionaly splitt the sample vector,
	 * x \in R^{d} into \vec{x} = (x_1, x_2, \ldots, x_q \vert x_{q + 1}, \ldots, x_d),
	 * The last part of the sample vector, the one thoese index is larger than
	 * q, are given and are denoted by \vec{x}^{c}, the other one are denoted by
	 * \vec{x}^{f}, the paper uses \prme, but I do not like it.
	 *
	 * According to them the probability weight of C_k, the kth leaf,
	 * is given as:
	 * 	\frac{1}{p(\vec{x}^{c})} \frac{n(C_k)}{n_T} \prod_{i = q + 1}^{d} p[x_i \vert \vec{\theta_i}(C_k)]
	 *
	 * The first factor is independend from the leaf and only depends on some global
	 * properties, it can thus be ignored.
	 * Actually it is the normalization factor, so it can be computed, and will be,
	 * after all valus are calculated.
	 *
	 * The second factor, \frac{n(C_k)}{n_T}, is the factor that is already there for
	 * sample sin the module, it is the number of sample in the kth leaf divided by the total
	 * number of sampels in the tree.
	 *
	 * The last part, the product, is the probability evaluated at the conditioned points.
	 * Note that this is the probability on the global data space.
	 * The transformation factor is not the usual inverse volume of global data space,
	 * but only the extent in that dimension.
	 * Since it is the same for all leafes we will the scalling factor not include
	 * into the calculation of the weights, since it is the same it will be canceled out.
	 * However it will enter the normalization factor after we have calculated the sum
	 * of the weights.
	 */

	//This is the weight vector, it is not needed and passed to the sampler object
	//It will contains the modified weight if the leafs
	WeightVector_t leafWeights;	//We will not preallocate them and hope for the best

	Size_t nTotNodes    = 0;
	Size_t choosenNodes = 0;

	/*
	 * Now we will iterate through the leafs and check them
	 */
	const LeafIterator_t endOfTree = LeafIterator_t();	//This is teh end iterator
	for(auto cLeafIt = tree.getLeafIterator(); cLeafIt != endOfTree; ++cLeafIt)
	{
		//Increse the counter of the nodes that we have selected
		nTotNodes += 1;

		//Make some tests
		yggdrasil_assert(cLeafIt.isEnd() == false);
		yggdrasil_assert(cLeafIt->isLeaf());

		//Access the iterator to get a node facade
		const NodeFacade_t cLeaf = cLeafIt.access();

		//Get the domain of the leaf
		const auto cLeafCube = cLeaf.getDomain();

		//Test if the domain intersects
		if(cLeafCube.intersectsWithCondition(m_rootConditions) == true)
		{
			/*
			 * We have found a leaf that intersets with the conditions.
			 * So we have to create a proxy and to create calculate the
			 * probability weight
			 */

			//Increase the count of the nodes that we use
			choosenNodes += 1;



			/*
			 * Create the proxy
			 *
			 * Note the proxy needs the conditions expressed in
			 * rescalled node data space, hower the transformation
			 * will be applyed by the conditurctor.
			 * It needs the condition on the root data space
			 */

			//Create the proxy; use the rescalled conditions
			m_leafList.emplace_back(cLeaf, m_rootConditions);	//Will call the constructor


			/*
			 * Calculate the probability weights
			 */

			//This is teh first factor, the number of nodes in the leaf
			//by the number of total nodes
			const Real_t firstFactor = cLeaf.getMass() / Real_t(m_nSamples);

			//Get the model
			const auto cLeafParModel = cLeaf.getParModel();

			//This is teh accumulator for the weight
			Real_t prodWeightAccu = 1.0;

			//Create the rescalled conditions
			const MultiCondition_t rescalledNodeDSCondi = cLeafCube.contractToUnit(m_rootConditions);

			/*
			 * Going throught the conditions to get them
			 *
			 * It is important that we iterate over the condition
			 * expressed in rescalled data range, also note that the
			 * evaluateModelInSingleDim() returns the pdf expressed on
			 * the root data space and we will not transform it to
			 * the orginal data space, see also the note above.
			 *
			 * First the function expects its argument to be in the
			 * rescalled node space, so we must iterate over them.
			 *
			 * To seconds we note that it does not matter.
			 * The reason is that the factor that is used for
			 * the transformation is teh same for every node,
			 * so it cancels out, so as the \frac{1}{p(\vec{x}^{c})}
			 * was droped, because it is the same for all.
			 */
			for(const SingleCondition_t& sCondi : rescalledNodeDSCondi)
			{
				//Get the data
				const Size_t cDim = sCondi.getConditionDim();
				const auto   cVal = sCondi.getConditionValue();
				yggdrasil_assert(cDim < cLeafCube.size());

				//caluclate contribution of the dimension
				//The result calculated by this function is expresed in the rescalled root data
				//space, since the factor that transforms such a value to the global data space is teh
				//same for all leafs, we will not include it.
				const Real_t contriThisLeaf = cLeafParModel->evaluateModelInSingleDim(cVal, cDim, cLeafCube[cDim]);

				//Make some checks
				yggdrasil_assert(isValidFloat(contriThisLeaf));
				yggdrasil_assert(contriThisLeaf >= 0.0);	//We allow zero, is a buit stupid, but hwo cares

				//Accumulate the value
				prodWeightAccu *= contriThisLeaf;
			}; //End for(sCondi):

			//Test if the final weight is balid
			yggdrasil_assert(isValidFloat(prodWeightAccu));
			yggdrasil_assert(prodWeightAccu >= 0.0);

			//Now we can calculate the final probability mass
			const Real_t finalProbMass = firstFactor * prodWeightAccu;

			//Now inserting it into the weight vector
			leafWeights.push_back(finalProbMass);
		}; //End if: no intersection is present

		//Both vecvtors must be the same
		yggdrasil_assert(leafWeights.size() == m_leafList.size());

		//
		//We have nothing to do here
	}; //End for(cLeaf)


	/*
	 * If we are here then the data space
	 * contains the conditions.
	 * This means at least some leafs should be seletced.
	 * We regards it as an error if no samples are selected.
	 */
	if(m_leafList.size() == 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("No leaf was sellected to join the set, but the conditions where inside thew original data space.");
	}; //End: no leafs where selected

	if(this->notOverConstrained() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("An overconstrained sampler resulted, but this should not have happened.");
	};

	/*
	 * I realized that my idea was wrong.
	 * So everything in the other _pdf branch is wrong.
	 * This is the part that is apready merged into master. I will deactivate it.
	 */
#if 0
	/*
	 * In order for efficiently or at least more efficient searching we need to bring
	 * the leafs in an order that is more appropriate.
	 * For that we will sort them lexocographic, such that it they are in a better ordering.
	 * We also have to reorder the weight array.
	 */

	//Perform the sorting
	const yggdrasil_indexArray_t newLeafOrdering =
		yggdrasil_argSortContainer(
				m_leafList, 		//The container we want to sort
				//The sorting fucntion that is used
				[](const LeafProxy_t& lhs, const LeafProxy_t& rhs) -> bool
					{return (lhs.getDomain() < rhs.getDomain());}
		); // End argSort fucntion

	//Now we perform the reordering of the container and the leaf weight vector
	yggdrasil_reorderContainer(newLeafOrdering, m_leafList);	//The proxy itself
	yggdrasil_reorderContainer(newLeafOrdering, leafWeights);	//The weights
#endif


	/*
	 * ==== GENERATING THE LEAF SELECTOR ==
	 *
	 * Now we finish this
	 * We do this by creating the leaf selector
	 */
	m_leafSelecter = DisctreteSampler_t(leafWeights.begin(), leafWeights.end());

	/*
	 * Calculating the conditionaed factor.
	 * We now calculate the factor that is needed to transform an unconditionaed
	 * probability to an conditioned probability, this is done by caluclating the
	 * factor p(\vec{x}^{c}) from the paper.
	 * This is an normalization factor of the weights.
	 * We have computed them already, however some part is missing.
	 * This is that we have expressed the probability not on the original
	 * data soace but on the rescalled root data space.
	 *
	 * Including this factor is not that trivial, as itz may apear.
	 * First it is such that it is only present in the case the
	 *
	 *
	 * But this factor can be included easaly into by mutipling
	 * by the transform constant.
	 * The reason is that it is the same for every domain.
	 * However this factor has only to be included in the case of an conditioned sampler.
	 * Since this factor
	 */

	//First we sum up the weights we have somputed thus far.
	const Real_t sumWeightsThusFar = yggdrasil_recursiveSumming(leafWeights);

	/*
	 * Calculating the transformation factor
	 *
	 * We will now calculate the transformation factor that we have
	 * ignored in the computatin of the weights.
	 * We will include it now.
	 * The factor is the inverse product of the length of the dimensions
	 * that we have conditioned.
	 */
	Real_t protOfLength = 1.0;	//This is only the product of the lengths
					// Not the inverse
	for(const SingleCondition_t& sCondi : m_rootConditions)
	{
		//Get the dimension that we conditioned on
		const Size_t cDim = sCondi.getConditionDim();

		//Get the length of that dimension
		const Real_t dimLength = m_globDataSpace.at(cDim).getLengthReal();

		//Test the length
		yggdrasil_assert(isValidFloat(dimLength));
		yggdrasil_assert(dimLength > 0.0);

		//Incooperate the length
		protOfLength *= dimLength;
	}; //End for(sCondi): mult up the weight

	//Now we incooperated the transformation for the real data space.
	//We have to keep in mind that we still have to use the divided version
	const Real_t sumWeightOrgDS = sumWeightsThusFar / protOfLength;

	//Now we transform the value to its purpose
	m_ipxc = 1.0 / sumWeightOrgDS;

	if(isValidFloat(m_ipxc) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The normalization factor for the probability is not a valid number.");
	};
	if(m_ipxc <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The normalization factor for the probability is negative or zero.");
	};

#if 0
	std::cout
		<< "\n"
		<< "NORMAL CONSTANT: " << m_ipxc << "\n"
		<< "TOT NODES: " << nTotNodes << "\n"
		<< "SELECTED: "  << choosenNodes << "\n"
		<< "\n"
		<< std::endl;
#endif
}; //End: building constructor


yggdrasil_treeSampler_t::yggdrasil_treeSampler_t(
	const Tree_t& 		tree)
 :
  yggdrasil_treeSampler_t(tree, MultiCondition_t(tree.nDims()))
{};


yggdrasil_treeSampler_t::yggdrasil_treeSampler_t(
	const Tree_t& 		tree,
	const ConditionMap_t& 	condiMap)
 :
  yggdrasil_treeSampler_t(tree, MultiCondition_t(tree.nDims(), condiMap))
{};


yggdrasil_treeSampler_t::yggdrasil_treeSampler_t(
	const Tree_t& 			tree,
	const ConditionVector_t& 	condiVector)
 :
  yggdrasil_treeSampler_t(tree, MultiCondition_t(tree.nDims(), condiVector))
{};


YGGDRASIL_NS_END(yggdrasil)



