#pragma once
/**
 * \brief	This file implements a condition for several dimension.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleList.hpp>
//#include <samples/yggdrasil_sampleCollection.hpp>
//	In the case I include this header a compilation error happens.
//	I have no real ideay why, but this way iot works.

#include <tree_geni/yggdrasil_dimensionCondition.hpp>

//INcluide std
#include <vector>
#include <map>
#include <algorithm>
#include <string>


YGGDRASIL_NS_BEGIN(yggdrasil)

//This fwd is needed because otherwhise teh collection is not found
class yggdrasil_sampleCollection_t;


/**
 * \brief	This class implements a condition on several dimensions.
 * \class 	yggdrasil_multiCondition_t
 *
 * This class is implemented as an array fo severall conditions.
 * It is used to encode the restriction on several dimensions at once.
 * It is intended as a building block that can be used for several
 * operations needed for the correct working of that class.
 *
 * This class is also able to transform restricted samples to
 * the full form. The restructed form of a sample of dimension d,
 * which is also the underling diemnsion of the sample space, is
 * a sample of dimensions dof (d - nConditions).
 * The first component of that sample is the first comonent where
 * no restriction is applied to and so on.
 */
class yggdrasil_multiCondition_t
{
	/*
	 * =====================
	 * Typedefs
	 */
public:
	using SingleCondition_t 	= yggdrasil_singleDimCondition_t;	//!< This is the calss that models a soingle dimension.
	using ConditionVector_t 	= std::vector<SingleCondition_t>;	//!< This is teh collection of many conditions.
										//!<  This is a standard vector for compabiility with pyYggdrasil.
	using ConditionMap_t 		= std::map<Size_t, Numeric_t>;		//!< THis is a map that encodes the condition, is used for construction.

	using iterator 			= ConditionVector_t::const_iterator;	//!< This is the iterator type, for iterating over the conditions
	using const_iterator 		= ConditionVector_t::const_iterator;	//!< COnstant iterator for iteration over the conditrions

	using Sample_t 			= yggdrasil_sample_t;			//!< This is the sample class.
	using SampleList_t 		= ::yggdrasil::yggdrasil_sampleList_t;	//!< This is the type for a list of samples
	using SampleArray_t		= ::yggdrasil::yggdrasil_arraySample_t;	//!< This is a space efficient way to stoirage a slot of sampels.
	using SampleCollection_t	= yggdrasil_sampleCollection_t;	//!< This is the sample collection type

	using ValueArray_t 		= yggdrasil_valueArray_t<Numeric_t>;	//!< This is an array for Numeric
	using DimensionList_t 		= yggdrasil_indexArray_t;		//!< This is the class that is used to list dimensions.
										//!<  It is a zero baed index.

	/*
	 * ========================
	 * Building Constructors
	 */
public:
	/**
	 * \brief	Builds a multi condition out of a map.
	 *
	 * The map is interpreted such that the the key is the
	 * dimension vehere the condition is applied to and
	 * value is the condition itself.
	 * Also teh number of the smple space must be given.
	 *
	 * Also the length of the condition must be less than the size of
	 * the underling sample space.
	 *
	 * \param  nDims	The number of dimensions that is used.
	 * \param  condi 	The map of conditions.
	 *
	 * \throw 	If a compability condition is vioplated.
	 */
	explicit
	yggdrasil_multiCondition_t(
		const Int_t 				nDims,
		const ConditionMap_t& 			condi);


	/**
	 * \brief	Builds a multio condition out of a vector.
	 *
	 * This constructor takes a list of single dimension conditions.
	 * The conditions must not be in a particular order, but
	 * valid and each dimenion must be unique.
	 *
	 * Also the length of the condition must be less than the size of
	 * the underling sample space.
	 *
	 * \param  nDims 	The dimensionality of teh underling sample space.
	 * \param  condi 	Vector of conditions.
	 *
	 * \throw 	If inconsistencies are detected.
	 */
	explicit
	yggdrasil_multiCondition_t(
		const Int_t 				nDims,
		const std::vector<SingleCondition_t>&	condi);


	/**
	 * \brief	Building constructor.
	 *
	 * This constructors will construct the empty condition.
	 * This means that no conditions are applied.
	 * For several reasons this constructor still needs
	 * the dimension of the unerliny sample space.
	 *
	 * \param  nDims 	The dimensionality of teh underling sample space.
	 */
	explicit
	yggdrasil_multiCondition_t(
		const Int_t 		nDims);


	/*
	 * ======================
	 * Other constructors.
	 *
	 * These are the one that could be made defaultl.
	 */
public:
	/**
	 * \brief 	Copy constructor.
	 *
	 * Defaulted.
	 */
	yggdrasil_multiCondition_t(
		const yggdrasil_multiCondition_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Defaulted.
	 */
	yggdrasil_multiCondition_t&
	operator= (
		const yggdrasil_multiCondition_t&)
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiCondition_t(
		yggdrasil_multiCondition_t&& )
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_multiCondition_t&
	operator= (
		yggdrasil_multiCondition_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Defaulted
	 */
	~yggdrasil_multiCondition_t()
	 = default;


	/*
	 * ================
	 * Private Constructor
	 */
private:
	/**
	 * \brief	Default constructor.
	 *
	 * This constructs an invalid condition
	 */
	yggdrasil_multiCondition_t();



	/*
	 * ====================
	 * Utility functions
	 *
	 * These are important functions
	 * for information retrival
	 */
public:
	/**
	 * \brief	This function returns the number of different
	 * 		 restrictions that are applied.
	 *
	 * This basically returns the size of the condi container
	 * that was passed upon constructing.
	 */
	Size_t
	nConditions()
	 const
	{
		if(m_nDims == 0)	//This is only a partial invalid tester
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Tried to access an invalid condition.");
		};

		return m_conditions.size();
	}; //End: nConditions


	/**
	 * \brief	Thsi returns the size of the underliying sample space.
	 */
	Size_t
	nDims()
	 const
	 noexcept
	{
		return m_nDims;
	};


	/**
	 * \brief	Return the degree of freedomes.
	 *
	 * This is the dimension of the underling sample sapce minus the number of
	 * the conditions that where applied.
	 */
	Size_t
	nDOF()
	 const
	{
		if(m_nDims == 0)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Tried to access an invalid condition.");
		};
		yggdrasil_assert(m_conditions.size() <= m_nDims);

		return Size_t(m_nDims - m_conditions.size());
	}; //End: nDOF



	/**
	 * \brief	Returns true if *this has no restriction.
	 *
	 * This is basically a fast test for nCOnditions() == 0.
	 */
	bool
	noConditions()
	 const
	{
		if(m_nDims == 0)	//This is only a partial invalid tester
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Tried to access an invalid condition.");
		};
		return m_conditions.empty();
	};


	/**
	 * \brief	This function tests if *this is valid.
	 */
	bool
	isValid()
	 const
	 noexcept;


	/**
	 * \brief	This function transforms *this into a string.
	 *
	 * It will produce a textual represenation of the following form:
	 * 	{COND1, ... | nDims}
	 * Where COND1 is the output of the first condition, by calling
	 * SingleCondition_t::print() on it.
	 * At the end the dimension of the underling sample condition is outputted.
	 */
	std::string
	print()
	 const;


	/*
	 * ====================
	 * Accessing
	 */
public:
	/**
	 * \brief	This fucntion returns a const reference to the ith condition.
	 *
	 * This function does not return the condition that is applied to
	 * dimension i, but ith condition. The order of the conditions
	 * is such that the dimensions are sorted such that the accessing happens in accending order.
	 *
	 * \param  i 		The index of the condition that should be accessed
	 */
	const SingleCondition_t&
	operator[](
		const Size_t 	i)
	 const
	{
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(i < m_conditions.size()); 	//Test out of bound
		yggdrasil_assert(m_conditions[i].isValid());	//Test if valid
		yggdrasil_assert(std::is_sorted(m_conditions.cbegin(), m_conditions.cend()));

		return m_conditions[i];
	}; //ENd operator[]


	/**
	 * \brief	This fucntion returns a const reference to the ith condition.
	 *
	 * This function does not return the condition that is applied to
	 * dimension i, but ith condition. The order of the conditions
	 * is such that the dimensions are sorted such that the accessing happens in accending order.
	 *
	 * \param  i 	The index of the condition that should be accessed.
	 *
	 * \throw 	If an out of bound access happens
	 */
	const SingleCondition_t&
	at(
		const Size_t 	i)
	 const
	{
		//TEst if invalid
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access an invalid multi condition.");
		};

		//TEst out of bound
		if(!(i < m_conditions.size()))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("An out of bound access happened, accessed index " + std::to_string(i) + ", but the size is " + std::to_string(m_conditions.size()));
		};

		//Test some invariants
		yggdrasil_assert(m_conditions[i].isValid());
		yggdrasil_assert(std::is_sorted(m_conditions.cbegin(), m_conditions.cend()));

		//Retrun a reference, can not be an out of bound
		return m_conditions[i];
	}; //ENd operator[]


	/**
	 * \brief	This function returns a constant iterator to the first condition
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		return m_conditions.cbegin();
	}; //ENd: begin


	/**
	 * \brief	This function returns the constant past the end itrerator of teh range of conditions.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		return m_conditions.cend();
	};


	/**
	 * \brief	This returns the conditions of *this inside a vector.
	 *
	 * The order of the condition is unspecific.
	 *
	 * \throw 	If *this is invalid.
	 */
	ConditionVector_t
	getConditionVector()
	 const;


	/**
	 * \brief	This fucntion returns a represention of a map of *this.
	 *
	 * The encoding is teh same as the map that is accesped as constructor.
	 *
	 * \throw 	If *this is invalid.
	 */
	ConditionMap_t
	getConditionMap()
	 const;


	/*
	 * =========================
	 * Dimension functions
	 */
public:
	/**
	 * \brief	Thsi function returns all dimensions that have a conditions.
	 *
	 * This function only returns the dimensions (zero basied indexes) that
	 * a condition is applied to.
	 *
	 * There is no guarantee about the ordering of the dimensions.
	 *
	 * \throw 	In the case *this is invalid.
	 */
	DimensionList_t
	getConditionedDimensions()
	 const;


	/**
	 * \brief	This function returns a vector that contains the values
	 * 		 That are applied.
	 *
	 * The ordering of the value is teh same as the one of the
	 * getConditionedDimensions() function.
	 *
	 * \throw 	If *thsi is invalid.
	 */
	ValueArray_t
	getConditionedValues()
	 const;


	/**
	 * \brief	This function returns a list of the free dimensions.
	 *
	 * The free dimensions are the dimensions where no condition is applied to.
	 *
	 * \throw 	If this is detected to be invalid.
	 */
	DimensionList_t
	getFreeDimensions()
	 const;




	/*
	 * ======================
	 * Sample Transformation Functions
	 *
	 * These fucntions deals with the transformation of samples
	 * from a restricted for to the full form.
	 */
public:
	/**
	 * \brief	This function transforms the restricted sample s
	 * 		 to its full form.
	 *
	 * This fucntion basiclally expands the samples to length d, which
	 * is the underling dimension and then sets all components, that are
	 * restricted to their respective values.
	 *
	 * \param  s 		The restricted sample.
	 *
	 * \throw 	If an inconsistency is detected.
	 */
	Sample_t
	expandSample(
		const Sample_t& 	s)
	 const;


	/**
	 * \brief	This function performs the compressing of the sample s.
	 *
	 * The compressing is done by removing all components of the sample
	 * that have a condition, for the sake of explanation they are set to a
	 * special value q. Then all elements, which are not set to q are copied
	 * into a new sample. The resulting sample will have dimension dof.
	 * Note that the dimension of samples
	 *
	 * \param  s		The sample that should be restricted.
	 *
	 * \throw 	If an inconsistency is detected.
	 *
	 * \note	The values of the components of sample s, where
	 * 		 restrictions are applied, have no meaning and will be ignored.
	 */
	Sample_t
	restrictSample(
		const Sample_t& 	s)
	 const;


	/*
	 * \brief	This function expads all samples in the sample list.
	 *
	 * This function performs expanding of the samples on a sample list container.
	 *
	 * \param  s 	The sample list that should be expanded
	 *
	 * \throw 	If inconsistencies are detected.
	 */
	SampleList_t
	expandSample(
		const SampleList_t& 	s)
	 const;


	/*
	 * \brief	This functiuon performs restriction of samples.
	 *
	 * This function performs teh restriction operators, removing all dimensions
	 * where a condition is applied from the samples that are stored inside
	 * sample list s, that is passed as an argument.
	 *
	 * \param  s	The sample list that should be used.
	 *
	 * \throw 	If inconsistencies are detected.
	 *
	 * \note	The values of the components of sample s, where
	 * 		 restrictions are applied, have no meaning and will be ignored.
	 */
	SampleList_t
	restrictSample(
		const SampleList_t& 	s)
	 const;



	/*
	 * \brief	This function expads all samples in the sample array.
	 *
	 * This function performs expanding of the samples on a sample array container.
	 *
	 * \param  s 	The sample array that should be expanded
	 *
	 * \throw 	If inconsistencies are detected.
	 */
	SampleArray_t
	expandSample(
		const SampleArray_t& 	s)
	 const;


	/*
	 * \brief	This functiuon performs restriction of samples.
	 *
	 * This function performs teh restriction operators, removing all dimensions
	 * where a condition is applied from the samples that are stored inside
	 * sample array s, that is passed as an argument.
	 *
	 * \param  s	The sample array that should be used.
	 *
	 * \throw 	If inconsistencies are detected.
	 *
	 * \note	The values of the components of sample s, where
	 * 		 restrictions are applied, have no meaning and will be ignored.
	 */
	SampleArray_t
	restrictSample(
		const SampleArray_t& 	s)
	 const;


	/*
	 * \brief	This function expads all samples in the sample collection.
	 *
	 * This function performs expanding of the samples on a sample collection container.
	 *
	 * \param  s 	The sample collection that should be expanded
	 *
	 * \throw 	If inconsistencies are detected.
	 *
	 * \note 	These function uses a generic version of the expanding algorithm,
	 * 		 that does not explit the structure of the collection.
	 */
	SampleCollection_t
	expandSample(
		const SampleCollection_t& 	s)
	 const;


	/*
	 * \brief	This functiuon performs restriction of samples.
	 *
	 * This function performs teh restriction operators, removing all dimensions
	 * where a condition is applied from the samples that are stored inside
	 * sample collection s, that is passed as an argument.
	 *
	 * \param  s	The sample collection that should be used.
	 *
	 * \throw 	If inconsistencies are detected.
	 *
	 * \note	The values of the components of sample s, where
	 * 		 restrictions are applied, have no meaning and will be ignored.
	 *
	 * \note 	These function uses a generic version of the restriction algorithm,
	 * 		 that does not explit the structure of the collection.
	 */
	SampleCollection_t
	restrictSample(
		const SampleCollection_t& 	s)
	 const;


	/*
	 * =========================
	 * Appyling Conditions
	 */
public:
	/**
	 * \brief	This function applies the conditions of *this to the sample s.
	 *
	 * Appling menas that all components that are restricted, a condition is applied to them,
	 * are modifed. The values of these components are set to the values given by the
	 * conditions.
	 * Formaly this fucntion is the same as first restrict the sample and then
	 * expanding the result again. But without the need of creating a temporary.
	 *
	 * The sample s is modified and needs to have the same dimension as the underyling
	 * sample sample of *this. The components of the values are not checked.
	 *
	 * \param  s	The sample that should be modified.
	 *
	 * \return 	A reference to the modified sample s (returns argument).
	 *
	 * \throw 	If inconsistencies are detected.
	 */
	Sample_t&
	applyConditions(
		Sample_t& 	s)
	 const;


	/**
	 * \brief	This function applies the conditions of *this to all samples
	 * 		 that are stored inside the sample list s.
	 *
	 * Appling menas that all components that are restricted, a condition is applied to them,
	 * are modifed. The values of these components are set to the values given by the
	 * conditions.
	 * Formaly this fucntion is the same as first restrict the sample and then
	 * expanding the result again. But without the need of creating a temporary.
	 *
	 * The container s is modified and needs to have the same dimension as the underyling
	 * sample sample of *this. The components of the values are not checked.
	 *
	 * \param  s	The sample that should be modified.
	 *
	 * \return 	A reference to the modified sample s (returns argument).
	 *
	 * \throw 	If inconsistencies are detected.
	 */
	SampleList_t&
	applyConditions(
		SampleList_t& 	s)
	 const;


	/**
	 * \brief	This function applies the conditions of *this to all samples
	 * 		 that are stored inside the sample array s.
	 *
	 * Appling menas that all components that are restricted, a condition is applied to them,
	 * are modifed. The values of these components are set to the values given by the
	 * conditions.
	 * Formaly this fucntion is the same as first restrict the sample and then
	 * expanding the result again. But without the need of creating a temporary.
	 *
	 * The container s is modified and needs to have the same dimension as the underyling
	 * sample sample of *this. The components of the values are not checked.
	 *
	 * \param  s	The sample that should be modified.
	 *
	 * \return 	A reference to the modified sample s (returns argument).
	 *
	 * \throw 	If inconsistencies are detected.
	 */
	SampleArray_t&
	applyConditions(
		SampleArray_t& 	s)
	 const;


	/**
	 * \brief	This function applies the conditions of *this to all samples
	 * 		 that are stored inside the sample Collection s.
	 *
	 * Appling menas that all components that are restricted, a condition is applied to them,
	 * are modifed. The values of these components are set to the values given by the
	 * conditions.
	 * Formaly this fucntion is the same as first restrict the sample and then
	 * expanding the result again. But without the need of creating a temporary.
	 *
	 * The container s is modified and needs to have the same dimension as the underyling
	 * sample sample of *this. The components of the values are not checked.
	 *
	 * \param  s	The sample that should be modified.
	 *
	 * \return 	A reference to the modified sample s (returns argument).
	 *
	 * \throw 	If inconsistencies are detected.
	 */
	SampleCollection_t&
	applyConditions(
		SampleCollection_t& 	s)
	 const;



	/*
	 * ====================
	 * Quering functions
	 */
public:
	/**
	 * \brief	This function returns true if *this has a condition
	 * 		 for dimension d.
	 *
	 * This function searches all the conditions and test, if the dimension
	 * is known.
	 * This fucntion is efficient.
	 *
	 * \param  d 	The dimension to query.
	 *
	 * \throw 	If the dimension exceends values
	 */
	bool
	hasConditionFor(
		const Int_t 	d)
	 const
	{
		yggdrasil_assert(this->isValid());

		//Test if d is valid
		if(d < 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension is negative.");
		};

		//TEst out of bound
		if(!(Size_t(d) < m_nDims))	//This will also be triggered in invalid cases
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("An out of bound access happened, accessed index " + std::to_string(d) + ", but the size is " + std::to_string(m_nDims));
		};

		//Now we search for the dimension
		const auto foundIT = std::lower_bound(
				m_conditions.cbegin(), m_conditions.cend(),			//Range to search
				d,								//Value to search for
				[](const SingleCondition_t& c, const Int_t d) -> bool		//Function to use
					{return (c.getConditionDim() < d) ? true : false;}
		);

		//Test if the end iterator was found or if the condition was not found
		if(foundIT == m_conditions.cend())
		{
			//The end iterator was found, so we can not access foundIT
			return false;
		}; //End: noting was found

		//Now we test if teh found is for the same dimension
		return (foundIT->getConditionDim() == d) ? true : false;
	}; //ENd: hasn restruiction


	/**
	 * \brief	This function returns the condition of diomension d.
	 *
	 * This function rely returns a condition that is associated to a dimension.
	 * Note that if the condition is not knnown an invalid condition is returned.
	 * This can not be accessed or rather should not be accessed.
	 *
	 * \param  d 	The dimension we want the condition for.
	 *
	 * \throw 	If inconsistencies are detected.
	 */
	SingleCondition_t
	getConditionFor(
		const Int_t 	d)
	 const
	{
		yggdrasil_assert(this->isValid());

		//Test if d is valid
		if(d < 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension is negative.");
		};

		//TEst out of bound
		if(!(Size_t(d) < m_nDims))	//Will also be triggered in invalid cases, also according to our definition
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("An out of bound access happened, accessed index " + std::to_string(d) + ", but the size is " + std::to_string(m_nDims));
		};

		//Now we search for the dimension
		const auto foundIT = std::lower_bound(
				m_conditions.cbegin(), m_conditions.cend(),			//Range to search
				d,								//Value to search for
				[](const SingleCondition_t& c, const Int_t d) -> bool		//Comaprer to use
					{return (c.getConditionDim() < d) ? true : false;}
		);

		//Test if the end iterator was found or if the condition was not found
		if(foundIT == m_conditions.cend())
		{
			//The end iterator was found, so we can not access foundIT
			return SingleCondition_t::CREAT_INVALID_CONDITION();
		}; //End: noting was found

		//Now we test if teh found is for the same dimension
		return (foundIT->getConditionDim() == d) ? (*foundIT) : SingleCondition_t::CREAT_INVALID_CONDITION();
	}; //End: getRestriction


	/*
	 * ======================
	 * Static fucntion
	 */
public:
	/**
	 * \brief	This function will construct an invalid condition.
	 *
	 * It is ment for internal use only, since an invalid
	 * condition should never occure under normal circumstances.
	 */
	static
	yggdrasil_multiCondition_t
	CREAT_INVALID_CONDITION()
	{
		return yggdrasil_multiCondition_t();
	}; //End create invalid conditrioon



	/*
	 * =====================
	 * Private functions
	 */
private:
	/**
	 * \brief	This fucntion returns true if the the restriction is known.
	 *
	 * This fucntion only searches for the dimension,
	 * by using operator== of the single dimension restriction.
	 * Note that this function is slow and not potimal,
	 * but it is internal any only used in certain cases.
	 *
	 * \param  condi 	The condition, or rather its dimension, that should be searched for.
	 */
	bool
	internal_knownCondition(
		const SingleCondition_t&	 condi)
	 const;


	/*
	 * =========================
	 * Frieds
	 */
private:
	/**
	 * \brief 	This fucntion performs the expanding of all samples inside the container.
	 *
	 * \param  self		This is a constant multi conndtion reference of *this.
	 * \param  samples 	This is a sample container of restricted samples.
	 *
	 * \tparam  Container_t		This is the sample container that is used.
	 *
	 */
	template<
		class 	Conatiner_t
	>
	friend
	Conatiner_t
	ygInternal_expandSampleContainer(
		const yggdrasil_multiCondition_t& 	self,
		const Conatiner_t& 			samples);



	/**
	 * \brief	This function performs the restriction of all
	 * 		 the samples in the smaple container.
	 *
	 * \param  self		This is the constant multi condition that is used for the restriction.
	 * \param  samples 	This is the container of samples that should be expanded.
	 *
	 * \tparam  Container_t		This is the sample container that is used.
	 *
	 */
	template<
		class 	Conatiner_t
	>
	friend
	Conatiner_t
	ygInternal_restrictSampleContainer(
		const yggdrasil_multiCondition_t& 	self,
		const Conatiner_t& 			samples);




	/*
	 * =======================
	 * Private Memebbers
	 */
public:
	Size_t 			m_nDims;		//This is teh dimension of the underling sample sapce
	ConditionVector_t	m_conditions;		//This vector stores all the conditions
}; //End class(yggdrasil_multiCondition_t)



YGGDRASIL_NS_END(yggdrasil)



