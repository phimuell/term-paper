/**
 * \brief	This file implements the computation of a sample that is conditioned
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>

#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>
#include <tree_geni/ygInternal_leafProxy.hpp>
#include <tree_geni/yggdrasil_treeSampler.hpp>

#include <samples/yggdrasil_sample.hpp>

#include <random/yggdrasil_random.hpp>

//INcluide std
#include <vector>
#include <random>


YGGDRASIL_NS_BEGIN(yggdrasil)


std::pair<yggdrasil_treeSampler_t::Sample_t, Real_t>
yggdrasil_treeSampler_t::wh_computePDF(
		const Sample_t& 	x)
 const
{
	if(m_leafList.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("A sampler with an emptry list can not be used.");
	};

	//We enforce that we can do smoething
	yggdrasil_assert(this->notOverConstrained() == true);

	//This is the dimension
	const Size_t D = m_globDataSpace.nDims();

	//This is the number of constarints
	const Size_t nCondi = m_rootConditions.nConditions();

	//Test if everything is consistent
	yggdrasil_assert(D > 0);
	yggdrasil_assert(D >= nCondi);

	//This is teh number of free parameters we have
	const Size_t DOF = D - nCondi;

	//Test if the passed sample is correct
	if(DOF != Size_t(x.nDims()))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The sample had a dimension of " + std::to_string(x.nDims()) + " but the degrees of freedom is just " + std::to_string(DOF));
	}; //End if: check dofs


	/*
	 * Now we reconstruct the sample
	 * for that we must transfer it to the root data space
	 * We can not use the transform functions of the cube.
	 * Because of the dimension problems, we must do it by
	 * hand, at the same time we will also restore the sample.
	 */

	//This is teh restored sample, expressed on root data space
	Sample_t rootDSSample(D, NAN);

	//This is the index for the x sample
	Size_t xIdx = 0;

	//Now going through the dimension and apply them
	for(Size_t i = 0; i != D; ++i)
	{
		//Test if we have a constraint ot not
		if(m_rootConditions.hasConditionFor(i) == true)
		{
			/*
			 * We have a condition for thsi dimension.
			 * this means we just have to load the value that
			 * is stored inside
			 */

			//Load the condition
			const SingleCondition_t& sCond = m_rootConditions.getConditionFor(i);

			//Load the value
			const Numeric_t cVal = sCond.getConditionValue();

			//Set the component, the value is already in the right space
			rootDSSample[i] = cVal;
		}
		else
		{
			/*
			 * In this case we have no condition, thsi means
			 * we must transform the value by hand
			 */

			//We load the value of the sample, for that we mst use the xIdx
			const Numeric_t xVal = x[xIdx];

			//We must now transform it
			const Numeric_t xValRoot = m_globDataSpace[i].contractToUnit(xVal);

			//Now we write it into the root sample
			rootDSSample[i] = xValRoot;

			//Increase the xIndex, since we have handled a component
			xIdx += 1;
		}; //End else
	}; //End for(i): going through the dimensions

	//It must be the case that all elements were read
	yggdrasil_assert(xIdx == x.size());

	/*
	 * Now we have a sample that lieves on the root data space and that
	 * has the restriction applied to it
	 */
	yggdrasil_assert(rootDSSample.isValid());
	yggdrasil_assert(rootDSSample.isInsideUnit());

	/*
	 * Now we search for the cube where the sample belongs to.
	 * for that we will iterate throught the cubes and
	 * test if teh sample lies inside teh cube.
	 *
	 * This is extremly inefficient, since we must iterate
	 * through all the leafs.
	 */

	//This variable is the pdf of the sample
	//expressed in toot data space
	//The value is uncodnitioned.
	Real_t rootDSPdf_unCond = NAN;

	for(const LeafProxy_t& proxy : m_leafList)
	{
		//Get the domain of the leaf
		const HyperCube_t& proxDomain = proxy.getDomain();

		//Test the domain
		yggdrasil_assert(proxDomain.isValid());

		//Test if the sample lies inside the domain
		//tghat is occupied by the current node
		if(proxDomain.isInside(rootDSSample) == true)
		{
			//Compute the unconditioned probability on the root data space
			rootDSPdf_unCond = proxy.computeUnCondPDF(rootDSSample, m_nSamples);

			//Test the returned values
			if(isValidFloat(rootDSPdf_unCond) == false)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("COmputed an invalid probability.");
			};
			if(rootDSPdf_unCond < 0.0)	//We allow zero
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("A negative probability was computed.");
			};

			/*
			 * We have found and computed the value
			 * so we can end the iteration
			 */
			break;
		}; //End if: the sample lies inside that domain
	}; //End for(proxy):

	/*
	 * It could be possible that we doid not found something
	 */
	if(isValidFloat(rootDSPdf_unCond) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("No leaf was found that contained the sample.");
	};


	/*
	 * Now we must transform the samples to global data space
	 */
	m_globDataSpace.mapFromUnit_inPlace(rootDSSample);	//Note this is inplace it is a bit missleadig

	/*
	 * Now we must transform the probability.
	 * The probability is expressed in root data space
	 * it must thus be mapped back to global data space.
	 * In a second approce we must also accound for
	 * the conditioning
	 */

	//Map back to global
	const Real_t globDSPdf_unCond = rootDSPdf_unCond * m_trafoRootToGlob;

	//Now we account for the conditioning
	const Real_t globDSPdf_cond = globDSPdf_unCond * m_ipxc;


	/*
	 * Return
	 */
	return std::make_pair(std::move(rootDSSample), globDSPdf_cond);
}; //End: workhorse function



YGGDRASIL_NS_END(yggdrasil)



