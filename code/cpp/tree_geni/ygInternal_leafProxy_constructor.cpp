/**
 * \brief	This function implements the constructors of the leaf proxy.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>
#include <tree_geni/ygInternal_leafProxy.hpp>


//INcluide std
#include <memory>


YGGDRASIL_NS_BEGIN(yggdrasil)

ygInternal_leafProxy_t::ygInternal_leafProxy_t(
	const NodeFacade_t& 	node,
	const MultiConditon_t&	rootDSCondi)
 :
  m_nodeDomain(HyperCube_t::MAKE_INVALID_CUBE()),
  m_parModel(nullptr),
  m_reNDSCondi(node.nDims())	//This is default constructed, we must transform it
{
	//Test if the node is valid
	if(node->checkIntegrity() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node that was passed to the proxy is not intger.");
	};
	if(node->isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The passed node is not a leaf.");
	};
	if(node->isFullySplittedLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The leaf is not fully splitted.");
	};

	/*
	 * Copy the data domain.
	 */
	m_nodeDomain = node->getDomain();

	//Enforce the valid domain
	if(m_nodeDomain.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The valid leaf returned an invalid domain.");
	};


	/*
	 * Copy the model
	 */
	yggdrasil_assert(node->hasParModel());
	m_parModel = node->getParModel()->clone();

	//Test if teh nodel is fitted
	if(m_parModel == nullptr)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The clone function of the model returned a null pointer.");
	};
	if(m_parModel->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The cloned model is not associated.");
	};
	if(m_parModel->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The model is not completly fitted.");
	};

	/*
	 * Conditions
	 */

	//Test if the condition is valid
	if(rootDSCondi.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The conditions are not valid.");
	};
	if(rootDSCondi.nDims() != Size_t(m_nodeDomain.nDims()))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the conditions " + std::to_string(rootDSCondi.nDims()) + ", and the dimension of the domain " + std::to_string(m_nodeDomain.nDims()) + " are not the same.");
	};

	//Transform the conditions to the rescalled node data space
	//and safe them
	m_reNDSCondi = m_nodeDomain.contractToUnit(rootDSCondi);
	yggdrasil_assert(m_reNDSCondi.isValid());

	//
	//End building constructor
}; //End: building


ygInternal_leafProxy_t::ygInternal_leafProxy_t(
	const ygInternal_leafProxy_t& 	src)
 :
  m_nodeDomain(src.m_nodeDomain),
  m_parModel(src.m_parModel->clone()),
  m_reNDSCondi(src.m_reNDSCondi)
{
	//Enforce the valid domain
	if(m_nodeDomain.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The valid leaf returned an invalid domain.");
	};

	//Test if teh nodel is fitted
	if(m_parModel == nullptr)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The clone function of the model returned a null pointer.");
	};
	if(m_parModel->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The cloned model is not associated.");
	};
	if(m_parModel->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The model is not completly fitted.");
	};

	//
	//End building constructor
}; //End: copy constructor


ygInternal_leafProxy_t&
ygInternal_leafProxy_t::operator= (
	const ygInternal_leafProxy_t& src)
{
	//Make a copy
	ygInternal_leafProxy_t tmp(src);

	//Move tnp into *this
	*this = std::move(tmp);

	//Return *this
	return *this;
}; //End: copy assignment






YGGDRASIL_NS_END(yggdrasil)



