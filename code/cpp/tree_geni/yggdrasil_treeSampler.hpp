#pragma once
/**
 * \brief	This file implements a sampler for the tree.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>
#include <core/yggdrasil_PDFValueArray.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>

#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>
#include <tree_geni/ygInternal_leafProxy.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <random/yggdrasil_random.hpp>

//INcluide std
#include <vector>
#include <random>

//Include Eigen
#include <core/yggdrasil_eigen.hpp>	//Contains <Eigen/Core>


YGGDRASIL_NS_BEGIN(yggdrasil)






/**
 * \brief	This class implements sampling on a tree object.
 * \class 	yggdrasil_treeSampler_t
 *
 * This function takes the distribution that is descriobed by a tree
 * and turns it into a sampler object.
 * It supports fucntions that are similar to the random distribution
 * interface, but is a bit more limited. The reason is that *this
 * will process the tree, given to it and transform it
 * in a more appropriate way.
 * Note that copies are made so the sampler will not change if the tree
 * does.
 *
 * Note that this class maintains a list of proxies to the leafs.
 * In the context of this class proxy and leaf are synonymes to each other,
 * if not specified otherwhise.
 *
 * This class is able to compute the pdf of the samples under the condition.
 *
 * Note that the class provides an (untested) fucntion to compute the
 * probability of stand alone samples. But this function was never tested,
 * nor it is integrated in teh general framework and it is fairly inefficient.
 * Especially if the number of proxies is large.
 * The function requieres a very special format of the samples.
 * Assume that D is the dimension of the underling sample space, firther let
 * C be the number of constaints that is applied.
 * Then the number of degrees of freedoms is D - C, which we call dof.
 * This means that the function only accpets samples of length dof.
 * The interpretation of the components is the following.
 * The first dimension of the passed sample is not interpreted as the
 * first dimension, but as the first non constrained dimension.
 */
class yggdrasil_treeSampler_t
{
	/*
	 * =====================
	 * Typedefs
	 */
public:
	using MultiCondition_t 		= yggdrasil_multiCondition_t;		//!< This is a multi condition.
	using SingleCondition_t 	= yggdrasil_singleDimCondition_t;	//!< This is the calss that models a soingle dimension.
	using ConditionVector_t 	= std::vector<SingleCondition_t>;	//!< This is teh collection of many conditions.
	using ConditionMap_t 		= std::map<Size_t, Numeric_t>;		//!< THis is a map that encodes the condition, is used for construction.

	using Tree_t 			= yggdrasil_DETree_t;			//!< Type of the tree.
	using NodeFacade_t 		= Tree_t::NodeFacade_t;			//!< This is the node facade
	using LeafIterator_t 		= Tree_t::SubLeafIterator_t;		//!< This is the leaf iterator to iterate over the leafs
	using LeafProxy_t 		= ygInternal_leafProxy_t;		//!< This is a class that models a leaf, it is just a copy of the important stuff.
	using LeafProxyList_t 		= std::vector<LeafProxy_t>;		//!< This is a collection for storing many leaf proxies.
	using LeafPrxyIterator_t 	= LeafProxyList_t::const_iterator;	//!< This is an interator that allows iterating over the proxies.

	using WeightVector_t 		= yggdrasil_valueArray_t<Real_t>;	//!< This is the vector for the weights, is not used directly.
	using DisctreteSampler_t 	= std::discrete_distribution<Size_t>;	//!< This is a discrete distzribution, needed for selecting the leaf
	using UnitSampler_t 		= std::uniform_real_distribution<Numeric_t>;	//!< This is a generator needed for the generation of the samples.

	using HyperCube_t 		= yggdrasil_hyperCube_t;		//!< This is teh domain object

	using Sample_t 			= yggdrasil_sample_t;			//!< This is teh class that expresses one sample.
	using SampleList_t 		= ::yggdrasil::yggdrasil_sampleList_t;	//!< This is the type for a list of samples
	using SampleArray_t		= ::yggdrasil::yggdrasil_arraySample_t;	//!< This is a space efficient way to stoirage a slot of sampels.
	using SampleCollection_t	= ::yggdrasil::yggdrasil_sampleCollection_t;	//!< This is the sample collection type

	//Return value for pdf vector fucntions
	using PDFValueArray_t 		= yggdrasil_PDFValueArray_t<Numeric_t>;	//!< This is the type for representing many values of pdfs.
	using PDFValueArrayEigen_t 	= ::Eigen::VectorXd;			//!< THis is the type for returning Eigen vectors.


	/*
	 * ========================
	 * Building Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This construct a tree sampler.
	 * It takes a tree and conditions.
	 * *this will be automatically constructed.
	 * The conditions must be defined on the
	 * global data space.
	 *
	 * In the case that no leaf element can satisfy
	 * the conditions then an empty sampler is generated.
	 * This will not cause an exception to be trigered,
	 * but all sampling functions will fail.
	 *
	 * In the case of the case that the conditions does not
	 * intersects with the global data space
	 * no error is guaranteed, but the list of leafs will
	 * be empty, this mins notOverConstraint() will returns false.
	 * The condition is set to the invalid condition.
	 *
	 * \param  tree		This is the tree that is used for sampling.
	 * \param  condi	This are the conditions that should be applied.
	 *
	 * \note	All othe building constructors are
	 * 		 reduced to this one.
	 */
	yggdrasil_treeSampler_t(
		const Tree_t& 			tree,
		const MultiCondition_t 		condi);


	/**
	 * \brief	Creates an unconstrained tree sampler.
	 *
	 * This function creats a sample that does not have any
	 * constarints. This means all leafs are selected.
	 * Their probability weight is determined by the fraction of
	 * samples they contain.
	 *
	 * It only needs a tree for the construction.
	 *
	 * \param  tree 	The tree that should be used to create the sampler.
	 */
	yggdrasil_treeSampler_t(
		const Tree_t& 		tree);


	/**
	 * \brief	This constructors bould a condiitioned tree sampler.
	 *
	 * The conditions did not have to be passed to *this as a condition
	 * object, it is sufficient to pass a condition map to *this.
	 * The multi condition object will be constructed by *thsi.
	 *
	 * \param  tree		The tree that should be sampled.
	 * \param  condiMap 	The condition map that should be used.
	 */
	yggdrasil_treeSampler_t(
		const Tree_t& 		tree,
		const ConditionMap_t& 	condiMap);


	/**
	 * \brief	This constructo builds a constrained
	 * 		 tree sampler.
	 *
	 * The conditions are extracted from the condidtion vector
	 * that is passed as an argument.
	 *
	 * \param  tree		The tree that should be generated.
	 * \param  condiVector	This is the vector with the condition that should be applied.
	 */
	yggdrasil_treeSampler_t(
		const Tree_t& 			tree,
		const ConditionVector_t&	condiVector);



	/*
	 * ======================
	 * Other constructors.
	 *
	 * These are the one that could be made defaultl.
	 */
public:
	/**
	 * \brief	Default constructor.
	 *
	 * Is deleted
	 */
	yggdrasil_treeSampler_t()
	 = delete;


	/**
	 * \brief 	Copy constructor.
	 *
	 * Defaulted.
	 */
	yggdrasil_treeSampler_t(
		const yggdrasil_treeSampler_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Defaulted.
	 */
	yggdrasil_treeSampler_t&
	operator= (
		const yggdrasil_treeSampler_t&)
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_treeSampler_t(
		yggdrasil_treeSampler_t&& )
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_treeSampler_t&
	operator= (
		yggdrasil_treeSampler_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Defaulted
	 */
	~yggdrasil_treeSampler_t()
	 = default;


	/*
	 * =====================
	 * General status functions
	 */
public:
	/**
	 * \brief	This function returns true if some leafs
	 * 		 of the tree could satisfy the conditions.
	 *
	 * The name of this function is a bit miss leading.
	 * The reason is that its scope has evolved in time.
	 * However its intentions remained the same.
	 *
	 * If thsi function returns true then it is possible
	 * that it can be sampled from it.
	 *
	 * If this fucntion returns true, then it means *this can not
	 * be used to generate samples, it also means that some functions
	 * will throw. This is done since their resould could
	 * be interpreted differently than they should.
	 */
	bool
	notOverConstrained()
	 const
	 noexcept
	{
		yggdrasil_assert(m_globDataSpace.isValid());
		yggdrasil_assert(m_leafList.empty() ? (m_rootConditions.nDims() == 0) : (m_rootConditions.nDims() > 0));

		return (m_leafList.empty() == false);
	}; //End: is over constraint


	/*
	 * ====================
	 * Accessing
	 * 	Domain
	 *
	 * Meant for information retrival.
	 *
	 */
public:
	/**
	 * \brief	Get the dimension of the underling sample space.
	 *
	 * Note this fucntion can return zero, in the case that the
	 * condistion is invalid.
	 */
	Size_t
	nDims()
	 const
	 noexcept
	{
		return m_rootConditions.nDims();	//It does not mather on which space we are
	}; //End: nDims


	/**
	 * \brief	Return the number of samples in the tree.
	 *
	 * The number of samples was determined uppon constructing.
	 *
	 * \throw 	If *this is invalid.
	 */
	Size_t
	nSamples()
	 const
	{
		if(m_rootConditions.nDims() == 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not access an invalid tree sampler.");
		};

		return m_nSamples;
	}; //End: nSamples


	/**
	 * \brief	Return the global data sapce of the tree.
	 *
	 * Note that this function will not throw if *this is over constraint.
	 */
	const HyperCube_t&
	getGlobalDataSpace()
	 const
	 noexcept
	{
		yggdrasil_assert(m_globDataSpace.isValid());

		return m_globDataSpace;
	}; //End: getGlobalDataSpace


	/**
	 * \brief	This fucntion returns the normalization constant for the probability.
	 *
	 * This value is 1/p(x^c) it is used to transform an uncodnitional probability
	 * to an conditional probability.
	 * In the case of an unconditioned sampler it is one.
	 *
	 * Note that this value is expressed in global data space.
	 *
	 * Multiplying this this value and an unconditioned pdf value will
	 * result in the pdf of that samples, when the conditions are applied.
	 * Note that there is no way for this function to detect if the value
	 * satisfy thet condition or not (which would result in zero).
	 */
	Real_t
	get_iPxc()
	 const
	 noexcept
	{
		yggdrasil_assert(isValidFloat(m_ipxc));
		yggdrasil_assert(m_ipxc > 0.0);

		return m_ipxc;
	}; //End: get the normalization constant


	/*
	 * ====================
	 * Accessing
	 *
	 * 	The proxy
	 */
public:
	/**
	 * \brief	This function returns an iterator to the
	 * 		 first prox object that met the conditions.
	 */
	LeafPrxyIterator_t
	proxyBegin()
	 const
	 noexcept
	{
		return m_leafList.cbegin();
	}; //End: proxyBegin


	/**
	 * \brief	This fucntion returns the pass teh end iterator
	 * 		 to the list of proxies that mets the conditions.
	 */
	LeafPrxyIterator_t
	proxyEnd()
	 const
	 noexcept
	{
		return m_leafList.cend();
	}; //End: proxyEnd


	/**
	 * \begin 	Returns the numbers of leafs that met the condition.
	 *
	 * This returns the length of the proxy list.
	 * Note this function allows also zero as return value.
	 * This means that no leaf met the condition.
	 *
	 * Note this fucntion will not throw in the case that *this
	 * is obver constrained.
	 */
	Size_t
	nProxies()
	 const
	 noexcept
	{
		return m_leafList.size();
	}; //ENd size of proxies


	/**
	 * \brief	This function returns a constant reference
	 * 		 to the ith proxy.
	 *
	 * Note that this functuion will generate an exception if
	 * out of bound is detected, this is unusual.
	 *
	 * \param  i 	The index of the proxy that is used.
	 *
	 * \throw 	If an out of bounds happens.
	 */
	const LeafProxy_t&
	operator[] (
		const Size_t 	i)
	 const
	{
		//Test an out of bound
		if(!(i < m_leafList.size()))	//This is also a protection agians the overcontraint case
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Can not access index " + std::to_string(i) + " of the proxy list, the list has just a size of " + std::to_string(m_leafList.size()));
		};

		return m_leafList[i];
	}; //End: operator[]



	/*
	 * =====================
	 * Accessing
	 *
	 * 	Conditions
	 */
public:
	/**
	 * \brief	This fucntion returns the conditions that are imposed.
	 *
	 * Note that the conditions that are returned are defined in the
	 * rescaled root data space and not in the global/original data space.
	 *
	 * This function can return invalid conditions.
	 * This is the case if *this is over constaint.
	 */
	const MultiCondition_t&
	getRootConditions()
	 const
	 noexcept
	{
		return m_rootConditions;
	}; //End: getRootConditions


	/**
	 * \brief	This function returns the conditions that are
	 * 		 imposed on the global data space.
	 *
	 * This are defined on the global space, this conditions
	 * where given upon construction, up to numerical issues.
	 *
	 * Note that they are not stored explicitly, this means
	 * they must be created each time they are requested.
	 *
	 * Note that in the case that *this is over constarint
	 * the function will return the invalid conditions.
	 */
	MultiCondition_t
	getGlobalConditions()
	 const
	{
		/*
		 * We must first check if the condition is invalid or not
		 */
		if(m_rootConditions.nDims() == 0)
		{
			/*
			 * The condition is invalid, so we return the
			 * invalid constraints.
			 */
			yggdrasil_assert(this->notOverConstrained() == false);

			return MultiCondition_t::CREAT_INVALID_CONDITION();
		}; //End if: conditions is invalid

		/*
		 * The constraint is not invalid, so we are not over constarint.
		 * thus we returned a transformed version
		 */
		yggdrasil_assert(this->notOverConstrained() == true);

		return m_globDataSpace.mapFromUnit(m_rootConditions);
	}; //End: getGlobal conditions


	/**
	 * \brief	Returns the number of conditions that are imposed.
	 *
	 * No not confuse it with the noConditions() function, that checks
	 * if the numbers of conditions is zero.
	 *
	 * \throw	This fucntion will throw if thsi is over constraint.
	 */
	Size_t
	nConditions()
	 const
	{
		//Note thet this will throw is the condition is invalid
		return m_rootConditions.nConditions();		//For that it does not mather on which space we are.
	}; //ENd: nCondi


	/**
	 * \brief	This function returns true if no conditions are applied.
	 *
	 * Thsi checks if the size of the conditions is zero, but is more efficiently.
	 * Do not confuse this with the nConditions() fucntions, which returns
	 * the number of conditions.
	 *
	 * \throws 	If this is over constarined.
	 */
	bool
	noConditions()
	 const
	{
		//This function will throw we have not to check it.
		return m_rootConditions.noConditions();		//For that it does not mather on which space we are.
	}; //ENd: noCondi


	/**
	 * \brief	This fucntion returns the number of degrees of freedoms.
	 *
	 * This is the number of dimensions that where conditioned.
	 */
	Size_t
	nDOF()
	 const
	 noexcept
	{
		yggdrasil_assert(this->nDims() > 0);

		//Test if *this is over constraint,
		//in that case DOFS is 0
		if(m_leafList.empty() == true)
		{
			return 0;
		}; //End if: over constraint

		return (m_globDataSpace.nDims() - m_rootConditions.nConditions());
	}; //End: get numbers of DOFS



	/*
	 * ====================
	 * ====================
	 * Quering functions
	 *
	 * Only sampling function with optional associated pdfs are
	 * provided, the pdf of a general sample can not be quried.
	 * The reason is that it is extremly inefficient in dooing that
	 * use the tree if you need that feature.
	 *
	 * The sample fucntions returns conditioned PDF values.
	 */
public:
	/*
	 * ==================
	 * Single sample
	 */
public:
	/**
	 * \brief	This functin generates a sample from the tree.
	 *
	 * A pair is returned, second is the pdf.
	 *
	 * \param  geni 	This is the generator that is used.
	 *
	 * \note 	For consistency the fucntion also has an S at the end.
	 */
	std::pair<Sample_t, Real_t>
	generateSamplesPDF(
		yggdrasil_pRNG_t& 	geni);


	/**
	 * \brief	This fucntion generates a sample from the tree distribution.
	 *
	 * Note that the pdf is not caluclated.
	 *
	 * \param  geni		This is the generator that is used.
	 *
	 * \note 	For consistency the fucntion also has an S at the end.
	 */
	Sample_t
	generateSamples(
		yggdrasil_pRNG_t& 	geni);


	/*
	 * ==================
	 * Sample Array fucntions
	 */
public:
	/**
	 * \brief	Generate Samples and pdf that arer drawn from the tree.
	 *
	 * This function uses the tree to draw new samples.
	 * The samples are stored in a sample array.
	 *
	 * \param  geni 	The source of random numbers.
	 * \param  N 		The number of samples that should be generated.
	 *
	 * \throw 	If problems arised, as if *This is over contrained.
	 */
	std::pair<SampleArray_t, PDFValueArray_t>
	generateSamplesArrayPDF(
		yggdrasil_pRNG_t& 	geni,
		const Size_t 		N);


	/**
	 * \brief	This function generates Samples and PDFs at the same time.
	 *
	 * The samples are stored in a sample array, the PDFs are stored inside an
	 * Eigen vector.
	 *
	 * \param  geni		This is teh source of random.
	 * \param  N 		The number of samples that are generated.
	 */
	std::pair<SampleArray_t, PDFValueArrayEigen_t>
	generateSamplesArrayPDF_Eigen(
		yggdrasil_pRNG_t& 	geni,
		const Size_t 		N);


	/**
	 * \brief	This function generates samples.
	 *
	 * This function generqates samples and stores them in a sample array.
	 * The pdf values of them are not computed.
	 *
	 * \param  geni		The generator that is used.
	 * \param  n		The number of samples that are generated.
	 */
	SampleArray_t
	generateSamplesArray(
		yggdrasil_pRNG_t& 	geni,
		const Size_t 		N);


	/*
	 * ================
	 * Sample List
	 */
public:
	/**
	 * \brief	This function generates N samples and theirs PDF values.
	 *
	 * The generated samples are stored inside a sample list.
	 *
	 * \param  geni		The generator for generating the randomness.
	 * \param  N 		The number of samples that should be generated.
	 */
	std::pair<SampleList_t, PDFValueArray_t>
	generateSamplesListPDF(
		yggdrasil_pRNG_t& 	geni,
		const Size_t 		N);


	/**
	 * \brief	This function generates N samples and theirs PDF values.
	 *
	 * The generated samples are stored inside a sample list.
	 * The pdfs are stored inside an Eigen Vector.
	 *
	 * \param  geni		The generator for generating the randomness.
	 * \param  N 		The number of samples that should be generated.
	 */
	std::pair<SampleList_t, PDFValueArrayEigen_t>
	generateSamplesListPDF_Eigen(
		yggdrasil_pRNG_t& 	geni,
		const Size_t 		N);


	/**
	 * \brief	This fucntion ghenerates N samples, buit without the PDF.
	 *
	 * The generated samples are stored inside a sample list.
	 *
	 * \param  geni 	The randomness source.
	 * \param  N 		The number of samples to generate.
	 */
	SampleList_t
	generateSamplesList(
		yggdrasil_pRNG_t&	geni,
		const Size_t 		N);


	/*
	 * ===================
	 * Sample collection
	 */
public:
	/**
	 * \brief	This fucntion generates N samples and the pdfs that belongs to that.
	 *
	 * The samples are stored inside a sample collection.
	 *
	 * \param  geni		The source of randomness.
	 * \param  N 		The number of samples that should be generated.
	 */
	std::pair<SampleCollection_t, PDFValueArray_t>
	generateSamplesCollectionPDF(
		yggdrasil_pRNG_t& 	geni,
		const Size_t 		N);


	/**
	 * \brief	This fucntion generates N samples and the pdfs that belongs to that.
	 *
	 * The samples are stored inside a sample collection.
	 * The pdf values are stored inside an Eigen vector.
	 *
	 * \param  geni		The source of randomness.
	 * \param  N 		The number of samples that should be generated.
	 */
	std::pair<SampleCollection_t, PDFValueArrayEigen_t>
	generateSamplesCollectionPDF_Eigen(
		yggdrasil_pRNG_t& 	geni,
		const Size_t 		N);


	/**
	 * \brief	This fucntion generates N samples.
	 *
	 * The samples are stored inside a sample collection.
	 * The pdf of the samples are not generated.
	 *
	 * \param  geni		The source of randomness.
	 * \param  N 		The number of samples that should be generated.
	 */
	SampleCollection_t
	generateSamplesCollection(
		yggdrasil_pRNG_t& 	geni,
		const Size_t 		N);









	/*
	 * =====================
	 * Private functions
	 */
private:
	/**
	 * \brief	This si the work horse function.
	 *
	 * This function is used by all other functions to generate samples.
	 * This function does not apply a full test series, it only does some
	 * consistency check. It is used to generate samples and the
	 * coresponding probability.
	 *
	 * The sample and probability that are returned is expressed on
	 * the GLOBAL data sapce, thus no rescalling has to be done.
	 * The probability that this fucntion returns takes the condition
	 * into account.
	 *
	 * As its distribution objects counterparts, it guarantees,
	 * that the probability, in finite aretmetic of the sample is
	 * greater than zero at least when it was generated.
	 *
	 * This function needs an externel generator.
	 *
	 * \param  geni 	This is the random source.
	 *
	 * \note	Previously the probability was never conditioned.
	 */
	std::pair<Sample_t, Real_t>
	wh_generateSample(
		yggdrasil_pRNG_t& 	geni);


	/**
	 * \brief	This is a fucntion that calculates
	 * 		 the probability of a sample.
	 *
	 * Note that this function is, due to the internal
	 * format of the sampler extreemly inefficient.
	 * Howert it is provided fro testing putposes.
	 *
	 * It takes a sample, which has a very special format.
	 * The dimension of the sample is nDims() - nConditions().
	 * This is teh numnber of free dimensions.
	 * The conditioned probability will be computed.
	 * The probability and the returned sample is expressed
	 * global data space.
	 *
	 * It will return a pair. first is a sample that
	 * is the passed sample, but with full dimensions,
	 * expressed on global data space. second is the
	 * probability of that sample under the condition.
	 *
	 * The passed sample is interpreted on global data space.
	 *
	 * \param  x 		The sample of reduced dimensionality
	 * 			 that should be judged.
	 */
	std::pair<Sample_t, Real_t>
	wh_computePDF(
		const Sample_t& 	x)
	 const;


	/*
	 * =====================
	 * Friends
	 */
private:
	/**
	 * \brief	This is an internal function for the tree sampler.
	 *
	 * This function can generate samples and pdfs, the types that are
	 * templated, thus all containers and pdf vectors can be
	 * served from one single function.
	 * The pdf vectors are not filled, if the pointer to the vector is null.
	 *
	 * If no pdf values are generated, second of the retrun type will be
	 * empty.
	 *
	 * \param  self		This is a tree sampler.
	 * \param  geni 	This is the random generator.
	 * \param  N 		The number of samples that should be generated.
	 *
	 * \tparam  pdfVector_t 	The type of the vector for the pdfs.
	 * 				 Need to have resize(), operator[] and size().
	 * \tparam  Container_t 	The type of the container.
	 * \tparam  genPDF 		Bool to indicate if pdf values should be generated.
	 */
	template<
		class 	Container_t,
		class 	pdfVector_t,
		bool 	genPDF
	>
	friend
	std::pair<Container_t, pdfVector_t>
	ygInternal_treeSampler_driver(
		yggdrasil_treeSampler_t&	self,
		yggdrasil_pRNG_t& 		geni,
		const Size_t 			N);



	/*
	 * =======================
	 * Private Memebbers
	 */
public:
	Size_t 				m_nSamples;		//!< This are all the samples that are stored in the tree upon constructing.
	LeafProxyList_t 		m_leafList;		//!< This is a list of all the leaf proxies that are supported.
	HyperCube_t 			m_globDataSpace;	//!< This is the global data domain.
	MultiCondition_t 		m_rootConditions;	//!< These are the sampling conditions expressed in ROOT DATA SPACE.
	DisctreteSampler_t 		m_leafSelecter;		//!< This is a discreted distribution that is able to select the leaf.
	UnitSampler_t 			m_uniformSampler;	//!< This is the uniform distribution sample to generate the values of teh samples.
	Real_t 				m_trafoRootToGlob;	//!< This is teh transformation factor, it is basically the inverse of the volumen of teh global data space.
	Real_t 				m_ipxc;			//!< This is the value 1/p(x^c) from the paper. It is the normalization for the probability to make them conditioned.
								//!<  This value is computed on th global data space.
}; //End class(yggdrasil_treeSampler_t)







YGGDRASIL_NS_END(yggdrasil)



