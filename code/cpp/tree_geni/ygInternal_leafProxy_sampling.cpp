/**
 * \brief	This function implements the sampling functions of the leaf node.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>
#include <tree_geni/ygInternal_leafProxy.hpp>

#include <samples/yggdrasil_sample.hpp>


//INcluide std
#include <memory>


YGGDRASIL_NS_BEGIN(yggdrasil)


Real_t
ygInternal_leafProxy_t::sampleLeaf(
	Sample_t* const 	s,
	const Size_t 		nTot)
{
	yggdrasil_assert(m_parModel != nullptr);

	//Sample the node
	m_parModel->drawSampleOnThis(s, m_reNDSCondi, m_nodeDomain);

	//We now have to compute the probability. The sample is already
	//in the rescalled node data sapce, what the fucntin also expects.
	//It returns the probability expresed in root data space, also what we need
	const Real_t rootDSSpace_pdf = m_parModel->evaluateModelForSample(*s, m_nodeDomain, nTot);

	//We have to transform the sample from the rescalled node data space to the root data space
	m_nodeDomain.mapFromUnit_inPlace(*s);

	//Enforce some conditions
	yggdrasil_assert(isValidFloat(rootDSSpace_pdf));
	yggdrasil_assert(rootDSSpace_pdf >= 0.0);
	yggdrasil_assert(s->isValid());
	yggdrasil_assert(s->isInsideUnit());

	//Now return the value
	return rootDSSpace_pdf;
}; //End sample leaf





YGGDRASIL_NS_END(yggdrasil)



