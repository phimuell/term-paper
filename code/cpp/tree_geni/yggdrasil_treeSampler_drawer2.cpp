/**
 * \brief	This file implements the drawing fucntions of the tree.
 * 		 This function eclusively deals withe the sampling generation towards cntainers.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>

#include <tree/yggdrasil_treeImpl.hpp>
#include <tree/yggdrasil_nodeFacace.hpp>
#include <tree/yggdrasil_leafIterator.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>
#include <tree_geni/yggdrasil_multiDimCondition.hpp>
#include <tree_geni/ygInternal_leafProxy.hpp>
#include <tree_geni/yggdrasil_treeSampler.hpp>

#include <samples/yggdrasil_sample.hpp>

#include <random/yggdrasil_random.hpp>

//INcluide std
#include <vector>
#include <random>


YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the driver
template<
	class 	Container_t,
	class 	pdfVector_t,
	bool 	genPDF
>
extern
std::pair<Container_t, pdfVector_t>
ygInternal_treeSampler_driver(
	yggdrasil_treeSampler_t&	self,
	yggdrasil_pRNG_t& 		geni,
	const Size_t 			N);

/*
 * ================================
 */

//Some using declarations that will make life easier
using Sample_t 			= yggdrasil_treeSampler_t::Sample_t;
using PDFValueArray_t 		= yggdrasil_treeSampler_t::PDFValueArray_t;
using PDFValueArrayEigen_t 	= yggdrasil_treeSampler_t::PDFValueArrayEigen_t;
using SampleArray_t 		= yggdrasil_treeSampler_t::SampleArray_t;
using SampleList_t 		= yggdrasil_treeSampler_t::SampleList_t;
using SampleCollection_t 	= yggdrasil_treeSampler_t::SampleCollection_t;


/*
 * ====================
 * Single sample
 */
std::pair<Sample_t, Real_t>
yggdrasil_treeSampler_t::generateSamplesPDF(
	yggdrasil_pRNG_t& 	geni)
{
	if(this->notOverConstrained() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The tree sampler is over constrained.");
	};
	if(this->nProxies() == 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The sampler did not have any proxies.");
	};
	if(this->getRootConditions().isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The conditions are invalid.");
	};
	if(this->getGlobalDataSpace().isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The global data domain is invalid.");
	};

	return this->wh_generateSample(geni);
};


Sample_t
yggdrasil_treeSampler_t::generateSamples(
	yggdrasil_pRNG_t& 	geni)
{
	return this->generateSamplesPDF(geni).first;
};


/*
 * ====================
 * Array
 */

std::pair<SampleArray_t, PDFValueArray_t>
yggdrasil_treeSampler_t::generateSamplesArrayPDF(
	yggdrasil_pRNG_t& 	geni,
	const Size_t 		N)
{
	return ygInternal_treeSampler_driver<SampleArray_t, PDFValueArray_t, true>(*this, geni, N);
};


std::pair<SampleArray_t, PDFValueArrayEigen_t>
yggdrasil_treeSampler_t::generateSamplesArrayPDF_Eigen(
	yggdrasil_pRNG_t& 	geni,
	const Size_t 		N)
{
	return ygInternal_treeSampler_driver<SampleArray_t, PDFValueArrayEigen_t, true>(*this, geni, N);
};


SampleArray_t
yggdrasil_treeSampler_t::generateSamplesArray(
	yggdrasil_pRNG_t& 	geni,
	const Size_t 		N)
{
	return ygInternal_treeSampler_driver<SampleArray_t, PDFValueArrayEigen_t, false>(*this, geni, N).first;
};



/*
 * ====================
 * List
 */

std::pair<SampleList_t, PDFValueArray_t>
yggdrasil_treeSampler_t::generateSamplesListPDF(
	yggdrasil_pRNG_t& 	geni,
	const Size_t 		N)
{
	return ygInternal_treeSampler_driver<SampleList_t, PDFValueArray_t, true>(*this, geni, N);
};


std::pair<SampleList_t, PDFValueArrayEigen_t>
yggdrasil_treeSampler_t::generateSamplesListPDF_Eigen(
	yggdrasil_pRNG_t& 	geni,
	const Size_t 		N)
{
	return ygInternal_treeSampler_driver<SampleList_t, PDFValueArrayEigen_t, true>(*this, geni, N);
};


SampleList_t
yggdrasil_treeSampler_t::generateSamplesList(
	yggdrasil_pRNG_t& 	geni,
	const Size_t 		N)
{
	return ygInternal_treeSampler_driver<SampleList_t, PDFValueArrayEigen_t, false>(*this, geni, N).first;
};


/*
 * ====================
 * Collection
 */

std::pair<SampleCollection_t, PDFValueArray_t>
yggdrasil_treeSampler_t::generateSamplesCollectionPDF(
	yggdrasil_pRNG_t& 	geni,
	const Size_t 		N)
{
	return ygInternal_treeSampler_driver<SampleCollection_t, PDFValueArray_t, true>(*this, geni, N);
};


std::pair<SampleCollection_t, PDFValueArrayEigen_t>
yggdrasil_treeSampler_t::generateSamplesCollectionPDF_Eigen(
	yggdrasil_pRNG_t& 	geni,
	const Size_t 		N)
{
	return ygInternal_treeSampler_driver<SampleCollection_t, PDFValueArrayEigen_t, true>(*this, geni, N);
};


SampleCollection_t
yggdrasil_treeSampler_t::generateSamplesCollection(
	yggdrasil_pRNG_t& 	geni,
	const Size_t 		N)
{
	return ygInternal_treeSampler_driver<SampleCollection_t, PDFValueArrayEigen_t, false>(*this, geni, N).first;
};


/*
 * =====================
 * Friends / Driver
 */
template<
	class 	Container_t,
	class 	pdfVector_t,
	bool 	genPDF
>
extern
std::pair<Container_t, pdfVector_t>
ygInternal_treeSampler_driver(
	yggdrasil_treeSampler_t&	self,
	yggdrasil_pRNG_t& 		geni,
	const Size_t 			N)
{
	/*
	 * Perform the tests
	 */
	if(self.notOverConstrained() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The tree sampler is over constrained.");
	};
	if(self.nProxies() == 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The sampler did not have any proxies.");
	};
	if(self.getRootConditions().isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The conditions are invalid.");
	};
	if(self.getGlobalDataSpace().isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The global data domain is invalid.");
	};
	if(self.nDims() == 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension of the sample space is zero.");
	};

	/*
	 * ====================
	 * Create the output variable.
	 */

	//load the data
	const Size_t nDims = self.nDims();
	yggdrasil_assert(nDims > 0);

	//
	//Generate the container for storing the samples
	Container_t samples(nDims, N);

	//Test the container
	yggdrasil_assert(samples.nDims() == nDims);
	yggdrasil_assert(samples.nSamples() == N);

	//
	//Allocate the size of the pdf container
	pdfVector_t pdfs(genPDF ? N : 0);

	//Test the output
	yggdrasil_assert(pdfs.size() == ((genPDF == true) ? N : 0));

	//This is teh sample that is used as work sample
	typename Container_t::Sample_t workingSample(nDims);

	//This is the variable for the pdf
	Real_t workingPDF = NAN;


	/*
	 * ===============
	 * Generate the samples
	 * 	Note the wh_ fucntin guarantees us that the pdfs are well behaved
	 * 	so we just need to iterate over them.
	 */
	for(Size_t i = 0; i != N; ++i)
	{
		//Set the working PDF again to NAN
		workingPDF = NAN;

		//Generate a sample
		std::tie(workingSample, workingPDF) = self.wh_generateSample(geni);

		//Check the sample, only by asserts
		yggdrasil_assert(isValidFloat(workingPDF));
		yggdrasil_assert(workingPDF > 0.0);
		yggdrasil_assert(workingSample.isValid());

		//Now insering the sample to the container
		samples.setSampleTo(workingSample, i);

		//TEst if the pdf must be written
		if(genPDF == true)
		{
			//Test for out of bound
			yggdrasil_assert(i < pdfs.size());

			//Set the value
			pdfs[i] = workingPDF;
		}; //End: write pdf
	}; //ENd for(i): generate the samples



	/*
	 * Return the samples
	 */
	return std::make_pair(std::move(samples), std::move(pdfs));
}; //ENd: driver


YGGDRASIL_NS_END(yggdrasil)



