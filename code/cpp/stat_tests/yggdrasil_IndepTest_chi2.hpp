#pragma once
/**
 * \brief	This file implements a chi2 test for independence test
 *
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <util/yggdrasil_dimPair.hpp>
#include <util/yggdrasil_chi2_frequencyTable.hpp>
#include <util/yggdrasil_statTestRank.hpp>

#include <samples/yggdrasil_sample.hpp>

#include <interfaces/yggdrasil_interface_IndepTest.hpp>



//Incluide std
#include <memory>
#include <vector>
#include <map>


YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \class 	yggdrasil_Chi2IndepTest_t
 *
 * This class implements the independence via a chi2 test.
 * It is based on the R function form the original code.
 *
 *
 * This class is final
 */
class yggdrasil_Chi2IndepTest_t final : public yggdrasil_IndepTest_i
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using HyperCube_t 	= yggdrasil_hyperCube_t;	//!< This is the type that represents a domain.
	using Sample_t 		= yggdrasil_sample_t;		//!< This is the type of a single sample.
	using DimArray_t 	= yggdrasil_dimensionArray_t;	//!< This is the type of the diomensional array.
	using NodeFacade_t 	= yggdrasil_nodeFacade_t;	//!< This is the node facade.
	using SplitSequence_t 	= yggdrasil_splitSequence_t;	//!< This is a type for managing a series of splits.
	using IndepTest_ptr	= std::unique_ptr<yggdrasil_IndepTest_i>;	//!< This is the managed pointer of the independence test.
	using FreqTable_t 	= yggdrasil_chi2FreqTable_t;		//!< This is the frequency table, that is used to test the indepencency condition.
	using BinEdgeArray_t 	= FreqTable_t::BinEdgesArray_t;	//!< This is the type for managing the bins is a single dimension.

	using DimPair_t 	= yggdrasil_dimPair_t;		//!< This is a pair of dimensions.
	using TestRank_t 	= yggdrasil_statTestRank_t<DimPair_t>;	//!< This si the class that manages the test results.
	using SingleTestRes_t 	= TestRank_t::SingleTestRes_t;	//!< A single test result

	using DimPairList_t 	= ::std::vector<DimPair_t>;	//!< This is a list of all dimensions that is present.


	/*
	 * ==========================
	 * Internal Typedefs
	 */
private:
	using BinEdgesSS_t 	= BinEdgeArray_t;		//!< This is the class for managing the bins in a single dimension.
	using BinEdgesDim_t 	= ::std::vector<BinEdgesSS_t>;		//!< This is the class that manages the bins in all dimensions.
	using IndepTests_t 	= ::std::map<DimPair_t, FreqTable_t>;	//!< This is the map that represnets all the tests.

	using BinEdges_cIt 	= BinEdgesDim_t::const_iterator;	//!< Constant iterator to iterate over the different bin edges.
	using BinEdges_it 	= BinEdgesDim_t::iterator;		//!< Iterator to iterate over the different bin edges.

	using IndepTests_it 	= IndepTests_t::iterator;		//!< Map iterator to iterate over the different frequency tables.
	using IndepTests_cIt 	= IndepTests_t::const_iterator;		//!< Constant map iterator to iterate over the different frequency tables,




	/*
	 * ===================================
	 * Constructor
	 *
	 * The other constructor are declared public.
	 * The interface declared them protected.
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This is a building constructor.
	 * It only needs the numbers of dimensions, the data will have.
	 * We need only this one, because we will bind the data later
	 * to the test.
	 *
	 * This constructor accepts zero as rgument for the dimensions.
	 * In that case it will support the late buinding policy.
	 *
	 * \param  d		The numbers of dimension the data will have.
	 * \param  sigLevel  	The significance level, that should be used.
	 */
	yggdrasil_Chi2IndepTest_t(
		const Int_t 		d,
		const Numeric_t 	sigLevel);


	/**
	 * \brief	This constructor only takes the signivicance level.
	 *
	 * This constructor is syntactlcally indentical, with the calling
	 * of the baove constzruction with 0 for the dimensional
	 * argument.
	 *
	 * \param  sigLevel	The significant niveau to use.
	 */
	yggdrasil_Chi2IndepTest_t(
		const Numeric_t 	sigLevel);


	/**
	 * \brief	This function returns an empty copy of this.
	 *
	 * This function is used to propagate the test down the tree.
	 * Empty means that no data are associated to the newly crated instance.
	 *
	 * The unique pointer indicates that the calling function takes resposibility
	 * to manage it.
	 *
	 * \note 	From all spelling errors you encountered here, this one is intended.
	 */
	virtual
	IndepTest_ptr
	creatOffspring()
	 const
	 override;



	/**
	 * \brief	Destructor
	 *
	 * It is defaulted.
	 */
	~yggdrasil_Chi2IndepTest_t()
	 noexcept;



	/*
	 * =============================
	 * =============================
	 * Interface functions
	 */


	/*
	 * =========================
	 *
	 * Initial testing function.
	 */
public:
	/**
	 * \brief	This function informs the object that the
	 * 		 initial testing is about to begin.
	 *
	 * For a detailed description see the super class.
	 *
	 * \throw	This function throws if *this is associated,
	 * 		 or internal function throws. However implementations
	 * 		 are encuraged to perform some sort of sanity checks.
	 *
	 * \note	This function performs the associaation
	 */
	virtual
	void
	preInitialHookInit(
		const NodeFacade_t 	nodeToAssosiate)
	 override;


	/**
	 * \brief 	this function performs preprocessing
	 * 		 on a single dimension \c d.
	 *
	 * See interface function for a detailed description.
	 *
	 * \param  d 		The dimension we act on.
	 * \param  parModel	This is the parameteric model that is needed.
	 * \param  subTreeRoot	This is the root of the subtree
	 * 			 that is rooted where *this
	 * 			 belonges to.
	 *
	 * \throw	This function may throw if called twice.
	 * 		 Implementations are encuraged to
	 * 		 do sanity checks.
	 */
	virtual
	void
	preComputeDimension(
		const Int_t 		d,
		cParModel_ptr 		parModel,
		const NodeFacade_t& 	subTreeNode)
	 override;


	/**
	 * \brief	This function perform the
	 * 		 initial fitting of \c *this.
	 *
	 * See interface class for a detailed description.
	 *
	 * \param  dimPair	The diomension pair that
	 * 			 should be tested
	 * \param  leaf		The leaf we test on.
	 *
	 * \return	Returns if the test was able to be done.
	 *
	 * \throw 	If the pair was tested before, if the dimension where illegal.
	 * 		 Implementations can impose further requieremtns.
	 * 		 Also if the implementatio was not able to fit.
	 */
	virtual
	bool
	performInitailTest(
		const DimPair_t 	dimPair,
		const NodeFacade_t& 	leaf)
	 override;


	/**
	 * \brief	This function is called by the implementation
	 * 		 to mark the end of teh testing process.
	 *
	 * For a detailed description see the fucntion in the super class.
	 *
	 * \throw 	It is general unspecoific under which conditions this function
	 * 		 raises an exception, however implementations are encuraged to
	 * 		 perform basic combability and sanety checks to ensure consistency
	 * 		 of *this.
	 */
	virtual
	void
	endOfTestProcess()
	 override;



	/*
	 * ===========================
	 * Split Inspection Functions
	 */
public:
	/**
	 * \brief	This function computes the split sequences.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 *
	 *
	 * \throw 	If \c *this is not in a consistent state.
	 * 		 If the object is not able to compute this sequence.
	 * 		 Implementaations can further strengthen this requirement.
	 * 		 Also if no tests were performed yet.
	 *
	 * \notice	In erlier versions of this interface, this function was
	 * 		 requiered to be thread safe. It was learned that this
	 * 		 requierement may not be able to fulfilled, so it was droped.
	 */
	virtual
	SplitSequence_t
	computeSplitSequence()
	 const
	 override;


	/**
	 * \brief	This function returns true if at least one split has to be performed.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 */
	virtual
	bool
	areSplitsToPerform()
	 const
	 override;


	/**
	 * \brief	This function checks if the split was the same.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 *
	 * \throw 	If there was no update of this is inconsistent.
	 * 		 Also some underlying functin may throw.
	 *
	 * \note 	This means that a false of this fucntion, means
	 * 		 that the splits have a new order.
	 */
	virtual
	bool
	areSplitsStable()
	 override;


	/*
	 * =================================
	 * Operational function
	 *
	 * These functions are not matematical, but from the implementation.
	 */
public:

	/**
	 * \brief	This function returns \c true if \c *this is consistent.
	 *
	 * See the super class for a detained descrioprion.
	 */
	virtual
	bool
	isConsistent()
	 const
	 override;


	/**
	 * \brief	This function returns true if all
	 * 		 tests of \c *this are done.
	 *
	 * See the super class for a detained description.
	 *
	 * \throw	If *this is not associated.
	 */
	virtual
	bool
	allTestsDone()
	 const
	 override;


	/**
	 * \brief	This function returns true if the test on the
	 * 		 dimPair was already performed.
	 *
	 * See interface for a detailed description.
	 *
	 * \param  dimPair 	The dimension that should be tested.
	 *
	 * \throw 	If the test does not exist or *this is not associated.
	 *
	 * \note	This function was previously called "isDimensionsTested"
	 * 		 This was strange and a bit missleading, but consistent
	 * 		 with the GOF test. But it was descided to change the name.
	 */
	virtual
	bool
	isPairTested(
		const DimPair_t 	dimPair)
	 const
	 override;




	/*
	 * =====================
	 * None State Query Functions
	 *
	 * This functions gives information not about the
	 * state but of the, lets call it static state information.
	 * This is the stuff stat does nto change.
	 */
public:
	/**
	 * \brief	This function returns true if *this
	 * 		 is \e associated with a node.
	 *
	 * See the super class for more information.
	 */
	virtual
	bool
	isAssociated()
	 const
	 override;

	/**
	 * \brief	This fucntion returns the numbers of thest
	 * 		 that are managed by *this.
	 *
	 * See Interface class for a detailed description.
	 */
	virtual
	Size_t
	nDifferentTests()
	 const
	 override;



	/**
	 * \brief	This function returns a list (actually a vector)
	 * 		 that stores all the different interactions
	 * 		 that are monitored by *this.
	 *
	 * See interface function for a detailed description.
	 *
	 * \throw	This fuction may throw.
	 */
	virtual
	DimPairList_t
	getAllTestKeys()
	 const
	 override;


	/**
	 * \brief	This function returns the number of
	 * 		 dimensions that needs precomputing.
	 *
	 * See interface function for a detailed description.
	 *
	 * \throw	It is not specified if this function throws.
	 */
	virtual
	Size_t
	nPreComputeDimensions()
	 const
	 override;


	/**
	 * \brief	This function returns a list of all dimensions
	 * 		 that needs preprocessing.
	 *
	 * See interface fucntion for a detailed description.
	 *
	 * \throw	It is not specified if thsi fucntion throws.
	 */
	virtual
	DimList_t
	getAllPrecomputeKeys()
	 const
	 override;





	/*
	 * ====================
	 * Update functions
	 *
	 * These functions are concerned with teh update functionality
	 */
public:
	/**
	 * \brief	This function adds the new sample \c nSample to \c *this.
	 *
	 * See the super class to see a description of the function.
	 *
	 *
	 * \throw 	It is not defined when an exception is raised.
	 * 		 However it is recomended that some sanaty checs are performed.
	 */
	virtual
	void
	addNewSample(
		const Sample_t&		nSample,
		const HyperCube_t&	domain)
	 override;


	/**
	 * \brief	This functions starts the updazte process of *this.
	 *
	 *
	 * \throw	This fucntion throws if *this is not associated or
	 * 		 if *this is \e consistent.
	 */
	virtual
	void
	preCommitHookInit()
	 override;


	/**
	 * \brief	Commit the changes of \c *this .
	 *
	 * See the interface fucntion for more information.
	 *
	 * \throw 	It is not defined when an exception is raised.
	 * 		 However it is recomended that some sanaty checs are performed.
	 *
	 */
	virtual
	bool
	commitChanges(
		const DimPair_t 	dimPair,
		const NodeFacade_t& 	subTreeRoot)
	 override;





	/*
	 * =================================
	 * Newly publicshed constructors
	 */
public:
	/**
	 * \brief	Default constructor
	 *
	 * Deleted,
	 */
	yggdrasil_Chi2IndepTest_t()
	 = delete;


	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_Chi2IndepTest_t(
		const yggdrasil_Chi2IndepTest_t&);


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_Chi2IndepTest_t(
		yggdrasil_Chi2IndepTest_t&&);


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_Chi2IndepTest_t&
	operator= (
		const yggdrasil_Chi2IndepTest_t&);

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_Chi2IndepTest_t&
	operator= (
		yggdrasil_Chi2IndepTest_t&&);


	/*
	 * ===================
	 * Private functions
	 */
private:
	/**
	 * \brief	This fucntion returns the number of bins that
	 * 		 were used to bin the data.
	 *
	 * This value is the same for all diomension, since
	 * we use the raw sample count for that!
	 * This value is determined on the basis of the
	 * the samples that where passed to the preInitialHookInit()
	 * function.
	 *
	 * \throw	If *this is not associated.
	 */
	Size_t
	getEstNBins()
	 const;


	/**
	 * \brief	This fuction returns the degree of freedom of a test.
	 *
	 * Since the degree of freeded depends on the number of bins,
	 * it is also the same for all diemnsions.
	 *
	 * This This function will only return positive
	 * numbers and zero.
	 *
	 *
	 * \throw 	If *Thsi si not associated.
	 */
	Numeric_t
	getDOF()
	 const;


	/**
	 * \brief	This function test if the test is trivial.
	 *
	 * Since the numbers of bins is the same in each dimension.
	 * We can prety clearly say when the tests are trivial.
	 *
	 * The test is trivial if the number of dimension is one.
	 * And thsi si also handled by thei function.
	 * However the test is also trivial if we have only one
	 * bin. In that case we have a DOF of zero.
	 *
	 * \note	Note the case with one dimension is still
	 * 		 handled seperatly, but this function is
	 * 		 able to detect this situation.
	 *
	 * \throw	If *this is not associated.
	 */
	bool
	isTestTrivial()
	 const;


	/*
	 * =====================
	 * Deleted constructors
	 */
public:
	yggdrasil_Chi2IndepTest_t(
		const Int_t 	dim)
	 = delete;

	yggdrasil_Chi2IndepTest_t(
		const Size_t 	dim)
	 = delete;

	yggdrasil_Chi2IndepTest_t(
		const Numeric_t 	sigLevel,
		const Int_t 		d)
	 = delete;



	/*
	 * ======================
	 * Private Member
	 */
public:
	TestRank_t 		m_testRank;	//!< This is the variable that stores the test rank.
	IndepTests_t 		m_indepTests;	//!< This is the variable that stores all the dimensional tests.
	BinEdgesDim_t 		m_binEdges;	//!< This is a variable that stores the bin edges for all dimensions.
	Size_t 			m_dims;		//!< The number of different dimensions.
	Size_t 			m_estNBins;	//!< This is the number of bins that are used in this test. It is important
						//!<  to note that all dimensions have the same number of bins, since it depends onyl on the
						//!<  the numbers of samples, however their locations are at different points.
	Numeric_t 		m_sigLevel;	//!< This is the significance level.
	bool 			m_isAssociated;	//!< This variable indicates if *this is associated.
	bool 			m_isInitialTest;//!< This variable indicates if *this si still in the initial fitt face (internaly)
}; //End class: yggdrasil_Chi2IndepTest_t


YGGDRASIL_NS_END(yggdrasil)
