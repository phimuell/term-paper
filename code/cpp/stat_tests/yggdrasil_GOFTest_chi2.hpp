#pragma once
/**
 * \brief	This file implements the chi2 test for the GOF.
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <util/yggdrasil_chi2_testtable.hpp>
#include <util/yggdrasil_statTestRank.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>

#include <interfaces/yggdrasil_interface_GOFTest.hpp>
#include <interfaces/yggdrasil_interface_parametricModel.hpp>




//Incluide std
#include <memory>


YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the node
class yggdrasil_DETNode_t;

/**
 * \class 	yggdrasil_Chi2GOFTest_t
 * \brief	This class implements the GOF test by means of the chi2 test.
 *
 * This class is final, meaning no class can inherent from it.
 * This class uses the CHI2 table.
 */
class yggdrasil_Chi2GOFTest_t final : public yggdrasil_GOFTest_i
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using HyperCube_t 	= yggdrasil_hyperCube_t;	//!< This is the type that represents a domain.
	using Sample_t 		= yggdrasil_sample_t;		//!< This is the type of a single sample
	using Node_ptr 		= yggdrasil_DETNode_t*;		//!< This is a pointer to a node
	using cNode_ptr 	= const yggdrasil_DETNode_t*;	//!< This is a pointer to a constant node
	using SplitSequence_t 	= yggdrasil_splitSequence_t;	//!< This is a type for managing a series of splits
	using GOFTest_ptr	= std::unique_ptr<yggdrasil_GOFTest_i>;	//!< This is the managed pointer of the GOF test
	using ParModel_t 	= yggdrasil_parametricModel_i;	//!< The normal pointer
	using ParamModel_ptr 	= const yggdrasil_parametricModel_i*;	//!< This i sthe pointer to the parameteric model.
	using Chi2TestTable_t 	= yggdrasil_chi2TestTable_t;	//!< This is the test table that is used.
	using Chi2TTArray_t 	= std::vector<Chi2TestTable_t>;	//!< This is the set of all the test tables.
	using TestRank_t 	= yggdrasil_statTestRank_t<Int_t>;	//!< This si the class that manages the test results.
	using SingleTestRes_t	= TestRank_t::SingleTestRes_t;	//!< This is a soingle test
	using DimArray_t 	= yggdrasil_dimensionArray_t;	//!< This is the dimensuonal array
	using IntervalBound_t 	= yggdrasil_intervalBound_t;	//!< This is a one dimensional bound


	/*
	 * ===================================
	 * Constructor
	 *
	 * The constructors are made public.
	 * In the inerface they where protected.
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This is a building constructor.
	 * It only needs the numbers of dimensions, the data will have.
	 * We need only this one, because we will bind the data later
	 * to the test.
	 *
	 * This constructor accepts zero as argument,
	 * In that case the number of dimensions
	 * are estimated from the node that is passed
	 * upon the initalization.
	 *
	 * \param  d		The numbers of dimension the data will have.
	 * \param  sigLevel	This is the significance level that should be used.
	 */
	yggdrasil_Chi2GOFTest_t(
		const Int_t 		d,
		const Numeric_t 	sigLevel);

	/**
	 * \brief	This s the building constructor, that only needs
	 * 		 to know the significance level.
	 *
	 * This constructor is able to late buind
	 *
	 * \param  sigLevel 	This is the significance level.
	 *
	 * \note	This constructor calls the normal constructor,
	 * 		 with zero as dimnsional argumnt.
	 */
	yggdrasil_Chi2GOFTest_t(
		const Numeric_t 	sigLevel);


	/**
	 * \brief	Destructor
	 *
	 * It is defaulted.
	 * But it is virtual
	 */
	virtual
	~yggdrasil_Chi2GOFTest_t()
	 noexcept;


	/**
	 * \brief	This function returns an empty copy of this.
	 *
	 * It is basically a substitute of a copy constructor.
	 *
	 * This function is used to propagate the test down the tree.
	 * Empty means that no data are associated to the newly crated instance.
	 *
	 * The unique pointer indicates that the calling function takes resposibility
	 * to manage it.
	 *
	 * \note 	From all spelling errors you encountered here, this one is intended.
	 */
	virtual
	GOFTest_ptr
	creatOffspring()
	 const
	 override;


	/*
	 * =============================
	 * =============================
	 * Interface functions
	 */

	/*
	 * ======================
	 * Test functions
	 */
public:
	/**
	 * \brief	This function informs the object that the
	 * 		 initial testing is about to begin.
	 *
	 * See GOF interface for more information.
	 *
	 * \throw	This function throws if *this is associated,
	 * 		 or internal function throws. However implementations
	 * 		 are encuraged to perform some sort of sanity checks.
	 *
	 * \note 	This function performs the association to the given node.
	 */
	virtual
	void
	preInitialHookInit(
		const NodeFacade_t 	nodeToAssociate)
	 override;


	/**
	 * \brief	This function performs the initial test on
	 * 		 the given leaf and the diomension.
	 *
	 * See GOF interface for more information.
	 *
	 * \param  parModel 	This is the parameteric model that will be used.
	 * \param  d 		The dimension that should be tested.
	 * \param  leaf 	The leaf node we want to test on.
	 *
	 * \throw 	If the dimension is out of range.
	 * 		 Also if *this is not possible to do the testing.
	 * 		 Also if a dimension is attemted to be tested twice.
	 * 		 Implementations are encuraged to test more.
	 * 		 And set stronger requierements.
	 */
	virtual
	bool
	performInitailTest(
		cParModel_ptr 	parModel,
		NodeFacade_t 	leaf,
		const Int_t 	d)
	 override;


	/**
	 * \brief	This function is called by the implementation
	 * 		 to mark the end of teh testing process.
	 *
	 * See GOF interface for more information.
	 *
	 * \throw 	It is general unspecoific under which conditions this function
	 * 		 raises an exception, however implementations are encuraged to
	 * 		 perform basic combability and sanety checks to ensure consistency
	 * 		 of *this.
	 */
	virtual
	void
	endOfTestProcess()
	 override;


	/*
	 * ===========================
	 * Split Inspection Functions
	 */
public:
	/**
	 * \brief	This function computes the split sequences.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 *
	 *
	 * \throw 	If \c *this is not in a consistent state.
	 * 		 If the object is not able to compute this sequence.
	 * 		 Implementaations can further strengthen this requirement.
	 * 		 Also if no tests were performed yet.
	 *
	 * \notice	In erlier versions of this interface, this function was
	 * 		 requiered to be thread safe. It was learned that this
	 * 		 requierement may not be able to fulfilled, so it was droped.
	 */
	virtual
	SplitSequence_t
	computeSplitSequence()
	 const
	 override;


	/**
	 * \brief	This function returns true if at least one split has to be performed.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 */
	virtual
	bool
	areSplitsToPerform()
	 const
	 override;


	/**
	 * \brief	This function checks if the split was the same.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 *
	 * \throw 	If there was no update of this is inconsistent.
	 * 		 Also some underlying functin may throw.
	 *
	 * \note 	This means that a false of this fucntion, means
	 * 		 that the splits have a new order.
	 */
	virtual
	bool
	areSplitsStable()
	 override;


	/*
	 * =================================
	 * Status fucntion
	 *
	 * This fucntion gives information about the state of
	 * *this.
	 *
	 */
public:
	/**
	 * \brief	This function returns \c true if
	 * 		 \c *this is consistent.
	 *
	 * For a extended description of the function see the super class.
	 *
	 * \throw	If *this is not associated to any node.
	 */
	virtual
	bool
	isConsistent()
	 const
	 override;



	/**
	 * \brief	This function returns true if *this
	 * 		 is \e associated with a node.
	 *
	 * See the superclass for an detained description.
	 */
	virtual
	bool
	isAssociated()
	 const
	 override;


	/**
	 * \brief	This function returns true if all
	 * 		 tests of \c *this are done.
	 *
	 * See the super class for a detained description.
	 *
	 * \throw	If *this is not associated.
	 */
	virtual
	bool
	allTestsDone()
	 const
	 override;


	/**
	 * \brief	This function returns true if the test
	 * 		 of dimension \c d was completted.
	 *
	 * See the GOF interface for more information.
	 *
	 * \param  d 	The dimension to test.
	 *
	 * \throw	If *this is not associated or the dimension is invalid.
	 *
	 * \note	This function previously also acceped negative values.
	 * 		 Then it checked all the dimensions, this feature/abuse
	 * 		 was removed.
	 */
	virtual
	bool
	isDimensionTested(
		const Int_t 	d)
	 const
	 override;




	/*
	 * =====================
	 * None State Query Functions
	 *
	 * This functions gives information not about the
	 * state but of the, lets call it static state information.
	 * This is the stuff stat does nto change.
	 */
public:
	/**
	 * \brief	This function returns the number of
	 * 		 tests that *this managaes.
	 *
	 * See the GOF tester interface.
	 *
	 * This function will/should never return zero-.
	 *
	 * \throw 	If the function ould attemp to return zero.
	 *
	 */
	virtual
	Size_t
	nDifferentTests()
	 const
	 override;


	/**
	 * \brief	This function returns a list of the different
	 * 		 doimensions that are tested.
	 *
	 * See the GOF interface for more information.
	 *
	 * \throw	 Generally unspecific, but underling functions may throw.
	 */
	virtual
	DimList_t
	getAllTestKeys()
	 const
	 override;




	/*
	 * ====================
	 * Update functions
	 *
	 * These functions are concerned with teh update functionality
	 *
	 */
public:
	/**
	 * \brief	This function adds the new sample \c nSample to \c *this.
	 *
	 * See the super class to see a description of the function.
	 *
	 *
	 * \throw 	It is not defined when an exception is raised.
	 * 		 However it is recomended that some sanaty checs are performed.
	 */
	virtual
	void
	addNewSample(
		const Sample_t&		nSample,
		const HyperCube_t&	domain)
	 override;


	/**
	 * \brief	This functions starts the updazte process of *this.
	 *
	 * See GOF interface foir a description.
	 *
	 * \throw	This fucntion throws if *this is not associated or
	 * 		 if *this is \e consistent.
	 */
	virtual
	void
	preCommitHookInit()
	 override;


	/**
	 * \brief	Commit the changes of \c *this .
	 *
	 * See the GOF interface for a description.
	 *
	 *
	 * \param  parModel	A pointer to the parameteric model.
	 * \param  subTreeRoot	The node where *this belongs to,
	 * 			 this must not be neccessaryaly a leaf.
	 * \param  d		The dimension that should be tested.
	 *
	 * \return 	This function returns true if the testing was successfull,
	 * 		 this means that all test could be done.
	 * 		 If false is returned yggdrasil will gnerate an exception.
	 *
	 * \throw	An exception is raised if the preCommitInitHook() was not called.
	 * 		 This also means that the dimension \c d is not checked.
	 * 		 Implementation can enforce more restrictive requierements.
	 */
	virtual
	bool
	commitChanges(
		cParModel_ptr 	parModel,
		NodeFacade_t 	subTreeRoot,
		const Int_t 	d)
	 override;



	/*
	 * ======================0
	 * Internal helper functions
	 *
	 * These are internal functions that are private and
	 */
private:
	/**
	 * \brief	This fucntion returns the number of
	 * 		 bins that have to be used for the test.
	 *
	 * \throw	This funciton throws if *this is not associated.
	 */
	Size_t
	getEstimatedNBins()
	 const;


	/**
	 * \brief	This function returns the degree
	 * 		 of freedom that has to be used.
	 *
	 * The degree of freedom (DOF) is given by:
	 * 	N - f - 1
	 * Where N is the number of bins and f is the number
	 * of parameters that are bound by the parameteric model.
	 *
	 * This number can also be negative, which indicates that we
	 * will allways accept the test.
	 *
	 * \throw 	If *thsi is not associated.
	 */
	Numeric_t
	getDOF()
	 const;


	/**
	 * \brief	This function returns true if the test is trival.
	 *
	 * A test if trivial if the null hypotheis is accepted by default.
	 * For example this indicates that the Dof value is negative.
	 *
	 * \throw	If *this is not allocated.
	 */
	bool
	isTestTrivial()
	 const;






	/*
	 * =================================
	 * Newly Publicied constructors
	 */
public:
	/**
	 * \brief	Default constructor
	 *
	 * Is deleted.
	 */
	yggdrasil_Chi2GOFTest_t()
	 noexcept
	 = delete;


	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_Chi2GOFTest_t(
		const yggdrasil_Chi2GOFTest_t&);


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_Chi2GOFTest_t(
		yggdrasil_Chi2GOFTest_t&&)
	 noexcept;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_Chi2GOFTest_t&
	operator= (
		const yggdrasil_Chi2GOFTest_t&);

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_Chi2GOFTest_t&
	operator= (
		yggdrasil_Chi2GOFTest_t&&)
	 noexcept;


	/*
	 * =======================
	 * Deleted constructors
	 *
	 * These constructors are deleted
	 * for safty reasons.
	 */
public:
	yggdrasil_Chi2GOFTest_t(
		const Int_t 	dim)
	 = delete;

	yggdrasil_Chi2GOFTest_t(
		const Size_t 	dim)
	 = delete;

	yggdrasil_Chi2GOFTest_t(
		const Numeric_t 	sigLevel,
		const Int_t 		d)
	 = delete;


	/*
	 * =========================
	 * Private Memebers
	 */
private:
	Numeric_t 	m_sigLevel;		//!< This is the significance level that should be used
	Chi2TTArray_t 	m_TTArray;		//!< Array of the chi2 test for the different dimensions
	TestRank_t 	m_testRes;		//!< This is the structire that holds all the test reuslts.
	Size_t 		m_nDims;		//!< THis is the number of dimensions
	bool 		m_isAssociated;		//!< This boll indicates if *this is associated or not.
	bool 		m_isConsistent;		//!< This bool indicates if *this is considiten
	Size_t 		m_estBins;		//!< This is the number of bins that is computed to use.
						//!<  This value is set in the pre init function.
	Numeric_t 	m_DOFs;			//!< This is the degree of freedom that the CHI2 test has.
						//!<  This value is set in the init funciton and can also be negative.
						//!<  In this case, no test has to be performed.
}; //End class: yggdrasil_Chi2GOFTest_t


YGGDRASIL_NS_END(yggdrasil)
