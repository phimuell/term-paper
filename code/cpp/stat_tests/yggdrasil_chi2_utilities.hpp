#pragma once
/**
 * \brief	This file implements some utility functions that are needed in the chi2 test.
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>


//Incluide std


YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \brief	Estimate the number of bins given a number of samples
 *
 * This function calculates the numbers of samples given a certain number of samples.
 * See also the code in the R function.
 *
 * \param  nSamples		The number of samples for which we want to calculate the number of bins.
 * \param  sigLevel 		This is the significance niveau, meaning alpha.
 * \param  isMultiVariate	This is used to tell \c *this that an independence test is constructed.
 *
 *
 * \note 	See: 	'Cochran, W.G., The Chi Square Test of Goodness of Fit. The Annals of Mathematical Statistics, 1952. 23(3): p. 315-345.'
 * 			'Mann, H.B. and A. Wald, On the Choice of the Number of Class Intervals in the Application of the Chi Square Test. The Annals of Mathematical Statistics, 1942. 13(3): p. 306-317.'
 * 			'Bagnato, L., A. Punzo, and O. Nicolis, The autodependogram: a graphical device to investigate serial dependences. Journal of Time Series Analysis, 2012. 33(2): p. 233-254.'
 */
Size_t
yggdrasil_chi2_calcNBins(
	const Numeric_t sigLevel,
	const bool 	isMultiVariate,
	const Size_t 	nSamples);

Size_t
yggdrasil_chi2_calcNBins(
	const bool 	isMultiVariate,
	const Numeric_t sigLevel,
	const Size_t 	nSamples)
 = delete;

Size_t
yggdrasil_chi2_calcNBins(
	const bool 	isMultiVariate,
	const Size_t 	nSamples,
	const Numeric_t sigLevel)
 = delete;


Size_t
yggdrasil_chi2_calcNBins(
	const Numeric_t sigLevel,
	const Size_t 	nSamples,
	const bool 	isMultiVariate)
 = delete;


Size_t
yggdrasil_chi2_calcNBins(
	const Size_t 	nSamples,
	const Numeric_t sigLevel,
	const bool 	isMultiVariate)
 = delete;

Size_t
yggdrasil_chi2_calcNBins(
	const Size_t 	nSamples,
	const bool 	isMultiVariate,
	const Numeric_t sigLevel)
 = delete;

YGGDRASIL_NS_END(yggdrasil)




