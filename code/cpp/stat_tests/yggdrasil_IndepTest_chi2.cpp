/**
 * \brief	This file implements the functions for the independence test with the chi2.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <stat_tests/yggdrasil_IndepTest_chi2.hpp>
#include <stat_tests/yggdrasil_chi2_utilities.hpp>

#include <interfaces/yggdrasil_interface_GOFTest.hpp>

//Incluide std
#include <memory>

//Include boost
#include <boost/math/distributions/chi_squared.hpp>



YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_Chi2IndepTest_t::IndepTest_ptr
yggdrasil_Chi2IndepTest_t::creatOffspring()
 const
{
	if(m_dims == 0)
	{
		return IndepTest_ptr(new yggdrasil_Chi2IndepTest_t(m_sigLevel));
	}
	else
	{
		return IndepTest_ptr(new yggdrasil_Chi2IndepTest_t(m_dims, m_sigLevel));
	};
};




yggdrasil_Chi2IndepTest_t::SplitSequence_t
yggdrasil_Chi2IndepTest_t::computeSplitSequence()
 const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(m_testRank.isComplete());
	yggdrasil_assert(m_sigLevel > 0.0);
	yggdrasil_assert(m_indepTests.empty() == true); //The helper structs are needed to be removbed
	yggdrasil_assert(m_binEdges.empty() == true);
	yggdrasil_assert(m_estNBins > 0);

	//Tghis must be associated, all tests done and and the rank must be sorted.
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test is not associated.");
	};
	if(this->allTestsDone() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Not all tests are done.");
	};
	if(this->m_testRank.isSorted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test ranks are not sorted.");
	};

	/*
	 * In the case of a trivial test, we have accepted all
	 * null hypothesis.
	 */
	if(this->isTestTrivial() == true)	//This also includes dim == 1 and the m_estNBin == 1
	{
		yggdrasil_assert(m_estNBins >= 1);
		yggdrasil_assert((m_dims == 1) ? (this->nDifferentTests() == 0) : (m_estNBins == 1));
		yggdrasil_assert(::std::all_of(m_testRank.begin(), m_testRank.end(),
				[](const SingleTestRes_t& s) -> bool {return (s.pValue() > 1.0);}));

		//Return empty sequence to indicate that there are no tests to perform
		return SplitSequence_t();
	}; //End only one dime

	/*
	 * Many dimensions
	 */
	//Now we will get the pValue and test it is below the significance niveau
	//If not then the empty sequence is returned
	const auto&      testSmallestPValue = m_testRank.getRank(0);
	const DimPair_t& dimToSplit         = testSmallestPValue.key();
	const Numeric_t  pValue             = testSmallestPValue.pValue();

	//Test if the smalles p Value is below the significance level
	if(pValue < m_sigLevel)
	{
		//We have to split
		SplitSequence_t ss = SplitSequence_t()
			.copyAndAddNewLevelOfSplit(dimToSplit.getFirstDim())
			.copyAndAddNewLevelOfSplit(dimToSplit.getSecondDim());

		yggdrasil_assert(ss.nSplits() == 2);
		yggdrasil_assert(ss.isValid() == true);

		yggdrasil_assert(this->nDifferentTests() > 0);

		return ss;
	}; //End there is something to split

	/*
	 * There is noting to split, so return the empty sequence
	 */
	return SplitSequence_t();
}; //End: compueSplitSequence


bool
yggdrasil_Chi2IndepTest_t::areSplitsToPerform()
 const
{
	yggdrasil_assert(m_dims > 0);
	yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(m_testRank.isComplete());
	yggdrasil_assert(m_sigLevel > 0.0);
	yggdrasil_assert(m_indepTests.empty() == true); //The helper structs are needed to be removbed
	yggdrasil_assert(m_binEdges.empty() == true);
	yggdrasil_assert(m_estNBins > 0);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test is not associated.");
	};
	if(this->allTestsDone() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Not all tests are done.");
	};
	if(this->m_testRank.isSorted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test ranks are not sorted.");
	};

	/*
	 * If the test is trivbial, we can imediatly say that
	 * it is the case
	 */
	if(this->isTestTrivial() == true)	//Also includes the dimenison case
	{
		yggdrasil_assert(m_dims > 0);
		yggdrasil_assert(m_estNBins > 0);
		yggdrasil_assert((m_dims == 1) || (m_estNBins == 1));

		return false;
	}; //End: trivial case


	/*
	 * We only have to search for the smalles p value
	 * and test if it is below the thresh hold.
	 */
	const auto&      testSmallestPValue = m_testRank.getRank(0);
	const Numeric_t  pValue             = testSmallestPValue.pValue();

	//Test if the smalles p Value is below the significance level
	if(pValue < m_sigLevel)
	{
		//The p value is too low, wo there is a splitting
		return true;
	};

	/*
	 * If we are here, then there are no split to perform
	 */
	return false;
}; //End: are splits to be done


bool
yggdrasil_Chi2IndepTest_t::areSplitsStable()
{
	throw YGGDRASIL_EXCEPT_illMethod("invalid method.");
}; //End: areSplitsStable



void
yggdrasil_Chi2IndepTest_t::preInitialHookInit(
	const NodeFacade_t 	nodeToAssosiate)
{
	//This must not be associated yet and the tests and oder stuff must be empty as well
	if(this->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The independence test is already associated.");
	};
	if(m_binEdges.size() != 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bin edge array is also allocated.");
	};
	if(m_indepTests.size() != 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test tabels are allocated as well.");
	};
	if(m_isInitialTest != true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this is not in the initial testing phace.");
	};
	if(nodeToAssosiate.isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node that was given to the indeopendence test for association, was not okay.");
	};
	if(nodeToAssosiate.isParModelFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The parameteric model is not fuilly fitted.");
	};
	if(nodeToAssosiate.getGOFTest()->areSplitsToPerform() == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The independence testing function was calle, but the gof test has splits.");
	};
	if(nodeToAssosiate.getIndepTest() != this)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("What are you tring to do, the node does not own the independence test.");
	};


	//Load the domain of the node
	const HyperCube_t& domain = nodeToAssosiate.getDomain();
	yggdrasil_assert(domain.isValid());

	//This bool indicates if the dimension was set in the beginning or not
	const bool dimWasSet = (m_dims != 0) ? true : false;


	//Set the dimensions
	//We can not bound yet
	if(m_dims == 0)
	{
		//Load the dimenison
		m_dims = nodeToAssosiate.nDims();
	}; //End if: late bind

	if(nodeToAssosiate.nDims() != m_dims)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The sample collection of the node and the gof test have a different doimensions.");
	};
	if(m_dims >= Constants::YG_MAX_DIMENSIONS)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is too large.");
	};

	/*
	 * Make *This associated
	 *
	 * We will not construct the test rank yet.
	 * This will be postponed.
	 *
	 * However *this will be a biut inconsistent.
	 */
	m_isAssociated = true;

	//Get the number of samples
	const Size_t nSamples = nodeToAssosiate.nSamples();

	/*
	 * Caclulating the number of bins
	 */
	yggdrasil_assert(m_estNBins == 0);	//Has still to hold
	m_estNBins = yggdrasil_chi2_calcNBins(m_sigLevel, true, nSamples);
	yggdrasil_assert(m_estNBins >= 1);
	yggdrasil_assert(m_estNBins == 1 ? this->isTestTrivial() : true);


	if(m_dims > 1 && this->isTestTrivial() == false)
	{
		//We now have to bound
		m_testRank.lateBind(m_dims * (m_dims - 1) / 2);

		/**
			* Allocating all the tables.
			*
			* We will also test if we must trivaly bind them
			*/
		m_binEdges.resize(m_dims);

		/*
		* Creating all the interactions that are needed
		*/
		for(Size_t i = 0; i != m_dims; ++i)
		{
			//Get the length of the first dimension
			const auto iLength = domain[i].getLength();

			//test if the first dimension is too small,
			//this means that we can tribvial bind the bins
			if(iLength < Constants::YG_SMALLEST_LENGTH)
			{
				//Encountered a trivial test because of its size
#if defined(YGGDRASIL_PRINT_LENGTH_ISSUE) && YGGDRASIL_PRINT_LENGTH_ISSUE == 1
				std::cout << " >> Encountered a small element in the independence test. Dimension " << i << " is too small." << std::endl;
#endif
				m_binEdges.at(i).trivialBind();
			};

			for(Size_t j = i + 1; j != m_dims; ++j)
			{
				//This is the current key
				const DimPair_t key(i, j);

				//Get the second length
				const auto jLength = domain[j].getLength();

				//Test if we have to create such a table
				if(((iLength < Constants::YG_SMALLEST_LENGTH) || (jLength < Constants::YG_SMALLEST_LENGTH)))
				{
					//We must crate a trivial test
#if defined(YGGDRASIL_PRINT_LENGTH_ISSUE) && YGGDRASIL_PRINT_LENGTH_ISSUE == 1
					std::cout << " >> Encountered a samll element in the independence test. The test (" << i << ", " << j << ") will be skipped." << std::endl;
#endif
					m_testRank.addTrivialTestInstance(key);
				}
				else
				{
					//Ad a not trivial test
					m_testRank.addTestInstance(key);

					if(m_indepTests.count(key) != 0)
					{
						throw YGGDRASIL_EXCEPT_LOGIC("The key " + key.print() + " is already inside the map.");
					};

					//Insering, the operator[] will default construct a map
					(void)(m_indepTests[key]);

					if(m_indepTests.count(key) != 1)
					{
						throw YGGDRASIL_EXCEPT_LOGIC("The key " + key.print() + " was not inserted.");
					};
				}; //End else: we have a trivial test
			}; //End for(j): second dimension
		}; //End for(i): first dim

		yggdrasil_assert(m_indepTests.size() == m_testRank.nDifferentTests());
	}
	else
	{
		//Only bound if the dimension was not set
		if(dimWasSet == false)
		{
			//The bounding is zero
			m_testRank.lateBind(0);
		};
	};

	//Trequier that this is assoicated
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this was not able to become associated.");
	};
	if(this->m_testRank.isComplete() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The test rank is not complete.");
	};

#ifndef NDEBUG
	for(const SingleTestRes_t& res : m_testRank)
	{
		const auto key = res.key();
		if(res.isTrivialTest() == true)
		{
			yggdrasil_assert(m_indepTests.count(key) == 0);
		}
		else
		{
			yggdrasil_assert(m_indepTests.count(key) == 1);
		};
	}; //End for(res)
#endif

	return;
}; //End: preInitialHookInit




void
yggdrasil_Chi2IndepTest_t::preComputeDimension(
	const Int_t 		d,
	cParModel_ptr 		parModel,
	const NodeFacade_t& 	subTreeNode)
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The precompute functionw as called obut *This was not associated.");
	};
	if(parModel->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The model is not associated, that should be used for the precomputation of dinmension " + std::to_string(d));
	};
	if(parModel->isConsistent() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The model for precomputing dimension " + std::to_string(d) + " is not consistent.");
	};
	if(parModel->allDimensionsFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Not all dimension of the models are fitted.");
	};
	if(subTreeNode.isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node for precomputing dimension " + std::to_string(d) + " is not okay.");
	};
	if(this->m_isInitialTest == true)
	{
		//In this calse we are guaranteed to get a leaf
		if(subTreeNode->isLeaf() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The node for precomputing dimension " + std::to_string(d) + " is not a leaf.");
		};
	}//End if: it is the initial test
	else
	{
		//In the case that not an initail fitt is performed
		//we can not say for sure if we get a leaf or not, but we know
		//that we must get a truesplit
		if(subTreeNode.isLeaf() == false)
		{
			if(subTreeNode.isTrueSplit() == false)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("The precompution function must get a leaf or a true split, but it get neither of thge tow.");
			}; //End: is not a true split
		}; //End: is not a leaf
	}; //End else: it is not the initial test

	if(m_binEdges.at(d).isTrivial() == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Called the precompute function on a trivial dimension.");
	};

	/*
	 * Test if the test are trivial
	 * For that we can use the all in one code
	 */
	if(this->isTestTrivial() == true)
	{
		yggdrasil_assert((m_dims == 1) ? (d == 0) : true);
		yggdrasil_assert(m_binEdges.empty() == true);
		yggdrasil_assert(m_indepTests.empty() == true);

		//Return we have done all
		return;
	}; //End if: test is trivial


	//Test if the structures are allocated
	if(m_binEdges.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bin is not allocated.");
	};

	/*
	 * Now performe the precomputation.
	 */
	BinEdgesSS_t& thisBinEdge = m_binEdges.at(d);	//Save access

	//Test if this dimension was already precomputed.
	//This is done by testing if the underling array is
	//associated.
	if(thisBinEdge.isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bin edges for dimension " + std::to_string(d) + " is already associated. This could indicate that a doimension is tried to be precomputed twice.");
	};

	//Get the dimensional array
	const DimArray_t& dimArray = subTreeNode.getDimensionalArray(d);

	//Get the number of samples
	const Size_t nSamples = dimArray.nSamples();

	//Get the domain of the node
	const HyperCube_t domain = subTreeNode.getDomain();

	//Comnpute the number of bins that should be made
	const Size_t numberOfBins = this->getEstNBins();

	//Make a smll test
	yggdrasil_assert(numberOfBins == yggdrasil_chi2_calcNBins(m_sigLevel, true, nSamples));
	yggdrasil_assert(numberOfBins > 0);

	//Perform the association of the edges
	thisBinEdge = parModel->generateBins(
			d,
			numberOfBins,
			domain.at(d)
			);


	//Test if it was successfull
	if(thisBinEdge.isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The building of the edges did not work.");
	};
	yggdrasil_assert(numberOfBins == thisBinEdge.nBins());

	/*
	 * This if we have enough space to compute and
	 * use a partitioning.
	 * For that we have to compute the number of sampels
	 * that could be used.
	 */
	const Size_t nMaxSampleForPart = Constants::TEMP_MEMORY_INDEPTEST_MB * 1024 * 1024 / (domain.nDims() * sizeof(::yggdrasil::SortIndex_t));

	//Van only add a partitojning, if this is a leaf
	if(nSamples < nMaxSampleForPart && subTreeNode.isLeaf())
	{
		//Use the partitioning
		thisBinEdge.addPartition(subTreeNode.getDimensionalArray(d));
	}; //ENd if: no partitioning used


	//We can now return
	return;
}; //End: precompute dimension


bool
yggdrasil_Chi2IndepTest_t::performInitailTest(
	const DimPair_t 	dimPair,
	const NodeFacade_t& 	leaf)
{
	yggdrasil_assert(m_testRank.isComplete());
	yggdrasil_assert(m_testRank.isTrivialTest(dimPair) == false);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this si not associated. This should be because in the precompute function *this is associate.");
	};
	if(this->isPairTested(dimPair) == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The pair " + dimPair.print() + " was already tested.");
	};
	if(leaf.isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The node for precomputing dimension " + dimPair.print() + " is not okay.");
	};
	if(leaf.isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node that was passed to the initial fitting function of the pair " + dimPair.print() + " is not a leaf.");
	};
	if(leaf.isDirty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The leaf that was passed to this is dirty.");
	};

	yggdrasil_assert(m_dims > 1); //The dimension must be largter than one,
				      // because otherwhise this function should not be called

	/*
	 * Load some parameters
	 */

	//Now load the index of the two interactions that are needed
	const auto dim1 = dimPair.getFirstDim();
	const auto dim2 = dimPair.getSecondDim();

	//Get the domain
	const HyperCube_t& domain = leaf.getDomain();
	yggdrasil_assert(domain.isValid() == true);

	//The length value is a bit different in this case.
	//see detconstruct.R, function dimstosplit, somewhere at line 235
	const Numeric_t length1 = domain.at(dim1).getLength();
	const Numeric_t length2 = domain.at(dim2).getLength();

	//This is the size value, that is used as tie breaker.
	//Notice that the 1/ that is used in the R code is iocooperated
	//by the comparisson operationr
	const Numeric_t szeVal = length1 * length2;

	/*
	 * Handle the trivial test case
	 */
	if(this->isTestTrivial() == true)
	{
		yggdrasil_assert(m_binEdges.empty() == true);
		yggdrasil_assert(m_indepTests.empty() == true);

		//Save the test result as passed
		const Numeric_t pValue = 1.0001;

		//Store it
		yggdrasil_assert(m_testRank.isSpecificTestDone(dimPair) == false);
		m_testRank.storeTestResult(dimPair, pValue, szeVal);

		//Return since we have done what we have to
		return m_testRank.isSpecificTestDone(dimPair);
	}; //End vial test case

	/*
	 * Non trivial test case
	 */
	yggdrasil_assert(m_binEdges.empty() == false);
	yggdrasil_assert(m_indepTests.empty() == false);

	//Load the frequency table object that is associated to this value
	FreqTable_t& thisFreqTable = m_indepTests.at(dimPair);

	//Test if it is allready associated, this is a safty check
	if(thisFreqTable.isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The frequency table of the pair " + dimPair.print() + " is allready associated, this may indicate that the initial fitting fucntion was called twice with the same argument.");
	};

	/*
	 * Load more parameters
	 */

	//Now load the binedges of that dimension
	const BinEdgeArray_t& binEdges1 = m_binEdges.at(dim1);
	const BinEdgeArray_t& binEdges2 = m_binEdges.at(dim2);

	yggdrasil_assert(binEdges1.isAssociated() == true);
	yggdrasil_assert(binEdges2.isAssociated() == true);
	yggdrasil_assert(binEdges1.nBins() == binEdges2.nBins());
	yggdrasil_assert(binEdges1.nBins() == this->getEstNBins());

	//If we are here me must have more than one bin
	yggdrasil_assert(binEdges1.nBins() > 1);


	//Load the dimensional arraies
	const DimArray_t& dimArray1 = leaf.getDimensionalArray(dim1);
	const DimArray_t& dimArray2 = leaf.getDimensionalArray(dim2);

	yggdrasil_assert(dimArray1.nSamples() == leaf.getMass());
	yggdrasil_assert(dimArray2.nSamples() == leaf.getMass());
	yggdrasil_assert(dimArray1.getAssociatedDim() == dim1);
	yggdrasil_assert(dimArray2.getAssociatedDim() == dim2);

	//Zero sample should not be handled
	if(leaf.getMass() == 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("A leaf with zero mass should not habve been processed here.");
	};


	//Create the frequency table, more associate it
	thisFreqTable.buildInitialFreqTable(
		dimArray1, binEdges1,	//The first dimension
		dimArray2, binEdges2,	//The second dimension
		true 			//Use the sorting algorithm.
		);
	yggdrasil_assert(thisFreqTable.isAssociated() == true);
	yggdrasil_assert(thisFreqTable.getDOF() > 0.0);
	yggdrasil_assert(yggdrasil_approxTheSame(thisFreqTable.getDOF(), this->getDOF()) == true);

	/*
	 * Now we compute the CHI2 value
	 */
	const auto Chi2StatValue = thisFreqTable.computeChi2Value();

	if(::std::isnan(Chi2StatValue) == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The chi2 value of the pair " + dimPair.print() + ", was not a valid number.");
	};
	if(Chi2StatValue < 0.0)	//We allow a zero value
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The computed chi2 value of dimension " + dimPair.print() + ", was negative, it was " + std::to_string(Chi2StatValue));
	};


	/*
	 * Get the DOF, it is important that we also check for zero values,
	 * since zero is allready handled with the test above.
	 */
	const auto DOF = thisFreqTable.getDOF();
	if(DOF <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dof value of pair " + dimPair.print() + " was negative, it was " + std::to_string(DOF));
	};

	//This is the variable for the p value
	Numeric_t pValue;

	//Boost has problems with infinity value of CHI2, so we will patch it
	if(std::isinf(Chi2StatValue) == false)
	{
#ifdef YGGDRASIL_INDEP_CHI2_TEST_AUTO_FAIL
		/*
		 * It is very stupid to do it that way, but it is very easy and
		 * it is the least intrusive way to do it.
		 */
		pValue = 0.0;
#else
		//The value is not infinity, so we can compute it
		::boost::math::chi_squared_distribution<Numeric_t> chi2Dist(DOF);
		pValue = ::boost::math::cdf(::boost::math::complement(chi2Dist, Chi2StatValue));
#endif
	}
	else
	{
		//The value of the test statistic is infinity, this means that the
		//Null was infinitly strong rejected. Thus the pValue has to be zero
		pValue = 0.0;
	};

	if(isValidFloat(pValue) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The p value is not a valid number.");
	};
	yggdrasil_assert(pValue >= 0.0);


	//Now store the information
	m_testRank.storeTestResult(dimPair, pValue, szeVal);

	return (m_testRank.isSpecificTestDone(dimPair));
}; //End initail testing



void
yggdrasil_Chi2IndepTest_t::endOfTestProcess()
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The end of test process fucntion was called on a not associated test.");
	};
	if(m_testRank.isComplete() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The end of test process fucntion was called, but the test rank object in incomplete.");
	};
	if(m_testRank.isSorted() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The m_test rank is already sorted. This could means that the endOfTestProcess funciton was called twice");
	};
	if(this->allTestsDone() == false)
	{
		yggdrasil_assert(m_testRank.allTestsDone() == true);
		throw YGGDRASIL_EXCEPT_LOGIC("The end of test process fucntion was called, but not all test where done.");
	};
	if(this->isTestTrivial() == true)
	{
		//Test that have to hold only in the case of more than one dimension
		if(m_indepTests.empty() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The end of test process fucntion was called, but the independence test map is empty.");
		};
		if(m_binEdges.empty() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The end of test process fucntion was called, but the bin edges array was empty.");
		};
	}; //End if: only more than one dimensions


	//The initial fitting is now done
	m_isInitialTest = false;

	//Sort the test rank
	m_testRank.sortResult();
	yggdrasil_assert(m_testRank.isSorted() == true);

	/*
	 * We only have to clear up the memeory if the tests
	 * were noit trivial.
	 */
	if(this->isTestTrivial() == false)
	{
		if(m_indepTests.size() != this->nDifferentTests())
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The frequency table list has not the correct length.");
		};
		if(m_binEdges.empty() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The list of the different bins is empty.");
		};
		if(m_binEdges.size() != m_dims)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The list of the different bins has the wrong length.");
		};

		//unbind the frequency table
		for(auto& freqTableP : m_indepTests)
		{
			yggdrasil_assert(m_testRank.isSpecificTestDone(freqTableP.first) == true);
			yggdrasil_assert(m_testRank.isTrivialTest(freqTableP.first) == false);

			/*
			 * Previously we where not able to guarantee if a frequency
			 * table was allocated or not.
			 * However since the introduction of the "isTestTrivial()" function,
			 * and its extension to also consoidering the bins we can say that.
			 *
			 * So now e know that if the test is not trivial,
			 * then we must have that all tabels are present
			 * and associated.
			 */

			if(freqTableP.second.isAssociated() == false)
			{
				yggdrasil_assert(this->isPairTested(freqTableP.first) == false);
				throw YGGDRASIL_EXCEPT_RUNTIME("Found a test that is not associated.");
			};

			//Unbound the data
			freqTableP.second.unboundData();

			//Test if table is associated
			if(freqTableP.second.isAssociated() == true)
			{
				//Table is associated so simple deleting

				//Unbound the frequency table
			}; //End if: associated

			//Final paranoia check if the tabel was successfully cleared
			yggdrasil_assert(freqTableP.second.isAssociated() == false);
		}; //ENd for(freqTableP

		//Use the same trick with the swapping such that we are sure that the
		//map realy is cleared
		IndepTests_t().swap(m_indepTests);

		//unbound all the bins
		for(BinEdgesSS_t& bin : m_binEdges)
		{
			if(bin.isTrivial() == false)
			{
				yggdrasil_assert(bin.isAssociated() == true);
				bin.unboundData();
				yggdrasil_assert(bin.isAssociated() == false);
			};
		}; //End for(bin): unbinding

		//We have also to clear the structure
		BinEdgesDim_t().swap(m_binEdges);

	}; //End if: clearing the memory that was allocated.

	// we are done with the clean up
	return;
}; //End: endOfTestProcess




bool
yggdrasil_Chi2IndepTest_t::isPairTested(
	const DimPair_t 	dimPair)
 const
{
	//Test if *this is associated
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this is associated.");
	};
	if(m_testRank.isComplete() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The test rank score is not complete.");
	};

	return m_testRank.isSpecificTestDone(dimPair);
}; //End: isDimensionTested


bool
yggdrasil_Chi2IndepTest_t::allTestsDone()
 const
{
	//Test if *this is associated
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this is associated.");
	};
	if(m_testRank.isComplete() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The test rank score is not complete.");
	};

	return m_testRank.allTestsDone();
}; //End: isDimensionTested


Size_t
yggdrasil_Chi2IndepTest_t::nDifferentTests()
 const
{
	//Return the number of the supposed tests
	return m_testRank.nDifferentTests();
}; //End: nDifferentKeys()


yggdrasil_Chi2IndepTest_t::DimPairList_t
yggdrasil_Chi2IndepTest_t::getAllTestKeys()
 const
{
	//It is not needed that it is associated but we need to be complete
	if(m_testRank.isComplete() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test rank is incomplete. This indicates that the preInitialHookInit() function was not called.");
	};

	//Use the function of the test rank object to get all the keys
	const DimPairList_t keyList(m_testRank.getKeyList());
	yggdrasil_assert(keyList.size() == this->nDifferentTests());
	yggdrasil_assert(keyList.size() == this->m_indepTests.size());

	return keyList;
}; //End: getAllTestKeys()



Size_t
yggdrasil_Chi2IndepTest_t::nPreComputeDimensions()
 const
{
	//Now we must test how many non trivial dimensions we have
	Size_t nonTrivDims = 0;
	for(const BinEdgeArray_t& bin : m_binEdges)
	{
		if(bin.isTrivial() == false)
		{
			nonTrivDims += 1;
		};
	}; //End for(bin):

	return nonTrivDims;
}; //End: nPreComputeDimension


yggdrasil_Chi2IndepTest_t::DimList_t
yggdrasil_Chi2IndepTest_t::getAllPrecomputeKeys()
 const
{
	DimList_t dimList;
	yggdrasil_assert(this->isTestTrivial() ? m_binEdges.empty() : m_dims == m_binEdges.size());

	if(this->isTestTrivial() == false)
	{
		dimList.reserve(m_dims);
		for(Size_t i = 0; i != m_dims; ++i)
		{
			if(m_binEdges[i].isTrivial() == false)
			{
				dimList.push_back(i);
			};
		}; //ENd for(i)
	}; //End if: Test is trivial

	return dimList;
}; //End: getAllProcomp key

bool
yggdrasil_Chi2IndepTest_t::isAssociated()
 const
{
	/*
	 * Lets ask George Bool what he thinks
	 */
	if(m_dims == 1)
	{
		return m_isAssociated;
	}

	if(m_isAssociated == true)
	{
		yggdrasil_assert(m_testRank.isComplete() == true);
		yggdrasil_assert(m_dims > 0);
		yggdrasil_assert(m_dims < Constants::YG_MAX_DIMENSIONS);
		return true;
	}; //ENd true case


	//Nothing of teh sort
	return false;
}; //End: isEmpty


bool
yggdrasil_Chi2IndepTest_t::isConsistent()
 const
{
	std::cerr << "Not iomplemented method." << std::endl;
	std::abort();
};


void
yggdrasil_Chi2IndepTest_t::addNewSample(
	const Sample_t&		nSample,
	const HyperCube_t&	domain)
{
	yggdrasil_assert(m_isInitialTest == false);
	throw YGGDRASIL_EXCEPT_illMethod("uniomplemented Methdo");
	(void)nSample;
	(void)domain;
};


void
yggdrasil_Chi2IndepTest_t::preCommitHookInit()
{
	yggdrasil_assert(m_isInitialTest == false);
	throw YGGDRASIL_EXCEPT_illMethod("Implement me.");
};

bool
yggdrasil_Chi2IndepTest_t::commitChanges(
	const DimPair_t 	dimPair,
	const NodeFacade_t& 	subTreeRoot)
{
	throw YGGDRASIL_EXCEPT_illMethod("Unimplemented Method.");
	(void)dimPair;
	(void)subTreeRoot;
};



yggdrasil_Chi2IndepTest_t::yggdrasil_Chi2IndepTest_t(
	const Numeric_t 	sigLevel)
 :
  m_testRank(),
  m_indepTests(),
  m_binEdges(),
  m_dims(0),
  m_estNBins(0),
  m_sigLevel(sigLevel),
  m_isAssociated(false),
  m_isInitialTest(true)	//By definiton at the beginning we are in the initial fitting face
{
	if(m_sigLevel < 0.0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The significance level that was provided, " + std::to_string(m_sigLevel) + ", is negative.");
	};
	if(1.0 < m_sigLevel)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The significance level that was provided, " + std::to_string(m_sigLevel) + ", is greater than one.");
	};


	/*
	 * The test stables and so on, will be created by the
	 * preInitialHookInit() fucniton, when they are needed.
	 *
	 * Previously this was done by the constructor.
	 */
}; //End building constzuctor


yggdrasil_Chi2IndepTest_t::yggdrasil_Chi2IndepTest_t(
	const Int_t 		d,
	const Numeric_t 	sigLevel)
 :
  m_testRank(),
  m_indepTests(),
  m_binEdges(),
  m_dims(0),	//This is the simplest way, this will lead to a later binding automatically
  m_estNBins(0),
  m_sigLevel(sigLevel),
  m_isAssociated(false),
  m_isInitialTest(true)	//By definiton at the beginning we are in the initial fitting face
{
	if(d == Constants::YG_INVALID_DIMENSION)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is invalid.");
	};
	if(d < 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is negative.");
	};
	if(d >= Constants::YG_MAX_DIMENSIONS)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is larger than the maximum.");
	};
	if(m_sigLevel < 0.0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The significance level that was provided, " + std::to_string(m_sigLevel) + ", is negative.");
	};
	if(1.0 < m_sigLevel)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The significance level that was provided, " + std::to_string(m_sigLevel) + ", is greater than one.");
	};


	/*
	 * Now we will bind in the preInitialHookInit() function.
	 *
	 * This is easier.
	 */



	/*
	 * The test stables and so on, will be created by the
	 * preInitialHookInit() fucniton, when they are needed.
	 *
	 * Previously this was done by the constructor.
	 */
}; //End building constzuctor


yggdrasil_Chi2IndepTest_t::~yggdrasil_Chi2IndepTest_t()
 noexcept
 = default;


yggdrasil_Chi2IndepTest_t::yggdrasil_Chi2IndepTest_t(
	const yggdrasil_Chi2IndepTest_t&)
 = default;


yggdrasil_Chi2IndepTest_t::yggdrasil_Chi2IndepTest_t(
	yggdrasil_Chi2IndepTest_t&&)
 = default;


yggdrasil_Chi2IndepTest_t&
yggdrasil_Chi2IndepTest_t::operator= (
	const yggdrasil_Chi2IndepTest_t&)
 = default;


yggdrasil_Chi2IndepTest_t&
yggdrasil_Chi2IndepTest_t::operator= (
	yggdrasil_Chi2IndepTest_t&&)
 = default;



Size_t
yggdrasil_Chi2IndepTest_t::getEstNBins()
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this is not associated.");
	};
	yggdrasil_assert(m_estNBins >= 1);
	yggdrasil_assert(m_dims >= 1);

	return m_estNBins;
}; //ENd: getEstNBins

Numeric_t
yggdrasil_Chi2IndepTest_t::getDOF()
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this is not associated.");
	};
	yggdrasil_assert(m_estNBins >= 1);
	yggdrasil_assert(m_dims >= 1);

	const Numeric_t oneD = m_estNBins - 1.0;

	return (oneD * oneD);
}; //End: getDOF


bool
yggdrasil_Chi2IndepTest_t::isTestTrivial()
 const
{
	if(this->m_isAssociated == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this is not associated.");
	};
	yggdrasil_assert(m_estNBins >= 1);
	yggdrasil_assert(m_dims >= 1);

	return ((m_estNBins == 1) || (m_dims == 1)) ? true : false;
}; //End: isTestTrivial




 YGGDRASIL_NS_END(yggdrasil)


