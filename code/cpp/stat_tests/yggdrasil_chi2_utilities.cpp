/**
 * \brief	This file implements some utility functions that are needed in the chi2 test.
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <stat_tests/yggdrasil_chi2_utilities.hpp>


//Incluide std


//Include boost
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/normal.hpp>


YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \brief	Estimate the number of bins given a number of samples
 *
 * This function calculates the numbers of samples given a certain number of samples.
 * See also the code in the R function.
 *
 * \param  nSamples		The number of samples for which we want to calculate the number of bins.
 * \param  sigLevel 		This is the significance niveau, meaning alpha.
 * \param  isMultiVariate	This is used to tell \c *this that an independence test is constructed.
 *
 *
 * \note 	See: 	'Cochran, W.G., The Chi Square Test of Goodness of Fit. The Annals of Mathematical Statistics, 1952. 23(3): p. 315-345.'
 * 			'Mann, H.B. and A. Wald, On the Choice of the Number of Class Intervals in the Application of the Chi Square Test. The Annals of Mathematical Statistics, 1942. 13(3): p. 306-317.'
 * 			'Bagnato, L., A. Punzo, and O. Nicolis, The autodependogram: a graphical device to investigate serial dependences. Journal of Time Series Analysis, 2012. 33(2): p. 233-254.'
 */
Size_t
yggdrasil_chi2_calcNBins(
	const Numeric_t sigLevel,
	const bool 	isMultiVariate,
	const Size_t 	nSamples)
{
	if(std::isnan(sigLevel) == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The signiuficance lebvel is NAN.");
	};
	if(sigLevel <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("THe significance level ois zero or lower.");
	};


	//Regardless of the significance level, we allways return one
	//if the number of samples is zero
	if(nSamples == 0)
	{
		return 1;
	};

	//This is the standard normal distribution
	::boost::math::normal_distribution<Numeric_t> NormalDist;

	// Loading the parameters
	const Numeric_t n = (Numeric_t)nSamples;

	// c <- stats::qnorm(alpha, lower = FALSE)
	const Numeric_t c = ::boost::math::quantile(::boost::math::complement(NormalDist, sigLevel)); //Must be the complement I guess

	const Numeric_t ks_tmp = n * 0.2;  //Same as n / 5.0

	// kp is more complicated to calculate
	// 	kp <- 4*(2*(n-1)^2/c^2)^(1/5)
	const Numeric_t kp_1 = (n - 1.0 ) / c;
	const Numeric_t kp_2 = kp_1 * kp_1;
	const Numeric_t kp_3 = 2 * kp_2;
	const Numeric_t kp_4 = ::std::pow(kp_3, 0.2);
	const Numeric_t kp_tmp = 4 * kp_4;

	// Test if we have to make it for the multivariate
	const Numeric_t ks = (isMultiVariate == true) ? ::std::sqrt(ks_tmp) : ks_tmp;
	const Numeric_t kp = (isMultiVariate == true) ? ::std::sqrt(kp_tmp) : kp_tmp;

	//Now figguring out which is the final result
	// k <- max(1, floor(min(ks,kp)))
	const Size_t k = std::max<Size_t>(1, Size_t(std::floor(::std::min(ks, kp))));

	return k;

}; //End: caclualtze bins

YGGDRASIL_NS_END(yggdrasil)




