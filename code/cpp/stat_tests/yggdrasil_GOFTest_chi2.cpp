/**
 * \brief	This file implements the functions for the GOF Test with the chi2
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_treeNode.hpp>

#include <stat_tests/yggdrasil_GOFTest_chi2.hpp>
#include <stat_tests/yggdrasil_chi2_utilities.hpp>

//INcluide std
#include <memory>

//Include boost
#include <boost/math/distributions/chi_squared.hpp>




YGGDRASIL_NS_BEGIN(yggdrasil)


void
yggdrasil_Chi2GOFTest_t::endOfTestProcess()
{
	yggdrasil_assert(m_nDims > 0);
	yggdrasil_assert(m_nDims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(m_testRes.isComplete());
	yggdrasil_assert(m_estBins > 0);
	yggdrasil_assert(m_sigLevel > 0.0);
	yggdrasil_assert(this->isTestTrivial() == true ? (m_TTArray.empty()) : (m_TTArray.size() == m_nDims));

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Cab not unbound an empty GOF test.");
	};
	if(this->allTestsDone() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Not all test are done, so the end can not be here.");
	};
	if(m_testRes.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test results are not valid, but the end of process function was called.");
	};

	/*
	 * We only have to unbound if we have done some
	 * tests. The test table array could also be empty.
	 *
	 * This can happen for example if the domain is empty.
	 */
	if(this->isTestTrivial() == false)
	{
		/*
		 * We make use of the fact that we can oiterate
		 * only over the non trivial tests.
		 */
		//for(Size_t i = 0; i != m_nDims; ++i)
		for(const auto& i : this->getAllTestKeys() )
		{
			yggdrasil_assert(m_TTArray.at(i).isAssociated() == true);
			m_TTArray.at(i).unboundData();
			yggdrasil_assert(m_TTArray.at(i).isAssociated() == false);
		}; //End for(i): iterating
		yggdrasil_assert(::std::all_of(m_TTArray.cbegin(), m_TTArray.cend(),
			[](const Chi2TestTable_t& tt) -> bool {return tt.isAssociated() == false;}));

		//Clear the allocation
		Chi2TTArray_t emptyArray;
		m_TTArray.swap(emptyArray);
	}; //End unbounding

	//Also perform the sorting to get the split sequence
	m_testRes.sortResult();

	yggdrasil_assert(m_testRes.isSorted());
	yggdrasil_assert(this->isAssociated() == true);
	yggdrasil_assert(this->allTestsDone());

	//We are also now consistent again
	m_isConsistent = true;

	return;
}; //End: endOfProcess


Size_t
yggdrasil_Chi2GOFTest_t::nDifferentTests()
 const
{
	//For the reason of consistency this function
	//must return the number of non trivial tests
	return m_testRes.nDifferentTests();
};

yggdrasil_Chi2GOFTest_t::DimList_t
yggdrasil_Chi2GOFTest_t::getAllTestKeys()
 const
{
	if(m_testRes.isComplete() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test list is not complete. This indictaes the the preInitialHookInit() function was not called.");
	};

	/*
	 * Notice that this will only iterate over the non trivial tests
	 * This is a feature of Yggdrasil.
	 */
	DimList_t dimKeys = m_testRes.getKeyList();
	yggdrasil_assert(dimKeys.size() == m_testRes.nDifferentTests());

	return dimKeys;
}; //End: getKEys

yggdrasil_Chi2GOFTest_t::GOFTest_ptr
yggdrasil_Chi2GOFTest_t::creatOffspring()
 const
{
	yggdrasil_assert(m_sigLevel > 0.0);

	if(m_nDims != 0)
	{
		yggdrasil_assert(m_nDims > 0);
		yggdrasil_assert(m_nDims < Constants::YG_MAX_DIMENSIONS);
		return GOFTest_ptr(new yggdrasil_Chi2GOFTest_t(m_nDims, m_sigLevel));
	}
	else
	{
		return GOFTest_ptr(new yggdrasil_Chi2GOFTest_t(m_sigLevel));
	};
};

void
yggdrasil_Chi2GOFTest_t::preCommitHookInit()
{
	throw YGGDRASIL_EXCEPT_illMethod("Implement me.");
};



yggdrasil_Chi2GOFTest_t::yggdrasil_Chi2GOFTest_t(
	const Int_t 	d,
	const Numeric_t sigLevel)
 :
  m_sigLevel(sigLevel),
  m_TTArray(),
  m_testRes(),
  m_nDims(d),
  m_isAssociated(false),
  m_isConsistent(false),	//It is actually undefined, but we define it as inconsistent.
  m_estBins(0),
  m_DOFs(NAN)
{
	if(d == Constants::YG_INVALID_DIMENSION)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is invalid.");
	};
	if(d < 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is negative.");
	};
	if(d >= Constants::YG_MAX_DIMENSIONS)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension is larger than the maximum.");
	};
	if(::std::isnan(m_sigLevel) == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The significance level is nan");
	};
	if(m_sigLevel <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The significance level is to small, it was " + std::to_string(m_sigLevel));
	};
	if(1.0 <= m_sigLevel)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The significance level is to large, it was " + std::to_string(m_sigLevel));
	};

	//To be able to support late binding
	if(m_nDims != 0)
	{
		m_testRes.lateBind(m_nDims);
	};
}; //End building constructor


yggdrasil_Chi2GOFTest_t::yggdrasil_Chi2GOFTest_t(
	const Numeric_t 	sigLevel)
 :
  m_sigLevel(sigLevel),
  m_TTArray(),
  m_testRes(),
  m_nDims(0),
  m_isAssociated(false),
  m_isConsistent(false),	//It is actually undefined, but we define it as inconsistent.
  m_estBins(0),
  m_DOFs(NAN)
{
	if(::std::isnan(m_sigLevel) == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The significance level is nan");
	};
	if(m_sigLevel <= 0.0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The significance level is to small, it was " + std::to_string(m_sigLevel));
	};
	if(1.0 <= m_sigLevel)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The significance level is to large, it was " + std::to_string(m_sigLevel));
	};

	/*
	 * The late bind for the test result will be called in the opre init hook function.
	 */
}; //End building constructor


void
yggdrasil_Chi2GOFTest_t::preInitialHookInit(
	const NodeFacade_t 	nodeToAssociate)
{
	yggdrasil_assert(m_nDims >= 0);
	yggdrasil_assert(m_nDims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(m_sigLevel > 0.0);
	yggdrasil_assert(m_TTArray.empty() == true);
	yggdrasil_assert(::std::isnan(m_DOFs) == true);
	yggdrasil_assert(m_estBins == 0);

	/*
	 * Set up the internal structure
	 */
	if(nodeToAssociate->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The node to associaate is wrong.");
	};
	if(nodeToAssociate.getSampleCollection().isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The collection is not valid.");
	};
	if(nodeToAssociate.isParModelFitted() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The parameteric model is not fully fitted.");
	};
	if(this->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The gof test is allready assocaited.");
	};

	//Make this associated
	m_isAssociated = true;

	/*
	 * Test if the dimension must be lat bound
	 */
	if(m_nDims == 0)
	{
		//Load the dimnension from the node
		m_nDims = nodeToAssociate.nDims();
		yggdrasil_assert(m_nDims > 0);

		//Make a check
		if(m_nDims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimension is too large.");
		};

		//Perform the late binding of the test results
		m_testRes.lateBind(m_nDims);
	}; //End if: the dimension must be estimated


	if(m_nDims == 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension is zero.");
	};

	//This test would actually belong in a else brach, but I think it does not
	//bother anymone if it is here.
	if(nodeToAssociate.nDims() != m_nDims)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The sample collection of the node and the gof test have a different doimensions.");
	};

	/*
	 * Here we register the different tests.
	 *
	 * We will not register all the tests/dimensions,
	 * but only the one that are non trivial.
	 *
	 * We define a test as non trivial, if the
	 * size of the domain is too small.
	 */

	//Get the domain
	const HyperCube_t& domain = nodeToAssociate.getDomain();
	yggdrasil_assert(domain.isValid());
	yggdrasil_assert((Size_t)domain.nDims() == m_nDims);

	for(Size_t i = 0; i != m_nDims; ++i)
	{
		//This is the current domain
		const IntervalBound_t currInterval = domain[i];

		//This is teh length of the current domain
		const auto lengthOfCurrDomain = currInterval.getLength();

		//We we test if teh domain is to small
		if(lengthOfCurrDomain < Constants::YG_SMALLEST_LENGTH)
		{
			//The domain is not small, so we register a trivial test
			m_testRes.addTrivialTestInstance(uInt_t(i));
#if defined(YGGDRASIL_PRINT_LENGTH_ISSUE) && YGGDRASIL_PRINT_LENGTH_ISSUE == 1
			std::cout << " >> Encountered a samll element in the GOF test." << std::endl;
#endif
		}
		else
		{
			//The size is not too small,s o we can register a normal test
			m_testRes.addTestInstance(uInt_t(i));
		};
	}; //End for(i)

	if(m_testRes.isComplete() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The test result is not complete.");
	};


	/*
	 * Determine if the test is trivial.
	 *
	 * This is the case if the number of sampels/bins
	 * so som small that the DOF is zero or smaller.
	 */
	//get the number of samples
	const auto nSample = nodeToAssociate.nSamples();

	//calculate the number of bins
	m_estBins = yggdrasil_chi2_calcNBins(m_sigLevel, false, nSample);
	yggdrasil_assert(m_estBins >= 1);

	//Get the parameteric mode, is needed for the calcualtion of the DOF
	const ParModel_t* model = nodeToAssociate.getParModel();

	//Now calculate the number DOF
	//Technically this can be negative
	yggdrasil_assert(isValidFloat(m_DOFs) == false);
	m_DOFs = m_estBins - 1.0 - model->getBoundDOF();
	yggdrasil_assert(isValidFloat(m_DOFs));

	//If the DOF is greater than zero, we have to
	//allocate the internbal structures
	if(this->isTestTrivial() == false)
	{
		/*
		 * The test is not trivial, also we have to allocate the resource for it.
		 */

		//Reserve space
		m_TTArray.reserve(m_nDims);

		//Create the table and register the dimension
		for(Size_t i = 0; i != m_nDims; ++i)
		{
			//Create the test table
			//False because we are not multivariate
			m_TTArray.emplace_back(m_sigLevel, false);
		}; //End for(i)
	}; //End we have to allocate the internal structures

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("It was not possible to associate the node.");
	};
	yggdrasil_assert(::std::all_of(m_testRes.begin(), m_testRes.end(),
		[](const SingleTestRes_t& s) -> bool {return (s.isTrivialTest() == s.isTestDone());}));

	return;
}; //End: preInitHookInit


yggdrasil_Chi2GOFTest_t::SplitSequence_t
yggdrasil_Chi2GOFTest_t::computeSplitSequence()
 const
{
	yggdrasil_assert(m_nDims > 0);
	yggdrasil_assert(m_nDims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(m_testRes.isComplete());
	yggdrasil_assert(m_sigLevel > 0.0);
	yggdrasil_assert(m_TTArray.empty() == true);
	yggdrasil_assert(::std::isnan(m_DOFs) == false);
	yggdrasil_assert(m_estBins > 0);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The split sequence function was called, but this is not even associated.");
	};
	if(this->allTestsDone() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Not all dimension where tested yet.");
	};
	if(m_testRes.isSorted() == false)
	{
		//This is done in the endOfTestProcess fucntion
		throw YGGDRASIL_EXCEPT_LOGIC("The test results are already sorted.");
	};

	//If the test is trivial then we can imediatly return
	//the empty sequence
	if(this->isTestTrivial() == true)
	{
		return SplitSequence_t();
	}; //End if: test is trivial so return empty

	//Now we will get the pValue and test it is below the significance niveau
	//If not then the empty sequence is returned
	const SingleTestRes_t&  testSmallestPValue = m_testRes.getRank(0);
	const Int_t 		dimToSplit         = testSmallestPValue.key();
	const Numeric_t 	pValue             = testSmallestPValue.pValue();

	if(pValue < m_sigLevel)
	{
		if(this->isTestTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Despite the fact that the tests are trivialy accepted, it seams that one test was rejected.");
		};

		//We have to split
		SplitSequence_t ss = SplitSequence_t().copyAndAddNewLevelOfSplit(dimToSplit);

		return ss;
	}; //End there is something to split

	/*
	 * There is noting to split, so return the empty sequence
	 */
	return SplitSequence_t();
}; //End compute split sequence


bool
yggdrasil_Chi2GOFTest_t::areSplitsToPerform()
 const
{
	yggdrasil_assert(m_nDims > 0);
	yggdrasil_assert(m_nDims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(m_testRes.isComplete());
	yggdrasil_assert(m_sigLevel > 0.0);
	yggdrasil_assert(m_TTArray.empty() == true);
	yggdrasil_assert(::std::isnan(m_DOFs) == false);
	yggdrasil_assert(m_estBins > 0);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The split sequence function was called, but this is not even associated.");
	};
	if(this->allTestsDone() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Not all dimension where tested yet.");
	};
	if(m_testRes.isSorted() == false)
	{
		//This is done in the endOfTestProcess fucntion
		throw YGGDRASIL_EXCEPT_LOGIC("The test results are already sorted.");
	};

	//We can make a small bypass if we are on an empty domain
	//because tehre are no splits to be done
	if(this->isTestTrivial() == true)
	{
		//We are on an empty domain
		yggdrasil_assert(m_TTArray.empty() == true);

		return false;
	};

	//Now we will get the pValue and test it is below the significance niveau
	//If not then the empty sequence is returned
	const auto&     testSmallestPValue = m_testRes.getRank(0);
	const Numeric_t pValue             = testSmallestPValue.pValue();

	if(pValue < m_sigLevel)
	{
		return true;
	}; //End there is something to split

	/*
	 * There is noting to split, so return the empty sequence
	 */
	return false;
}; //End: are there splits


bool
yggdrasil_Chi2GOFTest_t::areSplitsStable()
{
	throw YGGDRASIL_EXCEPT_illMethod("Not implemented");
};


bool
yggdrasil_Chi2GOFTest_t::performInitailTest(
	cParModel_ptr 	parModel,
	NodeFacade_t 	leaf,
	const Int_t 	d)
{
	yggdrasil_assert(m_nDims > 0);
	yggdrasil_assert(m_nDims < Constants::YG_MAX_DIMENSIONS);
	yggdrasil_assert(m_testRes.isComplete());
	yggdrasil_assert(m_sigLevel > 0.0);
	yggdrasil_assert(this->isTestTrivial() == false ? m_TTArray.size() == m_nDims : m_TTArray.empty());
	yggdrasil_assert(d >= 0);
	yggdrasil_assert((Size_t)d < m_nDims);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Called the initail fitting fiunction on a not associated gof test.");
	};
	if(this->isDimensionTested(d) == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension " + std::to_string(d) + " is already tested.");
	};

	if(leaf->isLeaf() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The leaf is not a leaf.");
	};
	if(leaf->isOKNode() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Can not fit on an invalid leaf");
	};
	if(m_testRes.isComplete() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test result is not complete.");
	};
	if(leaf.isDirty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The leaf is dirty for the initial fitting.");
	};

	yggdrasil_assert(m_testRes.isSpecificTestDone(d) == false);

	const IntervalBound_t domain   = leaf->getDomain().at(d);
	yggdrasil_assert(domain.isValid() == true);

	/*
	 * Case:
	 * 	Is test trivial
	 */
	if(this->isTestTrivial() == true)
	{
		yggdrasil_assert(this->getDOF() <= 0.0);

		// In this case we habe not allocated the space for the test table
		yggdrasil_assert(m_TTArray.empty() == true);

		/*
		 * In the case of zero samples we accept the
		 * test by default. This is done by useing
		 * a very high p value
		 */
		const Numeric_t pValue = 1.0001;

		//This is the size of the interval, used as tie breaker
		const Numeric_t sizePart = domain.getLength();

		//Store it
		yggdrasil_assert(m_testRes.isSpecificTestDone(d) == false);
		m_testRes.storeTestResult(d, pValue, sizePart);

		//Return since we have done what we have to
		return m_testRes.isSpecificTestDone(d);
	}; //End if: handle the case of zero sample


	/*
	 * Case:
	 * 	Many sample
	 */
	yggdrasil_assert(::std::isnan(m_DOFs) == false);
	yggdrasil_assert(m_DOFs > 0.0);

	//These are the samples that need testing
	const DimArray_t&     dimArray = leaf->getSampleCollection().getCDim(d);

	//Get the number of samples inside the array
	const Size_t nSamples = dimArray.nSamples();

	//We have allocated sapce for the test table in that case
	yggdrasil_assert(m_TTArray.size() == m_nDims);

	//This is the test table
	Chi2TestTable_t& thisTestTable = m_TTArray.at(d);

	//Make a test if the table is already associated
	if(thisTestTable.isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test table of dimension " + std::to_string(d) + " is already allocated.");
	};
	yggdrasil_assert(m_estBins == yggdrasil_chi2_calcNBins(m_sigLevel, false, nSamples));


	//Calculate the bins (positions) that we need
	yggdrasil_assert(m_estBins > 0);
	const Chi2TestTable_t::BinEdgesArray_t thisBins = parModel->generateBins(d, m_estBins, domain);
	yggdrasil_assert(m_estBins == thisBins.nBins());

	/*
	 * Perform the actual binning
	 */

	//Create the test table
	const bool succ = thisTestTable.internal_buildInitialChi2TableDirect(
			dimArray,
			thisBins,
			true	//We use the storting distribnution algorithm
			);

	if(succ == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The creation of the tablke of diomension " + std::to_string(d) + " failed.");
	};
	yggdrasil_assert(thisTestTable.isAssociated() == true);



	/*
	 * Perform the statistical test
	 */

	//Now calculate the expected counts
	ParModel_t::ValueArray_t expectedBinCounts = parModel->getExpectedCountsInBins(d, thisBins, domain);

#ifndef NDEBUG
	/*
	 * This test tests if the expected number of bins is rougthly equal the total
	 * nuber of bins
	 *
	 * Note the test could fail even if everything is correct the reason is
	 * that numerical cancelation can occure
	 */
	{
		Real_t totExpectedCouts = 0.0;
		for(const auto binCount : expectedBinCounts)
		{
			yggdrasil_assert(binCount >= 0.0);
			totExpectedCouts += binCount;
		}; //ENd for(binCount)

		const Real_t factor = std::abs(1.0 - totExpectedCouts / parModel->nSampleBase());
		yggdrasil_assert(factor < 0.01); //We allow an error of 1%
	}; //ENd: test scope
#endif

	//Calculate the test statistic
	::yggdrasil::Real_t Chi2 = thisTestTable.computeChi2Value(expectedBinCounts);
	yggdrasil_assert(Chi2 >= 0.0);

	if(::std::isnan(Chi2) == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The test statitsic has become NaN");
	}; //End if: Chi2 has turned nan

	//this is the pvalue variable
	Numeric_t pValue;
	yggdrasil_assert(this->getDOF() > 0.0);

	//Boost has problems with infinity value of CHI2, so we will patch it
	if(std::isinf(Chi2) == false)
	{
		//The value is not infinity, so we can compute it
		::boost::math::chi_squared_distribution<Numeric_t> chi2Dist(this->getDOF() );
		pValue = ::boost::math::cdf(::boost::math::complement(chi2Dist, Chi2));
	}
	else
	{
		//We have infinity, the pValue is not 1.0 but zero, since
		pValue = 0.0;
	};

	//
	//Now save the result

	//get the size
	const Numeric_t sizePart = domain.getLength();

	//Store it
	m_testRes.storeTestResult(d, pValue, sizePart);

	//
	//Return value
	//This inmplicitly test that the test succeded, becaus eyggdrasil tests it.
	return m_testRes.isSpecificTestDone(d);
}; //End: perform initial fitting


bool
yggdrasil_Chi2GOFTest_t::isDimensionTested(
	const Int_t 	d)
 const
{
	//We test if we are associated
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test is not associated.");
	};
	yggdrasil_assert(m_testRes.isComplete() == true);

	return m_testRes.isSpecificTestDone(d);
}; //End: isDimensionTested

bool
yggdrasil_Chi2GOFTest_t::allTestsDone()
 const
{
	//We test if we are associated
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test is not associated.");
	};
	yggdrasil_assert(m_testRes.isComplete() == true);

	return m_testRes.allTestsDone();
}; //End: allTestDone


bool
yggdrasil_Chi2GOFTest_t::isAssociated()
 const
{
	if(m_isAssociated == true)
	{
		yggdrasil_assert(m_nDims > 0);
		yggdrasil_assert(m_nDims < Constants::YG_MAX_DIMENSIONS);
		yggdrasil_assert(m_testRes.isComplete());
		yggdrasil_assert(m_estBins > 0);
		yggdrasil_assert(m_sigLevel > 0.0);

		/*
		 * It is not possible to test if the size of the areray
		 * is correct. because it could be allready delaocated.
		 * This happens in the end of test function.
		 */

		return true;
	}; //End if: true case

	return false;
}; //End: isAssociated



bool
yggdrasil_Chi2GOFTest_t::isConsistent()
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("By definition a test that is not associated can not be consistent.");
	};

	return m_isConsistent;
}; //End is COnsistent


void
yggdrasil_Chi2GOFTest_t::addNewSample(
	const Sample_t&		nSample,
	const HyperCube_t&	domain)
{
	//Make *this inconsistent; This is only a reminder
	m_isConsistent = false;


	//THis fucntiojnality is not yet implemented
	throw YGGDRASIL_EXCEPT_illMethod("Not implemented");
	(void)nSample;
	(void)domain;
};


bool
yggdrasil_Chi2GOFTest_t::commitChanges(
		cParModel_ptr 	parModel,
		NodeFacade_t 	subTreeRoot,
		const Int_t 	d)
{
	throw YGGDRASIL_EXCEPT_illMethod("Not implemented");
	(void)parModel;
	(void)subTreeRoot;
	(void)d;
};








yggdrasil_Chi2GOFTest_t::~yggdrasil_Chi2GOFTest_t()
 noexcept
 = default;



yggdrasil_Chi2GOFTest_t::yggdrasil_Chi2GOFTest_t(
	const yggdrasil_Chi2GOFTest_t&)
 = default;


yggdrasil_Chi2GOFTest_t::yggdrasil_Chi2GOFTest_t(
	yggdrasil_Chi2GOFTest_t&&)
 noexcept
 = default;


yggdrasil_Chi2GOFTest_t&
yggdrasil_Chi2GOFTest_t::operator= (
	const yggdrasil_Chi2GOFTest_t&)
 = default;

yggdrasil_Chi2GOFTest_t&
yggdrasil_Chi2GOFTest_t::operator= (
	yggdrasil_Chi2GOFTest_t&&)
 noexcept
 = default;



Size_t
yggdrasil_Chi2GOFTest_t::getEstimatedNBins()
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test is not associated.");
	};
	yggdrasil_assert(m_estBins != 0);

	return m_estBins;
}; //End: getEstimatedNBins


Numeric_t
yggdrasil_Chi2GOFTest_t::getDOF()
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The test is not associated.");
	};
	yggdrasil_assert(::std::isnan(m_DOFs) == false);

	return m_DOFs;
}; //End getDOF


bool
yggdrasil_Chi2GOFTest_t::isTestTrivial()
 const
{
	yggdrasil_assert(::std::isnan(m_DOFs) == false);

	return (m_DOFs <= 0.0) ? true : false;
}; //ENd: is test trivial





YGGDRASIL_NS_END(yggdrasil)
