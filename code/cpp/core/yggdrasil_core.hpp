#pragma once
#ifndef YGGDRASIL_CORE__YGGDRASIL_CORE_HPP
#define YGGDRASIL_CORE__YGGDRASIL_CORE_HPP
/*
 * \brief	This file contains some of the core functionality
 */

/**
 * \def YGGDRASIL_START_NS
 * \brief this macro declares a namespace with the given name
 */
#define YGGDRASIL_NS_START(NAME) namespace NAME {
#define YGGDRASIL_NS_BEGIN(NAME) namespace NAME {
#define YGGDRASIL_NS_END(NAME) }



/**
 * \def YGGDRASIL_LIKELY
 * \def YGGDRASIL_UNLIKELY
 * \brief give some hints to the branch prediction
 *
 * This macros emit some instruction to the branch prediction.
 * Use with care, since it can cost a lot if they are placed wrong.
 */
#define YGGDRASIL_LIKELY(x)       __builtin_expect(!!(x),1)
#define YGGDRASIL_UNLIKELY(x)     __builtin_expect(!!(x),0)


/**
 * \def	YGGDRASIL_NOEXCEPT_IF( expr )
 *
 * \brief 	This macro is for the case, if one whjant to test, if an expresion throws.
 * 		It is intended for the declaration directly.
 */
#define YGGDRASIL_NOEXCEPT_IF(cond) noexcept(noexcept(cond))



#endif //End include guard
