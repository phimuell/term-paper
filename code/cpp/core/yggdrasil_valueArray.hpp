#pragma once
/**
 * \brief	This file contains a class that represents some
 * 		 numbers that are not designated to a particular purpose.
 *
 * Some of the classes inside yggdrasils are mere wrapper arround a vector.
 * We have done this to enforce a typesystem.
 * This class is now an exception, it is intended to be used as a general type
 * that does not reflect a particular purpose.
 * Classes should be constructable from this, but this should not be done
 * in an implicit way.
 */

//Includeing yggndrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>

//Including std
#include <vector>


YGGDRASIL_NS_START(yggdrasil)


/**
 * \using	yggdrasil_valuearray_t
 * \brief	This is the general vector like type inside yggdrasil.
 *
 * It can be for example be used as a result of the slicing operation.
 * Or it is the result of a transform operation.
 * It is guaranteed to be a standard conforming vector,
 * but must not be the one from teh standard.
 * Also it is guaraneed to represent continues memory.
 *
 * \tparam  T		The type that is extracted
 * \tparam  Allocator	The type of the allocator
 */
template<
	class 	T,
	class 	Allocator = ::std::allocator<T>
	>
using yggdrasil_valueArray_t = ::std::vector<T, Allocator>;



YGGDRASIL_NS_END(yggdrasil)




