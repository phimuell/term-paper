#pragma once
/**
 * \brief	This file defines some typedefs that are used inside yggdrasil when dealing with eigen.
 * 		 It also includes the Eigen Core header.
 *
 * It is based mainly on the pybind documentation.
 */

//Include Yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_int.hpp>

//Include Eigen
#include <Eigen/Core>


YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \brief	This is the general stride that allows Eigen to use non continues memory.
 */
using yggdrasil_eigenStride_t = Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>;

/**
 * \brief	This is the type for a general reference type.
 *
 * It allows to bind to non continues memeory.
 * On the pybind11 documentation there was a zero as second template argument of
 * Ref, this was removed and substituted by the Unaligned enum.
 *
 * \tparam  MatrixType 		The type of the matrix that should be bounded
 */
template<
	class 	MatrixType
>
using yggdrasil_eigenRef_t = ::Eigen::Ref<MatrixType, Eigen::Unaligned, yggdrasil_eigenStride_t>;


/**
 * \brief	This is an pybind11 specific eigen Map object.
 *
 * It can be used to encode raw memeory as eigen type.
 *
 * \tparam  MatrixType 		What is the Eigen Type that should be mapped.
 */
template<
	class 	MatrixType
>
using yggdrasil_eigenMap_t= ::Eigen::Map<MatrixType, Eigen::Unaligned, yggdrasil_eigenStride_t>;



/**
 * \brief	This is the type of a column matrix of Eigen.
 *
 * It is already set to the numeric type.
 * It is redeclared to comply with the recomendation of pybind11.
 */
using yggdrasil_eigenColMatrix_t = ::Eigen::Matrix<
					::yggdrasil::Numeric_t,			//Scalar
					::Eigen::Dynamic,			//Rows
					::Eigen::Dynamic,			//Cols
					::Eigen::ColMajor | ::Eigen::AutoAlign	//Options
				>;

/**
 * \brief	This is the type of a row matrix of Eigen.
 *
 * It is already set to the numeric type.
 * It is redeclared to comply with the recomendation of pybind11.
 */
using yggdrasil_eigenRowMatrix_t = ::Eigen::Matrix<
					::yggdrasil::Numeric_t,			//Scalar
					::Eigen::Dynamic,			//Rows
					::Eigen::Dynamic,			//Cols
					::Eigen::RowMajor | ::Eigen::AutoAlign	//Options
				>;

/**
 * \brief	This is the type of a vector of Eigen.
 *
 * It is already set to the numeric type.
 * This technically it does not make any different, but it is typed as a row vector.
 * It is redeclared to comply with the recomendation of pybind11.
 */
using yggdrasil_eigenRowVector_t = ::Eigen::Matrix<
					::yggdrasil::Numeric_t,			//Scalar
					1,					//Rows (Statically set to 1)
					::Eigen::Dynamic,			//Cols
					::Eigen::RowMajor | ::Eigen::AutoAlign	//Options
				>;

YGGDRASIL_NS_END(yggdrasil)





