/**
 * \brief 	Implements some functions for the exception class
 */


#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>


//Include std
#include <exception>
#include <string>
#include <sstream>
#include <iostream>

YGGDRASIL_NS_START(yggdrasil)


void
yggdrasil_exception::prot_composeMessage()
{
	try
	{
		std::stringstream s;

		//Make a nice header
		s << "\n=====================================\n";

		/*
		* Make an introduction
		*/
		s << "A "; 	//I konw maybe there is an "n" but I don't care
		if(m_type.empty() == false)
		{
			s << m_type << " ";
		};
		s << "Exception occured:\n";

		// Print out the usefull stuff
		s << "Function: " << m_func << "\n";
		s << "File:     " << m_file << "\n";
		s << "Line:     " << m_line << "\n";

		//Print out the message
		if(m_message.empty() == false)
		{
			s << "Message:\n" << m_message << "\n";
		};

		//Footer
		s << "=====================================\n";

		m_outputStorage = s.str();

		volatile const char* t = m_outputStorage.c_str(); //This is for a test maybe the string does something, so we know that it works, I hope.
		(void)t;
	}
	catch(...)
	{
		//Something bad happened
		std::cerr << "Exception during the exception. Abort." << "\n";
		std::cerr.flush();

		std::terminate();
	};

	return;
};


const char*
yggdrasil_exception::what() const noexcept
{
		return m_outputStorage.c_str();
};





YGGDRASIL_NS_END(yggdrasil)




