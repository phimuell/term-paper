#pragma once
/**
 * \brief	This file implements the value pdf array.
 */

//Includeing yggndrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_valueArray.hpp>

//Including std
#include <memory>


YGGDRASIL_NS_START(yggdrasil)


/**
 * \brief	This name represents the object that is used to store
 * 		 PDF values in an array/vector.
 *
 * This object should be used if the user want to represent many pdf
 * values at the same time
 *
 * This class/using comes from the python interface.
 * I think that it will be transformed into a class.
 *
 *
 * \tparam  T		The type that is extracted
 * \tparam  Allocator	The type of the allocator
 */
template<
	class 	T,
	class 	Allocator = ::std::allocator<T>
	>
using yggdrasil_PDFValueArray_t = yggdrasil_valueArray_t<T, Allocator>;



YGGDRASIL_NS_END(yggdrasil)




