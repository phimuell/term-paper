#pragma once
/**
 * \brief	This file contains a class that can do some form of slicing
 *
 * One could also do something with meta programing, but this is an overkill.
 * These classes have some asspect of a std::vector.
 */

//Includeing yggndrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>

//We include the value array, since they are copled
#include <core/yggdrasil_valueArray.hpp>

//Including std
#include <vector>


YGGDRASIL_NS_START(yggdrasil)

/**
 * \using 	yggdrasil_indexArray_t
 * \brief	This class is represents a slice or many index at once.
 *
 * The intentions is that this can be passed to a container and the extraction
 * can then be performed in one step, instead of many.
 *
 * This class is basically a reduced vector.
 * However unlike other classes in yggdrasil it guarantees continuus memory.
 *
 * \note 	Currently the class is implemented by a typedef of a vector.
 * 		It is not guaranteed that this is always the case.
 */
using yggdrasil_indexArray_t = ::std::vector<Size_t>;


YGGDRASIL_NS_END(yggdrasil)




