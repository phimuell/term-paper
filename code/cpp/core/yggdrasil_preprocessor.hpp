#pragma once
#ifndef YGGDRASIL_CORE__YGGDRASIL_PREPROCESOR
#define YGGDRASIL_CORE__YGGDRASIL_PREPROCESOR
/*
 * \brief	This file defines some preprocessor functions that are usefull.
 */


/**
 * \def YGGDRASIL_PP_CONCATENATE
 * \param first 	The first token
 * \param second	The second token
 *
 * \brief concatenates the two tokens together.
 */
#define YGGDRASIL_PP_CONCATENATE(First, Second) First ## Second





#endif //End include guard
