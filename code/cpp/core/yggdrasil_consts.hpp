#pragma once
/**
 * \brief	This file contains constanst that are used throughout yggdrasil
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_int.hpp>

//Include std
#include <limits>
#include <cmath>

YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \namespace	Constants
 * \brief	In this namespace we define the constants
 *
 * Some of them are static but some are also extern
 */
YGGDRASIL_NS_BEGIN(Constants)

/**
 * \brief	This is the numeric value to identify the invalid dimenion
 */
const static ::yggdrasil::Int_t YG_INVALID_DIMENSION = Int_t(-1);


/**
 * \brief	This is the maximum number of dimensions that are supported
 *
 * When using this, please remember that we start counting at zero.
 * This means a DIMENSION INDEX of 255 also names an ilegal dimension
 */
const static ::yggdrasil::Int_t YG_MAX_DIMENSIONS = 255;

/**
 * \brief	This is the machine epsilon
 */
const static ::yggdrasil::Numeric_t EPSILON = ::std::numeric_limits<::yggdrasil::Numeric_t>::epsilon() / 1000;


/**
 * \brief	This is the constant that describes the final size of an ellement.
 *
 * If we have delet like distributions, it will happen that we will
 * splitt the domain to smaller and smaller pieces.
 * We rescale the sample, but we still have to maintain the
 * size of each domain, in rescalled data space.
 * Thus we will finaly reach a level where the sizes are too
 * small, and an error will be generated.
 *
 * This value is now the end length.
 * If a dimension of a domain is below this value, but above
 * the invalid level, we will stop splitting and
 * consider all tests, involving that dimension as passed.
 *
 * The value we have used is approximatly, 10^-11, in rescalled data space.
 *
 *
 * \note	This is different to the R-Code, which does not neede that.
 * 		 The reason for that is that the R-Code first applies unique,
 * 		 when computing the number of bins, this means that we have essentially
 * 		 only one sample and thus one bin. So all samples lies in one bin and the
 * 		 test is accepted.
 */
#ifdef YGGDRASIL_NO_SIZE_RESTRICTION
const static ::yggdrasil::Numeric_t YG_SMALLEST_LENGTH = 0.0;
#else
const static ::yggdrasil::Numeric_t YG_SMALLEST_LENGTH = 1000 * EPSILON;
#endif


/**
 * \brief	This is the number of cuts we allow in one split operation
 *
 * This limit is imposed to limit the exponential groth of the tree
 * during the construction process.
 * This
 */
const static ::yggdrasil::Size_t MAXIMAL_SPLIT_SEQUENCE_LENGTH = 2;


/**
 * \brief	Technical limits of the split depth.
 *
 * This is a limit that is imposed by the implementation.
 * It is the maximum length of a split sequence the
 * the implementation can handle.
 *
 * The value of 30 should be adequate, since it
 * will result in \f$ 2^{30} \f$ new subdomains.
 */
const static ::yggdrasil::Size_t MAX_SPLIT_DEPTH = 30;


/**
 * \brief	This is the size that the rescalling code uses as working set.
 *
 * This value, memory usage in KILO BYTE, is essentially the value
 * of how many samples are sescalled at once.
 * This affects more the sorting, since it determine, how
 * many samples are sorted at once.
 *
 * It is recomended to set this value to something that is
 * in the order of the L3 cahce.
 *
 * If all samples at once should be processed then set it to zero.
 *
 * The default is 7MB
 */
const static ::yggdrasil::Size_t YG_RESCALLING_WS_KB = 5 * 1024;


/**
 * \brief	This is the size of the temporary working array
 * 		 in the test tsblae.
 *
 * Each call to trequency table, which is used inside the
 * GOF tester (CHI2 version) will allocate an array of
 * type SortIndex_t when using the sorting scheme.
 *
 * You can deisable this by setting the value to zero.
 *
 * Notice that the value is in KILO BYTE, and one samples
 * occupies eight bytes.
 * Also notice that each, if multi threading is enabled
 * is allowed to allocate such a table.
 * Also note that there is also the dimension array that is
 * used.
 *
 * Use the L3 cache size of our machine to determine this
 * value, the default assumes a value of about 8MB,
 */
const static ::yggdrasil::Size_t TEMP_ARRAY_TT_KB = 4096;


/**
 * \brief	This is the size of the temporary array that is
 * 		 created in the frequency table.
 *
 * Notice that two arrays will be created in a single test.
 * But their combined size will not exceed this value.
 *
 * The value is in KILO BYTE.
 * It should also be used for the L3 cache.
 *
 * Notice that each thread that execute a independence test,
 * will allocate its helper array.
 *
 * The sorting will we disable. if set to zero
 */
const static ::yggdrasil::Size_t TEMP_ARRAY_FT_KB = 4096;


/**
 * \brief	This is the size, in MEGABYTE, a single instance
 * 		 of the independence test can hold as temporary.
 *
 * This is needed for the partition to speed up the independence test.
 *
 * For the other two numeric constants one can pretty good explain
 * why the optimal value should be at arround half the L3 cache.
 * However for this constant there is no way to tell it.
 *
 * Technically it should be determined experimently.
 * The value I put, has no real meaning.
 */
const static ::yggdrasil::Size_t TEMP_MEMORY_INDEPTEST_MB = 512;




YGGDRASIL_NS_END(Constants)


/**
 * \brief	This is a fucntion that tests if two numbers are
 * 		 Aprroximatly the same.
 *
 * It test if the absolute difference is smaller than a certain
 * multiply of the Maschine epsilon.
 *
 * \param  a	The first number.
 * \param  b	The second number.
 * \param  N	The factor of the epsilon.
 */
template<
	class A_t,
	class B_t
	>
inline
bool
yggdrasil_approxTheSame(
	const A_t&	a,
	const B_t&	b,
	const Size_t 	N = 100)
{
	return (::std::abs(a - b) < (N * ::yggdrasil::Constants::EPSILON)) ? true : false;
}; //End: approx the same


YGGDRASIL_NS_END(yggdrasil)



