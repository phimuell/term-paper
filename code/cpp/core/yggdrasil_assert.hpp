#pragma once
#ifndef YGGDRASIL_CORE__YGGDRASIL_ASSERT_HPP
#define YGGDRASIL_CORE__YGGDRASIL_ASSERT_HPP
/**
 *  \brief	This file defines our assert
 *  \author	Stefano Weidmann (stefanow@ethz.ch)
 *
 *  In order to work properly, this file sould be included as the first one.
 *
 *  This implementation was originally written by Stefano Weidmann (stefanow@ethz.ch).
 *  It was then addapted by Philip Müller (phimuell@ethz.ch) and was incooperated into pgl.
 *  Finally it made its way inside Yggdrasil
 */

// Include the corefile
#include <core/yggdrasil_core.hpp>


//Some C stuff
#include <cassert>
#include <cstdlib>

#include <iostream>

//Boost for the preprocessor
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/stringize.hpp>



/*
 * This is the custom Assert we used
 */
#if __cplusplus < 201103L
#error "yggdrasil_assert needs at least C++11!"
#endif


#define myAssertImplementation__(r, name, elem)	 \
if (!(elem)) { \
	std::cerr << "\n" << (name) << " FAILED\n--------------------------------------------------------" \
	<< "\nfile: " << __FILE__ \
	<< "\nfunction: " << __func__ \
	<< "\nline: " << __LINE__ \
	/*<< "\nthread: " << getMyThreadNumber()*/ \
	<< "\ncondition which failed: " << BOOST_PP_STRINGIZE(elem) \
	<< "\n"; \
	std::cerr.flush(); \
	std::abort(); \
}
// the do {...} while(false) is here to enable the use of this macro inside a block
// see http://www.bruceblinn.com/linuxinfo/DoWhile.html
#define myAssertCheck__FORCE(name, seq) \
do { \
	BOOST_PP_SEQ_FOR_EACH(myAssertImplementation__, name, seq); \
} while(false)

#ifndef NDEBUG

	#pragma message "NDEBUG not set, assertions are enabled"
	#define myAssertCheck__(name, seq) myAssertCheck__FORCE(name, seq)

#else

	#pragma message "NDEBUG set, assertions are disabled"
	#define myAssertCheck__(name, seq)
#endif

//DEfine some macros that gives us syntactic sugar
#define yggdrasil_invar(...) myAssertCheck__("INVARIANT", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))
#define yggdrasil_precond(...) myAssertCheck__("PRECONDITION", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))
#define yggdrasil_postcond(...) myAssertCheck__("POSTCONDITION", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))
#define yggdrasil_require(...) precond(__VA_ARGS__)
#define yggdrasil_ensure(...) postcond(__VA_ARGS__)

#ifdef yggdrasil_assert
	#undef yggdrasil_assert
#endif

//Define the macro yggdrasil_assert
#define yggdrasil_assert(...) myAssertCheck__("ASSERTION", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))


//If the assert is not disabled, overwrite it with the superior yggdrasil_assert
#ifndef NDEBUG
	#undef assert
	#define assert(...) myAssertCheck__FORCE( "ASSERTION", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))
#endif



//
#define yggdrasil_implies(When, Then) ((!(When)) || (Then))





#endif //End includeguard

