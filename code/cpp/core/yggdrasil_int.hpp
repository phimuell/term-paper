#pragma once
/**
 * \brief This file declares integer that are used through out the library
 *
 * The declarations are similar to the one in the cstdint header.
 * The header is similar to cstdint, but it also declares some typedefs for floating points.
 */

#include <core/yggdrasil_core.hpp>

#include <cstdint>
#include <cmath>

namespace yggdrasil
{

	/*
	 * Integer declarations
	 */

	using Int_t 		= int;
	using uInt_t		= unsigned int;

	using Int8_t 		= std::int8_t;
	using uInt8_t 		= std::uint8_t;

	using Int16_t		= std::uint16_t;
	using uInt16_t 		= std::uint16_t;

	using Int32_t		= std::int32_t;
	using uInt32_t		= std::uint32_t;

	using Int64_t 		= std::int64_t;
	using uInt64_t		= std::uint64_t;

	using Size_t		= std::uintmax_t;
	using sSize_t		= std::intmax_t;


	using Numeric_t 	= double;
	using Real_t		= long double;
	using Float_t		= float;

	/**
	 * \breif	This is a type that is uded for sorting inside
	 * 		 the table classes.
	 *
	 * This is something to preserve some memory.
	 * It is defined here to be able to change it
	 * globaly in a very easy mannor.
	 */
	using SortIndex_t 	= ::yggdrasil::uInt32_t;


	/**
	 * \brief	This function is able to test if a float is valid in some sense.
	 *
	 * Since we will have most likely numerical problem, we define a valid number
	 * as not NAN nor INFINITY.
	 *
	 * This impplies that also subnormal floats are valid.
	 *
	 * \param  arg		The float to test.
	 */
	template<class FP_t>
	bool
	isValidFloat(
		const FP_t&	arg)
	{
		switch(std::fpclassify(arg))
		{
		  case FP_INFINITE:  return false;
		  case FP_NAN:       return false;
		  case FP_NORMAL:    return true;
		  case FP_SUBNORMAL: return true;
		  case FP_ZERO:      return true;
		  default:           return false;	// We defione it taht way
    		}; //End switch

    		//We will never be here

	}; //End: isValidFLoat


}; //End namespace(yggdrasil)



