#pragma once
#ifndef YGGDRASIL_CORE__YGGDRASIL_EXCEPTION_HPP
#define YGGDRASIL_CORE__YGGDRASIL_EXCEPTION_HPP
/**
 * \brief 	defines the yggdrasil exception
 *
 * The yggdrasil exceptions are similar to the exception provided by the standard, they are named the same, expet with the yggdrasil
 * prefix. They inherent form std::exception.
 * They provide more information, but required more input argument, to ease their useage, use rthe provided macros.
 */

//Include yggdrasil
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_int.hpp>


//Include std
#include <exception>
#include <string>


YGGDRASIL_NS_START(yggdrasil)

/**
 * \brief 	Base class o yggdrasil exceptions
 *
 * This class is the base of yggdrasil exceptions.
 * It stores some more informations than the normal exception does.
 * Compare its output to an assert.
 */
class yggdrasil_exception : public std::exception
{
	/*
	 * Some typedefs
	 */
public:
	using String_t = std::string; //!< The used string type

	/**
	 * \brief constructiors of the exception
	 *
	 * The default constructor is forbidden, but the others are defaulted.
	 * Destructor is virtual
	 */

	// Default constructor is forbidden
	inline
	yggdrasil_exception() = delete;

	// Defaulted copy and move constructors
	inline
	yggdrasil_exception(
		const yggdrasil_exception&) = default;

	inline
	yggdrasil_exception(
		yggdrasil_exception&&) = default;


	//Also defaulted assignments
	inline
	yggdrasil_exception&
	operator= (
		const yggdrasil_exception&) = default;

	inline
	yggdrasil_exception&
	operator= (
		yggdrasil_exception&&) = default;


	virtual
	~yggdrasil_exception() = default;


	/**
	 * \brief constructor to properly create a yggdrasil_exception
	 *
	 * It takes different argument, the last one is for the type, this one is defaulted
	 */
public:
	inline
	yggdrasil_exception(
		const String_t&  message,
		const String_t&  func,
		const String_t&  file,
		const int 	 line,
		const String_t&  type = String_t()):
	  std::exception(),
	  m_message(message),
	  m_func(func),
	  m_file(file),
	  m_type(type),
	  m_line(line),
	  m_outputStorage()
	{
		this->prot_composeMessage();

	};

	/**
	 * \brief 	This function formats the message and stors it in the m_outputStorage
	 */
protected:
	void
	prot_composeMessage();


	/**
	 * This is the what function.
	 * it allows to print out some usefull informations
	 */
public:
	virtual
	const char* what() const noexcept override;

protected:
	String_t 	m_message;	//!< Message to store some usefull error notice
	String_t 	m_func;		//!< Name of the function that trows the exception
	String_t 	m_file;		//!< File where the exception was generated
	String_t 	m_type;		//!< A type of the exeption can be empty
	int 		m_line;		//!< The linenumber where the exception was thrown
	String_t 	m_outputStorage;	//!< A string to store the output, Why not return a string
};

/*
 * This macro defines honor the rule of 5
 */
#define YGGDRASIL_CORE_EXCEPT_INTERNAL_RULE_OF_FIVE(Klasse) 		\
	Klasse ( const Klasse &) = default; 			\
	Klasse ( Klasse && ) = default;				\
	Klasse () = default;					\
	Klasse & operator= (const Klasse &) = default;		\
	Klasse & operator= ( Klasse && ) = default;		\
	virtual ~ Klasse () = default;



/**
 * \breif	Runtime Exeption Class
 *
 * This class is the base for all runtime exception.
 * Runtime exceptions are exception that occured during runtime, and that are (probably) not related to programing error.
 * The perfect program should throw them. They are for situation where memory allocation fails, a file is not there or something like them.
 */
class yggdrasil_runtime_error : public yggdrasil::yggdrasil_exception
{
public:
	YGGDRASIL_CORE_EXCEPT_INTERNAL_RULE_OF_FIVE( yggdrasil_runtime_error)

public:
	inline
	yggdrasil_runtime_error(
		const String_t&  message,
		const String_t&  func,
		const String_t&  file,
		const int 	 line,
		const String_t&  type = String_t("Runtime")):
	  yggdrasil_exception(message, func, file, line, type)
	{};
};


/**
 * \breif	Logic Exeption Class
 *
 * This class is the base for all logical errors that can arire.
 * A logical error is an error that has to do with errors that are not runtime induced.
 * Like passing to few argument to a function ore an out of range error.
 */
class yggdrasil_logic_error : public yggdrasil::yggdrasil_exception
{
public:
	YGGDRASIL_CORE_EXCEPT_INTERNAL_RULE_OF_FIVE( yggdrasil_logic_error)

public:
	inline
	yggdrasil_logic_error(
		const String_t&  message,
		const String_t&  func,
		const String_t&  file,
		const int 	 line,
		const String_t&  type = String_t("Logic")):
	  yggdrasil_exception(message, func, file, line, type)
	{};
};





// ==============================================
// ==============================================
//
// MACRO MAGIC

/*
 * This macro writes an exception class, that inherents from a given base.
 */
#define YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION(ClassName, BaseClass, TYPE) class ClassName : public BaseClass { 	\
	public:												  	\
	YGGDRASIL_CORE_EXCEPT_INTERNAL_RULE_OF_FIVE( ClassName )							\
	public:													\
	inline													\
	ClassName (												\
		const String_t&  message,									\
		const String_t&  func,										\
		const String_t&  file,										\
		const int 	 line,										\
		const String_t&  type = String_t( TYPE )):							\
	  BaseClass (message, func, file, line, type)								\
	{};													\
}


// Make some runtime based errors
YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION(yggdrasil_range_error, yggdrasil_runtime_error, "Range");
YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION(yggdrasil_underflow_error, yggdrasil_runtime_error, "Underflow");
YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION(yggdrasil_overflow_error, yggdrasil_runtime_error, "Overflow");
YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION(yggdrasil_alloc_error, yggdrasil_runtime_error, "Allocation");
YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION(yggdrasil_nullptr_error, yggdrasil_runtime_error, "NullPtr Access");
YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION(yggdrasil_time_error, yggdrasil_runtime_error, "Error in Time Functions");


// Make some logic errors
YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION(yggdrasil_invalid_argument, yggdrasil_logic_error, "Invalid Argument");
YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION(yggdrasil_length_error, yggdrasil_logic_error, "Length");
YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION(yggdrasil_out_of_range_error, yggdrasil_logic_error, "Out of Bound");
YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION(yggdrasil_illegal_method_call_error, yggdrasil_logic_error, "Not Implemented Method");


#undef YGGDRASIL_CORE_EXCEPT_CREATE_EXCEPTION


/*
 * Make some macros that are used to generate clean exceptions, without much hassel
 */

#define YGGDRASIL_EXCEPT_RUNTIME(MSG) 	::yggdrasil::yggdrasil_runtime_error( MSG, __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define YGGDRASIL_EXCEPT_ALLOC(MSG)   	::yggdrasil::yggdrasil_alloc_error( MSG, __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define YGGDRASIL_EXCEPT_NULL(MSG)   		::yggdrasil::yggdrasil_nullptr_error( MSG, __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define YGGDRASIL_EXCEPT_TIME(MSG)   		::yggdrasil::yggdrasil_time_error( MSG, __PRETTY_FUNCTION__, __FILE__, __LINE__)

#define YGGDRASIL_EXCEPT_LOGIC(MSG)		::yggdrasil::yggdrasil_logic_error( MSG,  __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define YGGDRASIL_EXCEPT_InvArg(MSG)		::yggdrasil::yggdrasil_invalid_argument( MSG,  __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define YGGDRASIL_EXCEPT_LENGTH(MSG)		::yggdrasil::yggdrasil_length_error(MSG,  __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define YGGDRASIL_EXCEPT_OutOfBound(MSG)	::yggdrasil::yggdrasil_out_of_range_error(MSG,   __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define YGGDRASIL_EXCEPT_illMethod(MSG)	::yggdrasil::yggdrasil_illegal_method_call_error(MSG,   __PRETTY_FUNCTION__, __FILE__, __LINE__)















YGGDRASIL_NS_END(yggdrasil)





#undef YGGDRASIL_CORE_EXCEPT_INTERNAL_RULE_OF_FIVE

#endif //End include guards
