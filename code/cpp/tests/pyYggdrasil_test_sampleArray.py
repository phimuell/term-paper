import pyYggdrasil
s = pyYggdrasil.Sample(3,4.9)
s1 = pyYggdrasil.Sample([1.0, 2.1, 3.2])

# Creating a sample array
sa = pyYggdrasil.SampleArray(d = 3)
print("After creation")

print("The sample array is for sample of dimension {} (3), and has currently {} (0) many sampels.".format(sa.nDims(), len(sa)))

# Resize
print("Now perform the resizing and set its size to two.")
sa.resize(2)
print("The sample array is for sample of dimension {} (3), and has currently {} (2) many sampels.".format(sa.nDims(), len(sa)))

# Now we enter the samples
sa.loadSample(s, 0);
sa[1] = s1;

# Print out
print("The first sample, should be 3 x 4.9: {}".format(sa[0]))
print("The second sample should be the series: {}".format(sa[1]))

wasError = False
try:
    sa[2]
except:
    wasError = True

if wasError == True:
    print("As we have expected an error occured.")
else:
    print("An error should have occured, but it dod not happen.")

# Now clearing
sa.clear()
print("after clearing: The sample array is for sample of dimension {} (3), and has currently {} (0) many sampels.".format(sa.nDims(), len(sa)))

# Now testing resizing
sa.resize(10)
print("The size of the array is {}".format(sa.size()))
if(sa.size() != 10):
    raise ValueError("The resizing did not work, the array should host 10 samples, but it only hosts {} many samples.".format(sa.size()))

## Adding a sample
sa.addNewSample(s1)
if(sa.size() != 11):
    raise ValueError("The adding of the sample did not have any effct, the isze hould be 11, but it is {}".format(sa.size()))
print("The size after the insertion at the end is {}".format(sa.size()))








