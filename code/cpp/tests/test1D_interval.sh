#!/bin/bash
#
# This script compiles the tests for the tagged pointer
#######################

# Compile everything
# Is needed for the exceptions
g++ -std=c++11 -I../ -c ../core/yggdrasil_exception.cpp -o test1D_int1.o

# Compile the test program
g++ -std=c++11 -O0 -g  -I../ ./test1D_interval.cpp -c -o test1D_int2.o

# Link the program
g++ test1D_int1.o test1D_int2.o -o test1D_int.out

# run the prgram
./test1D_int.out
if [ $? -eq 0 ]
then
	echo "No errors where detected."
else
	echo "Errors where detected"
	exit 1
fi

# No error where detected so cleaing
rm test1D_int1.o test1D_int2.o test1D_int.out





