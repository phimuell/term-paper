/**
 * \file	tests/taggedPtr_test.cpp
 * \brief	A small test of the taged pointer sins it is not trivial
 */

#include <util/yggdrasil_taggedPointer.hpp>

using namespace yggdrasil;

int
main(
	int 	argc,
	char**	argv)
{
	//Create some pointers
	int* p1 = new int(1);
	int* p2 = new int(2);

	//Should be 1 and 2
	//std::cout << "P1 = " << *p1 << "; P2 = " << *p2 << std::endl;

	//Create some tagged pointers
	yggdrasil_taggedPointer_t<int> t1;
	yggdrasil_taggedPointer_t<int> t2;

#define IS_NULL(t) (t.isNull() ? "YES" : "NO")

	//Should be both yes
	//std::cout << "T1 is null " << IS_NULL(t1) << "; T2 is null " << IS_NULL(t2) << std::endl;

	if(t1.isNull() == false)
	{
		std::cerr << "Default constructor has failed on t1." << std::endl;
		return 1;
	};

	if(t2.isNull() == false)
	{
		std::cerr << "Default constructor has failed on t2." << std::endl;
		return 1;
	};

	//Test the tags
	if(t1.hasTag() == true)
	{
		std::cerr << "The there is a tag, but there should not be one." << std::endl;
		return 1;
	};

	//Now we load the pointer
	t1.setPointerTo(p1);

	//test if it has worked
	if(t1.isNull() == true)
	{
		std::cerr << "The pointer is still zero, but it was popolated." << std::endl;
		return 1;
	};

	//Comapre if the pointer is still the same
	if(t1.get() != p1)
	{
		std::cout << "The pointer is not decoded correctly." << std::endl;
		return 1;
	};


	//Now set a tag

	for(int i = 0; i != 256; ++i)
	{
		t1.setTag(i);

		if(t1.hasTag() != (i == 0 ? false : true))
		{
			std::cerr << "Tag is not set correctly for i = " << i << std::endl;
			std::abort();
			return 1;
		};

		if(t1.get() != p1)
		{
			std::cerr << "Tag has contaminated the address , for i = " << i << std::endl;
			return 1;
		};

		if(t1.getTag() != yggdrasil_taggedPointer_t<int>::Tag_t(i))
		{
			std::cerr << "The tag is not read correcly for i = " << i << std::endl;
			return 1;
		};
	}; //end for(i)

	//Set a tag, for testing if the exchange function works
	t2.setTag(23);	//Value does not matter, should only be != 0
						yggdrasil_assert(t2.hasTag() == true);

	//Set the pointer value, this will remove the tag
	t2.setPointerTo(p2);

	//Test if the tag was removed
	if(t2.hasTag() == true)
	{
		std::cerr << "The setPointerTo() did not clear the tag." << std::endl;
		return 1;
	};

	//Set a new tag
	t2.setTag(10);


	//Testing of ecxchanging the address
	for(int i = 0; i != 256; ++i)
	{
		t1.setPointerTo(p1);

		if(t1 == t2)
		{
			std::cerr << "Camaring(1) is broken, for i = " << i << std::endl;
			return 1;
		};

		if(t1.isNull() == true)
		{
			std::cerr << "Pointer detection is not correct, for i = " << i << std::endl;
			return 1;
		};

		t1.setTag(i);

		//Tey still has to be unequal
		if(t1 == t2)
		{
			std::cerr << "Comparing(2) is broken, for i = " << i << std::endl;
			return 1;
		};

		if(t1.getTag() != i)
		{
			std::cerr << "The tag detection is broken; for i = " << i << std::endl;
			std::abort();
			return 1;
		};

		if(t1.get() != p1)
		{
			std::cerr << "Pointer is incorrectly load; for i = " << i << std::endl;
			return 1;
		};

		t1.exchangePointer(p2);

		//Now they have to be the same
		if(t1 != t2)
		{
			std::cerr << "Comapring(3) is broken, for i = " << i << std::endl;
			return 1;
		};

		if(t1.get() != p2)
		{
			std::cerr << "Pointer excahnge is broken; for i = " << i << std::endl;
			std::abort();
			return 1;
		};

		if(t1.getTag() != i)
		{
			std::cerr << "Pointer exchange deletes tag; for i = " << i << std::endl;
			return 1;
		};

		//Now we test the set functionality
		t1.setPointerTo(p1);
		if(t1.hasTag() == true)
		{
			std::cerr << "The address set does not clear the tag." << std::endl;
			return 1;
		};

	}; //end for(i)


	//Test the reading
	*p1 = 2;
	*p2 = 4;

	t1.setPointerTo(p1);
	t2.setPointerTo(p2);

	//Testing of ecxchanging the address
	for(int i = 0; i != 256; ++i)
	{

		t1.setTag(i);
		if(2 != *t1)
		{
			std::cerr << "Deref operator is broken, for i = " << i << std::endl;
			return 1;
		};

	}; //end for(i)

	t1.setTag(1);
	t2.setTag(2);

	t1 = std::move(t2);

	if(t2.isNull() == false)
	{
		std::cerr << "The move does not remove the adress" << std::endl;
		return 1;
	};
	if(t2.hasTag() == true)
	{
		std::cerr << "The move does not remove the tag" << std::endl;
		return 1;
	};
	if(t1.getTag() != 2)
	{
		std::cerr << "The tag is not copied, in the move" << std::endl;
		return 1;
	};
	if(t1.get() != p2)
	{
		std::cerr << "The pointer is not copied." << std::endl;
		return 1;
	};










	return 0;
}; //End main





