/**
 */

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <util/yggdrasil_chi2_frequencyTable.hpp>
#include <algorithm>
#include <tuple>
#include <random>


using namespace yggdrasil;
using Array_t = yggdrasil::yggdrasil_dimensionArray_t;
using Coll_t  = yggdrasil::yggdrasil_sampleCollection_t;
using FreqTable_t = ::yggdrasil::yggdrasil_chi2FreqTable_t;
using BinEdges_t = FreqTable_t::BinEdgesArray_t;
using IntervalBound_t = ::yggdrasil::yggdrasil_intervalBound_t;

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};

int
main(
	int 	argc,
	char**	argv)
{
	std::mt19937_64 geni(42);

	bool Q[2] = {false, true};

	for(bool q : Q)
	{


		//Test 1
		{

			//Create an empty dim array
			Array_t dimArray1(0);
			Array_t dimArray2(1);

			//Create a test table
			FreqTable_t table;

			//Make some special
			const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
			BinEdges_t::ValueArray_t emptyInnerEdges;

			const BinEdges_t bins1(unitInterval, emptyInnerEdges);
			const BinEdges_t bins2(unitInterval, emptyInnerEdges);

			//bind the table
			bool hasThrown = false;
			try
			{
				table.buildInitialFreqTable(dimArray1, bins1, dimArray2, bins2, q);
			}
			catch(const yggdrasil_exception& e)
			{
				hasThrown = true;
				(void)e;
			};


			//Test
			if(hasThrown == false)
			{
				std::cerr << "The construction of the empty freq table did not produce an error" << std::endl;
				std::abort();
			};

		}; //End test 1

		//Test 2
		{

			//Create an empty dim array
			Array_t::ValueArray_t valArr1 = {0.5};
			Array_t::ValueArray_t valArr2 = {0.5};

			Array_t dimArray1(0);
			dimArray1.copyAndLoadValueArray(valArr1);

			Array_t dimArray2(1);
			dimArray2.copyAndLoadValueArray(valArr2);

			//Create a test table
			FreqTable_t table;

			//Make some special
			const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
			BinEdges_t::ValueArray_t emptyInnerEdges;

			const BinEdges_t bins1(unitInterval, emptyInnerEdges);
			const BinEdges_t bins2(unitInterval, emptyInnerEdges);

			//bind the table
			table.buildInitialFreqTable(dimArray1, bins1, dimArray2, bins2, q);


			//Test
			if(table.nBinsFirst() != 1)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};
			if(table.nBinsSecond() != 1)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};
			if(table.getFrequency(0, 0) != 1)
			{
				std::cerr << "The bin count was supposed to constain one sample, but it contained " << table.getFrequency(0, 0) << std::endl;
				std::abort();
			};

		}; //End test 2


		//Test 3
		{
			const Size_t N[4] = {100, 333, 432, 782};
			const int J[4] = {0, 1, 2, 3};

			//This are the index that corresponds for the accessing
			const int accFirst[4] = {0, 1, 0, 1};
			const int accSecon[4] = {0, 0, 1, 1};

			Array_t::ValueArray_t valArr1, valArr2;
			for(int k : J)
			{
				const int i = accFirst[k];
				const int j = accSecon[k];
				std::uniform_real_distribution<double> dist1(0.01 + i * 0.5, (i + 1) * 0.5 - 0.01);
				std::uniform_real_distribution<double> dist2(0.01 + j * 0.5, (j + 1) * 0.5 - 0.01);
				for(Size_t kk = 0; kk != N[k]; ++kk)
				{
					valArr1.push_back(dist1(geni));
					valArr2.push_back(dist2(geni));
				}; //End for(i):
			}; //End for(j):

			std::vector<Size_t> idxArray(valArr1.size());
			for(Size_t i = 0; i != valArr1.size(); ++i)
			{
				idxArray[i] = i;
			};

			std::shuffle(idxArray.begin(), idxArray.end(), geni);
			Array_t::ValueArray_t v1(idxArray.size());
			Array_t::ValueArray_t v2(idxArray.size());
			for(Size_t i = 0; i != idxArray.size(); ++i)
			{
				v1[i] = valArr1[idxArray[i]];
				v2[i] = valArr2[idxArray[i]];
			};

			v1.swap(valArr1);
			v2.swap(valArr2);
			v1.clear();
			v2.clear();


			//Create an empty dim array
			Array_t dimArray1(0);
			dimArray1.copyAndLoadValueArray(valArr1);

			Array_t dimArray2(1);
			dimArray2.copyAndLoadValueArray(valArr2);

			//Create a test table
			FreqTable_t table;

			//Make some special
			const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
			BinEdges_t::ValueArray_t innerEdges = {0.5};

			const BinEdges_t bins1(unitInterval, innerEdges);
			const BinEdges_t bins2(unitInterval, innerEdges);

			//bind the table
			table.buildInitialFreqTable(dimArray1, bins1, dimArray2, bins2, q);


			//Test
			if(table.nBinsFirst() != 2)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};
			if(table.nBinsSecond() != 2)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};

			for(int k = 0; k != 4; ++k)
			{
				const int i = accFirst[k];
				const int j = accSecon[k];

				if(table.getFrequency(i, j) != N[k])
				{
					std::cerr << "The bin count was supposed to constain " << N[k] << " many sample, but it contained " << table.getFrequency(i, j)
						<< " in bin (" << i << ", " << j << ")." << std::endl;
					std::abort();
				};
			}; //End for(k)

		}; //End test 3



		//Test 4
		{
			//Create an empty dim array
			Array_t::ValueArray_t valArr1 = {0.25, 0.25, 0.75, 0.75};
			Array_t::ValueArray_t valArr2 = {0.25, 0.75, 0.25, 0.75};

			Array_t dimArray1(0);
			dimArray1.copyAndLoadValueArray(valArr1);

			Array_t dimArray2(1);
			dimArray2.copyAndLoadValueArray(valArr2);

			//Create a test table
			FreqTable_t table;

			//Make some special
			const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
			BinEdges_t::ValueArray_t innerEdges = {0.5};

			const BinEdges_t bins1(unitInterval, innerEdges);
			const BinEdges_t bins2(unitInterval, innerEdges);

			//bind the table
			table.buildInitialFreqTable(dimArray1, bins1, dimArray2, bins2, q);


			//Test
			if(table.nBinsFirst() != 2)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};
			if(table.nBinsSecond() != 2)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};

			for(int i = 0; i != 2; ++i)
			{
				for(int j = 0; j != 2; ++j)
				{
					if(table.getFrequency(i, j) != 1)
					{
						std::cerr << "The bin count was supposed to constain one sample, but it contained " << table.getFrequency(i, j)
							<< " in bin (" << i << ", " << j << ")." << std::endl;
						std::abort();
					};
				}; //End for(j):
			}; //End for(i):

		}; //End test 4

		//Test 5
		{
			Size_t N[16];
			std::uniform_int_distribution<Size_t> nDist(100, 300);
			for(int i = 0; i != 16; ++i)
			{
				N[i] = nDist(geni);
			};


			int J[16];
			for(int i = 0; i != 16; ++i)
			{
				J[i] = i;
			};

			//This are the index that corresponds for the accessing
			int accFirst[16];
			int accSecon[16];
			for(int j = 0; j != 4; ++j)
			{
				for(int i = 0; i != 4; ++i)
				{
					accFirst[4 * j + i] = i;
					accSecon[4 * j + i] = j;
				}; //End for(i)
			}; //End for(i)

			Array_t::ValueArray_t valArr1, valArr2;
			for(int k : J)
			{
				const int i = accFirst[k];
				const int j = accSecon[k];
				std::uniform_real_distribution<double> dist1(0.01 + i * 0.25, (i + 1) * 0.25 - 0.01);
				std::uniform_real_distribution<double> dist2(0.01 + j * 0.25, (j + 1) * 0.25 - 0.01);
				for(Size_t kk = 0; kk != N[k]; ++kk)
				{
					valArr1.push_back(dist1(geni));
					valArr2.push_back(dist2(geni));
				}; //End for(i):
			}; //End for(j):

			std::vector<Size_t> idxArray(valArr1.size());
			for(Size_t i = 0; i != valArr1.size(); ++i)
			{
				idxArray[i] = i;
			};

			std::shuffle(idxArray.begin(), idxArray.end(), geni);
			Array_t::ValueArray_t v1(idxArray.size());
			Array_t::ValueArray_t v2(idxArray.size());
			for(Size_t i = 0; i != idxArray.size(); ++i)
			{
				v1[i] = valArr1[idxArray[i]];
				v2[i] = valArr2[idxArray[i]];
			};

			v1.swap(valArr1);
			v2.swap(valArr2);
			v1.clear();
			v2.clear();


			//Create an empty dim array
			Array_t dimArray1(0);
			dimArray1.copyAndLoadValueArray(valArr1);

			Array_t dimArray2(1);
			dimArray2.copyAndLoadValueArray(valArr2);

			//Create a test table
			FreqTable_t table;

			//Make some special
			const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
			BinEdges_t::ValueArray_t innerEdges = {0.25, 0.50, 0.75};

			const BinEdges_t bins1(unitInterval, innerEdges);
			const BinEdges_t bins2(unitInterval, innerEdges);

			//bind the table
			table.buildInitialFreqTable(dimArray1, bins1, dimArray2, bins2, q);


			//Test
			if(table.nBinsFirst() != 4)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};
			if(table.nBinsSecond() != 4)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};

			for(const int k : J)
			{
				const int i = accFirst[k];
				const int j = accSecon[k];

				if(table.getFrequency(i, j) != N[k])
				{
					std::cerr << "The bin count was supposed to constain " << N[k] << " many sample, but it contained " << table.getFrequency(i, j)
						<< " in bin (" << i << ", " << j << ")." << std::endl;
					std::abort();
				};
			}; //End for(k)

		}; //End test 5


		//Test 5.2
		{
			//Create an empty dim array
			Array_t::ValueArray_t valArr1 = {0.25, 0.25, 0.75};
			Array_t::ValueArray_t valArr2 = {0.25, 0.75, 0.25};

			Array_t dimArray1(0);
			dimArray1.copyAndLoadValueArray(valArr1);

			Array_t dimArray2(1);
			dimArray2.copyAndLoadValueArray(valArr2);

			//Create a test table
			FreqTable_t table;

			//Make some special
			const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
			BinEdges_t::ValueArray_t innerEdges = {0.5};

			const BinEdges_t bins1(unitInterval, innerEdges);
			const BinEdges_t bins2(unitInterval, BinEdges_t::ValueArray_t());

			//bind the table
			table.buildInitialFreqTable(dimArray1, bins1, dimArray2, bins2, q);


			//Test
			if(table.nBinsFirst() != 2)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};
			if(table.nBinsSecond() != 1)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};

			for(int i = 0; i != 2; ++i)
			{
				const int e = (i == 0) ? 2 : 1;
				if(table.getFrequency(i, 0) != e)
				{
						std::cerr << "The bin count was supposed to constain one sample, but it contained " << table.getFrequency(i, 0)
							<< " in bin (" << i << ", " << 0 << ")." << std::endl;
						std::abort();
				};
			}; //End for(i):


		}; //End: test 5.2


		//Test 6
		{
			for(int fSize = 1; fSize != 16; fSize++)
			{
				for(int sSize = 1; sSize != 16; ++sSize)
				{
					const double sMargin = 0.001;
					const double fH = 1.0 / fSize;
					const double sH = 1.0 / sSize;

					std::vector<Size_t> N(sSize * fSize);
					std::uniform_int_distribution<Size_t> nDist(100, 3000);
					for(int i = 0; i != N.size(); ++i)
					{
						N[i] = nDist(geni);
					};


					std::vector<int> J(fSize * sSize);
					for(int i = 0; i != J.size(); ++i)
					{
						J[i] = i;
					};

					//This are the index that corresponds for the accessing
					std::vector<int> accFirst(fSize * sSize);
					std::vector<int> accSecon(fSize * sSize);
					for(int j = 0; j != sSize; ++j)
					{
						for(int i = 0; i != fSize; ++i)
						{
							accFirst[fSize * j + i] = i;
							accSecon[fSize * j + i] = j;
						}; //End for(i)
					}; //End for(i)

					Array_t::ValueArray_t valArr1, valArr2;
					for(int k : J)
					{
						const int i = accFirst[k];
						const int j = accSecon[k];
						std::uniform_real_distribution<double>
							dist1(
								sMargin + i * fH,
								(i + 1) * fH - sMargin);
						std::uniform_real_distribution<double>
							dist2(
								sMargin + j * sH,
								(j + 1) * sH - sMargin);

						for(Size_t kk = 0; kk != N[k]; ++kk)
						{
							valArr1.push_back(dist1(geni));
							valArr2.push_back(dist2(geni));
						}; //End for(i):
					}; //End for(j):

					std::vector<Size_t> idxArray(valArr1.size());
					for(Size_t i = 0; i != valArr1.size(); ++i)
					{
						idxArray[i] = i;
					};

					std::shuffle(idxArray.begin(), idxArray.end(), geni);
					Array_t::ValueArray_t v1(idxArray.size());
					Array_t::ValueArray_t v2(idxArray.size());
					for(Size_t i = 0; i != idxArray.size(); ++i)
					{
						v1[i] = valArr1[idxArray[i]];
						v2[i] = valArr2[idxArray[i]];
					};

					v1.swap(valArr1);
					v2.swap(valArr2);
					v1.clear();
					v2.clear();


					//Create an empty dim array
					Array_t dimArray1(0);
					dimArray1.copyAndLoadValueArray(valArr1);

					Array_t dimArray2(1);
					dimArray2.copyAndLoadValueArray(valArr2);

					//Create a test table
					FreqTable_t table;

					//Make some special
					const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
					BinEdges_t::ValueArray_t innerEdges1;
					for(int i = 1; i != fSize; ++i)
					{
						innerEdges1.push_back(i * fH);
					};
					BinEdges_t::ValueArray_t innerEdges2;
					for(int i = 1; i != sSize; ++i)
					{
						innerEdges2.push_back(i * sH);
					};

					const BinEdges_t bins1(unitInterval, innerEdges1);
					const BinEdges_t bins2(unitInterval, innerEdges2);

					//bind the table
					table.buildInitialFreqTable(dimArray1, bins1, dimArray2, bins2, q);


					//Test
					if(table.nBinsFirst() != fSize)
					{
						std::cerr << "There where not zero bins." << std::endl;
						std::abort();
					};
					if(table.nBinsSecond() != sSize)
					{
						std::cerr << "There where not zero bins." << std::endl;
						std::abort();
					};

					for(const int k : J)
					{
						const int i = accFirst[k];
						const int j = accSecon[k];

						if(table.getFrequency(i, j) != N[k])
						{
							std::cerr << "The bin count was supposed to constain " << N[k] << " many sample, but it contained " << table.getFrequency(i, j)
								<< " in bin (" << i << ", " << j << ")." << std::endl;
							std::abort();
						};
					}; //End for(k)

				}; //end second size
			}; //end first size

		}; //End test 6

		//Test 6.2
		{
			for(int fSize = 1; fSize != 16; fSize++)
			{
				for(int sSize = 1; sSize != 16; ++sSize)
				{
					const double sMargin = 0.001;
					const double fH = 1.0 / fSize;
					const double sH = 1.0 / sSize;

					std::vector<Size_t> N(sSize * fSize);
					std::uniform_int_distribution<Size_t> nDist(100, 300);
					for(int i = 0; i != N.size(); ++i)
					{
						N[i] = nDist(geni);
					};


					std::vector<int> J(fSize * sSize);
					for(int i = 0; i != J.size(); ++i)
					{
						J[i] = i;
					};

					//This are the index that corresponds for the accessing
					std::vector<int> accFirst(fSize * sSize);
					std::vector<int> accSecon(fSize * sSize);
					for(int j = 0; j != sSize; ++j)
					{
						for(int i = 0; i != fSize; ++i)
						{
							accFirst[fSize * j + i] = i;
							accSecon[fSize * j + i] = j;
						}; //End for(i)
					}; //End for(i)

					Array_t::ValueArray_t valArr1, valArr2;
					for(int k : J)
					{
						const int i = accFirst[k];
						const int j = accSecon[k];
						std::uniform_real_distribution<double>
							dist1(
								sMargin + i * fH,
								(i + 1) * fH - sMargin);
						std::uniform_real_distribution<double>
							dist2(
								sMargin + j * sH,
								(j + 1) * sH - sMargin);

						for(Size_t kk = 0; kk != N[k]; ++kk)
						{
							valArr1.push_back(dist1(geni));
							valArr2.push_back(dist2(geni));
						}; //End for(i):
					}; //End for(j):

					std::vector<Size_t> idxArray(valArr1.size());
					for(Size_t i = 0; i != valArr1.size(); ++i)
					{
						idxArray[i] = i;
					};

					std::shuffle(idxArray.begin(), idxArray.end(), geni);
					Array_t::ValueArray_t v1(idxArray.size());
					Array_t::ValueArray_t v2(idxArray.size());
					for(Size_t i = 0; i != idxArray.size(); ++i)
					{
						v1[i] = valArr1[idxArray[i]];
						v2[i] = valArr2[idxArray[i]];
					};

					v1.swap(valArr1);
					v2.swap(valArr2);
					v1.clear();
					v2.clear();


					//Create an empty dim array
					Array_t dimArray1(0);
					dimArray1.copyAndLoadValueArray(valArr1);

					Array_t dimArray2(1);
					dimArray2.copyAndLoadValueArray(valArr2);

					//Create a test table
					FreqTable_t table;

					//Make some special
					const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
					BinEdges_t::ValueArray_t innerEdges1;
					for(int i = 1; i != fSize; ++i)
					{
						innerEdges1.push_back(i * fH);
					};
					BinEdges_t::ValueArray_t innerEdges2;
					for(int i = 1; i != sSize; ++i)
					{
						innerEdges2.push_back(i * sH);
					};

					BinEdges_t bins1(unitInterval, innerEdges1);
					bins1.addPartition(dimArray1);

					BinEdges_t bins2(unitInterval, innerEdges2);
					bins2.addPartition(dimArray2);


					//bind the table
					table.buildInitialFreqTable(dimArray1, bins1, dimArray2, bins2, q);


					//Test
					if(table.nBinsFirst() != fSize)
					{
						std::cerr << "There where not zero bins." << std::endl;
						std::abort();
					};
					if(table.nBinsSecond() != sSize)
					{
						std::cerr << "There where not zero bins." << std::endl;
						std::abort();
					};

					for(const int k : J)
					{
						const int i = accFirst[k];
						const int j = accSecon[k];

						if(table.getFrequency(i, j) != N[k])
						{
							std::cerr << "The bin count was supposed to constain " << N[k] << " many sample, but it contained " << table.getFrequency(i, j)
								<< " in bin (" << i << ", " << j << ")." << std::endl;
							std::abort();
						};
					}; //End for(k)

				}; //end second size
			}; //end first size

		}; //End test 6



	}; //End for(q):


	return 0;

}; //End:  main



