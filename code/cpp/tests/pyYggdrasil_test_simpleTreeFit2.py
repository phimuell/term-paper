import pyYggdrasil
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

def main():

    # Generate a domain [-5, 5[
    domainI  = pyYggdrasil.Interval(-5.0, 5.0)
    domainHC = pyYggdrasil.HyperCube(1, domainI)

    # The number of sample we want
    N = 100000

    # The distribution of the samples
    mu  = 0.0
    std = 1.0

    # Generate the sample
    sa = pyYggdrasil.SampleCollection(1)

    # This is a regular numpy array
    xSample = np.zeros(N)

    # This is a counter
    c = 0

    while (sa.nSamples() < N):

        # Propose a sample
        propSample = np.random.normal(mu, std)

        if(domainI.isInside(propSample)):
            s = pyYggdrasil.Sample(1, propSample)
            sa.addNewSample(s)
            xSample[c] = propSample
            c += 1
        # end if: sample was good
    #end: while

    # Make a builder
    b = pyYggdrasil.TreeBuilder(pyYggdrasil.eParModel.ConstModel).setGOFLevel(0.001).setIndepLevel(0.001).useSizeSplitter()
    print("The used builder:")
    print(b)

    # Construct the tree
    tree = pyYggdrasil.DETree(sa, domainHC, b)

    # Get the tree map
    depthMap = tree.getDepthMap()

    # Print some information from the depth map
    print('Number of leafs: {}'.format(depthMap.getLeafCount()))
    print('Tree reports:    {}'.format(tree.nLeafs()))
    print('Mean Depth:    {}'.format(depthMap.getDepthMean()))

    # Print itterating over the trees
    c = 0
    totVol = 0.0
    for leaf in tree:
        vol = leaf.getDomain().getVol()
        totVol += vol
        print('Leaf {} contains {} many leafs and occupies a volume of {}'.format(c, leaf.nSamples(), vol))
    print('Total volume {}'.format(totVol))





if __name__ == "__main__":
    main()













