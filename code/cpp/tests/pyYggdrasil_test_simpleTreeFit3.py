import pyYggdrasil
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

def main():

    # The generator that we use
    geni = pyYggdrasil.Random.pRNG(0)
    print("Type of geni:")
    print(type(geni))

    # Domension of teh problem
    D = 2

    # Generate a domain [-5, 5[
    domainI  = pyYggdrasil.Interval(-5.0, 5.0)
    domainHC = pyYggdrasil.HyperCube(D, domainI)

    # The number of sample we want
    N = 10000

    # The distribution of the samples
    Mu    = np.array([0.0, 0.0])
    Sigma = np.matrix([[1, 0.5], [0.5, 1]])

    # Generate the distribution
    GaussDistri = pyYggdrasil.Random.Gauss(Mu, Sigma, domainHC)

    print("Type of gauss:")
    print(type(GaussDistri))


    print("The sampling domain:")
    print(GaussDistri.getSampleDomain())
    print("The domain is unbounded: {}".format(GaussDistri.isUnbounded()))
    print()

    print("The mean:")
    print(GaussDistri.getMean())
    print()

    print("The coavr:")
    print(GaussDistri.getCoVar())
    print()

    # Generate the sample
    sa = GaussDistri.generateSamplesCollection(g = geni, N = N)

    # Make a builder
    b = pyYggdrasil.TreeBuilder(pyYggdrasil.eParModel.ConstModel).setGOFLevel(0.001).setIndepLevel(0.001).useSizeSplitter()
    print("The used builder:")
    print(b)

    # Construct the tree
    tree = pyYggdrasil.DETree(sa, domainHC, b)

    # Get the tree map
    depthMap = tree.getDepthMap()

    # Print some information from the depth map
    print('Number of leafs: {}'.format(depthMap.getLeafCount()))
    print('Tree reports:    {}'.format(tree.nLeafs()))
    print('Mean Depth:    {}'.format(depthMap.getDepthMean()))

    # Print itterating over the trees
    c = 0
    totVol = 0.0
    for leaf in tree:
        vol = leaf.getDomain().getVol()
        totVol += vol
        print('Leaf {} contains {} many leafs and occupies a volume of {}'.format(c, leaf.nSamples(), vol))
    print('Total volume {}'.format(totVol))



if __name__ == "__main__":
    main()













