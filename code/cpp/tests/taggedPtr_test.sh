#!/bin/bash
#
# This script compiles the tests for the tagged pointer
#######################

# Compile everything
# Is needed for the exceptions
g++ -std=c++11 -I../ -c ../core/yggdrasil_exception.cpp -o tagPTRTest1.o

# Compile the test program
g++ -std=c++11 -O0 -g  -I../ taggedPtr_test.cpp -c -o tagPTRTest2.o

# Link the program
g++ tagPTRTest1.o tagPTRTest2.o -o tagPTRTest.out

# run the prgram
./tagPTRTest.out
if [ $? -eq 0 ]
then
	echo "No errors where detected."
else
	echo "Errors where detected"
	exit 1
fi

# No error where detected so cleaing
rm tagPTRTest1.o tagPTRTest2.o tagPTRTest.out





