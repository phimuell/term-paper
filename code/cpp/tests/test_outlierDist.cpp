
#include <core/yggdrasil_consts.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_outlierPDF.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <algorithm>
#include <tuple>
#include <random>

#include <Eigen/Dense>


using namespace yggdrasil;

std::mt19937_64 geni(42);

using HyperCube_t = yggdrasil_hyperCube_t;

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


using outLier_t = ::yggdrasil::yggdrasil_outlierPDF_t;


//This is for computing the relative error
template<
	class T
	>
Numeric_t
relError(
	const T& 	estimated,
	const T&	expected)
{
	const T difference = estimated - expected;


	/*
	std::cerr << "\nDIFF NORM: " << difference.norm() << "\n";
	std::cerr << "DIFF MAX COEF: " << difference.array().abs().maxCoeff() << "\n";
	std::cerr << "EXP NORM:    " << expected.norm() << "\n";
	std::cerr << "DIF: " << difference << "\n";
	*/

	//Handle the case if the mean is zero
	const Numeric_t absExp = expected.norm();

	if(absExp < 1000 * yggdrasil::Constants::EPSILON)
	{
		return difference.norm();
	};

	return (difference.norm() / expected.norm());
};






int
main()
{
	// Test1
	{
		const Size_t N = 1000000;

		//Create a gaus
		outLier_t dist = (*(outLier_t*)(yggdrasil_randomDistribution_i::FACTORY(eDistiType::Outlier, 1).first.get()));

		//Get the empirical values
		auto theoMean = dist.getMean();
		auto theoVar  = dist.getCoVar();

		//Generate some samples
		outLier_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

		//This are the estimated params
		outLier_t::MeanVec_t estMean;
		outLier_t::CoVarMat_t estCoVar;

		//Calculate
		std::tie(estMean, estCoVar) = yggdrasil_estinmateStatistic(samples);

		const auto relErrMean  = relError(estMean, theoMean);
		const auto relErrCoVar = relError(estCoVar, theoVar);

		std::cerr
			<< "The mean:\n"
			<< estMean
			<< "\n"
			<< "Rel Error: " << relErrMean
			<< "\n\n";

		std::cerr
			<< "The estimated covariance:\n"
			<< estCoVar
			<< "\n"
			<< "RelError: " << relErrCoVar
			<< std::endl;


		if(relErrMean >= 0.05)
		{
			std::cerr
				<< "Relative error of mean, " << relErrMean << ", was too high"
				<< std::endl;
			std::abort();
		};
		if(relErrCoVar>= 0.05)
		{
			std::cerr
				<< "Relative error of covar, " << relErrCoVar << ", was too high"
				<< std::endl;
			std::abort();
		};
	}; //End test 1

	// Test2
	{
		const Size_t N = 1000000;

		//Create a gaus
		outLier_t dist = (*(outLier_t*)(yggdrasil_randomDistribution_i::FACTORY(eDistiType::Outlier, 0).first.get()));

		//Get the empirical values
		auto theoMean = dist.getMean();
		auto theoVar  = dist.getCoVar();

		//Generate some samples
		outLier_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

		//This are the estimated params
		outLier_t::MeanVec_t estMean;
		outLier_t::CoVarMat_t estCoVar;

		//Calculate
		std::tie(estMean, estCoVar) = yggdrasil_estinmateStatistic(samples);

		const auto relErrMean  = relError(estMean, theoMean);
		const auto relErrCoVar = relError(estCoVar, theoVar);

		std::cerr
			<< "The mean:\n"
			<< estMean
			<< "\n"
			<< "Rel Error: " << relErrMean
			<< "\n\n";

		std::cerr
			<< "The estimated covariance:\n"
			<< estCoVar
			<< "\n"
			<< "RelError: " << relErrCoVar
			<< std::endl;


		if(relErrMean >= 0.05)
		{
			std::cerr
				<< "Relative error of mean, " << relErrMean << ", was too high"
				<< std::endl;
			std::abort();
		};
		if(relErrCoVar>= 0.05)
		{
			std::cerr
				<< "Relative error of covar, " << relErrCoVar << ", was too high"
				<< std::endl;
			std::abort();
		};
	}; //End test 2

	return 0;
}; //End: main






