/**
 * \brief	This is a test for the tree sampler, it is a very simple test.
 */

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeImpl.hpp>

#include <para_models/yggdrasil_linModel.hpp>
#include <para_models/yggdrasil_constModel.hpp>
#include <stat_tests/yggdrasil_IndepTest_chi2.hpp>
#include <stat_tests/yggdrasil_GOFTest_chi2.hpp>

#include <split_scheme/yggdrasil_sizeBasedSplitting.hpp>
#include <split_scheme/yggdrasil_scoreBasedSplitting.hpp>

#include <tree_geni/yggdrasil_treeSampler.hpp>

#include <algorithm>
#include <tuple>
#include <random>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_multiGauss.hpp>

#include <algorithm>
#include <tuple>
#include <random>
#include <chrono>

#include <Eigen/Dense>

using Array_t = yggdrasil::yggdrasil_dimensionArray_t;
using Coll_t  = yggdrasil::yggdrasil_sampleCollection_t;
using SplitCom_t = yggdrasil::yggdrasil_singleSplit_t;
using SplitSeq_t = yggdrasil::yggdrasil_splitSequence_t;
using HyperCube_t = Coll_t::HyperCube_t;
using IntervalBound_t = HyperCube_t::IntervalBound_t;
using IndexArray_t = Coll_t::IndexArray_t;
using ValArray_t = Coll_t::ValueArray_t;
using Node_t  = yggdrasil::yggdrasil_DETNode_t;
using Builder_t = yggdrasil::yggdrasil_treeBuilder_t;
using SampleCollection_t = yggdrasil::yggdrasil_sampleCollection_t;
using Tree_t = yggdrasil::yggdrasil_DETree_t;


using namespace yggdrasil;

std::mt19937_64 geni(42);

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


using Gauss_t = ::yggdrasil::yggdrasil_multiDimGauss_t;




bool
isTreeFullyValid(
	const yggdrasil_DETree_t& 	tree)
{
	if(tree.checkIntegrity() == false)
	{
		std::cerr << "The tree diod not pass the integrity check." << std::endl;
		return false;
	};


	yggdrasil_DETree_t::SubLeafIterator_t ende;
	for( auto it = tree.getLeafIterator(); it != ende; ++it)
	{
		const auto nf = *it;

		if(nf.isFullySplittedLeaf() == false)
		{
			std::cerr << "The node is not fully splitted." << std::endl;
			return false;
		};
		if(nf.checkIntegrity() == false)
		{
			std::cerr << "The node failed the integrty check." << std::endl;
			return false;
		};
	}; //ENd for(it)

	if(tree.isFullySplittedTree() == false)
	{
		std::cerr << "The tree did not appear to be fully fitted." << std::endl;
		return false;
	};


	return true;
}; //End tree






int
main(
	int 	argc,
	char** 	argv)
{
	eParModel mod = eParModel::LinModel;
	if(argc == 2)
	{
		if(argv[1] == std::string("--const"))
		{
			mod = eParModel::ConstModel;
		};
	};

	//This is teh configuration we use
	Builder_t builder = Builder_t(mod)
			.useMedianSplitter()
			.setGOFLevel(0.001)
			.setIndepLevel(0.001)
	; //End

	std::cout
		<< "BUILDER:\n"
		<< builder.print()
		<< "\n\n"
		<< std::endl;


	//The sample size to use
	const Size_t N = 300000;

	//Test 1
	{
		//Generate a distribution
		auto dist = yggdrasil::yggdrasil_randomDistribution_i::FACTORY(yggdrasil::eDistiType::Gauss, 2).first;

		//Generate samples for the fitting
		SampleCollection_t col = dist->generateSamplesCollection(geni, N);

		//Compute the mean and the std of the samples
		const auto statFitSamples = yggdrasil::yggdrasil_estinmateStatistic(col);

		//Fitt the tree
		Tree_t tree(col, dist->getSampleDomain(), builder);

		//Building a tree sampler, that is not trestricted
		yggdrasil::yggdrasil_treeSampler_t treeDist(tree);

		//Generate many sampels
		SampleCollection_t bootstrappedSamples = treeDist.generateSamplesCollection(geni, N * 10);

		//Compute the statistic of the boot strapped
		const auto bsStat = yggdrasil::yggdrasil_estinmateStatistic(bootstrappedSamples);


		//Output
		std::cout
			<< "First Test:\n";
		std::cout
			<< "Theoretical mean: " << dist->getMean().transpose() << "\n"
			<< "Fit Sample Means: " << statFitSamples.first.transpose() << "\n"
			<< "boot Mean:        " << bsStat.first.transpose() << "\n"
			<< "\n";

		std::cout
			<< "Theoretical CoVar:\n"
			<< dist->getCoVar()
			<< "\n\n"
			<< "Fit Sample CoVar:\n"
			<< statFitSamples.second
			<< "\n\n"
			<< "Boot CoVar:\n"
			<< bsStat.second
			<< "\n"
			<< std::endl;
	}; //End: test 1

	//Test 2
	{
		//Generate a distribution
		auto dist = yggdrasil::yggdrasil_randomDistribution_i::FACTORY(yggdrasil::eDistiType::Gauss, -2).first;

		//Generate samples for the fitting
		SampleCollection_t col = dist->generateSamplesCollection(geni, N);

		//Compute the mean and the std of the samples
		const auto statFitSamples = yggdrasil::yggdrasil_estinmateStatistic(col);

		//Fitt the tree
		Tree_t tree(col, dist->getSampleDomain(), builder);

		//Building a tree sampler, that is not trestricted
		yggdrasil::yggdrasil_treeSampler_t treeDist(tree);

		//Generate many sampels
		SampleCollection_t bootstrappedSamples = treeDist.generateSamplesCollection(geni, N * 10);

		//Compute the statistic of the boot strapped
		const auto bsStat = yggdrasil::yggdrasil_estinmateStatistic(bootstrappedSamples);


		//Output
		std::cout
			<< "Second Test:\n";
		std::cout
			<< "Theoretical mean: " << dist->getMean().transpose() << "\n"
			<< "Fit Sample Means: " << statFitSamples.first.transpose() << "\n"
			<< "boot Mean:        " << bsStat.first.transpose() << "\n"
			<< "\n";

		std::cout
			<< "Theoretical CoVar:\n"
			<< dist->getCoVar()
			<< "\n\n"
			<< "Fit Sample CoVar:\n"
			<< statFitSamples.second
			<< "\n\n"
			<< "Boot CoVar:\n"
			<< bsStat.second
			<< "\n"
			<< std::endl;
	}; //End: test 2


	return 0;
}; //End: main






