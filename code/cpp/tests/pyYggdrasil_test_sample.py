import pyYggdrasil
s = pyYggdrasil.Sample(3,4.9)

print("A sample of dimension 3, all elements set to 4.9")
print(s)

s1 = pyYggdrasil.Sample([1.0, 2.1, 3.2])
print("A sample with increasing comonents")
print(s1)

print("The dimension of the sample is {}, it should be 3".format(len(s1)))

print("Iterating through the sample s1 and setting each component to the index value minus 0.5")
for i in range(s1.size()):
    oldVal = s1[i]
    s1[i] = i - 0.5
    print("old val: {};   new val: {}".format(oldVal, s1[i]))

print("Now we iterate over the full sample, with the iterator interface.")
i = 0

wasError = False
try:
    s1[4]
except:
    wasError = True

if wasError == True:
    print("As we have expected an error occured.")
else:
    print("An error should have occured, but it dod not happen.")


