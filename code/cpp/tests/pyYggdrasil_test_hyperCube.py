import pyYggdrasil

# This should construct a 2 d unit hypercube
hc = pyYggdrasil.HyperCube(2)
print('The "default" consttucted interval is {}'.format(hc))

if(hc.isValid() == True):
    print("The generated cibe is valid as we have expected.")
else:
    raise ValueError("The cube is not valid, this si an error.")

# construct a hypercube from an interval
inter1 = pyYggdrasil.Interval((0.4, 5.0))
hc2 = pyYggdrasil.HyperCube(2, inter1)
print('The hypercube shoud have two dimensions, the intervals should be [0.4, 5[; in reality it is {}'.format(hc2))

wasError = False
try:
    hc2[1] = pyYggdrasil.Interval(-1.0, -0.5)
except:
    wasError = True

if(wasError == True):
    print("As expected, tring to assign an interval leads to an error")
else:
    print("The assignment did not produce an error, this is an error.")
    raise ValueError("No error generated")

wasError = False
try:
    hc2.setInvalidDimension(0, pyYggdrasil.Interval(-10.0, -9.0))
except:
    wasError = True
if(wasError == True):
    print("It was not possible to exchange a valid interval.")
else:
    raise ValueError("It was possible to exchange a valid interval, this is an error.")

# Now triy to generate an invalid interval
iHc = pyYggdrasil.CREAT_INVALID_CUBE(2)
if(iHc.isValid() == True):
    raise ValueError("The invalid constructed cube was valid, this is a serious error.")

iHc.setInvalidDimension(0, pyYggdrasil.Interval(-10.0, -9.0))
if(iHc.isValid(0) == False):
    raise ValueError("It was not possible to exchange the interval in the first dimension.")
if(iHc.isValid(1) == True):
    raise ValueError("The second dimension of the cube is not invalid as it should.")
if(iHc.isValid() == True):
    raise ValueError("The cube in its entierty is not valid, this is an error.");

# Try to set the cube again
wasError = False
try:
    iHc.setInvalidDimension(0, pyYggdrasil.Interval(-10.0, -3.0))
except:
    wasError = True
if(wasError == True):
    print("It was not possible to exchange a valid interval again.")
else:
    raise ValueError("It was possible to exchange a valid interval, this is an error.")

iHc.setInvalidDimension(1, pyYggdrasil.Interval(-100, -30))
if(iHc.isValid() == False):
    raise ValueError("The cube is not valid as it should be now.")

print("iHc = {}".format(iHc))








