import pyYggdrasil
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

def main():

    # Generate a domain [-5, 5[
    domainI  = pyYggdrasil.Interval(-5.0, 5.0)
    domainHC = pyYggdrasil.HyperCube(1, domainI)

    # The number of sample we want
    N = 10000

    # The distribution of the samples
    mu  = 0.0
    std = 1.0

    # Generate the sample
    sa = pyYggdrasil.SampleCollection(1)

    # This is a regular numpy array
    xSample = np.zeros(N)

    # This is a counter
    c = 0

    while (sa.nSamples() < N):

        # Propose a sample
        propSample = np.random.normal(mu, std)

        if(domainI.isInside(propSample)):
            s = pyYggdrasil.Sample(1, propSample)
            sa.addNewSample(s)
            xSample[c] = propSample
            c += 1
        # end if: sample was good
    #end: while

    # Make a builder
    b = pyYggdrasil.TreeBuilder(pyYggdrasil.eParModel.ConstModel).setGOFLevel(0.1)
    print("The used builder:")
    print(b)

    # Construct the tree
    tree = pyYggdrasil.DETree(sa, domainHC, b)

    # Construct a grid to plot
    Qn = 10 * N;
    xQ_np = np.linspace(-5.0, 5.0, num = Qn, endpoint = False)

    # Create a sample array for the latter evaluation
    saQ = pyYggdrasil.SampleArray(1, Qn)
    pdf_ex = np.zeros(Qn)
    for i in range(Qn):
        saQ[i] = pyYggdrasil.Sample(1, xQ_np[i])
        pdf_ex[i] = norm.pdf(xQ_np[i], mu, std)
    #end for(i)

    # Evaluating the grid
    pdf_est = tree.evaluateSamplesAt_noErr(saQ)



    # Plot the resiult
    plt.plot(xQ_np, pdf_ex, label="Exact pdf")
    plt.plot(xQ_np, pdf_est, label="Estimated pdf")
    plt.hist(xSample, bins='auto', normed=True)

    ax = plt.gca()

    plt.show()
    plt.savefig("testPlot.eps")




if __name__ == "__main__":
    main()













