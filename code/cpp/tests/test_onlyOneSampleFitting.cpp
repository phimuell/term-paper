/**
 * \brief	This is a test that only deals with one samples, but many times over.
 */


#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeImpl.hpp>

#include <para_models/yggdrasil_linModel.hpp>
#include <stat_tests/yggdrasil_IndepTest_chi2.hpp>
#include <stat_tests/yggdrasil_GOFTest_chi2.hpp>

#include <split_scheme/yggdrasil_sizeBasedSplitting.hpp>
#include <split_scheme/yggdrasil_scoreBasedSplitting.hpp>

#include <algorithm>
#include <tuple>
#include <random>

using Array_t = yggdrasil::yggdrasil_dimensionArray_t;
using Coll_t  = yggdrasil::yggdrasil_sampleCollection_t;
using SplitCom_t = yggdrasil::yggdrasil_singleSplit_t;
using SplitSeq_t = yggdrasil::yggdrasil_splitSequence_t;
using HyperCube_t = Coll_t::HyperCube_t;
using IntervalBound_t = HyperCube_t::IntervalBound_t;
using IndexArray_t = Coll_t::IndexArray_t;
using ValArray_t = Coll_t::ValueArray_t;
using Node_t  = yggdrasil::yggdrasil_DETNode_t;

using namespace yggdrasil;

std::mt19937_64 geni(42);

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


//This function test if the tree is good
bool
isTreeFullyValid(
	const yggdrasil_DETree_t& 	tree)
{
	auto it = tree.getLeafIterator();
	for(; it.isEnd() == false; it.nextLeaf())
	{
		const auto nf = it.access();

		if(nf.isFullySplittedLeaf() == false)
		{
			std::cerr << "The node is not fully splitted." << std::endl;
			return false;
		};
		if(nf.checkIntegrity() == false)
		{
			std::cerr << "The node failed the integrty check." << std::endl;
			return false;
		};
	}; //ENd for(it)


	return true;
}; //End tree


int
main()
{

	const int D = 2;

	//Create the tests
	std::unique_ptr<yggdrasil_GOFTest_i> gof = std::unique_ptr<yggdrasil_GOFTest_i>(new yggdrasil_Chi2GOFTest_t(D, 0.05));
	std::unique_ptr<yggdrasil_IndepTest_i> indep = std::unique_ptr<yggdrasil_IndepTest_i>(new yggdrasil_Chi2IndepTest_t(D, 0.05));
	std::unique_ptr<yggdrasil_parametricModel_i> parMod = std::unique_ptr<yggdrasil_parametricModel_i>(new yggdrasil_linearModel_t(D));
	std::unique_ptr<yggdrasil_splitFinder_i> splitFinder = std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil::yggdrasil_sizeSplit_t());


	//Test 3
	{
		const int N = 100;
		//std::uniform_real_distribution<double> dist(-10.0, 10.0);
		std::normal_distribution<double> dist(0, 1.0);
		const double lowB = -10.0;
		const double upB  =  10.0;
		const IntervalBound_t renze(lowB, upB);


		Coll_t c(D);

		for(int d = 0; d != D; ++d)
		{
			ValArray_t dd(N);
			for(int i = 0; i != N; ++i)
			{
				const double val = d == 0 ? 0.5 : 0.75;
				if(renze.isInside(val) == false)
				{
					i -= 1;
					continue;
				};

				dd.at(i) = val;
			}; //End for(i)

			c.getDim(d).copyAndLoadValueArray(dd);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
		};

		//We must construct out own domain, since the estimation will fail
		const HyperCube_t unitCube = HyperCube_t::MAKE_UNIT_CUBE(D);
		yggdrasil_DETree_t tree(c, unitCube, splitFinder, parMod, gof, indep, false);


		std::cout << "The node after construction:" << std::endl;
		tree.printToStream(std::cout, true) << "\n" << std::flush;

		if(isTreeFullyValid(tree) == false)
		{
			//Out is done in function
			std::abort();
		};
	}; //End test 3

	return 0;
}; //End: main






