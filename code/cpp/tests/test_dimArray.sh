#!/bin/bash
#
# This script compiles the tests for the tagged pointer
#######################

# Compile everything
# Is needed for the exceptions
g++ -std=c++11 -I../ -c ../core/yggdrasil_exception.cpp -o except.o

# Compile the test program
g++ -std=c++14 -O0 -g  -I../ -c ./test_dimArray.cpp  -o test_dimArray.o

# Link the program
g++ except.o test_dimArray.o -o test_dimArray.out

# run the prgram
./test_dimArray.out
if [ $? -eq 0 ]
then
	echo "No errors where detected."
else
	echo "Errors where detected"
	exit 1
fi

# No error where detected so cleaing
rm except.o test_dimArray.o test_dimArray.out





