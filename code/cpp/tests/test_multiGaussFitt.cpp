
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeImpl.hpp>

#include <para_models/yggdrasil_linModel.hpp>
#include <para_models/yggdrasil_constModel.hpp>
#include <stat_tests/yggdrasil_IndepTest_chi2.hpp>
#include <stat_tests/yggdrasil_GOFTest_chi2.hpp>

#include <split_scheme/yggdrasil_sizeBasedSplitting.hpp>
#include <split_scheme/yggdrasil_scoreBasedSplitting.hpp>

#include <algorithm>
#include <tuple>
#include <random>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_multiGauss.hpp>

#include <algorithm>
#include <tuple>
#include <random>
#include <chrono>

#include <Eigen/Dense>

using Array_t = yggdrasil::yggdrasil_dimensionArray_t;
using Coll_t  = yggdrasil::yggdrasil_sampleCollection_t;
using SplitCom_t = yggdrasil::yggdrasil_singleSplit_t;
using SplitSeq_t = yggdrasil::yggdrasil_splitSequence_t;
using HyperCube_t = Coll_t::HyperCube_t;
using IntervalBound_t = HyperCube_t::IntervalBound_t;
using IndexArray_t = Coll_t::IndexArray_t;
using ValArray_t = Coll_t::ValueArray_t;
using Node_t  = yggdrasil::yggdrasil_DETNode_t;


using namespace yggdrasil;

std::mt19937_64 geni(42);

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


using Gauss_t = ::yggdrasil::yggdrasil_multiDimGauss_t;


void
computeMeanAndStd(
	const Gauss_t::SampleArray_t&	samples,
	Gauss_t::CoVarMat_t&		coVar,
	Gauss_t::MeanVec_t&		mean)
{
	const auto nDims = samples.nDims();
	const auto nSamples = samples.nSamples();

	mean.resize(nDims);
	mean.setZero();

	coVar.resize(nDims, nDims);
	coVar.setZero();

	//Compute the mean
	for(Size_t i = 0; i != nSamples; ++i)
	{
		const Numeric_t* beginCurrSample = samples.beginSample(i);

		for(Size_t j = 0; j != nDims; ++j)
		{
			mean[j] += beginCurrSample[j];
		}; //End for(j):
	}; //End for(i):

	mean /= (Numeric_t)nSamples;

	for(Size_t k = 0; k != nSamples; ++k)
	{
		const Numeric_t* beginCurrSample = samples.beginSample(k);

		for(Size_t i = 0; i != nDims; ++i)
		{
			for(Size_t j = 0; j != nDims; ++j)
			{
				coVar(i, j) += (beginCurrSample[i] - mean[i]) * (beginCurrSample[j] - mean[j]);
			}; //ENd for(j):
		}; //ENd for(i)
	}; //End for(k):

	//Scalling
	//Is this approriate
	coVar /= (nSamples - 1);

	return;
}; //ENd compute the mean



bool
isTreeFullyValid(
	const yggdrasil_DETree_t& 	tree)
{
	if(tree.checkIntegrity() == false)
	{
		std::cerr << "The tree diod not pass the integrity check." << std::endl;
		return false;
	};


	yggdrasil_DETree_t::SubLeafIterator_t ende;
	for( auto it = tree.getLeafIterator(); it != ende; ++it)
	{
		const auto nf = *it;

		if(nf.isFullySplittedLeaf() == false)
		{
			std::cerr << "The node is not fully splitted." << std::endl;
			return false;
		};
		if(nf.checkIntegrity() == false)
		{
			std::cerr << "The node failed the integrty check." << std::endl;
			return false;
		};
	}; //ENd for(it)

	if(tree.isFullySplittedTree() == false)
	{
		std::cerr << "The tree did not appear to be fully fitted." << std::endl;
		return false;
	};


	return true;
}; //End tree






int
main()
{

	//These are the different sizes that we are testing
	Size_t diffSizes[5];
	diffSizes[0] = 100;
	for(int i = 1; i != 5; ++i)
	{
		diffSizes[i] = diffSizes[i - 1] * 10;
	};

	std::chrono::high_resolution_clock::time_point startT, endT;


	for(const Size_t N : diffSizes)
	{
		std::cout << "Handle Size: " << N << std::endl;

		// Test1
		{
			const Size_t D = 2;
			std::unique_ptr<yggdrasil_GOFTest_i> gof = std::unique_ptr<yggdrasil_GOFTest_i>(new yggdrasil_Chi2GOFTest_t(0.05));
			std::unique_ptr<yggdrasil_IndepTest_i> indep = std::unique_ptr<yggdrasil_IndepTest_i>(new yggdrasil_Chi2IndepTest_t(0.05));
			std::unique_ptr<yggdrasil_parametricModel_i> parMod = std::unique_ptr<yggdrasil_parametricModel_i>(new yggdrasil_linearModel_t("0"));
			std::unique_ptr<yggdrasil_splitFinder_i> splitFinder = std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil::yggdrasil_sizeSplit_t());

			Gauss_t::CoVarMat_t Sigma(2, 2);
			Sigma << 2, 0, 0, 2;

			Gauss_t::MeanVec_t Mu(2);
			Mu << 1.4, -4;

			//Create a gaus
			Gauss_t dist(Mu, Sigma);

			//Generate some samples
			Gauss_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

			HyperCube_t invalDom = HyperCube_t::MAKE_INVALID_CUBE();

			startT = std::chrono::high_resolution_clock::now();
			yggdrasil_DETree_t tree(std::move(Coll_t(samples)), invalDom, splitFinder, parMod, gof, indep);
			endT = std::chrono::high_resolution_clock::now();

			const auto timeSpent = endT - startT;

			std::cout << " fitting the 2D tree: " << std::chrono::duration_cast<::std::chrono::seconds>(timeSpent).count() << "s" << std::endl;


			if(isTreeFullyValid(tree) == false)
			{
				//Out is done in function
				std::abort();
			};



		}; //End test 1

		// Test1.2
		{
			const Size_t D = 2;
			std::unique_ptr<yggdrasil_GOFTest_i> gof = std::unique_ptr<yggdrasil_GOFTest_i>(new yggdrasil_Chi2GOFTest_t(0.05));
			std::unique_ptr<yggdrasil_IndepTest_i> indep = std::unique_ptr<yggdrasil_IndepTest_i>(new yggdrasil_Chi2IndepTest_t(0.05));
			std::unique_ptr<yggdrasil_parametricModel_i> parMod = std::unique_ptr<yggdrasil_parametricModel_i>(new yggdrasil_constantModel_t(0));
			std::unique_ptr<yggdrasil_splitFinder_i> splitFinder = std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil::yggdrasil_sizeSplit_t());

			Gauss_t::CoVarMat_t Sigma(2, 2);
			Sigma << 2, 0, 0, 2;

			Gauss_t::MeanVec_t Mu(2);
			Mu << 1.4, -4;

			//Create a gaus
			Gauss_t dist(Mu, Sigma);

			//Generate some samples
			Gauss_t::SampleList_t samples = dist.generateSamplesList(geni, N);

			//This is the repetition count
			const Size_t nRep = std::max<Size_t>(1, N * 0.01);
			std::uniform_int_distribution<Size_t> sSelect(0, N - 1);
			const Size_t maxCopies = ::std::min<Size_t>(10, 0.001 * N);
			std::uniform_int_distribution<Size_t> nCopyies(1, maxCopies);
			for(Size_t i = 0; i != nRep; ++i)
			{
				const Size_t idxToCopy = sSelect(geni);
				const Size_t nReplicas = (maxCopies == 0) ? 1 : nCopyies(geni);
				yggdrasil_sample_t src(samples[idxToCopy]);
				for(Size_t j = 0; j != nReplicas; ++j)
				{
					samples.push_back(src);
				};
			}; //End for(i)

			const Size_t nAdded = samples.size() - N;

			std::cerr << " -- Added sampel: " << nAdded << " " << (100.0 * nAdded / N) << "%" << std::endl;

			Coll_t sColl(samples);


			HyperCube_t invalDom = HyperCube_t::MAKE_INVALID_CUBE();

			startT = std::chrono::high_resolution_clock::now();
			yggdrasil_DETree_t tree(sColl, invalDom, splitFinder, parMod, gof, indep, false);
			endT = std::chrono::high_resolution_clock::now();

			const auto timeSpent = endT - startT;

			std::cout << " fitting the 2D tree: " << std::chrono::duration_cast<::std::chrono::seconds>(timeSpent).count() << "s" << std::endl;


			if(isTreeFullyValid(tree) == false)
			{
				//Out is done in function
				std::abort();
			};
		}; //End test 1




		//Test 2
		{
			const Size_t D = 4;
			std::unique_ptr<yggdrasil_GOFTest_i> gof = std::unique_ptr<yggdrasil_GOFTest_i>(new yggdrasil_Chi2GOFTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_IndepTest_i> indep = std::unique_ptr<yggdrasil_IndepTest_i>(new yggdrasil_Chi2IndepTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_parametricModel_i> parMod = std::unique_ptr<yggdrasil_parametricModel_i>(new yggdrasil_linearModel_t(D));
			std::unique_ptr<yggdrasil_splitFinder_i> splitFinder = std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil::yggdrasil_sizeSplit_t());


			Gauss_t::CoVarMat_t Sigma(4, 4);
			Sigma
				<< 1.0, -0.344, 0.141, -0.486,
				-0.344, 1, 0.586, 0.244,
				0.141, 0.586, 1, -0.544,
				-0.486, 0.244, -0.544, 1;

			Gauss_t::MeanVec_t Mu(4);
			Mu << 1.4, -4, 0, 3;


			//Create a gaus
			Gauss_t dist(Mu, Sigma);

			//Generate some samples
			Gauss_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

			HyperCube_t invalDom = HyperCube_t::MAKE_INVALID_CUBE();

			startT = std::chrono::high_resolution_clock::now();
			yggdrasil_DETree_t tree(std::move(Coll_t(samples)), invalDom, splitFinder, parMod, gof, indep);
			endT = std::chrono::high_resolution_clock::now();

			const auto timeSpent = endT - startT;

			std::cout << " fitting the 4D tree: " << std::chrono::duration_cast<::std::chrono::seconds>(timeSpent).count() << "s" << std::endl;


			if(isTreeFullyValid(tree) == false)
			{
				//Out is done in function
				std::abort();
			};
		}; //End: Test2


		// Test2.2
		{
			const Size_t D = 4;
			std::unique_ptr<yggdrasil_GOFTest_i> gof = std::unique_ptr<yggdrasil_GOFTest_i>(new yggdrasil_Chi2GOFTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_IndepTest_i> indep = std::unique_ptr<yggdrasil_IndepTest_i>(new yggdrasil_Chi2IndepTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_parametricModel_i> parMod = std::unique_ptr<yggdrasil_parametricModel_i>(new yggdrasil_linearModel_t("0"));
			std::unique_ptr<yggdrasil_splitFinder_i> splitFinder = std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil::yggdrasil_sizeSplit_t());

			Gauss_t::CoVarMat_t Sigma(4, 4);
			Sigma
				<< 1.0, -0.344, 0.141, -0.486,
				-0.344, 1, 0.586, 0.244,
				0.141, 0.586, 1, -0.544,
				-0.486, 0.244, -0.544, 1;

			Gauss_t::MeanVec_t Mu(4);
			Mu << 1.4, -4, 0, 3;

			//Create a gaus
			Gauss_t dist(Mu, Sigma);

			//Generate some samples
			Gauss_t::SampleList_t samples = dist.generateSamplesList(geni, N);

			//This is the repetition count
			const Size_t nRep = std::max<Size_t>(1, N * 0.01);
			std::uniform_int_distribution<Size_t> sSelect(0, N - 1);
			const Size_t maxCopies = ::std::min<Size_t>(10, 0.001 * N);
			std::uniform_int_distribution<Size_t> nCopyies(1, maxCopies);
			for(Size_t i = 0; i != nRep; ++i)
			{
				const Size_t idxToCopy = sSelect(geni);
				const Size_t nReplicas = (maxCopies == 0) ? 1 : nCopyies(geni);
				yggdrasil_sample_t src(samples[idxToCopy]);
				for(Size_t j = 0; j != nReplicas; ++j)
				{
					samples.push_back(src);
				};
			}; //End for(i)

			const Size_t nAdded = samples.size() - N;

			std::cerr << " -- Added sampel: " << nAdded << " " << (100.0 * nAdded / N) << "%" << std::endl;

			Coll_t sColl(samples);


			HyperCube_t invalDom = HyperCube_t::MAKE_INVALID_CUBE();

			startT = std::chrono::high_resolution_clock::now();
			yggdrasil_DETree_t tree(sColl, invalDom, splitFinder, parMod, gof, indep, false);
			endT = std::chrono::high_resolution_clock::now();

			const auto timeSpent = endT - startT;

			std::cout << " fitting the 4D tree: " << std::chrono::duration_cast<::std::chrono::seconds>(timeSpent).count() << "s" << std::endl;


			if(isTreeFullyValid(tree) == false)
			{
				//Out is done in function
				std::abort();
			};
		}; //End test 2.2



		// Test2.3
		{
			const Size_t D = 4;
			std::unique_ptr<yggdrasil_GOFTest_i> gof = std::unique_ptr<yggdrasil_GOFTest_i>(new yggdrasil_Chi2GOFTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_IndepTest_i> indep = std::unique_ptr<yggdrasil_IndepTest_i>(new yggdrasil_Chi2IndepTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_parametricModel_i> parMod = std::unique_ptr<yggdrasil_parametricModel_i>(new yggdrasil_linearModel_t("0"));
			std::unique_ptr<yggdrasil_splitFinder_i> splitFinder = std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil::yggdrasil_sizeSplit_t());

			Gauss_t::CoVarMat_t Sigma(4, 4);
			Sigma
				<< 1.0, -0.344, 0.141, -0.486,
				-0.344, 1, 0.586, 0.244,
				0.141, 0.586, 1, -0.544,
				-0.486, 0.244, -0.544, 1;

			Gauss_t::MeanVec_t Mu(4);
			Mu << 1.4, -4, 0, 3;

			//Create a gaus
			Gauss_t dist(Mu, Sigma);

			//Generate some samples
			Gauss_t::SampleList_t samples = dist.generateSamplesList(geni, N);

			Size_t w = 0;

			//This is the repetition count
			const Size_t nRep = std::max<Size_t>(1, N * 0.03);
			std::uniform_int_distribution<Size_t> sSelect(0, N - 1);
			const Size_t maxCopies = ::std::min<Size_t>(10, std::max<Size_t>(1, N * 0.001));
			std::uniform_int_distribution<Size_t> nCopyies(1, maxCopies);
			std::uniform_int_distribution<Size_t> dimSepect(0, D - 1);
			for(Size_t i = 0; i != nRep; ++i)
			{
				const Size_t idxToCopy = sSelect(geni);
				const Size_t nReplicas = (maxCopies == 0) ? 1 : nCopyies(geni);
				yggdrasil_sample_t src(samples[idxToCopy]);

				const Size_t selectedDim = dimSepect(geni);
				for(Size_t j = 0; j != nReplicas; ++j)
				{
					const Size_t dIdx = sSelect(geni);
					samples[dIdx][selectedDim] = src[selectedDim];
					w += 1;
				};
			}; //End for(i)

			//const Size_t nAdded = samples.size() - N;
			std::cout << " --- Treated samples: " << w << " " << (100.0 * w / N) << "%" << std::endl;

			//std::cerr << " -- Added sampel: " << nAdded << " " << (100.0 * nAdded / N) << "%" << std::endl;

			Coll_t sColl(samples);


			HyperCube_t invalDom = HyperCube_t::MAKE_INVALID_CUBE();

			startT = std::chrono::high_resolution_clock::now();
			yggdrasil_DETree_t tree(sColl, invalDom, splitFinder, parMod, gof, indep, false);
			endT = std::chrono::high_resolution_clock::now();

			const auto timeSpent = endT - startT;

			std::cout << " fitting the 4D tree: " << std::chrono::duration_cast<::std::chrono::seconds>(timeSpent).count() << "s" << std::endl;


			if(isTreeFullyValid(tree) == false)
			{
				//Out is done in function
				std::abort();
			};



		}; //End test 1
	}; //End for(N)


	return 0;
}; //End: main






