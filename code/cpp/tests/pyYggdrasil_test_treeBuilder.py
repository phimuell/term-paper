import pyYggdrasil

b = pyYggdrasil.TreeBuilder()
print("{}".format(b))

if(b.hasParModel() == True):
    raise ValueError("The builder has a model, but it should have no model.")

b.setGOFLevel(0.1).setIndepLevel(0.2)

# test if the changes has taken effect
if(b.getGOFLevel() != 0.1):
    raise ValueError("THe setting of the gof level did not work. the level was {}, instead of {}".format(b.getGOFLevel(), 0.1))

if(b.getIndepLevel() != 0.2):
    raise ValueError("THe setting of the indep level did not work. the level was {}, instead of {}".format(b.getIndepLevel(), 0.2))

if(b.isMedianSplitter() == False):
    raise ValueError("The median splitter is not selected.")

b.useConstantModel().useSizeSplitter()

if(b.isSizeSplitter() == False):
    raise ValueError("the constant splitter is not selected.")
if(b.useConstantModel() == False):
    raise ValueError("The consatnt model is not selected.")

print("The builder after the cahnges:")
print(b)



