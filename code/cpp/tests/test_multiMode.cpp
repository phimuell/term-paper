

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_multiModalDistribution.hpp>
#include <random/yggdrasil_multiGauss.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <algorithm>
#include <tuple>
#include <random>

#include <Eigen/Dense>


using namespace yggdrasil;

std::mt19937_64 geni(42);

using HyperCube_t = yggdrasil_hyperCube_t;
using IntervalBound_t = HyperCube_t::IntervalBound_t;

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


using Distribution_t = ::yggdrasil::yggdrasil_multiModalDistribution_t;
using Gauss_t        = ::yggdrasil::yggdrasil_multiDimGauss_t;




//This is for computing the relative error
template<
	class T
	>
Numeric_t
relError(
	const T& 	estimated,
	const T&	expected)
{
	const T difference = estimated - expected;

	if(expected.norm() < yggdrasil::Constants::EPSILON * 100)
	{
		//We only consider the estimated
		return estimated.norm();
	}; //End if: the expected is too small

	/*
	std::cerr << "\nDIFF NORM: " << difference.norm() << "\n";
	std::cerr << "DIFF MAX COEF: " << difference.array().abs().maxCoeff() << "\n";
	std::cerr << "EXP NORM:    " << expected.norm() << "\n";
	std::cerr << "DIF: " << difference << "\n";
	*/

	//return (difference.array().abs().maxCoeff() / expected.array().abs().maxCoeff());
	return (difference.norm() / expected.norm());
};









int
main()
{
	// Test1
	{
		const Size_t N = 100000;

		Distribution_t::CoVarMat_t Sigma(1, 1);
		Sigma << 1;

		Distribution_t::MeanVec_t Mu(1);
		Mu << 0.5;

		//Create a first gauss
		Gauss_t dist1(Mu, Sigma, HyperCube_t(1, IntervalBound_t(-5.0, 5.0)));

		//Change mu
		Mu << (-0.5);

		//Create a gauss
		Gauss_t dist2(Mu, Sigma, HyperCube_t(1, IntervalBound_t(-5.0, 5.0)));

		//Create the weight wector
		Distribution_t::WeightVector_t W = {0.5};

		//Generate the distribution
		Distribution_t::ModeVector_t M;
		M.push_back(dist1.clone());
		M.push_back(dist2.clone());

		//Create the distribution
		Distribution_t dist(M, W);

		//Generate the samples
		const Distribution_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

		//The teoretic values
		Distribution_t::MeanVec_t  theoMean  = dist.getMean();
		Distribution_t::CoVarMat_t theoCoVar = dist.getCoVar();


		//Calculate
		Distribution_t::MeanVec_t estMean;
		Distribution_t::CoVarMat_t estCoVar;
		std::tie(estMean, estCoVar) = yggdrasil_estinmateStatistic(samples);

		const Numeric_t relErrCoVar = relError(estCoVar, theoCoVar);
		const Numeric_t relErrMean  = relError(estMean,  theoMean);

		std::cerr
			<< "The estimated mean:\n"
			<< estMean.transpose()
			<< "\n"
			<< "\n"
			<< "\nTheo mean:\n"
			<< theoMean.transpose() << "\n"
			<< "Rel Error: " << relErrMean
			<< "\n\n\n";

		std::cerr
			<< "The estimated covariance:\n"
			<< estCoVar
			<< "\n"
			<< "\nTheo CoVar:\n"
			<< theoCoVar << "\n"
			<< "RelError: " << relErrCoVar
			<< "\n\n"
			<< std::endl;


		if(relErrMean >= 0.05)
		{
			std::cerr
				<< "Relative error of mean, " << relErrMean << ", was too high"
				<< std::endl;
			std::abort();
		};
		if(relErrCoVar>= 0.05)
		{
			std::cerr
				<< "Relative error of covar, " << relErrCoVar << ", was too high"
				<< std::endl;
			std::abort();
		};
	}; //End test 1

	// Test2
	{
		const Size_t N = 100000;

		Distribution_t::CoVarMat_t Sigma(2, 2);
		Sigma << 4.0, -2.28, -2.28, 1.44;

		Distribution_t::MeanVec_t Mu1(2), Mu2(2), Mu3(2), Mu4(2);
		Mu1 << 3.0, 1.0;
		Mu2 << 3.0, 3.0;
		Mu3 << 1.0, 3.0;
		Mu4 << 1.0, 1.0;

		HyperCube_t hc(2, IntervalBound_t(-7.0, 13.0));

		//Create the four modes
		Gauss_t dist1(Mu1, Sigma, hc);
		Gauss_t dist2(Mu2, Sigma, hc);
		Gauss_t dist3(Mu3, Sigma, hc);
		Gauss_t dist4(Mu4, Sigma, hc);

		//Create the weight wector
		Distribution_t::WeightVector_t W = {0.25, 0.25, 0.25};

		//Generate the distribution
		Distribution_t::ModeVector_t M;
		M.push_back(dist1.clone());
		M.push_back(dist2.clone());
		M.push_back(dist3.clone());
		M.push_back(dist4.clone());

		//Create the distribution
		Distribution_t dist(M, W);

		//Generate the samples
		const Distribution_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

		//The teoretic values
		Distribution_t::MeanVec_t  theoMean  = dist.getMean();
		Distribution_t::CoVarMat_t theoCoVar = dist.getCoVar();


		//Calculate
		Distribution_t::MeanVec_t estMean;
		Distribution_t::CoVarMat_t estCoVar;
		std::tie(estMean, estCoVar) = yggdrasil_estinmateStatistic(samples);

		const Numeric_t relErrCoVar = relError(estCoVar, theoCoVar);
		const Numeric_t relErrMean  = relError(estMean,  theoMean);

		std::cerr
			<< "The estimated mean:\n"
			<< estMean.transpose()
			<< "\n"
			<< "\n"
			<< "\nTheo mean:\n"
			<< theoMean.transpose() << "\n"
			<< "Rel Error: " << relErrMean
			<< "\n\n\n";

		std::cerr
			<< "The estimated covariance:\n"
			<< estCoVar
			<< "\n"
			<< "\nTheo CoVar:\n"
			<< theoCoVar << "\n"
			<< "RelError: " << relErrCoVar
			<< "\n\n"
			<< std::endl;


		if(relErrMean >= 0.05)
		{
			std::cerr
				<< "Relative error of mean, " << relErrMean << ", was too high"
				<< std::endl;
			std::abort();
		};
		if(relErrCoVar>= 0.05)
		{
			std::cerr
				<< "Relative error of covar, " << relErrCoVar << ", was too high"
				<< std::endl;
			std::abort();
		};
	}; //End test 2
	return 0;
}; //End: main






