import pyYggdrasil

# This should construct an unit interval
inter = pyYggdrasil.Interval()
print('The "default" consttucted interval is {}'.format(inter))
print('Manually access the bound gives [{}, {}['.format(inter.lower(), inter.upper()))

# construct from a pair or is it a tuple
inter1 = pyYggdrasil.Interval((0.4, 5.0))
print('The interval constructed from tuple, it should be [0.4, 5.0[ and is {}'.format(inter1))





