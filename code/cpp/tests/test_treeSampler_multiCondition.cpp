/**
 * \brief	This is a test for the tree sampler, it is a very simple test.
 * 		 This tests the multicondition and especially the restriction.
 */

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeImpl.hpp>

#include <para_models/yggdrasil_linModel.hpp>
#include <para_models/yggdrasil_constModel.hpp>
#include <stat_tests/yggdrasil_IndepTest_chi2.hpp>
#include <stat_tests/yggdrasil_GOFTest_chi2.hpp>

#include <split_scheme/yggdrasil_sizeBasedSplitting.hpp>
#include <split_scheme/yggdrasil_scoreBasedSplitting.hpp>

#include <tree_geni/yggdrasil_multiDimCondition.hpp>

#include <algorithm>
#include <tuple>
#include <random>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_multiGauss.hpp>

#include <algorithm>
#include <tuple>
#include <random>
#include <chrono>

#include <Eigen/Dense>

using Array_t = yggdrasil::yggdrasil_dimensionArray_t;
using Coll_t  = yggdrasil::yggdrasil_sampleCollection_t;
using SplitCom_t = yggdrasil::yggdrasil_singleSplit_t;
using SplitSeq_t = yggdrasil::yggdrasil_splitSequence_t;
using HyperCube_t = Coll_t::HyperCube_t;
using IntervalBound_t = HyperCube_t::IntervalBound_t;
using IndexArray_t = Coll_t::IndexArray_t;
using ValArray_t = Coll_t::ValueArray_t;
using Node_t  = yggdrasil::yggdrasil_DETNode_t;
using Builder_t = yggdrasil::yggdrasil_treeBuilder_t;
using SampleCollection_t = yggdrasil::yggdrasil_sampleCollection_t;
using Tree_t = yggdrasil::yggdrasil_DETree_t;


using namespace yggdrasil;

std::mt19937_64 geni(42);

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


using Gauss_t = ::yggdrasil::yggdrasil_multiDimGauss_t;







int
main(
	int 	argc,
	char** 	argv)
{
	//This is the test parameters
	const int D = 10;	//Numbers of the dimensions we whant to apply
	const int N = 4;	//This is the number of samples we put into the container.

	/*
	 * Generate the map that we will use for the restriction
	 * It has 10 dimensions
	 */
	std::vector<int> trueFreeDims 	  = {0,       3, 4,    6, 7,    9};
	std::vector<int> trueRestrictDims = {   1, 2,       5,       8   };
	yggdrasil_multiCondition_t::ConditionMap_t cMap;

	//This is a test sample that is used
	//Note that its values of restricted dimensions are not correct.
	yggdrasil_sample_t trueMasterSample(D);
	for(int i = 0; i != D; ++i)
	{
		trueMasterSample[i] = i;
	}; //End for(i):

	//this is the sample that is the true restriction of the master sample
	yggdrasil_sample_t trueRestrictedSample(trueFreeDims.size(), -1.0);	//Is for reference
	for(Size_t i = 0; i != trueFreeDims.size(); ++i)
	{
		trueRestrictedSample.at(i) = trueMasterSample.at(trueFreeDims.at(i));
	}; //End for(i):

	if(trueRestrictedSample.size() != trueFreeDims.size())
	{
		std::cerr
			<< "The restricted sample has the wrong dimension." << std::endl;
		std::abort();
	};

	//create the restruiction
	for(const int d : trueRestrictDims)
	{
		cMap[d] = -d;
	}; //End for(i):

	//Create the multi condition
	yggdrasil_multiCondition_t multiCondition(D, cMap);

	//Make a sample container
	yggdrasil_arraySample_t trueMasterSampleContainer(D, N);
	yggdrasil_arraySample_t trueRestrictedSampleContainer(trueFreeDims.size(), N);
	for(int j = 0; j != N; ++j)
	{
		trueMasterSampleContainer.setSampleTo(trueMasterSample, j);
		trueRestrictedSampleContainer.setSampleTo(trueRestrictedSample, j);
	}; //End for(j):


	//Test 1
	{
		/*
		 * Test if the free dimensions are extracted correctly
		 */
		auto testFreeDims = multiCondition.getFreeDimensions();

		//Sort them in case they are not sorted there is no guarantee
		std::sort(testFreeDims.begin(), testFreeDims.end());

		if(testFreeDims.size() != trueFreeDims.size())
		{
			std::cerr
				<< "In reality there are " << trueFreeDims.size() << " many free dimensions, but only " << testFreeDims.size() << " where found." << std::endl;
			std::abort();
		};

		for(Size_t i = 0; trueFreeDims.size() != i; ++i)
		{
			if(testFreeDims.at(i) != trueFreeDims.at(i))
			{
				std::cerr
					<< "The free dimension with index " << i << " is not correct." << std::endl;
				std::abort();
			};
		}; //End for
	};
	// End Test1

	//Test 2
	{
		/*
		 * Test if the restricted dimensions are the correct ones
		 */
		auto testRestrictDims = multiCondition.getConditionedDimensions();
		std::sort(testRestrictDims.begin(), testRestrictDims.end()); //Ensure that they are sorted

		if(testRestrictDims.size() != trueRestrictDims.size())
		{
			std::cerr
				<< "The number of restricted dimensions is wrong." << std::endl;
		};

		for(Size_t j = 0; j != trueRestrictDims.size(); ++j)
		{
			if(trueRestrictDims.at(j) != testRestrictDims.at(j))
			{
				std::cerr
					<< "A wrong dimension was found in the restricted dimnension." << std::endl;
			};
		}; //End for(j):
	};
	//End Test2


	//Test 3
	{
		/*
		 * Test if the sample is restricted correctly and also if it is expanded correctly
		 */
		const auto testRestrictedSample = multiCondition.restrictSample(trueMasterSample);

		if(testRestrictedSample.size() != trueFreeDims.size())
		{
			std::cerr
				<< "The dimension of the restricted sample is wwrong (I)." << std::endl;
			std::abort();
		};
		if(testRestrictedSample.size() != trueRestrictedSample.size())
		{
			std::cerr
				<< "The dimension of the restricted sample is wwrong (II)." << std::endl;
			std::abort();
		};

		//Test if the values of the samples are correct
		for(Size_t i = 0; i != trueRestrictedSample.size(); ++i)
		{
			if(trueRestrictedSample.at(i) != testRestrictedSample.at(i))
			{
				std::cerr
					<< "The value of the restricted sample is wrong (I)" << std::endl;
				std::abort();
			};

			if((Numeric_t)trueFreeDims.at(i) != testRestrictedSample.at(i))
			{
				std::cerr
					<< "The value of the restricted sample is wrong (II)" << std::endl;
				std::abort();
			};

		}; //End for(i):

		//Test the expanding
		//This will not restore the master sample
		const auto testRestoredSample = multiCondition.expandSample(testRestrictedSample);

		if(testRestoredSample.nDims() != D)
		{
			std::cerr
				<< "The dimension of the restricted dimension is wrong." << std::endl;
			std::abort();
		};

		//Test the free dimensions
		for(Size_t j = 0; j != trueFreeDims.size(); ++j)
		{
			const auto J = trueFreeDims.at(j);
			if(trueMasterSample.at(J) != testRestoredSample.at(J))
			{
				std::cerr
					<< "Error in the free dimension part of teh restored sample." << std::endl;
				std::abort();
			};
		}; //End for(J): test the free dimensions of teh restored sample

		//Test the restricted dimensions
		for(Size_t j = 0; j != trueRestrictDims.size(); ++j)
		{
			const auto J = trueRestrictDims.at(j);
			const auto trueValue = -(Numeric_t(J));
			if(trueValue != testRestoredSample.at(J))
			{
				std::cerr
					<< "Error in the restricted dimension part of the restored sample." << std::endl;
				std::abort();
			};
		}; //End for(j): test the restricted dimensions of the restored samples
	};
	//End test 3


	//Test 4
	{
		/*
		 * Test if the sample is restricted correctly and also if it is expanded correctly
		 * This time on the sample container
		 *
		 * 	This code is copied from the single sample test but was addapted
		 */
		const auto testRestrictedSampleContainer = multiCondition.restrictSample(trueMasterSampleContainer);
		const yggdrasil_sample_t testRestrictedSample = testRestrictedSampleContainer.getSample(0);

		if(testRestrictedSample.size() != trueFreeDims.size())
		{
			std::cerr
				<< "The dimension of the restricted sample is wwrong (I)." << std::endl;
			std::abort();
		};
		if(testRestrictedSample.size() != trueRestrictedSample.size())
		{
			std::cerr
				<< "The dimension of the restricted sample is wwrong (II)." << std::endl;
			std::abort();
		};

		//Test if the values of the samples are correct
		for(Size_t i = 0; i != trueRestrictedSample.size(); ++i)
		{
			if(trueRestrictedSample.at(i) != testRestrictedSample.at(i))
			{
				std::cerr
					<< "The value of the restricted sample is wrong (I)" << std::endl;
				std::abort();
			};

			if((Numeric_t)trueFreeDims.at(i) != testRestrictedSample.at(i))
			{
				std::cerr
					<< "The value of the restricted sample is wrong (II)" << std::endl;
				std::abort();
			};

		}; //End for(i):

		//Test the expanding
		//This will not restore the master sample
		const auto testRestoredSampleContainer = multiCondition.expandSample(testRestrictedSampleContainer);
		const yggdrasil_sample_t testRestoredSample = testRestoredSampleContainer.getSample(0);

		if(testRestoredSample.nDims() != D)
		{
			std::cerr
				<< "The dimension of the restricted dimension is wrong." << std::endl;
			std::abort();
		};

		//Test the free dimensions
		for(Size_t j = 0; j != trueFreeDims.size(); ++j)
		{
			const auto J = trueFreeDims.at(j);
			if(trueMasterSample.at(J) != testRestoredSample.at(J))
			{
				std::cerr
					<< "Error in the free dimension part of teh restored sample." << std::endl;
				std::abort();
			};
		}; //End for(J): test the free dimensions of teh restored sample

		//Test the restricted dimensions
		for(Size_t j = 0; j != trueRestrictDims.size(); ++j)
		{
			const auto J = trueRestrictDims.at(j);
			const auto trueValue = -(Numeric_t(J));
			if(trueValue != testRestoredSample.at(J))
			{
				std::cerr
					<< "Error in the restricted dimension part of the restored sample." << std::endl;
				std::abort();
			};
		}; //End for(j): test the restricted dimensions of the restored samples
	};
	//End test 3


	return 0;
}; //End: main






