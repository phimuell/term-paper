

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_multiGauss.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <algorithm>
#include <tuple>
#include <random>

#include <Eigen/Dense>


using namespace yggdrasil;

std::mt19937_64 geni(42);

using HyperCube_t = yggdrasil_hyperCube_t;

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


using Gauss_t = ::yggdrasil::yggdrasil_multiDimGauss_t;


void
computeMeanAndStd(
	const Gauss_t::SampleArray_t&	samples,
	Gauss_t::CoVarMat_t&		coVar,
	Gauss_t::MeanVec_t&		mean)
{
	const auto nDims = samples.nDims();
	const auto nSamples = samples.nSamples();

	mean.resize(nDims);
	mean.setZero();

	coVar.resize(nDims, nDims);
	coVar.setZero();

	//Compute the mean
	for(Size_t i = 0; i != nSamples; ++i)
	{
		const Numeric_t* beginCurrSample = samples.beginSample(i);

		for(Size_t j = 0; j != nDims; ++j)
		{
			mean[j] += beginCurrSample[j];
		}; //End for(j):
	}; //End for(i):

	mean /= (Numeric_t)nSamples;

	for(Size_t k = 0; k != nSamples; ++k)
	{
		const Numeric_t* beginCurrSample = samples.beginSample(k);

		for(Size_t i = 0; i != nDims; ++i)
		{
			for(Size_t j = 0; j != nDims; ++j)
			{
				coVar(i, j) += (beginCurrSample[i] - mean[i]) * (beginCurrSample[j] - mean[j]);
			}; //ENd for(j):
		}; //ENd for(i)
	}; //End for(k):

	//Scalling
	//Is this approriate
	coVar /= (nSamples - 1);

	return;
}; //ENd compute the mean



//This is for computing the relative error
template<
	class T
	>
Numeric_t
relError(
	const T& 	estimated,
	const T&	expected)
{
	const T difference = estimated - expected;

	/*
	std::cerr << "\nDIFF NORM: " << difference.norm() << "\n";
	std::cerr << "DIFF MAX COEF: " << difference.array().abs().maxCoeff() << "\n";
	std::cerr << "EXP NORM:    " << expected.norm() << "\n";
	std::cerr << "DIF: " << difference << "\n";
	*/

	//return (difference.array().abs().maxCoeff() / expected.array().abs().maxCoeff());
	return (difference.norm() / expected.norm());
};









int
main()
{
	// Test1
	{
		Gauss_t::CoVarMat_t Sigma(2, 2);
		Sigma << 2, 0, 0, 2;

		Gauss_t::MeanVec_t Mu(2);
		Mu << 1.4, -4;

		const Size_t N = 100000;

		//Create a gaus
		Gauss_t dist(Mu, Sigma);

		//Generate some samples
		Gauss_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

		//This are the estimated params
		Gauss_t::MeanVec_t estMean;
		Gauss_t::CoVarMat_t estCoVar;

		//Calculate
		computeMeanAndStd(samples, estCoVar, estMean);

		const auto relErrMean  = relError(estMean, Mu);
		const auto relErrCoVar = relError(estCoVar, Sigma);

		std::cerr
			<< "The mean:\n"
			<< estMean.transpose()
			<< "\n"
			<< "Rel Error: " << relErrMean
			<< "\n\n";

		std::cerr
			<< "The estimated covariance:\n"
			<< estCoVar
			<< "\n"
			<< "RelError: " << relErrCoVar
			<< std::endl;


		if(relErrMean >= 0.05)
		{
			std::cerr
				<< "Relative error of mean, " << relErrMean << ", was too high"
				<< std::endl;
			std::abort();
		};
		if(relErrCoVar>= 0.05)
		{
			std::cerr
				<< "Relative error of covar, " << relErrCoVar << ", was too high"
				<< std::endl;
			std::abort();
		};
	}; //End test 1

	// Test1.1
	{
		Gauss_t::CoVarMat_t Sigma(2, 2);
		Sigma << 1, 0, 0, 1;

		Gauss_t::MeanVec_t Mu(2);
		Mu << 0.5, 0.5;

		const Size_t N = 100;

		//Domain
		HyperCube_t unitCube = HyperCube_t::MAKE_UNIT_CUBE(2);

		//Create a gaus
		Gauss_t dist(Mu, Sigma, unitCube);

		//Generate some samples
		Gauss_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

		for(Size_t i = 0; i != N; ++i)
		{
			if(unitCube.isInside(samples[i]) == false)
			{
				std::cerr << "Samples outside the unit cube were generated."
					<< std::endl;
				std::abort();
			};
		};
	}; //End test 1


	//Test 2
	{
		Gauss_t::CoVarMat_t Sigma(4, 4);
		Sigma
			<< 1.0, -0.344, 0.141, -0.486,
			-0.344, 1, 0.586, 0.244,
			0.141, 0.586, 1, -0.544,
			-0.486, 0.244, -0.544, 1;

		Gauss_t::MeanVec_t Mu(4);
		Mu << 1.4, -4, 0, 3;

		const Size_t N = 100000;

		//Create a gaus
		Gauss_t dist(Mu, Sigma);

		//Generate some samples
		Gauss_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

		//This are the estimated params
		Gauss_t::MeanVec_t estMean;
		Gauss_t::CoVarMat_t estCoVar;

		//Calculate
		computeMeanAndStd(samples, estCoVar, estMean);

		const auto relErrMean  = relError(estMean, Mu);
		const auto relErrCoVar = relError(estCoVar, Sigma);

		std::cerr
			<< "The mean:\n"
			<< estMean.transpose()
			<< "\n"
			<< "Rel Error: " << relErrMean
			<< "\n\n";

		std::cerr
			<< "The estimated covariance:\n"
			<< estCoVar
			<< "\n"
			<< "RelError: " << relErrCoVar
			<< std::endl;


		if(relErrMean >= 0.05)
		{
			std::cerr
				<< "Relative error of mean, " << relErrMean << ", was too high"
				<< std::endl;
			std::abort();
		};
		if(relErrCoVar>= 0.05)
		{
			std::cerr
				<< "Relative error of covar, " << relErrCoVar << ", was too high"
				<< std::endl;
			std::abort();
		};

	}; //End: Test2


	return 0;
}; //End: main






