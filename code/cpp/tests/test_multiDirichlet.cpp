

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_multiDirichle.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <algorithm>
#include <tuple>
#include <random>

#include <Eigen/Dense>


using namespace yggdrasil;

std::mt19937_64 geni(42);

using HyperCube_t = yggdrasil_hyperCube_t;

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


using Dirichlet_t = ::yggdrasil::yggdrasil_multiDimDirichlet_t;


void
computeMeanAndStd(
	const Dirichlet_t::SampleArray_t&	samples,
	Dirichlet_t::CoVarMat_t&		coVar,
	Dirichlet_t::MeanVec_t&		mean)
{
	const auto nDims = samples.nDims();
	const auto nSamples = samples.nSamples();

	mean.resize(nDims);
	mean.setZero();

	coVar.resize(nDims, nDims);
	coVar.setZero();

	//Compute the mean
	for(Size_t i = 0; i != nSamples; ++i)
	{
		const Numeric_t* beginCurrSample = samples.beginSample(i);

		for(Size_t j = 0; j != nDims; ++j)
		{
			mean[j] += beginCurrSample[j];
		}; //End for(j):
	}; //End for(i):

	mean /= (Numeric_t)nSamples;

	for(Size_t k = 0; k != nSamples; ++k)
	{
		const Numeric_t* beginCurrSample = samples.beginSample(k);

		for(Size_t i = 0; i != nDims; ++i)
		{
			for(Size_t j = 0; j != nDims; ++j)
			{
				coVar(i, j) += (beginCurrSample[i] - mean[i]) * (beginCurrSample[j] - mean[j]);
			}; //ENd for(j):
		}; //ENd for(i)
	}; //End for(k):

	//Scalling
	//Is this approriate
	coVar /= (nSamples - 1);

	return;
}; //ENd compute the mean



//This is for computing the relative error
template<
	class T
	>
Numeric_t
relError(
	const T& 	estimated,
	const T&	expected)
{
	const T difference = estimated - expected;

	/*
	std::cerr << "\nDIFF NORM: " << difference.norm() << "\n";
	std::cerr << "DIFF MAX COEF: " << difference.array().abs().maxCoeff() << "\n";
	std::cerr << "EXP NORM:    " << expected.norm() << "\n";
	std::cerr << "DIF: " << difference << "\n";
	*/

	//return (difference.array().abs().maxCoeff() / expected.array().abs().maxCoeff());
	return (difference.norm() / expected.norm());
};









int
main()
{
	// Test1
	{
		const Size_t N = 1000000;

		Dirichlet_t::Alpha_t Alpha(2);
		Alpha << 1.0, 2.0;

		//Create a gaus
		Dirichlet_t dist(Alpha);

		//Generate some samples
		Dirichlet_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

		//This are the estimated params
		Dirichlet_t::MeanVec_t  estMean;
		Dirichlet_t::CoVarMat_t estCoVar;

		//The teoretic values
		Dirichlet_t::MeanVec_t  theoMean  = dist.getMean();
		Dirichlet_t::CoVarMat_t theoCoVar = dist.getCoVar();

		//Calculate
		computeMeanAndStd(samples, estCoVar, estMean);

		const Numeric_t relErrCoVar = relError(estCoVar, theoCoVar);
		const Numeric_t relErrMean  = relError(estMean,  theoMean);

		std::cerr
			<< "The mean:\n"
			<< estMean.transpose()
			<< "\n"
			<< "\nTheo mean:\n"
			<< dist.getMean().transpose() << "\n"
			<< "Rel Error: " << relErrMean
			<< "\n\n";

		std::cerr
			<< "The estimated covariance:\n"
			<< estCoVar
			<< "\n"
			<< "\nTheo CoVar:\n"
			<< dist.getCoVar() << "\n"
			<< "RelError: " << relErrCoVar
			<< std::endl;


		if(relErrMean >= 0.05)
		{
			std::cerr
				<< "Relative error of mean, " << relErrMean << ", was too high"
				<< std::endl;
			std::abort();
		};
		if(relErrCoVar>= 0.05)
		{
			std::cerr
				<< "Relative error of covar, " << relErrCoVar << ", was too high"
				<< std::endl;
			std::abort();
		};
	}; //End test 1


	//Test 2
	{
		const Size_t N = 100000;

		Dirichlet_t::Alpha_t Alpha(4);
		Alpha << 6.13, 9.29, 10.6, 8.24;

		//Create a gaus
		Dirichlet_t dist(Alpha);

		//Generate some samples
		Dirichlet_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

		//This are the estimated params
		Dirichlet_t::MeanVec_t  estMean;
		Dirichlet_t::CoVarMat_t estCoVar;

		//The teoretic values
		Dirichlet_t::MeanVec_t  theoMean  = dist.getMean();
		Dirichlet_t::CoVarMat_t theoCoVar = dist.getCoVar();

		//Calculate
		computeMeanAndStd(samples, estCoVar, estMean);

		const auto relErrCoVar = relError(estCoVar, theoCoVar);
		const auto relErrMean  = relError(estMean, theoMean);

		std::cerr
			<< "The mean:\n"
			<< estMean.transpose()
			<< "\n"
			<< "Rel Error: " << relErrMean
			<< "\n\n";

		std::cerr
			<< "The estimated covariance:\n"
			<< estCoVar
			<< "\n"
			<< "RelError: " << relErrCoVar
			<< std::endl;


		if(relErrMean >= 0.05)
		{
			std::cerr
				<< "Relative error of mean, " << relErrMean << ", was too high"
				<< std::endl;
			std::abort();
		};
		if(relErrCoVar>= 0.05)
		{
			std::cerr
				<< "Relative error of covar, " << relErrCoVar << ", was too high"
				<< std::endl;
			std::abort();
		};

	}; //End: Test2


	return 0;
}; //End: main






