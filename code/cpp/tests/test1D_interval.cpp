/**
 * \brief	This file contains a small test for the 1D interval
 */

#include <util/yggdrasil_intervalBound.hpp>
#include <algorithm>
#include <tuple>


using interval_t = yggdrasil::yggdrasil_intervalBound_t;

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};

int
main(
	int 	argc,
	char**	argv)
{
	//Scope: 1
	//A small test
	{
		yggdrasil::yggdrasil_intervalBound_t test = yggdrasil::yggdrasil_intervalBound_t::CREAT_INVALID_INTERVAL();

		if(test.isValid() == true)
		{
			std::cerr << "Default does not construct an invalid domain." << std::endl;
			std::abort();
			return 1;
		};
	}; //End: scope 1

	//Test case 2
	{
		bool hasThrown = false;
		try
		{
			//This is an interval with zero length
			interval_t t(0.1, 0.1);
		}
		catch(std::exception& e)
		{
			hasThrown = true;
		};

		if(hasThrown == false)
		{
			std::cerr << "A zero length interval does not result in an error." << std::endl;
			std::abort();
			return 1;
		};
	}; //End case 2

	// TEST 3
	{
		interval_t t(0.0, 1.0);

		if(isApprox(1.0, t.getLength()) == false)
		{
			std::cerr << "The length is incoorect." << std::endl;
			std::abort();
			return 1;
		};

		if(isApprox(1.0, t.getUpperBound()) == false)
		{
			std::cerr << "The upper bound is incorrect." << std::endl;
			std::abort();
			return 1;
		};

		if(isApprox(0.0, t.getLowerBound()) == false)
		{
			std::cerr << "The lower bound is incorrect." << std::endl;
			std::abort();
			return 1;
		};

		if(t.isInside(0.5) == false)
		{
			std::cerr << "The inside test 1 fails." << std::endl;
			return 1;
		};

		if(t.isInside(1.0) == true)
		{
			std::cerr << "The bound is not sharp" << std::endl;
			return 1;
		};

		if(t.isCloseToUpperBound(1.0) == false)
		{
			std::cerr << "The close test fails." << std::endl;
			return 1;
		};
	}; //End test 3


	//Test 4:
	{
		interval_t t(0.0, 1.0);

		interval_t
			left  = interval_t::CREAT_INVALID_INTERVAL(),
			right = interval_t::CREAT_INVALID_INTERVAL();

		std::tie(left, right) = t.doSplit(0.5);

		std::cout
			<< "LEFT:  " << left  << "\n"
			<< "RIGHT: " << right << std::endl;



		if(left.isInside(0.5) == true)
		{
			std::cerr << "Split did not work on left" << std::endl;
			return 1;
		};

		if(left.isCloseToUpperBound(0.5) == false)
		{
			std::cerr << "Close on left is wrong." << std::endl;
			return 1;
		};

		if(right.isInside(0.5) == false)
		{
			std::cerr << "The bound on right is wrong." << std::endl;
			return 1;
		};
	}; //End test 4

	//Test 5
	{
		interval_t t(1.0, 11.0);

		if(isApprox(6.0, t.mapFromUnit(0.5)) == false)
		{
			std::cerr << "Mapping is broken" << std::endl;
			return 1;
		};

		if(isApprox(0.5, t.contractToUnit(6.0)) == false)
		{
			std::cerr << "Contracting is broken" << std::endl;
			return 1;
		};


	}; //End test 5



	return 0;

}; //End:  main



