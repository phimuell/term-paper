/**
 * \brief	This file contains a small test for the node
 */


extern
bool
ygInternal_testSplitRoutine();

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_linModel.hpp>
#include <para_models/yggdrasil_constModel.hpp>
#include <stat_tests/yggdrasil_IndepTest_chi2.hpp>
#include <stat_tests/yggdrasil_GOFTest_chi2.hpp>

#include <split_scheme/yggdrasil_sizeBasedSplitting.hpp>
#include <split_scheme/yggdrasil_scoreBasedSplitting.hpp>

#include <algorithm>
#include <tuple>
#include <random>

using Array_t = yggdrasil::yggdrasil_dimensionArray_t;
using Coll_t  = yggdrasil::yggdrasil_sampleCollection_t;
using SplitCom_t = yggdrasil::yggdrasil_singleSplit_t;
using SplitSeq_t = yggdrasil::yggdrasil_splitSequence_t;
using HyperCube_t = Coll_t::HyperCube_t;
using IntervalBound_t = HyperCube_t::IntervalBound_t;
using IndexArray_t = Coll_t::IndexArray_t;
using ValArray_t = Coll_t::ValueArray_t;
using Node_t  = yggdrasil::yggdrasil_DETNode_t;

using namespace yggdrasil;

std::mt19937_64 geni(42);

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


bool
performTest(
	const yggdrasil_parametricModel_i& parMod,
	const Node_t&		node,
	const HyperCube_t&	hc,
	const Size_t 		addMass,
	const Int_t 		D)
{
	for(int d = 0; d != D; ++d)
	{
		/*
		 * It is always one, since the tree only consistes of a single node
		 * Thus there is no correction to get to the rescdalled root data range.
		 */
		//const Numeric_t expectedValue = 1.0 / hc.at(d).getLength();
		const Numeric_t expectedValue = 1.0;

		ValArray_t X;

		for(int i = 0; i != 99; ++i)
		{
			const Numeric_t x  = 0.005 + 0.01 * i;
			X.push_back(x);
			const Numeric_t v1 = parMod.evaluateModelInSingleDim(x, d, hc.at(d));
			if(isApprox(expectedValue, v1) == false)
			{
				std::cerr << "The model estimated for x_" << d << " = " << x << " a density of " << v1 << " instead of " << expectedValue << std::endl;
				std::abort();
				return false;
			};
		}; //End for(i)

		ValArray_t ExpectedPDF(X.size(), expectedValue);
		ValArray_t V2 = parMod.evaluateModelInSingleDim(X, d, hc.at(d));

		for(Size_t i = 0; i != X.size(); ++i)
		{
			if(isApprox(ExpectedPDF.at(i), V2.at(i)) == false)
			{
				std::cerr << "(V): The model estimated at position x_" << d << " = " << X.at(i) << " was " << V2.at(i) << ", but the expected value was " << ExpectedPDF.at(i) << std::endl;
				std::abort();
				return false;
			};
		}; //End for(i)
	}; //End for(d)


	//Test with a sample
	yggdrasil_sample_t sample(2);
	const Numeric_t expectedValue = 1.0;	//The scaling factor is one to get to the rescalled root data space
	for(int i = 0; i != 99; ++i)
	{
		for(int j = 0; j != 99; ++j)
		{
			sample.at(0) = 0.005 + i * 0.01;
			sample.at(1) = 0.005 + j * 0.01;

			const Numeric_t v1 = parMod.evaluateModelForSample(sample, hc, node.getMass() + addMass);

			if(isApprox(v1, expectedValue) == false)
			{
				std::cerr << "For sample " << sample << " we have " << v1 << ", instead of " << expectedValue << std::endl;
				std::abort();
				return false;
			};
		}; //End for(j)
	}; //End for(i)

	return true;
}; //End consistency test


bool
ygInternal_testSplitRoutine()
{
	const int D = 2;

	//Create the tests
	yggdrasil_Chi2GOFTest_t gof(D, 0.05);
	yggdrasil_Chi2IndepTest_t indep(D, 0.05);

	//These tests perform some access/logic tests
	{
		yggdrasil_constantModel_t parMod(D);
		if(parMod.isAssociated() == true)
		{
			std::cerr << "The model is not empty." << std::endl;
			std::abort();
			return false;
		};

		{
			bool hasThrown = false;

			try
			{
				const Size_t nS = parMod.isConsistent();
				(void)nS;
			}
			catch(yggdrasil_exception& e)
			{
				hasThrown = true;
			};

			if(hasThrown == false)
			{
				std::cerr << "A call to the consistency function of an empty model, did not produce an error." << std::endl;
				std::abort();
				return false;
			};
		};

		{
			bool hasThrown = false;

			try
			{
				const Size_t nS = parMod.nSampleBase();
				(void)nS;
			}
			catch(yggdrasil_exception& e)
			{
				hasThrown = true;
			};

			if(hasThrown == false)
			{
				std::cerr << "A call to the nSampleBase function of an empty model, did not produce an error." << std::endl;
				std::abort();
				return false;
			};
		};

		{
			bool hasThrown = false;
			HyperCube_t hc(D, IntervalBound_t(0, 1));

			try
			{
				const Numeric_t v = parMod.evaluateModelInSingleDim(0.5, 1, hc.at(1));
				(void)v;
			}
			catch(yggdrasil_exception& e)
			{
				hasThrown = true;
			};

			if(hasThrown == false)
			{
				std::cerr << "A call to the nSampleBase function of an empty model, did not produce an error." << std::endl;
				std::abort();
				return false;
			};
		};
	}; //End access tests

	//A simple test function
	{
		Array_t::ValueArray_t
			valArr1 = {0.25, 0.25, 0.75, 0.75},
			valArr2 = {0.25, 0.75, 0.25, 0.75};

		Array_t::ValueArray_t vL[] = {valArr1, valArr2};
		//Node_t::HyperCube_t hc(2, IntervalBound_t(0.0, 1.0));
		Node_t::HyperCube_t hc(2);
		hc.setIntervalTo(0, IntervalBound_t(0.0, 1.0));
		hc.setIntervalTo(1, IntervalBound_t(0.0, 2.0));

		yggdrasil_constantModel_t parMod(D);
		if(parMod.isAssociated() == true)
		{
			std::cerr << "The model is not empty." << std::endl;
			std::abort();
			return false;
		};

		Coll_t c(D);

		for(int d = 0; d != D; ++d)
		{
			if(c[d].nSamples() != 0)
			{
				std::cerr << "Problems with the created dimesnions they have size." << std::endl;
				std::abort();
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "The valuid function is wrong." << std::endl;
				std::abort();
			};

			c.getDim(d).copyAndLoadValueArray(vL[d]);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
			return false;
		};

		Node_t node(c, &parMod, &gof, &indep, &hc);

		if(parMod.isAssociated() == true)
		{
			std::cerr << "The model is associated." << std::endl;
			std::abort();
		};


		//Now we fit the model
		for(int d = 0; d != D; ++d)
		{
			if(parMod.isDimensionFitted(d) == true)
			{
				std::cerr << "The dimension " << d << " was already fitted." << std::endl;
				std::abort();
				return false;
			};

			parMod.fitModelInDimension(node.makeFacade(), d);

			if(parMod.isDimensionFitted(d) == false)
			{
				std::cerr << "The dimension " << d << " was not fitted." << std::endl;
				std::abort();
				return false;
			};
		};

		if(parMod.allDimensionsFitted() == false)
		{
			std::cerr << "Not all dimensions are fitted." << std::endl;
			std::abort();
		};

		if(parMod.isAssociated() == false)
		{
			std::cerr << "The fitting has node made the model non empty." << std::endl;
			std::abort();
			return false;
		};

		if(parMod.nSampleBase() != valArr1.size())
		{
			std::cerr << "The model is based on " << parMod.nSampleBase() << " instead of " << valArr1.size() << " samples." << std::endl;
			std::abort();
			return false;
		};

		performTest(parMod, node, hc, 0, D);


		/*
		 * Now we add a sample to the model
		 */
		if(parMod.isConsistent() == false)
		{
			std::cerr << "the model is not consistent." << std::endl;
			std::abort();
			return false;
		};

		yggdrasil_sample_t sample = {0.5, 0.33};
		parMod.addNewSample(sample, hc);

		if(parMod.isConsistent() == true)
		{
			std::cerr << "The model is still consistent." << std::endl;
			std::abort();
			return false;
		};
		if(parMod.nSampleBase() != valArr1.size())
		{
			std::cerr << "The model is based on " << parMod.nSampleBase() << " instead of " << valArr1.size() << std::endl;
			std::abort();
			return false;
		};

		//Add a bunch of the same samples
		Size_t addedSamples = 1;	//We have already some
		for(Int_t i = 0; i != 568; ++i)
		{
			parMod.addNewSample(sample, hc);
			addedSamples += 1;

			if(parMod.nSampleBase() != valArr1.size())
			{
				std::cerr << "The model is based on " << parMod.nSampleBase() << " instead of " << valArr1.size() << std::endl;
				std::abort();
				return false;
			};
		};

	}; //End: Test



	std::cout << "Did the test." << std::endl;
	return true;

}; //End test split


int
main(
	int 	argc,
	char**	argv)
{
	std::cerr
		<< "This test was reduced, and should be extended again. "
		<< "The main reason for that is, that the model and the node are very tithly coupled. "
		<< "So one has first to find a way to decoule it. "
		<< "For that a more developed tree should be first implemented."
		<< std::endl;

#if 1
	std::uniform_real_distribution<double> dist(-10000.0, 10000.0);
	const int N = 10000;
	const int D = 4;
	Array_t::ValueArray_t valArr1, valArr2, valArr3, valArr4;

	for(int i = 0; i != N; ++i)
	{
		valArr1.push_back(dist(geni));
		valArr2.push_back(dist(geni));
		valArr3.push_back(dist(geni));
		valArr4.push_back(dist(geni));
	};

	Array_t::ValueArray_t vL[] = {valArr1, valArr2, valArr3, valArr4};
#else
#endif

	//Create the tests
	yggdrasil_Chi2GOFTest_t gof(D, 0.05);
	yggdrasil_Chi2IndepTest_t indep(D, 0.05);
	yggdrasil_linearModel_t parMod(D);


	//Test 1
	{
		Coll_t c(D);

		for(int d = 0; d != D; ++d)
		{
			if(c[d].nSamples() != 0)
			{
				std::cerr << "Problems with the created dimesnions they have size." << std::endl;
				std::abort();
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "The valuid function is wrong." << std::endl;
				std::abort();
			};

			c.getDim(d).copyAndLoadValueArray(vL[d]);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
			return 1;
		};

		HyperCube_t hc(D);
		Node_t node(c, &parMod, &gof, &indep, &hc);

		if(node.checkIntegrity() == false)
		{
			std::cerr << "The node is not integer." << std::endl;
			std::abort();
			return 1;
		};
	};//End test 1


	if(ygInternal_testSplitRoutine() == false)
	{
		std::cerr << "Split failed." << std::endl;
		std::abort();
		return 1;
	};



	return 0;

}; //End:  main


