/**
 * \brief	This file contains a small test for the dim array.
 */

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <algorithm>
#include <tuple>
#include <random>


using Array_t = yggdrasil::yggdrasil_dimensionArray_t;
using Coll_t  = yggdrasil::yggdrasil_sampleCollection_t;

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};

int
main(
	int 	argc,
	char**	argv)
{

	std::mt19937_64 geni(42);
#if 1
	std::uniform_real_distribution<double> dist(-10000.0, 10000.0);
	const int N = 10000;
	const int D = 4;
	Array_t::ValueArray_t valArr1, valArr2, valArr3, valArr4;

	for(int i = 0; i != N; ++i)
	{
		valArr1.push_back(dist(geni));
		valArr2.push_back(dist(geni));
		valArr3.push_back(dist(geni));
		valArr4.push_back(dist(geni));
	};

	Array_t::ValueArray_t vL[] = {valArr1, valArr2, valArr3, valArr4};
#else
#endif

	//Test: 1
	{
		for(int d = 1; d != ::yggdrasil::Constants::YG_MAX_DIMENSIONS; ++d)
		{
			try
			{
				Coll_t c(d);

				if(c.nDims() != d)
				{
					std::cerr << "The numbers of diomesion is wrong, it was " << c.nDims() << " but should be " << d << std::endl;
					std::abort();
					return 1;
				};

				if(c.isValid() == false)
				{
					std::cerr << "The collection is not valid." << std::endl;
					std::abort();
				};

				for(int dd = 0; dd != d; ++dd)
				{
					if(c.getDim(dd).getAssociatedDim() != dd)
					{
						std::cout << "The nuilding is not correct." << std::endl;
					};
				};
			}
			catch(yggdrasil::yggdrasil_exception& e)
			{
				std::cerr << "An unexpected ecetion has occured.\nThe test was:\n"
					<< e.what() << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(d)
	}; //End: Test 1


	//Test: 2
	{
		int invalidDim[] = {-1, 0, ::yggdrasil::Constants::YG_MAX_DIMENSIONS, ::yggdrasil::Constants::YG_MAX_DIMENSIONS + 1};
		for(int d : invalidDim)
		{
			bool hasThrown = false;
			try
			{
				Coll_t a(d);
			}
			catch(::yggdrasil::yggdrasil_exception& e)
			{
				hasThrown = true;
			};

			if(hasThrown == false)
			{
				std::cout << "The dimension " << d << " was not recognized illegal." << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(d)
	}; //End: Test 2


	//Test: 3
	{
		Coll_t c(D);

		for(int d = 0; d != D; ++d)
		{
			if(c[d].nSamples() != 0)
			{
				std::cerr << "Problems with the created dimesnions they have size." << std::endl;
				std::abort();
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "The valuid function is wrong." << std::endl;
				std::abort();
			};

			c.getDim(d).copyAndLoadValueArray(vL[d]);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
		};


		for(int d = 0; d != D; ++d)
		{
			const auto& dimA = c.getCDim(d);
			auto it = c.cbeginSample(d);
			for(int i = 0; i != N; ++i)
			{
				if(vL[d].at(i) != dimA.at(i))
				{
					std::cerr << "The coping does not work." << std::endl;
					std::abort();
				};
				if(vL[d].at(i) != dimA[i])
				{
					std::cerr << "The coping does not work ([])." << std::endl;
					std::abort();
				};
				if(vL[d].at(i) != *it)
				{
					std::cerr << "The coping does not work (it)." << std::endl;
					std::abort();
				};
				++it;
			}; //End for(i)
		}; //end for(d)
	}; //End Test 3


	//Test 4
	{
		Coll_t c(D);

		for(int d = 0; d != D; ++d)
		{
			if(c[d].nSamples() != 0)
			{
				std::cerr << "Problems with the created dimesnions they have size." << std::endl;
				std::abort();
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "The valuid function is wrong." << std::endl;
				std::abort();
			};

			c.getDim(d).copyAndLoadValueArray(vL[d]);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
		};

		if(c.nSamples() != N)
		{
			std::cerr << "The samples are wrong." << std::endl;
			std::abort();
		};

		{
			Coll_t::HyperCube_t q(D, Coll_t::HyperCube_t::IntervalBound_t(-10000.0, 10000));

			if(c.isInsideCube(q) == false)
			{
				std::cout << "The sampels are not inside the cube." << std::endl;
				std::abort();
			};
		}

		{
			Coll_t::HyperCube_t q(D, Coll_t::HyperCube_t::IntervalBound_t(-10000.0, 0));

			if(c.isInsideCube(q) == true)
			{
				std::cout << "The sampels are not outside the cube." << std::endl;
				std::abort();
			};
		}

	};//End test 4

	//Test 5
	{
		Coll_t::Sample_t s(D, 1.0);
		Coll_t c(D);

		if(c.isValid() == false)
		{
			std::cerr << "the collection is not valid." << std::endl;
			std::abort();
		};

		if(c.nSamples() != 0)
		{
			std::cerr << "Samples are not zero." << std::endl;
			std::abort();
		};

		c.addNewSample(s);

		if(c.isValid() == false)
		{
			std::cerr << "the collection is not valid." << std::endl;
			std::abort();
		};

		if(c.nSamples() != 1)
		{
			std::cerr << "Samples are not one." << std::endl;
			std::abort();
		};

		for(int d = 0; d != D; ++d)
		{
			s.at(d) = -1.0;
			s[d] = 9.9;
			if(c.getDim(d).at(0) != 1.0)
			{
				std::cout << "problem with loading the samples." << std::endl;
			};
		};

	}; //End test 5


	//test 6
	{
		Coll_t c(D);

		std::vector<int> S(D);

		for(int d = 0; d != D; ++d)
		{
			if(c[d].nSamples() != 0)
			{
				std::cerr << "Problems with the created dimesnions they have size." << std::endl;
				std::abort();
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "The valuid function is wrong." << std::endl;
				std::abort();
			};

			c.getDim(d).copyAndLoadValueArray(vL[d]);
			S.at(d) = c.nSamples(d);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
		};

		if(c.nSamples() != N)
		{
			std::cerr << "The samples are wrong." << std::endl;
			std::abort();
		};

		for(const auto r : S)
		{
			if(r != N)
			{
				std::cerr << "Problem with counting." << std::endl;
				std::abort();
			};
		};


		for(int d = 0; D != d; ++d)
		{
			for(int r = 0; r != D; ++r)
			{
				if(S[r] != c.getCDim(r).nSamples())
				{
					std::cerr << "nSamples has not expected value.," << std::endl;
					std::abort();
				};
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "Collection failed valid test." << std::endl;
				std::abort();
			};

			Coll_t::DELETE_SAMPLES_FROM_COLLECTION_SINGLE_DIMENSION(c, d);
			S.at(d) = 0;



			for(int r = 0; r != D; ++r)
			{
				if(S[r] != c.getCDim(r).nSamples())
				{
					std::cerr << "nSamples has not expected value.," << std::endl;
					std::abort();
				};
			};
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is not valid." << std::endl;
			std::abort();
		};

		for(int r = 0; r != D; ++r)
		{
			if(S[r] != 0)
			{
				std::cerr << "S has not the expected value." << std::endl;
				std::abort();
			};

			if(S[r] != c.getCDim(r).nSamples())
			{
				std::cerr << "nSamples has not expected value.," << std::endl;
				std::abort();
			};
		};

	}; //End test 6


	return 0;

}; //End:  main



