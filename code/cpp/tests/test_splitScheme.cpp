/*
 * Small tzest for the split schemes
 */

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <split_scheme/yggdrasil_scoreBasedSplitting.hpp>


#include <algorithm>
#include <tuple>
#include <random>



using Array_t = yggdrasil::yggdrasil_dimensionArray_t;
using Coll_t  = yggdrasil::yggdrasil_sampleCollection_t;
using SplitCom_t = yggdrasil::yggdrasil_singleSplit_t;
using SplitSeq_t = yggdrasil::yggdrasil_splitSequence_t;
using HyperCube_t = Coll_t::HyperCube_t;
using IntervalBound_t = HyperCube_t::IntervalBound_t;
using IndexArray_t = Coll_t::IndexArray_t;
using ValArray_t = Coll_t::ValueArray_t;

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


int
main(
	int 	argc,
	char**	argv)
{
	yggdrasil::yggdrasil_scoreSplit_t Split;


	//Test 1
	{
		HyperCube_t hc(1, IntervalBound_t(0.0, 1.0));
		SplitSeq_t sc = SplitSeq_t().copyAndAddNewLevelOfSplit(0);
		IndexArray_t ia;
		Coll_t col(1);


		const SplitCom_t f1 = Split.determineSplitLocation(hc, sc, ia.begin(), ia.end(), col.getDim(0));

		if(f1.getSplitDim() != 0)
		{
			std::cerr << "The split dimension was not 0, but " << f1.getSplitDim() << std::endl;
			std::abort();
			return 1;
		};

		if(isApprox(0.5, f1.getSplitPos()) == false)
		{
			std::cerr << "the spit position is not 0.5, but " << f1.getSplitPos() << std::endl;
			std::abort();
			return 1;
		};
	}; //End: Test 1

	// Test 2
	{
		HyperCube_t hc(1, IntervalBound_t(0.0, 1.0));
		SplitSeq_t sc = SplitSeq_t().copyAndAddNewLevelOfSplit(0);
		IndexArray_t ia = {0, 1, 2};
		ValArray_t va = {0.8, 0.9, 0.1};
		Coll_t col(1);
		col.getDim(0).copyAndLoadValueArray(va);

		const SplitCom_t f1 = Split.determineSplitLocation(hc, sc, ia.begin(), ia.end(), col.getDim(0));

		if(f1.getSplitDim() != 0)
		{
			std::cerr << "The split dimension was not 0, but " << f1.getSplitDim() << std::endl;
			std::abort();
			return 1;
		};

		//if(isApprox(0.5, f1.getSplitPos()) == false)
		if(0.8 != f1.getSplitPos())
		{
			std::cerr << "the spit position is not 0.8, but " << f1.getSplitPos() << std::endl;
			std::abort();
			return 1;
		};
	}; //End test 2

	// Test 3
	{
		HyperCube_t hc(1, IntervalBound_t(0.0, 1.0));
		SplitSeq_t sc = SplitSeq_t().copyAndAddNewLevelOfSplit(0);
		IndexArray_t ia = {0, 1};
		ValArray_t va = {0.9, 0.3};
		Coll_t col(1);
		col.getDim(0).copyAndLoadValueArray(va);

		const SplitCom_t f1 = Split.determineSplitLocation(hc, sc, ia.begin(), ia.end(), col.getDim(0));

		if(f1.getSplitDim() != 0)
		{
			std::cerr << "The split dimension was not 0, but " << f1.getSplitDim() << std::endl;
			std::abort();
			return 1;
		};

		//if(isApprox(0.5, f1.getSplitPos()) == false)
		if(0.6 != f1.getSplitPos())
		{
			std::cerr << "the spit position is not 0.6, but " << f1.getSplitPos() << std::endl;
			std::abort();
			return 1;
		};
	}; //End test 2

	std::mt19937_64 geni(42);

	//Test 4
	{
		HyperCube_t hc(1, IntervalBound_t(0.0, 1.0));
		SplitSeq_t sc = SplitSeq_t().copyAndAddNewLevelOfSplit(0);
		IndexArray_t ia;
		ValArray_t va;

		const int N = 1000;

		std::uniform_real_distribution<double> distL(0.0, 0.4);
		std::uniform_real_distribution<double> distU(0.6, 0.99);

		for(int i = 0; i != N; ++i)
		{
			va.push_back(distL(geni));
			va.push_back(distU(geni));
		}; //End for(i)

		//This is the supposed middle point
		const double Middle = 0.563219;
		va.push_back(Middle);

		std::shuffle(va.begin(), va.end(), geni);

		for(int i = 0; va.size() != i; ++i)
		{
			ia.push_back(i);
		};

		Coll_t col(1);
		col.getDim(0).copyAndLoadValueArray(va);

		const SplitCom_t f1 = Split.determineSplitLocation(hc, sc, ia.begin(), ia.end(), col.getDim(0));

		if(f1.getSplitDim() != 0)
		{
			std::cerr << "The split dimension was not 0, but " << f1.getSplitDim() << std::endl;
			std::abort();
			return 1;
		};

		//if(isApprox(0.5, f1.getSplitPos()) == false)
		if(Middle != f1.getSplitPos())
		{
			std::cerr << "the spit position is not " << Middle << ", but " << f1.getSplitPos() << std::endl;
			std::abort();
			return 1;
		};
	}; //End test 4


	//Test 5
	{
		HyperCube_t hc(1, IntervalBound_t(0.0, 1.0));
		SplitSeq_t sc = SplitSeq_t().copyAndAddNewLevelOfSplit(0);
		IndexArray_t ia;
		ValArray_t va1, va2;

		const int N = 1000;

		std::uniform_real_distribution<double> distL(0.0, 0.4);
		std::uniform_real_distribution<double> distU(0.6, 0.99);

		for(int i = 0; i != N; ++i)
		{
			va1.push_back(distL(geni));
			va2.push_back(distU(geni));
		}; //End for(i)

		//This is the supposed middle point
		const double LMax = *std::max_element(va1.begin(), va1.end());
		const double UMin = *std::min_element(va2.begin(), va2.end());
		const double Middle = (LMax + UMin) * 0.5;

		ValArray_t va;
		for(int i = 0; i != N; ++i)
		{
			va.push_back(va1.at(i));
			va.push_back(va2.at(i));
		}; //End for(i)

		std::shuffle(va.begin(), va.end(), geni);

		for(int i = 0; va.size() != i; ++i)
		{
			ia.push_back(i);
		};

		Coll_t col(1);
		col.getDim(0).copyAndLoadValueArray(va);

		const SplitCom_t f1 = Split.determineSplitLocation(hc, sc, ia.begin(), ia.end(), col.getDim(0));

		if(f1.getSplitDim() != 0)
		{
			std::cerr << "The split dimension was not 0, but " << f1.getSplitDim() << std::endl;
			std::abort();
			return 1;
		};

		//if(isApprox(0.5, f1.getSplitPos()) == false)
		if(Middle != f1.getSplitPos())
		{
			std::cerr << "the spit position is not " << Middle << ", but " << f1.getSplitPos() << std::endl;
			std::abort();
			return 1;
		};
	}; //End test 4

	return 0;
};







