/**
 * \brief	This file contains a small test for the descend way
 */

#include <util/yggdrasil_subDomainEnum.hpp>
#include <algorithm>
#include <tuple>


using SubDomEnum_t = ::yggdrasil::yggdrasil_subDomainEnum_t;


int
main(
	int 	argc,
	char**	argv)
{
	// Test 1:
	{
		SubDomEnum_t Domain;

		if(Domain.getDepth() != 0)
		{
			std::cerr << "The depth was not 0, instead it was " << Domain.getDepth() << std::endl;
			std::abort();
			return 1;
		};

		bool hasThrown = false;
		try
		{
			Domain.getWay();
		}
		catch(yggdrasil::yggdrasil_exception& e)
		{
			std::cout << "The expected exception occured.\nThe message was:\n" << e.what() << std::endl;
			hasThrown = true;
		};

		if(hasThrown == false)
		{
			std::cerr << "The exception did not occure" << std::endl;
			std::abort();
			return 1;
		};
	}; //End: Test 1


	//Test 2:
	{
		SubDomEnum_t Domain;
		Domain = Domain.decendToLeft();

		if(Domain.getDepth() != 1)
		{
			std::cerr << "The depth was not 1, but " << Domain.getDepth() << std::endl;
			std::abort();
			return 1;
		};

		if(Domain.wasLeft(0) != true)
		{
			std::cerr << "The descend was not to the left" << std::endl;
			std::abort();
			return 1;
		};

		if(Domain.wasRight(0) != false)
		{
			std::cerr << "The descend was to the right, but we sent it to the left." << std::endl;
			std::abort();
			return 1;
		};

		//Again decend to the left
		Domain = Domain.decendToLeft();

		if(Domain.getDepth() != 2)
		{
			std::cerr << "The depth was not 2, but " << Domain.getDepth() << std::endl;
			std::abort();
			return 1;
		};

		//Old decend still intact
		if(Domain.wasLeft(0) != true)
		{
			std::cerr << "The descend was not to the left" << std::endl;
			std::abort();
			return 1;
		};

		if(Domain.wasRight(0) != false)
		{
			std::cerr << "The descend was to the right, but we sent it to the left." << std::endl;
			std::abort();
			return 1;
		};

		//New decend
		if(Domain.wasLeft(1) != true)
		{
			std::cerr << "The descend was not to the left" << std::endl;
			std::abort();
			return 1;
		};

		if(Domain.wasRight(1) != false)
		{
			std::cerr << "The descend was to the right, but we sent it to the left." << std::endl;
			std::abort();
			return 1;
		};


		// Now send it to the right
		Domain = Domain.decendToRight();

		if(Domain.getDepth() != 3)
		{
			std::cerr << "The depth was not 3, but " << Domain.getDepth() << std::endl;
			std::abort();
			return 1;
		};

		//Old decend still intact
		if(Domain.wasLeft(0) != true)
		{
			std::cerr << "The descend was not to the left" << std::endl;
			std::abort();
			return 1;
		};

		if(Domain.wasRight(0) != false)
		{
			std::cerr << "The descend was to the right, but we sent it to the left." << std::endl;
			std::abort();
			return 1;
		};

		if(Domain.wasLeft(1) != true)
		{
			std::cerr << "The descend was not to the left" << std::endl;
			std::abort();
			return 1;
		};

		if(Domain.wasRight(1) != false)
		{
			std::cerr << "The descend was to the right, but we sent it to the left." << std::endl;
			std::abort();
			return 1;
		};

		if(Domain.wasLeft(2) != false)
		{
			std::cerr << "The descend was not to the right" << std::endl;
			std::abort();
			return 1;
		};

		if(Domain.wasRight(2) != true)
		{
			std::cerr << "The descend was to the left, but we sent it to the right." << std::endl;
			std::abort();
			return 1;
		};
	}; //End: Test 2



	return 0;

}; //End:  main



