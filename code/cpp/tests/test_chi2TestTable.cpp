/**
 */

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <util/yggdrasil_chi2_testtable.hpp>
#include <algorithm>
#include <tuple>
#include <random>


using namespace yggdrasil;
using Array_t = yggdrasil::yggdrasil_dimensionArray_t;
using Coll_t  = yggdrasil::yggdrasil_sampleCollection_t;
using BinEdges_t = ::yggdrasil::yggdrasil_genBinEdges_t;
using TestTable_t = ::yggdrasil::yggdrasil_chi2TestTable_t;
using IntervalBound_t = ::yggdrasil::yggdrasil_intervalBound_t;

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};

int
main(
	int 	argc,
	char**	argv)
{
	std::mt19937_64 geni(42);

	bool strat[2] = {false, true};
	for(const bool q : strat)
	{

		//Test 1
		{

			//Create an empty dim array
			Array_t emptyArray(0);

			//Create a test table
			TestTable_t table(0.05, false);

			//Make some special
			const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
			TestTable_t::ValueArray_t emptyInnerEdges;

			const BinEdges_t bins(unitInterval, emptyInnerEdges);

			//bind the table
			table.internal_buildInitialChi2TableDirect(emptyArray, bins, q);

			//Test
			if(table.nBins() != 1)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};
			if(table.getCountInBin(0) != 0)
			{
				std::cerr << "The bin count was supposed to be empty, but it contained " << table.getCountInBin(0) << std::endl;
				std::abort();
			};

		}; //End test 1

		//Test 2
		{

			//Create an empty dim array
			Array_t::ValueArray_t valArr1 = {0.90, 0.75, 0.50, 0.25};

			Array_t dimArr(0);
			dimArr.copyAndLoadValueArray(valArr1);

			//Create a test table
			TestTable_t table(0.05, false);

			//Make some special
			const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
			TestTable_t::ValueArray_t innerEdges = {0.30, 0.60, 0.80};

			const BinEdges_t bins(unitInterval, innerEdges);

			//bind the table
			table.internal_buildInitialChi2TableDirect(dimArr, bins, q);

			//Test
			if(table.nBins() != 4)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};
			for(int i = 0; i != 4; ++i)
			{

				if(table.getCountInBin(i) != 1)
				{
					std::cerr << "The bin count of bin " << i << " was supposed to be one, but it contained " << table.getCountInBin(1) << std::endl;
					std::abort();
				};
			}; //End for(i)

		}; //End test 2


		//Test 3
		{
			const int N[4] = {100, 333, 432, 782};
			const int J[4] = {0, 3, 1, 2};

			Array_t::ValueArray_t valArr;
			for(int j : J)
			{
				std::uniform_real_distribution<double> dist(0.01 + j * 0.25, (j + 1) * 0.25 - 0.01);
				for(int i = 0; i != N[j]; ++i)
				{
					valArr.push_back(dist(geni));
				}; //End for(i):
			}; //End for(j):

			std::shuffle(valArr.begin(), valArr.end(), geni);
			Array_t dimArr(0);
			dimArr.copyAndLoadValueArray(valArr);

			//Create a test table
			TestTable_t table(0.05, false);

			//Make some special
			const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
			TestTable_t::ValueArray_t innerEdges = {0.25, 0.50, 0.75};

			const BinEdges_t bins(unitInterval, innerEdges);

			//bind the table
			table.internal_buildInitialChi2TableDirect(dimArr, bins, q);

			//Test
			if(table.nBins() != 4)
			{
				std::cerr << "There where not zero bins." << std::endl;
				std::abort();
			};
			for(int i = 0; i != 4; ++i)
			{
				if(table.getCountInBin(i) != N[i])
				{
					std::cerr << "The bin count of bin " << i << " was supposed to be one, but it contained " << table.getCountInBin(1) << std::endl;
					std::abort();
				};
			}; //End for(i)
		}; //End test 3


		//Test 3.2
		{
			std::uniform_int_distribution<int> dist(1000, 3000);
			const double sMargin = 0.000001;
			for(int n = 1; n != 30; ++n)
			{
				const double h = 1.0 / n;
				if(sMargin >= (2 * h))
				{
					throw YGGDRASIL_EXCEPT_RUNTIME("margin is to big.");
				};

				std::vector<Size_t> expCount(n);
				for(int i = 0; i != n; ++i)
				{
					expCount.at(i) = dist(geni);
				};

				//Generating of the samples
				Array_t::ValueArray_t valArr;
				for(int j = 0; j != n; ++j)
				{
					std::uniform_real_distribution<double> dist(
							sMargin + j * h,
							(j + 1) * h - sMargin
							);
					for(int i = 0; i != expCount[j]; ++i)
					{
						valArr.push_back(dist(geni));
					}; //End for(i):
				}; //End for(j):


				const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
				TestTable_t::ValueArray_t innerEdges;
				for(int i = 1; i != n; ++i)
				{
					innerEdges.push_back(i * h);
				}; //End for(i):

				std::shuffle(valArr.begin(), valArr.end(), geni);
				Array_t dimArr(0);
				dimArr.copyAndLoadValueArray(valArr);


				const BinEdges_t bins(unitInterval, innerEdges);

				//bind the table
				TestTable_t table(0.05, false);
				table.internal_buildInitialChi2TableDirect(dimArr, bins, q);

				if(table.nBins() != n)
				{
					std::cerr << "The test table had " << table.nBins() << " many bins, expected" << n << std::endl;
					std::abort();
				};

				for(int i = 0; i != n; ++i)
				{
					if(table.getCountInBin(i) != expCount[i])
					{
						std::cout << "Expected " << expCount.at(i) << " many sample in bin " << i << " but got " << table.getCountInBin(i) << std::endl;
						std::abort();
					};
				}; //End for(i):
			}; //End for(n)


		}; //End test 3.2

	}; //End for(b)


	//Test 4
	{

		//Create an empty dim array
		Array_t::ValueArray_t valArr1 = {0.90, 0.75, 0.50, 0.25};

		Array_t dimArr(0);
		dimArr.copyAndLoadValueArray(valArr1);

		//Create a test table
		TestTable_t table(0.05, false);

		//Make some special
		const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
		TestTable_t::ValueArray_t innerEdges = {0.30, 0.60, 0.80};

		const BinEdges_t bins(unitInterval, innerEdges);

		//bind the table
		table.internal_buildInitialChi2TableDirect(dimArr, bins, false);

		//Test
		if(table.nBins() != 4)
		{
			std::cerr << "There where not zero bins." << std::endl;
			std::abort();
		};
		for(int i = 0; i != 4; ++i)
		{

			if(table.getCountInBin(i) != 1)
			{
				std::cerr << "The bin count of bin " << i << " was supposed to be one, but it contained " << table.getCountInBin(1) << std::endl;
				std::abort();
			};
		}; //End for(i)

		//This is the expected count
		const TestTable_t::ValueArray_t expCount = {1, 1, 1, 1};

		//The chi2 value must be zero
		const auto Chi2 = table.computeChi2Value(expCount);

		if(isApprox(Chi2, 0.0) == false)
		{
			std::cerr << "The chi2 value is not zero as expected. ir was " << Chi2 << std::endl;
			std::abort();
		};
	}; //End test 4



	//Test 5
	{

		//Create an empty dim array
		Array_t::ValueArray_t valArr1 = {0.90, 0.75, 0.50};

		Array_t dimArr(0);
		dimArr.copyAndLoadValueArray(valArr1);

		//Create a test table
		TestTable_t table(0.05, false);

		//Make some special
		const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
		TestTable_t::ValueArray_t innerEdges = {0.30, 0.60, 0.80};

		const BinEdges_t bins(unitInterval, innerEdges);

		//bind the table
		table.internal_buildInitialChi2TableDirect(dimArr, bins, false);

		//Test
		if(table.nBins() != 4)
		{
			std::cerr << "There where not zero bins." << std::endl;
			std::abort();
		};
		for(int i = 0; i != 4; ++i)
		{

			if(table.getCountInBin(i) != (i == 0) ? 0 : 1)
			{
				std::cerr << "The bin count of bin " << i << " was supposed to be one, but it contained " << table.getCountInBin(1) << std::endl;
				std::abort();
			};
		}; //End for(i)

		//This is the expected count
		const TestTable_t::ValueArray_t expCount = {0, 1, 1, 1};

		//The chi2 value must be zero
		const auto Chi2 = table.computeChi2Value(expCount);

		if(isApprox(Chi2, 0.0) == false)
		{
			std::cerr << "The chi2 value is not zero as expected. ir was " << Chi2 << std::endl;
			std::abort();
		};
	}; //End test 5


	//Test 6
	{

		//Create an empty dim array
		Array_t::ValueArray_t valArr1 = {0.90, 0.75, 0.50, 0.25};

		Array_t dimArr(0);
		dimArr.copyAndLoadValueArray(valArr1);

		//Create a test table
		TestTable_t table(0.05, false);

		//Make some special
		const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();
		TestTable_t::ValueArray_t innerEdges = {0.30, 0.60, 0.80};

		const BinEdges_t bins(unitInterval, innerEdges);

		//bind the table
		table.internal_buildInitialChi2TableDirect(dimArr, bins, false);

		//Test
		if(table.nBins() != 4)
		{
			std::cerr << "There where not zero bins." << std::endl;
			std::abort();
		};
		for(int i = 0; i != 4; ++i)
		{

			if(table.getCountInBin(i) != 1)
			{
				std::cerr << "The bin count of bin " << i << " was supposed to be one, but it contained " << table.getCountInBin(1) << std::endl;
				std::abort();
			};
		}; //End for(i)

		//This is the expected count
		const TestTable_t::ValueArray_t expCount = {0, 1, 1, 1};

		//The chi2 value must be zero
		const auto Chi2 = table.computeChi2Value(expCount);

		if(::std::isinf(Chi2) == false)
		{
			std::cerr << "The chi2 value is not zero as expected. ir was " << Chi2 << std::endl;
			std::abort();
		};
	}; //End test 6




	return 0;

}; //End:  main



