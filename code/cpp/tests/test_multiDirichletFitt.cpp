
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <tree/yggdrasil_treeNode.hpp>
#include <tree/yggdrasil_treeImpl.hpp>

#include <para_models/yggdrasil_linModel.hpp>
#include <para_models/yggdrasil_constModel.hpp>
#include <stat_tests/yggdrasil_IndepTest_chi2.hpp>
#include <stat_tests/yggdrasil_GOFTest_chi2.hpp>

#include <split_scheme/yggdrasil_sizeBasedSplitting.hpp>
#include <split_scheme/yggdrasil_scoreBasedSplitting.hpp>

#include <algorithm>
#include <tuple>
#include <random>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_multiDirichle.hpp>

#include <algorithm>
#include <tuple>
#include <random>
#include <chrono>

#include <Eigen/Dense>

using Array_t = yggdrasil::yggdrasil_dimensionArray_t;
using Coll_t  = yggdrasil::yggdrasil_sampleCollection_t;
using SplitCom_t = yggdrasil::yggdrasil_singleSplit_t;
using SplitSeq_t = yggdrasil::yggdrasil_splitSequence_t;
using HyperCube_t = Coll_t::HyperCube_t;
using IntervalBound_t = HyperCube_t::IntervalBound_t;
using IndexArray_t = Coll_t::IndexArray_t;
using ValArray_t = Coll_t::ValueArray_t;
using Node_t  = yggdrasil::yggdrasil_DETNode_t;


using namespace yggdrasil;

std::mt19937_64 geni(42);

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


using Dirichlet_t = ::yggdrasil::yggdrasil_multiDimDirichlet_t;



bool
isTreeFullyValid(
	const yggdrasil_DETree_t& 	tree)
{
	if(tree.checkIntegrity() == false)
	{
		std::cerr << "The tree diod not pass the integrity check." << std::endl;
		return false;
	};


	auto it = tree.getLeafIterator();
	for(; it.isEnd() == false; it.nextLeaf())
	{
		const auto nf = it.access();

		if(nf.isFullySplittedLeaf() == false)
		{
			std::cerr << "The node is not fully splitted." << std::endl;
			return false;
		};
		if(nf.checkIntegrity() == false)
		{
			std::cerr << "The node failed the integrty check." << std::endl;
			return false;
		};
	}; //ENd for(it)

	if(tree.isFullySplittedTree() == false)
	{
		std::cerr << "The tree did not appear to be fully fitted." << std::endl;
		return false;
	};


	return true;
}; //End tree






int
main()
{

	//These are the different sizes that we are testing
	Size_t diffSizes[5];
	diffSizes[0] = 100;
	for(int i = 1; i != 5; ++i)
	{
		diffSizes[i] = diffSizes[i - 1] * 10;
	};

	std::chrono::high_resolution_clock::time_point startT, endT;


	for(const Size_t N : diffSizes)
	{
		std::cout << "Handle Size: " << N << std::endl;

		// Test1
		{
			const Size_t D = 2;
			std::unique_ptr<yggdrasil_GOFTest_i> gof = std::unique_ptr<yggdrasil_GOFTest_i>(new yggdrasil_Chi2GOFTest_t(0.05));
			std::unique_ptr<yggdrasil_IndepTest_i> indep = std::unique_ptr<yggdrasil_IndepTest_i>(new yggdrasil_Chi2IndepTest_t(0.05));
			std::unique_ptr<yggdrasil_parametricModel_i> parMod = std::unique_ptr<yggdrasil_parametricModel_i>(new yggdrasil_linearModel_t("0"));
			std::unique_ptr<yggdrasil_splitFinder_i> splitFinder = std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil::yggdrasil_sizeSplit_t());

			Dirichlet_t::Alpha_t Alpha(2);
			Alpha << 1.0, 2.0;

			//Create a gaus
			Dirichlet_t dist(Alpha);

			//Generate some samples
			Dirichlet_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

			HyperCube_t invalDom = dist.getSampleDomain();

			startT = std::chrono::high_resolution_clock::now();
			yggdrasil_DETree_t tree(std::move(Coll_t(samples)), invalDom, splitFinder, parMod, gof, indep);
			endT = std::chrono::high_resolution_clock::now();

			const auto timeSpent = endT - startT;

			std::cout << " fitting the 2D tree: " << std::chrono::duration_cast<::std::chrono::seconds>(timeSpent).count() << "s" << std::endl;


			if(isTreeFullyValid(tree) == false)
			{
				//Out is done in function
				std::abort();
			};



		}; //End test 1

		// Test1.2
		{
			const Size_t D = 2;
			std::unique_ptr<yggdrasil_GOFTest_i> gof = std::unique_ptr<yggdrasil_GOFTest_i>(new yggdrasil_Chi2GOFTest_t(0.05));
			std::unique_ptr<yggdrasil_IndepTest_i> indep = std::unique_ptr<yggdrasil_IndepTest_i>(new yggdrasil_Chi2IndepTest_t(0.05));
			std::unique_ptr<yggdrasil_parametricModel_i> parMod = std::unique_ptr<yggdrasil_parametricModel_i>(new yggdrasil_constantModel_t(0));
			std::unique_ptr<yggdrasil_splitFinder_i> splitFinder = std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil::yggdrasil_sizeSplit_t());

			Dirichlet_t::Alpha_t Alpha(2);
			Alpha << 1.0, 2.0;

			//Create a gaus
			Dirichlet_t dist(Alpha);

			//Generate some samples
			Dirichlet_t::SampleList_t samples = dist.generateSamplesList(geni, N);

			//This is the repetition count
			const Size_t nRep = std::max<Size_t>(1, N * 0.01);
			std::uniform_int_distribution<Size_t> sSelect(0, N - 1);
			const Size_t maxCopies = ::std::min<Size_t>(10, 0.001 * N);
			std::uniform_int_distribution<Size_t> nCopyies(1, maxCopies);
			for(Size_t i = 0; i != nRep; ++i)
			{
				const Size_t idxToCopy = sSelect(geni);
				const Size_t nReplicas = (maxCopies == 0) ? 1 : nCopyies(geni);
				yggdrasil_sample_t src(samples[idxToCopy]);
				for(Size_t j = 0; j != nReplicas; ++j)
				{
					samples.push_back(src);
				};
			}; //End for(i)

			const Size_t nAdded = samples.size() - N;

			std::cerr << " -- Added sampel: " << nAdded << " " << (100.0 * nAdded / N) << "%" << std::endl;

			Coll_t sColl(samples);


			HyperCube_t invalDom = dist.getSampleDomain();

			startT = std::chrono::high_resolution_clock::now();
			yggdrasil_DETree_t tree(sColl, invalDom, splitFinder, parMod, gof, indep, false);
			endT = std::chrono::high_resolution_clock::now();

			const auto timeSpent = endT - startT;

			std::cout << " fitting the 2D tree: " << std::chrono::duration_cast<::std::chrono::seconds>(timeSpent).count() << "s" << std::endl;


			if(isTreeFullyValid(tree) == false)
			{
				//Out is done in function
				std::abort();
			};
		}; //End test 1




		//Test 2
		{
			const Size_t D = 4;
			std::unique_ptr<yggdrasil_GOFTest_i> gof = std::unique_ptr<yggdrasil_GOFTest_i>(new yggdrasil_Chi2GOFTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_IndepTest_i> indep = std::unique_ptr<yggdrasil_IndepTest_i>(new yggdrasil_Chi2IndepTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_parametricModel_i> parMod = std::unique_ptr<yggdrasil_parametricModel_i>(new yggdrasil_linearModel_t(D));
			std::unique_ptr<yggdrasil_splitFinder_i> splitFinder = std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil::yggdrasil_sizeSplit_t());

			Dirichlet_t::Alpha_t Alpha(4);
			Alpha << 6.13, 9.29, 10.6, 8.24;

			//Create a gaus
			Dirichlet_t dist(Alpha);

			//Generate some samples
			Dirichlet_t::SampleArray_t samples = dist.generateSamplesArray(geni, N);

			HyperCube_t invalDom = dist.getSampleDomain();

			startT = std::chrono::high_resolution_clock::now();
			yggdrasil_DETree_t tree(std::move(Coll_t(samples)), invalDom, splitFinder, parMod, gof, indep);
			endT = std::chrono::high_resolution_clock::now();

			const auto timeSpent = endT - startT;

			std::cout << " fitting the 4D tree: " << std::chrono::duration_cast<::std::chrono::seconds>(timeSpent).count() << "s" << std::endl;


			if(isTreeFullyValid(tree) == false)
			{
				//Out is done in function
				std::abort();
			};
		}; //End: Test2


		// Test2.2
		{
			const Size_t D = 4;
			std::unique_ptr<yggdrasil_GOFTest_i> gof = std::unique_ptr<yggdrasil_GOFTest_i>(new yggdrasil_Chi2GOFTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_IndepTest_i> indep = std::unique_ptr<yggdrasil_IndepTest_i>(new yggdrasil_Chi2IndepTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_parametricModel_i> parMod = std::unique_ptr<yggdrasil_parametricModel_i>(new yggdrasil_linearModel_t("0"));
			std::unique_ptr<yggdrasil_splitFinder_i> splitFinder = std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil::yggdrasil_sizeSplit_t());

			Dirichlet_t::Alpha_t Alpha(4);
			Alpha << 6.13, 9.29, 10.6, 8.24;

			//Create a gaus
			Dirichlet_t dist(Alpha);

			//Generate some samples
			Dirichlet_t::SampleList_t samples = dist.generateSamplesList(geni, N);

			//This is the repetition count
			const Size_t nRep = std::max<Size_t>(1, N * 0.01);
			std::uniform_int_distribution<Size_t> sSelect(0, N - 1);
			const Size_t maxCopies = ::std::min<Size_t>(10, 0.001 * N);
			std::uniform_int_distribution<Size_t> nCopyies(1, maxCopies);
			for(Size_t i = 0; i != nRep; ++i)
			{
				const Size_t idxToCopy = sSelect(geni);
				const Size_t nReplicas = (maxCopies == 0) ? 1 : nCopyies(geni);
				yggdrasil_sample_t src(samples[idxToCopy]);
				for(Size_t j = 0; j != nReplicas; ++j)
				{
					samples.push_back(src);
				};
			}; //End for(i)

			const Size_t nAdded = samples.size() - N;

			std::cerr << " -- Added sampel: " << nAdded << " " << (100.0 * nAdded / N) << "%" << std::endl;

			Coll_t sColl(samples);


			HyperCube_t invalDom = dist.getSampleDomain();

			startT = std::chrono::high_resolution_clock::now();
			yggdrasil_DETree_t tree(sColl, invalDom, splitFinder, parMod, gof, indep, false);
			endT = std::chrono::high_resolution_clock::now();

			const auto timeSpent = endT - startT;

			std::cout << " fitting the 4D tree: " << std::chrono::duration_cast<::std::chrono::seconds>(timeSpent).count() << "s" << std::endl;


			if(isTreeFullyValid(tree) == false)
			{
				//Out is done in function
				std::abort();
			};
		}; //End test 2.2



		// Test2.3
		{
			const Size_t D = 4;
			std::unique_ptr<yggdrasil_GOFTest_i> gof = std::unique_ptr<yggdrasil_GOFTest_i>(new yggdrasil_Chi2GOFTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_IndepTest_i> indep = std::unique_ptr<yggdrasil_IndepTest_i>(new yggdrasil_Chi2IndepTest_t(D, 0.05));
			std::unique_ptr<yggdrasil_parametricModel_i> parMod = std::unique_ptr<yggdrasil_parametricModel_i>(new yggdrasil_linearModel_t("0"));
			std::unique_ptr<yggdrasil_splitFinder_i> splitFinder = std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil::yggdrasil_sizeSplit_t());


			Dirichlet_t::Alpha_t Alpha(4);
			Alpha << 6.13, 9.29, 10.6, 8.24;

			//Create a gaus
			Dirichlet_t dist(Alpha);

			//Generate some samples
			Dirichlet_t::SampleList_t samples = dist.generateSamplesList(geni, N);

			Size_t w = 0;

			//This is the repetition count
			const Size_t nRep = std::max<Size_t>(1, N * 0.03);
			std::uniform_int_distribution<Size_t> sSelect(0, N - 1);
			const Size_t maxCopies = ::std::min<Size_t>(10, std::max<Size_t>(1, N * 0.001));
			std::uniform_int_distribution<Size_t> nCopyies(1, maxCopies);
			std::uniform_int_distribution<Size_t> dimSepect(0, D - 1);
			for(Size_t i = 0; i != nRep; ++i)
			{
				const Size_t idxToCopy = sSelect(geni);
				const Size_t nReplicas = (maxCopies == 0) ? 1 : nCopyies(geni);
				yggdrasil_sample_t src(samples[idxToCopy]);

				const Size_t selectedDim = dimSepect(geni);
				for(Size_t j = 0; j != nReplicas; ++j)
				{
					const Size_t dIdx = sSelect(geni);
					samples[dIdx][selectedDim] = src[selectedDim];
					w += 1;
				};
			}; //End for(i)

			//const Size_t nAdded = samples.size() - N;
			std::cout << " --- Treated samples: " << w << " " << (100.0 * w / N) << "%" << std::endl;

			//std::cerr << " -- Added sampel: " << nAdded << " " << (100.0 * nAdded / N) << "%" << std::endl;

			Coll_t sColl(samples);


			HyperCube_t invalDom = dist.getSampleDomain();

			startT = std::chrono::high_resolution_clock::now();
			yggdrasil_DETree_t tree(sColl, invalDom, splitFinder, parMod, gof, indep, false);
			endT = std::chrono::high_resolution_clock::now();

			const auto timeSpent = endT - startT;

			std::cout << " fitting the 4D tree: " << std::chrono::duration_cast<::std::chrono::seconds>(timeSpent).count() << "s" << std::endl;


			if(isTreeFullyValid(tree) == false)
			{
				//Out is done in function
				std::abort();
			};



		}; //End test 1
	}; //End for(N)


	return 0;
}; //End: main






