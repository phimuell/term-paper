/**
 * \brief	This file contains a small test for the dim array.
 */

#include <samples/yggdrasil_dimensionArray.hpp>
#include <algorithm>
#include <tuple>
#include <random>


using Array_t = yggdrasil::yggdrasil_dimensionArray_t;

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};

int
main(
	int 	argc,
	char**	argv)
{

	std::mt19937_64 geni(42);
#if 1
	std::uniform_real_distribution<double> dist(-10000.0, 10000.0);
	const int N = 10000;
	Array_t::ValueArray_t valArr;

	valArr.reserve(N);
	for(int i = 0; i != N; ++i)
	{
		valArr.push_back(dist(geni));
	};
#else
#endif

	//Test: 1
	{
		for(int d = 0; d != ::yggdrasil::Constants::YG_MAX_DIMENSIONS; ++d)
		{
			try
			{
				Array_t a(d);

				if(a.getAssociatedDim() != d)
				{
					std::cerr << "The associated dimesion is wroing. it should be " << d << " but is " << a.getAssociatedDim() << std::endl;
					std::abort();
					return 1;
				};
			}
			catch(yggdrasil::yggdrasil_exception& e)
			{
				std::cerr << "An unexpected ecetion has occured.\nThe test was:\n"
					<< e.what() << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(d)
	}; //End: Test 1


	//Test: 2
	{
		int invalidDim[] = {-1, ::yggdrasil::Constants::YG_MAX_DIMENSIONS, ::yggdrasil::Constants::YG_MAX_DIMENSIONS + 1};
		for(int d : invalidDim)
		{
			bool hasThrown = false;
			try
			{
				Array_t a(d);
			}
			catch(::yggdrasil::yggdrasil_exception& e)
			{
				hasThrown = true;
			};

			if(hasThrown == false)
			{
				std::cout << "The dimension " << d << " was not recognized illegal." << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(d)
	}; //End: Test 2


	//Test: 3
	{
		Array_t a(0, valArr);

		auto it = a.begin();
		for(int i = 0; i != N; ++i)
		{
			if(a[i] != a.at(i))
			{
				std::cout << "At position " << i << " the indexing stops working" << std::endl;
				std::abort();
				return 1;
			};

			if(a[i] != *it)
			{
				std::cout << "At position " << i << " the indexing iterator stops working" << std::endl;
				std::abort();
				return 1;
			};

			++it;

		}; //End for(i)

		bool wasThrown = false;
		try
		{
			const auto d = a.at(N);
		}
		catch(::yggdrasil::yggdrasil_exception& e)
		{
			wasThrown = true;
		};

		if(wasThrown == false)
		{
			std::cout << "The out of bound exccess was not captured." << std::endl;
			std::abort();
			return 1;
		};
	}; //End test: 3


	//Test: 4
	{
		for(int i = 0; i != 2 * N; ++i)
		{
			Array_t a(0);

			if(a.getNSamples() != 0)
			{
				std::cout << "The empty array has a size of " << a.nSamples() << std::endl;
				std::abort();
				return 1;
			};

			a.firstUsageInit(N, i);

			if(a.getNSamples() != N)
			{
				std::cout << "The allocating function does not work. the size is " << a.getNSamples() << std::endl;
				std::abort();
				return 1;
			};

			if(a.getCapacity() != std::max<std::size_t>(N, i))
			{
				std::cout << "The capacity is wrong, it is " << a.getCapacity() << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(i)
	}; //End test: 3



	//Test: 5
	{
		Array_t::IndexArray_t idx;
		for(int i = 0; i != N; i += 2)
		{
			idx.push_back(i);
		};

		Array_t a(0, valArr);

		if(a.nSamples() != N)
		{
			std::abort();
		};

		Array_t::ValueArray_t v = a.extractIndices(idx);

		for(int i = 0; i != idx.size(); ++i)
		{
			if(a.at(idx[i]) != v.at(i))
			{
				std::cerr << "The extracting does not work." << std::endl;
				std::abort();
				return 1;
			};
		};
	}; //End test: 5

	//Test: 6
	{
		Array_t a(0, valArr);

		Array_t::IndexArray_t idx = a.testAll([](const auto x) -> bool { return x <= 0.0;});
		auto p = [](const auto x) -> bool { return x <= 0.0;};


		if(a.nSamples() != N)
		{
			std::abort();
		};

		int c = 0;
		for(int i = 0; i != a.nSamples(); ++i)
		{
			if(p(a.at(i)))
			{
				if(idx.at(c) != i)
				{
					std::cerr << "Extrracting (test) does not work." << std::endl;
					std::abort();
					return 1;
				};
				c += 1;
			}
			else
			{
				if(c != idx.size())
				{
					if(idx.at(c) < i)
					{
						std::cerr << "Extracting does not work (test)" << std::endl;
						std::abort();
						return 1;
					};
				}
			};
		};

		if(c != idx.size())
		{
			std::cerr << "Somthing fischy is going on" << std::endl;
			std::abort();
			return 1;
		};
	}; //End test: 6

	//Test: 7
	{
		Array_t a(0);
		Array_t::ValueArray_t v(valArr);

		a.consumeValueArray(v);

		if(v.empty() == false)
		{
			std::cerr << "v is not empty." << std::endl;
			std::abort();
			return 1;
		};

		if(a.nSamples() != valArr.size())
		{
			std::cerr << "Not the correct size." << std::endl;
			std::abort();
			return 1;
		};

		for(int i = 0; i != a.nSamples(); ++i)
		{
			if(a.at(i) != valArr.at(i))
			{
				std::cout << "Problem with the loading." << std::endl;
				std::abort();
				return 1;
			};
		};


	}; //End test: 7





	return 0;

}; //End:  main



