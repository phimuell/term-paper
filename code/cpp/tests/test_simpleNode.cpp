/**
 * \brief	This file contains a small test for the node
 */

extern
bool
ygInternal_testSplitRoutine();

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <tree/yggdrasil_treeNode.hpp>

#include <para_models/yggdrasil_linModel.hpp>
#include <stat_tests/yggdrasil_IndepTest_chi2.hpp>
#include <stat_tests/yggdrasil_GOFTest_chi2.hpp>

#include <split_scheme/yggdrasil_sizeBasedSplitting.hpp>
#include <split_scheme/yggdrasil_scoreBasedSplitting.hpp>

#include <algorithm>
#include <tuple>
#include <random>

using Array_t = yggdrasil::yggdrasil_dimensionArray_t;
using Coll_t  = yggdrasil::yggdrasil_sampleCollection_t;
using SplitCom_t = yggdrasil::yggdrasil_singleSplit_t;
using SplitSeq_t = yggdrasil::yggdrasil_splitSequence_t;
using HyperCube_t = Coll_t::HyperCube_t;
using IntervalBound_t = HyperCube_t::IntervalBound_t;
using IndexArray_t = Coll_t::IndexArray_t;
using ValArray_t = Coll_t::ValueArray_t;
using Node_t  = yggdrasil::yggdrasil_DETNode_t;

using namespace yggdrasil;

std::mt19937_64 geni(42);

bool
isApprox(
	double 	a,
	double 	b)
{
	if(std::abs(a - b) < 100.0 * std::numeric_limits<double>::epsilon())
	{
		return true;
	}
	else
	{
		return false;
	};
};


bool
ygInternal_testSplitRoutine()
{
	const int D = 2;

	//Create the tests
	yggdrasil_Chi2GOFTest_t gof(D, 00.5);
	yggdrasil_Chi2IndepTest_t indep(D, 0.05);
	yggdrasil_linearModel_t parMod(D);

	{
		Array_t::ValueArray_t
			valArr1 = {0.25, 0.25, 0.75, 0.75},
			valArr2 = {0.25, 0.75, 0.25, 0.75};

		Array_t::ValueArray_t vL[] = {valArr1, valArr2};


		Coll_t c(D);

		for(int d = 0; d != D; ++d)
		{
			if(c[d].nSamples() != 0)
			{
				std::cerr << "Problems with the created dimesnions they have size." << std::endl;
				std::abort();
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "The valuid function is wrong." << std::endl;
				std::abort();
			};

			c.getDim(d).copyAndLoadValueArray(vL[d]);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
			return false;
		};

		HyperCube_t hc(D);
		Node_t node(c, &parMod, &gof, &indep, &hc);

		std::cout << "The node before the split:\n";
		node.printToStream(std::cout) << "\n";

		if(node.checkIntegrity() == false)
		{
			std::cerr << "The node is not integer." << std::endl;
			std::abort();
			return false;
		};
		SplitSeq_t split = SplitSeq_t().copyAndAddNewLevelOfSplit(0).copyAndAddNewLevelOfSplit(1);
		yggdrasil::yggdrasil_sizeSplit_t splitFinder;

		node.priv_splitNode(split, nullptr, &splitFinder);
		std::cout
			<< "The node after the split:\n"
			<< "   Please notice that due to the structure of this test, the model will not be fitted.\n";
		node.printToStream(std::cout) << "\n";
	};


	/*
	 * These thest has only two sampes.
	 * This means that two splits are empty.
	 */
	{
		Array_t::ValueArray_t
			valArr1 = {0.25, 0.25},
			valArr2 = {0.25, 0.75};

		Array_t::ValueArray_t vL[] = {valArr1, valArr2};

		Node_t::HyperCube_t hc(2, IntervalBound_t(0.0, 1.0));


		Coll_t c(D);

		for(int d = 0; d != D; ++d)
		{
			if(c[d].nSamples() != 0)
			{
				std::cerr << "Problems with the created dimesnions they have size." << std::endl;
				std::abort();
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "The valuid function is wrong." << std::endl;
				std::abort();
			};

			c.getDim(d).copyAndLoadValueArray(vL[d]);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
			return false;
		};

		Node_t node(c, &parMod, &gof, &indep, &hc);

		if(node.checkIntegrity() == false)
		{
			std::cerr << "The node is not integer." << std::endl;
			std::abort();
			return false;
		};
		SplitSeq_t split = SplitSeq_t().copyAndAddNewLevelOfSplit(0).copyAndAddNewLevelOfSplit(1);
		yggdrasil::yggdrasil_sizeSplit_t splitFinder;

		node.priv_splitNode(split, nullptr, &splitFinder);


		std::cout
			<< "\n\n\n"
			<< "This one should have two empty:\n";
		node.printToStream(std::cout) << "\n";
	};


	/*
	 * Here we make two splits
	 */
	{
		Array_t::ValueArray_t
			valArr1 = {0.25, 0.25},
			valArr2 = {0.25, 0.75};

		Array_t::ValueArray_t vL[] = {valArr1, valArr2};

		Node_t::HyperCube_t hc(2, IntervalBound_t(0.0, 1.0));


		Coll_t c(D);

		for(int d = 0; d != D; ++d)
		{
			if(c[d].nSamples() != 0)
			{
				std::cerr << "Problems with the created dimesnions they have size." << std::endl;
				std::abort();
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "The valuid function is wrong." << std::endl;
				std::abort();
			};

			c.getDim(d).copyAndLoadValueArray(vL[d]);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
			return false;
		};

		Node_t node(c, &parMod, &gof, &indep, &hc);

		if(node.checkIntegrity() == false)
		{
			std::cerr << "The node is not integer." << std::endl;
			std::abort();
			return false;
		};
		SplitSeq_t split = SplitSeq_t().copyAndAddNewLevelOfSplit(0);
		yggdrasil::yggdrasil_sizeSplit_t splitFinder;

		node.priv_splitNode(split, nullptr, &splitFinder);
	};

	/*
	 * This is a test for many samples
	 */
	{
		std::uniform_real_distribution<double> dist(-10000.0, 10000.0);
		const int N = 10000;
		const int D = 4;
		Array_t::ValueArray_t valArr1, valArr2, valArr3, valArr4;

		for(int i = 0; i != N; ++i)
		{
			valArr1.push_back(dist(geni));
			valArr2.push_back(dist(geni));
			valArr3.push_back(dist(geni));
			valArr4.push_back(dist(geni));
		};

		Array_t::ValueArray_t vL[] = {valArr1, valArr2, valArr3, valArr4};


		Coll_t c(D);

		for(int d = 0; d != D; ++d)
		{
			if(c[d].nSamples() != 0)
			{
				std::cerr << "Problems with the created dimesnions they have size." << std::endl;
				std::abort();
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "The valuid function is wrong." << std::endl;
				std::abort();
			};

			c.getDim(d).copyAndLoadValueArray(vL[d]);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
			return false;
		};

		HyperCube_t hc(D);
		Node_t node(c, &parMod, &gof, &indep, &hc);

		if(node.checkIntegrity() == false)
		{
			std::cerr << "The node is not integer." << std::endl;
			std::abort();
			return false;
		};
		SplitSeq_t split = SplitSeq_t().copyAndAddNewLevelOfSplit(0).copyAndAddNewLevelOfSplit(3);
		yggdrasil::yggdrasil_scoreSplit_t splitFinder;

		node.priv_splitNode(split, nullptr, &splitFinder);
		std::cout
			<< "\n\n\n"
			<< "This one should have many samples (omitted):\n";
		node.printToStream(std::cout, true) << "\n";
	};

	{
		const Size_t N = 5000;
		const int J[4] = {0, 1, 2, 3};

		//This are the index that corresponds for the accessing
		const int accFirst[4] = {0, 1, 0, 1};
		const int accSecon[4] = {0, 0, 1, 1};

		Array_t::ValueArray_t valArr1, valArr2;
		for(int k : J)
		{
			const int i = accFirst[k];
			const int j = accSecon[k];
			const double fi = (i == 0) ? 0.3 : 0.7;	//Thgis describes the first node that is splitted
			const double oi = (i == 0) ? 0.0 : 0.3;
			double fj, oj;
			if(i == 0)
			{
				fj = (j == 0) ? 0.8 : 0.2;
				oj = (j == 0) ? 0.0 : 0.8;
			}
			else
			{
				fj = (j == 0) ? 0.2 : 0.8;
				oj = (j == 0) ? 0.0 : 0.2;

			};
			std::uniform_real_distribution<double> dist1(oi + 0.01, oi + fi - 0.01);
			std::uniform_real_distribution<double> dist2(oj + 0.01, oj + fj - 0.01);
			for(Size_t kk = 0; kk != N; ++kk)
			{
				valArr1.push_back(dist1(geni));
				valArr2.push_back(dist2(geni));

				yggdrasil_assert(valArr1.back() <= 1.0);
				yggdrasil_assert(0.0 <= valArr1.back());
				yggdrasil_assert(valArr2.back() <= 1.0);
				yggdrasil_assert(0.0 <= valArr2.back());
			}; //End for(i):
			if(i == 0 & j == 0)
			{
				valArr1.push_back(dist1(geni));
				valArr2.push_back(dist2(geni));
			};
		}; //End for(j):

		std::vector<Size_t> idxArray(valArr1.size());
		for(Size_t i = 0; i != valArr1.size(); ++i)
		{
			idxArray[i] = i;
		};

		std::shuffle(idxArray.begin(), idxArray.end(), geni);
		Array_t::ValueArray_t v1(idxArray.size());
		Array_t::ValueArray_t v2(idxArray.size());
		for(Size_t i = 0; i != idxArray.size(); ++i)
		{
			v1[i] = valArr1[idxArray[i]];
			v2[i] = valArr2[idxArray[i]];
		};

		v1.swap(valArr1);
		v2.swap(valArr2);
		v1.clear();
		v2.clear();


		//Create an empty dim array
		Array_t dimArray1(0);
		dimArray1.copyAndLoadValueArray(valArr1);

		Array_t dimArray2(1);
		dimArray2.copyAndLoadValueArray(valArr2);


		Array_t::ValueArray_t vL[] = {valArr1, valArr2};


		Coll_t c(D);

		for(int d = 0; d != D; ++d)
		{
			if(c[d].nSamples() != 0)
			{
				std::cerr << "Problems with the created dimesnions they have size." << std::endl;
				std::abort();
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "The valuid function is wrong." << std::endl;
				std::abort();
			};

			c.getDim(d).copyAndLoadValueArray(vL[d]);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
			return false;
		};

		yggdrasil::yggdrasil_scoreSplit_t splitFinder;

		//Recreate the index array
		for(Size_t i = 0; i != valArr1.size(); ++i)
		{
			idxArray[i] = i;
		};

		HyperCube_t hcD = HyperCube_t::MAKE_UNIT_CUBE(2);
		SplitSeq_t testForPrefinder = SplitSeq_t().copyAndAddNewLevelOfSplit(0);
		const SplitCom_t f_1 = splitFinder.determineSplitLocation(hcD, testForPrefinder, idxArray.begin(), idxArray.end(), c.getDim(0));

		HyperCube_t hc = HyperCube_t::MAKE_UNIT_CUBE(2);
		//HyperCube_t hc = HyperCube_t::MAKE_INVALID_CUBE();
		Node_t node(c, &parMod, &gof, &indep, &hc);

		std::cout << "The node before the split:\n";
		node.printToStream(std::cout) << "\n";

		if(node.checkIntegrity() == false)
		{
			std::cerr << "The node is not integer." << std::endl;
			std::abort();
			return false;
		};
		SplitSeq_t split = SplitSeq_t().copyAndAddNewLevelOfSplit(0).copyAndAddNewLevelOfSplit(1);

		node.priv_splitNode(split, nullptr, &splitFinder);
		std::cout
			<< "The node after the split:\n"
			<< "   Please notice that due to the structure of this test, the model will not be fitted.\n";
		node.printToStream(std::cout, true) << "\n";

		std::cout
			<< "The first dimension should be splitted in at arround 0.3.\n"
			<< "The left node should be splitted at 0.8\n"
			<< "The right node at 0.2\n";

	};

	return true;

}; //End test split


int
main(
	int 	argc,
	char**	argv)
{

#if 1
	std::uniform_real_distribution<double> dist(-10000.0, 10000.0);
	const int N = 10000;
	const int D = 4;
	Array_t::ValueArray_t valArr1, valArr2, valArr3, valArr4;

	for(int i = 0; i != N; ++i)
	{
		valArr1.push_back(dist(geni));
		valArr2.push_back(dist(geni));
		valArr3.push_back(dist(geni));
		valArr4.push_back(dist(geni));
	};

	Array_t::ValueArray_t vL[] = {valArr1, valArr2, valArr3, valArr4};
#else
#endif

	//Create the tests
	yggdrasil_Chi2GOFTest_t gof(D, 0.05);
	yggdrasil_Chi2IndepTest_t indep(D, 0.05);
	yggdrasil_linearModel_t parMod(D);


	//Test 1
	{
		Coll_t c(D);

		for(int d = 0; d != D; ++d)
		{
			if(c[d].nSamples() != 0)
			{
				std::cerr << "Problems with the created dimesnions they have size." << std::endl;
				std::abort();
			};

			if(c.isValid() != (d == 0))
			{
				std::cerr << "The valuid function is wrong." << std::endl;
				std::abort();
			};

			c.getDim(d).copyAndLoadValueArray(vL[d]);
		}; //End for(d)

		if(c.isValid() == false)
		{
			std::cerr << "The collection is invalid." << std::endl;
			std::abort();
			return 1;
		};

		HyperCube_t hc(D);
		Node_t node(c, &parMod, &gof, &indep, &hc);

		if(node.checkIntegrity() == false)
		{
			std::cerr << "The node is not integer." << std::endl;
			std::abort();
			return 1;
		};
	};//End test 1


	if(ygInternal_testSplitRoutine() == false)
	{
		std::cerr << "Split failed." << std::endl;
		std::abort();
		return 1;
	};



	return 0;

}; //End:  main



