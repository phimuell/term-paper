/**
 * \brief	This file contains a small test for the descend way
 */

#include <util/yggdrasil_subDomainRanges.hpp>
#include <algorithm>
#include <random>
#include <tuple>


using SubDomRange_t = ::yggdrasil::yggdrasil_subDomainRanges_t;
using SingleSubRange_t = SubDomRange_t::SingleRange_t;
using SubDomEnum_t = ::yggdrasil::yggdrasil_subDomainEnum_t;



int
main(
	int 	argc,
	char**	argv)
{

	std::mt19937_64 geni(42);


#if 1
	std::uniform_real_distribution<double> dist(-10000.0, 10000.0);
	const int N = 10000;
	std::vector<double> r;
	std::vector<SubDomRange_t::SingleRange_t::Size_t> ind;

	r.reserve(N);
	ind.reserve(N);
	for(int i = 0; i != N; ++i)
	{
		r.push_back(dist(geni));
		ind.push_back(i);
	};
#else
	std::vector<double> r = {-7500.0, -2500.0, 2500.0, 7500.0};
	std::vector<SubDomRange_t::SingleRange_t::Size_t> ind = {0, 1, 2, 3};
	const int N = 4;

	std::shuffle(r.begin(), r.end(), geni);
#endif


	auto endTopPartition = std::partition(
		ind.begin(), ind.end(),
	 	[&r](const double& i) -> bool
	 	  {
	 		return (r[i] < 0.0) ? true : false;
		  }
		);

	auto endLeftPartition = std::partition(
		ind.begin(), endTopPartition,
	 	[&r](const double& i) -> bool
	 	  {
	 		return (r[i] < (-5000.0)) ? true : false;
		  }
		);

	auto endRightPartition = std::partition(
		endTopPartition, ind.end(),
	 	[&r](const double& i) -> bool
	 	  {
	 		return (r[i] < (5000.0)) ? true : false;
		  }
		);

	//Inserting into the range

	SubDomRange_t Range(2);
	if(Range.nSplits() != 2)
	{
		std::cerr << "The range does not eexpect 2 splits but " << Range.nSplits() << std::endl;
		std::abort();
		return 1;
	};
	if(Range.nSubRanges() != 4)
	{
		std::cerr << "The range does not expect 4 subranges, but " << Range.nSubRanges() << std::endl;
		std::abort();
		return 1;
	};
	const auto Base = ind.begin();

	//First Subrange -> LL
	{
		SubDomEnum_t E = SubDomEnum_t().decendToLeft().decendToLeft();
		SingleSubRange_t s(0, endLeftPartition - Base);

		if(s.isValid() == false)
		{
			std::cerr << "The first subrange is not valid." << std::endl;
			std::abort();
			return 1;
		};

		//Inserting into subrange
		Range.exchangeSubDomain(E, s);
	}; //End: first subrange



	//Second Subrange -> LR
	{
		SubDomEnum_t E = SubDomEnum_t().decendToLeft().decendToRight();
		SingleSubRange_t s(endLeftPartition - Base, endTopPartition - Base);

		if(s.isValid() == false)
		{
			std::cerr << "The second subrange is not valid." << std::endl;
			std::abort();
			return 1;
		};

		//Inserting into subrange
		Range.exchangeSubDomain(E, s);
	}; //End:

	//forth Subrange -> RR
	{
		SubDomEnum_t E = SubDomEnum_t().decendToRight().decendToRight();
		SingleSubRange_t s(endRightPartition - Base, ind.end() - Base);

		if(s.isValid() == false)
		{
			std::cerr << "The forth subrange is not valid." << std::endl;
			std::abort();
			return 1;
		};

		//Inserting into subrange
		Range.exchangeSubDomain(E, s);
	}; //End:

	//The find union must now throw an exception
	{
		bool findUnionProducedAnException = false;
		try
		{
			const auto fU = Range.findUnionRange();

			std::cerr << "The union test have not produced an exception. The result was: " << fU << std::endl;
			std::abort();
			return 1;
		}
		catch(yggdrasil::yggdrasil_exception& e)
		{
			findUnionProducedAnException = true;
		};

		if(findUnionProducedAnException != true)
		{
			std::cerr << "The union test have not produced an exception." << std::endl;
			std::abort();
			return 1;
		};
	}; //End: exception test


	//third Subrange -> RL
	{
		SubDomEnum_t E = SubDomEnum_t().decendToRight().decendToLeft();
		SingleSubRange_t s(endTopPartition - Base, endRightPartition - Base);

		if(s.isValid() == false)
		{
			std::cerr << "The third subrange is not valid." << std::endl;
			std::abort();
			return 1;
		};

		//Inserting into subrange
		Range.exchangeSubDomain(E, s);
	}; //End:


	//Tests if they where all stable
	try
	{
		const auto fU = Range.findUnionRange();

		if(fU != N)
		{
			std::cerr << "The union range is wrong, the found size was " << fU << ", instead of " << N << std::endl;
			std::abort();
			return 1;
		};
	}
	catch(yggdrasil::yggdrasil_exception& e)
	{
		std::cerr << "The find union range produced an exception, that was not expected.\nThe message was:\n"
			<< e.what() << std::endl;
		std::abort();
		return 1;
	};

	if(Range.allSubDomainsValid() == false)
	{
		std::cerr << "The Subdomains are not valid." << std::endl;
		std::abort();
		return 1;
	};

	//Now test if the predicate worked


	//First Subrange -> LL
	{
		SubDomEnum_t E = SubDomEnum_t().decendToLeft().decendToLeft();
		auto p = [](const double w) -> bool { return ((-10000.0 <= w) && (w < -5000.0)) ? true : false;};

		for(auto q = Range.getSubDomain(E).begin(); q != Range.getSubDomain(E).end(); ++q)
		{
			const auto tI = ind[q];
			if(p(r[tI]) == false)
			{
				std::cout << "The Sorting failed for index " << tI << ", the value was " << r[tI] << "; First Range, Way: " << E.getWay() << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(q)
	}; //End: first subrange

	//Second Subrange -> LR
	{
		SubDomEnum_t E = SubDomEnum_t().decendToLeft().decendToRight();
		auto p = [](const double w) -> bool { return ((-5000.0 <= w) && (w < 0.0)) ? true : false;};

		for(auto q = Range.getSubDomain(E).begin(); q != Range.getSubDomain(E).end(); ++q)
		{
			const auto tI = ind[q];
			if(p(r[tI]) == false)
			{
				std::cout << "The Sorting failed for index " << tI << ", the value was " << r[tI] << "; Second Range, Way: " << E.getWay() << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(q)
	}; //End:

	//Third Subrange -> RL
	{
		SubDomEnum_t E = SubDomEnum_t().decendToRight().decendToLeft();
		auto p = [](const double w) -> bool { return ((0.0 <= w) && (w < 5000.0)) ? true : false;};

		for(auto tI : Range.getSubDomain(E).loopOver(ind))
		{
			if(p(r[tI]) == false)
			{
				std::cout << "The Sorting failed for index " << tI << ", the value was " << r[tI] << "; Third Range, Way: " << E.getWay() << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(q)
	}; //End:

	//Forth Subrange -> RR
	{
		SubDomEnum_t E = SubDomEnum_t().decendToRight().decendToRight();
		auto p = [](const double w) -> bool { return ((5000.0 <= w) && (w < 10000.0)) ? true : false;};
		const auto subR = Range.getSubDomain(E);

		for(auto q = subR.begin(ind); q != subR.end(ind); ++q)
		{
			const auto tI = *q;
			if(p(r[tI]) == false)
			{
				std::cout << "The Sorting failed for index " << tI << ", the value was " << r[tI] << "; Third Range, Way: " << E.getWay() << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(q)
	}; //End:


	//Perform the search optimization
	Range.finalizeArrayStructure(ind);

	//First Subrange -> LL
	{
		SubDomEnum_t E = SubDomEnum_t().decendToLeft().decendToLeft();
		auto p = [](const double w) -> bool { return ((-10000.0 <= w) && (w < -5000.0)) ? true : false;};

		for(auto q = Range.getSubDomain(E).begin(); q != Range.getSubDomain(E).end(); ++q)
		{
			const auto tI = ind[q];
			if(p(r[tI]) == false)
			{
				std::cout << "The Sorting failed for index " << tI << ", the value was " << r[tI] << "; First Range, Way: " << E.getWay() << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(q)
	}; //End: first subrange

	//Second Subrange -> LR
	{
		SubDomEnum_t E = SubDomEnum_t().decendToLeft().decendToRight();
		auto p = [](const double w) -> bool { return ((-5000.0 <= w) && (w < 0.0)) ? true : false;};

		for(auto q = Range.getSubDomain(E).begin(); q != Range.getSubDomain(E).end(); ++q)
		{
			const auto tI = ind[q];
			if(p(r[tI]) == false)
			{
				std::cout << "The Sorting failed for index " << tI << ", the value was " << r[tI] << "; Second Range, Way: " << E.getWay() << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(q)
	}; //End:

	//Third Subrange -> RL
	{
		SubDomEnum_t E = SubDomEnum_t().decendToRight().decendToLeft();
		auto p = [](const double w) -> bool { return ((0.0 <= w) && (w < 5000.0)) ? true : false;};

		for(auto tI : Range.getSubDomain(E).loopOver(ind))
		{
			if(p(r[tI]) == false)
			{
				std::cout << "The Sorting failed for index " << tI << ", the value was " << r[tI] << "; Third Range, Way: " << E.getWay() << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(q)
	}; //End:

	//Forth Subrange -> RR
	{
		SubDomEnum_t E = SubDomEnum_t().decendToRight().decendToRight();
		auto p = [](const double w) -> bool { return ((5000.0 <= w) && (w < 10000.0)) ? true : false;};
		const auto subR = Range.getSubDomain(E);

		for(auto q = subR.begin(ind); q != subR.end(ind); ++q)
		{
			const auto tI = *q;
			if(p(r[tI]) == false)
			{
				std::cout << "The Sorting failed for index " << tI << ", the value was " << r[tI] << "; Third Range, Way: " << E.getWay() << std::endl;
				std::abort();
				return 1;
			};
		}; //End for(q)
	}; //End:


	return 0;

}; //End:  main



