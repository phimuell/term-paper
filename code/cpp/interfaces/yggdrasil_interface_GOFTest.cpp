/**
 * \brief	This file implements the functions for the GOF Test interface.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <interfaces/yggdrasil_interface_GOFTest.hpp>

//INcluide std

YGGDRASIL_NS_BEGIN(yggdrasil)

void
yggdrasil_GOFTest_i::endOfTestProcess()
{
	//Do Nothing
	return;
};

bool
yggdrasil_GOFTest_i::isGOFTest()
 const
{
	return true;
};


bool
yggdrasil_GOFTest_i::isIndependenceTest()
 const
{
	return false;
};


yggdrasil_GOFTest_i::~yggdrasil_GOFTest_i()
 noexcept = default;

yggdrasil_GOFTest_i::yggdrasil_GOFTest_i()
 noexcept = default;


yggdrasil_GOFTest_i::yggdrasil_GOFTest_i(
	const yggdrasil_GOFTest_i&)
 noexcept = default;


yggdrasil_GOFTest_i::yggdrasil_GOFTest_i(
	yggdrasil_GOFTest_i&&)
 noexcept = default;


yggdrasil_GOFTest_i&
yggdrasil_GOFTest_i::operator= (
	const yggdrasil_GOFTest_i&)
 noexcept = default;

yggdrasil_GOFTest_i&
yggdrasil_GOFTest_i::operator= (
	yggdrasil_GOFTest_i&&)
 noexcept = default;








YGGDRASIL_NS_END(yggdrasil)
