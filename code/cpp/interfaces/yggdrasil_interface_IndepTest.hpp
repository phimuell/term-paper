#pragma once
/**
 * \brief	This file implements the interface for all indepencence tests.
 *
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <util/yggdrasil_dimPair.hpp>

#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>

#include <interfaces/yggdrasil_interface_statisticalTests.hpp>
#include <interfaces/yggdrasil_interface_parametricModel.hpp>



//Incluide std
#include <memory>
#include <vector>


YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the node
class yggdrasil_DETNode_t;

//FWD of the builder
class yggdrasil_treeBuilder_t;

/**
 * \class 	yggdrasil_IndepTest_i
 * \brief	This class represents the basis for all independence tests.
 *
 * This class implements the interface for all the independence test
 * functionality. This means every independence  test must be derived
 * from this class and adhere to its requierements.
 * This interfaces extends the basic statistical test interface.
 *
 * As it is already noted in the base interface, this class has
 * made some changes, since it was first created.
 * The newest, and hopefully the last addition is the one,
 * that it operates only on the rescalled data space that is
 * defined by the leaf it is associated with.
 *
 * The workflow for the initial testing is the following.
 *
 * 	\li 	A single (serial) call to the preInitialHookInit() function, this will set up internal structures.
 * 	\li	A call to the precomputeDimension() fucntion on all dimensions, this can happen in parallel.
 * 	\li 	Calls to the initial fitting function with all possible dependencies, this can happens concurrently.
 * 	\li 	A single, serial call to the endOfTestProcess() function.
 * 	\li	The split sequence is now aviable.
 *
 * The workflow of the update process is as follow:
 * 	\li	A single, serial call to the preCOmmitHookInit() function, this will reset internal structure and prepare for the update.
 * 	\li	A call to the precomputeDimension() fucntion on all dimensions, this can happen in parallel.
 * 	\li	Calls to the commit function with all possioble dependencies, they can happen in parallel.
 * 	\li 	A single, serial call to the endOfTestProcess() function.
 * 	\li	The new split sequence is now aviable.
 *
 * The dependencies can be queried with teh getAllTestKeys() function.
 * The dimensions can be aquiered by the getAllPrecomputeKeys() function.
 * In contrast to the GOF test, here the test involves a dimension pair.
 */
class yggdrasil_IndepTest_i : public yggdrasil_statisticalTestsBase_i
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using HyperCube_t 	= yggdrasil_hyperCube_t;	//!< This is the type that represents a domain.
	using Sample_t 		= yggdrasil_sample_t;		//!< This is the type of a single sample.
	using NodeFacade_t 	= yggdrasil_nodeFacade_t;	//!< This is the node facade.
	using SplitSequence_t 	= yggdrasil_splitSequence_t;	//!< This is a type for managing a series of splits.
	using IndepTest_ptr	= std::unique_ptr<yggdrasil_IndepTest_i>;	//!< This is the managed pointer of the independence test.
	using DimList_t 	= ::std::vector<Int_t>;		//!< This is a list of the dimensions that are tested.
	using DimPair_t 	= yggdrasil_dimPair_t;		//!< This is a pair of dimensions.
	using DimPairList_t 	= ::std::vector<DimPair_t>;	//!< This is a list of all dimensions that is present.
	using ParModel_ptr 	= yggdrasil_parametricModel_i*;	//!< This is a pointer to the parameteric model
	using cParModel_ptr 	= const yggdrasil_parametricModel_i*;	//!< This is a pointer to a constant parameteric model.


	/*
	 * ===================================
	 * Constructor
	 *
	 * All constructrs are defaulted and protected
	 * The destructor is public and virtual,
	 * but an implementation is provided.
	 */
public:
	/**
	 * \brief	Destructor
	 *
	 * It is defaulted.
	 * But it is virtual
	 */
	virtual
	~yggdrasil_IndepTest_i()
	 noexcept;

	/**
	 * \brief	This function returns an empty copy of this.
	 *
	 * This function is used to propagate the test down the tree.
	 * Empty means that no data are associated to the newly crated instance.
	 *
	 * The unique pointer indicates that the calling function takes resposibility
	 * to manage it.
	 *
	 * \note 	From all spelling errors you encountered here, this one is intended.
	 */
	virtual
	IndepTest_ptr
	creatOffspring()
	 const
	 = 0;

	/**
	 * \brief	Given a buiolder and the indep tester model enum, a
	 * 		 indep tester will be created.
	 *
	 * \param  indepTester	The indep type that should be created.
	 * \param  builder	The builder that describes this a bit furtther.
	 *
	 * \note	This function is implemented in the factory folder.
	 */
	static
	IndepTest_ptr
	FACTORY(
		const eIndepTester 			indepTerster,
		const yggdrasil_treeBuilder_t&		builder);




	/*
	 * =============================
	 * =============================
	 * Interface functions
	 */








	/*
	 * =========================
	 *
	 * Initial testing function.
	 */
public:
	/**
	 * \brief	This function informs the object that the
	 * 		 initial testing is about to begin.
	 *
	 * For a detailed description see the super class.
	 *
	 * \throw	This function throws if *this is associated,
	 * 		 or internal function throws. However implementations
	 * 		 are encuraged to perform some sort of sanity checks.
	 *
	 * \note 	This is a remainder, that this fucntion is supposed to
	 * 		 create the associaation, and thsi is an after thought.
	 */
	virtual
	void
	preInitialHookInit(
		const NodeFacade_t 	nodeToAccosiate)
	 override
	 = 0;


	/**
	 * \brief 	this function performs preprocessing
	 * 		 on a single dimension \c d.
	 *
	 * This function is provided to do computatins that
	 * would requiere synchronisation, also important
	 * precomputations can be done, like setting up some
	 * dimension specific structures.
	 *
	 * This function must be thread safe, if the dimension
	 * \c d is different. It must be called once before
	 * the actual computation is done.
	 *
	 * If this function is called the very first time
	 * the function is guaranteed to get a leaf. If this
	 * fucntion is caled during the course of some updating
	 * then the the node must not be al leaf, howevber
	 * it is a true split, then.
	 *
	 * A list of all the precompute keys can be aquired by
	 * calling the getAllPrecomuteKeys() fucntion.
	 *
	 * Before this function is called the preInitialHookInit()
	 * fucntion has to be called.
	 *
	 * It is important that this fucntion does not associates
	 * *this to the node (this was changed).
	 *
	 * \param  d 		The dimension we act on.
	 * \param  parModel	This is the parameteric model that is needed.
	 * \param  subTreeRoot	This is the root of the subtree
	 * 			 that is rooted where *this
	 * 			 belonges to.
	 *
	 * \throw	This function may throw if called twice.
	 * 		 Implementations are encuraged to
	 * 		 do sanity checks.
	 *
	 * \note 	In earlier version of the interface this fucntionw as supposed
	 * 		 to create the bound between *this and the node.
	 * 		 However it was learned that thisis bad, so it was decided
	 * 		 that the associaation is created by the init function.
	 */
	virtual
	void
	preComputeDimension(
		const Int_t 		d,
		cParModel_ptr 		parModel,
		const NodeFacade_t& 	subTreeNode)
	 = 0;


	/**
	 * \brief	This function perform the
	 * 		 initial fitting of \c *this.
	 *
	 * Calling this function will perform an
	 * independence test between the dimensions
	 * specified by the dimension pair object \c dimPair
	 * that was passed as argument.
	 *
	 * All valid keys can be attained by calling the
	 * getAllTestKeys() function, it is recomended
	 * to do that.
	 *
	 * Before this function can be called the
	 * preComputeDimension() function has to be
	 * called.
	 *
	 * This function shall be thread safe as long
	 * as the dimension pair are different for different
	 * calls. It is also an error if the test is
	 * performed twiche on the same pair.
	 *
	 * \param  dimPair	The diomension pair that
	 * 			 should be tested
	 * \param  leaf		The leaf we test on.
	 *
	 * \return	Returns if the test was able to be done.
	 *
	 * \throw 	If the pair was tested before, if the dimension where illegal.
	 * 		 Implementations can impose further requieremtns.
	 * 		 Also if the implementatio was not able to fit.
	 */
	virtual
	bool
	performInitailTest(
		const DimPair_t 	dimPair,
		const NodeFacade_t& 	leaf)
	 = 0;


	/**
	 * \brief	This function is called by the implementation
	 * 		 to mark the end of teh testing process.
	 *
	 * For a detailed description see the fucntion in the super class.
	 *
	 * \throw 	It is general unspecoific under which conditions this function
	 * 		 raises an exception, however implementations are encuraged to
	 * 		 perform basic combability and sanety checks to ensure consistency
	 * 		 of *this.
	 */
	virtual
	void
	endOfTestProcess()
	 override
	 = 0;


	/*
	 * ===========================
	 * Split Inspection Functions
	 */
public:
	/**
	 * \brief	This function computes the split sequences.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 *
	 *
	 * \throw 	If \c *this is not in a consistent state.
	 * 		 If the object is not able to compute this sequence.
	 * 		 Implementaations can further strengthen this requirement.
	 * 		 Also if no tests were performed yet.
	 *
	 * \notice	In erlier versions of this interface, this function was
	 * 		 requiered to be thread safe. It was learned that this
	 * 		 requierement may not be able to fulfilled, so it was droped.
	 */
	virtual
	SplitSequence_t
	computeSplitSequence()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns true if at least one split has to be performed.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 */
	virtual
	bool
	areSplitsToPerform()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function checks if the split was the same.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 *
	 * \throw 	If there was no update of this is inconsistent.
	 * 		 Also some underlying functin may throw.
	 *
	 * \note 	This means that a false of this fucntion, means
	 * 		 that the splits have a new order.
	 */
	virtual
	bool
	areSplitsStable()
	 override
	 = 0;





	/*
	 * =================================
	 * Operational function
	 *
	 * These functions are not matematical, but from the implementation.
	 */
public:

	/**
	 * \brief	This function returns \c true if \c *this is consistent.
	 *
	 * See the super class for a detained descrioprion.
	 */
	virtual
	bool
	isConsistent()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns true if all
	 * 		 tests of \c *this are done.
	 *
	 * See the super class for a detained description.
	 *
	 * \throw	If *this is not associated.
	 */
	virtual
	bool
	allTestsDone()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns true if the test on the
	 * 		 dimPair was already performed.
	 *
	 * This fucntion only indicates if the test was done successfulls,
	 * meaning that no error was detected or so.
	 * It does not indicate if the test was accepted or not.
	 *
	 * This function returns true until the preCommitHookInit()
	 * function was called.
	 *
	 * \param  dimPair 	The dimension that should be tested.
	 *
	 * \throw 	If the test does not exist or *this is not associated.
	 *
	 * \note	This function was previously called "isDimensionsTested"
	 * 		 This was strange and a bit missleading, but consistent
	 * 		 with the GOF test. But it was descided to change the name.
	 */
	virtual
	bool
	isPairTested(
		const DimPair_t 	dimPair)
	 const
	 = 0;




	/*
	 * =====================
	 * None State Query Functions
	 *
	 * This functions gives information not about the
	 * state but of the, lets call it static state information.
	 * This is the stuff stat does nto change.
	 */
public:
	/**
	 * \brief	Ths function returns true if \c *this is a gof test.
	 *
	 * Since this interface is for independence tests, this function will allways return false.
	 */
	virtual
	bool
	isGOFTest()
	 const
	 final;


	/**
	 * \brief	This function returns true if *this
	 * 		 is \e associated with a node.
	 *
	 * See the super class for more information.
	 */
	virtual
	bool
	isAssociated()
	 const
	 override
	 = 0;



	/**
	 * \brief	Ths function returns true if \c *this is a gof test.
	 *
	 * Since this is an indep test, this function will always return true.
	 */
	virtual
	bool
	isIndependenceTest()
	 const
	 final;


	/**
	 * \brief	This fucntion returns the numbers of thest
	 * 		 that are managed by *this.
	 *
	 * This is most likely $(d - 1) * d / $, where d is the
	 * number of dimensions, but there is no requieret.
	 * If some test already can be decided, this function
	 * can skip them.
	 *
	 * However this function is allowed to return zero.
	 * There is no specification under which conditions
	 * this happens.
	 *
	 * This function requieres that *this associated with a node.
	 * It is undefined at which stage of the computation the keys are
	 * aviable. The only restriction is that they are aviable \e after the
	 * preInitialHookInit() function was called.
	 * They must also be aviable until the lifetime of *this end.
	 *
	 */
	virtual
	Size_t
	nDifferentTests()
	 const
	 override
	 = 0;



	/**
	 * \brief	This function returns a list (actually a vector)
	 * 		 that stores all the different interactions
	 * 		 that are monitored by *this.
	 *
	 * This function basically returns a vector that contains all dimneison
	 * pairs that are accepted by the fitting functions.
	 * It is guaranteed that all values that are acceped by the test fucntions
	 * are returned by this function.
	 * Some tests that could be decioded eqrly on can be skiped.
	 *
	 * This function is also allowed to return the empty list in case
	 * no tests are to be performed.
	 *
	 * This function does requiere that *this is associated with a node.
	 * It is undefined at which stage of the computation the keys are
	 * aviable. The only restriction is that they are aviable \e after the
	 * preInitialHookInit() function was called.
	 * They must also be aviable until the lifetime of *this end.
	 *
	 * \note	This function may be slow since the list may
	 * 		 be constructed on demand.
	 *
	 * \throw	This fuction may throw.
	 */
	virtual
	DimPairList_t
	getAllTestKeys()
	 const
	 = 0;


	/**
	 * \brief	This function returns the number of
	 * 		 dimensions that needs precomputing.
	 *
	 * This fucntion gives the number of dimensions
	 * that needs a preprocessing.
	 * A list of all keys (dimensions) that needs
	 * preprocessing can be attained by the
	 * getAppPrecomputeKesy() fucntion.
	 * No assumtion qout which dimensions are to be tested
	 * can be made from this value.
	 *
	 * This function does requiere that *this is associated with a node.
	 * It is undefined at which stage of the computation the keys are
	 * aviable. The only restriction is that they are aviable \e after the
	 * preInitialHookInit() function was called.
	 * They must also be aviable until the lifetime of *this end.
	 *
	 * \throw	It is not specified if this function throws.
	 */
	virtual
	Size_t
	nPreComputeDimensions()
	 const
	 = 0;


	/**
	 * \brief	This function returns a list of all dimensions
	 * 		 that needs preprocessing.
	 *
	 * The list returned by this function conatins all the keys
	 * that needs preprocessing, it is also the set of all keys
	 * that are accepted by the preprocessing fucntion.
	 * The length of the list can be attained by calling the
	 * nPreComputeDimensions() function.
	 *
	 * It is not guaranteed that the list are consecutive numbers
	 * starting at zero and goes up to the length of that list.
	 *
	 * This function does requiere that *this is associated with a node.
	 * It is undefined at which stage of the computation the keys are
	 * aviable. The only restriction is that they are aviable \e after the
	 * preInitialHookInit() function was called.
	 * They must also be aviable until the lifetime of *this end.
	 *
	 *
	 * \throw	It is not specified if thsi fucntion throws.
	 */
	virtual
	DimList_t
	getAllPrecomputeKeys()
	 const
	 = 0;





	/*
	 * ====================
	 * Update functions
	 *
	 * These functions are concerned with teh update functionality
	 */
public:
	/**
	 * \brief	This function adds the new sample \c nSample to \c *this.
	 *
	 * See the super class to see a description of the function.
	 *
	 *
	 * \throw 	It is not defined when an exception is raised.
	 * 		 However it is recomended that some sanaty checs are performed.
	 */
	virtual
	void
	addNewSample(
		const Sample_t&		nSample,
		const HyperCube_t&	domain)
	 override
	 = 0;


	/**
	 * \brief	This functions starts the updazte process of *this.
	 *
	 *
	 * \throw	This fucntion throws if *this is not associated or
	 * 		 if *this is \e consistent.
	 */
	virtual
	void
	preCommitHookInit()
	 override
	 = 0;


	/**
	 * \brief	Commit the changes of \c *this .
	 *
	 * This function will redo the test. It is like calling the initial
	 * fitting fucntion again, but with a node that must not be a leaf.
	 *
	 * This function must be thread safe for calls with
	 * different dimension pairs.
	 *
	 * This function is only for the updating of \c *this.
	 * To check if the tests are the same the function areSplitsStable() .
	 * has to be invoked.
	 *
	 * Before this function can be called the preCommitHookInit() function
	 * has to be called once.
	 * Further the function preComputeDimension() has to be called for each
	 * dimension once.
	 *
	 * The node \c subTreeRoot, is not guiaranteed to be a leaf.
	 * It can be a split, in which case all the leafes beneath are tested.
	 * Or a leaf.
	 *
	 * \param  dimPair	This is the dimension pair that should be retested.
	 * \param  subTreeRoot	This is the node where *this should be checked.
	 *
	 * \return 	This function returns true if the testing was successfull,
	 * 		 this means that the test could be done.
	 * 		 If false is returned yggdrasil will gnerate an exception.
	 *
	 * \throw 	It is not defined when an exception is raised.
	 * 		 However it is recomended that some sanaty checs are performed.
	 *
	 */
	virtual
	bool
	commitChanges(
		const DimPair_t 	dimPair,
		const NodeFacade_t& 	subTreeRoot)
	 = 0;






	/*
	 * =================================
	 * Protected constructors
	 */
protected:
	/**
	 * \brief	Default constructor
	 *
	 * Defaulted
	 */
	yggdrasil_IndepTest_i()
	 noexcept;


	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_IndepTest_i(
		const yggdrasil_IndepTest_i&)
	 noexcept;


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_IndepTest_i(
		yggdrasil_IndepTest_i&&)
	 noexcept;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_IndepTest_i&
	operator= (
		const yggdrasil_IndepTest_i&)
	 noexcept;

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_IndepTest_i&
	operator= (
		yggdrasil_IndepTest_i&&)
	 noexcept;

}; //End class: yggdrasil_IndepTest_i


YGGDRASIL_NS_END(yggdrasil)
