/**
 * \brief	This file implements the functions for the independence test.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <interfaces/yggdrasil_interface_IndepTest.hpp>

//INcluide std

YGGDRASIL_NS_BEGIN(yggdrasil)


void
yggdrasil_IndepTest_i::endOfTestProcess()
{
	//Do nothing
	return;
};



bool
yggdrasil_IndepTest_i::isGOFTest()
 const
{
	return false;
};


bool
yggdrasil_IndepTest_i::isIndependenceTest()
 const
{
	return true;
};


yggdrasil_IndepTest_i::~yggdrasil_IndepTest_i()
 noexcept = default;

yggdrasil_IndepTest_i::yggdrasil_IndepTest_i()
 noexcept = default;


yggdrasil_IndepTest_i::yggdrasil_IndepTest_i(
	const yggdrasil_IndepTest_i&)
 noexcept = default;


yggdrasil_IndepTest_i::yggdrasil_IndepTest_i(
	yggdrasil_IndepTest_i&&)
 noexcept = default;


yggdrasil_IndepTest_i&
yggdrasil_IndepTest_i::operator= (
	const yggdrasil_IndepTest_i&)
 noexcept = default;

yggdrasil_IndepTest_i&
yggdrasil_IndepTest_i::operator= (
	yggdrasil_IndepTest_i&&)
 noexcept = default;








YGGDRASIL_NS_END(yggdrasil)
