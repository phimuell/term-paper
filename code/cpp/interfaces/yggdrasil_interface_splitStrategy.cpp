/**
 * \brief	This file implements the split strategy.
 *
 * Contains the implementation.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <interfaces/yggdrasil_interface_splitStrategy.hpp>

//INcluide std

YGGDRASIL_NS_BEGIN(yggdrasil)

yggdrasil_splitFinder_i::~yggdrasil_splitFinder_i()
 noexcept = default;

yggdrasil_splitFinder_i::yggdrasil_splitFinder_i()
 noexcept = default;


yggdrasil_splitFinder_i::yggdrasil_splitFinder_i(
	const yggdrasil_splitFinder_i&)
 noexcept = default;


yggdrasil_splitFinder_i::yggdrasil_splitFinder_i(
	yggdrasil_splitFinder_i&&)
 noexcept = default;


yggdrasil_splitFinder_i&
yggdrasil_splitFinder_i::operator= (
	const yggdrasil_splitFinder_i&)
 noexcept = default;

yggdrasil_splitFinder_i&
yggdrasil_splitFinder_i::operator= (
	yggdrasil_splitFinder_i&&)
 noexcept = default;








YGGDRASIL_NS_END(yggdrasil)
