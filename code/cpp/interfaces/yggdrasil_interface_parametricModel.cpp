/*
 * \brief	This file contains the default implementaion for
 * 		 the functions of the parameteric model interface.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>

//INcluide std
#include <algorithm>

YGGDRASIL_NS_BEGIN(yggdrasil)


Numeric_t
yggdrasil_parametricModel_i::evaluateModelInSingleDim(
	const Int_t 		d,
	const Numeric_t 	x,
	const IntervalBound_t&	domain)
 const
{
	throw YGGDRASIL_EXCEPT_illMethod("This method can not be used.");
	(void)d;
	(void)x;
	(void)domain;
};


Numeric_t
yggdrasil_parametricModel_i::evaluateModelForSample(
	const Sample_t&		s,
	const HyperCube_t&	domain,
	const Size_t 		nTotSamples)
 const
{
	yggdrasil_assert(s.size() == domain.size());

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("No initial fit was performed.");
	};

	Numeric_t   res  = (Numeric_t(this->nSampleBase())) / (Numeric_t(nTotSamples));
	const Int_t N    = s.size();
	for(int i = 0; i != N; ++i)
	{
		res *= this->evaluateModelInSingleDim(s[i], i, domain[i]);
	}; //End for(i)

	return res;
};


yggdrasil_parametricModel_i::ParametricModel_ptr
yggdrasil_parametricModel_i::clone()
 const
{
	throw YGGDRASIL_EXCEPT_illMethod("The clone method is not implemented.");
};



std::string
yggdrasil_parametricModel_i::print(
	const std::string& 	intent)
 const
{
	return std::string(intent + std::string("MODEL(N.A.) = [??]"));
}; //End print


std::string
yggdrasil_parametricModel_i::print()
 const
{
	return this->print("");
};


yggdrasil_parametricModel_i::ValueArray_t
yggdrasil_parametricModel_i::evaluateModelInSingleDim(
	const ValueArray_t&	xSamples,
	const Int_t 		d,
	const IntervalBound_t&	domain)
 const
{
	yggdrasil_assert(d >= 0);
	yggdrasil_assert(d != Constants::YG_INVALID_DIMENSION);
	yggdrasil_assert(d <= Constants::YG_MAX_DIMENSIONS);

	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("No initial fit was performed.");
	};

	const Size_t N = xSamples.size();
	ValueArray_t Res(N);

	for(Size_t i = 0; i != N; ++i)
	{
		Res[i] = this->evaluateModelInSingleDim(xSamples[i], d, domain);
	}; //End for(i)

	return Res;
};




yggdrasil_parametricModel_i::DimList_t
yggdrasil_parametricModel_i::getAllDimensionKeys()
 const
{
	//get the number of dimensions
	const Int_t nDims = this->nDifferentFittings();

	//Test if zero, beter safe than sorry
	if(nDims == 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("There were no dimension to fit.");
	};
	if(nDims < 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("There were a negative number of dimensions.");
	};
	if(nDims >= Constants::YG_MAX_DIMENSIONS)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The maximal number of dimension was surrpassed.");
	};

	//Create the list and fill it
	DimList_t keys(nDims);

	for(Int_t d = 0; d != nDims; ++d)
	{
		keys.at(d) = d;
	}; //End for(d)

	return keys;
}; //End


bool
yggdrasil_parametricModel_i::allDimensionsFitted()
 const
{
	//Get all the domains
	const DimList_t kesy = this->getAllDimensionKeys();
	yggdrasil_assert(kesy.empty() == false);
	yggdrasil_assert((Int_t)kesy.size()  == this->nDifferentFittings());

	for(const Int_t d : kesy)
	{
		if(this->isDimensionFitted(d) == false)
		{
			return false;
		};
	}; //End for(d)

	return true;
}; //End


yggdrasil_parametricModel_i::~yggdrasil_parametricModel_i()
 noexcept = default;

yggdrasil_parametricModel_i::yggdrasil_parametricModel_i()
 noexcept = default;


yggdrasil_parametricModel_i::yggdrasil_parametricModel_i(
	const yggdrasil_parametricModel_i&)
 noexcept = default;


yggdrasil_parametricModel_i::yggdrasil_parametricModel_i(
	yggdrasil_parametricModel_i&&)
 noexcept = default;


yggdrasil_parametricModel_i&
yggdrasil_parametricModel_i::operator= (
	const yggdrasil_parametricModel_i&)
 noexcept = default;

yggdrasil_parametricModel_i&
yggdrasil_parametricModel_i::operator= (
	yggdrasil_parametricModel_i&&)
 noexcept = default;








YGGDRASIL_NS_END(yggdrasil)
