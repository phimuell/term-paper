/**
 * \brief	This file implements the functions for the basis interface of all statistical tests.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <interfaces/yggdrasil_interface_statisticalTests.hpp>

//INcluide std

YGGDRASIL_NS_BEGIN(yggdrasil)

bool
yggdrasil_statisticalTestsBase_i::areSplitsToPerform()
 const
{
	// No test, we hope the underling function will take care of that.

	const SplitSequence_t splitSeq = this->computeSplitSequence();

	if(splitSeq.isEmpty() == false)
	{
		//The split sequence is not empty, this
		//means there are split to be performed
		return true;
	};


	//No splits are there, so we return false
	return false;
}; //End: are splits to perfrom


void
yggdrasil_statisticalTestsBase_i::endOfTestProcess()
{
	//We do nothing
	return;
}; //End of test process



yggdrasil_statisticalTestsBase_i::~yggdrasil_statisticalTestsBase_i()
 noexcept = default;

yggdrasil_statisticalTestsBase_i::yggdrasil_statisticalTestsBase_i()
 noexcept = default;


yggdrasil_statisticalTestsBase_i::yggdrasil_statisticalTestsBase_i(
	const yggdrasil_statisticalTestsBase_i&)
 noexcept = default;


yggdrasil_statisticalTestsBase_i::yggdrasil_statisticalTestsBase_i(
	yggdrasil_statisticalTestsBase_i&&)
 noexcept = default;


yggdrasil_statisticalTestsBase_i&
yggdrasil_statisticalTestsBase_i::operator= (
	const yggdrasil_statisticalTestsBase_i&)
 noexcept = default;

yggdrasil_statisticalTestsBase_i&
yggdrasil_statisticalTestsBase_i::operator= (
	yggdrasil_statisticalTestsBase_i&&)
 noexcept = default;








YGGDRASIL_NS_END(yggdrasil)
