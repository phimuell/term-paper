#pragma once
/**
 * \brief	This file implements the general interface for the tests.
 *
 * This interface is only provided to provide a single base class
 * for both tests.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>

#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>



//Incluide std
#include <memory>


YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \class 	yggdrasilstatisticalTestsBase_i
 * \brief	This class represents the basis for all statistical tests.
 *
 * This interface is only the basis for the other two kinds of statistical
 * tests that are needed inside Yggdrasil.
 * Some functions that are, semantically, needed in both class are not
 * implemented here, but in the two subinterfaces, since the arguments
 * are different.
 *
 * This class defines a set of fucntions that are "portable" bettween
 * the two different usages of the tests.
 *
 * This is allready the thrid, probably, incrantion of this
 * interface. During the implemenation it was learned that some functions,
 * that where initially defined are not needed or have the wrong meaning.
 * Also some fucntions where missing althogether.
 * So this interface experied a lot of changes.
 *
 * It is important that this interfaces and the two subclasses all operates
 * on the rescalled data space that are defined by the leaf, they where
 * originally fitted one.
 *
 * This interface is not meant for direct implementation.
 * Rather the two subclasses should be implemented.
 */
class yggdrasil_statisticalTestsBase_i
{
	/*
	 * ============================
	 * Typedef
	 */
	using HyperCube_t 	= yggdrasil_hyperCube_t;	//!< This is the type that represents a domain.
	using Sample_t 		= yggdrasil_sample_t;		//!< This is the type of a single sample
	using SplitSequence_t 	= yggdrasil_splitSequence_t;	//!< This is a type for managing a series of splits
	using NodeFacade_t 	= yggdrasil_nodeFacade_t;	//!< This is the node facade class.


public:
	/*
	 * ===================================
	 * Constructor
	 *
	 * All constructrs are defaulted and protected
	 * The destructor is public and virtual,
	 * but an implementation is provided.
	 */
public:
	/**
	 * \brief	Destructor
	 *
	 * It is defaulted.
	 * But it is virtual
	 */
	virtual
	~yggdrasil_statisticalTestsBase_i()
	 noexcept;



	/*
	 * =============================
	 * =============================
	 * Interface functions
	 */

	/*
	 * ======================
	 * Test functions
	 *
	 * Here are some fucntions which deals with the testing.
	 * Not all fucntions that do testings are defined here.
	 * Some are also defined in the update section of this
	 * interface.
	 */
public:
	/**
	 * \brief	This function informs the object that the
	 * 		 initial testing is about to begin.
	 *
	 * This is the pendant to the preCommitHookInit() function
	 * that informs *this that the update starts.
	 * This fucntion is called exactly once before the
	 * initial tests starts.
	 * It is intended that *this can allocate the internal
	 * structures that are needed.
	 *
	 * This calling this fucntion associates *this with the node
	 * that is given as argument.
	 * The given node is guarantted to be a leaf.
	 *
	 * This function is abstract.
	 *
	 * \param  nodeToAssociate	This is the node that will be associate
	 * 				 to *this.
	 *
	 * \throw	This function throws if *this is associated,
	 * 		 or internal function throws. However implementations
	 * 		 are encuraged to perform some sort of sanity checks.
	 *
	 * \note 	In earlier versions this function did not the association.
	 * 		 However it was learned that it should probably associate
	 * 		 *this to the given node. This is why it now takes arguemnbts.
	 */
	virtual
	void
	preInitialHookInit(
		const NodeFacade_t 	nodeToAssociate)
	 = 0;


	/**
	 * \brief	This function is called by the implementation
	 * 		 to mark the end of teh testing process.
	 *
	 * It is provided such that implementations can clear internal tempoary
	 * storage. This is more a hack than anything else.
	 * It is intended such that statistics which are not updatable could
	 * free up memeory that is obsolete.
	 *
	 * This function provides a default implementation that does nothing.
	 * However the fucntion is abstract.
	 *
	 * This function shall not affect the change of \c *this.
	 * It is also very important to realize, that this function
	 * is not allowed to remove the association of *this and
	 * the node *this was initally tested on.
	 *
	 * It is requiered that this function computes the split sequences.
	 * Or at least bring the internal state in such a state that it can be computed
	 * by the compueSplitSequence() function.
	 *
	 *
	 * \throw 	It is general unspecoific under which conditions this function
	 * 		 raises an exception, however implementations are encuraged to
	 * 		 perform basic combability and sanety checks to ensure consistency
	 * 		 of *this.
	 */
	virtual
	void
	endOfTestProcess()
	 = 0;




	/*
	 * ===========================
	 * Split Inspection Functions
	 */
public:
	/**
	 * \brief	This function computes the split sequences.
	 *
	 * This function returns the split sequence that is associated with \c *this.
	 * In order to work \c *this must be in a consistent state and the tests
	 * must have been performed.
	 *
	 * This function is not thread safe, since it is expected to be called only once.
	 *
	 * If all tests are passed, meaning that no splits has to be performed,
	 * the function shall return the empty sequence.
	 *
	 * An empty sequences means that there are no splits to perform.
	 *
	 * The test sequence is only aviable after the endOfTestProcess()
	 * function was called. It remains aviable until the preCommitHookInit()
	 * function is called. It will be reaviable at the next call of the end fucntion.
	 *
	 *
	 * \throw 	If \c *this is not in a consistent state or *this is
	 * 		 not associated with data or no tests where performed.
	 * 		 Also if not aviable.
	 * 		 If the object is not able to compute this sequence.
	 * 		 Implementaations can further strengthen this requirement.
	 *
	 * \notice	In erlier versions of this interface, this function was
	 * 		 requiered to be thread safe. It was learned that this
	 * 		 requierement may not be able to fulfilled, so it was droped.
	 */
	virtual
	SplitSequence_t
	computeSplitSequence()
	 const
	 = 0;


	/**
	 * \brief	This function returns true if at least one split has to be performed.
	 *
	 * This function shall only be called after all tests are performed.
	 * This function has the same requieremmtnts as the the computeSplitSequence()
	 * function.
	 *
	 * This function provides a default implementation.
	 * The implementation generates the split sequence and tests if it is \e not empty.
	 * The default implementation does not perform any tests on its own, but relies
	 * that the requierements set forth by the underling function are meet.
	 */
	virtual
	bool
	areSplitsToPerform()
	 const;


	/**
	 * \brief	This function checks if the split was the same after an updated.
	 *
	 * In essents this function tests, if the outcome of the test is the same,
	 * in terms of the splitting sequence as it was upon the very first test.
	 * Hence the split (sequence) has tood stable and has not changed.
	 *
	 * It is important to note that also the indirect splits, that are
	 * generated by this split are associated to this result.
	 *
	 * This fucntion can only be called after the endOfTestProcess() function
	 * was called and at least one update was performed.
	 *
	 *
	 * \return 	True if the split is still the same as for the first time.
	 *
	 * \throw 	If there was no update of this is inconsistent.
	 * 		 Also some underlying functin may throw.
	 *
	 * \note 	This means that a false of this fucntion, means
	 * 		 that the splits have a new order.
	 */
	virtual
	bool
	areSplitsStable()
	 = 0;



	/*
	 * =================================
	 * Status fucntion
	 *
	 * This fucntion gives information about the state of
	 * *this.
	 *
	 */
public:
	/**
	 * \brief	This function returns \c true if
	 * 		 \c *this is consistent.
	 *
	 * A statistical test is consistent if no data was
	 * added since the last test was performed.
	 *
	 * In the case that *this is not associated to
	 * any node, there is a problem, should we return
	 * true or not. What is sure is, that calling this
	 * function in such a state, is not fully correct
	 * and is most likely an error, than any thing else.
	 * For this reason this function generates an
	 * exception if *this is not associated.
	 *
	 * \throw	If *this is not associated to any node.
	 *
	 * \note	This function was previously marked as \c noexcept.
	 *
	 */
	virtual
	bool
	isConsistent()
	 const
	 = 0;



	/**
	 * \brief	This function returns true if *this
	 * 		 is \e associated with a node.
	 *
	 * In generall terms, a test gets associated to a
	 * leaf/node when the (first) initial test is performed.
	 * This happens uppon the first call to the test function.
	 * Node and test enters a bound that is \e not possible
	 * to be removed. This connection also continue if
	 * the leaf, *this was initially tested on, is splitted.
	 *
	 * An important note about the meaning of the return value
	 * \c true of this function is, that it does not mean,
	 * that all tests were done. It simply means that
	 * at least one test was started.
	 *
	 * \note	This function was previously called "isEmpty()".
	 * 		 The naming was bad and missleading, so it was
	 * 		 changed to this. Also the meaning of the
	 * 		 function swaped, meaning that a true means
	 * 		 no associatation.
	 *
	 * \note	The gof and the independence test interface provide functions
	 * 		 to query the state of a specific test.
	 */
	virtual
	bool
	isAssociated()
	 const
	 = 0;


	/**
	 * \brief	This function returns true if all
	 * 		 tests of \c *this are done.
	 *
	 * Notice that unlike the association of *this with a
	 * leaf/node lasts until the end of the lifetime of
	 * the node, the "done" state of the test is shorter.
	 *
	 * A (specific) test, is considered done until, the
	 * update process starts. The update process is
	 * initialized by calling the preCommitHookInit()
	 * function, this function will reset all tests from
	 * done to not done.
	 * And so will do this fucntion.
	 *
	 * IT is also important that the return value of this
	 * function means that all tests are passed or not.
	 * It simply means that all tests could be successfully,
	 * in terms of "they produced \e some result", executed.
	 *
	 * \note	This function was previously implemented
	 * 		 by passing negative values to the functions
	 * 		 that queries the state of the individual
	 * 		 tests (functions were implemented in the
	 * 		 subclasses of \c *this), however this
	 * 		 was ugly and a bit confusing. therefore this
	 * 		 fucntion was implemented.
	 *
	 * \throw	If *this is not associated.
	 */
	virtual
	bool
	allTestsDone()
	 const
	 = 0;


	/*
	 * =====================
	 * None State Query Functions
	 *
	 * This functions gives information not about the
	 * state but of the, lets call it static state information.
	 * This is the stuff stat does nto change.
	 */
public:
	/**
	 * \brief	Ths function returns true if \c *this is a gof test.
	 *
	 * Either this or the the isIndependenceTest() function must return true.
	 */
	virtual
	bool
	isGOFTest()
	 const
	 = 0;

	/**
	 * \brief	Ths function returns true if \c *this is a gof test.
	 *
	 * Either this or the the isGOFTest() function must return true.
	 */
	virtual
	bool
	isIndependenceTest()
	 const
	 = 0;


	/**
	 * \brief	This function returns the number of
	 * 		 tests that *this managaes.
	 *
	 * What this number means is quite different in the
	 * different situations.
	 * For a GOF test this simply returns the number
	 * of diemnions. For an independence test, this
	 * function returns the number of interactions
	 * that could be dependent. This is most likely
	 * $(n - 1) * n / 2$. However different test
	 * could return different values.
	 *
	 * Under certain condition this fucntion is allowed to
	 * return zero. A GOF test is never allowed to do that.
	 * However an independence test is allowed to return zero,
	 * but only if there is just one dimension.
	 *
	 * \throw 	It is unspecific if thsi function throws.
	 *
	 * \note	There is a function that generates all teh
	 * 		 keys that are accepted. For technical
	 * 		 restriction this functions are defined
	 * 		 in the GOF and independence test
	 * 		 interfaces.
	 */
	virtual
	Size_t
	nDifferentTests()
	 const
	 = 0;





	/*
	 * ====================
	 * Update functions
	 *
	 * These functions are concerned with teh update functionality.
	 * The normal work flow is described in detail in the subclasses.
	 * Since the arguments of the different kind of tests is different,
	 * not all functions that are needed are implemented here.
	 * some of them are implemented in the underling subclasses.
	 */
public:
	/**
	 * \brief	This function adds the new sample \c
	 * 		 nSample to \c *this.
	 *
	 * This function orperates on the rescalled data range, that is given
	 * by the leaf/node *this is associated with.
	 * This means that the sample must be scalled, such that it lies in the
	 * unit hypercube.
	 *
	 * This function is used to incooperate new samples into the test.
	 * Like an update. It could be difficult to actually achieve that.
	 * Some test are just not able to be updatable. However if it is
	 * possible, this function is called to incooperate now data.
	 *
	 * If it is not possible then this function should have no effect.
	 *
	 * This function must not be thread safe.
	 * Yggdrassil will make sure, that this access to this function is serialized.
	 *
	 *
	 * \param  nSample	The new sample that should be added to \c *this.
	 * \param  domain 	This is the domain \c *this is reponsible for, rescalled.
	 *
	 * \throw	This function throws if *this is not associated or if
	 * 		 not all tests are done. implementation can specify
	 * 		 more restriction.
	 *
	 * \note 	This function must not have an effect if the statistic
	 * 		 can not be made updatable.
	 */
	virtual
	void
	addNewSample(
		const Sample_t&		nSample,
		const HyperCube_t&	domain)
	 = 0;


	/**
	 * \brief	This functions starts the updazte process of *this.
	 *
	 * This function is called once before the full update process stzarts.
	 * This fucntion is provided to set up some internal states, if needed.
	 *
	 * It is also important that this fucntion will set the state of the test
	 * from "done" to "undone". This means all tests, and every individual
	 * test, will return false, when querried about its state.
	 *
	 * This function must not be thread safe, since it is called only once.
	 * Calling this fucntion more than once, if not seperated by a endOfTestProcess()
	 * call is considered an error.
	 *
	 * This function does similar things as the preInitialHookInit() fucntion
	 * but in different situation. For example this function is not
	 * intended to allocate the internal structures but to reset them.
	 * Also this fucntion can be called multible times.
	 *
	 * It is also the duty of this function to save the old state of the split sequence
	 * such that it is able to be compared against a possible new outcome.
	 *
	 * \throw	This fucntion throws if *this is not associated or
	 * 		 if *this is \e consistent.
	 */
	virtual
	void
	preCommitHookInit()
	 = 0;





	/*
	 * =================================
	 * Protected constructors
	 */
protected:
	/**
	 * \brief	Default constructor
	 *
	 * Defaulted
	 */
	yggdrasil_statisticalTestsBase_i()
	 noexcept;


	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_statisticalTestsBase_i(
		const yggdrasil_statisticalTestsBase_i&)
	 noexcept;


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_statisticalTestsBase_i(
		yggdrasil_statisticalTestsBase_i&&)
	 noexcept;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_statisticalTestsBase_i&
	operator= (
		const yggdrasil_statisticalTestsBase_i&)
	 noexcept;

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_statisticalTestsBase_i&
	operator= (
		yggdrasil_statisticalTestsBase_i&&)
	 noexcept;

}; //End class: yggdrasil_statisticalTestsBase_i


YGGDRASIL_NS_END(yggdrasil)
