#pragma once
/**
 * \brief	This file implements the split strategy.
 *
 * This interface implements the functionality to find the split location.
 * It operates on index arrays.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitCommand.hpp>
#include <util/yggdrasil_splitSequence.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the building object
class yggdrasil_treeBuilder_t;

/**
 * \class 	yggdrasil_splitFinder_i
 * \brief	This class finds the location of a split.
 *
 * This class operates on pointers to a index array that maps into the dimension array.
 *
 * It is inmportant to note that they operate \e always on the rescalled data.
 * even in the intermediate splits
 * We can guarantee this since we rescale the data right after the split.
 * This means that the data that is passed to *this (rather implementation of this
 * interface) is inside the range [0, 1[.
 *
 * Also the return value must be interpretted in teh rescalled node data space ([0, 1[).
 */
class yggdrasil_splitFinder_i
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using IndexArray_t 	= yggdrasil_indexArray_t;	//!< This is the index Array
	using IndexArray_It 	= IndexArray_t::iterator;	//!< This is the iterator on which we work
	using DimensionArray_t 	= yggdrasil_dimensionArray_t;	//!< This is the dimeions array
	using SplitCommand_t 	= yggdrasil_singleSplit_t;	//!< This is the type that stores a split
	using SplitSequence_t 	= yggdrasil_splitSequence_t;	//!< This is the split sequence
	using HyperCube_t 	= yggdrasil_hyperCube_t;	//!< This is the hypercube type
	using IntervalBound_t 	= yggdrasil_intervalBound_t;	//!< This is the type that reprensents an interval
	using SplitStragety_ptr = ::std::unique_ptr<yggdrasil_splitFinder_i>;


	/*
	 * ===================================
	 * Constructor
	 *
	 * All constructrs are defaulted and protected
	 * The destructor is public and virtual,
	 * but an implementation isprovided.
	 */
public:
	/**
	 * \brief	Destructor
	 *
	 * It is defaulted.
	 * But it is virtual
	 */
	virtual
	~yggdrasil_splitFinder_i()
	 noexcept;

	/**
	 * \brief	This si the factory method.
	 *
	 * This construct a split stragety that is described by the
	 * building object.
	 *
	 * \param builder 	The building object.
	 *
	 * \note	This function is implemented in the factory fodler.
	 */
	static
	std::unique_ptr<yggdrasil_splitFinder_i>
	FACTORY(
		const yggdrasil_treeBuilder_t&	builder);






	/*
	 * ====================
	 * Interface functions
	 *
	 * Here are the functions that defines the interface
	 */
public:
	/**
	 * \brief	This function calculates the split location.
	 *
	 * Given an indexrange and the and sampels in the yet unsplited dimensionaly array,
	 * this function calculates the location of the split.
	 * The index range can be modified for aciving the computaion.
	 * The dimension where the split has to be is determined by the
	 * current head of the split sequence.
	 *
	 * It is important that the split position that is returnned, has to be
	 * with respect to the rescalled data range.
	 * This means that it must be in the range [0, 1[.
	 *
	 * The domain that is passed to *this is in the rescalled root data space.
	 * This mean sit describes the location of the node to split relative to the
	 * unit cube where we have mapped the original data to when we performed
	 * the initial rescalling to that cube.
	 *
	 *
	 * \param  domain	This is the full cube that has to be splitted. It is in the rescalled root data space.
	 * \param  spliSeq 	This is the split sequence, the head is used as dimension to select.
	 * \param  startRange 	This is an iterator to the first index that belongs to the samples that are splited
	 * \param  endRange 	This is the past the end iterator
	 * \param  dimArray 	This is all the samples
	 *
	 * \return 	This function returns a single split
	 *
	 * \post 	The dinmesion where the split has to be occure, as reported
	 * 		 by SplitCommand_t::getSplitDim has to be the same as the head of the split sequence.
	 *
	 * \note 	They are not aprticular usefull, but we allow splits of empty domains.
	 */
	virtual
	SplitCommand_t
	determineSplitLocation(
		const HyperCube_t& 		domain,
		const SplitSequence_t& 		splitSeq,
		IndexArray_It 			startRange,
		IndexArray_It 			endRange,
		const DimensionArray_t&		dimArray)
	 const
	 = 0;


	/**
	 * \brief	Thsi function returns a cloned version of \c *this.
	 *
	 * This function performs a cloning operation.
	 */
	virtual
	std::unique_ptr<yggdrasil_splitFinder_i>
	creatOffspring()
	 const
	 = 0;



	/*
	 * =================================
	 * Protected constructors
	 */
protected:
	/**
	 * \brief	Default constructor
	 *
	 * Defaulted
	 */
	yggdrasil_splitFinder_i()
	 noexcept;


	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_splitFinder_i(
		const yggdrasil_splitFinder_i&)
	 noexcept;


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_splitFinder_i(
		yggdrasil_splitFinder_i&&)
	 noexcept;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_splitFinder_i&
	operator= (
		const yggdrasil_splitFinder_i&)
	 noexcept;

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_splitFinder_i&
	operator= (
		yggdrasil_splitFinder_i&&)
	 noexcept;

}; //End class: yggdrasil_splitFinder_i


YGGDRASIL_NS_END(yggdrasil)
