#pragma once
/**
 * \brief	This file implements the interface for all GOF tests.
 *
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>

#include <samples/yggdrasil_sample.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>

#include <interfaces/yggdrasil_interface_statisticalTests.hpp>
#include <interfaces/yggdrasil_interface_parametricModel.hpp>



//Incluide std
#include <memory>


YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the node
class yggdrasil_DETNode_t;

//FWD of the builder
class yggdrasil_treeBuilder_t;

/**
 * \class 	yggdrasil_GOFTest_i
 * \brief	This class represents the basis for all GOF tests.
 *
 * This class implements the interface of the goodness of fitt (GOF)
 * functionality. This means every GOF test must be derived from this
 * class and adhere to its requierements.
 * This interfaces extends the basic statistical test interface.
 * The fucntions that are added are specific to the GOF fucntionality.
 *
 * As it is already noted in the base interface, this class has
 * made some changes, since it was first created.
 * The newest, and hopefully the last addition is the one,
 * that it operates only on the rescalled data space that is
 * defined by the leaf it is associated with.
 *
 * The workflow for the initial testing is the following.
 *
 * 	\li 	A single (serial) call to the preInitialHookInit() function, this will set up internal structures.
 * 	\li 	Calls to the initial fitting function with all dimensions, this can happens concurrently.
 * 	\li 	A single, serial call to the endOfTestProcess() function.
 * 	\li	The split sequence is now aviable.
 *
 * The workflow of the update process is as follow:
 * 	\li	A single, serial call to the preCOmmitHookInit() function, this will reset internal structure and prepare for the update.
 * 	\li	Calls to the commit function, they can happen in parallel.
 * 	\li 	A single, serial call to the endOfTestProcess() function.
 * 	\li	The new split sequence is now aviable.
 *
 * The list of all dimensions that have to be tested can be attained by a call to
 * getAllTestKeys() function.
 */
class yggdrasil_GOFTest_i : public yggdrasil_statisticalTestsBase_i
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using HyperCube_t 	= yggdrasil_hyperCube_t;	//!< This is the type that represents a domain.
	using Sample_t 		= yggdrasil_sample_t;		//!< This is the type of a single sample
	using Node_ptr 		= yggdrasil_DETNode_t*;		//!< This is a pointer to a node
	using cNode_ptr 	= const yggdrasil_DETNode_t*;	//!< This is a pointer to a constant node
	using NodeFacade_t 	= yggdrasil_nodeFacade_t;	//!< This is the node facade.
	using SplitSequence_t 	= yggdrasil_splitSequence_t;	//!< This is a type for managing a series of splits
	using GOFTest_ptr	= std::unique_ptr<yggdrasil_GOFTest_i>;	//!< This is the managed pointer of the GOF test
	using DimList_t 	= ::std::vector<Int_t>;		//!< This is a list of the dimensions that are tested.
	using ParModel_ptr 	= yggdrasil_parametricModel_i*;	//!< This is a pointer to the parameteric model
	using cParModel_ptr 	= const yggdrasil_parametricModel_i*;	//!< This is a pointer to a constant parameteric model.


	/*
	 * ===================================
	 * Constructor
	 *
	 * All constructrs are defaulted and protected
	 * The destructor is public and virtual,
	 * but an implementation is provided.
	 */
public:
	/**
	 * \brief	Destructor
	 *
	 * It is defaulted.
	 * But it is virtual
	 */
	virtual
	~yggdrasil_GOFTest_i()
	 noexcept;


	/**
	 * \brief	This function returns an empty copy of this.
	 *
	 * It is basically a substitute of a copy constructor.
	 *
	 * This function is used to propagate the test down the tree.
	 * Empty means that no data are associated to the newly crated instance.
	 *
	 * The unique pointer indicates that the calling function takes resposibility
	 * to manage it.
	 *
	 * \note 	From all spelling errors you encountered here, this one is intended.
	 */
	virtual
	GOFTest_ptr
	creatOffspring()
	 const
	 = 0;


	/**
	 * \brief	Given a buiolder and the GOF tester model enum, a
	 * 		 gof tester will be created.
	 *
	 * \param  gofTester	The gof type that should be created.
	 * \param  builder	The builder that describes this a bit furtther.
	 *
	 * \note	This function is implemented in the factory folder.
	 */
	static
	GOFTest_ptr
	FACTORY(
		const eGOFTester 			gofTester,
		const yggdrasil_treeBuilder_t&		builder);





	/*
	 * =============================
	 * =============================
	 * Interface functions
	 */

	/*
	 * ======================
	 * Test functions
	 *
	 * Here are some fucntions which deals with the testing.
	 * Not all fucntions that do testings are defined here.
	 * Some are also defined in the update section of this
	 * interface.
	 */
public:
	/**
	 * \brief	This function informs the object that the
	 * 		 initial testing is about to begin.
	 *
	 * For a detailed description see the super class.
	 *
	 * \throw	This function throws if *this is associated,
	 * 		 or internal function throws. However implementations
	 * 		 are encuraged to perform some sort of sanity checks.
	 *
	 * \note 	This is a remainder, that this fucntion is supposed to
	 * 		 create the associaation, and thsi is an after thought.
	 */
	virtual
	void
	preInitialHookInit(
		const NodeFacade_t 	nodeToAccosiate)
	 override
	 = 0;


	/**
	 * \brief	This function performs the initial test on
	 * 		 the given leaf and the diomension.
	 *
	 * This fucntion performs the first GOF test on the given leaf,
	 * represented by the \c leaf facade object and on dimension
	 * \c d. Thsi function assumes that the preInitHookInit() fucntion
	 * was called previously.
	 *
	 * The model object is the one that is bound to the leaf and
	 * it was fitted on the leaf. It is guaranteed to be associated,
	 * all dimensions are fitted and it is consistent.
	 *
	 * The test then computes the GOF statistics and stores it internaly.
	 *
	 * This function is requiered to be thread safe under the restrictions,
	 * that different calls to this function operates on different dimensions.
	 * The attempt to test a fucntion twice will lead to undefined behaviour
	 * and implementations are encuraged to detect and raise an exception.
	 *
	 * This function returns a bool that indicates if the test was able to
	 * complete. This does not mean that the null hypothesis was not rejected,
	 * it soley means that the test was successfully done.
	 * If this function retrns a false then igdrasil will raise an exception.
	 *
	 * After this function is returned the this dimension is condiedered done.
	 *
	 * This function is abstract.
	 *
	 * \param  parModel 	This is the parameteric model that will be used.
	 * \param  d 		The dimension that should be tested.
	 * \param  leaf 	The leaf node we want to test on.
	 *
	 * \throw 	If the dimension is out of range.
	 * 		 Also if *this is not possible to do the testing.
	 * 		 Also if a dimension is attemted to be tested twice.
	 * 		 Implementations are encuraged to test more.
	 * 		 And set stronger requierements.
	 *
	 * \note 	In earlier version of the interface this fucntionw as supposed
	 * 		 to create the bound between *this and the node.
	 * 		 However it was learned that thisis bad, so it was decided
	 * 		 that the associaation is created by the init function.
	 */
	virtual
	bool
	performInitailTest(
		cParModel_ptr 	parModel,
		NodeFacade_t 	leaf,
		const Int_t 	d)
	 = 0;


	/**
	 * \brief	This function is called by the implementation
	 * 		 to mark the end of teh testing process.
	 *
	 * For a detailed description see the fucntion in the super class.
	 *
	 * \throw 	It is general unspecoific under which conditions this function
	 * 		 raises an exception, however implementations are encuraged to
	 * 		 perform basic combability and sanety checks to ensure consistency
	 * 		 of *this.
	 */
	virtual
	void
	endOfTestProcess()
	 override
	 = 0;


	/*
	 * ===========================
	 * Split Inspection Functions
	 */
public:
	/**
	 * \brief	This function computes the split sequences.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 *
	 *
	 * \throw 	If \c *this is not in a consistent state.
	 * 		 If the object is not able to compute this sequence.
	 * 		 Implementaations can further strengthen this requirement.
	 * 		 Also if no tests were performed yet.
	 *
	 * \notice	In erlier versions of this interface, this function was
	 * 		 requiered to be thread safe. It was learned that this
	 * 		 requierement may not be able to fulfilled, so it was droped.
	 */
	virtual
	SplitSequence_t
	computeSplitSequence()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns true if at least one split has to be performed.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 */
	virtual
	bool
	areSplitsToPerform()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function checks if the split was the same.
	 *
	 * For a more detailed description, see the definition in the
	 * supercalss.
	 *
	 * \throw 	If there was no update of this is inconsistent.
	 * 		 Also some underlying functin may throw.
	 *
	 * \note 	This means that a false of this fucntion, means
	 * 		 that the splits have a new order.
	 */
	virtual
	bool
	areSplitsStable()
	 override
	 = 0;


	/*
	 * =================================
	 * Status fucntion
	 *
	 * This fucntion gives information about the state of
	 * *this.
	 *
	 */
public:
	/**
	 * \brief	This function returns \c true if
	 * 		 \c *this is consistent.
	 *
	 * For a extended description of the function see the super class.
	 *
	 * \throw	If *this is not associated to any node.
	 */
	virtual
	bool
	isConsistent()
	 const
	 override
	 = 0;



	/**
	 * \brief	This function returns true if *this
	 * 		 is \e associated with a node.
	 *
	 * See the superclass for an detained description.
	 */
	virtual
	bool
	isAssociated()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns true if all
	 * 		 tests of \c *this are done.
	 *
	 * See the super class for a detained description.
	 *
	 * \throw	If *this is not associated.
	 */
	virtual
	bool
	allTestsDone()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns true if the test
	 * 		 of dimension \c d was completted.
	 *
	 * This fucntion returns true if the test of dimension \c d was done.
	 * The value does not indicate if the test was accepted or not.
	 * It only indicates theat the test fucntion, the initial fitting fucntion
	 * or the commit function, on that dimension has been called and
	 * no error was detected.
	 *
	 * Notice that this fucntion will return true until the
	 * preCommitHookInit() function was called.
	 *
	 * \param  d 	The dimension to test.
	 *
	 * \throw	If *this is not associated or the dimension is invalid.
	 *
	 * \note	This function previously also acceped negative values.
	 * 		 Then it checked all the dimensions, this feature/abuse
	 * 		 was removed.
	 */
	virtual
	bool
	isDimensionTested(
		const Int_t 	d)
	 const
	 = 0;








	/*
	 * =====================
	 * None State Query Functions
	 *
	 * This functions gives information not about the
	 * state but of the, lets call it static state information.
	 * This is the stuff stat does nto change.
	 */
public:
	/**
	 * \brief	Ths function returns true if \c *this is a gof test.
	 *
	 * Since this is an GOF test for sure, this fucntion returns true.
	 */
	virtual
	bool
	isGOFTest()
	 const
	 final;

	/**
	 * \brief	Ths function returns true if \c *this is a gof test.
	 *
	 * Since this is a gof test for sure, this function returns false.
	 */
	virtual
	bool
	isIndependenceTest()
	 const
	 final;


	/**
	 * \brief	This function returns the number of
	 * 		 tests that *this managaes.
	 *
	 * This function returns the number of tests that must be done.
	 * No assumtion whioch tests must be done is given.
	 *
	 * it is only requiered that the value returned by this function
	 * is the size of the list that uis returned by the getAllTestKeys()
	 * function.
	 *
	 * This function does requiere that *this is associated with a node.
	 * It is undefined at which stage of the computation the keys are
	 * aviable. The only restriction is that they are aviable \e after the
	 * preInitialHookInit() function was called.
	 * They must also be aviable until the lifetime of *this end.
	 *
	 * \throw 	If the function ould attemp to return zero.
	 *
	 */
	virtual
	Size_t
	nDifferentTests()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns a list of the different
	 * 		 doimensions that are tested.
	 *
	 * More technically it returns all keys, all of them, that are accepted
	 * as a dimensional argument by the initial testing function or
	 * the commit function as a dimensional parameter.
	 *
	 * It is not requiered that they are consecuttive numbers.
	 * It is only requerred that this function returns all accepted keys.
	 *
	 * This function does requiere that *this is associated with a node.
	 * It is undefined at which stage of the computation the keys are
	 * aviable. The only restriction is that they are aviable \e after the
	 * preInitialHookInit() function was called.
	 * They must also be aviable until the lifetime of *this end.
	 *
	 * The size of the returned list may also be zero.
	 * This could happens if all the test are trivial.
	 *
	 * \throw	 Generally unspecific, but underling functions may throw.
	 */
	virtual
	DimList_t
	getAllTestKeys()
	 const
	 = 0;









	/*
	 * ====================
	 * Update functions
	 *
	 * These functions are concerned with teh update functionality
	 *
	 */
public:
	/**
	 * \brief	This function adds the new sample \c nSample to \c *this.
	 *
	 * See the super class to see a description of the function.
	 *
	 *
	 * \throw 	It is not defined when an exception is raised.
	 * 		 However it is recomended that some sanaty checs are performed.
	 */
	virtual
	void
	addNewSample(
		const Sample_t&		nSample,
		const HyperCube_t&	domain)
	 override
	 = 0;

	/**
	 * \brief	This functions starts the updazte process of *this.
	 *
	 * See superclass for a description.
	 *
	 * \throw	This fucntion throws if *this is not associated or
	 * 		 if *this is \e consistent.
	 */
	virtual
	void
	preCommitHookInit()
	 override
	 = 0;


	/**
	 * \brief	Commit the changes of \c *this .
	 *
	 * This functon will make the change that where introduced
	 * by the addition of new data of \c *this.
	 *
	 * This function is abstract and there is no
	 * default implementation.
	 *
	 * This function must be thread safe if the dimensions of
	 * different calls are different, which allows paralization.
	 * However calling this function with the same dimension
	 * multible time in the same update process will result
	 * in undefined behaviour.
	 *
	 * In order to call this function the preCommitInitHook()
	 * function has to be called once first.
	 *
	 * The node that is passed to this fucntion is the same
	 * as *this is associated with. However it is not
	 * guaranteed that the node is still a leaf, since it
	 * could be splited.
	 *
	 * \param  parModel	A pointer to the parameteric model.
	 * \param  subTreeRoot	The node where *this belongs to,
	 * 			 this must not be neccessaryaly a leaf.
	 * \param  d		The dimension that should be tested.
	 *
	 * \return 	This function returns true if the testing was successfull,
	 * 		 this means that all test could be done.
	 * 		 If false is returned yggdrasil will gnerate an exception.
	 *
	 * \throw	An exception is raised if the preCommitInitHook() was not called.
	 * 		 This also means that the dimension \c d is not checked.
	 * 		 Implementation can enforce more restrictive requierements.
	 */
	virtual
	bool
	commitChanges(
		cParModel_ptr 	parModel,
		NodeFacade_t 	subTreeRoot,
		const Int_t 	d)
	 = 0;




	/*
	 * =================================
	 * Protected constructors
	 */
protected:
	/**
	 * \brief	Default constructor
	 *
	 * Defaulted
	 */
	yggdrasil_GOFTest_i()
	 noexcept;


	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_GOFTest_i(
		const yggdrasil_GOFTest_i&)
	 noexcept;


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_GOFTest_i(
		yggdrasil_GOFTest_i&&)
	 noexcept;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_GOFTest_i&
	operator= (
		const yggdrasil_GOFTest_i&)
	 noexcept;

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_GOFTest_i&
	operator= (
		yggdrasil_GOFTest_i&&)
	 noexcept;

}; //End class: yggdrasil_GOFTest_i


YGGDRASIL_NS_END(yggdrasil)
