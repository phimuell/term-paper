#pragma once
/**
 * \brief	This file implements the interface for the parametric model.
 *
 * This is the general interface of the parametric model.
 * The parametric model is associated to the full cube and all dimesnions thereof.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_generalBinEdges.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>

#include <tree_geni/yggdrasil_multiDimCondition.hpp>

#include <factory/yggdrasil_treeBuilder_Enums.hpp>


//Incluide std
#include <memory>
#include <string>

YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the node
class yggdrasil_DETNode_t;

//FWD of the builder
class yggdrasil_treeBuilder_t;

/**
 * \class 	yggdrasil_parametricModel_i
 * \brief	This class represents the parameteric model associated to a node.
 *
 * The parametric model is our small/simple model that describes
 * the distribution inside a single element.
 *
 * It is described in detail in the technical specifications of yggdrasil.
 *
 * Its main purpose is to return the density estimates at a certain point.
 * It is implemented as an interface to enable many possible implementations thereof.
 *
 * As we have discribed in the specifications, we suggest to implementations,
 * that they are updatable, the interface is desigened in a way such
 * that also non-updatable implementations are possible.
 * Regardless if this is the case implementation must be hable to handle the add and commit
 * workflow.
 *
 * We want to make things clear, adding data to *this shall never invalidate the curent estimater.
 * It should still be able to evaluate the modell as if no datat where added yet.
 * Once the commit was performed the changes, the addition of data should take effect.
 *
 * On the thread safty side, we requiere that *this should be thread safe if non mutating functions
 * are called from several threads and on all dimensions.
 * In the case of mutable functions, we requier that as long as the mutation affects
 * different dimensions there should be no race condition.
 *
 * This is now somthing like the thrid version of the code.
 * The main differences compared to the second version is that this
 * version now followes the R-Code.
 * This also means that it has this inconsistency of the domain size
 * that is used to break ties.
 * In the second version it was assumend tha the domain paramter,
 * that is passed to the object, and also contained inside of the node.
 * Specifies the space that is occupied in real data space.
 * Now this has changed and it is only the size occupied in the
 * rescalled root data space, this means the unit hyper cube.
 *
 * The rescalling of the data into the different ranges introduces
 * a scalling factor that is equal to the inverse of the volume,
 * that is occupied in the original data space!
 * But we do not have that information at our disposal.
 * After some thinking we have descided to stick to the R-Code.
 * There the "objects" calculating the the value on the rescalled
 * root data space. In a second step they are transformed back to the
 * original sample space.
 *
 * We will do the same. This means the parametric objects have
 * to return values with respect to the rescalled root data space.
 *
 * It is very important to keep in mind, that the input that the
 * models process has to lie in the interval [0, 1[.
 * They are interpreted as beeing inside domain, relative to the
 * rescalled root data space, of the element they bound to.
 *
 *
 * There is also a different correcting factor that may or may not be applied.
 * This is the number of samples in a node's domain.
 *
 * WHat is also different from the R-Code is the fact that we use
 * theoretical quantiles, and not empirical one, to determine bins.
 * This menas that the code must be possible to compute quantiles.
 *
 * Generally this function operates on imputs that are spaced in the range [0, 1[.
 * However the domain that is passed to the code is expected to be relative to
 * the original data space.
 *
 * Simmilary to the tests that also forms a bound of themself to the
 * leaf they operate on, the models also following this approaces.
 * This was the case since the beginning. However was learned that the
 * then choosen approace was not so good.
 * It was then decided that the object should also get a prepare fucntion
 * and an end fucntion.
 *
 * The use case of thsi function is the following.
 * Before everything happens is it allocated.
 *
 * Then the first initial fitting ios done.
 *
 * \li	First the preFittingHookInit() function is called. This call bounds *this
 * 	 to the node that was given as argument.
 * \li	Then the function fitModelInDimension() is called for each dimension, this can
 * 	 happen in parallel and any order, as long as it is done once for each dimension
 * \li 	At the end of the fitting process you can call the endOfFittingProcess() function
 * 	 to inform the fucntion that the computaion has ended.
 *
 * THe workflow of the update is the following, that after some data where added to *this
 *
 * \li	The preCommitHookInit() function is called once to inform *thsi about the update.
 * \li	Then the commitChanges() fucntion is called on each dimension, which can happens in parallel
 * 	 and any order to refit the data.
 * \li	At the end the endOfFittingProcess() function is called to indicate the end.
 *
 *
 * Building the parameteric models.
 * A parameteric model can be builded explicitly or via the help of the builder concept.
 * The builder concept allwos a lightweight class to provide something that is close
 * to named arguments.
 * For that reasons all parameteric models must be able to bind late.
 * This means that the number of dimensions must not neccessaraly be known
 * at construction. The number of dimension can then be estimated using
 * the passed node at the preFittingHookInit() function.
 *
 * Also all implementation of *this interface has to accept a string for construction.
 * This way information can be passed to the implementation during construction.
 * This is full in the spirit of UNIX "Use test as interface, because it is universal",
 * also slow and ugly and messy, but this is an other story.
 *
 * \note 	For tecnical reasons it is relatively hard to keep the
 * 		 numbers of samples in the \e entier tree at all nodes correct.
 * 		 This is that this information is not known by an instance and
 * 		 must be provided by the user.
 * 		 However some functions, that only operate on a single dimension,
 * 		 did not need this information anyway.
 * 		 This means that this information, the number of the samples
 * 		 in the \e entier tree, must be provided by the user/caller if needed.
 */
class yggdrasil_parametricModel_i
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using ValueArray_t 		= yggdrasil_valueArray_t<Numeric_t>; 	//!< This is the value array class.
	using Sample_t 			= yggdrasil_sample_t;			//!< This is the type of a single sample
	using SampleCollection_t	= yggdrasil_sampleCollection_t;		//!< This is the sample collection
	using DimensionArray_t 		= SampleCollection_t::DimensionArray_t;	//!< This is the dimeions array
	using IntervalBound_t 		= yggdrasil_intervalBound_t;		//!< This is the type that reprensents an interval
	using HyperCube_t 		= yggdrasil_hyperCube_t;		//!< This is the type that represents a domain.
	using ParametricModel_ptr	= std::unique_ptr<yggdrasil_parametricModel_i>;	//!< This is the managed pointer of the clone function
	using cNode_ptr 		= const yggdrasil_DETNode_t*;			//!< This is the node type
	using NodeFacade_t 		= yggdrasil_nodeFacade_t;		//!< This is a convenient wrapper for the node.
	using DimList_t 		= ::std::vector<Int_t>;		//!< This is a list of the dimensions that are tested.
	using BinEdgeArray_t 		= yggdrasil_genBinEdges_t;		//!< This is a class for the bins.
	using MultiCondition_t 		= yggdrasil_multiCondition_t;		//!<This is a multi dim condition.



	/*
	 * ==============================
	 * Factory Method
	 */
public:
	/**
	 * \brief	This is the factory method of the aprameteric model.
	 *
	 * As argument this function takes a eParModel enum and
	 * then returns a parameteric model
	 *
	 */


	/*
	 * ===================================
	 * Constructor
	 *
	 * All constructrs are defaulted and protected
	 * The destructor is public and virtual,
	 * but an implementation is provided.
	 */
public:
	/**
	 * \brief	Destructor
	 *
	 * It is defaulted.
	 * But it is virtual
	 */
	virtual
	~yggdrasil_parametricModel_i()
	 noexcept;


	/**
	 * \brief	This function creates an empty copy of \c *this.
	 *
	 * The empty is to be understood, as that no data is associated with the
	 * newly created model.
	 * It is basically a newly constructed object.
	 *
	 * It returns a unique pointer.
	 * This is done to indicate that the responsibility is handed over to
	 * the calling routine.
	 *
	 * \note 	From all spelling errors you encountered here, this one is intended.
	 */
	virtual
	ParametricModel_ptr
	creatOffspring()
	 const
	 = 0;


	/**
	 * \brief	This function returns a clone of *this.
	 *
	 * Thsi function is like a copy constructor without the need to know
	 * the type. It differs from the creatOffSpling function in the sense
	 * that the returned model is bound to a node.
	 * It is an exact copy of *this.
	 * This function is primary needed in the tree generator.
	 *
	 * Note there is a default implementation that throws.
	 */
	virtual
	ParametricModel_ptr
	clone()
	 const;


	/**
	 * \brief	This function is the factory fucntion.
	 *
	 * Given the type and the builder object, it will create an
	 * Insitance of the parameteric model.
	 *
	 * \param  parModel 	The model that should be clreated.
	 * \param  builder	The builder to use.
	 *
	 * \note	This function is implemented in the factory fodler.
	 */
	static
	ParametricModel_ptr
	FACTORY(
		const eParModel 			parModel,
		const yggdrasil_treeBuilder_t&		builder);



	/*
	 * ====================
	 * ====================
	 * Interface functions
	 *
	 * Here are the functions that defines the interface
	 */

	/*
	 * ==================
	 * Fitting function
	 */
public:
	/**
	 * \brief	This is the precompute function for the fitting.
	 *
	 * This function is called exactly once before the fitting is done.
	 * It allws the implementation to postpone the allocation of the
	 * internal structure until they are needed.
	 *
	 * Calling this function indicates to *this that the fitting is about to begin.
	 *
	 * It is also important that calling this fuction will associate *this
	 * with the presented node, which is guaranteed to be a leaf.
	 *
	 * This function has to be called once at the beginning of the
	 * fittng process. It is not requiered to be thread safe.
	 *
	 * \param  nodeToAssociate	This is the node *this will associate
	 * 				 itself to.
	 *
	 * \throw 	If the node is not valid or this is allready associated.
	 *
	 * \note	This function is new.
	 */
	virtual
	void
	preFittingHookInit(
		const NodeFacade_t 	nodeToAssociate)
	 = 0;



	/**
	 * \brief	This function fits the parameteric model of \c *this.
	 *
	 * This function expects a leaf the paramettric model od diomension \c d
	 * is then created based on the information in the leaf.
	 *
	 * Calling this function for different \c d from many threads must be
	 * safe.
	 * This allows to build the model in parallel.
	 * A dimension can only be build once and shall create an error if attempted.
	 *
	 * The leaf pointer must be the same for every dimension.
	 * Implementations are not requiered to check this.
	 *
	 * The data in the leaf must be scalled in the range [0, 1[.
	 * Implementations are not requiered to check that nut encouraged to do so.
	 *
	 * The argument \c leaf is guaranteed to be a leaf node.
	 *
	 * The assocoationb to the leaf was foprmed by the calling of the preFittingHookInit()
	 * function.
	 *
	 * \return 	True if the fit was possible, since the contrary must triger an
	 * 		 exception this function will always return true.
	 *
	 * \param  leaf 	The leaf used to build the model.
	 * \param  d 		The dimensionsion that should be builded.
	 *
	 * \throw 	If the model is unable to fit, or underling functions.
	 * 		 Implementations can permitted to strengthen the requierements.
	 * 		 Also if *thsi is allready associated.
	 */
	virtual
	bool
	fitModelInDimension(
		NodeFacade_t 	leaf,
		const Int_t 	d)
	 = 0;


	/**
	 * \brief	This function informs *thsi that the fitting has ended.
	 *
	 * This function allows *this to deallocate some internal structures that
	 * are maight be allocated and now are not needed any longer.
	 *
	 * This fucntion is nott allowed to remove the associateion of *this
	 * and the leaf it was fitted on.
	 * Nor shall it end the fitting state of teh dimensions
	 *
	 * This function is called once after the fitting.
	 * It is not requiered to be thread safe.
	 *
	 * \throw	If not all dimensions are fitted or this is not associated at all
	 * 		 Also if internal consistency chekks fail
	 */
	virtual
	void
	endOfFittingProcess()
	 = 0;


	/*
	 * =======================
	 * State functions
	 *
	 * These are the state fucntions that may change during the
	 * computation as they can beppends if updaztes are running or not.
	 */
public:
	/**
	 * \brief	Thsi function returns true if dimension \c d of \c *this is fitted.
	 *
	 * If the fitting process in one dimension was successfully.
	 * A return value of \c true, means that the object was able to compute
	 * the paramter for that dimensoion and no error was detected.
	 * It is important to understand that this fucntion can also be called
	 * if *this is not associated to any data.
	 *
	 * \param  d 	The dimension to check.
	 *
	 * \throw 	If d exceeds the dimensions.
	 *
	 * \note	Earlier versions of the interface also allowed a negativ
	 * 		 value of the dimension. This feature was removed and is now considered
	 * 		 an error.
	 */
	virtual
	bool
	isDimensionFitted(
		const Int_t 	d)
	 const
	 = 0;



	/**
	 * \brief	This function returns \c true if \c *this
	 * 		 is consistent.
	 *
	 * A model is consisten, if since it was last updated/commited
	 * no changes (no samples where added) where performed.
	 * Another view is that a commit is needed.
	 *
	 * This function will only return true if \e all dinension are consistent.
	 *
	 * \throw 	If \c *this is not associated to data, meaning no
	 * 		 fit was ever performed, then this function throws.
	 */
	virtual
	bool
	isConsistent()
	 const
	 = 0;



	/**
	 * \brief	THis function tests if all tests where performed.
	 *
	 * This function provides a not so efficient default implementation.
	 * The function is abstract.
	 * It is important to understand that this fucntion can also be called
	 * if *this is not associated to any data.
	 *
	 * \throw	If an underling fucntion throws
	 */
	virtual
	bool
	allDimensionsFitted()
	 const
	 = 0;




	/*
	 * ====================
	 * Information functions
	 *
	 * This functions give access to the static information of *this.
	 * This are informations that are not depends on the update state of *this.
	 * Like the association, once it is established, it will continue to
	 * exists.
	 */
public:
	/**
	 * \brief	This function returns true if \c *this is not associated to any data.
	 *
	 * A return value \c true, means that at least one dimensionw was fitted.
	 * This happens upon the first call to fitModelInDimension().
	 * The bound can not be removed.
	 *
	 *
	 * \note	This fucntion was previously called 'isEmpty()'.
	 * 		 The name wead unsuitable for that, alning was
	 * 		 inverse to the one it had now.
	 * 		 The 'isEmpty()' fucntion was removed.
	 */
	virtual
	bool
	isAssociated()
	 const
	 = 0;


	/**
	 * \brief	This function return a string that can be used to inform the user.
	 *
	 * This string is used in the print function.
	 * Its stile should be somthing like:
	 * 	MODEL(WhatTypeIAm) = [Paramaters]
	 *
	 * Thsi function has a default implementation.
	 * However it is recomended that the models provide one.
	 *
	 * The function takes the as optional argument the intent.
	 * This argument is needed if multiple lines are printed.
	 *
	 * The first intent is allready applied by the
	 * calling code, it only has to be used in the case that
	 * more than one line are to be written.
	 *
	 * \param  intent	This is the intent that is used.
	 */
	virtual
	std::string
	print(
		const std::string& 	intent)
	 const;


	/**
	 * \brief	This is the default version of the printing fucntion.
	 *
	 * This version is final and calls teh intent function, and uses
	 * the empty string as intent.
	 */
	virtual
	std::string
	print()
	 const
	 final;


	/**
	 * \brief	This function returns the number of different dimensions.
	 *
	 * This function returns the number of different dimensions that are fitted.
	 * This function shall never return zereo.
	 * An exception must be raised if this happens.
	 *
	 * \throw 	If the return value is zero.
	 */
	virtual
	Int_t
	nDifferentFittings()
	 const
	 = 0;


	/**
	 * \brief	This fucntion returns a vector that contains the
	 * 		 keys (dimension \c d) that are accepted by the
	 * 		 fitting fucntion as dimensional argument.
	 *
	 * In general this are the numbers 0 up to nDifferentFittings() - 1.
	 * This function must guarante that all accepted dimensional
	 * arguments must be contained in the return value.
	 *
	 * This function is primarly provioded, since the tests provides them.
	 *
	 * For this fucntion there is a default implementation.
	 * That must not be called ecplicitly.
	 *
	 * \throw 	This fucntion throws if the list is empty.
	 */
	virtual
	DimList_t
	getAllDimensionKeys()
	 const;

	/**
	 * \brief	This function returns the numbers of samples the estimate is based on.
	 *
	 * In other terms thsi function returns the numbers of samples that are
	 * located inside the domain that \c *thsi was fitted on.
	 * It is very important that this is number os associated with
	 * with the fit and encludes the samples which where added since
	 * the last updates.
	 *
	 * This function is abstract.
	 *
	 * \throw 	If \c *this was not yet fitted an exception should be raised.
	 */
	virtual
	Size_t
	nSampleBase()
	 const
	 noexcept(false)
	 = 0;



	/*
	 * ===================
	 * PDF Function
	 *
	 * These functions allows to query the density.
	 * Some of them operate on just one dimension, so they do not
	 * need the number of samples in the full tree.
	 * These function who need them will need them as an argument.
	 *
	 * So keep the discription of the function short the factor,
	 * \f$ n(C_k) / n \f$, form the paper, will henceforce only be
	 * called the scalling factor.
	 *
	 * There is the other factor that is introduced by the
	 * rescalling of the data, such that it allways stays inside
	 * the interval [0, 1[, function must allways expect that the data
	 * is inside that range, the data is then interpreted to lie
	 * inside the domain they where fitted on, relative to the
	 * rescalled root data space.
	 *
	 * The value that are returned are also relative to the full
	 * rescalled root data space.
	 */
public:
	/**
	 * \brief	This function evalutes a the model of dimension
	 * 		 \c d at location \c x, in that dimension.
	 *
	 * This function expects the value \c x inside the range [0, 1[.
	 *
	 * This function is a basic function it is provided because it
	 * is very generall and a lot of more involved functions can be
	 * reduced to the needs, that this function can provide.
	 * It is not recomended to use it, since it is not particular efficient.
	 *
	 * The function does not need to check if \c x is inside the valid domain.
	 * This assumes that \c x is inside the domain.
	 *
	 * This function operates on just one dimension so the scalling factor,
	 * the number of the samples, has not to be included in the result.
	 * However the factor that is introduced by the rescalling of the domain has
	 * to be included such that the result can be interpreted on the rescalled
	 * root data space.
	 *
	 * This function is abstract.
	 *
	 * \param  d 		This is the dimension we evaluate the model for
	 * \param  x 		This is the location of the model we evaluate the model for.
	 * \param  domain	This is the domain bound form the hypercube \c *this is defined on.
	 * 			 The value is relative to the rescalled root data space.
	 *
	 * \note	This function must be thread safe.
	 *
	 * \throw 	The implementation is free to generate exceptions.
	 */
	virtual
	Numeric_t
	evaluateModelInSingleDim(
		const Numeric_t 	x,
		const Int_t 		d,
		const IntervalBound_t&	domain)
	 const
	 = 0;


	/**
	 * \brief	This function is present to avoid casting
	 *
	 * This function throws an exception and can not be overwritten.
	 */
	virtual
	Numeric_t
	evaluateModelInSingleDim(
		const Int_t 		d,
		const Numeric_t 	x,
		const IntervalBound_t&	domain)
	 const
	 final;


	/**
	 * \brief	This function is used to evaluate the model
	 * 		 in a single dimension for many values.
	 *
	 * This function also operates on the unit interval [0, 1[.
	 * This means the rescalling has to ba applied already.
	 * Further this function is not requiered to check that.
	 *
	 * This is a fuction that is used to evaluate the model
	 * at many location in the same dimension.
	 * All locations are interpreted as coordinate in dimension \c d .
	 * This means that only one dimension can be queried at a time.
	 *
	 * There is a default implementation that is reduced to
	 * the single dimension function.
	 * But the implementation is very lousy, but portable.
	 *
	 * Since this function operates just in one dimension,
	 * the scaling factor that stemes from the number of particles
	 * has not to be included in the final result.
	 * However the corrections due to the rescalling of the intervals
	 * has to be applied such that the data can be interpreted
	 * in the rescalled root data space.
	 *
	 * This function has to be thread safe.
	 *
	 * \param  X		These are the sample positins where the model should be evaluated.
	 * \param  d		The dimension we are interested in.
	 * \param  domain	This is the domain \c *this was fitted on only the corresponding interval.
	 * 			 The value is relative to the rescalled root data space.
	 *
	 * \throw	It is unspecific if the implementation throws, hower some sanity tests are recomended.
	 *
	 * \return 	Returns the evaluation of the model only in dimension \c d for the samples locatied at \c X
	 *
	 * \note 	This function was renamed.
	 */
	virtual
	ValueArray_t
	evaluateModelInSingleDim(
		const ValueArray_t&	xSamples,
		const Int_t 		d,
		const IntervalBound_t&	domain)
	 const
	 = 0;


	/**
	 * \brief	This function is able to evaluate the model at sample location \c x.
	 *
	 * This is a method that operates on a smaple.
	 * This is the method that is called after a query operation.
	 *
	 * The coordinate of the sample is interporeted as [0, 1[ inside
	 * the element. This means rescalling has allready been applied.
	 * This function is not requiered to check that.
	 *
	 * This function is abstract, but there is default implementation.
	 * There is a very inefficient implementation, it is recomended
	 * to the user to override the function.
	 *
	 * This function operates on a full sample.
	 * This means that the number of total samples in the tree has to
	 * be given as an argument to the function and the factor must be included.
	 * Also the scalling factor that stems from rescalling has to be applied.
	 * The returned PDF can then be interpreted as the pdf on the rescalled
	 * root data space.
	 *
	 * This function is requiered to be thread safe.
	 *
	 * \param  x		This is the sample that we want to evaluate.
	 * \param  domain 	This is the hypercube, \c *this was defined on.
	 * 			 The value is relative to the rescalled root data space.
	 * \param  nTotSamples	The number of total samples in the enteier tree.
	 *
	 * \throw 	The conditions on which this function generates an exception
	 * 		 are not specified.
	 * 		 The implementation is encurrated to perform some simple checking.
	 */
	virtual
	Numeric_t
	evaluateModelForSample(
		const Sample_t&		s,
		const HyperCube_t&	domain,
		const Size_t 		nTotSamples)
	 const
	 = 0;



	/*
	 * =========================
	 * Test Functions
	 *
	 * Thsi function are the one that are used inside the test environemnt.
	 *
	 * It is important that these functions does not operate on the
	 * original data space, nor on the rescalled root data space.
	 * Insted they operate directly on exclusively on the rescalled domain
	 * this is defined on.
	 */
public:
	/**
	 * \brief	This function returns the expected counts inside the bins.
	 *
	 * This function is explicitly designed to be used inside the testing function.
	 *
	 * As argument it takes the bin locations, in the usual formats.
	 * The bin edges values are interpreted in the usual [0, 1[ interval.
	 * This function then returns how many samples should be in any of the bins,
	 * if the underling hypothsis is true.
	 *
	 * This means that no factor what so ever is needed to be incooperated.
	 *
	 * This function is also used by the gof tester, so the user is encuraged to make it fast.
	 *
	 * \param  d		The dimension for which we want to evaluate.
	 * \param  binEdges 	The bin edges, is an array of length n + 1.
	 * \param  domian 	This is the domain on which we operate.
	 * 			 The value is relative to the rescalled root data space.
	 *
	 * \return 	This function returns a value array of length n.
	 *
	 * \throw 	Generally unspecific. However implementations should make some
	 * 		 compability tests. Also memory allocation could throw.
	 *
	 * \note	The name of this function was changed, this is tro reflect its purpose.
	 *
	 * \note	The value returned by this function must not be scalled by any factor.
	 * 		 This means this is not just the CDF value in each bin, but actually
	 * 		 the CDF value scalled by the number of the samples iside teh domain managed
	 * 		 by the node.
	 */
	virtual
	ValueArray_t
	getExpectedCountsInBins(
		const Int_t 		d,
		const BinEdgeArray_t& 	binEdge,
		const IntervalBound_t&	domain)
	 const
	 = 0;


	/**
	 * \brief	This function is used for the quantiles.
	 *
	 * This function compuztes the bins for the tests.
	 * It is bassically the quantile function.
	 *
	 * The bin edges should be such that the theoretical frequencies,
	 * observaion counts in each bin, if *this realy describes the
	 * samples, the same.
	 * This is very complictaed for saying that the quantiles
	 * are equadistanct spreaded.
	 *
	 * The argument \c nBins is interpreted as the number of bins,
	 * that should be generates. The bins should be equally spreded
	 * alsong the CDF value.
	 *
	 * \param  d		The dimension we opewrate on.
	 * \param  nBins	The number of bins that should be created.
	 * \param  domain	The domain *This was fitted on.
	 * 			 The value is relative to the rescalled root data space.
	 *
	 * \throws	Generally unspeciffic, however implementations
	 * 		 should make some compability tests.
	 * 		 If *this is not consistent an exception should be raised.
	 */
	virtual
	BinEdgeArray_t
	generateBins(
		const Int_t 		d,
		const Int_t 		nBins,
		const IntervalBound_t&	domain)
	 const
	 = 0;


	/**
	 * \brief	This function returns the number of bounds DOF of this model.
	 *
	 * Thsi function is important for the statistical tests.
	 * This is the number of parameters that the tests needs.
	 * For the sake of generality, the value does not need to be an integer.
	 */
	virtual
	Numeric_t
	getBoundDOF()
	 const
	 = 0;


	/*
	 * ===========================
	 * Sampling
	 */
public:
	/**
	 * \brief	This function draws a sample.
	 *
	 * This fucntion allows to draw a random sample.
	 * The distribution that is used to sample this is
	 * described by *this.
	 *
	 * This function gets a sample, its component are unifomly
	 * distributed samples, on the interval [0, 1[.
	 * This is the random source that is used for generating the sample.
	 * Note that this sample is also used to return the sample.
	 *
	 * The sample that is generated should be interpreted in the
	 * _rescalled_ node data space (this means the unit cube).
	 *
	 * This function also has to respect the conditions that are
	 * passed to this function, this means for the components
	 * that are restricted the values have to be inserted by this function.
	 * Note that this conditions are expresed in rescalled node data space.
	 * This means they are ready to use.
	 *
	 * The node that is passed describbes the location hand shape
	 * of the owning leaf of *this.
	 * It is important to remember that this domain is relative to
	 * to the root data space.
	 *
	 * \param  s			Pointer to the random source and the output varibal.
	 * \param  reNodeDSCondition	This is teh condition object, in rescalled node data space, that restricts the sampling.
	 * \param  domain 		This is the domain where the owning leaf is defined on.
	 *
	 * \note	The fucntion is not marked const in order to allow complec logic.
	 */
	virtual
	void
	drawSampleOnThis(
		Sample_t* const   		s,
		const MultiCondition_t& 	reNodeDSCondition,
		const HyperCube_t& 		domain)
	 = 0;





	/*
	 * =================================
	 * Update functions
	 *
	 * These fucntions deals with the updating of teh estimator.
	 */
public:

	/**
	 * \brief	This is a function that informs *this that the update process starts.
	 *
	 * Thsi function is provided to enable *this to realocate structures that are needed
	 * for the fitting process. It is also there to "Unset" the fitting state.
	 * If this function is called it is requierd, that the fitting state of all dimensions
	 * is changed form "done" to "undone".
	 *
	 * Please note that the association of *this is not revoked.
	 *
	 *
	 * \throw	If *this is not associated or not fitted at all.
	 * 		 Also if it is not consistent.
	 */
	virtual
	void
	preCommitHookInit()
	 = 0;


	/**
	 * \brief	This function adds the new sample \c nSample to \c *this.
	 *
	 * The result is that \c *this will become inconsistent,
	 * if it is not. The changes are not yet commited,
	 * meaning that \c *this still evaluate for the old data set.
	 *
	 * It is assumed that the new sample \c nSample lies
	 * inside the domain \c *this is responsible for.
	 * This means that the rescalling was alredy applied.
	 * Also all coordinates lies in the range [0, 1[.
	 *
	 * This function is abstract and there is no default implementation.
	 *
	 * This function must not be thread safe.
	 * If multible threads calling this funtion results
	 * in undefineted behaviour.
	 *
	 * \param  nSample	The new sample that should be added to \c *this.
	 * \param  domain 	This is the domain \c *this is reponsible for.
	 * 			 The value is relative to the rescalled root data space.
	 *
	 * \throw 	It is not defined when an exception is raised.
	 * 		 However it is recomended that some sanaty checs are performed.
	 *
	 * \note 	This function must not have an effect if the model can not be made updatable.
	 */
	virtual
	void
	addNewSample(
		const Sample_t&		nSample,
		const HyperCube_t&	domain)
	 = 0;



	/**
	 * \brief	Commit the changes of \c *this in dimension \c d .
	 *
	 * This function will reusslt in the update of the model
	 * in dimension \c d . The added data will now take effect.
	 * This means that \c *this will now evaluate differently.
	 *
	 * This function is abstract and there is no default implementation.
	 *
	 * As long as the dimension parameter \c d of two different
	 * calls to this function are different the fucntion must be
	 * thread safe. This function has to be called once for each dimension.
	 *
	 * If the last dimension was updated, then *this becomes
	 * consistent again.
	 *
	 * The node that is represented by the subTreeRootNode
	 * must be the same as it was during the initial fitting.
	 * It is not guaranteed that the node is a leaf any longer.
	 *
	 * After this fucntion has been called on all dimensions the
	 * endOfFittingProcess() function must be called.
	 *
	 * \param  d 			The dimension we update.
	 * \param  subTreeRootNode	The node where the subtree is rooted.
	 *
	 * \return 	This function returns true if the fitting was successfull.
	 * 		 If false is returned yggdrasil will gnerate an exception.
	 *
	 * \throw 	It is not defined when an exception is raised.
	 * 		 However it is recomended that some sanaty checs are performed.
	 */
	virtual
	bool
	commitChanges(
		const Int_t 	d,
		NodeFacade_t 	subTreeRootNode)
	 = 0;







	/*
	 * =================================
	 * Protected constructors
	 */
protected:
	/**
	 * \brief	Default constructor
	 *
	 * Defaulted
	 */
	yggdrasil_parametricModel_i()
	 noexcept;


	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_parametricModel_i(
		const yggdrasil_parametricModel_i&)
	 noexcept;


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_parametricModel_i(
		yggdrasil_parametricModel_i&&)
	 noexcept;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_parametricModel_i&
	operator= (
		const yggdrasil_parametricModel_i&)
	 noexcept;

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_parametricModel_i&
	operator= (
		yggdrasil_parametricModel_i&&)
	 noexcept;

}; //End class: yggdrasil_parametricModel_i


YGGDRASIL_NS_END(yggdrasil)
