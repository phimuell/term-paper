#pragma once
/**
 * \brief	This file implements a representation of a sample
 *
 * This file implements a sample.
 * It is basically an array of the numeric type.
 *
 * Since we mostly work with one dimension of a collection of
 * samples at one time, this class is not used ofern, or should not be used often.
 * However it can be handy at certain times.
 *
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_intervalBound.hpp>

//Including std
#include <vector>
#include <initializer_list>
#include <string>


YGGDRASIL_NS_START(yggdrasil)

/**
 * \class 	yggdrasil_sample_t
 * \brief	Representation of a smaple
 *
 * This represent a sample, a full samples, not just some dimensions of it, no it is the real thing.
 * It is currently implemented in a very stupid way.
 * Since it is only a wrapper around a vector.
 *
 * As outlined in the specification, we works with dimensions of many samples.
 * Instead with many (full) samples.
 * So using this is a bit inefficient, since it is not the way we designed the code.
 * However fore some operation and for some interface functions,
 * it can be pretty handy to have a class that represnets a full sample.
 *
 * \note	I plane to chnage this at a later point.
 * 		 One could implement this in a more space efficient way, for example.
 * 		 But since we does not work with samples often, the priority is low.
 * 		 However we will always provide this interface.
 *
 * \note	The semantic is inspired by the std::vector .
 *
 * \note	There is no guarantee about the underling memeory layout.
 *
 * \note 	Counting of the dimensions starts at zero.
 */
class yggdrasil_sample_t
{
	/*
	 * =================
	 * Typedef
	 */
public:
	using Vector_t		= ::std::vector<Numeric_t>;	//!< This is the underlying implementation of the collection of numbers
								//!< This typedef is meant for internal work
	using Allocator_t 	= Vector_t::allocator_type;	//!< This is the allocator type, only for internal use
	using value_type 	= Vector_t::value_type;		//!< The underlying type of sample
	using reference 	= Vector_t::reference;		//!< This is a a reference
	using const_reference	= Vector_t::const_reference;	//!< This is a constant reference
	using iterator 		= Vector_t::iterator;		//!< The iterator allows traversing the dimensions
	using const_iterator	= Vector_t::const_iterator;	//!< This is the constant iterator

	using Size_t 		= yggdrasil::Size_t;		//!< This is the size type
	using size_type 	= Size_t;			//!< also size type for compability

	//this is the inteval
	using IntervalBound_t 	= ::yggdrasil::yggdrasil_intervalBound_t;


	/*
	 * ===================
	 * Constructors
	 *
	 * The big 6 are defaulted.
	 *
	 * The proper way to build is from an initalizer list or a vector.
	 * There is also the way to dbnuild it form a pointer an a lenngth.
	 */
public:

	/**
	 * \brief	Building constructor with vector.
	 *
	 * Build the samples with a vector directly.
	 * The data of the vector is moved into this.
	 *
	 * \param  Vector 	The vector to use for building.
	 *
	 * \throw	If the underling implementation throws (memory).
	 * 		 Also if some constarins are violated.
	 */
	yggdrasil_sample_t(
		const Vector_t&		Vector) :
	  m_sample(Vector)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The provided data, resulted in an invalid sample.");
		};
	};


	/**
	 * \brief	Building constructor with a range.
	 *
	 * This constructor constructs the sample with a range.
	 * This can be usefull in certain situation.
	 *
	 * \param  start	This is the beginning of the range.
	 * \param  last 	This is the pointer to the past the end value.
	 *
	 * \tparam  InputIt	The imputiterator type that should be used to construct.
	 */
	template<
		class InputIt
	 >
	yggdrasil_sample_t(
		InputIt start,
		InputIt last)
	 :
	  m_sample(start, last)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Provided data resulted in an invalid range.");
		};
	}; //End: range building



	/**
	 * \brief	Building constructor, with an initializer_list
	 *
	 * \param  List 	The list to use to build
	 */
	yggdrasil_sample_t(
		::std::initializer_list<Numeric_t>	List) :
	  m_sample(List)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The provided data, resulted in an invalid sample.");
		};
	};


	/**
	 * \brief	Building constructor.
	 *
	 * This fucntion will build a sample of dimension \c d .
	 * All components of the vectors will be set to zero.
	 *
	 * \param  d 	The dimension of the sample.
	 *
	 * \note 	That this constructor allows the construction of
	 * 		 a sample with dimension 0, this sample is by
	 * 		 definition invalid.
	 */
	explicit
	yggdrasil_sample_t(
		const Int_t 	d) :
	 m_sample(d, Numeric_t(0.0))
	{};


	/**
	 * \brief	Building constructor.
	 *
	 * This function will build a smaple of dimension \c d .
	 * All comonents of the sample will be set to \c a .
	 *
	 * \param  d 	The dimension of the sample.
	 * \param  a	The value of the comonents.
	 *
	 * \note 	That this constructor allows the construction of
	 * 		 a sample with dimension 0, this sample is by
	 * 		 definition invalid.
	 */
	explicit
	yggdrasil_sample_t(
		const Int_t 		d,
		const Numeric_t 	a) :
	  m_sample(d, a)
	{};


	/**
	 * \brief	Safty constructor.
	 *
	 * This constructor is like the building constructor that takes an Int_t
	 * and a Numeric_t but in swapped order.
	 * This constructor is deleted.
	 */
	explicit
	yggdrasil_sample_t(
		const Numeric_t 	a,
		const Int_t 		d)
	 = delete;


	/**
	 * \brief	Default constructor.
	 *
	 * Defaulted. We allow it for pragmatic reasons.
	 * But we requere it explicit.
	 *
	 * \note	It will construct an invalid sample, since it has zero dimensions.
	 */
	explicit
	yggdrasil_sample_t() YGGDRASIL_NOEXCEPT_IF(Allocator_t()) :
	  m_sample()
	{};


	/**
	 * \brief	The copy constructor.
	 *
	 * Also Defaulted. Again it must be explicit.
	 */
	explicit
	yggdrasil_sample_t(
		const yggdrasil_sample_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Also defaulted, but unlike the copy constructor not explicit.
	 */
	yggdrasil_sample_t(
		yggdrasil_sample_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Also defaulted.
	 */
	yggdrasil_sample_t&
	operator=(
		const yggdrasil_sample_t&) = default;

	/**
	 * \brief	Move assignment.
	 *
	 * Is also defaulted, it is not marked as noexcept,
	 * to have compability with pre C++17.
	 */
	yggdrasil_sample_t&
	operator=(
		yggdrasil_sample_t&&) = default;



	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_sample_t() = default;


	/*
	 * ====================
	 * Utilitiy functions
	 */
public:
	/**
	 * \brief	Returns the numbers of dimensions of \c *this .
	 *
	 * This function returns an int and not a size.
	 * This is for semantic reasons.
	 *
	 * It is unrealistic that we will ever exceed the limit of an int.
	 */
	Int_t
	nDims()
	  const
	  noexcept
	{
		return Int_t(m_sample.size());
	};


	/**
	 * \brief	Retruns the numbers of dimensions of \c *this .
	 *
	 * Is present to attain comability with teh vector.
	 * It returns the length as a size type.
	 */
	Size_t
	size()
	  const
	  noexcept
	{
		return m_sample.size();
	};


	/**
	 * \brief	This function returns \c true if \c *this is valid.
	 *
	 * A valid sample does not contain NAN or INFINITY.
	 * Also if the samples has to many dimensions.
	 * Also a smaple with doimension zero is invalid.
	 *
	 * \note	This means that also subnormal floats are valid.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		if(m_sample.size() >= Constants::YG_MAX_DIMENSIONS)
		{
			return false;
		};

		if(m_sample.size() == 0)
		{
			return false;
		};

		for(const Numeric_t& x : m_sample)
		{
			if(isValidFloat(x) == false)
			{
				return false;
			};
		};

		return true;
	};


	/**
	 * \brief	This function checks if *this is inside the unit hypercube.
	 *
	 * This funciton basically checks if all coordinates of the samples are
	 * inside the interval [0, 1[.
	 * It also makes the test if the value is near the upper bound
	 */
	bool
	isInsideUnit()
	 const
	 noexcept
	{
		//Create the unit interval
		const IntervalBound_t unitInterval = IntervalBound_t::CREAT_UNIT_INTERVAL();

		//Now check
		for(const Numeric_t x : m_sample)
		{
			if(!(unitInterval.isInside(x) || unitInterval.isCloseToUpperBound(x)))
			{
				return false;
			};
		}; //ENd for(x):

		return true;
	};


	/**
	 * \brief	This function writtes *this to a string.
	 *
	 * This function writes the same as the output operator produces
	 */
	std::string
	print()
	 const;





	/*
	 * ====================
	 * Accesser function
	 */
public:
	/**
	 * \brief	Returns a reference to the value of the \c i th dimension of \c *this .
	 *
	 * \param  i	The dimension to querey.
	 *
	 * \throw 	If the dimension is exceeded.
	 */
	reference
	at(
		const Size_t 	i)
	{
		//Test the out of bound access
		if(this->size() <= i)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Accessed element " + std::to_string(i) + " but size is " + std::to_string(this->size()));
		};

		return m_sample.operator[](i);
	}; //End: at


	/**
	 * \brief	Returns a const reference to the value of the \c i th dimension of \c *this .
	 *
	 * \param  i	The dimension to querey.
	 *
	 * \throw 	If the dimension is exceeded.
	 */
	const_reference
	at(
		const Size_t 	i)
	  const
	{
		//Test the out of bound access
		if(this->size() <= i)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Accessed element " + std::to_string(i) + " but size is " + std::to_string(this->size()));
		};

		return m_sample.operator[](i);
	}; //End: at (const)


	/**
	 * \brief	This function returns a reference to the value of the \c i th dimesnion of \c *this .
	 *
	 * This function does not throw, but guards the access with an assert.
	 *
	 * \param  i 	The dimension to query
	 */
	reference
	operator[](
		const Size_t 	i)
	  noexcept
	{
		yggdrasil_assert(i < this->size());

		return m_sample.operator[](i);
	}; //End operator[]


	/**
	 * \brief	Returns a constant reference to the value of the \c i th dimension of \c *this .
	 *
	 * This function does not throw, but guards the access with an assert.
	 *
	 * \param  i	The dimension to querey
	 */
	const_reference
	operator[](
		const Size_t 	i)
	  const
	  noexcept
	{
		yggdrasil_assert(i < this->size());

		return m_sample.operator[](i);
	}; //End: operator[] (const)



	/**
	 * \brief	This function returns a pointer to the underling memeory array.
	 *
	 * If *this is empty then the returned pointer shall not be dereferenced.
	 * It is not guaranteed that the pointer is the nullptr in that case.
	 */
	Numeric_t*
	data()
	 noexcept
	{
		yggdrasil_assert(this->isValid());

		return m_sample.data();
	};


	/**
	 * \brief	This function returns a pointer to the underling memeory array.
	 *
	 * This is the constant version
	 * If *this is empty then the returned pointer shall not be dereferenced.
	 * It is not guaranteed that the pointer is the nullptr in that case.
	 */
	const Numeric_t*
	data()
	 const
	 noexcept
	{
		yggdrasil_assert(this->isValid());

		return m_sample.data();
	};



	/*
	 * ======================
	 * Iterator functions
	 */
public:
	/**
	 * \brief	Returns the beginning of the underling sample.
	 */
	iterator
	begin()
	 noexcept
	{
		return m_sample.begin();
	};


	/**
	 * \brief	Returns the past the end iterator to the sample.
	 */
	iterator
	end()
	 noexcept
	{
		return m_sample.end();
	};


	/**
	 * \brief	Return the constant iterator to the beginning of the components.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		return this->m_sample.cbegin();
	};

	/**
	 * \brief	Explicit constant version to the beginning of the comonents.
	 */
	const_iterator
	cbegin()
	 const
	 noexcept
	{
		return this->m_sample.cbegin();
	};


	/**
	 * \brief	Return the constant iterator to the past the end conmponent.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		return this->m_sample.cend();
	};

	/**
	 * \brief	Explicit constant version to the past the end component.
	 */
	const_iterator
	cend()
	 const
	 noexcept
	{
		return this->m_sample.cend();
	};



	/*
	 * =====================0
	 * Friend functions
	 */
private:
	friend
	std::ostream&
	operator<< (
		std::ostream& 			o,
		const yggdrasil_sample_t& 	s);



	/*
	 * ====================
	 * Private memebers
	 */
private:
	Vector_t 	m_sample;		//!< This vector contains the data
}; //End class: yggdrasil_sample_t


YGGDRASIL_NS_END(yggdrasil)


