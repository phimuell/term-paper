#pragma once
/**
 * \file	core/yggdrasil_dimensionArray.hpp
 * \brief	This stores a single dimension of a sample collection.
 *
 * As we have discussed it in the technical specifications, we do not work with a complete sample.
 * Instead we work with a certain dimesnion of a collection of samples.
 *
 * This file implements now the storage of a dimension of samples.
 * This functionality is tightly cupled with the dimension collection, which stores several dimension
 * and thus forms a complete collection of samples.
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <core/yggdrasil_indexArray.hpp>
#include <core/yggdrasil_valueArray.hpp>

//Including std
#include <vector>


YGGDRASIL_NS_START(yggdrasil)

/**
 * \class	yggdrasil_dimensionArray_t
 * \brief	This class stores a single dimension of several samples.
 *
 * This function stores one dimension of a sample collection.
 * A sample collection stores thus many such dimensional array.
 *
 * A dimensional array is bound to one dimension, and this dimension
 * is explicitly stored.
 *
 * The designe goals is that this class acts as the std::vector in some asspect.
 * At a later point it will be transformed into a paged array, which
 * will improve the efficienty of certain operations.
 *
 */
class yggdrasil_dimensionArray_t
{
	/*
	 * =======================
	 * Typedef
	 */
public:
	using Numeric_t 		= ::yggdrasil::Numeric_t;	//!< The underling numeric type
	using ValueArray_t 		= yggdrasil_valueArray_t<Numeric_t>;	//!< This is the type of the result of an index operation
	using Array_t 			= ValueArray_t;			//!< The underling array, that stores the data
	using SampleIterator_t		= Array_t::iterator;		//!< This is the iterator for iterating throgh the data
	using cSampleIterator_t		= Array_t::const_iterator;	//!< Const version of the iterator
	using Size_t 			= ::yggdrasil::Size_t;		//!< Type for counting
	using Sample_t 			= yggdrasil::yggdrasil_sample_t;//!< This is the sampel type

	//
	//Index array
	using IndexArray_t		= yggdrasil_indexArray_t;	//!< This is the index array for slicing operations
	using Index_t 			= IndexArray_t::value_type;	//!< This is the type of the index

	//
	//Types for establishing comability with the standard semantic
	using value_type 		= Array_t::value_type;		//!< The value type
	using reference 		= Array_t::reference;		//!< A reference
	using const_reference 		= Array_t::const_reference;	//!< Constant regference
	using iterator 			= SampleIterator_t;		//!< Using for iteration
	using const_iterator 		= cSampleIterator_t;		//!< The constant iterator
	using size_type			= Size_t;



	/*
	 * ========================
	 * Constructor
	 *
	 * The big 6 are present.
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This is one of the building constructor.
	 * It allows to build the dimensional array tied to an dimension,
	 * but without any data.
	 *
	 * \param  dim	The number of the dimension that we want to build.
	 *
	 * \throw 	If the dimension is invalid.
	 */
	yggdrasil_dimensionArray_t(
		const Int_t 	dim) :
	  m_dim(dim),
	  m_data()
	{
		//Test if dim is ok
		if(this->hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimension " + std::to_string(dim) + " is not valid.");
		};
	};


	/**
	 * \brief	Construct a dimensional array with data.
	 *
	 * This constructor constructs an array tied to a dimension and associated data.
	 *
	 * \throw	If the dimension is invalid.
	 *
	 * \note 	First the array is tied to a dimension and then the check is performed.
	 * 		 Only after the check hjas been performed and passed, the data is copied into \c *this .
	 *
	 * \note	The data is copied and not moved.
	 *
	 * \param  dim		The number of the dimension we want to build
	 * \param  data		The data that is used to build
	 */
	yggdrasil_dimensionArray_t(
		const Int_t 		dim,
		const ValueArray_t&	data) :
	  m_dim(dim),
	  m_data()
	{
		//Test if dim is ok
		if(this->hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimension " + std::to_string(dim) + " is not valid.");
		};

		//Copy the data
		m_data = data;
	};


	/**
	 * \brief	Default constructor.
	 *
	 * The default constructor constructs an illegal dimension.
	 *
	 * \note 	Please notice that an illegal dimension is realy useless, since
	 * 		 it is not psooible to change the dimension.
	 * 		 This function is provided for internal use and teh user should not use it.
	 *
	 */
	explicit
	yggdrasil_dimensionArray_t() :
	  m_dim(Constants::YG_INVALID_DIMENSION),
	  m_data()
	{};


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	explicit
	yggdrasil_dimensionArray_t(
		const 	yggdrasil_dimensionArray_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_dimensionArray_t(
		yggdrasil_dimensionArray_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 *
	 * \note	No comapability test is performed.
	 */
	yggdrasil_dimensionArray_t&
	operator= (
		const yggdrasil_dimensionArray_t&) = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 *
	 * \note	No comapability test is performed.
	 */
	yggdrasil_dimensionArray_t&
	operator= (
		yggdrasil_dimensionArray_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_dimensionArray_t() = default;


	/*
	 * =========================
	 * Generall functions
	 */
public:
	/**
	 * \brief	Returns the associated dimension of \c *this .
	 */
	Int_t
	getAssociatedDim()
	  const
	  noexcept
	{
		return this->m_dim;
	};


	/**
	 * \brief	Returns \c true if the dimension is illegal.
	 */
	bool
	isDimensionIllegal()
	  const
	  noexcept
	{
		return !(this->hasValidDimensions());
	};


	/**
	 * \brief	Get the number os stored samples inside \c *this .
	 *
	 * Remember that this sample only stores one dimensions of the sample collection.
	 * If the collection is inconsistent this can be missleading.
	 *
	 */
	Size_t
	getNSamples()
	  const
	  noexcept
	{
		return this->m_data.size();
	};

	/**
	 * \brief	This returns the numbers of sampes stiored inside \c *this .
	 *
	 * This function is equivalent to getNSamples() .
	 *
	 * This function is for compability with the other parts of the code.
	 */
	Size_t
	nSamples()
	 const
	 noexcept
	{
		return m_data.size();
	};


	/**
	 * \brief	Also returns the number of samples.
	 *
	 * This is here for compability with the vector semantics.
	 */
	Size_t
	size()
 	  const
 	  noexcept
 	{
 		return this->m_data.size();
	};


	/**
	 * \brief	This function returns \c true if the dimension of \c *this is valid.
	 */
	bool
	hasValidDimensions()
	 const
	 noexcept
	{
		if(m_dim == Constants::YG_INVALID_DIMENSION)
		{
			return false;
		};

		//DImension 0 is valid
		if(m_dim < 0)
		{
			return false;
		};

		if(m_dim >= Constants::YG_MAX_DIMENSIONS)
		{
			return false;
		};

		return true;
	}; //End: hasValidDimensions


	/*
	 * ====================
	 * Accesser function
	 *
	 * All of them guards if the dimension is valid
	 * The at function does it with an exception.
	 * The [] does it with an assert
	 */
public:
	/**
	 * \brief	Returns a reference to the \c i th sample.
	 *
	 * \param  i	The sample to querey.
	 *
	 * \throw 	If the size is exceeded.
	 *
	 * \note 	The test if the dimension is valid is onyl done in an assert.
	 */
	reference
	at(
		const Size_t 	i)
	{
		yggdrasil_assert(this->hasValidDimensions());

		//Test the out of bound access
		if(this->size() <= i)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Accessed element " + std::to_string(i) + " but size is " + std::to_string(this->size()));
		};

		return m_data.operator[](i);
	}; //End: at


	/**
	 * \brief	Returns a const reference to the \c i th sample.
	 *
	 * \param  i	The sample to querey.
	 *
	 * \throw 	If the sample index \c i dies not exists.
	 *
	 * \note	The check if the dimension is valid, is done by an assert.
	 */
	const_reference
	at(
		const Size_t 	i)
	  const
	{
		yggdrasil_assert(this->hasValidDimensions());

		//Test the out of bound access
		if(this->size() <= i)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Accessed element " + std::to_string(i) + " but size is " + std::to_string(this->size()));
		};

		return m_data.operator[](i);
	}; //End: at (const)


	/**
	 * \brief	This function returns a reference to the value of the \c i th samples.
	 *
	 * This function does not throw, but guards the access with an assert.
	 *
	 * \param  i 	The sample to query.
	 */
	reference
	operator[](
		const Size_t 	i)
	  noexcept
	{
		yggdrasil_assert(this->isDimensionIllegal() == false);
		yggdrasil_assert(i < this->size());

		return m_data.operator[](i);
	}; //End operator[]


	/**
	 * \brief	Returns a constant reference to the value of the \c i th sample.
	 *
	 * This function does not throw, but guards the access with an assert.
	 *
	 * \param  i	The sample to querey.
	 */
	const_reference
	operator[](
		const Size_t 	i)
	  const
	  noexcept
	{
		yggdrasil_assert(this->isDimensionIllegal() == false);
		yggdrasil_assert(i < this->size());

		return m_data.operator[](i);
	}; //End: operator[] (const)


	/**
	 * \brief	This function sets all values of *this to v.
	 *
	 * This function bypasses v to the assign function of the underlying container.
	 * There is no check performed on v.
	 * The only requirement is that *this is valid.
	 * All components of *this will afterwards be equal v.
	 *
	 * \param  v 	The value we use for assigning.
	 */
	void
	assign(
		const Numeric_t 	v)
	{
		if(this->hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Tried to use an invalid dimension array for assigning.");
		};

		//Call the internal function for teh assigning
		this->m_data.assign(this->m_data.size(), v);	//Assign needs the size

		return;
	}; //End assign


	/*
	 * ==================================
	 * Iterator access function
	 *
	 * After some consideration, we have decided that only the
	 * begin() class function check if the dimension is valid.
	 * The end()'s do not check it.
	 */
public:
	/**
	 * \brief	Returns an Iterator to the beginning of the underling structure.
	 *
	 * \throw 	This function check if the array is valid.
	 */
	SampleIterator_t
	begin()
	  noexcept(false)
	{
		if(this->isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to access an illegal dimension.");
		};

		return this->m_data.begin();
	};

	/**
	 * \brief	Returns an const iterator to the beginning of the underling structure.
	 *
	 * \throw 	This function check if the array is valid.
	 */
	cSampleIterator_t
	begin()
	  const
	  noexcept(false)
	{
		if(this->isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to access an illegal dimension.");
		};

		return this->m_data.cbegin();
	};

	/**
	 * \brief	Returns an const iterator to the beginning of the underling structure.
	 *
	 * This is the explicit const function.
	 *
	 * \throw 	This function check if the array is valid.
	 */
	cSampleIterator_t
	cbegin()
	  const
	  noexcept(false)
	{
		if(this->isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to access an illegal dimension.");
		};

		return this->m_data.cbegin();
	};


	/**
	 * \brief	Returns an iterator to the past the end element of the underling sample collection.
	 *
	 * This function does not check if the dimension is valid.
	 */
	SampleIterator_t
	end()
 	  noexcept
 	{
 		return this->m_data.end();
	};

	/**
	 * \brief	Returns a constant iterator to the past the end element of the underling sample collection.
	 *
	 * This function does not check if the dimension is valid.
	 */
	cSampleIterator_t
	end()
	  const
 	  noexcept
 	{
 		return this->m_data.cend();
	};


	/**
	 * \brief	Returns a constant iterator to the past the end element of the underling sample collection.
	 *
	 * This is the explicit constant version.
	 * This function does not check if the dimension is valid.
	 */
	cSampleIterator_t
	cend()
 	  const
 	  noexcept
 	{
 		return this->m_data.cend();
	};


	/*
	 * =======================
	 * Manipulation functions
	 *
	 * Tese function are used to manipulate the underling collection.
	 * Where appropriate the functions should check if they operate on valid arrays.
	 */
public:

	/**
	 * \brief	This function adds a new data at the end of \c *this .
	 *
	 * This function is basically a push_back function.
	 *
	 * \param newData	 This is the data that we want to add.
	 *
	 * \throw	If the underling container is illegal, an exeption is generated.
	 */
	void
	addNewData(
		const Numeric_t 	newData)
	{
		if(this->isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to manipulat an illegal array.");
		};

		m_data.push_back(newData);

		return;
	}; //End: addNewData (Numeric_t)


	/**
	 * \brief	This function adds new data to \c *this , but operates on a sample.
	 *
	 * It extracts the correct dimension from the sample by itself.
	 * The data is added at the end.
	 *
	 * \param  newSample	The sample where to load the data from
	 *
	 * \throw 	May throw if iligal of the sample is to short.
	 */
	void
	addNewSample(
		const Sample_t&		newSample)
	{
		if(this->isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimension is illegal.");
		};

		if(newSample.nDims() < this->m_dim)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to access a dimension " + std::to_string(m_dim) + ", but the sample has only " + std::to_string(newSample.nDims()));
		};

		//Now we load
		m_data.push_back(newSample[m_dim]);

		return;
	}; //End: addNewSample


	/*
	 * ========================
	 * Index function
	 *
	 * These function operates on a whole bunch of indexes at the same time
	 */
public:
	/**
	 * \brief	Extracts (copy) all elements that are specified by the \c Ind .
	 *
	 * This copies all elements that are specified in the index array Ind into a
	 * value array and returns it.
	 *
	 * \throw 	If an outof bound access happens.
	 */
	ValueArray_t
	extractIndices(
		const IndexArray_t&	Ind)
	  const
	{
		if(this->isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Operated on an illegal dimension");
		};

		ValueArray_t res;
		res.reserve(Ind.size());

		//Coping
		for(const Index_t& ind : Ind)
		{
			//TODO: is costly think of something different
			res.push_back(m_data.at(ind));
		};

		return res;
	}; //ENd extractIndices


	/**
	 * \brief	This function applies the predicate p to all indexes.
	 *
	 * If the predicate returns true, the \em index is recorded and inserted into an index array,
	 * which is then returned.
	 *
	 * \param  p	The predicate that should be applied.
	 *
	 * \note	It is undefined behaviour of the predicate mutate the value.
	 */
	template<class Predicate_t>
	IndexArray_t
	testAll(
		Predicate_t&& 	pred)
	  const
	{
		//Test if this this is an illegal dimension
		if(this->isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Operated on an illegal dimension.");
		};

		//Create the index array
		IndexArray_t res;

		//The size of *this
		const Size_t thisSize = this->size();

		//Iterate through all the sample and apply the predicate
		for(Size_t it = 0; it != thisSize; ++it)
		{
			//Test predicate
			if(pred(m_data[it]) == true)
			{
				//Copy the index
				res.push_back(it);
			};
		}; //End for(it): iterating through the samples

		//Return the indexes
		return res;
	}; //End: testAll



	/*
	 * ======================
	 * Managing function
	 *
	 * This functions allow a managing of teh underling data.
	 * Most of tem are non binding requests.
	 *
	 * They are more for internal operations.
	 */
public:
	/**
	 * \brief	Returns the capacity of \c *this .
	 *
	 * This is the amount of storage that is allocated to *this.
	 * But not all may be used.
	 * The value is interpreted in base types of \c *this .
	 * In this case Numeric_t.
	 *
	 * This allows an amortized const insertion cost.
	 */
	Size_t
	getCapacity()
	  const
	  noexcept
	{
		return this->m_data.capacity();
	};


	/**
	 * \brief	This function is used to reserve a certain amount of storage.
	 *
	 * This fuction is used for prealocating the storage.
	 * The \em size , the numbers of samples inside \c *this , can not be decreased by the function but only increased.
	 * If a new allocation happens all iterators are invalidated.
	 *
	 * \param  new_cap	The new allocated size, counted in base types (Numeric_t).
	 *
	 * \returns 	If a reallocation happens true is returned, false if no allocation happens.
	 * 		False also means that iterators remains stable.
	 *
	 * \throw 	This function may throw if the allocation process throws.
	 *
	 * \note	No guarantees about expeption guarantees are made.
	 */
	bool
	preallocateStorage(
		const Size_t 	new_cap)
	{
		if(this->isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to extend an illegal dimension.");
		};

		//Make a copy of the old pointer, to compare it later
		Numeric_t* const oldPointer = m_data.data();

		//relaocating
		this->m_data.reserve(new_cap);

		//Test if an allocation occured
		return (m_data.data() == oldPointer) ? false : true;
	}; //End: preallocateStorage


	/**
	 * \brief	This function resizes \c *this .
	 *
	 * This resizing only works if the size of \c *this is zero.
	 * This means \c *this must be empty, but bound to a valid dimension.
	 *
	 * This is typically the case when the array is newly
	 * build and awaits filling.
	 *
	 * This function also allows to reserve some extra space.
	 * This means first a preallocation is performed and
	 * after that the size is set to the specific value.
	 *
	 * The newCapacity is the maximum from the argument
	 * and the newsize.
	 * If newCap is smaller than the newSize,
	 * then no additional storage is allocated.
	 *
	 * \param  newSize	This is the numbers of allocated (builded) samples, that can be accessed.
	 * \param  neCap 	This is the preallocated storage; it is defaulted to zero, which means that
	 * 			 no extra storage is allocated.
	 *
	 * \throw 	If underling function failling or if
	 * 		 *this is not empty or invalid.
	 */
	void
	firstUsageInit(
		const Size_t 	newSize,
		const Size_t 	newCap = 0)
	{
		if(this->hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimensions " + std::to_string(m_dim) + " is not valid.");
		};

		//Test if the size is zero
		if(m_data.size() != 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("*this is not empty.");
		};

		//Now we allocating memory
		m_data.reserve(std::max(newSize, newCap));

		//Only resize if new size is not zero
		if(newSize > 0)
		{
			//New we set the size
			m_data.resize(newSize);
		};

		return;
	};

	/**
	 * \brief	This function resizes \c *this .
	 *
	 * This resizing only works if the size of \c *this is zero.
	 * This means \c *this must be empty, but bound to a valid dimension.
	 *
	 * This is typically the case when the array is newly
	 * build and awaits filling.
	 *
	 * This function is an alias for firstUsageInit().
	 * It was learned that the interface of this class is a bit restrictive.
	 *
	 *
	 * \param  newSize	This is the numbers of allocated (builded) samples, that can be accessed.
	 *
	 * \throw 	If underling function failling or if
	 * 		 *this is not empty or invalid.
	 */
	void
	resize(
		const Size_t 	newSize)
	{
		//Implement the alias functionality
		this->firstUsageInit(newSize);

		return;
	}; //End: resizing


	/**
	 * \brief	This function performs a swap.
	 *
	 * The content from \c other is moved into \c *this and the content of \c *this moved into \c oher.
	 * For that the underling swap fucntion is used.
	 * This means that only pointers are swaped and the coplexity is constant.
	 * All iterators pointing to elements inside \c other now points to the same elements but
	 * now located inside \c *this , the reverse is also true.
	 *
	 * The swap only takes place if the dimensions are the same and both are valid.
	 *
	 * \param  other	The other array
	 *
	 * \throw 	If the dimensions are not the same.
	 * 		 Further if other and *this are not valid
	 */
	void
	swap(
		yggdrasil_dimensionArray_t&	other)
	{
		if(this->hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("*this is not valid.");
		};
		if(other.hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("other is not valid.");
		};

		if(other.getAssociatedDim() != this->getAssociatedDim())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimensions are not the same. *this has dimension " + std::to_string(this->m_dim) + ", other has dimension " + std::to_string(other.m_dim));
		};

		//Now the swap can take place
		this->m_data.swap(other.m_data);

		return;
	}; //End swap


	/**
	 * \brief	This function loads the content from the value array \c valArray into \c *this.
	 *
	 * This function only works if \c *this is bound to a valid dimebnsion and is empty.
	 * The content of the \c valArray is \em moved into \c *this.
	 * Meaning that \c valArray becomes empty.
	 *
	 * This is done by a swap.
	 *
	 * \param  valArray	The value array that is the source of the loading operation.
	 *
	 * \throw	If \c *this is not bound to a valid dimension.
	 * 		 Also if \c *this is not empty.
	 */
	void
	consumeValueArray(
		ValueArray_t&	valArray)
	{
		if(this->hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimensions " + std::to_string(m_dim) + " is not valid.");
		};

		//Test if the size is zero
		if(m_data.size() != 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("*this is not empty.");
		};


		//Now load
		m_data.swap(valArray);

		return;
	}; //End: consumeValueArray


	/**
	 * \brief	This function loads the value array \c valArr into \c *this.
	 *
	 * This function \em copies the data of the \c valArr so it remains unaffected.
	 * As with the consumeValueArray(), \c *this must be emoty and tied to a valid dimension.
	 *
	 * \param  valArray	The value array that is the source of the loading operation.
	 *
	 * \throw	If \c *this is not bound to a valid dimension.
	 * 		 Also if \c *this is not empty.
	 */
	void
	copyAndLoadValueArray(
		const ValueArray_t&	valArray)
	{
		if(this->hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimensions " + std::to_string(m_dim) + " is not valid.");
		};

		//Test if the size is zero
		if(m_data.size() != 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("*this is not empty.");
		};


		//Now load
		m_data.assign(valArray.begin(), valArray.end());

		return;
	}; //End: consumeValueArray


	/**
	 * \brief	This function moves \c dimArray into \c *this.
	 *
	 * This si done by move assignment.
	 *
	 * \param  dimArray	The value array that is the source of the loading operation.
	 *
	 * \throw	If \c *this is not bound to a valid dimension.
	 * 		 Also if \c *this is not empty.
	 */
	void
	moveLoadDimArray(
			yggdrasil_dimensionArray_t&	dimArr)
	{
		if(this->hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimensions " + std::to_string(m_dim) + " is not valid.");
		};

		if(dimArr.getAssociatedDim() != this->getAssociatedDim())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The associated diomensions are not the same, this has " + std::to_string(this->getAssociatedDim()) + ", the source has " + std::to_string(dimArr.getAssociatedDim()));
		};

		//Test if the size is zero
		if(m_data.size() != 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("*this is not empty.");
		};
		const auto sizeOfSource = dimArr.m_data.size();


		//Now load
		this->m_data = ::std::move(dimArr.m_data);
		yggdrasil_assert(this->m_data.size() == sizeOfSource);

		return;
	}; //End:


	/**
	 * \brief	This function loads the dimensional array \c dimArr into \c *this.
	 *
	 * This function \em copies the data of the \c dimArr so it remains unaffected.
	 * As with the consumeValueArray(), \c *this must be emoty and tied to a valid dimension.
	 *
	 * This is like the value array but works on dimensional array.
	 * It is only possible to load the same dimension into *thsi.
	 *
	 * \param  dimArray	The value array that is the source of the loading operation.
	 *
	 * \throw	If \c *this is not bound to a valid dimension.
	 * 		 Also if \c *this is not empty.
	 */
	void
	copyAndLoadDimArray(
			const yggdrasil_dimensionArray_t&	dimArr)
	{
		if(this->hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimensions " + std::to_string(m_dim) + " is not valid.");
		};

		if(dimArr.getAssociatedDim() != this->getAssociatedDim())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The associated diomensions are not the same, this has " + std::to_string(this->getAssociatedDim()) + ", the source has " + std::to_string(dimArr.getAssociatedDim()));
		};

		//Test if the size is zero
		if(m_data.size() != 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("*this is not empty.");
		};


		//Now load
		m_data.assign(dimArr.m_data.begin(), dimArr.m_data.end());

		return;
	}; //End:


	/**
	 * \brief	This function returns a constant reference to the underling array.
	 *
	 * This function is used internaly inside Yggdrasil.
	 * There are no guarantees about anything.
	 *
	 * \throw	If this is not valid.
	 */
	const Array_t&
	internal_getInternalArray()
	 const
	 noexcept
	{
		return m_data;
	};


	Array_t&
	internal_getInternalArray()
	 noexcept
	{
		return m_data;
	};


	/*
	 * =======================
	 * Private variable
	 */
private:
	Int_t 		m_dim;		//!< Which dimension this array represent
	Array_t 	m_data;		//!< The underling data
}; //End class: yggdrasil_dimensionArray_t



YGGDRASIL_NS_END(yggdrasil)

