/**
 * \brief	This file implements a representation of a sample.
 *
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <samples/yggdrasil_sample.hpp>

//Including std
#include <vector>
#include <initializer_list>
#include <string>
#include <sstream>


YGGDRASIL_NS_START(yggdrasil)

std::ostream&
operator<< (
	std::ostream&			o,
	const yggdrasil_sample_t&	s)
{
	o << "(";
	if(s.nDims() == 0)
	{
		o << "EMPTY";
	}
	else
	{
		const Int_t N = s.nDims();
		for(Int_t i = 0; i != N; ++i)
		{
			if(i != 0)
			{
				o << ", ";
			};

			o << s[i];
		}; //End for(i):
	};

	o << ")";

	return o;
}; //End: ouptut


std::string
yggdrasil_sample_t::print()
 const
{
	//Make a string stream
	std::stringstream s;

	//Reuse the output functions
	s << *this;

	//return the string
	return s.str();
}; //ENd print



YGGDRASIL_NS_END(yggdrasil)


