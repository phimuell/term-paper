/**
 * \brief	This file implements the funtionality such that we can operate on samples not just dimensions.
 *
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_matrixOrder.hpp>
#include <samples/yggdrasil_container_util.txx>


//Including std
#include <vector>

YGGDRASIL_NS_START(yggdrasil)


yggdrasil_sampleCollection_t::yggdrasil_sampleCollection_t(
	const SampleArray_t&	sampleArray)
 :
  yggdrasil_sampleCollection_t(sampleArray.nDims() )
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The sample collection is not valid.");
	};
	if((Size_t)this->nDims() != sampleArray.nDims())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("SOmething in the allocation failed.");
	};

	const Size_t D = sampleArray.nDims();
	const Size_t N = sampleArray.nSamples();

	//Allocate space in the dimensions array
	for(Size_t d = 0; d != D; ++d)
	{
		yggdrasil_assert(m_dims.at(d).isDimensionIllegal() == false);
		yggdrasil_assert((Size_t)m_dims.at(d).getAssociatedDim() == d);

		m_dims.at(d).firstUsageInit(N);

		yggdrasil_assert(m_dims.at(d).nSamples() == N);
	}; //ENd for(d): allocating space


	//Now we start to coping the data
	//This is not very inefficient, but it could be worser
	for(Size_t k = 0; k != N; ++k)
	{
		yggdrasil_assert(sampleArray.at(k).isValid() == true);

		//This is the source sample, a pointer to it
		const Numeric_t* const src = sampleArray.beginSample(k);

		//This loop is for coping
		for(Size_t d = 0; d != D; ++d)
		{
			yggdrasil_assert(isValidFloat(src[d]) == true);

			m_dims[d][k] = src[d];
		}; //End for(d): coping the samples into the different locations
	}; //ENd for(k): iterating through the samples

}; //End building constructor with sample array


yggdrasil_sampleCollection_t::yggdrasil_sampleCollection_t(
	const SampleList_t&	sampleList)
 :
  yggdrasil_sampleCollection_t(sampleList.at(0).nDims() )
{
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The sample collection is not valid.");
	};
	if(this->nDims() != sampleList.at(0).nDims())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("SOmething in the allocation failed.");
	};

	const Size_t D = this->nDims();
	const Size_t N = sampleList.size();

	//Allocate space in the dimensions array
	for(Size_t d = 0; d != D; ++d)
	{
		yggdrasil_assert(m_dims.at(d).isDimensionIllegal() == false);
		yggdrasil_assert((Size_t)m_dims.at(d).getAssociatedDim() == d);

		m_dims.at(d).firstUsageInit(N);

		yggdrasil_assert(m_dims.at(d).nSamples() == N);
	}; //ENd for(d): allocating space


	//Now we start to coping the data
	//This is not very inefficient, but it could be worser
	for(Size_t k = 0; k != N; ++k)
	{
		//THis is a reference to the sample
		const Sample_t& kSample = sampleList.at(k);
		yggdrasil_assert(kSample.nDims() == D);
		yggdrasil_assert(kSample.isValid() == true);

		//This loop is for coping
		for(Size_t d = 0; d != D; ++d)
		{
			m_dims[d][k] = kSample[d];
		}; //End for(d): coping the samples into the different locations
	}; //ENd for(k): iterating through the samples

}; //End building constructor with sample array



yggdrasil_sampleCollection_t::yggdrasil_sampleCollection_t(
	const yggdrasil_eigenRef_t<const RowMatrix_t>&		matrix,
	const eMatrixViewRow& 					view)
 :
  yggdrasil_sampleCollection_t(ygInternal_nDimsFromMatrix(matrix, view))
{
	/*
	 * This is the only container that uses a special way of constructing it.
	 * Since it is considered the work horse.
	 *
	 * For this container it is best if we use a hand crafted version of
	 * if Dimension is the vew
	 */
	if(ygInternal_isSampleView(view) == true)
	{
		//view is sample, so just the default
		ygInternal_loadMatrixIntoContainer<yggdrasil_sampleCollection_t, RowMatrix_t>(this, view, matrix);
	}
	else
	{
		/*
		 * The viuew is set to dimension, so we can most efficiently load it$
		 */

		//Load the parameter
		const Size_t D = this->nDims();
		const Size_t N = ygInternal_nSamplesFromMatrix(matrix, view);

		//Make some tests
		if(D != Size_t(matrix.rows()))
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Something is wrong with the dimensionality expected " + std::to_string(D) + " rows, but only get " + std::to_string(matrix.rows()));
		};

		//Going through the dimension
		for(Size_t j = 0; j != D; ++j)
		{
			//Get a creference of the current dimensional array
			DimensionArray_t& jDimArray = m_dims.at(j);

			//Get the row; note this is some crazy Eigen type
			const auto matrixRow = matrix.row(j);

			//Test if the dimension is correctly
			if(N != Size_t(matrix.cols()))
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("Something is wrong with the dimensionality expected " + std::to_string(N) + " cols, but only get " + std::to_string(matrix.cols()));
			};

			//Now we allocate memory in the dimensional array
			jDimArray.firstUsageInit(N);

			if(jDimArray.nSamples() != N)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("Allocation failed in dimension " + std::to_string(j));
			};


			//Now copy the samples
			for(Size_t i = 0; i != N; ++i)
			{
				jDimArray[i] = matrixRow[i];
			}; //ENd for(i): coping
		}; //End for(j): the dimensions

	}; //ENnd else: handling of special care

}; //End: construct from row major matrix


yggdrasil_sampleCollection_t::yggdrasil_sampleCollection_t(
	const yggdrasil_eigenRef_t<const ColMatrix_t>&		matrix,
	const eMatrixViewRow& 					view)
 :
  yggdrasil_sampleCollection_t(ygInternal_nDimsFromMatrix(matrix, view))
{
	/*
	 * For this construcr we can do something very efficient
	 * if the view is set to sample.
	 */
	if(ygInternal_isSampleView(view) == true)
	{
		/*
		 * Handling the spcial case
		 */
		//Load the parameter
		const Size_t D = this->nDims();
		const Size_t N = ygInternal_nSamplesFromMatrix(matrix, view);

		//Make some tests
		if(D != Size_t(matrix.cols()))
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Something is wrong with the dimensionality expected " + std::to_string(D) + " cols, but only get " + std::to_string(matrix.cols()));
		};

		//Going through the dimension
		for(Size_t j = 0; j != D; ++j)
		{
			//Get a creference of the current dimensional array
			DimensionArray_t& jDimArray = m_dims.at(j);

			//Get the colum; note this is some crazy Eigen type
			const auto matrixCol = matrix.col(j);

			//Test if the dimension is correctly
			if(N != Size_t(matrix.rows()))
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("Something is wrong with the dimensionality expected " + std::to_string(N) + " rows, but only get " + std::to_string(matrix.rows()));
			};

			//Now we allocate memory in the dimensional array
			jDimArray.firstUsageInit(N);

			if(jDimArray.nSamples() != N)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("Allocation failed in dimension " + std::to_string(j));
			};


			//Now copy the samples
			for(Size_t i = 0; i != N; ++i)
			{
				jDimArray[i] = matrixCol[i];
			}; //ENd for(i): coping
		}; //End for(j): the dimensions

		//
		//End: HAndling the special case
	}
	else
	{
		//THe default case
		ygInternal_loadMatrixIntoContainer<yggdrasil_sampleCollection_t, ColMatrix_t>(this, view, matrix);
	}; //End else: normal case

}; //End: column major




yggdrasil_sampleCollection_t::RowMatrix_t
yggdrasil_sampleCollection_t::getRowMatrix(
	const eMatrixViewRow 		view)
 const
{
	//Create the matrix
	RowMatrix_t mat;

	//Call the internal help fucntion
	ygInternal_loadContainerIntoMatrix<yggdrasil_sampleCollection_t, RowMatrix_t>(*this, mat, view);

	return mat;
}; //End: ghetROwMajor


yggdrasil_sampleCollection_t::ColMatrix_t
yggdrasil_sampleCollection_t::getColMatrix(
	const eMatrixViewRow 		view)
 const
{
	//Crteate the matrix
	ColMatrix_t mat;

	//Call the internal fucntion
	ygInternal_loadContainerIntoMatrix<yggdrasil_sampleCollection_t, ColMatrix_t>(*this, mat, view);

	return mat;
}; //ENd: getColMatrix




template<class 	DimArr_t>
void
yggdrasil_sampleCollection_t::internal_setDimensionArray(
	const Size_t 		j,
	const DimArr_t&		dArr)
{
	//load parameters that are needed
	const Size_t NSamples = this->nSamples();
	const Size_t NDims    = this->nDims();
	const Size_t NdArr    = dArr.size();

	if(NDims <= j)
	{
		throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(NDims) + ".");
	};

	if(NSamples != NdArr)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimensional array that should be loaded, has " + std::to_string(NdArr) + " many samples, but the collection has " + std::to_string(NSamples) + " many samples.");
	};

	//Load a reference to the dimensional array, we are gonna modify
	DimensionArray_t& dArrJ = m_dims.at(j);

	yggdrasil_assert(NdArr == dArrJ.size());	//Make a small consistency check
	yggdrasil_assert(j == (Size_t)dArrJ.getAssociatedDim());

	Size_t 	       aIdx 	 = 0;			//This is an index for addressing the argument array
	const SampleIterator_t dArrJ_end = dArrJ.end();		//This is the end iterator of the imensional array

	for(SampleIterator_t it = dArrJ.begin(); it != dArrJ_end; ++it)
	{
		yggdrasil_assert(aIdx < dArr.size());	//Small consistency check
		const Numeric_t v = dArr[aIdx];		//Load the value
		*it = v;				//Write the value
		++aIdx;					//Increment the index for the array
	}; //End for(it): coping the data

	//We can now end
	return;
}; //End: internal load fucntion


void
yggdrasil_sampleCollection_t::setDimensionArray(
	const Size_t 						j,
	const yggdrasil_eigenRef_t<const Eigen::VectorXd>& 	dArr)
{
	//Call the internal function
	this->internal_setDimensionArray(j, dArr);

	return;
};


void
yggdrasil_sampleCollection_t::setDimensionArray(
	const Size_t 						j,
	const yggdrasil_eigenRef_t<const PyVector_t>& 	dArr)
{
	//Call the internal function
	this->internal_setDimensionArray(j, dArr);

	return;
};









YGGDRASIL_NS_END(yggdrasil)




