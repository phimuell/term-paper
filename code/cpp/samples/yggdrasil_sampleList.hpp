#pragma once
/**
 * \brief	This file implements a sample list.
 *
 * A sample list is basically a wrapper arround a
 * vectot of type samples. It is provided for convenient.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <core/yggdrasil_eigen.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_matrixOrder.hpp>

//Including std
#include <vector>


YGGDRASIL_NS_START(yggdrasil)


//FWD of the Sample array class
class yggdrasil_arraySample_t;

//FWD of the sample collection type
class yggdrasil_sampleCollection_t;

/**
 * \class 	yggdrasil_sampleList_t
 * \brief	Representation of many samples.
 *
 * This class is basically a vector of samples.
 * It is not very space efficient since it has a lot of
 * memory over head. Howver it is convenient at some points.
 *
 * It is the only osample container that allows range based loops
 * over the samples (probably the array will be able to do that soon^R).
 *
 * It is not space efficient.
 * Previously it was a typedef of std::vector<::yggdrasil::yggdrasil_sample_t>.
 * However it was learned that in order for making the python interface
 * nice it should be a distinct type, so it is provided.
 * It provides some of the functionality of the vbector.
 *
 * It also implements the Universal container interface completly,
 * also its first extension.
 *
 * An important note is that it it possible to insert samples into *this
 * with a different dimension than *this.
 * This can only happen when using the non constant operator[](i), since
 * this hand out a reference ot a sample.
 * Some fucntion tests if the sample has the right dimension some does not.
 * A check of the full container can be done by verifyDims().
 * Note that this is only a problem in C++ and not in python since there
 * are no references.
 *
 */
class yggdrasil_sampleList_t
{
	/*
	 * =================
	 * Typedef
	 */
public:
	using Sample_t 		= ::yggdrasil::yggdrasil_sample_t;	//!< This is the smaple type.
	using Vector_t		= ::std::vector<Sample_t>;		//!< This is the underlying implementation of the collection of numbers
									//!< This typedef is meant for internal work
	using Allocator_t 	= Vector_t::allocator_type;		//!< This is the allocator type, only for internal use
	using value_type 	= Vector_t::value_type;			//!< The underlying type of sample
	using reference 	= Vector_t::reference;			//!< This is a a reference
	using const_reference	= Vector_t::const_reference;		//!< This is a constant reference
	using iterator 		= Vector_t::iterator;			//!< The iterator allows traversing the dimensions
	using const_iterator	= Vector_t::const_iterator;		//!< This is the constant iterator

	using Size_t 		= yggdrasil::Size_t;		//!< This is the size type
	using size_type 	= Size_t;			//!< also size type for compability
	using DimensionArray_t 	= yggdrasil_dimensionArray_t;	//!< This is the type for storing a single dimension

	//These are the types for the Eigen integration
	using ColMatrix_t 	= yggdrasil_eigenColMatrix_t;	//This is a colunm major matrix
	using RowMatrix_t 	= yggdrasil_eigenRowMatrix_t;	//This is a row major matrix
	using PyVector_t 	= yggdrasil_eigenRowVector_t;	//This is the type for a vector, that is used inside pyggdrasil.


	/*
	 * ===================
	 * Constructors
	 *
	 * This class supports the usual constructors.
	 * Also the one that are requiered for the interface.
	 * It also allows to be constructed from
	 * its earlier incranation.
	 * However it is not allowed to do the inverse.
	 */
public:
	/**
	 * \brief	This is the proper building construction.
	 *
	 * This constructor aqllocates the internal array.
	 * Given the dimension nDim of the samples and the
	 * numbers of samples.
	 *
	 * \param  nDims	The dimension of the samples.
	 * \param  nSamples	The number of samples.
	 */
	yggdrasil_sampleList_t(
		const Size_t 	nDims,
		const Size_t 	nSamples)
	 :
	  m_samples(),
	  m_nDims(nDims)
	{
		//We do not allow the dimension to be zero
		if(m_nDims == 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension is zero.");
		};

		//It must be greater than zero
		if(m_nDims <= 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid. 8");
		};

		if(m_nDims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid (to large).");
		};

		//Perform the allocation
		m_samples = Vector_t(nSamples, Sample_t(nDims));
	}; //End: building constructor


	/**
	 * \brief	This function is used to contract *this without the need of
	 * 		 knowing the number of samples in advance.
	 *
	 * This function is basically the same as the building constructor with two
	 * arguments, but do not allocate memeory, since the number of samples
	 * is assumend to be zero.
	 *
	 * \param  nDims	The number of dimensions.
	 */
	yggdrasil_sampleList_t(
		const Size_t 	nDims)
	 :
	  m_samples(),
	  m_nDims(nDims)
	{
		//We do not allow the dimension to be zero
		if(m_nDims == 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension is zero.");
		};

		//It must be greater than zero
		if(m_nDims <= 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid. 8");
		};

		if(m_nDims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid (to large).");
		};
	};


	/**
	 * \brief	This constructor constructs *this from a
	 * 		 row major Eigen matrix.
	 *
	 * How the dimension are interpreted are conttrolled by the
	 * view parameter.
	 * For this constructor setting it to row is the most efficient
	 * choice. This means each row contains one sample.
	 *
	 * \param  matrix	This si the matrix we load from.
	 * \param  view 	How the matrix should be interpreted.
	 */
	explicit
	yggdrasil_sampleList_t(
		const yggdrasil_eigenRef_t<const RowMatrix_t>& 		matrix,
		const eMatrixViewRow&					view);


	/**
	 * \brief	This constructor constructs a sample list from a
	 * 		 column major Eigen matrix.
	 *
	 * The way how this matrix will be interpreted can be controled
	 * with the view parameter.
	 * For this class and matrix, the most efficient choice is
	 * to use the Dimension conffiguration.
	 *
	 * \param  matrix 	This is the matrix that is loaded.
	 * \param  view		How the matrix should be interpreted.
	 */
	explicit
	yggdrasil_sampleList_t(
		const yggdrasil_eigenRef_t<const ColMatrix_t>& 		matrix,
		const eMatrixViewRow& 					view);


	/**
	 * \brief	Default constructor.
	 */
	yggdrasil_sampleList_t()
	 = delete;


	/**
	 * \brief	The copy constructor.
	 *
	 * Also Defaulted. Again it must be explicit.
	 */
	explicit
	yggdrasil_sampleList_t(
		const yggdrasil_sampleList_t&) = default;


	/**
	 * \brief	This function performs a deep copy of *this.
	 *
	 * This fucntion allows to copy the container without the need of knowing
	 * which type is has. This is important/usefull only in Python.
	 */
	yggdrasil_sampleList_t
	clone()
	 const
	{
		return yggdrasil_sampleList_t(*this);
	}; //End: clone


	/**
	 * \brief	This function performs a deep copy of sArray.
	 *
	 * This function loads the sample from the sample array sArray into *this.
	 * sArray is not modified.
	 * This allows the transformation of an sample array into a sample list.
	 *
	 * \param  sArray	The container to load from
	 *
	 * \throw	If an error is detected.
	 */
	explicit
	yggdrasil_sampleList_t(
		const yggdrasil_arraySample_t& 		sArray);


	/**
	 * \brief	This function performs a deep copy of sColl.
	 *
	 * This functions loads the samples from the sample collection
	 * sCol and stores them in *this. sCol is not modified.
	 * This function allows to transform a collection into an array.
	 *
	 * \param  sCol 	The collection ot load
	 *
	 * \throw 	If an error is detected.
	 */
	explicit
	yggdrasil_sampleList_t(
		const yggdrasil_sampleCollection_t& 	sCol);


	/**
	 * \brief	Move constructor.
	 *
	 * Also defaulted, but unlike the copy constructor not explicit.
	 */
	yggdrasil_sampleList_t(
		yggdrasil_sampleList_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Also defaulted.
	 */
	yggdrasil_sampleList_t&
	operator=(
		const yggdrasil_sampleList_t&) = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is also defaulted, it is not marked as noexcept,
	 * to have compability with pre C++17.
	 */
	yggdrasil_sampleList_t&
	operator=(
		yggdrasil_sampleList_t&&) = default;



	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_sampleList_t() = default;


	/*
	 * ==================
	 * Compability constructor
	 *
	 * These constructor makes it possible to load
	 * a list from its internal representation
	 */
public:

	/**
	 * \brief	This constructor moves the content of the list into *this.
	 *
	 * First a test is performed if the number of dimensions is the same for all samples.
	 * sList is empty after wards.
	 *
	 * \param  sList	The list that is loaded.
	 *
	 * \throw	If compability contions are violated. Also if the list is empty.
	 */
	yggdrasil_sampleList_t(
		Vector_t&& 	sList)
	 :
	  m_samples(),
	  m_nDims(0)
	{
		if(sList.size() == 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Can not construct a smaple list from an empty vector of samples.");
		};

		//Load the number of dimensions form the samples
		m_nDims = sList.at(0).nDims();

		//We do not allow the dimension to be zero
		if(m_nDims == 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension is zero.");
		};

		//It must be greater than zero
		if(m_nDims <= 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid. 8");
		};

		if(m_nDims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid (to large).");
		};

		//The number of samples that should be loaded
		const Size_t N = sList.size();

		//Test if the sample dimension is correct for all
		for(Size_t i = 0; i != N; ++i)
		{
			//Test oif the sample is
			if(m_nDims != (Size_t)sList[i].nDims())
			{
				throw YGGDRASIL_EXCEPT_InvArg("Not all sample in the vector of samples had the same dimension.");
			};
		}; //ENd for(i): loading

		//Now move the sList into *this
		m_samples = std::move(sList);
	}; //ENd move constructor


	/**
	 * \brief	This function copies the sList into *this.
	 *
	 * First it is tested if all the samples have the same dimension.
	 * For that it is requiered that at least one sample is in the vector.
	 * Then the copy is performed.
	 *
	 * \param  sList	The source list that should be loaded.
	 *
	 * \throw	If an internal fucntion throws, or a compability condition is violated.
	 * 		 Also if *thsi is empty.
	 */
	yggdrasil_sampleList_t(
		const Vector_t&		sList)
	:
	  m_samples(),
	  m_nDims()
	{
		if(sList.size() == 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Can not construct a smaple list from an empty vector of samples.");
		};

		//Load the number of dimensions form the samples
		m_nDims = sList.at(0).nDims();

		//We do not allow the dimension to be zero
		if(m_nDims == 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension is zero.");
		};

		//It must be greater than zero
		if(m_nDims <= 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid. 8");
		};

		if(m_nDims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid (to large).");
		};

		//The number of samples that should be loaded
		const Size_t N = sList.size();

		//Test if the sample dimension is correct for all
		for(Size_t i = 0; i != N; ++i)
		{
			//Test oif the sample is
			if(m_nDims != (Size_t)sList[i].nDims())
			{
				throw YGGDRASIL_EXCEPT_InvArg("Not all sample in the vector of samples had the same dimension.");
			};
		}; //ENd for(i): loading

		//Now move the sList into *this
		m_samples.assign(sList.cbegin(), sList.cend());
	}; //ENd copy from the sList



	/**
	 * \brief	Thsi is the copy assignement of old sample lists
	 *
	 * This deletes the content of *this and overwrites with teh content of sList.
	 * The content of sList is preserved since a copy is done.
	 * It is requiered that the dimensions of the samples must match.
	 *
	 * \param  sList	The source list.
	 *
	 * \throw	If the dimensions did not match or an internal function throws.
	 *
	 * \note	It is allowed that sList is empyt.
	 */
	yggdrasil_sampleList_t&
	operator= (
		const Vector_t&		sList)
	{
		//We do not allow the dimension to be zero
		if(m_nDims == 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension is zero.");
		};

		//It must be greater than zero
		if(m_nDims <= 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid. 8");
		};

		if(m_nDims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid (to large).");
		};

		//The number of samples that should be loaded
		const Size_t N = sList.size();

		//Test if the sample dimension is correct for all
		for(Size_t i = 0; i != N; ++i)
		{
			//Test oif the sample is
			if(m_nDims != (Size_t)sList[i].nDims())
			{
				throw YGGDRASIL_EXCEPT_InvArg("Not all sample in the vector of samples had the same dimension.");
			};
		}; //ENd for(i): loading

		//Now move the sList into *this
		m_samples = sList;

		//Return this
		return *this;
	}; //ENd: assignemnt from sList


	/**
	 * \brief	Thsi is the move assignement of old sample lists
	 *
	 * This deletes the content of *this and overwrites with teh content of sList.
	 * The content of sList is moved.
	 * It is requiered that the dimensions of the samples must match.
	 *
	 * \param  sList	The source list.
	 *
	 * \throw	If the dimensions did not match or an internal function throws.
	 *
	 * \note	It is allowed that sList is empyt.
	 */
	yggdrasil_sampleList_t&
	operator= (
		Vector_t&&		sList)
	{
		//We do not allow the dimension to be zero
		if(m_nDims == 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension is zero.");
		};

		//It must be greater than zero
		if(m_nDims <= 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid. 8");
		};

		if(m_nDims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid (to large).");
		};

		//The number of samples that should be loaded
		const Size_t N = sList.size();

		//Test if the sample dimension is correct for all
		for(Size_t i = 0; i != N; ++i)
		{
			//Test oif the sample is
			if(m_nDims != (Size_t)sList[i].nDims())
			{
				throw YGGDRASIL_EXCEPT_InvArg("Not all sample in the vector of samples had the same dimension.");
			};
		}; //ENd for(i): loading

		//Now move the sList into *this
		m_samples = std::move(sList);

		//Return this
		return *this;
	}; //ENd: assignemnt from sList





	/*
	 * =======================
	 * Size functions
	 */
public:
	/**
	 * \brief	Returns the number of the dimension a sample has.
	 *
	 */
	Size_t
	nDims()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDims > 0);

		return m_nDims;
	}; //End: get nDims


	/**
	 * \brief	This fucntion returns the number of sampels that are stored inside *this.
	 */
	Size_t
	nSamples()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDims > 0);

		return m_samples.size();
	}; //End: nSamples


	/**
	 * \brief	Thsi function returns the numbers of samples in *this.
	 *
	 * This function is provided for syntactical reasons.
	 */
	Size_t
	size()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDims > 0);

		return m_samples.size();
	}; //End: size


	/**
	 * \brief	This function retuirns true if *this is empty
	 */
	bool
	empty()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDims > 0);

		return m_samples.empty();
	}; //ENd: empty


	/**
	 * \brief	This function checks if the dimnesionality of the
	 * 		 samples inside *this is consistent.
	 *
	 * This requierment arrises because of the unique feature of this container.
	 * It is able to hand out references to samples, thus one can assigne a sample
	 * with a wrong dimension, this function performs an explicit test of that.
	 *
	 * In debug mode this function also tests if the sample is valid.
	 * If an invalid sample is found an assertion is trigered.
	 */
	bool
	verifyDims()
	 const
	{
		if(m_nDims <= 0)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("A sample list with zero dimension was found.");
		};

		//EMpty containers are by default valid
		if(m_samples.empty() == true)
		{
			return true;
		};

		//Going through the samples and test
		for(const Sample_t& s : m_samples)
		{
			//Test if the sample is valid
			//only in debuig mode
			yggdrasil_assert(s.isValid());

			//Test the dimension
			if(Size_t(s.nDims()) != m_nDims)
			{
				//Found a sample witzh a different dimension
				yggdrasil_assert(false);	//IN debug mode i whant that the here the error happens
				return false;
			}; //End if: dims are wrong
		}; //ENd for(s):

		//We found nothing, so we return true
		return true;
	}; //ENd: verify dimensions




	/*
	 * =========================
	 * This functions allows to access the samples
	 * And also the underling memeroy.
	 */
public:
	/**
	 * \brief	This function returns a reference to the n-th samples.
	 *
	 * This function returns a utable reference to the sample.
	 * This is different from the other sample containers.
	 *
	 * \param  n 	The sample to access.
	 *
	 * \note	This function only checks with asserts if
	 * 		 the access is inside the bounds.
	 */
	Sample_t&
	operator[] (
		const Size_t 	n)
	 noexcept
	{
		//Consistency checks
		yggdrasil_assert(m_nDims > 0);

		//Input checks
		yggdrasil_assert(n < m_samples.size());
		yggdrasil_assert((Size_t)m_samples[n].nDims() == m_nDims);

		//Return the smaple
		return m_samples[n];
	}; //End operator[]


	/**
	 * \brief	This function returns a reference to the n-th samples.
	 *
	 * This function returns a const reference to the sample.
	 * This is different from the other sample containers.
	 *
	 * \param  n 	The sample to access.
	 *
	 * \note	This function only checks with asserts if
	 * 		 the access is inside the bounds.
	 */
	const Sample_t&
	operator[] (
		const Size_t 	n)
	 const
	 noexcept
	{
		//Consistency checks
		yggdrasil_assert(m_nDims > 0);

		//Input checks
		yggdrasil_assert(n < m_samples.size());
		yggdrasil_assert((Size_t)m_samples[n].nDims() == m_nDims);

		//Return the smaple
		return m_samples[n];
	}; //End operator[]



	/**
	 * \brief	This function returns a reference to the n-th samples.
	 *
	 * \param  n 	The sample to access.
	 *
	 * \throw	If an out of bound access is detected or the sample does not have the right dimension.
	 */
	Sample_t&
	at(
		const Size_t 	n)
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);

		//Input checks
		if(!(n < m_samples.size()))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(n) + ", but there where only " + std::to_string(m_samples.size()));
		};
		if(!((Size_t)m_samples[n].nDims() == m_nDims))
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Detected a sample with the wrong dimension.");
		};

		//Return the smaple we have already tested the access
		return m_samples[n];
	}; //End operator[]

	/**
	 * \brief	This function returns a constant reference to the n-th samples.
	 *
	 * \param  n 	The sample to access.
	 *
	 * \throw	If an out of bound access is detected or the sample does not have the right dimension.
	 */
	const Sample_t&
	at(
		const Size_t 	n)
	 const
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);

		//Input checks
		if(!(n < m_samples.size()))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(n) + ", but there where only " + std::to_string(m_samples.size()));
		};
		if(!((Size_t)m_samples[n].nDims() == m_nDims))
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Detected a sample with the wrong dimension.");
		};

		//Return the smaple we have already tested the access
		return m_samples[n];
	}; //End operator[]


	/**
	 * \brief	This function returns a copy of the ith sample.
	 *
	 * This fucntion is part of the UCI.
	 * It performs a copy operation on the sample and returns it.
	 *
	 * \param  i	The index of the sample that should be accessed.
	 *
	 * \throw 	If an out of bound access happens
	 */
	Sample_t
	getSample(
		const Size_t 	i)
	 const
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);

		//Input checks
		if(!(i < m_samples.size()))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(i) + ", but there where only " + std::to_string(m_samples.size()));
		};

		Sample_t sample(m_samples[i]);	//Allows nrvo

		if((Size_t)sample.nDims() != m_nDims) //Since we hand out references this can happens.
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("A sample with the wrong dimension was inserted into the container.");
		};

		return sample;
	}; //ENd: getsample


	/**
	 * \brief	This fucntion sets the sample at index i to the given sample s.
	 *
	 * This function is part of the UCI.
	 * It checks if the sample has the right dimension.
	 *
	 * \param  s	The sample to load.
	 * \param  i	The index where we have the sample writting to.
	 *
	 * \throw 	If an underling function throws, also if the dimension is not correct.
	 */
	void
	setSampleTo(
		const Sample_t&		s,
		const Size_t 		i)
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);

		if((Size_t)s.nDims() != m_nDims)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension of the sample is not correct. the list has dimension " + std::to_string(m_nDims) + ", but the sample has dimension " + std::to_string(s.nDims()) + ".");
		};

		//Input checks
		if(!(i < m_samples.size()))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(i) + ", but there where only " + std::to_string(m_samples.size()));
		};

		//Overwrite the sample
		m_samples[i] = s;

		return;
	}; //ENd set sample to


	/**
	 * \brief	This function adds a new sample at the end of *this.
	 *
	 * Note that this function can lead to a memeory realocation.
	 * Thus all pointers will become invalid.
	 *
	 * \param  s	The samples that should be added
	 *
	 * \throw	If the dimension of teh sample is not correct.
	 */
	void
	addNewSample(
		const Sample_t&		s)
	{
		//Check consistency
		yggdrasil_assert(m_nDims != 0);

		if((Size_t)s.nDims() != this->nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The sample has the wrong dimension.");
		};

		//Add the new sample to the container
		m_samples.push_back(s);

		return;
	}; //ENd: add new sample



	/**
	 * \brief	This function adds a a sample at the end of the container.
	 *
	 * Thsi fnction is providded for compability with the std::vector.
	 * Howver it is an alias of addNewSample.
	 *
	 * \param  s	The new sample to add.
	 */
	void
	push_back(
		const Sample_t&		s)
	{
		this->addNewSample(s);

		return;
	}; //End: Push_back


	/*
	 * ===================
	 * Dimension access.
	 *
	 * This functions are requiered by the second extension of the UCI.
	 */
public:
	/**
	 * \brief	This function allows accessing a single dimension of the samples.
	 * 		 Since this functions are in a non compatible format a copy is created.
	 *
	 * Thsi function returns a dimensional array, this means only one dimension of the samples.
	 * The order is preserved.
	 *
	 * \param  d	The dimension that is requested.
	 *
	 * \throw	If an out of bound access is detected or an internal function throws.
	 */
	DimensionArray_t
	getDimensionArray(
		const Size_t 	d)
	 const
	{
		if(!(d < this->m_nDims))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimensional array of dimension " + std::to_string(d) + " does not exists.");
		};

		//This is the array that is used
		DimensionArray_t dimArray(d);	//Initalize it with the dimension, not possible to allocate it doirectly
		yggdrasil_assert(dimArray.isDimensionIllegal() == false);

		//Allocating the space that is needed for the computation
		dimArray.firstUsageInit(this->nSamples());

		//We we call the template function to fill the array
		this->internal_getDimensionArray<DimensionArray_t>(d, &dimArray);

		//Return the array
		return dimArray;
	}; //End: getDimensionalArray


	/**
	 * \brief	This is a templated method that allows the reading the dimension
	 * 		 array in an arbitrary container, that supports some basic functions.
	 *
	 * This function is equivalent to the getDimensionalArray(d) fucntion.
	 * But the cotainer that is used can be selected via template argument.
	 * The argument is not returned but given as argument (pointer to it to
	 * indicate that we modify it.).
	 *
	 * \param  d 		This is the dimension we request.
	 * \param  dimArr	This is the destination we use.
	 *
	 * \param  dimArr_t 	This is the type of the conatiner that is used.
	 * 			 it must support operator[], size() and resize(newSize)
	 *
	 * \note	This function has an internal prefix.
	 * 		 Thsi means the user can not relay on its existence nor that it
	 * 		 will always do the same.
	 */
	template<
		class 	dimArray_t
	>
	void
	internal_getDimensionArray(
		const Size_t 		d,
		dimArray_t* const 	dimArray)
	 const
	{
		if(!(d < this->m_nDims))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimensional array of dimension " + std::to_string(d) + " does not exists.");
		};

		//Get the number of sample
		const Size_t N = this->nSamples();

		//Test if the destination is large enough.
		if(dimArray->size() != N)
		{
			//The destination has the wrong size, we resize it now
			dimArray->resize(N);
		}; //End if: resize samples

		//Now going through the array (*this) and collecting the data
		for(Size_t i = 0; i != N; ++i)
		{
			//Load the sample
			const Sample_t& s = m_samples[i];

			//For this container this can happens
			if(m_nDims != s.size())
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("The sample list contained a sample with the wrong dimension. This is a serious problem.");
			}; //End: test container dimension

			//Load the value
			const Numeric_t value = m_samples[i][d];

			//Check if the access is save
			yggdrasil_assert(i < dimArray->size());

			//write the value to the destination
			(*dimArray)[i] = value;
		}; //End for(i): goiung through


		//Return the function
		return;
	}; //End: internal_getDimensionArray


	/*
	 * ====================
	 * Matrix Access.
	 *
	 * These fucntions transforms *this into a matrix.
	 * The format depends on the return value
	 */
public:
	/**
	 * \brief	This function converts *this into a row major
	 * 		 Eigen matrix.
	 *
	 * This function transfornms *this into an Row-Major Eigen matrix.
	 * The way how the matrix is written can be controlled by the
	 * view parameter. The most efficient choice is to set it to Row.
	 * This means that each row will contain one sample.
	 *
	 * \throw	This function throws if a sample with invalid
	 * 		 dimension is found.
	 */
	RowMatrix_t
	getRowMatrix(
		const eMatrixViewRow 		view)
	 const;


	/**
	 * \brief	Transforms *this into a column major Eigen type
	 * 		 and return the result.
	 *
	 * This function transfornms *this into an Column-Major Eigen matrix.
	 * The way how the matrix is written can be controlled by the
	 * view parameter. The most efficient choice is to set it to Dimension.
	 * This means that each column will contain one sample.
	 *
	 * \throw	This function throws if a sample with invalid
	 * 		 dimension is found.
	 */
	ColMatrix_t
	getColMatrix(
		const eMatrixViewRow 		view)
	 const;


	/*
	 * ===================
	 * Fourth Extension
	 */
public:
	/**
	 * \brief	This function sets the value of the jth component of the ith sample to v.
	 *
	 * This function is basically to manipulate a single sample without the need to load that sample first.
	 * This is important when working on python or with templated funcitons.
	 *
	 * \param  i 	The sample that should me manipulated.
	 * \param  j 	The component that should be manipulated.
	 * \param  v 	The new value that the sample should have in component j.
	 *
	 * \throw 	If an error is detected.
	 */
	void
	setSampleComponent(
		const Size_t 		i,
		const Size_t 		j,
		const Numeric_t 	v)
	{
		const Size_t NSamples = m_samples.size();	//Load the number of samples in the list

		//Make some tests
		if(NSamples <= i) //Negative samples are handled by type
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The sample " + std::to_string(i) + " does not exist. The container only has " + std::to_string(NSamples) + " many samples.");
		};

		if(m_nDims <= j)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(m_nDims) + ".");
		};

		if(isValidFloat(v) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Tried to set an invalid value.");
		};

		//Test if teh sample has the correct doimesion, thsi container technically allows that
		yggdrasil_assert((Size_t)m_samples[i].nDims() == m_nDims);

		//Now set the value
		m_samples[i][j] = v;

		return;
	}; //End: setSample Component

	void
	setSampleComponent(
		const Size_t 		q1,
		const Numeric_t 	q2,
		const Size_t 		q3)
	 = delete;

	void
	setSampleComponent(
		const Numeric_t 	q1,
		const Size_t 		q2,
		const Size_t 		q3)
	 = delete;


	/**
	 * \brief	This function returns the value that sample i has in its jth component.
	 *
	 * \param  i	The index of the sample that should be accessed.
	 * \param  j	The component that should be accessed.
	 *
	 * \throw 	If inconcistencies are detected.
	 */
	Numeric_t
	getSampleComponent(
		const Size_t 		i,
		const Size_t 		j)
	 const
	{
		//Make some tests
		if(m_samples.size() <= i) //Negative samples are handled by type
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The sample " + std::to_string(i) + " does not exist. The container only has " + std::to_string(m_samples.size()) + " many samples.");
		};

		if(m_nDims <= j)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(m_nDims) + ".");
		};

		//Access the sample by reference
		const Sample_t& s = m_samples[i];

		//Test if the sample has the correct dimension (is a list thing)
		yggdrasil_assert((Size_t)s.nDims() == m_nDims);

		//Return the value
		return Numeric_t(s[j]);
	}; //End: getSampleComponent


	/**
	 * \brief	This functions sets the jth componetns of all samples to v.
	 *
	 * This functions is equivalent to calling the setSampleComponent function on any sample
	 * but is much more efficient.
	 *
	 * \param  j 	The component that should be manipulated.
	 * \param  v 	The new value that the samples should have in component j.
	 *
	 * \throw 	If an error is detected.
	 *
	 * \note	For consistency thsi fucntion checks, by means of assert, if all samples have the correct dimension.
	 */
	void
	setDimensionTo(
		const Size_t 		j,
		const Numeric_t 	v)
	{
		if(m_nDims <= j)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(m_nDims) + ".");
		};

		if(isValidFloat(v) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Tried to set an invalid value.");
		};

		//Iterate through the array and apply the change.
		for(Sample_t& s : m_samples)
		{
			//Check if the diemnsion of the sample is correct (technically this is possible to violate)
			yggdrasil_assert((Size_t)s.nDims() == m_nDims);

			//Set the value
			s[j] = v;
		}; //End for(s):

		return;
	}; //End: setDimensionTo

	void
	setDimensionTo(
		const Numeric_t 	q1,
		const Size_t 		q2)
	 = delete;


	/**
	 * \brief	This function sets the jth component of the samples in
	 * 		 *this to the values indicated by the dimensional array dArr.
	 * 		 The dimension of that array is ignored.
	 *
	 * Thsi function allows to set the values of the component j of all sampels.
	 * Contrary to the setDimensionTo() function, which sets all to the same value.
	 * This fucntion sets the values to an individual value.
	 * The values are taken from the dimensional array.
	 * The dimension parrameter of teh array is ignored.
	 * But the array must be valid.
	 *
	 * \param  j 	The dimension that should be manipoulated.
	 * \param  dArr The dimensional array, dimension of it is ignored.
	 *
	 * \throw	If an inconsisitency is detected. The values that are used are only checked
	 * 		 by an assert if they are valid or not.
	 */
	void
	setDimensionArray(
		const Size_t 			j,
		const DimensionArray_t&		dArr);


	/**
	 * \brief	This function sets the jth component of the samples in
	 * 		 *this to the values indicated by the Eigen Vector dArr.
	 *
	 * Thsi function allows to set the values of the component j of all sampels.
	 * Contrary to the setDimensionTo() function, which sets all to the same value.
	 * This fucntion sets the values to an individual value.
	 * The values are taken from the dimensional array.
	 * This function is for python.
	 *
	 * \param  j 	The dimension that should be manipoulated.
	 * \param  dArr The vector where the data is taken from.
	 *
	 * \throw	If an inconsisitency is detected. The values that are used are only checked
	 * 		 by an assert if they are valid or not.
	 *
	 * \note 	This function takes a vector that is compatible with teh requirements
	 * 		 that are imposed by pibind11.
	 */
	void
	setDimensionArray(
		const Size_t 						j,
		const yggdrasil_eigenRef_t<const PyVector_t>& 	dArr);


	/**
	 * \brief	This function sets the jth component of the samples in
	 * 		 *this to the values indicated by the Eigen Vector dArr.
	 *
	 * Thsi function allows to set the values of the component j of all sampels.
	 * Contrary to the setDimensionTo() function, which sets all to the same value.
	 * This fucntion sets the values to an individual value.
	 * The values are taken from the dimensional array.
	 *
	 * This function takes a normal Eigen::VectorXd type as argument.
	 *
	 *
	 * \param  j 	The dimension that should be manipoulated.
	 * \param  dArr The vector where the data is taken from.
	 *
	 * \throw	If an inconsisitency is detected. The values that are used are only checked
	 * 		 by an assert if they are valid or not.
	 */
	void
	setDimensionArray(
		const Size_t 						j,
		const yggdrasil_eigenRef_t<const Eigen::VectorXd>& 	dArr);


	/**
	 * \brief	This function allows to set the values of all samples in a certain dimension to
	 * 		 the values that are specified in the dimensional array.
	 * 		 The dimension to which we apply the modification is extracted from the array.
	 *
	 * \param  dArr		The array that is read.
	 *
	 * \throw	If an inconsistency is detected.
	 */
	void
	setDimensionArray(
		const DimensionArray_t& 	dArr)
	{
		//Test iof teh dimension is valid
		if(dArr.isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The passed dimensional array is not valid.");
		};

		//Call the implementing fucntion, that will also do the other tests
		this->setDimensionArray(dArr.getAssociatedDim(), dArr);

		//Return
		return;
	}; //End: setDimensionArray


	/**
	 * \brief	Thsi is the teh core implementation function of the funciton that sets a complete dimension.
	 *
	 * \param  j	The dimension where we apply the change.
	 * \param  dArr The array that is loaded.
	 *
	 * \tparam  DimArr_t 	The type that we load from.
	 *
	 * \throw 	If inconsistencies are detected.
	 */
private:
	template<class 	DimArr_t>
	void
	internal_setDimensionArray(
		const Size_t 		j,
		const DimArr_t&		dArr);




	/*
	 * Iterators
	 *
	 * Here are some forms of iterator functions
	 */
public:
	/*
	 * \brief	Returns an iterator to the first sample
	 */
	iterator
	begin()
	 noexcept
	{
		yggdrasil_assert(m_nDims != 0);
		return m_samples.begin();
	};

	/**
	 * \brief	Return a constant iterator to the first sample of *this.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDims != 0);
		return m_samples.cbegin();
	};


	/**
	 * \brief	Return a constant iterator to the first sample.
	 *
	 * Explicit version
	 */
	const_iterator
	cbegin()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDims != 0);
		return m_samples.cbegin();
	};


	/**
	 * \brief	Returns the iterator that points past the end of the sample array.
	 */
	iterator
	end()
	 noexcept
	{
		yggdrasil_assert(m_nDims != 0);
		return m_samples.end();
	};


	/**
	 * \brief	Returns a consatnt iterator to the pass the end sample array.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDims != 0);
		return m_samples.cend();
	};


	/**
	 * \brief	Return a constant iterator to the pass the end of the sample array.
	 *
	 * Explicit version
	 */
	const_iterator
	cend()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDims != 0);
		return m_samples.cend();
	};



	/*
	 * ================
	 * Storage modifier
	 *
	 * This functions are able to change the internal storage.
	 */
public:
	/**
	 * \brief	This function allows to set the size of the array.
	 *
	 * This basically calls resize on the underling vector.
	 * It is only possible to change the size of *this is the
	 * number of samples is zero.
	 *
	 * Note that it is not possible to change the number of dimensions.
	 *
	 * \param  newNSamples		The new number of samples.
	 *
	 * \throw	If nSamples is not zero.
	 */
	void
	resize(
		const Size_t 	newNSamples)
	{
		//Test if we are allowed to do this
		if(m_samples.size() != 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The sampels count was allready set and thus can not be changed.");
		};
		yggdrasil_assert(m_samples.empty() == true);
		yggdrasil_assert(m_nDims != 0);
		const Size_t D = m_nDims;

		m_samples.resize(newNSamples);
		yggdrasil_assert(m_samples.size() == newNSamples);
		yggdrasil_assert(D == m_nDims);

		return;
	}; //End: resize


	/**
	 * \brief	This function preallocates memeory of the underling array.
	 *
	 * This fucntion will speed up insertiuons unsing push pack's.
	 * It only works if the container is empty.
	 *
	 * \param  N	The numbers of sampels that should be reserved.
	 *
	 * \note 	This container previously throwed an exception
	 * 	 	 if the size was already set. this was changed by
	 * 	 	 simply doing nothing.
	 */
	void
	reserve(
		const Size_t 	n)
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);

		//Test if we are allowed to do this
		if(m_samples.size() != 0)
		{
			//This is new, in that case we do not throw
			return;
		};
		yggdrasil_assert(m_samples.empty() == true);

		//Tets if the reservation was already needed
		if(n <= 0)
		{
			//The reservation is not needed
			return;
		};

		/*
		 * Perfrom the actual reservation
		 */
		m_samples.reserve(n);

		return;
	}; //End: resize


	/**
	 * \brief	This is the clear function that deallocates the memeory.
	 *
	 * This function sets the number of samples to zero.
	 * The memeory is cleared by the swap trick.
	 *
	 * The number of dimension is not changed.
	 */
	void
	clear()
	{
		yggdrasil_assert(m_nDims != 0);

		//Call the internal clear fucntion
		m_samples.clear();

		//Clear is an non binding request.
		//This will enforce the request.
		Vector_t().swap(m_samples);

		return;
	}; //End: clear


	/*
	 * ====================
	 * Private memebers
	 */
private:
	Vector_t 	m_samples;		//!< This vector contains the data
	Size_t 		m_nDims;		//!< This is the dimension of samples that are allocated.
}; //End class: yggdrasil_sampleList_t


YGGDRASIL_NS_END(yggdrasil)


