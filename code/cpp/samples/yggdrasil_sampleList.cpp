/**
 * \brief	This file implements a sample list.
 *
 * A sample list is basically a wrapper arround a
 * vectot of type samples. It is provided for convenient.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <core/yggdrasil_eigen.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_matrixOrder.hpp>
#include <samples/yggdrasil_container_util.txx> 	//This is the helper code

//Including std
#include <vector>


YGGDRASIL_NS_START(yggdrasil)

yggdrasil_sampleList_t::yggdrasil_sampleList_t(
	const yggdrasil_eigenRef_t<const RowMatrix_t>& 		matrix,
	const eMatrixViewRow& 					view)
 :
  yggdrasil_sampleList_t(ygInternal_nDimsFromMatrix(matrix, view))
{
	//Call the helper code
	ygInternal_loadMatrixIntoContainer<yggdrasil_sampleList_t, RowMatrix_t>(this, view, matrix);
}; //End: construction from a row matrix


yggdrasil_sampleList_t::yggdrasil_sampleList_t(
	const yggdrasil_eigenRef_t<const ColMatrix_t>& 		matrix,
	const eMatrixViewRow& 					view)
 :
  yggdrasil_sampleList_t(ygInternal_nDimsFromMatrix(matrix, view))
{
	//Call the helper code
	ygInternal_loadMatrixIntoContainer<yggdrasil_sampleList_t, ColMatrix_t>(this, view, matrix);
}; //End: construct from colum major



yggdrasil_sampleList_t::RowMatrix_t
yggdrasil_sampleList_t::getRowMatrix(
	const eMatrixViewRow 		view)
 const
{
	//Create the matrix
	RowMatrix_t mat;

	//Fill the matrix
	//The true will instruct the function to enforce the correct dimension
	//by an exception, is only needed in the list.
	ygInternal_loadContainerIntoMatrix<yggdrasil_sampleList_t, RowMatrix_t, true>(*this, mat, view);

	return mat;
}; //End getRowMatrix


yggdrasil_sampleList_t::ColMatrix_t
yggdrasil_sampleList_t::getColMatrix(
	const eMatrixViewRow 		view)
 const
{
	//Create the matrix
	ColMatrix_t mat;

	//Fill the matrix
	//The true will instruct the function to enforce the correct dimension
	//by an exception, is only needed in the list.
	ygInternal_loadContainerIntoMatrix<yggdrasil_sampleList_t, ColMatrix_t, true>(*this, mat, view);

	return mat;
}; //End: getColMatrix


yggdrasil_sampleList_t::yggdrasil_sampleList_t(
	const yggdrasil_arraySample_t& 		sArray)
 :
  yggdrasil_sampleList_t(sArray.nDims())	//Construct an empty container
{
	//Load the parameter
	const Size_t D = sArray.nDims();
	const Size_t N = sArray.nSamples();

	//Preallocate space for the insertion
	this->reserve(N);
	yggdrasil_assert(this->nSamples() == 0);

	/*
	 * Iterate through the array and load the sample.
	 * We will then move them into the underling container.
	 */
	for(Size_t i = 0; i != N; ++i)
	{
		//load the sample
		Sample_t s_i = sArray.getSample(i);

		//Move the sample into the container
		this->m_samples.push_back(std::move(s_i));

		yggdrasil_assert(this->nSamples() == i);
	}; //End for(i):

	//Nw do not need D
	(void)D;
}; //ENd: load from array


yggdrasil_sampleList_t::yggdrasil_sampleList_t(
	const yggdrasil_sampleCollection_t& 	sCol)
 :
  yggdrasil_sampleList_t(sCol.nDims())	//Construct an empy container
{
	//Load the parameter
	const Size_t D = sCol.nDims();
	const Size_t N = sCol.nSamples();

	//Preallocate space for the insertion
	this->reserve(N);
	yggdrasil_assert(this->nSamples() == 0);

	/*
	 * Iterate through the array and load the sample.
	 * We will then move them into the underling container.
	 */
	for(Size_t i = 0; i != N; ++i)
	{
		//load the sample
		Sample_t s_i = sCol.getSample(i);

		//Move the sample into the container
		this->m_samples.push_back(std::move(s_i));

		yggdrasil_assert(this->nSamples() == i);
	}; //End for(i):

	//Nw do not need D
	(void)D;
}; //ENd: load from collection



template<class DimArr_t>
void
yggdrasil_sampleList_t::internal_setDimensionArray(
		const Size_t 		j,
		const DimArr_t& 	dArr)
{
	//Make some tests
	if(m_nDims <= j)
	{
		throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(m_nDims) + ".");
	};

	const Size_t arrNSamples = dArr.size();		//Must use size() for being compatible
	const Size_t NSamples    = m_samples.size(); 	//Number of samples inside *this

	if(arrNSamples != NSamples)
	{
		throw YGGDRASIL_EXCEPT_OutOfBound("The dimensional array has " + std::to_string(arrNSamples) + " many samples, but the container only has " + std::to_string(NSamples));
	};

	//
	//Setting the values

	//Iterate over all the samples to change them
	for(Size_t i = 0; i != NSamples; ++i)
	{
		//Load the sample we want to manipulate
		Sample_t& s = m_samples[i];

		//Test if teh number of dimensions is correct (this is a list thing)
		yggdrasil_assert((Size_t)s.nDims() == m_nDims);

		//load the value we write to that sample
		const Numeric_t v = dArr[i];

		//Test if the value is valid
		yggdrasil_assert(isValidFloat(v));

		//Write the value to the corresponding location
		s[j] = v;
	}; //End for(i)

	return;
}; //End: setDimensionArray (template)



void
yggdrasil_sampleList_t::setDimensionArray(
	const Size_t 						j,
	const yggdrasil_eigenRef_t<const ::Eigen::VectorXd>& 	dArr)
{
	//Call the internal function
	internal_setDimensionArray(j, dArr);

	//Return
	return;
}; //End: setDimensionArray (Eigen)


void
yggdrasil_sampleList_t::setDimensionArray(
	const Size_t 						j,
	const yggdrasil_eigenRef_t<const PyVector_t>& 	dArr)
{
	//Call the internal function
	internal_setDimensionArray(j, dArr);

	//Return
	return;
}; //End: setDimensionArray (Eigen)

void
yggdrasil_sampleList_t::setDimensionArray(
	const Size_t 			j,
	const DimensionArray_t&		dArr)
{
	//TEst if teh dimensional array is valid
	if(dArr.isDimensionIllegal() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The passed dimensional array is not valid.");
	};

	//Call the implementing fucntion, that will also do the other tests
	this->internal_setDimensionArray(j, dArr);

	//Return
	return;
}; //End: setDimensionArray(org)








YGGDRASIL_NS_END(yggdrasil)


