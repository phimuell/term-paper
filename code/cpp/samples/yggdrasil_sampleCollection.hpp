#pragma once
/**
 * \brief	This file implements the funtionality such that we can operate on samples not just dimensions.
 *
 * This functionality is bassically needed to to tie the stored dimension together, such that
 * we have the impression of a full comllection of samples.
 *
 * For the terminology.
 * A collection is the set of samples that are located in a certain subset of space.
 * Usually this is one density element, but depending on the situation it could also be bigger.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <core/yggdrasil_eigen.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_matrixOrder.hpp>


//Including std
#include <vector>

YGGDRASIL_NS_START(yggdrasil)

/**
 * \class 	yggdrasil_sampleCollection_t
 * \brief	This class ties together some dimension to a full collection of samples.
 *
 * One have to keep in mind that we are dimension oriented,
 * so extracting single samples is not particullar efficient.
 *
 * However working on a dimension is very efficient.
 *
 * The intention of that class is be used inside a leaf of the tree.
 * However it will be present in the payloads of inner nodes (split).
 *
 * It allows fast access to the dimensions of a collection
 *
 * This class may look good, but it has meny frieds.
 *
 * The user will probably not realy work with it.
 * Instead a user will probably work with the dimension array at most.
 *
 * This class implements the universal container interface concept.
 * But it does not implements its extension.
 * There is also a great extension concerning the size() fucntion.
 * The C++ code returns the number of diemsnison, this is consistent
 * with the usage of the operator[] fucntions.
 *
 * But the python bindings returns the number of samples.
 * This is for the concistency of the operator[].
 *
 */
class yggdrasil_sampleCollection_t
{
	/*
	 * =======================
	 * Typedef
	 */
public:
	using Numeric_t 		= ::yggdrasil::Numeric_t;	//!< This is the Numeric type
	using DimensionArray_t 		= yggdrasil_dimensionArray_t;	//!< This is the type for storing a single dimension
	using Sample_t 			= DimensionArray_t::Sample_t;	//!< This is the type of a single sample
	using Size_t 			= DimensionArray_t::Size_t;	//!< The size type
	using HyperCube_t 		= ::yggdrasil::yggdrasil_hyperCube_t;	//!< This is the class for a hyper cube and the bound type

	using SampleArray_t 		= ::yggdrasil::yggdrasil_arraySample_t;	//!< This is a class for the storage of many samples.
	using SampleList_t 		= ::yggdrasil::yggdrasil_sampleList_t;		//!< This is another storage format, but inefficient.

	using IndexArray_t 		= DimensionArray_t::IndexArray_t; //!< This is the index array type
	using ValueArray_t 		= DimensionArray_t::ValueArray_t; //!< This is the vlaue array type
	using DimArrayCollection_t 	= ::std::vector<DimensionArray_t>;//!< This is the collection for the seperate dimensions

	//These are the types for the Eigen integration
	using ColMatrix_t 	= yggdrasil_eigenColMatrix_t;	//This is a colunm major matrix
	using RowMatrix_t 	= yggdrasil_eigenRowMatrix_t;	//This is a row major matrix
									  //!<  This is more for internal use
	using PyVector_t 	= yggdrasil_eigenRowVector_t;	//This is the type for a vector, that is used inside pyggdrasil.

	/**
	 * \typedef 	SampleIterator_t
	 * \brief	This iterator allows the iteration of all samples in a partuicular dimension.
	 *
	 * If one imagines the samples stored in a matrix such that each colum is a sample.
	 * Then this is an iterator to traverse all columns of a specific row.
	 */
	using SampleIterator_t		= DimensionArray_t::SampleIterator_t;
	using cSampleIterator_t 	= DimensionArray_t::cSampleIterator_t; 	//!< Constant version of dimension iterator

	/**
	 * \typedef 	DimIterators_t
	 * \brief	This allows to iterate over the single dimensions.
	 *
	 * They gives access to the individual DimensionArray_t.
	 */
	using DimIterator_t 		= DimArrayCollection_t::iterator;
	using cDimIterator_t 		= DimArrayCollection_t::const_iterator;		//!< This is the constant version



	/*
	 * ======================
	 * Constructor
	 *
	 * All of the big 6 are defaulted, except the default constructor which is not allowed.
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor takes the numbers of dimensions that should be created.
	 * The dimensional arrays are empty, but tight to a dimension.
	 *
	 * \param  dims 		The number of dimensions to create.
	 */
	explicit
	yggdrasil_sampleCollection_t(
		const Int_t 	dims) :
	  m_dims()
	{
		if(dims == Constants::YG_INVALID_DIMENSION)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(dims) + " is invalid. 7");
		};

		//It must be greater than zero
		if(dims <= 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(dims) + " is invalid. 8");
		};

		if(dims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(dims) + " is invalid (to large).");
		};

		//Create space for the dimensions
		m_dims.reserve(dims);
		for(int d = 0; d != dims; ++d)
		{
			m_dims.emplace_back(d);	//Construct a dim array inplace

			if(m_dims.back().isDimensionIllegal() == true)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("The construction of dimension " + std::to_string(d) + " resulted in an illegal dimension");
			};
		}; //End for(d): pushing back the dimensions
	}; //End: building constructor


	/**
	 * \brief	THis is a building constructor that creates a non empty container.
	 *
	 * Thsi constructor not only creates an empty collection of dimension dim,
	 * but also allocate space for the sample.
	 *
	 * \param  dim		This is the dimension of the samples.
	 * \param  nSamples	This is the number of samples for which we should allocate space.
	 *
	 * \throw 	If an error happens
	 */
	explicit
	yggdrasil_sampleCollection_t(
		const Int_t 	dims,
		const Size_t 	nSamples)
  	 :
  	  yggdrasil_sampleCollection_t(dims)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The empty cllection that was constructed is not valid.");
		};

		/*
		 * Allocate space for the samples
		 */
		yggdrasil_assert(m_dims.size() == (Size_t)dims);
		for(int d = 0; d != dims; ++d)
		{
			m_dims.at(d).firstUsageInit(nSamples);

			yggdrasil_assert(m_dims.at(d).hasValidDimensions());
			yggdrasil_assert(m_dims.size() == (Size_t)dims);
			yggdrasil_assert(m_dims.at(d).nSamples() == nSamples);
		}; //End for(d): pushing back the dimensions
		yggdrasil_assert(this->isValid());
	}; //End: building constructor



	/**
	 * \brief	This is a building constructor that operates on a sample array.
	 *
	 * A sample array is a space efficient method to store a lot of samples.
	 * However they in a smaple rather than a dimensional centred representation.
	 * This constructor is provided to genrate a collection out of such an array.
	 * The reason is that they are generated by the sample fucntion.
	 *
	 * \param  sampleArray 		The array that should be loaded
	 *
	 * \throw	If internal fucntion throws.
	 */
	explicit
	yggdrasil_sampleCollection_t(
		const SampleArray_t&	sampleArray);


	/**
	 * \brief	This is the building constructor that
	 * 		 takes a sample list for the construction.
	 *
	 * A sample list is an array of Sample_t object.
	 * As the user meight guess, this is not so efficient.
	 * This constructor is provided for convenience.
	 * Since some situations, like testing, it is good
	 * to have something like that.
	 *
	 * \param  sampleList		The sample list that should be loaded.
	 *
	 * \throw	If an internal fucntion throw.
	 * 		 Also the sample list must have a size greater than zero.
	 */
	explicit
	yggdrasil_sampleCollection_t(
		const SampleList_t&	sampleList);


	/**
	 * \brief	This function constructs a sample collection out of a
	 * 		 row major Eigen matrix.
	 *
	 * How the matrix is interpreted is determined by the view argument.
	 * If view is Sample, then eqch row contains one sample, if set to
	 * Dimension then each column will contain one sample.
	 *
	 * This function is most efficient if view is Dimension.
	 *
	 * \param  matrix 	This is the matrix that is loaded.
	 * \param  view 	How the matrix should be interpreted.
	 *
	 * Note that this is different from the other container, where the most
	 * efficient choice was view set to Sample.
	 */
	explicit
	yggdrasil_sampleCollection_t(
		const yggdrasil_eigenRef_t<const RowMatrix_t>&		matrix,
		const eMatrixViewRow& 					view);



	/**
	 * \brief	This constructor construct a sample from the given Matrix.
	 *
	 * How the matrix is interpreted depends on the view parameter.
	 * If view is set to Sample then each row will contain one sample.
	 * If set to Dimension instead, each column will conatin one sample.
	 *
	 * This fucntion is most efficient fio the view parameter is set to Sample.
	 *
	 * \param  matrix 	This is the matrix that is loaded.
	 * \param  view 	How the matrix should be interpreted.
	 *
	 * Note that this is different from the other container, where the most
	 * efficient choice was view set to Dimension.
	 */
	explicit
	yggdrasil_sampleCollection_t(
		const yggdrasil_eigenRef_t<const ColMatrix_t>&		matrix,
		const eMatrixViewRow& 					view);



	/**
	 * \brief	The default constructor.
	 *
	 * It is deleted.
	 * It is public make it explicit.
	 */
	yggdrasil_sampleCollection_t() = delete;


	/**
	 * \brief	The copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_sampleCollection_t(
		const yggdrasil_sampleCollection_t&) = default;


	/**
	 * \brief	This fucntion performs a copy operation of *this.
	 *
	 * This is usefull for copy the container without knowing its type.
	 * This function is primary usefull in Python.
	 */
	yggdrasil_sampleCollection_t
	clone()
	 const
	{
		return yggdrasil_sampleCollection_t(*this);
	}; //End: clone


	/**
	 * \brief	The move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_sampleCollection_t(
		yggdrasil_sampleCollection_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_sampleCollection_t&
	operator=(
		const yggdrasil_sampleCollection_t&) = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_sampleCollection_t&
	operator= (
		yggdrasil_sampleCollection_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_sampleCollection_t() = default;



	/*
	 * ======================
	 * Generall functions
	 */
public:

	/**
	 * \brief	Returns the numbers of dimensions
	 *
	 * This function does returns an integer and not size_t.
	 */
	Int_t
	nDims()
	  const
	  noexcept
	{
		return Int_t(m_dims.size());
	};

	/**
	 * \brief	Returns the number of dimensions the samples had.
	 *
	 * The reason why the size function returns the number of dimensions
	 * and not the number of samples is because the operator[] gives access
	 * to the dimensional arrays.
	 *
	 * For syntactical reasons this function returns a size_t.
	 */
	Size_t
	size()
	  const
	  noexcept
	{
		return m_dims.size();
	};


	/**
	 * \brief	Returns the numbers of samples that are stored in \c *this .
	 *
	 * Notice that this simply retrurns the size of the first dimension.
	 * During the building process this operation my not be accurate.
	 * So there is an optional parameter which allows to select the dimension to query.
	 *
	 * \param  dim 		The dimension to get the size information from; defaulted to 0.
	 *
	 * \throw 	If the dimension does not exits.
	 *
	 *
	 * \note 	The building process transforms a collection into an inconsistent state.
	 * 		 At least for a short amount of time. This is a byproduct of the
	 * 		 way how the storage requirement are keept low.
	 * 		 However the user will never deal with such a situation.
	 */
	Size_t
	nSamples(
		const Int_t 	dim = 0)
	  const
	{
		if((dim < 0) || (dim >= nDims()))
		{
			throw YGGDRASIL_EXCEPT_InvArg("Called the size function with an invalid argument of " + std::to_string(dim));
		};

		return m_dims[dim].size();
	};


	/**
	 * \brief	This function returns true if \c *this is in a valid state.
	 *
	 * A valid state is that we have at least one dimension.
	 * All dimensional array are valid, and they are tied to consecutivly
	 * numbered dimensions.
	 *
	 * Also all dimensin must host the same numbers of samples.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		//All dimensions must be valid and have the same number of samples

		//We requiere that we have a dimension
		if(m_dims.empty() == true)
		{
			return false;
		};

		//This must be done to ensure that we can safly query the first dimension for the ref size
		if(m_dims[0].hasValidDimensions() == false)
		{
			return false;
		};

		//this is the reference sample
		const Size_t refSize = m_dims[0].getNSamples();
		Int_t oldDimension = -1;	//This is ugly
		for(const DimensionArray_t& d : m_dims)
		{
			if(d.hasValidDimensions() == false)
			{
				return false;
			};

			if((oldDimension + 1) != d.getAssociatedDim())
			{
				return false;
			};
			oldDimension += 1;

			if(d.getNSamples() != refSize)
			{
				return false;
			};
		}; //End for(d): iterating through the dimenions

		return true;
	}; //End: isValid


	/**
	 * \brief	This function tests if all samples that are stored inside \c *this are inside the hypercube \c cube.
	 *
	 * This function iterates through the dimensions and uses the is inside function of the \c cube .
	 * It also considereas near upper bound cases.
	 *
	 * \param  cube 	The cube that is the bound.
	 * \param  n		The safty factor for the closness test of the uper bound; defaulted to 100.
	 *
	 * \throws	If the dimensions does not agree or bound or \c *this is invalid.
	 */
	bool
	isInsideCube(
		const HyperCube_t&	cube,
		const Numeric_t 	n = 100.0)
	 const
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("*this is not valid.");
		};

		if(cube.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The cube is not valid.");
		};

		if(m_dims.size() != (Size_t)cube.nDims())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimensions does not agree, *this has dimneiosn " + std::to_string(m_dims.size()) + ", but cube has dimension " + std::to_string(cube.nDims()) + ".");
		};

		/*
		 * Now we can make the tests
		 */
		for(Size_t d = 0; d != m_dims.size(); ++d)
		{
			const DimensionArray_t&	dimArray     = m_dims[d];
			const Size_t 		thisDimSize  = dimArray.size();
			const auto& 		thisDimBound = cube[d];

			for(Size_t i = 0; i != thisDimSize; ++i)
			{
				const Numeric_t thisValue = dimArray[i];
				if(!(thisDimBound.isInside(thisValue) || thisDimBound.isCloseToUpperBound(thisValue, n)))
				{
					return false;
				}; //End if: outside the bound
			}; //End for(i): iterating through the samples of this dimension
		}; //End for(d): iterating through the dimensions

		//We can now return true
		return true;
	}; //End: isINsideCube


	/*
	 * =======================
	 * Sample based functions
	 *
	 * These functions operates on samples.
	 * These fucntions are considered not efficient.
	 */
public:
	/**
	 * \brief	Adds a new sample to \c *this .
	 *
	 * This function adds the \c newSample to to the collection of \c *this .
	 * It is added at the end.
	 *
	 * \param  newSample 		This is the new sample that one should add.
	 *
	 * \throw 	If some constraints are violated.
	 *
	 * \note 	A valid test on \c *this is only done by an assert.
	 */
	void
	addNewSample(
		const Sample_t&		newSample)
	{
		yggdrasil_assert(this->isValid());

		//Test the size
		if(newSample.nDims() != nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenstion that should be added has the wrong dimension. It is a " + std::to_string(newSample.nDims()) + " dimensional sample, but the structure is a " + std::to_string(nDims()));
		};

		//Adding
		const auto Size = m_dims.size();
		for(Size_t i = 0; i != Size; ++i)
		{
			m_dims[i].addNewData(newSample[i]);
		};

		return;
	}; //End: addNewSample

	/**
	 * \brief	Thsi function returns a copy of the ith sample.
	 *
	 * It is important that this funciton only returns a reference.
	 * It is thus not possible to manipulate the sample that is
	 * stored inside *this through the returned object.
	 *
	 * \param  i	The sample index that should be accessed.
	 *
	 * \throw	If an out of bound access is detected.
	 */
	Sample_t
	getSample(
		const Size_t i)
	 const
	{
		yggdrasil_assert(this->isValid());

		//Get the gimensiions
		const Size_t D = this->nDims();

		//Test if the out of bound access happens
		if(!(i < this->nSamples()))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Tried to access a sample that has index " + std::to_string(i) + ", but the container only has " + std::to_string(this->nSamples()));
		};

		//Create a sample for returning
		Sample_t s(D);
		yggdrasil_assert(s.size() == D);

		//Exctracting the sample
		for(Size_t j = 0; j != D; ++j)
		{
			yggdrasil_assert(i < m_dims[j].size());
			s[j] = m_dims[j][i];
		}; //ENd for(j):

		//Return the sample
		return s;
	}; //End: getSample


	/**
	 * \brief	This function exchanges the sample that is stored at index i inside *thsi with the new sample s.
	 *
	 * This function mimiks the assignment operator that returns a reference.
	 *
	 * \param  s	The new sample that should be loaded.
	 * \param  i	The position at which s should be written to
	 *
	 * \throw 	If an out of bound access is detected, or compability conditions are not met.
	 */
	void
	setSampleTo(
		const Sample_t&		s,
		const Size_t 		i)
	{
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(s.isValid());

		//Get the gimensiions
		const Size_t D  = this->nDims();	//The dimension of the collection
		const Size_t Ds = s.nDims();		//THe dimension of the sample

		if(D != Ds)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension of the collection, " + std::to_string(D) + ", and the dimension of the sample, " + std::to_string(Ds) + ", are different.");
		};

		//Test if the out of bound access happens
		if(!(i < this->nSamples()))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Tried to access a sample that has index " + std::to_string(i) + ", but the container only has " + std::to_string(this->nSamples()));
		};

		//Load the sample
		for(Size_t j = 0; j != D; ++j)
		{
			yggdrasil_assert(i < m_dims[j].size());
			this->m_dims[j][i] = s[j];
		}; //ENd for(j):

		return;
	}; //ENd: setSampleTo



	/*
	 * ======================
	 * Matrix Accessing fucntion.
	 */
public:
	/**
	 * \brief	This function converts *this into a row major
	 * 		 matrix and returns the result.
	 *
	 * How the matrix canbe constructed is determined by the view parameter.
	 * If set to Sample each row will contain one sample, if set to Dimension
	 * each column will contain one sample.
	 *
	 * \param  view		How the matrix should be constructed.
	 */
	RowMatrix_t
	getRowMatrix(
		const eMatrixViewRow 	view)
	 const;


	/**
	 * \brief	This function converts *this into a column major
	 * 		 matrix and returns the result.
	 *
	 * How the matrix canbe constructed is determined by the view parameter.
	 * If set to Sample each row will contain one sample, if set to Dimension
	 * each column will contain one sample.
	 *
	 * \param  view		How the matrix should be constructed.
	 */
	ColMatrix_t
	getColMatrix(
		const eMatrixViewRow 	view)
	 const;



	/*
	 * ==================
	 * Accessing functions
	 *
	 * All the access fucntions does not check if *this is valid.
	 */
public:
	/**
	 * \brief	This function returns a reference to the \c i th dimension.
	 *
	 * \param  i	The dimension to access.
	 *
	 * \throw 	Throws if out of bound.
	 */
	DimensionArray_t&
	getDim(
		const Size_t 	i)
	{
		return m_dims.at(i);
	};


	/**
	 * \brief	Get a const reference to the \c i th dimension.
	 *
	 * \throw 	Throws if out of bound.
	 */
	const DimensionArray_t&
	getDim(
		const Size_t 	i)
	  const
	{
		return m_dims.at(i);
	};


	/**
	 * \brief	Get a const reference to the \c i th dimension, explicit constant version.
	 *
	 * \throw 	Throws if out of bound.
	 */
	const DimensionArray_t&
	getCDim(
		const Size_t 	i)
	  const
	{
		return m_dims.at(i);
	};


	/**
	 * \brief	Get reference to the \c i th dimension.
	 *
	 * This does not throw but asserts.
	 */
	DimensionArray_t&
	operator[](
		const Size_t 	i)
	  noexcept
	{
		yggdrasil_assert(size() > i);

		return m_dims[i];
	};


	/**
	 * \brief	Returns a const referendce to the \c i th dimension.
	 *
	 * This does not throw but asserts.
	 */
	const DimensionArray_t&
	operator[](
		const Size_t 	i)
	  const
	  noexcept
	{
		yggdrasil_assert(size() > i);

		return m_dims[i];
	};


	/**
	 * \brief	This function returns a constant reference of
	 * 		 to the dimensional array of dimension d.
	 *
	 * This function is equivalent to calling getCDim(d),
	 * but this one is requiered by the UCI.
	 *
	 * \param  d	The dimension that is requested.
	 *
	 * \throw	If an out of bound access happens
	 */
	const DimensionArray_t&
	getDimensionArray(
		const Size_t 	d)
	 const
	{
		if(!(d < m_dims.size()))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimensional array of dimension " + std::to_string(d) + " does not exists.");
		};

		return m_dims.at(d);
	}; //ENd: get dimension

	/*
	 * ===================
	 * Fourth Extension
	 */
public:
	/**
	 * \brief	This function sets the value of the jth component of the ith sample to v.
	 *
	 * This function is basically to manipulate a single sample without the need to load that sample first.
	 * This is important when working on Python or with templated funcitons.
	 *
	 * \param  i 	The sample that should me manipulated.
	 * \param  j 	The component that should be manipulated.
	 * \param  v 	The new value that the sample should have in component j.
	 *
	 * \throw 	If an error is detected.
	 */
	void
	setSampleComponent(
		const Size_t 		i,
		const Size_t 		j,
		const Numeric_t 	v)
	{
		//load parameters that are needed
		const Size_t NSamples = this->nSamples();
		const Size_t NDims    = this->nDims();

		//Make some tests
		if(NSamples <= i) //Negative samples are handled by type
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The sample " + std::to_string(i) + " does not exist. The container only has " + std::to_string(NSamples) + " many samples.");
		};

		if(NDims <= j)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(NDims) + ".");
		};

		if(isValidFloat(v) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Tried to set an invalid value.");
		};

		//This is a light wired consistency check
		yggdrasil_assert(m_dims.at(j).size() == NSamples);

		//Now set the value
		m_dims[j][i] = v;	//Here the order is different, we first have to load the dimensional array

		return;
	}; //End: setSample Component

	void
	setSampleComponent(
		const Size_t 		q1,
		const Numeric_t 	q2,
		const Size_t 		q3)
	 = delete;

	void
	setSampleComponent(
		const Numeric_t 	q1,
		const Size_t 		q2,
		const Size_t 		q3)
	 = delete;


	/**
	 * \brief	This function returns the value that sample i has in its jth component.
	 *
	 * \param  i	The index of the sample that should be accessed.
	 * \param  j	The component that should be accessed.
	 *
	 * \throw 	If inconcistencies are detected.
	 */
	Numeric_t
	getSampleComponent(
		const Size_t 		i,
		const Size_t 		j)
	 const
	{
		//load parameters that are needed
		const Size_t NSamples = this->nSamples();
		const Size_t NDims    = this->nDims();

		//Make some tests
		if(NSamples <= i) //Negative samples are handled by type
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The sample " + std::to_string(i) + " does not exist. The container only has " + std::to_string(NSamples) + " many samples.");
		};

		if(NDims <= j)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(NDims) + ".");
		};

		//This is a light wired consistency check
		yggdrasil_assert(m_dims.at(j).size() == NSamples);

		//Here the order is different, we first have to load the dimensional array
		const Numeric_t v = m_dims[j][i];

		return v;
	}; //End: getSampleComponent


	/**
	 * \brief	This functions sets the jth componetns of all samples to v.
	 *
	 * This functions is equivalent to calling the setSampleComponent function on any sample
	 * but is much more efficient.
	 *
	 * \param  j 	The component that should be manipulated.
	 * \param  v 	The new value that the samples should have in component j.
	 *
	 * \throw 	If an error is detected.
	 *
	 * \note	For consistency thsi fucntion checks, by means of assert, if all samples have the correct dimension.
	 */
	void
	setDimensionTo(
		const Size_t 		j,
		const Numeric_t 	v)
	{
		//load parameters that are needed
		const Size_t NSamples = this->nSamples();
		const Size_t NDims    = this->nDims();

		if(NDims <= j)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(NDims) + ".");
		};

		if(isValidFloat(v) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Tried to set an invalid value.");
		};

		DimensionArray_t& dArr_j = m_dims.at(j);	//get a reference of the diomension we have to set
		yggdrasil_assert(dArr_j.size() == NSamples);	//small consistency check
		yggdrasil_assert((Size_t)dArr_j.getAssociatedDim() == j);

		//Setting the value of the array to one value.
		dArr_j.assign(v);

		yggdrasil_assert((Size_t)dArr_j.getAssociatedDim() == j);

		return;
	}; //End: setDimensionTo



	void
	setDimensionTo(
		const Numeric_t 	q1,
		const Size_t 		q2)
	 = delete;


	/**
	 * \brief	This function sets the jth component of the samples in
	 * 		 *this to the values indicated by the dimensional array dArr.
	 * 		 The dimension of that array is ignored.
	 *
	 * Thsi function allows to set the values of the component j of all sampels.
	 * Contrary to the setDimensionTo() function, which sets all to the same value.
	 * This fucntion sets the values to an individual value.
	 * The values are taken from the dimensional array.
	 * The dimension parrameter of teh array is ignored.
	 * But the array must be valid.
	 *
	 * \param  j 	The dimension that should be manipoulated.
	 * \param  dArr The dimensional array, dimension of it is ignored.
	 *
	 * \throw	If an inconsisitency is detected. The values that are used are only checked
	 * 		 by an assert if they are valid or not.
	 */
	void
	setDimensionArray(
		const Size_t 			j,
		const DimensionArray_t&		dArr)
	{
		//load parameters that are needed
		const Size_t NSamples = this->nSamples();
		const Size_t NDims    = this->nDims();
		const Size_t NdArr    = dArr.size();

		if(NDims <= j)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(NDims) + ".");
		};

		if(dArr.hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Can not load an invalid dimensianl array into the collection.");
		};

		if(NSamples != NdArr)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimensional array that should be loaded, has " + std::to_string(NdArr) + " many samples, but the collection has " + std::to_string(NSamples) + " many samples.");
		};

		DimensionArray_t& dArr_j = m_dims.at(j);	//get a reference of the diomension we have to set
		yggdrasil_assert(dArr_j.size() == NSamples);	//small consistency check
		yggdrasil_assert((Size_t)dArr_j.getAssociatedDim() == j);

		//Load the sample by usingthe coipy assignment operator
		//We have to use the internal arrays for that.
		//Thsi is needed to conservce the dimension
		dArr_j.internal_getInternalArray() = dArr.internal_getInternalArray();

		yggdrasil_assert(j == (Size_t)m_dims[j].getAssociatedDim());	//You may think I am paranoid, but this assert was needed to find a nasty bug.

		return;
	}; //End setDimensionArray(copy)


	/**
	 * \brief	This function sets the jth component of the samples in
	 * 		 *this to the values indicated by the dimensional array dArr.
	 * 		 The dimension of that array is ignored.
	 * 		 The array is moved into *this.
	 *
	 * Thsi function allows to set the values of the component j of all sampels.
	 * Contrary to the setDimensionTo() function, which sets all to the same value.
	 * This fucntion sets the values to an individual value.
	 * The values are taken from the dimensional array.
	 * The dimension parrameter of teh array is ignored.
	 * But the array must be valid.
	 *
	 * This function does not copy the values but moves the data into *this.
	 * This means dArr will be empty afterwards.
	 *
	 * \param  j 	The dimension that should be manipoulated.
	 * \param  dArr The dimensional array, dimension of it is ignored.
	 * 		 It will be empty afterwads.
	 *
	 * \throw	If an inconsisitency is detected. The values that are used are only checked
	 * 		 by an assert if they are valid or not.
	 */
	void
	setDimensionArray(
		const Size_t 			j,
		DimensionArray_t&&		dArr)
	{
		//load parameters that are needed
		const Size_t NSamples = this->nSamples();
		const Size_t NDims    = this->nDims();
		const Size_t NdArr    = dArr.size();

		if(NDims <= j)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(NDims) + ".");
		};

		if(dArr.hasValidDimensions() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Can not load an invalid dimensianl array into the collection.");
		};

		if(NSamples != NdArr)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimensional array that should be loaded, has " + std::to_string(NdArr) + " many samples, but the collection has " + std::to_string(NSamples) + " many samples.");
		};

		DimensionArray_t& dArr_j = m_dims.at(j);	//get a reference of the diomension we have to set
		yggdrasil_assert(dArr_j.size() == NSamples);	//small consistency check
		yggdrasil_assert((Size_t)dArr_j.getAssociatedDim() == j);

		//Load the sample by usingthe coipy assignment operator
		//We have to operate on the internal arrays to keep dimensions conserved.
		dArr_j.internal_getInternalArray() = std::move(dArr.internal_getInternalArray());

		yggdrasil_assert(j == (Size_t)m_dims[j].getAssociatedDim());	//You may think I am paranoid, but this assert was needed to find a nasty bug.

		return;
	}; //End setDimensionArray(move)


	/**
	 * \brief	This function allows to set the values of all samples in a certain dimension to
	 * 		 the values that are specified in the dimensional array.
	 * 		 The dimension to which we apply the modification is extracted from the array.
	 *
	 * \param  dArr		The array that is read.
	 *
	 * \throw	If an inconsistency is detected.
	 */
	void
	setDimensionArray(
		const DimensionArray_t& 	dArr)
	{
		//Test iof teh dimension is valid
		if(dArr.isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The passed dimensional array is not valid.");
		};

		//Call the implementing fucntion, that will also do the other tests
		this->setDimensionArray(dArr.getAssociatedDim(), dArr);

		//Return
		return;
	}; //End: setDimensionArray(copy)


	/**
	 * \brief	This function allows to set the values of all samples in a certain dimension to
	 * 		 the values that are specified in the dimensional array.
	 * 		 The dimension to which we apply the modification is extracted from the array.
	 * 		 The array is moved into *this.
	 *
	 * \param  dArr		The array that is read. The array is empyt afterwards.
	 *
	 * \throw	If an inconsistency is detected.
	 */
	void
	setDimensionArray(
		DimensionArray_t&& 	dArr)
	{
		//Test iof teh dimension is valid
		if(dArr.isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The passed dimensional array is not valid.");
		};

		//Load the dimension, to avoid problems with the move statement
		const Size_t j = dArr.getAssociatedDim();

		//Call the implementing fucntion, that will also do the other tests
		this->setDimensionArray(j, std::move(dArr));

		//Return
		return;
	}; //End: setDimensionArray(copy)


	/**
	 * \brief	This function sets the jth component of the samples in
	 * 		 *this to the values indicated by the Eigen Vector dArr.
	 *
	 * Thsi function allows to set the values of the component j of all sampels.
	 * Contrary to the setDimensionTo() function, which sets all to the same value.
	 * This fucntion sets the values to an individual value.
	 * The values are taken from the dimensional array.
	 * This function is for python.
	 *
	 * \param  j 	The dimension that should be manipoulated.
	 * \param  dArr The vector where the data is taken from.
	 *
	 * \throw	If an inconsisitency is detected. The values that are used are only checked
	 * 		 by an assert if they are valid or not.
	 *
	 * \note 	This function takes a vector that is compatible with teh requirements
	 * 		 that are imposed by pibind11.
	 */
	void
	setDimensionArray(
		const Size_t 						j,
		const yggdrasil_eigenRef_t<const PyVector_t>& 	dArr);


	/**
	 * \brief	This function sets the jth component of the samples in
	 * 		 *this to the values indicated by the Eigen Vector dArr.
	 *
	 * Thsi function allows to set the values of the component j of all sampels.
	 * Contrary to the setDimensionTo() function, which sets all to the same value.
	 * This fucntion sets the values to an individual value.
	 * The values are taken from the dimensional array.
	 *
	 * This function takes a normal Eigen::VectorXd type as argument.
	 *
	 *
	 * \param  j 	The dimension that should be manipoulated.
	 * \param  dArr The vector where the data is taken from.
	 *
	 * \throw	If an inconsisitency is detected. The values that are used are only checked
	 * 		 by an assert if they are valid or not.
	 */
	void
	setDimensionArray(
		const Size_t 						j,
		const yggdrasil_eigenRef_t<const Eigen::VectorXd>& 	dArr);


	/**
	 * \brief	Thsi is the teh core implementation function of the funciton that sets a complete dimension.
	 *
	 * \param  j	The dimension where we apply the change.
	 * \param  dArr The array that is loaded.
	 *
	 * \tparam  DimArr_t 	The type that we load from.
	 *
	 * \throw 	If inconsistencies are detected.
	 */
private:
	template<class 	DimArr_t>
	void
	internal_setDimensionArray(
		const Size_t 		j,
		const DimArr_t&		dArr);



	/*
	 * =====================
	 * Iterators
	 * 	Access to the dimensions
	 *
	 * These function does not check if *this is valid.
	 */
public:
	/**
	 * \brief	Get an iterator to the first dimension of \c *this .
	 */
	DimIterator_t
	beginDim()
	  noexcept
	{
		return m_dims.begin();
	};


	/**
	 * \brief	Returns a constant iterator to the first dimension of \c *this .
	 */
	cDimIterator_t
	beginDim()
	  const
	  noexcept
	{
		return m_dims.cbegin();
	};


	/**
	 * \brief	Returns a constant iterator to the first diemnsion; explicit verison.
	 */
	cDimIterator_t
	cbeginDim()
	  const
	  noexcept
	{
		return m_dims.cbegin();
	};


	/**
	 * \brief	Get an iterator to the past the end dimension.
	 */
	DimIterator_t
	endDim()
	  noexcept
	{
		return m_dims.end();
	};


	/**
	 * \brief	Returns a constant iterator to the past the end dimension.
	 */
	cDimIterator_t
	endDim()
	  const
	  noexcept
	{
		return m_dims.cend();
	};


	/**
	 * \brief	Returns a constant iterator to the past the end dimension; explicit verison.
	 */
	cDimIterator_t
	cendDim()
	  const
	  noexcept
	{
		return m_dims.cend();
	};



	/*
	 * =====================
	 * Iterators
	 * 	Access to the samples
	 *
	 * These function does not check if *this is valid.
	 */
public:
	/**
	 * \brief	Returns an iterator the the first sample of the \c i th dimension.
	 *
	 * \param  i 	The dimension to query.
	 *
	 * \throw	If the dimenimension does not exists.
	 */
	SampleIterator_t
	beginSample(
		const Int_t 	i)
	{
		return m_dims.at(i).begin();
	};


	/**
	 * \brief	Returns a constant iterator to the first sample of the \c i th dimension.
	 *
	 * \param  i 	The dimension to query.
	 *
	 * \throw 	If the dimension does not exists.
	 */
	cSampleIterator_t
	beginSample(
		const Int_t 	i)
	  const
	{
		return m_dims.at(i).cbegin();
	};


	/**
	 * \brief	Returns a constant iterator to the first sample of the \c i th dimension; Explicit version.
	 *
	 * \param  i 	The dimension to query.
	 *
	 * \throw 	If the dimension does not exists.
	 */
	cSampleIterator_t
	cbeginSample(
		const Int_t 	i)
	  const
	{
		return m_dims.at(i).cbegin();
	};

	/**
	 * \brief	Returns an iterator the the first sample of the \c i th dimension.
	 *
	 * \param  i 	The dimension to query.
	 *
	 * \throw	If the dimenimension does not exists.
	 */
	SampleIterator_t
	endSample(
		const Int_t 	i)
	{
		return m_dims.at(i).end();
	};


	/**
	 * \brief	Returns a constant iterator to the first sample of the \c i th dimension.
	 *
	 * \param  i 	The dimension to query.
	 *
	 * \throw 	If the dimension does not exists.
	 */
	cSampleIterator_t
	endSample(
		const Int_t 	i)
	  const
	{
		return m_dims.at(i).cend();
	};


	/**
	 * \brief	Returns a constant iterator to the first sample of the \c i th dimension; Explicit version.
	 *
	 * \param  i 	The dimension to query.
	 *
	 * \throw 	If the dimension does not exists.
	 */
	cSampleIterator_t
	cendSample(
		const Int_t 	i)
	  const
	{
		return m_dims.at(i).cend();
	};


	/*
	 * ========================
	 * Capacity managing functions
	 */
public:
	/**
	 * \brief	This function sets the size of the collection to newSize.
	 *
	 * This means it allocates the underlings dimensional arrays to hold newSize many samples.
	 * No further initalization is performed.
	 * Not that this function fails if the size of *this is not zero.
	 *
	 * \param  newSize	The new number of samples.
	 *
	 * \throw	If *this is invalid or memory allocation throws, also if *this is not empty.
	 */
	void
	resize(
		const Size_t newSize)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not resize an invalid sample collection.");
		};
		if(this->nSamples() != 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not resize a not empty sample collection.");
		};
		if(this->nDims() <= 0)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The dimension parameter has the wrong size.");
		};

		const Int_t D = this->nDims();

		//Now we iterate throught the dimensional array and allocate them.
		for(DimensionArray_t& da : m_dims)
		{
			da.firstUsageInit(newSize);
		}; //ENd for(da):

		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The resize operation resulted in an invalid collection.");
		};
		if(this->nDims() != D)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The resize operation changed the number of dimension.");
		};

		return;
	}; //ENd resize

	/**
	 * \brief	This function preallocate space.
	 *
	 * This fucntion preallocate space for the samples.
	 * It is only allowed if the size of *this is zero.
	 * It preallocate space but does not change the size of *this.
	 * Note that if *this is not empty no error is generated,
	 * instead no effect is done.
	 *
	 * \param  newCapacity		The new capacity.
	 *
	 * \throw	If *this is invalid or memory allocation throws, also if *this is not empty.
	 */
	void
	reserve(
		const Size_t newCapacity)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not resize an invalid sample collection.");
		};
		if(this->nDims() <= 0)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The dimension parameter has the wrong size.");
		};

		//Test if we have to do anything
		if(this->nSamples() <= 0)
		{
			return;
		};
		if(newCapacity <= 0)
		{
			return;
		};


		const Int_t D = this->nDims();

		//Now we iterate throught the dimensional array and allocate them.
		for(DimensionArray_t& da : m_dims)
		{
			da.firstUsageInit(0, newCapacity);
		}; //ENd for(da):

		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The resize operation resulted in an invalid collection.");
		};
		if(this->nDims() != D)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The resize operation changed the number of dimension.");
		};

		return;
	}; //ENd reserve


	/**
	 * \brief	This function clears the internal memeory allocation.
	 *
	 * This function dealocates the dimensional arrays.
	 * However the number of diomensions is retained.
	 */
	void
	clear()
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not resize an invalid sample collection.");
		};
		if(this->nDims() <= 0)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The dimension parameter has the wrong size.");
		};

		//Get the number of dimensions
		const Int_t D = this->nDims();

		for(Int_t d = 0; d != D; ++d)
		{
			DELETE_SAMPLES_FROM_COLLECTION_SINGLE_DIMENSION(*this, d);
		}; //ENd for(d)

		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The clear operation resulted in an invalid collection.");
		};
		if(this->nDims() != D)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The clear operation changed the number of dimension.");
		};

		return;
	}; //End: clear


	/*
	 * ========================
	 * Static functions
	 */
public:
	/**
	 * \brief	This function deletes all samples in dimension \c d from the collection \c sc.
	 *
	 * All samples in dimension d are deleted and the capacity is reduced to zero (probably).
	 * This is intended as an internal function.
	 *
	 * \param  d		The dimension to clear.
	 * \param  sc 		The sample collection we act on.
	 *
	 * \note	This function will transform *this into a invalid state.
	 * 		 It is only used internaly.
	 *
	 * \throw	If the underling function throws, this at least happens
	 * 		 if \c d is invalid.
	 */
	static
	void
	DELETE_SAMPLES_FROM_COLLECTION_SINGLE_DIMENSION(
		yggdrasil_sampleCollection_t&		sc,
		const Int_t 				d)
	{
		sc.priv_clearDimension(d);
		return;
	};


	/*
	 * ====================
	 * Private Functions
	 */
private:
	/**
	 * \brief	This function clears the dimension \c d.
	 *
	 * All samples in dimension d are deleted and the capacity is reduced to zero (probably).
	 *
	 * \param  d		The dimension to clear.
	 *
	 * \note	This function will transform \c *this into a invalid state.
	 *
	 * \throw	If \c d is invalid.
	 */
	void
	priv_clearDimension(
		const Int_t 	d)
	{
		//We will do the swap trick; The only enured way

		if(d == Constants::YG_INVALID_DIMENSION)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(d) + " is invalid.");
		};

		//It must be greater or equal than zero
		if(d < 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(d) + " is invalid.");
		};

		if(d >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(d) + " is invalid (too large).");
		};

		if(d >= this->nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension " + std::to_string(d) + " does not exists; *this has only " + std::to_string(this->nDims()) + " dimensions");
		};

		//We now create an empty array
		DimensionArray_t empytArray(d);

		//Now we swap
		m_dims.at(d).swap(empytArray);
		yggdrasil_assert(m_dims.at(d).getAssociatedDim() == d);

		//The emptyArray, which contains the data, will go out of scope and be deleted
		return;
	}; //End: priv_clearDimension


	/*
	 * ======================
	 * Private members
	 */
private:
	DimArrayCollection_t 	m_dims;		//!< This variable stores the dimensions
}; //End class: yggdrasil_sampleCollection_t



YGGDRASIL_NS_END(yggdrasil)




