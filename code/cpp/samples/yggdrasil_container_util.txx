#pragma once
/**
 * \brief	This file implements the template functions that will convert matrixes to other conatiners.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <core/yggdrasil_eigen.hpp>

#include <samples/yggdrasil_matrixOrder.hpp>
#include <samples/yggdrasil_sample.hpp>

YGGDRASIL_NS_START(yggdrasil)

/**
 * \brief	This fucntion loads the matrix mat into the given container cont.
 * 		 The matrix is interpreted according to the view argument.
 *
 * The container will be filled using the UCI.
 * Only the dimension of the container must be correct, the size
 * will be set by this function.
 *
 * \param  container 		The container that should be filled.
 * \param  mView		How the matrix should be interpreted.
 * \param  mat 			This is the matrix that should be loaded
 *
 * \tparam  ConatinerType	The type of the contianer.
 * \tparam  MatrixType		The type of the matrix.
 */
template<
	class 	ConatinerType,
	class 	MatrixType
>
void
ygInternal_loadMatrixIntoContainer(
	ConatinerType* const 					container,
	const eMatrixViewRow&					mView,
	const yggdrasil_eigenRef_t<const MatrixType>&		mat)
{
	//Make some tests
	if(container == nullptr)
	{
		throw YGGDRASIL_EXCEPT_InvArg("Passewd the nullpointer to the function.");
	};

	//Load some parameter
	const Size_t nDims 	= ygInternal_nDimsFromMatrix(mat, mView);
	const Size_t nSamples 	= ygInternal_nSamplesFromMatrix(mat, mView);

	//Select the case
	if(ygInternal_isSampleView(mView) == true)
	{
		/*
		 * We have the sample view.
		 * This means a row encodes a full sample and the number of
		 * dimension is equal the columns of *this
		 */

		//Test if the container has the correct dimension
		if(container->nDims() != nDims)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The conatiner has the wrong dimension it has " + std::to_string(container->nDims()) + ", but it should have " + std::to_string(nDims));
		};

		//Test if the number of samples is correct and addpat them if needed
		if(container->nSamples() != nSamples)
		{
			//We must first clear if the size is not zero
			if(container->nSamples() != 0)
			{
				//We have to clear it, because of the restrictions imposed by resize
				container->clear();
			}; //End if: clearing

			//Now we can resize the container
			container->resize(nSamples);
			yggdrasil_assert(container->nSamples() == nSamples);
		}; //End if: addpat the sample size

		//This is a working sample
		yggdrasil_sample_t ySample(nDims);

		//Now we can iterate over the samples, this means the different rows
		for(Size_t i = 0; i != nSamples; ++i)
		{
			//Note that the returned type by this is not necessary an Eigen vector,
			//in fact it is most likely not, this is the reason why we use auto.
			const auto sample_i = mat.row(i);

			//Loading everything into the woring sample
			for(Size_t j = 0; j != nDims; ++j)
			{
				//load the value
				const Numeric_t jValue = sample_i[j];

				//Write it into the sample
				ySample[j] = jValue;
			}; //End for(j): going throught the dimensions


			//insert the sample into the container
			container->setSampleTo(ySample, i);
		}; //End for(i): iterating over the samples

		//
		//END
	}
	else
	{
		/*
		 * We are now in the case where we interprete a single row as dimension.
		 * Thus the samples are stored as columns
		 */

		//Test if the container has the correct dimension
		if(container->nDims() != nDims)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The conatiner has the wrong dimension it has " + std::to_string(container->nDims()) + ", but it should have " + std::to_string(nDims));
		};

		//Test if the number of samples is correct and addpat them if needed
		if(container->nSamples() != nSamples)
		{
			//We must first clear if the size is not zero
			if(container->nSamples() != 0)
			{
				//We have to clear it, because of the restrictions imposed by resize
				container->clear();
			}; //End if: clearing

			//Now we can resize the container
			container->resize(nSamples);
			yggdrasil_assert(container->nSamples() == nSamples);
		}; //End if: addpat the sample size

		//This is a working sample
		yggdrasil_sample_t ySample(nDims);

		//Now we can iterate over the samples, this means the different rows
		for(Size_t i = 0; i != nSamples; ++i)
		{
			//Note that the returned type by this is not necessary an Eigen vector,
			//in fact it is most likely not, this is the reason why we use auto.
			const auto sample_i = mat.col(i);

			//Loading everything into the woring sample
			for(Size_t j = 0; j != nDims; ++j)
			{
				//load the value
				const Numeric_t jValue = sample_i[j];

				//Write it into the sample
				ySample[j] = jValue;
			}; //End for(j): going throught the dimensions


			//insert the sample into the container
			container->setSampleTo(ySample, i);
		}; //End for(i): iterating over the samples

		//
		//END
	}; //End else: handling of different scheme


	/*
	 * We are now done with the code
	 */
	return;
}; //End: load matrix




/**
 * \brief	This fucntion creates a matrix out of a container.
 *
 * This fucntion is like the inverse of the load function.
 * It also allows to controll the writting of the matrix by a
 * view parameter.
 *
 * \param  container		The container we load from.
 * \param  mat 			The matrix we read from.
 * \param  mView		How the matrix is interpreted.
 *
 * \tparam  ContainerType	The container type, that supports the UCI.
 * \tparam  MatrixType 		The type of the matrix.
 * \tparam  enforceDim 		If set to true, the dimensionaliyt of the samples will be checkded by an exception.
 * 				 This flass is only usefull for the sample list, thus it defaults to false.
 */
template<
	class 	ConatinerType,
	class 	MatrixType,
	bool 	enforceDim = false
>
void
ygInternal_loadContainerIntoMatrix(
	const ConatinerType& 			container,
	MatrixType&				mat,
	const eMatrixViewRow&	 		mView)
{
	//First load general parameters
	const Size_t nDims    = container.nDims();
	const Size_t nSamples = container.nSamples();
	yggdrasil_assert(nDims > 0);


	//Now select how the matrix should be interpreted
	if(ygInternal_isSampleView(mView) == true)
	{
		/*
		 * A row is interpreted as a sample.
		 *
		 * So the number of samples is equal the number of rows
		 * and the number of columns is interpreted as the dimension.
		 */

		//Resize the matrix, souch that it has the right dimensions
		mat.resize(nSamples, nDims);

		if(mat.rows() != nSamples)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The allocation of the matrix failed, it had " + std::to_string(mat.rows()) + " many rows instead of " + std::to_string(nSamples));
		};
		if(mat.cols() != nDims)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The allocation of the matrix failed, it had " + std::to_string(mat.cols()) + " many cols instead of " + std::to_string(nDims));
		};

		//Going through the samples
		for(Size_t i = 0; i != nSamples; ++i)
		{
			//Test if the matrix is correct
			yggdrasil_assert(i < Size_t(mat.rows()));

			//Load the current sample
			const yggdrasil_sample_t ySample = container.getSample(i);

			if(enforceDim && (Size_t(ySample.nDims()) != nDims))
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("Found a sample with the wrong dimension.");
			};

			yggdrasil_assert((Size_t)ySample.nDims() == nDims);	//ENforce correct size;
										//Special reasons why it is still here

			//Now copy the dimensions into the matrix
			for(Size_t j = 0; j != nDims; ++j)
			{
				yggdrasil_assert(j < Size_t(mat.cols()));
				mat(i, j) = ySample[j];
			}; //ENd for(j): Copy the samples

			//
			// We have handled the sample
		}; //ENd for(i):

		//
		//End: Handling the sample case
	}
	else
	{
		/*
		 * A row is a dimension,
		 * thus we must iterating over the columns for the samples.
		 */
		mat.resize(nDims, nSamples);

		if(mat.cols() != nSamples)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The allocation of the matrix failed, it had " + std::to_string(mat.cols()) + " many cols instead of " + std::to_string(nSamples));
		};
		if(mat.rows() != nDims)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The allocation of the matrix failed, it had " + std::to_string(mat.rows()) + " many rows instead of " + std::to_string(nDims));
		};

		//Going through the samples
		for(Size_t i = 0; i != nSamples; ++i)
		{
			//Test if all is okay
			yggdrasil_assert(i < Size_t(mat.cols()));

			//Load the current sample
			const yggdrasil_sample_t ySample = container.getSample(i);
			yggdrasil_assert(Size_t(ySample.nDims()) == nDims);

			//Coping the sample into the matrix
			for(Size_t j = 0; j != nDims; ++j)
			{
				//Test if the matrix is right
				yggdrasil_assert(j < Size_t(mat.rows()));

				//Actual coping
				mat(j, i) = ySample[j];
			}; //End for(j): coping the sample
		}; //ENd for(i): handling samples

		//
		//End: Handling the dimension case
	}; //End else: a row is interpreted as dimension

	//
	//We are done
	return;
}; //ENd: write into matrix





YGGDRASIL_NS_END(yggdrasil)





