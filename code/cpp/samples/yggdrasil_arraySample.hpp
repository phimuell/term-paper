#pragma once
/**
 * \brief	This file implements a representation a
 * 		 space efficient representation of the samples.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <core/yggdrasil_eigen.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_matrixOrder.hpp>

//Including std
#include <vector>


YGGDRASIL_NS_START(yggdrasil)

//FWD of the Sample list class
class yggdrasil_sampleList_t;

//FWD of the sample collection type
class yggdrasil_sampleCollection_t;


/**
 * \class 	yggdrasil_arraySample_t
 * \brief	Representation of many samples.
 *
 * This class allows to store many samples in a very space effiient way.
 * The reaosn is that a ::std::vector<Sample_t> is basically an
 * array of arrays, with incures some overhead.
 * It is nice but not so good.
 * This class is provided to tackel that problem.
 * It basically stores one single array with data.
 * The samples are then interpreted as a consecutive range
 * of a certain length.
 *
 * When the array is accessesed the sample is then cnstructed on
 * demand. The problem is that this class will not (at least in the near
 * future) allow range based loops.
 *
 * THis class implements the universal container interface.
 *
 * \note	This class is many used for interacting with C or so.
 *
 * \note 	There is a using that declares the yggdrasil_sampleArray_t container.
 * 		 This is an alias of this container. The reason is that this is more canonical.
 * 		 Hower the code is not changed.
 */
class yggdrasil_arraySample_t
{
	/*
	 * =================
	 * Typedef
	 */
public:
	using Vector_t		= yggdrasil_valueArray_t<Numeric_t>;	//!< This is the underlying implementation of the collection of numbers
								//!< This typedef is meant for internal work
	using Allocator_t 	= Vector_t::allocator_type;	//!< This is the allocator type, only for internal use
	//using value_type 	= Vector_t::value_type;		//!< The underlying type of sample
	//using reference 	= Vector_t::reference;		//!< This is a a reference
	//using const_reference	= Vector_t::const_reference;	//!< This is a constant reference
	//using iterator 		= Vector_t::iterator;		//!< The iterator allows traversing the dimensions
	//using const_iterator	= Vector_t::const_iterator;	//!< This is the constant iterator

	using Size_t 		= yggdrasil::Size_t;		//!< This is the size type
	using size_type 	= Size_t;			//!< also size type for compability

	using Sample_t 		= ::yggdrasil::yggdrasil_sample_t;	//!< This is the smaple type.
	using DimensionArray_t 	= yggdrasil_dimensionArray_t;	//!< This is the type for storing a single dimension

	//These are the types for the Eigen integration
	using ColMatrix_t 	= yggdrasil_eigenColMatrix_t;	//This is a colunm major matrix
	using RowMatrix_t 	= yggdrasil_eigenRowMatrix_t;	//This is a row major matrix
	using PyVector_t 	= yggdrasil_eigenRowVector_t;	//This is the type for a vector, that is used inside pyggdrasil.

	/*
	 * ===================
	 * Constructors
	 *
	 * This class does not support the late binding
	 * construction that is used.
	 */
public:
	/**
	 * \brief	This is the proper building construction.
	 *
	 * This constructor aqllocates the internal array.
	 * Given the dimension nDim of the samples and the
	 * numbers of samples.
	 *
	 * \param  nDims	The dimension of the samples.
	 * \param  nSamples	The number of samples.
	 */
	yggdrasil_arraySample_t(
		const Size_t 	nDims,
		const Size_t 	nSamples)
	 :
	  m_samples(nDims * nSamples),
	  m_nDims(nDims),
	  m_nSamples(nSamples)
	{
		//We do not allow the dimension to be zero
		if(m_nDims == 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension is zero.");
		};

		//It must be greater than zero
		if(m_nDims <= 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid. 8");
		};

		if(m_nDims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid (to large).");
		};
	}; //End: building constructor


	/**
	 * \brief	This function is used to contract *this without the need of
	 * 		 knowing the number of samples in advance.
	 *
	 * This function is basically the same as the building constructor with two
	 * arguments, but do not allocate memeory, since the number of samples
	 * is assumend to be zero.
	 *
	 * \param  nDims	The number of dimensions.
	 */
	yggdrasil_arraySample_t(
		const Size_t 	nDims)
	 :
	  m_samples(),
	  m_nDims(nDims),
	  m_nSamples(0)
	{
		//We do not allow the dimension to be zero
		if(m_nDims == 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension is zero.");
		};

		//It must be greater than zero
		if(m_nDims <= 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid. 8");
		};

		if(m_nDims >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimenions " + std::to_string(m_nDims) + " is invalid (to large).");
		};
	};


	/**
	 * \brief	This constructor builds *this from the Eigen matrix.
	 *
	 * The matrix is a column major matrix.
	 * It is passed in an Eigen Reference type, thus allowing non pure
	 * instance to bind to that constructor.
	 *
	 * How the matrix is interpreted depends on the view parameter.
	 * This constructor is most efficient if it is set to Sample,
	 * meaning that each column will contain one sample.
	 *
	 * This constructor is most efficient if view is set to Dimension.
	 *
	 * \param  matrix 	This is the matrix that is passed.
	 * \param  view 	How the matrix should be interpreted.
	 */
	explicit
	yggdrasil_arraySample_t(
		const yggdrasil_eigenRef_t<const ColMatrix_t>& 		mat,
		const eMatrixViewRow& 					view);


	/**
	 * \brief	This construct takes an Eigen Row Major Matrix.
	 *
	 * It uses the Ref class of Eigen to allow the binding of a vide range of
	 * types that all could be expresed as a row major matrix.
	 *
	 * How the matrix is interpreted is controlled by the view parameter.
	 * This constructor is most efficient if it is set to Dimensions.
	 * This means that the samples are located in the columns of the matrix.
	 *
	 * This constructor is most efficient if view is set to Sample.
	 *
	 * \param  matrix	The matrix that should be loaded.
	 * \param  view 	How the matrix should be interpreted.
	 */
	explicit
	yggdrasil_arraySample_t(
		const yggdrasil_eigenRef_t<const RowMatrix_t>& 		matrix,
		const eMatrixViewRow& 					view);


	/**
	 * \brief	This constructor performs a deep copy of the sample list sList.
	 *
	 * This constructor transforms a sample list, which is passed by the sList argument
	 * into a sample array container type.
	 *
	 * \param  sList 	The list that should be loaded.
	 *
	 * \throw 	If errors occured.
	 */
	explicit
	yggdrasil_arraySample_t(
		const yggdrasil_sampleList_t& 		sList);


	/**
	 * \brief	This constructor performs a deep copy of the sample collection sCol.
	 *
	 * The collection is copied int this, the collection is not modified.
	 * This constructor provides a way of transforming a sample collection into an sample array.
	 *
	 * \param  sCol		The sample collection that is loaded.
	 *
	 * \throw 	If errory are detected.
	 */
	explicit
	yggdrasil_arraySample_t(
		const yggdrasil_sampleCollection_t& 	sCol);


	/**
	 * \brief	This is a clone function.
	 *
	 * This fucntion essentially performs a copy operation of *this.
	 * It is provided such that all containers can be copied without the need
	 * of knowing their type, this is usefull in Python, but not realy in C++.
	 */
	yggdrasil_arraySample_t
	clone()
	 const
	{
		return yggdrasil_arraySample_t(*this);
	}; //End clone function


	/**
	 * \brief	Default constructor.
	 */
	yggdrasil_arraySample_t()
	 = delete;


	/**
	 * \brief	The copy constructor.
	 *
	 * Also Defaulted. Again it must be explicit.
	 */
	explicit
	yggdrasil_arraySample_t(
		const yggdrasil_arraySample_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Also defaulted, but unlike the copy constructor not explicit.
	 */
	yggdrasil_arraySample_t(
		yggdrasil_arraySample_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Also defaulted.
	 */
	yggdrasil_arraySample_t&
	operator=(
		const yggdrasil_arraySample_t&) = default;

	/**
	 * \brief	Move assignment.
	 *
	 * Is also defaulted, it is not marked as noexcept,
	 * to have compability with pre C++17.
	 */
	yggdrasil_arraySample_t&
	operator=(
		yggdrasil_arraySample_t&&) = default;



	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_arraySample_t() = default;



	/*
	 * =======================
	 * Size functions
	 */
public:
	/**
	 * \brief	Returns the number of the dimension a sample has.
	 *
	 */
	Size_t
	nDims()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDims > 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert((m_nSamples != 0) ? (m_samples.size() % m_nSamples == 0) : true);

		return m_nDims;
	}; //End: get nDims


	/**
	 * \brief	This fucntion returns the number of sampels that are stored inside *this.
	 */
	Size_t
	nSamples()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDims > 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert((m_nSamples != 0) ? (m_samples.size() % m_nSamples == 0) : true);

		return m_nSamples;
	}; //End: nSamples


	/**
	 * \brief	Thsi function returns the numbers of samples in *this.
	 *
	 * This function is provided for syntactical reasons.
	 */
	Size_t
	size()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDims > 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert((m_nSamples != 0) ? (m_samples.size() % m_nSamples == 0) : true);

		return m_nSamples;
	}; //End: size



	/*
	 * =========================
	 * This functions allows to access the samples
	 * And also the underling memeroy.
	 */
public:
	/**
	 * \brief	This function returns the n-th samples.
	 *
	 * Notice that this function returns a copy of the sample
	 * and not a reference that can be written to.
	 * It is planed as an extention that a proxy class will be
	 * implemented that allows assignment.
	 *
	 * \param  n 	The sample to access.
	 *
	 * \note	This function only checks with asserts if
	 * 		 the access is inside the bounds.
	 */
	Sample_t
	operator[] (
		const Size_t 	n)
	 const
	{
		//Consistency checks
		yggdrasil_assert(m_nDims > 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert((m_nSamples != 0) ? (m_samples.size() % m_nSamples == 0) : true);

		//Input checks
		yggdrasil_assert(n < m_nSamples);
		yggdrasil_assert((n + 1) * m_nDims <= m_samples.size());	//Here it has to be the <= operator, because thjis is the end

		//Construct a smaple
		const Numeric_t* const startRange = m_samples.data() + n * m_nDims;
		const Numeric_t* const endRange   = startRange + m_nDims;
		Sample_t sample(startRange, endRange);

		//Return the smaple
		return sample;
	}; //End operator[]

	/**
	 * \brief	This function returns the n-th samples.
	 *
	 * Notice that this function returns a copy of the sample
	 * and not a reference that can be written to.
	 * It is planed as an extention that a proxy class will be
	 * implemented that allows assignment.
	 *
	 * \param  n 	The sample to access.
	 *
	 * \throw	If an out of bound access is detected.
	 */
	Sample_t
	at(
		const Size_t 	n)
	 const
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		//Input checks
		if(!(n < m_nSamples))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(n) + ", but there where only " + std::to_string(m_nSamples));
		};
		yggdrasil_assert((n + 1) * m_nDims <= m_samples.size());	//Here it has to be the <= operator, because thjis is the end


		//Construct a smaple
		const Numeric_t* const startRange = m_samples.data() + n * m_nDims;
		const Numeric_t* const endRange   = startRange + m_nDims;
		Sample_t sample(startRange, endRange);

		//Return the smaple
		return sample;
	}; //End operator[]



	/**
	 * \brief	This function returns a copy of the ith sample.
	 *
	 * This fucntion is part of the UCI.
	 * It performs a copy operation on the sample and returns it.
	 *
	 * \param  i	The index of the sample that should be accessed.
	 *
	 * \throw 	If an out of bound access happens
	 */
	Sample_t
	getSample(
		const Size_t 	i)
	 const
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		//Input checks
		if(!(i < m_nSamples))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(i) + ", but there where only " + std::to_string(m_nSamples));
		};
		yggdrasil_assert((i + 1) * m_nDims <= m_samples.size());	//Here it has to be the <= operator, because thjis is the end


		//Construct a smaple
		const Numeric_t* const startRange = m_samples.data() + i * m_nDims;
		const Numeric_t* const endRange   = startRange + m_nDims;
		Sample_t sample(startRange, endRange);

		//Return the smaple
		return sample;
	}; //ENd: getsample


	/**
	 * \brief	This fucntion sets the sample at index i to the given sample s.
	 *
	 * This function is part of the UCI.
	 * It is basically an alias of the load function.
	 * However this function checks if the sample has the right size
	 *
	 * \param  s	The sample to load.
	 * \param  i	The index where we have the sample writting to.
	 *
	 * \throw 	If an underling function throws.
	 */
	void
	setSampleTo(
		const Sample_t&		s,
		const Size_t 		i)
	{
		yggdrasil_assert(m_nDims > 0);
		if((Size_t)s.nDims() != m_nDims)	//check is needed, since the load function used does not perform it
		{
			throw YGGDRASIL_EXCEPT_InvArg("The sample does not have the correct dimension.");
		};
		this->loadSample(s.data(), i);

		return;
	}; //ENd set sample to


	/**
	 * \brief	This fucntion writes the information given at location src.
	 *
	 * This function basically copies the location pointed to by src into
	 * the correct location of the internal array.
	 * It is allumend that src is at least nDims long.
	 * No check is performed.
	 * It is recomended that this function is not used
	 * and the more safer variant that takes a sample as argument is used.
	 *
	 * \param  src		Pointer to the start.
	 * \param  idx		The smaple where we want to write to.
	 * 			 See it as destination.
	 *
	 * \throw	If an out of bound access is detected.
	 */
	void
	loadSample(
		const Numeric_t* const 		src,
		const Size_t 			idx)
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		if(src == nullptr)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The pointer to load is the null pointer.");
		};

		//Input checks
		if(!(idx < m_nSamples))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(idx) + ", but there where only " + std::to_string(m_nSamples));
		};
		yggdrasil_assert((idx + 1) * m_nDims <= m_samples.size());	//Here it has to be the <= operator, because thjis is the end

		//Get the start of the destination
		Numeric_t* dst = m_samples.data() + idx * m_nDims;

		//Perform the copy
		for(Size_t i = 0; i != m_nDims; ++i)
		{
			yggdrasil_assert(isValidFloat(src[i]) == true);
			dst[i] = src[i];
		}; //End for(i):

		//Return
		return;
	}; //End: load sample


	/**
	 * \brief	This is a sample load function that loads from a sample.
	 *
	 * This function should be prefered by the user code.
	 * This fucntions cheks if compability conditions are met and then
	 * calls the functions with a pointer.
	 *
	 * \param  src		The sanmple that should be inserted.
	 * \param  idx		The smaple where we want to write to.
	 * 			 See it as destination.
	 *
	 * \throw	If an out of bound access is detected.
	 * 		 or if the sample has the wrong dimension.
	 */
	void
	loadSample(
		const Sample_t&		s,
		const Size_t 		idx)
	{
		//COnsistency check
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		if(m_nDims != s.size())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The sample and the array has the wrong dimension, the sample has dimension " + std::to_string(s.size()) + " but the array has " + std::to_string(m_nDims));
		};

		//insert the sample
		this->loadSample(s.data(), idx);

		return;
	}; //End: load sample


	/**
	 * \brief	This function adds a new sample at the end of *this.
	 *
	 * Note that this function can lead to a memeory realocation.
	 * Thus all pointers will become invalid.
	 * This function also checks if the size of the sample that is added is correct.
	 *
	 * \param  s	The samples that should be added
	 *
	 * \throw	If the dimension of teh sample is not correct.
	 */
	void
	addNewSample(
		const Sample_t&		s)
	{
		//Check consistency
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		if((Size_t)s.nDims() != this->nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The sample has the wrong dimension.");
		};
		const Size_t D = this->nDims();

		//We use the internal push back function
		for(Size_t i = 0; i != D; ++i)
		{
			m_samples.push_back(s[i]);
		}; //End for(i): adding the samples

		//Increase the number of samples
		m_nSamples += 1;

		//Check consistency again
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		return;
	}; //ENd: add new sample


	/**
	 * \brief	This function returns a doimensional array
	 * 		 of the requested dimension.
	 *
	 * Note this class does not store the samples in a compatible format.
	 * So an array has to be allocated and must be returned.
	 * This can be costly.
	 * If you extensively need thsi function, consider switching to
	 * the sample collection.
	 *
	 * \param  d	The requested dimension.
	 *
	 * \throw	If an underling function throws or an out of bound is registered.
	 */
	DimensionArray_t
	getDimensionArray(
		const Size_t 	d)
	 const
	{
		if(!(d < this->m_nDims))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimensional array of dimension " + std::to_string(d) + " does not exists.");
		};

		//This is the array that is used
		DimensionArray_t dimArray(d);	//Initalize it with the dimension, not possible to allocate it doirectly
		yggdrasil_assert(dimArray.isDimensionIllegal() == false);

		//Allocating the space that is needed for the computation
		dimArray.firstUsageInit(this->m_nSamples);
		yggdrasil_assert(dimArray.size() == this->m_nSamples);

		//We we call the template function to fill the array
		this->internal_getDimensionArray<DimensionArray_t>(d, &dimArray);

		//Return the array
		return dimArray;
	}; //End: getDimensionArray


	/**
	 * \brief	This is a templated method that allows the reading the dimension
	 * 		 array in an arbitrary container, that supports some basic functions.
	 *
	 * This function is equivalent to the getDimensionalArray(d) fucntion.
	 * But the cotainer that is used can be selected via template argument.
	 * The argument is not returned but given as argument (pointer to it to
	 * indicate that we modify it.).
	 *
	 * \param  d 		This is the dimension we request.
	 * \param  dimArr	This is the destination we use.
	 *
	 * \param  dimArr_t 	This is the type of the conatiner that is used.
	 * 			 it must support operator[], size() and resize(newSize)
	 *
	 * \note	This function has an internal prefix.
	 * 		 Thsi means the user can not relay on its existence nor that it
	 * 		 will always do the same.
	 */
	template<
		class 	dimArray_t
	>
	void
	internal_getDimensionArray(
		const Size_t 		d,
		dimArray_t* const 	dimArray)
	 const
	{
		if(!(d < this->m_nDims))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimensional array of dimension " + std::to_string(d) + " does not exists.");
		};

		//Test if the destination is large enough.
		if(dimArray->size() != m_nSamples)
		{
			//The destination has the wrong size, we resize it now
			dimArray->resize(m_nSamples);
		}; //End if: resize samples


		//Now going through the array (*this) and collecting the data
		for(Size_t i = 0; i != m_nSamples; ++i)
		{
			//This is the index that we are going to access in the array of thsi
			const Size_t idx = d + i * m_nDims;	//TODO: strength reduction

			//Test if the index is valid (only in debug)
			yggdrasil_assert(idx < m_samples.size());

			//Load the value
			const Numeric_t value = m_samples[idx];

			//Check if the access is save
			yggdrasil_assert(i < dimArray->size());

			//write the value to the destination
			(*dimArray)[i] = value;
		}; //End for(i): goiung through


		//Return the function
		return;
	}; //End: internal_getDimensionArray


	/*
	 * =========================
	 * Fourth Extension
	 *
	 * Here are the functions that implements the fourth extension of the container
	 */
public:
	/**
	 * \brief	This function allows to set the jth component of the ith sample.
	 *
	 * This function allows to manipulate a component of a sample without the need of
	 * loading it first. This reduces some over load. it is needed since only
	 * the sample list can return references.
	 *
	 * \param  i 	The sample that should me manipulated.
	 * \param  j 	The component that should be manipulated.
	 * \param  v 	The new value that the sample should have in component j.
	 *
	 * \throw 	If an error is detected.
	 */
	void
	setSampleComponent(
		const Size_t 		i,
		const Size_t 		j,
		const Numeric_t 	v)
	{
		//Make some tests
		if(m_nSamples <= i) //Negative samples are handled by type
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The sample " + std::to_string(i) + " does not exist. The container only has " + std::to_string(m_nSamples) + " many samples.");
		};

		if(m_nDims <= j)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(m_nDims) + ".");
		};

		if(isValidFloat(v) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Tried to set an invalid value.");
		};

		//Calculate the location where the value is
		const Size_t idx = i * m_nDims + j;

		yggdrasil_assert(idx < m_samples.size());	//Make a consistency check.


		//Set the value
		m_samples[idx] = v;

		return;
	}; //End setSampleComponent

	void
	setSampleComponent(
		const Size_t 		q1,
		const Numeric_t 	q2,
		const Size_t 		q3)
	 = delete;

	void
	setSampleComponent(
		const Numeric_t 	q1,
		const Size_t 		q2,
		const Size_t 		q3)
	 = delete;


	/**
	 * \brief	This function returns the value of the ith sample in its jth component.
	 *
	 * This fucntion allows to access the value of a single component of one sample.
	 * Without the need to create a sample.
	 *
	 * \param  i 	The sample that should me manipulated.
	 * \param  j 	The component that should be manipulated.
	 *
	 * \throw 	If an error is detected.
	 */
	Numeric_t
	getSampleComponent(
		const Size_t 		i,
		const Size_t 		j)
	 const
	{
		//Make some tests
		if(m_nSamples <= i) //Negative samples are handled by type
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The sample " + std::to_string(i) + " does not exist. The container only has " + std::to_string(m_nSamples) + " many samples.");
		};

		if(m_nDims <= j)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(m_nDims) + ".");
		};

		//Calculate the location where the value is
		const Size_t idx = i * m_nDims + j;

		yggdrasil_assert(idx < m_samples.size());	//Make a consistency check.


		//Return the sample component (no reference)
		return Numeric_t(m_samples[idx]);
	}; //End getSampleComponent


	/**
	 * \brief	This function allows to set the value of the component j of all sample
	 * 		 to the value v.
	 *
	 * This function allows to manipulate the component of all samples at once.
	 *
	 * \param  j 	The dimension that is modiofied.
	 * \param  v	The value that should be used.
	 *
	 * \param 	If an out of bound access is detected.
	 */
	void
	setDimensionTo(
		const Size_t 		j,
		const Numeric_t 	v)
	{
		//Make some tests
		if(m_nDims <= j)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(m_nDims) + ".");
		};

		if(isValidFloat(v) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Tried to set an invalid value.");
		};

		//This is the accessing index
		Size_t idx = j;

		//Iterate over all the samples
		for(Size_t i = 0; i != m_nSamples; ++i)
		{
			//Check if we make an out of bound index
			yggdrasil_assert(idx < m_samples.size());

			//Set the value
			m_samples[idx] = v;

			//Increment the the index. We have to increment it by the number of dimensions
			idx += m_nDims;
		}; //End for(i): iterating over all teh samples

		//Return
		return;
	}; //End: setDimensionTo

	void
	setDimensionTo(
		const Numeric_t 	q1,
		const Size_t 		q2)
	 = delete;


	/**
	 * \brief	This function sets the value of component j to the value in the dimensinal array.
	 *
	 * This function allows to a single component of all samples at once.
	 * The values the components should be set to is taken from teh dimensional array.
	 * The dimensional parameter of the array is ignored.
	 * However the array must be valid.
	 *
	 * \param  j 	The dimension that should be manipoulated.
	 * \param  dArr The dimensional array, dimension of it is ignored.
	 *
	 * \throw	If an inconsisitency is detected. The values that are used are only checked
	 * 		 by an assert if they are valid or not.
	 */
	void
	setDimensionArray(
		const Size_t 			j,
		const DimensionArray_t& 	dArr);


	/**
	 * \brief	This function applies the value in the dimensinal array to *this.
	 * 		 The dimensions to which the changes are applied are taken from the dimensional array.
	 *
	 * This function is the same as the other overload except that the dimenison that should be modified
	 * is read from the dimensional array itself.
	 *
	 * \param  dArr The dimensional array.
	 *
	 * \throw	If an inconsisitency is detected. The values that are used are only checked
	 * 		 by an assert if they are valid or not.
	 */
	void
	setDimensionArray(
		const DimensionArray_t& 	dArr)
	{
		if(dArr.isDimensionIllegal() == true)
		{
			throw YGGDRASIL_EXCEPT_InvArg("An invalid array was passed to the function.");
		};

		//All other the checks are done in the implementing functions
		//Reduce the functionality of a known one
		this->setDimensionArray(dArr.getAssociatedDim(), dArr);

		return;
	}; //End: setDimensionArray


	/**
	 * \brief	This function sets the value of component j to the value in the dimensinal array.
	 *
	 * This function allows to a single component of all samples at once.
	 * The values the components should be set to is taken from the dimensional array.
	 * However the array must be valid.
	 * This time the array is that is loaded is an Eigen type.
	 * This function takes a normal Eigen::VectorXd type.
	 *
	 * \param  j 	The dimension that should be manipoulated.
	 * \param  dArr The dimensional array, dimension of it is ignored.
	 *
	 * \throw	If an inconsisitency is detected. The values that are used are only checked
	 * 		 by an assert if they are valid or not.
	 */
	void
	setDimensionArray(
		const Size_t 						j,
		const yggdrasil_eigenRef_t<const ::Eigen::VectorXd>& 	dArr);


	/**
	 * \brief	This function sets the value of component j to the value in the dimensinal array.
	 *
	 * This function allows to a single component of all samples at once.
	 * The values the components should be set to is taken from the dimensional array.
	 * However the array must be valid.
	 * This time the array is that is loaded is an Eigen type.
	 * This function takes an Eigen type that is suitable to be used in conjunction with NumPy.
	 * It is templated as instructed in the pybind documentation.
	 *
	 * \param  j 	The dimension that should be manipoulated.
	 * \param  dArr The dimensional array, dimension of it is ignored.
	 *
	 * \throw	If an inconsisitency is detected. The values that are used are only checked
	 * 		 by an assert if they are valid or not.
	 */
	void
	setDimensionArray(
		const Size_t 						j,
		const yggdrasil_eigenRef_t<const PyVector_t>& 	dArr);


	/**
	 * \brief	This is the function that implements the setting of the dimensional array.
	 *
	 * It is implemented in teh cpp file and is thus only aviable there.
	 *
	 * \param  j	The dimension where we apply the change.
	 * \param  dArr The array that is loaded.
	 *
	 * \tparam  DimArr_t 	The type that we load from.
	 *
	 * \throw 	If inconsistencies are detected.
	 */
private:
	template<class 	DimArr_t>
	void
	internal_setDimensionArray(
		const Size_t 		j,
		const DimArr_t&		dArr);





	/*
	 * ===========================
	 * Iterators
	 *
	 * Here are some forms of iterator functions
	 */
public:
	/**
	 * \brief	This function returns a constant pointer to the beginning of the nth sample.
	 *
	 * \param  n	The samples that we want to access.
	 *
	 * \throw	This function throws if an outof bound access is detected.
	 */
	const Numeric_t*
	beginSample(
		const Size_t 	n)
	 const
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		//Input checks
		if(!(n < m_nSamples))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(n) + ", but there where only " + std::to_string(m_nSamples));
		};
		yggdrasil_assert((n + 1) * m_nDims <= m_samples.size());	//Here it has to be the <= operator, because thjis is the end

		//Construct a smaple
		const Numeric_t* const startRange = m_samples.data() + n * m_nDims;

		return startRange;
	}; //End: beginSample (const)


	/**
	 * \brief	This function returns a constant pointer to the beginning of the nth sample.
	 *
	 * This is the explicit constant version.
	 *
	 * \param  n	The samples that we want to access.
	 *
	 * \throw	This function throws if an outof bound access is detected.
	 */
	const Numeric_t*
	cbeginSample(
		const Size_t 	n)
	 const
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		//Input checks
		if(!(n < m_nSamples))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(n) + ", but there where only " + std::to_string(m_nSamples));
		};
		yggdrasil_assert((n + 1) * m_nDims <= m_samples.size());	//Here it has to be the <= operator, because thjis is the end

		//Construct a smaple
		const Numeric_t* const startRange = m_samples.data() + n * m_nDims;

		return startRange;
	}; //End: cbeginSample


	/**
	 * \brief	This function returns a pointer to the beginning of the nth sample.
	 *
	 * \param  n	The samples that we want to access.
	 *
	 * \throw	This function throws if an outof bound access is detected.
	 */
	Numeric_t*
	beginSample(
		const Size_t 	n)
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		//Input checks
		if(!(n < m_nSamples))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(n) + ", but there where only " + std::to_string(m_nSamples));
		};
		yggdrasil_assert((n + 1) * m_nDims <= m_samples.size());	//Here it has to be the <= operator, because thjis is the end

		//Construct a smaple
		Numeric_t* const startRange = m_samples.data() + n * m_nDims;

		return startRange;
	}; //End: beginSample (non const)


	/**
	 * \brief	Thsi function returens a constant pointer to the past
	 * 		 the end range of the nth sample.
	 *
	 * This fucntion is basically the same as beginSample(n + 1),
	 * but with an updated bound checking.
	 */
	const Numeric_t*
	endSample(
		const Size_t 	n)
	 const
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		//Input checks
		if(!(n < m_nSamples))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(n) + ", but there where only " + std::to_string(m_nSamples));
		};
		yggdrasil_assert((n + 1) * m_nDims <= m_samples.size());	//Here it has to be the <= operator, because thjis is the end


		//Construct a smaple
		const Numeric_t* const endRange   = m_samples.data() + (n + 1) * m_nDims;

		return endRange;
	}; //End: endSample


	/**
	 * \brief	Thsi function constant returens a pointer to the past
	 * 		 the end range of the nth sample.
	 *
	 * This fucntion is basically the same as beginSample(n + 1),
	 * but with an updated bound checking.
	 * This is the explicit constant function.
	 */
	const Numeric_t*
	cendSample(
		const Size_t 	n)
	 const
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		//Input checks
		if(!(n < m_nSamples))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(n) + ", but there where only " + std::to_string(m_nSamples));
		};
		yggdrasil_assert((n + 1) * m_nDims <= m_samples.size());	//Here it has to be the <= operator, because thjis is the end


		//Construct a smaple
		const Numeric_t* const endRange   = m_samples.data() + (n + 1) * m_nDims;

		return endRange;
	}; //End: endSample


	/**
	 * \brief	Thsi function returens a pointer to the past
	 * 		 the end range of the nth sample.
	 *
	 * This fucntion is basically the same as beginSample(n + 1),
	 * but with an updated bound checking.
	 */
	Numeric_t*
	endSample(
		const Size_t 	n)
	{
		//Consistency checks
		yggdrasil_assert(m_nDims != 0);
		yggdrasil_assert(m_samples.size() / m_nDims == m_nSamples);
		yggdrasil_assert(m_samples.size() % m_nDims == 0);

		//Input checks
		if(!(n < m_nSamples))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Performed an out of bound access. Tried to access the element " + std::to_string(n) + ", but there where only " + std::to_string(m_nSamples));
		};
		yggdrasil_assert((n + 1) * m_nDims <= m_samples.size());	//Here it has to be the <= operator, because thjis is the end


		//Construct a smaple
		Numeric_t* const endRange   = m_samples.data() + (n + 1) * m_nDims;

		return endRange;
	}; //End: endSample



	/*
	 * ====================
	 * Matrix Access
	 *
	 * These functions are requiered by the thrid extension of the UCI.
	 * They allow to return the container as Eigen types.
	 */
public:
	/**
	 * \brief	This function transforms *this into a row major
	 * 		 Eigen matrix and returns the result.
	 *
	 * The output matrix depends on the view parameter.
	 * In case view is Sample, the number of rows is equal nSamples()
	 * and the number of columns is equal nDims().
	 * In the case of Dimension, the meaning in the other way around.
	 *
	 * This fucntion is most efficient is view is set to Sample.
	 *
	 * \param  view		How the matrix should be written.
	 */
	RowMatrix_t
	getRowMatrix(
		const eMatrixViewRow 	view)
	 const;


	/**
	 * \brief	This function transforms *this into a column
	 * 		 major Eigen matrix and rteturns it.
	 *
	 * The output matrix depends on the view parameter.
	 * In case view is Sample, the number of rows is equal nSamples()
	 * and the number of columns is equal nDims().
	 * In the case of Dimension, the meaning in the other way around.
	 *
	 * This fucntion is most efficient is view is set to Dimension.
	 *
	 * \param  view		How the matrix should be written.
	 */
	ColMatrix_t
	getColMatrix(
		const eMatrixViewRow 	view)
	 const;



	/*
	 * ================
	 * Storage modifier
	 *
	 * This functions are able to change the internal storage.
	 */
public:
	/**
	 * \brief	This function allows to set the size of the array.
	 *
	 * This basically calls resize on the underling vector.
	 * It is only possible to change the size of *this is the
	 * number of samples is zero.
	 *
	 * Note that it is not possible to change the number of dimensions.
	 *
	 * \param  newNSamples		The new number of samples.
	 *
	 * \throw	If nSamples is not zero.
	 */
	void
	resize(
		const Size_t 	newNSamples)
	{
		//Test if we are allowed to do this
		if(m_nSamples != 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The sampels count was allready set and thus can not be changed.");
		};

		yggdrasil_assert(m_samples.empty() == true);
		yggdrasil_assert(m_nDims != 0);

		const bool wasZero = this->internal_resize(newNSamples);
		yggdrasil_assert(wasZero == true);
		yggdrasil_assert(m_nSamples == newNSamples);
		yggdrasil_assert((m_nSamples * m_nDims) == m_samples.size());


		return;
	}; //End: resize


	/**
	 * \brief	This function performs a reserve operation.
	 *
	 * This is preallocating memeory but not initializing it.
	 * The capacity of the container is set to newCapacity.
	 * This function has only an effect if the size, number of samples,
	 * is zero.
	 *
	 * Note the size of *this does not change.
	 *
	 * \param  newCapacity	The new capacity.
	 *
	 * \throw  	If an internal function throws.
	 */
	void
	reserve(
		const Size_t newCapacity)
	{
		yggdrasil_assert(m_nDims > 0);

		//Test if samples are already in the container
		if(m_samples.size() != 0)
		{
			return;
		}; //End if: samples where already present

		if(newCapacity <= 0)
		{
			return;
		};

		/*
		 * Now we can perform a reserving
		 */
		m_samples.reserve(m_nDims * newCapacity);
		yggdrasil_assert(m_samples.size() == (m_nDims == m_nSamples));

		return;
	}; //ENd reserve


	/**
	 * \brief	This is the clear function that deallocates the memeory.
	 *
	 * This function sets the number of samples to zero.
	 * The memeory is cleared by the swap trick.
	 *
	 * The number of dimension is not changed.
	 */
	void
	clear()
	{
		yggdrasil_assert(m_nDims != 0);

		//Reset the count
		m_nSamples = 0;

		//Clear the memory
		Vector_t().swap(m_samples);

		return;
	}; //End: clear


	/**
	 * \brief	This function allows to resize the array, even after the
	 * 		 first allocation.
	 *
	 * This function is more for internal usage.
	 *
	 * \param  newSize 	The new site of *this.
	 *
	 * \return 	True if the the old size was zero.
	 */
	bool
	internal_resize(
		const Size_t newSize)
	{
		if(m_nDims == 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dimension of the array is zero.");
		};

		const bool retVal = (m_nSamples == 0) ? true : false;

		//Lets resize *This
		m_samples.resize(m_nDims * newSize);
		m_nSamples = newSize;

		return retVal;
	}; //End internal resize


	/*
	 * ====================
	 * Private memebers
	 */
private:
	Vector_t 	m_samples;		//!< This vector contains the data
	Size_t 		m_nDims;		//!< This is the dimension of samples that are allocated.
	Size_t 		m_nSamples;		//!< This is the number of samples that are stored.
}; //End class: yggdrasil_arraySample_t


/**
 * \brief	This is an alias of the yggdrasil_arraySample_t.
 * \using 	yggdrasil_sampleArray_t
 *
 * This using is provided to allow programs to uniformy access the
 * containers. it is arther unlogical that one is classed arraySample
 * and the other to use the sample before the name of the container.
 */
using yggdrasil_sampleArray_t = yggdrasil_arraySample_t;


YGGDRASIL_NS_END(yggdrasil)


