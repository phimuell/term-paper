#pragma once
/**
 * \brief	This file defines the enum that is used for the matrix interopretation
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>


YGGDRASIL_NS_START(yggdrasil)


/**
 * \brief	This enum encodes the interpretation of the matrix.
 * \enum 	eMatrixViewRow
 *
 * This enum encodes how a row of a matrix should be interpreted.
 * It has two values.
 *
 * The first one is 'Sample', this means a single row is interperted
 * as a sample, thus the numbers of columns is the dimensionality
 * of the sample and the number of rows is the number of samples.
 *
 * The second is 'Dimension'.
 * This means that a row is interpreted as dimension.
 * Thus one sample is one column.
 * This means that the number of columns is the number of samples
 * and the number of rows is the dimnensionality of the sample.
 *
 */
enum class eMatrixViewRow
{
	Sample,			//!< A row is interpreted as sample
	Dimension,		//!< A row is interpreted as dimension, thus a colum is seen as sample
}; //End enum(eMatrixViewRow)


/**
 * \brief	This fucntion tests if the enum is the sample value.
 *
 * Note that this function interpretes Dimension as not equal to sample.
 * In debug mode an assert is used.
 *
 * \param  view 	This is teh enum that should be examined.
 */
inline
bool
ygInternal_isSampleView(
	const eMatrixViewRow&		view)
{
	if(view == eMatrixViewRow::Sample)
	{
		return true;
	};

	yggdrasil_assert(view == eMatrixViewRow::Dimension);
	return false;
};


/**
 * \brief	THis fucntion returns the dimension based on
 *  		 the view parameter that is passed.
 *
 * Thsi fucntion uses the convention, that is outlied above.
 * Meaning in the case vew set to Sample, the number of columns
 * is returned. In the case of Dimension the number of rows is
 * returned, note that this function uses ygINternal_isSampleView()
 * to make its descision.
 *
 * \param  matrix	The matrix to query.
 * \param  view 	The view to use.
 *
 * \tparam  MatrixType 		Any matrix type that is used.
 */
template<
	class 	MatrixType
>
inline
Size_t
ygInternal_nDimsFromMatrix(
	const MatrixType& 	matrix,
	const eMatrixViewRow&	view)
{
	if(ygInternal_isSampleView(view) == true)
	{
		return matrix.cols();
	};

	return matrix.rows();
}; //End: estimate dimension


/**
 * \brief	This function is used to get the number of samples
 * 		 That are stored inside a matrix under view.
 *
 * In the case the view is Sample the number of rows is returned.
 * In the case of Dimension the number of columns is returned.
 * The function uses ygInternal_isSampleView() to dtermine which view is used.
 *
 * \param  matrix	The matrix to query.
 * \param  view 	The view to use.
 *
 * \tparam  MatrixType 		Any matrix type that is used.
 */
template<
	class 	MatrixType
>
inline
Size_t
ygInternal_nSamplesFromMatrix(
	const MatrixType& 	matrix,
	const eMatrixViewRow& 	view)
{
	if(ygInternal_isSampleView(view) == true)
	{
		return matrix.rows();
	};

	return matrix.cols();
}; //ENd: ygInternal get the number of samples from view



YGGDRASIL_NS_END(yggdrasil)






