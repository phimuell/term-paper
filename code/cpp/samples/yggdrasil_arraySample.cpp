/**
 * \brief	This file implements a representation a
 * 		 space efficient representation of the samples.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <core/yggdrasil_eigen.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_arraySample.hpp>
#include <samples/yggdrasil_container_util.txx>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_matrixOrder.hpp>

//Including std
#include <vector>


YGGDRASIL_NS_START(yggdrasil)


yggdrasil_arraySample_t::yggdrasil_arraySample_t(
	const yggdrasil_eigenRef_t<const ColMatrix_t>& 		mat,
	const eMatrixViewRow& 					view)
 :
  yggdrasil_arraySample_t(ygInternal_nDimsFromMatrix(mat, view))
{
	//Call the internal helper code
	ygInternal_loadMatrixIntoContainer<yggdrasil_arraySample_t, ColMatrix_t>(this, view, mat);
}; //ENd: construct from row mayjor


yggdrasil_arraySample_t::yggdrasil_arraySample_t(
	const yggdrasil_eigenRef_t<const RowMatrix_t>& 		matrix,
	const eMatrixViewRow& 					view)
 :
  yggdrasil_arraySample_t(ygInternal_nDimsFromMatrix(matrix, view))
{
	//Call the internal helper code
	ygInternal_loadMatrixIntoContainer<yggdrasil_arraySample_t, RowMatrix_t>(this, view, matrix);
}; //End: load from row major matrix

yggdrasil_arraySample_t::yggdrasil_arraySample_t(
	const yggdrasil_sampleList_t& 		sList)
 :
  yggdrasil_arraySample_t(sList.nDims()) 	//Construct an empty array, with set dimension but zero sample
{						//  Performs all the checks
	//Load parameter
	const Size_t D = this->nDims();		//Number of dimensions (for checking)
	const Size_t N = sList.nSamples();	//Number of samples to copy

	//Set the size of the container
	this->resize(N);
	yggdrasil_assert(this->nSamples() == N);

	//Iterating trough the samples and load them
	//we exploit the fact that we have references
	for(Size_t i = 0; i != N; ++i)
	{
		//load the sample
		const Sample_t& s_i = sList[i];

		//Because of the stzructure, we must ensure that the dimension is correct
		yggdrasil_assert((Size_t)s_i.nDims() == D);

		//Load the sampel into the container
		this->loadSample(s_i.data(), i);
	}; //End for(i): filling the array
}; //End: load from list

yggdrasil_arraySample_t::yggdrasil_arraySample_t(
	const yggdrasil_sampleCollection_t& 	sCol)
 :
  yggdrasil_sampleArray_t(sCol.nDims())		//Performs an initialization of the container
{
	//Load the parameter
	const Size_t D = sCol.nDims();
	const Size_t N = sCol.nSamples();

	//Preallocate space.
	this->reserve(N);
	yggdrasil_assert(this->nSamples() == 0);

	//We load the sample completly from the collection and will then
	//use the add new sample feature. There may be a more efficient way, but I do not
	//see it now
	for(Size_t i = 0; i != N; ++i)
	{
		//Load the sample
		const Sample_t s_i = sCol.getSample(i);

		//Add the sample to the container
		this->addNewSample(s_i);

		yggdrasil_assert(this->nSamples() == i);
	}; //End for(i):


	//We do not need it
	(void)D;
}; //End: load from collection




yggdrasil_arraySample_t::RowMatrix_t
yggdrasil_arraySample_t::getRowMatrix(
	const eMatrixViewRow 	view)
 const
{
	//Call the helper code
	RowMatrix_t mat;
	ygInternal_loadContainerIntoMatrix<yggdrasil_arraySample_t, RowMatrix_t>(*this, mat, view);

	return mat;
}; //End: getRowMatrix


yggdrasil_arraySample_t::ColMatrix_t
yggdrasil_arraySample_t::getColMatrix(
	const eMatrixViewRow 	view)
 const
{
	//Call the internal helper code
	ColMatrix_t mat;
	ygInternal_loadContainerIntoMatrix<yggdrasil_arraySample_t, ColMatrix_t>(*this, mat, view);

	return mat;
}; //End: getColMajor



void
yggdrasil_arraySample_t::setDimensionArray(
	const Size_t 			j,
	const DimensionArray_t& 	dArr)
{
	if(dArr.isDimensionIllegal() == true)	//Check if the array is valid
	{
		throw YGGDRASIL_EXCEPT_InvArg("The passed dimensional array has an illegal dimension.");
	};

	//Use the implementing fucntion; there the other checks are performed.
	internal_setDimensionArray(j, dArr);

	return;
}; //End: setDimensionalArray

void
yggdrasil_arraySample_t::setDimensionArray(
	const Size_t 						j,
	const yggdrasil_eigenRef_t<const ::Eigen::VectorXd>& 	dArr)
{
	//Call the internal code
	internal_setDimensionArray(j, dArr);

	return;
}; //End: setDimensionalArray (Eigen)


void
yggdrasil_arraySample_t::setDimensionArray(
	const Size_t 						j,
	const yggdrasil_eigenRef_t<const PyVector_t>& 	dArr)
{
	//Call the internal code
	internal_setDimensionArray(j, dArr);

	return;
}; //End: setDimensionalArray (Eigen)

template<class 	DimArr_t>
void
yggdrasil_arraySample_t::internal_setDimensionArray(
	const Size_t 		j,
	const DimArr_t& 	dArr)
{
	//Make some tests
	if(m_nDims <= j)
	{
		throw YGGDRASIL_EXCEPT_OutOfBound("The dimension " + std::to_string(j) + " does not exists. The container only has dimension " + std::to_string(m_nDims) + ".");
	};

	const Size_t arrNSamples = dArr.size();	//Must use size() for being compatible

	if(arrNSamples != m_nSamples)
	{
		throw YGGDRASIL_EXCEPT_OutOfBound("The dimensional array has " + std::to_string(arrNSamples) + " many samples, but the container only has " + std::to_string(m_nSamples));
	};

	//
	//Setting the values

	//This is an indexing for indexing the internal array
	Size_t idx = j;

	//Iterate over all the samples to change them
	for(Size_t i = 0; i != m_nSamples; ++i)
	{
		//Test if the index is correct
		yggdrasil_assert(idx < m_samples.size());

		//Load the value that we have to write to
		const Numeric_t v = dArr[i];

		//Check if the value is valid
		yggdrasil_assert(isValidFloat(v));

		//Write the value
		m_samples[idx] = v;

		//Increment the index for indexing the internal array
		idx += m_nDims;
	}; //End for(i)

	return;
}; //End: setDimensionalArray











YGGDRASIL_NS_END(yggdrasil)


