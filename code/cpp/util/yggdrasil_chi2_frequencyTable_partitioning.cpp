/**
 * \brief	This file implement the partitioning scheme
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_util.hpp>
#include <util/yggdrasil_chi2_frequencyTable.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sample.hpp>




//Incluide std
#include <memory>
#include <vector>
#include <numeric>
#include <algorithm>

//Include boost
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/normal.hpp>


YGGDRASIL_NS_BEGIN(yggdrasil)



bool
yggdrasil_chi2FreqTable_t::private_buildFreqTable_partion(
	const DimArray_t& 		dimArray1,
	const BinEdgesArray_t& 		binEdges1,
	const DimArray_t& 		dimArray2,
	const BinEdgesArray_t& 		binEdges2)
{
	if(m_freqTable.empty() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The frequency table is allocated, but it should not.");
	};
	if(binEdges1.hasPartition() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The partitioning code was calle, but the first bin, bid not have a partitioning.");
	};
	if(binEdges2.hasPartition() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The partitioning code was calle, but the second bin, bid not have a partitioning.");
	};

	//This is teh number of samples
	const Size_t nSamples = dimArray1.nSamples();

	//Allocateing the table
	m_freqTable.assign(m_nRowsFirst * m_nColsSecond, 0);

	//Set the garand total to zero
	m_grandTotal = 0;

	//this is for the end of the iteration
	yggdrasil_assert(m_nRowsFirst == binEdges1.nBins());
	yggdrasil_assert(m_nColsSecond == binEdges2.nBins());

	//Now we creat references to the two underlying arrays
	const BinEdgesArray_t::ValueArray_t& binEdgeArray1 = binEdges1.getUnderlyingArray();
	const BinEdgesArray_t::ValueArray_t& binEdgeArray2 = binEdges2.getUnderlyingArray();

	//We requiere the array to be sorted
	yggdrasil_assert(::std::is_sorted(binEdgeArray1.cbegin(), binEdgeArray1.cend()));
	yggdrasil_assert(::std::is_sorted(binEdgeArray2.cbegin(), binEdgeArray2.cend()));
	yggdrasil_assert(dimArray1.getAssociatedDim() != dimArray2.getAssociatedDim());


	/*
	 * Now we must go throug the the two partitioning and
	 * count where are the same elements
	 */
	for(Size_t i = 0; i != m_nRowsFirst; ++i)
	{
		//Get the range of the row
		const IdxItRange_t rowRange = binEdges1.getBinRangeIt(i);

		//Test if the range is empty
		if(rowRange.first != rowRange.second)
		{
			//THey are not empty
			yggdrasil_assert(rowRange.first < rowRange.second);

			for(Size_t j = 0; j != m_nColsSecond; ++j)
			{
				//Get the colum row range
				const IdxItRange_t colRange = binEdges2.getBinRangeIt(j);

				//Find the number of equal elements
				const Size_t samplesInThatPart = ygInternal_countSameElements(rowRange, colRange);

				//Add to grand total
				m_grandTotal += samplesInThatPart;

				//Write it into the table
				const ::yggdrasil::Size_t indexToAccess = i * m_nColsSecond + j;
				yggdrasil_assert(indexToAccess < m_freqTable.size());

				yggdrasil_assert(this->getFrequency(i, j) == 0);
				m_freqTable[indexToAccess] = samplesInThatPart;
				yggdrasil_assert(this->getFrequency(i, j) == samplesInThatPart);
			}; //End for(j): second index
		}; //ENd if: row range is empty
	}; //ENd for(i): going through the first index

	yggdrasil_assert(m_grandTotal == nSamples);

	/*
	 * The test is done above
	 */
	return true;
}; //End: building the freq table in standard way


Size_t
ygInternal_countSameElements(
	const yggdrasil_chi2FreqTable_t::IdxItRange_t 	range1,
	const yggdrasil_chi2FreqTable_t::IdxItRange_t 	range2)
{
	yggdrasil_assert(range1.first <= range1.second);
	yggdrasil_assert(range2.first <= range2.second);
	if(range1.first == range1.second)
	{
		return 0;
	}
	if(range2.first == range2.second)
	{
		return 0;
	};

#if 1
	//This is the size of the intersection
	Size_t intersectionSize = 0;

	//These are the heads of the current range
	auto range1Head = range1.first;
	auto range2Head = range2.first;

	yggdrasil_assert(::std::is_sorted(range1.first, range1.second));
	yggdrasil_assert(::std::is_sorted(range2.first, range2.second));

#define isRangeExhausted1 (range1Head == range1.second)
#define isRangeExhausted2 (range2Head == range2.second)

	while (!isRangeExhausted1 && !isRangeExhausted2)
	{
		//Read the indexes
		const SortIndex_t idx1 = *range1Head;
		const SortIndex_t idx2 = *range2Head;

		//Test if the tw are the same, then we have found one
		if(idx1 == idx2)
		{
			//Increment the interrsection size count
			intersectionSize += 1;

			//Move BOTH iterators further
			++range1Head;
			++range2Head;
		}
		else
		{
			/*
			 * They are not equal, so one of them must be smaller
			 * We will now find the start point
			 */
			if(idx1 < idx2)
			{
				/*
				 * Range one is smaller, so we must increase it until it
				 * is not smaller than the second range
				 */

				//First increment
				++range1Head;

				while(isRangeExhausted1 == false && ((*range1Head) < idx2))
				{
					//We are not ar the end of the range, but it is still
					//smaller, so we must increment again
					++range1Head;
				}; //End incrementing
				yggdrasil_assert((range1.second != range1Head) ? (idx2 <= (*range1Head)) : true);
			}
			else
			{
				/*
				 * Range two is smaller, so we will increent that one
				 * until we find the next possible start point
				 */
				//First increment
				++range2Head;

				while(isRangeExhausted2 == false && ((*range2Head) < idx1))
				{
					//We are not ar the end of the range, but it is still
					//smaller, so we must increment again
					++range2Head;
				}; //End incrementing
				yggdrasil_assert((range2.second != range2Head) ? (idx1 <= (*range2Head)) : true);
			};
		}; //End else: not equal
	}; //End while

	//Return the number of the equal elements
	return intersectionSize;

#undef isRangeExhausted1
#undef isRangeExhausted2
#else
	yggdrasil_chi2FreqTable_t::IndexArray_t intersection;
	::std::set_intersection(
		range1.first, range1.second,
		range2.first, range2.second,
		std::back_inserter(intersection)
		);
	return intersection.size();
#endif
}; //ENd: count same elements


YGGDRASIL_NS_END(yggdrasil)



