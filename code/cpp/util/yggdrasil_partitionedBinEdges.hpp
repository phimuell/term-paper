#pragma once
/**
 * \brief	This is a partitioned bin edge array.
 *
 * This file implements a partioned bin edge array.
 * This is like a normal bin edge array, but with
 * the addition that it also maintains the possiblity,
 * to hold a partitioning of the samples it was constructed on.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_generalBinEdges.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>


//Incluide std

//Include boost


YGGDRASIL_NS_BEGIN(yggdrasil)


/**
 * \class	yggdrasil_partionedBinEdges_t
 * \brief	This class also maintain a partitionit was constructed with.
 *
 * Unlike the generall bin edges, this class also maintains
 * a partitioning. This requieres that the class is constructed
 * with a dimensional array as additional argument.
 * It internaly maintains an array that represents an
 * index array. It will also maintain an array that describes the
 * start of each bin inside this array, with the same encoding.
 *
 * This class is intended for the usage in teh frequency table in order
 * to speed up the computration.
 * In order to construct itself it will uses the recursive bining method.
 * Notice that this requieres memeory and a cap should be introduced.
 */
class yggdrasil_partionedBinEdges_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Numeric_t 	= ::yggdrasil::Numeric_t;	//!< This is the underling numeric type.
	using Real_t 		= ::yggdrasil::Real_t;		//!< This is the high precission type.
	using DimArray_t 	= ::yggdrasil::yggdrasil_dimensionArray_t;	//!< Array that stores the samples
	using Size_t 		= ::yggdrasil::Size_t;		//!< This is the size type, it is different form the other usages
	using ValueArray_t      = ::yggdrasil::yggdrasil_valueArray_t<Numeric_t>;	//!< This vector contains the edge boundaries
	using IndexArray_t 	= ::yggdrasil::yggdrasil_valueArray_t<SortIndex_t>;	//!< This is the bindex array
	using BinPartArray_t	= ::yggdrasil::yggdrasil_valueArray_t<Size_t>;		//!< This is for maintinaing the start array
	using IntervalBound_t 	= ::yggdrasil::yggdrasil_intervalBound_t;	//!< This is the type that is used for the interval
	using const_iterator 	= ValueArray_t::const_iterator;	//!< Thsi si the iterator for the individual sizes

	using IndexIterator_t 	= IndexArray_t::const_iterator;	//!< This is the iterator for iterqating through the index array
	using IdxRange_t	= ::std::pair<SortIndex_t, SortIndex_t>;	//!< A pair that describesd the range of a bin
	using IdxItRange_t 	= ::std::pair<IndexIterator_t, IndexIterator_t>;	//Iterator for iterating through the index array

private:
	using iterator 		= ValueArray_t::iterator;	//!< This is the oiterator, but we allow only const.

	/*
	 * =================
	 * Constructors
	 *
	 * All constructers are defaulted.
	 */
public:
	/**
	 * \brief	This is the building constructor that
	 * 		 associates this directly with data.
	 *
	 * The user gives the interval that should be binned.
	 * And then the user must give the \e inner endpoints.
	 * This means if array innerEdges has n values,
	 * then *this will have n + 1 many bins.
	 *
	 * The array of the end points must be sorted in acending order.
	 *
	 * This will not construct an index array.
	 *
	 * \param  fullInterval		This is the full interval.
	 * \param  innerEdges 		This are the inner edges, must be sorted.
	 *
	 * \throw	If this is allready associated or the data is not sorted.
	 * 		 Also if an zero sized bin bould be constructed.
	 */
	yggdrasil_partionedBinEdges_t(
		const IntervalBound_t& 		fullInterval,
		const ValueArray_t& 		innerEdges)
	 :
	  yggdrasil_partionedBinEdges_t()
	{
		//All the tests are done in teh underling function
		this->buildBins(fullInterval, innerEdges);
	}; //End: building with direct buinding


	/**
	 * \brief	Building constructor and default constuctor.
	 *
	 * In this class the building constructor is actually also
	 * the default consztructor.
	 * The reaosn is that as every statistical test this class
	 * has to support lazzy binding, my term.
	 * This means it must be created and then filled with data later.
	 *
	 * \throws 	If underling functions throws.
	 */
	yggdrasil_partionedBinEdges_t()
	 :
	  m_binEdges(),
	  m_idxPartition(),
	  m_idxArray(),
	  m_isTrivial(false)
	{};


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_partionedBinEdges_t(
		const yggdrasil_partionedBinEdges_t&)
	 = default;

	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_partionedBinEdges_t(
		yggdrasil_partionedBinEdges_t&&)
	 = default;

	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_partionedBinEdges_t&
	operator= (
		const yggdrasil_partionedBinEdges_t&)
	 = default;

	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted
	 */
	yggdrasil_partionedBinEdges_t&
	operator= (
		yggdrasil_partionedBinEdges_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_partionedBinEdges_t()
	 = default;

	/**
	 * \brief	It is possible to assign a general bin and to *this.
	 *
	 * However a cleaning will be executed.
	 * If *this is partitoned an exception will be generated.
	 */
	yggdrasil_partionedBinEdges_t&
	operator= (
		const yggdrasil_genBinEdges_t& o)
	{
		if(this->isAssociated() == true)
		{
			if(this->hasPartition() == true)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("You tried to assigne a general bin to a partitioned bin. This is fine, but the parttioned bin is partitioned.");
			};

			//Unbound for proper assignment
			this->unboundData();
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};

		//Now assigne the bin count to this
		this->m_binEdges = o.m_binEdges;

		return *this;
	}; //ENd assignment

	/**
	 * \brief	It is possible to move assign a general bin and to *this.
	 *
	 * However a cleaning will be executed.
	 * If *this is partitoned an exception will be generated.
	 */
	yggdrasil_partionedBinEdges_t&
	operator= (
		yggdrasil_genBinEdges_t&& o)
	{
		if(this->isAssociated() == true)
		{
			if(this->hasPartition() == true)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("You tried to assigne a general bin to a partitioned bin. This is fine, but the parttioned bin is partitioned.");
			};

			//Unbound for proper assignment
			this->unboundData();
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};

		//Now assigne the bin count to this
		this->m_binEdges = std::move(o.m_binEdges);

		return *this;
	}; //ENd assignment



	/*
	 * ================
	 * Utility Functions
	 */
public:
	/**
	 * \brief	Return the number of bins \c *this has.
	 *
	 * \throw 	if \c *this is not associated to any data.
	 */
	Size_t
	nBins()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data has an undefined numbers of bins.");
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};

		assert(m_binEdges.size() >= 2);
		yggdrasil_assert(m_idxArray.empty() == m_idxPartition.empty());
		yggdrasil_assert((m_idxArray.empty() == false) ? (m_binEdges.size() == m_idxPartition.size()) : true);

		return (m_binEdges.size() - 1);
	}; //End: nBins


	/**
	 * \brief	This function returns true if \c *this is associated to a domain.
	 *
	 * \c *thsi is accociated to a data if there are some bin edges.
	 */
	bool
	isAssociated()
	 const
	 noexcept
	{
		yggdrasil_assert(m_idxArray.empty() == m_idxPartition.empty());
		if(this->isTrivial() == true)
		{
			return true;
		};

		yggdrasil_assert((m_idxArray.empty() == false) ? (m_binEdges.size() == m_idxPartition.size()) : true);
		return (m_binEdges.empty() == true) ? false : true;
	}; //End: isAssociated


	/**
	 * \brief	This function constructs a normal bin edges out of this.
	 */
	yggdrasil_genBinEdges_t
	getGeneralEdges()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The array is not associated.");
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};

		yggdrasil_genBinEdges_t genEdgeArr;

		//Copy the array into the edge
		genEdgeArr.m_binEdges.assign(this->m_binEdges.cbegin(), this->m_binEdges.cend());

		return genEdgeArr;
	}; //ENd get generall bin


	/*
	 * =====================
	 * Partition funcitons
	 *
	 * These are functions that gives access to the partition
	 */
public:
	/**
	 * \brief	Return true if *this has a partition.
	 */
	bool
	hasPartition()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Partiton queries are only allowed if *this is assoicted.");
		};
		if(this->isTrivial() == true)
		{
			//A trivial bins never have a partitioning
			return false;
		};

		if(m_idxPartition.size() > 0)
		{
			yggdrasil_assert(m_idxPartition.size() == m_binEdges.size());
			yggdrasil_assert(m_idxArray.empty() == false);
			return true;
		};
		yggdrasil_assert(m_idxArray.empty());
		yggdrasil_assert(m_idxPartition.empty());

		return false;
	}; //End has partitioning:

	/**
	 * \brief	This gives the indices of the samples in the ith bin.
	 *
	 * \param  i	The bin that should be accessed.
	 */
	IdxRange_t
	getBinRange(
		const Size_t 	i)
	 const
	{
		if(this->hasPartition() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("This does not have a partition.");
		};
		yggdrasil_assert(i < this->nBins());
		yggdrasil_assert(m_idxArray.empty() == false);

		return ::std::make_pair(m_idxPartition.at(i), m_idxPartition.at(i + 1));
	}; //ENd get range


	/**
	 * \brief	This fucntion returns iterators of the ith bin.
	 *
	 * \param  i 	This is the bin that we want to access.
	 */
	IdxItRange_t
	getBinRangeIt(
		const Size_t 	i)
	 const
	{
		if(this->hasPartition() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("This does not have a partition.");
		};
		yggdrasil_assert(i < this->nBins());
		yggdrasil_assert(m_idxArray.empty() == false);

		return ::std::make_pair(m_idxArray.cbegin() + m_idxPartition.at(i), m_idxArray.cbegin() + m_idxPartition.at(i + 1));
	}; //ENd get range

	/**
	 * \brief	This function returns the number of registered samples in bin i.
	 *
	 * \param  i 	The bin that should be accessed
	 */
	Size_t
	getBinCount(
		const Size_t 	i)
	 const
	{
		if(this->hasPartition() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("This does not have a partition.");
		};
		yggdrasil_assert(i < this->nBins());
		yggdrasil_assert(m_idxArray.empty() == false);
		yggdrasil_assert(m_idxPartition.at(i) <= m_idxPartition.at(i + 1));

		return Size_t(m_idxPartition.at(i + 1) - m_idxPartition.at(i));
	}; //ENd get bin count



	/*
	 * ===================
	 * Accessing functions
	 */
public:
	/**
	 * \brief	Thid fucntion returns the spanned interval of *this.
	 *
	 * This is basically the interval that was passed to *this during its construction.
	 * It is the interval formed from the very first lower bound and the very last
	 * upper bound.
	 *
	 * \throw	If *this is not associated.
	 */
	IntervalBound_t
	getSpanningInterval()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("*this is not associated.");
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};

		const IntervalBound_t spanInter(m_binEdges.front(), m_binEdges.back());
		yggdrasil_assert(spanInter.isValid() == true);

		return spanInter;
	}; //End: getSpannedInterval


	/**
	 * \brief	This function returns the interval that is spanned by the \c i th bin.
	 *
	 * \param  i 	Which interval should be queried
	 *
	 * \throw	If \c *this is not associated with any data.
	 * 		 i is out of range or if the interval that is constructed is invalid.
	 */
	IntervalBound_t
	getBinInterval(
		const uInt_t 	i)
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data, does not have bins.");
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};
		if((m_binEdges.size() - 1) <= i)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The bin " + std::to_string(i) + " does not exists.");
		};

		//The constructor of the interval throws if it is not valid
		const IntervalBound_t binInterval(m_binEdges.at(i), m_binEdges.at(i + 1));
		yggdrasil_assert(binInterval.isValid() == true);

		return binInterval;
	}; //End: getBinInterval


	/**
	 * \brief	This function returns the lower bound of the ith bin.
	 *
	 * This function is more efficient than callint the above and
	 * then use the lowerbound on the returned.
	 *
	 * However no interval is constructed, so no safty can be guaranteed.
	 * However this is most likely accaptable.
	 */
	Numeric_t
	getLowerBound(
		const uInt_t 	i)
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data, does not have bins.");
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};
		if((m_binEdges.size() - 1) <= i)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The bin " + std::to_string(i) + " does not exists.");
		};

		return m_binEdges[i];
	}; //End: getLowerBound


	/**
	 * \brief	This function returns the upper bound of the ith bin.
	 *
	 * This function is more efficient than callint the above and
	 * then use the upperBound fucntion on the returned.
	 *
	 * However no interval is constructed, so no safty can be guaranteed.
	 * However this is most likely accaptable.
	 */
	Numeric_t
	getUpperBound(
		const uInt_t 	i)
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data, does not have bins.");
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};
		if((m_binEdges.size() - 1) <= i)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The bin " + std::to_string(i) + " does not exists.");
		};

		return m_binEdges.at(i + 1);
	}; //End: getUpperBound


	/**
	 * \brief	Returns the iterator to the beiging of the underling array.
	 *
	 * This is the lower bound of the first interval.
	 *
	 * One can see the iterators allow the accessing of all lower bounds.
	 * consecutively.
	 * This holds except for the last one (end() -1 ), which is the upper bound
	 * of the last bin.
	 */
	const_iterator
	begin()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data, does not have bins.");
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};

		return m_binEdges.cbegin();
	};


	/**
	 * \brief	Returns the past the end iterator of the underling array.
	 */
	const_iterator
	end()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data, does not have bins.");
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};

		return m_binEdges.cend();
	};



	/**
	 * \brief	This fucntion returns the beginning of the innerEdges range.
	 *
	 * The inner edges range the bin boundary that are in the inside of the
	 * spanned interval. It is bassically the same as the argument that was
	 * used to build the bins.
	 *
	 * This fuction is technically equivalent with begin() + 1.
	 *
	 * In the case of just one bin, the end iiterator, which is this->end()
	 * is returned.
	 */
	const_iterator
	innerEdgesBegin()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("This is not associated.");
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};

		//Test if we have only one bin
		if(m_binEdges.size() == 2)
		{
			yggdrasil_assert(this->nBins() == 1);

			//In the case of jsut one bin, we have the empty range
			return m_binEdges.cend();
		}; //End only one bin

		//THe normal case
		const const_iterator beginInner = m_binEdges.cbegin() + 1;
		return beginInner;
	}; //End: innerEdgesBegin


	/**
	 * \brief	This function returns the past the end iterator of the inner range.
	 *
	 * This are all the internal bin boundaries that are not the boundaries given the the
	 * spanned interval.
	 *
	 * This is basically end() - 1, but with a correction for the case that only one bin
	 * is present. In that case this->end() is returned.
	 */
	const_iterator
	innerEdgesEnd()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("This is not associated.");
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};

		//Test if we have only one bin
		if(m_binEdges.size() == 2)
		{
			yggdrasil_assert(this->nBins() == 1);

			//In the case of jsut one bin, we have the empty range
			return m_binEdges.cend();
		}; //End only one bin

		//THe normal case
		const const_iterator endInner = m_binEdges.cend() - 1;
		return endInner;
	}; //End: innerEdgesBegin



	/**
	 * \brief	This fucntion returns a constant reference to the bin edges of \c *this.
	 *
	 * The format of the edges is a bit strange.
	 * First of all also this class adheres to the Yggdrasil definition, that an interval is
	 * a right open mathematical interval.
	 * This menans that the upper bound does not belong to the actual interval.
	 * Assume \c *this has n bins, then the returned array is of legth n + 1.
	 * The first n entried in the arrays are the lower bounds of the bins.
	 * To be precise the value that is stored at index i is the lower bound
	 * of the ith interval, the value stored at interval i + 1 is upper bound
	 * of the ith bin, and at the same time, the lower bound of the i + 1 bin.
	 * This means that last entry in the array is the upper bound of the last bin.
	 */
	const ValueArray_t&
	getUnderlyingArray()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not acccess an table that is not associated.");
		};
		if(this->isTrivial() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Can not access a trivial bin edge.");
		};

		return m_binEdges;
	}; //End: getBinEdges



	/*
	 * ================
	 * Managing functions
	 *
	 * These functions allows to manage *this.
	 * Like adding samples or so.
	 */
public:

	/**
	 * \brief	This function associated \c *thsi with data.
	 *
	 * The user gives the interval that should be binned.
	 * And then the user must give the \e inner endpoints.
	 * This means if array innerEdges has n values,
	 * then *this will have n + 1 many bins.
	 *
	 * The array of the end points must be sorted in acending order.
	 *
	 * This function does not set up the index structure.
	 *
	 * \param  fullInterval		This is the full interval.
	 * \param  innerEdges 		This are the inner edges, must be sorted.
	 *
	 * \throw	If this is allready associated or the data is not sorted.
	 * 		 Also if an zero sized bin bould be constructed.
	 */
	void
	buildBins(
		const IntervalBound_t& 		fullInterval,
		const ValueArray_t& 		innerEdges);


	/**
	 * \brief	This function bounds *this to the data.
	 *
	 * In contrast to the other function this function will
	 * generate the index structure.
	 *
	 * \param  fullInterval		This is the full interval.
	 * \param  innerEdges 		This are the inner edges, must be sorted.
	 *
	 * \throw	If this is allready associated or the data is not sorted.
	 * 		 Also if an zero sized bin bould be constructed.
	 */
	void
	buildBins(
		const IntervalBound_t& 		fullInterval,
		const ValueArray_t& 		innerEdges,
		const DimArray_t&		dimArray);

	/**
	 * \brief	This function allows to add a partition to an associated bin.
	 */
	void
	addPartition(
		const DimArray_t&		dimArray);


	/**
	 * \brief	This function makes *this trivial.
	 *
	 * A trivial bin edge array is associated, but it is not possible to
	 * do something with it, except unbinding it.
	 *
	 * \throw	If *this is allready associated.
	 */
	void
	trivialBind();


	/**
	 * \brief	This function returns a copy of \c *this, but not associated to any data.
	 *
	 * This function calls the building constructor.
	 * The parameters are the same as were used to construct \c *this.
	 *
	 * Thsi function works even if *this is not associated.
	 *
	 * This function is a bit old and should be removed.
	 * It is kept for compability.
	 */
	yggdrasil_partionedBinEdges_t
	getUnassociatedCopy()
	 const
	{
		return yggdrasil_partionedBinEdges_t();
	};


	/**
	 * \brief	This function undounds *this.
	 *
	 * It basically deletes the internal array.
	 * Notice that this fucntion is manyly for internal purpose.
	 *
	 * \throw	If *this is not associated.
	 */
	void
	unboundData();


	/**
	 * \brief	This function returns true if *this is considered trivial.
	 *
	 * A trivial binedge array doid not have bins or a partition.
	 * It is basically empty
	 */
	bool
	isTrivial()
	 const
	{
		yggdrasil_assert(m_idxArray.empty() == m_idxPartition.empty());
		if(m_isTrivial == true)
		{
			yggdrasil_assert(m_binEdges.empty());
			yggdrasil_assert(m_idxArray.empty());
			yggdrasil_assert(m_idxPartition.empty());

			return true;
		}; //End if: *this is trivial

		return false;
	}; //End: if ifTrivial


	/*
	 * =================
	 * Private Members
	 */
private:
	ValueArray_t 		m_binEdges;	//!< This vector stopres the bin edges, notice that there
						//!<  is one more edges than there are bins.
						//!<  Value i is the lower bound of the ith bin, hence i+1 is the upper bound of bin i.
	BinPartArray_t 		m_idxPartition;	//!< This array describes the partitioning of the index array.
						//!<  The encoding is the same as the index array.
	IndexArray_t 		m_idxArray;	//!< This is the index array that describes the ordering of the dimenionsla array.
	bool 			m_isTrivial;	//!< This bool indicate if *this is trivial, this means that it must not be tested
						//!<  Also that zero bins are okay.


}; //End class: yggdrasil_partionedBinEdges_t





YGGDRASIL_NS_END(yggdrasil)
