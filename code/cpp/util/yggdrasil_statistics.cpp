/*
 * This files implements the statistic class
 */

//Include PGL
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_assert.hpp>

#include <util/yggdrasil_statistics.hpp>


//INclude std
#include <limits>
#include <cmath>
#include <utility>
#include <type_traits>


YGGDRASIL_NS_BEGIN(yggdrasil)



yggdrasil_statistic_t::yggdrasil_statistic_t() noexcept :
	m_mu(0.0),
	m_M2(0.0),
	m_n(0)
{
};
//End constructor


void
yggdrasil_statistic_t::swap(
	yggdrasil_statistic_t& other)
{
	std::swap(this->m_mu, other.m_mu);
	std::swap(this->m_n, other.m_n);
	std::swap(this->m_M2, other.m_M2);
	std::swap(this->m_max, other.m_max);
	std::swap(this->m_min, other.m_min);

	return;
};


Numeric_t
yggdrasil_statistic_t::get_Min() const
{
	//yggdrasil_assert(m_n > 0);
	return m_min;
};


Numeric_t
yggdrasil_statistic_t::get_Max() const
{
	//yggdrasil_assert(m_n > 0);
	return m_max;
};



yggdrasil_statistic_t&
yggdrasil_statistic_t::reset()
{
	m_mu = 0.0;
	m_n = 0;
	m_M2 = 0.0;

	return *this;
};

Numeric_t
yggdrasil_statistic_t::get_Mean() const
{
	return this->m_mu;
};


Numeric_t
yggdrasil_statistic_t::get_eVariance() const
{
	if(m_n == 1 || m_n == 0)
	{
		// In the case of one sample, we can't do anything, so we return -1
		return Numeric_t(-1);
	}
	else
	{
		yggdrasil_assert(m_n >= 2);
		return (this->m_M2 / ((Numeric_t)(m_n -1)));
	};
};


Numeric_t
yggdrasil_statistic_t::get_Variance() const
{
	if(m_n == 1 || m_n == 0)
	{
		return Numeric_t(-1);
	}
	else
	{
		yggdrasil_assert(m_n >= 2);
		return (this->m_M2 / (Numeric_t)m_n);
	};
};

Size_t
yggdrasil_statistic_t::get_nRecords() const
{
	return m_n;
};

Numeric_t
yggdrasil_statistic_t::get_StdDeviation() const
{
	if(m_n == 1 || m_n == 0)
	{
		return Numeric_t(-1);
	}
	else
	{
		yggdrasil_assert(m_n >= 2);
		return std::sqrt(this->get_Variance() );
	};
};




YGGDRASIL_NS_END(yggdrasil)

