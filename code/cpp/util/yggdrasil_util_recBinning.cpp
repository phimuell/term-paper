/**
 *
 * \brief	This file implements the recursive binning.
 *
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>

#include <core/yggdrasil_indexArray.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <util/yggdrasil_util.hpp>

//Including std
#include <vector>
#include <algorithm>
#include <iterator>
#include <utility>
#include <limits>


YGGDRASIL_NS_START(yggdrasil)


/**
 * \brief	This si the internal function that handles the recurion
 * 		 determining of the bins.
 *
 * It will divide the domain and descend into the resulting sub domain
 * until it encounters a base case, that is solved directly.
 *
 * THis function handles a range of bin, as with almost all ranges
 * in C++ and in Yggdrasil, we use a past the end scheme for that.
 * The index range will be partitioned.
 *
 * This function modifies the array that is passed to it.
 * It also will apply a saturating scheme.
 *
 * \param  dinArray		The dimensional array.
 * \param  binArray 		The bin array that is used.
 * \param  idxGlob_begin	The index to the beginning of the global index array.
 * \param  idxRange_begin	The start of the current index range to process.
 * \param  idxRange_end		The PAST THE END iterator of the current index range.
 * \param  idxBin_begin		The first bin that we must handle.
 * \param  idxBin_end		The past the end bin, that we must handle.
 * \param  binIdxArray		The array that contains the beginnings.
 */
void
ygInternal_recArgBinning(
	const yggdrasil_dimensionArray_t&			dimArray,
	const yggdrasil_genBinEdges_t&				binArray,
	const yggdrasil_valueArray_t<SortIndex_t>::iterator 	idxGlob_begin,
	const yggdrasil_valueArray_t<SortIndex_t>::iterator 	idxRange_begin,
	const yggdrasil_valueArray_t<SortIndex_t>::iterator 	idxRange_end,
	const Size_t 						idxBin_begin,
	const Size_t 						idxBin_end,
	yggdrasil_valueArray_t<Size_t>*				binIdxArray);






yggdrasil_valueArray_t<Size_t>
yggdrasil_recArgBinning(
	const yggdrasil_dimensionArray_t&			dimArray,
	const yggdrasil_genBinEdges_t&				binArray,
	const yggdrasil_valueArray_t<SortIndex_t>::iterator 	idxArray_begin,
	const yggdrasil_valueArray_t<SortIndex_t>::iterator 	idxArray_end,
	const bool 						sortBins)
{
	//Test if the dim array is valud
	if(dimArray.isDimensionIllegal() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimensional array has an invalid dimenbsion.");
	};
	if(binArray.isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The bin array is not associated.");
	};

	//GEt the number of samples, we must use the index array for that.
	const Size_t nSamples = ::std::distance(idxArray_begin, idxArray_end);

	if(dimArray.nSamples() < nSamples)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The ndex array is longer than we have samples.");
	};


	//Get the number of bins that awhere to be processed
	const Size_t nBins = binArray.nBins();

	//Make the test if it is possible to perfom the computation
	const ::yggdrasil::Size_t maxSupportedSample = ::std::numeric_limits<SortIndex_t>::max();
	if(maxSupportedSample <= nSamples)	//It is importat that it is a <= comparison
	{
		/*
		 * NOTICE:
		 *
		 * If you run into this problem, and thinks you have the memeory (money) to allow for
		 * that many samples then you can remove this restriction by changing the line
		 * 	using SortIndex_t = ::yggdrasil::uInt32_t;
		 * to
		 * 	using SortIndex_t = ::yggdrasil::Size_t;
		 * In the core/yggdrasil_int.hpp file.
		 *
		 * Also be warned, that thsi will increase the memeory needs.
		 *
		 * There is an old saying.
		 * "X is to memory what Ronald Reagan is to money"
		 * This saying is also applicable to this.
		 */
		throw YGGDRASIL_EXCEPT_RUNTIME("More than supported samples. There where " + std::to_string(nSamples) + " but only " + std::to_string(maxSupportedSample) + " are supported.");
	}; //End problem with handling

#ifndef NDEBUG
	const Size_t nSamplesInDimArray = dimArray.nSamples();
	for(auto it = idxArray_begin; it != idxArray_end; ++it)
	{
		//We cave to campare against the total number of samples
		//inside the dinm array. It will never be equal, since this is the pass the end array.
		yggdrasil_assert(*it < nSamplesInDimArray);
	}; //ENd for(it)
#endif

	/*
	 * Now start the descending
	 * We will implement the recursion in an internal fucntion.
	 * This function is just the face to the world.
	 */

	//This is the index array that is needed
	yggdrasil_valueArray_t<Size_t> binIdxArray(nBins + 1, 0);

	/*
	 * We must only do something
	 * if we have sampels in the first palace.
	 * In the case of no samples the initialization of the
	 * return array is already the solution.
	 */
	if(nSamples != 0)
	{
		//We can write the solution directly for the first and the last bin
		binIdxArray.front() = 0;
		binIdxArray.back()  = nSamples;

		//Now call the recursive function
		ygInternal_recArgBinning(
			dimArray,
			binArray,
			idxArray_begin,		//At this level the global start is the start of the index array
			idxArray_begin,
			idxArray_end,
			0,			//We must start at the first one, which is idx 0
			nBins,			//This is the past the end index
			&binIdxArray);		//THis is the solution we generate

		yggdrasil_assert(binIdxArray.at(0) == 0);
		yggdrasil_assert(binIdxArray.at(binIdxArray.size() - 1) == nSamples);
		yggdrasil_assert(binIdxArray.size() == (nBins + 1));
#ifndef NDEBUG
		for(Size_t i = 1; i != binIdxArray.size(); ++i)
		{
			yggdrasil_assert(binIdxArray.at(i) <= binIdxArray.at(i));
		};
#endif


		//TEst if we have to sort them.
		if(sortBins == true)
		{
			for(Size_t bin = 0; bin != nBins; ++bin)
			{
				//Get the iterator that points at the beginning of the ranges
				auto binStart = idxArray_begin + binIdxArray.at(bin + 0);
				auto binEnd   = idxArray_begin + binIdxArray.at(bin + 1);

				//Now sort them
				::std::sort(binStart, binEnd);
			}; //End for(bin)
		}; //end if: sorting the bins
	}; //ENd if: no samples



	//Now returning the array
	return binIdxArray;
}; //End rec binning



void
ygInternal_recArgBinning(
	const yggdrasil_dimensionArray_t&			dimArray,
	const yggdrasil_genBinEdges_t&				binArray,
	const yggdrasil_valueArray_t<SortIndex_t>::iterator 	idxGlob_begin,
	const yggdrasil_valueArray_t<SortIndex_t>::iterator 	idxRange_begin,
	const yggdrasil_valueArray_t<SortIndex_t>::iterator 	idxRange_end,
	const Size_t 						idxBin_begin,
	const Size_t 						idxBin_end,
	yggdrasil_valueArray_t<Size_t>*				binIdxArray)
{
	if(!(idxBin_begin < idxBin_end))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The bin range was empyt.");
	};
	yggdrasil_assert(idxGlob_begin <= idxRange_begin);	//This muys hold
	yggdrasil_assert(idxRange_begin <= idxRange_end);	//The samples could be empty

	//Now many bins we must hanle
	const Size_t nBinsToHandle = idxBin_end - idxBin_begin;
	yggdrasil_assert(nBinsToHandle > 0);

	//This is the number of samples
	const Size_t nSamples = ::std::distance(idxRange_begin, idxRange_end);


	//The index in the solution array, must we relative to the global start of the index array,
	//so we must determine the offset from there
	const Size_t nOffsetToGlobBeginning = ::std::distance(idxGlob_begin, idxRange_begin);

	/*
	 * Now we handle the base case
	 */
	if(nBinsToHandle == 1)
	{
		//This base case is easy, we will write the solution direct into the array
		//All samples belongs to the bin direcxtly
		//Notice that it can happens that we will overwrite results

		binIdxArray->at(idxBin_begin    ) = nOffsetToGlobBeginning;
		binIdxArray->at(idxBin_begin + 1) = nOffsetToGlobBeginning + nSamples;

		//We are done
		return;
	};//End if: basecase -> one bin

	if(nBinsToHandle == 2)
	{
		/*
		 * At this point a simple partitioning is enougtt we
		 * must get the value that partitionin everything.
		 */

		//This is the partitioning point
		const Numeric_t partPoint = binArray.getUpperBound(idxBin_begin);

		//Call the partitioning function
		const auto beginSecondBin = yggdrasil_argPartition(idxRange_begin, idxRange_end, dimArray, partPoint);

		if(beginSecondBin != idxRange_end)
		{
			if(!(partPoint <= dimArray[*beginSecondBin]))
			{
				throw YGGDRASIL_EXCEPT_LOGIC("The partition did not work.");
			};
		}; //End if: check if partitioned woirked

#ifndef NDEBUG
		//
		//This is a more exterem check

		//Test the lower part
		for(auto it = idxRange_begin; it != beginSecondBin; ++it)
		{
			yggdrasil_assert(dimArray.at(*it) < partPoint);
		}; //End for(it): test the lower part

		//Test the upper part
		for(auto it = beginSecondBin; it != idxRange_end; ++it)
		{
			yggdrasil_assert(partPoint <= dimArray.at(*it));
		}; //End for(it):
#endif


		//Now determine the length of the two ranges
		const Size_t lengthFirstBin  = ::std::distance(idxRange_begin, beginSecondBin);
#ifndef NDEBUG
		const Size_t lengthSecondBin = ::std::distance(beginSecondBin, idxRange_end);
		yggdrasil_assert((lengthFirstBin + lengthSecondBin) == nSamples);
#endif

		//Now write it into the range
		yggdrasil_assert(binIdxArray->at(idxBin_begin + 1) == 0);	//This bin was never writtien to
		binIdxArray->at(idxBin_begin + 0) = nOffsetToGlobBeginning;
		binIdxArray->at(idxBin_begin + 1) = nOffsetToGlobBeginning + lengthFirstBin;
		binIdxArray->at(idxBin_begin + 2) = nOffsetToGlobBeginning + nSamples; // nSamples == length1 + length2

		//We are now done
		return;
	}; //ENd if: basecase two bions

	/*
	 * It could be fafourable to handle 3 bins directly
	 */


	/*
	 * More base cases would increase the efficiency, but I do not
	 * have time for implementing them
	 */

	//Determine the middle bin
	const Size_t middleBin = (idxBin_begin + idxBin_end) / 2;

	//Now determine the parting point
	//Here we must use the lower bound uinstead of the upper bound.
	//BEcause of the past the end scheme
	const Numeric_t partPoint = binArray.getLowerBound(middleBin);


	//Perform the partitioning of the domain
	const auto idxRange_middle = yggdrasil_argPartition(idxRange_begin, idxRange_end, dimArray, partPoint);

	if(idxRange_middle != idxRange_end)
	{
		if(!(partPoint <= dimArray[*idxRange_middle]))
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The partition did not work.");
		};
	};

#ifndef NDEBUG
	//
	//This is a more exterem check

	//Test the lower part
	for(auto it = idxRange_begin; it != idxRange_middle; ++it)
	{
		yggdrasil_assert(dimArray.at(*it) < partPoint);
	}; //End for(it): test the lower part

	//Test the upper part
	for(auto it = idxRange_middle; it != idxRange_end; ++it)
	{
		yggdrasil_assert(partPoint <= dimArray.at(*it));
	}; //End for(it):
#endif

	/*
	 * Recursive calling
	 */
	ygInternal_recArgBinning(
		dimArray,
		binArray,
		idxGlob_begin,
		idxRange_begin,		//Start at the beginning
		idxRange_middle,	//Only go up to the middle
		idxBin_begin,		//Start at beginning
		middleBin,		//Only go to the middle bins
		binIdxArray);

	ygInternal_recArgBinning(
		dimArray,
		binArray,
		idxGlob_begin,
		idxRange_middle,	//Start at the middle
		idxRange_end, 		//Go to the end
		middleBin,		//Start at the middle bin
		idxBin_end,		//Go to the end
		binIdxArray);


	/*
	 * We are now finished
	 */
	return;
}; //End ygInternal_recBinning



YGGDRASIL_NS_END(yggdrasil)

