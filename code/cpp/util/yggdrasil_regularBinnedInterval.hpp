#pragma once
/**
 * \brief	This is a binning class.
 *
 * This class stores a the binning of an interval in a compressed form.
 * It is used inside the tests.
 * It is space efficient, but it  has to deal numerical chancelation.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>

#include <core/yggdrasil_valueArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

//Incluide std
#include <utility>
#include <cmath>
#include <vector>
#include <iostream>
#include <sstream>
#include <iterator>

YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \class 	yggdrasil_regularBinnedInterval_t
 * \brief	This class implements a equaidistant binned interval.
 *
 * It models an interval, remember in yggdrasil an interval is defined
 * as a right open mathematical interval.
 * It is special in the sense that it assumes equidistant grid spacing.
 * Given an interval it can be splitted into several equidistance
 * bins. This class models this. The small bins is stored in a
 * compressed way. The downside of thsi is, that numerical cancelation
 * could occure.
 *
 * Please also see that the small bins are also right open intervals.
 */
class yggdrasil_regularBinnedInterval_t
{
	/*
	 * ===================
	 * Typedef
	 */
public:
	using Numeric_t 	= ::yggdrasil::Numeric_t;				//!< The unerling numeric type
	using Real_t 		= ::yggdrasil::Real_t;					//!< This is the high precission type
	using stdPair_t 	= ::std::pair<Numeric_t, Numeric_t>;			//!> This is the expresion as pair
	using ValueArray_t 	= ::yggdrasil::yggdrasil_valueArray_t<Numeric_t>;	//!< This is the type for the value array
	using IntervalBound_t 	= ::yggdrasil::yggdrasil_intervalBound_t;		//!< This is the type of a full interval.

	/*
	 * ==============
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * It is constructed form an interval and the number of bins.
	 *
	 * \param  domain	The interval that should be splitted.
	 * \param  nBins	The number of bins the interval should be splitted in.
	 *
	 * \throw	If the interval is invalid or the number is invalid.
	 * 		 Also if the resulting binning is invalid.
	 *
	 */
	yggdrasil_regularBinnedInterval_t(
	  	const IntervalBound_t&		domain,
	  	const uInt_t 			nBins)
	 :
	  yggdrasil_regularBinnedInterval_t() 	//Calling the default constructor
	{
		if(domain.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Tried to construct a binned interval, from an invalid interval.");
		};
		if(nBins <= 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The number of requested bins is less or equal to zero, it was " + std::to_string(nBins));
		};

		//Now compose it
		m_lowerBound = domain.getLowerBound();
		m_nBins      = nBins;
		m_binLength  = domain.getLength() / (Numeric_t(nBins));

		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("A non valid binned interval was tried to construct.");
		};
	};



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_regularBinnedInterval_t(
		const yggdrasil_regularBinnedInterval_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_regularBinnedInterval_t(
		yggdrasil_regularBinnedInterval_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted
	 */
	yggdrasil_regularBinnedInterval_t&
	operator= (
		const yggdrasil_regularBinnedInterval_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_regularBinnedInterval_t&
	operator= (
		yggdrasil_regularBinnedInterval_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_regularBinnedInterval_t() noexcept = default;


	/*
	 * ==================
	 * Private constructor
	 */
private:
	/**
	 * \brief	Default constructor constructs an invalid binned interval.
	 *
	 * This function is private and can be called by mean of calling the static function
	 * CREAT_INVALID_BINNED_INTERVAL().
	 */
	explicit
	yggdrasil_regularBinnedInterval_t()
	 noexcept
	 :
	  m_lowerBound(NAN),
	  m_binLength(NAN),
	  m_nBins(0)
	{};


	/*
	 * ================
	 * Info functions.
	 */
public:
	/**
	 * \brief	This function returns the length of a single bin of \c *this.
	 *
	 * The value retunred by thsi function is the value of the original interval
	 * divided by the number of bins.
	 *
	 * Notice that this function could return NAN.
	 * This indicates an invalid binned interval.
	 */
	Numeric_t
	getBinLength()
	 const
	 noexcept
	{
		yggdrasil_assert(::std::isnan(m_binLength) == false);

		return m_binLength;
	}; //End: get bin length


	/**
	 * \brief	Return the number of bins that are stored.
	 *
	 * Notice that this function can return 0.
	 * This indicates an invalid binned interval
	 */
	uInt_t
	nBins()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nBins > 0);

		return m_nBins;
	}; //End: get numbers of bins


	/**
	 * \brief	This function returns the interval root of \c *this .
	 *
	 * The interval root is the lower bound of the original interval.
	 * It could be optained by calling getBinLowerBound(0).
	 * But this function is more direct in doing that.
	 */
	Numeric_t
	getIntervalRoot()
	 const
	 noexcept
	{
		yggdrasil_assert(::std::isnan(m_lowerBound) == false);

		return m_lowerBound;
	}; //End: getIntervalRoot


	/**
	 * \brief	This function returns a pair that describes the a transformation.
	 *
	 * This is a high precision parameter for the transformation.
	 * Assume a value of \c x and one wants to determine the bin index.
	 * For that one executed the following calculation.
	 * 	x * ret.first - ret.second
	 * Where ret is the return value of a call to this function.
	 * It is important thqt we have to use floor then to get the
	 * real index.
	 *
	 * This function is provided to make fast trasformations.
	 * The user should not use it
	 *
	 * \throw 	This function checks if \c *this is valid.
	 */
	::std::pair<Real_t, Real_t>
	getBinTrafoParameters()
 	 const
 	{
 		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to access an invalid binned interval.");
		};

		//This may be a bit instable but who cares
		const Real_t retFirst  = Real_t(1.0) / m_binLength;
		const Real_t retSecond = Real_t(m_lowerBound) / m_binLength;

		return std::make_pair(retFirst, retSecond);
	}; //End: getBinTrafoParameters


	/*
	 * ===============
	 * Accessing functions
	 */
public:
	/**
	 * \brief	Returns the \c i th bin that is managed by \c *this.
	 *
	 * Not that the counting starts at 0.
	 * Also remember that the upper bound of the returned interval
	 * does not belong to the ith bin but to the next one.
	 *
	 * \param  i 	The bin that should be constructed.
	 *
	 * \throw 	This funciton constructs an interval if this interval
	 * 		 is invalid an exception is generated.
	 * 		 Also if \c i is out of range.
	 */
	IntervalBound_t
	getBin(
		const uInt_t 		i)
	 const
	{
		yggdrasil_assert(this->isOkay() == true);
		if(i >= m_nBins)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Tried to access the interval " + std::to_string(i) + " but there are only " + std::to_string(m_nBins) + " intervals.");
		};

		//Generate the two bounds
		const Numeric_t lowerBound =  i      * m_binLength + m_lowerBound;
		const Numeric_t upperBound = (i + 1) * m_binLength + m_lowerBound;

		yggdrasil_assert(lowerBound < upperBound);

		//Construct an interval
		return IntervalBound_t(lowerBound, upperBound);
	}; //End: getBin

	/**
	 * \brief	This function returns the lower bound od the \c i th bin.
	 *
	 * This function is technically equivalent to this->getBin(i).getLowerBound().
	 * However this function does not construct an interval but calculates
	 * the bound directly.
	 * It requieres that \c *this is okay (isOkay() returns true), but
	 * does not check other things.
	 *
	 * \param  i 	The bin that should be constructed.
	 *
	 * \throw 	If \c i is out of bound or \c *this is not okay.
	 */
	Numeric_t
	getBinLowerBound(
		const uInt_t 		i)
	 const
	{
		if(this->isOkay() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to access a not okay bin.");
		};
		if(i >= m_nBins)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Tried to access the interval " + std::to_string(i) + " but there are only " + std::to_string(m_nBins) + " intervals.");
		};

		const Numeric_t lowerBound = i * m_binLength + m_lowerBound;

		return lowerBound;
	}; //End: getBinLowerBound


	/**
	 * \bried	This function returns the upper bound of the ith bin.
	 *
	 * It is technically equivalent to this->getBin(i).getUpperBound().
	 * But it does not construct a full interval.
	 * This means that the interval can not throw.
	 * Instead \c *thsi must be okay.
	 * \param  i 	The bin that should be constructed.
	 *
	 * \throw 	If \c i is out of bound or \c *this is not okay.
	 */
	Numeric_t
	getBinUpperBound(
		const uInt_t 		i)
	 const
	{
		if(this->isOkay() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to access a not okay bin.");
		};
		if(i >= m_nBins)
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("Tried to access the interval " + std::to_string(i) + " but there are only " + std::to_string(m_nBins) + " intervals.");
		};

		const Numeric_t upperBound = (i + 1) * m_binLength + m_lowerBound;

		return upperBound;
	}; //End: getBinUpperBound

	/**
	 * \brief	This function returns the bin index of the value \c x.
	 *
	 * Given a number x, that has to lie inside the original interval
	 * \c this was constructed with, then it returns the bin in which
	 * \c x would lie.
	 *
	 * Notice that due to numerical instabilities the interval can be sligtly off.
	 * Esspecially at the boundary of very small intervals.
	 *
	 * This function does not check if the value of the computed interval makes sense.
	 *
	 * Notice that this function returns a signed integer.
	 * The bins generally operates with unsigned integers.
	 *
	 * \param  x	The value we work with.
	 *
	 * \throw 	If \c *this is not okay.
	 */
	Int_t
	inWhichBin(
		const Numeric_t 	x)
	 const
	{
		if(this->isOkay() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not operate on a binned interval that is not okay.");
		};

		const Numeric_t unrounded_bin = (x - m_lowerBound) / m_binLength;

		//Now we have to round it
		const Numeric_t rounded_bin   = std::floor(unrounded_bin);

		return rounded_bin;
	}; //End: inWhichBin


	/*
	 * =================
	 * Status functions
	 */
public:
	/**
	 * \brief	This function returns true if \c *this is valid.
	 *
	 * This function checks that the values are inside some usefull bounds.
	 * It also checks if the bounds are distinguishable$
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		//First check if *this is oaky
		if(this->isOkay() == false)
		{
			return false;
		};

		//Now check if the bounds can be distingiished
		Numeric_t prevBound = m_lowerBound;
		for(uInt_t i = 1; i <= m_nBins; ++i)
		{
			const Numeric_t currBound = i * m_binLength + m_lowerBound;

			if(!(prevBound < currBound))
			{
				return false;
			};

			prevBound = currBound;
		}; //End for(i):

		return true;
	}; //End: isValid

	/**
	 * \brief	This function returns true if the parameters of \c *this has usefull meansings.
	 *
	 * This is that they are not NAN or something like that.
	 * This function is cheaper than the isValid() function.
	 */
	bool
	isOkay()
	 const
	 noexcept
	{
		if(::std::isnan(m_lowerBound) == true)
		{
			return false;
		};
		if(::std::isnan(m_binLength) == true)
		{
			return false;
		};
		if(m_binLength <= 0.0)	//It is important that we also do not allow zero lenth bins
		{
			return false;
		};
		if(m_nBins <= 0)	//For provision of data type change
		{
			return false;
		};

		return true;
	}; //End is okay





	/*
	 * =========================
	 * Static Functions
	 */
public:
	/**
	 * \brief	This function constructs an invalid binned interval.
	 *
	 * This function is the only way to construct an invalid structure.
	 */
	static
	yggdrasil_regularBinnedInterval_t
	CREAT_INVALID_BINNED_INTERVAL()
	 noexcept
	{
		return yggdrasil_regularBinnedInterval_t();
	};



	/*
	 * ==============
	 * Private Variable
	 */
private:
	Numeric_t 	m_lowerBound;	//!< The lower bound, the beginning of the interval
	Numeric_t 	m_binLength; 	//!< This is the length of a single bin
	uInt_t 		m_nBins;	//!< The numbers of bins
}; //End class: yggdrasil_regularBinnedInterval_t




YGGDRASIL_NS_END(yggdrasil)

