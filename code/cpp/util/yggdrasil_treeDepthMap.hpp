#pragma once
/**
 * \brief	This file provides a deapth map for the tree.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_statistics.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>

//INcluide std
#include <map>



YGGDRASIL_NS_BEGIN(yggdrasil)


/**
 * \brief	This is a depth map for the tree.
 * \class 	yggdrasil_treeDepthMap_t
 *
 * A depth map is bvasically a historam.
 * It contains stores how many leafs have a certain depth.
 *
 * It is basically a statistiacl information.
 * It is constructed out of a node facade element.
 */
class yggdrasil_treeDepthMap_t
{
	/*
	 * =================
	 * Typedefs
	 */
public:
	using Node_t 		= yggdrasil_nodeFacade_t;	//!< This is the node type the class uses
	using Size_t 		= ::yggdrasil::Size_t;		//!< This si the type for counting
	using Depth_t 		= Size_t;			//!< This is the class that represents the depth

	using DepthMap_t 	= ::std::map<Depth_t, Size_t>;	//!< This is the map that is used to contains the histogram
	using iterator 		= DepthMap_t::const_iterator;	//!< This is the iterator to iterate through the map
	using const_iterator	= DepthMap_t::const_iterator;	//!> THis is the constant iterator


	/*
	 * ====================
	 * Constructor
	 *
	 */
public:
	/**
	 * \brief	THe building constructor.
	 *
	 * This gets a leaf and examines it.
	 *
	 * \param  root		The node where the tree, also subtree should be rooted.
	 */
	yggdrasil_treeDepthMap_t(
		const Node_t& 		root);


	/**
	 * \brief	The comyp constructor is defaulted
	 */
	yggdrasil_treeDepthMap_t(
		const yggdrasil_treeDepthMap_t&)
	 = default;


	/**
	 * \brief	Default constructor.
	 *
	 * Is allowed.
	 */
	explicit
	yggdrasil_treeDepthMap_t()
	 = default;


	/**
	 * \brief	The copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_treeDepthMap_t&
	operator= (
		const yggdrasil_treeDepthMap_t&)
	 = default;


	/**
	 * \brief	The move constructor.
	 *
	 * Is defaulted
	 */
	yggdrasil_treeDepthMap_t(
		yggdrasil_treeDepthMap_t&&)
	 = default;


	/**
	 * \brief	The move assignent.
	 *
	 * Is defaulted.
	 */
	yggdrasil_treeDepthMap_t&
	operator= (
		yggdrasil_treeDepthMap_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted
	 */
	~yggdrasil_treeDepthMap_t()
	 = default;


	/*
	 * ===================
	 * Simple access function
	 */
public:
	/**
	 * \brief	Returns the number of times a leaf at that depth was observed.
	 */
	Size_t
	getDepthCount(
		const Depth_t 	depth)
	 const
	{
		//Try to find the depth
		const iterator posPosition = m_depthMap.find(depth);

		//Test if the key is in the map or not
		if(posPosition == m_depthMap.cend())
		{
			//The key was not found, so we return zero
			return 0;
		}; //End if: not found

		//The depth was found, so return the count
		return posPosition->second;
	}; //End: getDepthCount


	/**
	 * \brief	Thsi function returns the ith recording.
	 *
	 * This allows to iterate over the different depths.
	 * This returns a pair, first is the depth that was known
	 * and second is the number of times that depth was observed.
	 *
	 * \param  i	The depth index.
	 *
	 * \throw 	If an out of bound is detected.
	 *
	 * \note	This function is extremely inefficient.
	 * 		 Use the iterators instead.
	 */
	std::pair<Depth_t, Size_t>
	getIthDepth(
		const Depth_t	i)
	 const
	{
		if(m_depthMap.empty() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The depth map is empty.");
		};

		if(!(i < m_depthMap.size()))
		{
			throw YGGDRASIL_EXCEPT_OutOfBound("The provided index " + std::to_string(i) + " is too large, the map only host " + std::to_string(m_depthMap.size()) + " many depths.");
		};

		auto it = m_depthMap.cbegin();
		std::advance(it, i);

		return *it;
	}; //End: get the


	/**
	 * \brief	This function returns a vector that contains all observed depth of *this.
	 *
	 */
	std::vector<Depth_t>
	getDiffDepth()
	 const
	{
		if(m_depthMap.empty() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The depth map is empty.");
		};

		//Create the vector with the depths
		std::vector<Depth_t> diffDepth;
		diffDepth.reserve(m_depthMap.size());	//Reserve space

		//Going through the map and inserting them
		for(const auto& d : m_depthMap)
		{
			diffDepth.push_back(d.first);
		}; //End for(d):

		return diffDepth;
	}; //End: getDiffDepth


	/**
	 * \brief	This function returns the different number
	 * 		 of depths that are contained in *this.
	 */
	Size_t
	nDiffDepth()
	 const
	 noexcept
	{
		return m_depthMap.size();
	};


	/**
	 * \brief	Thsi function returns the mean depth of *this.
	 *
	 * The mean that is computed by this, is like the mean that would be generated
	 * by the sequence of teh depths
	 */
	Numeric_t
	getDepthMean()
	 const
	{
		return m_stat.get_Mean();
	};


	/**
	 * \brief	This function returns the standard deviation of the depths.
	 *
	 * This is like the std of the sequences of the depths.
	 * If there are to little data to compute it, then zero is returned.
	 */
	Numeric_t
	getDepthStd()
	 const
	{
		return m_stat.get_StdDeviation();
	};


	/**
	 * \brief	This function returns the maximum depth of *this.
	 *
	 * \throw	If the map is empty.
	 */
	Size_t
	getMaxDepth()
	 const
	{
		if(m_depthMap.empty() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("The depth map is empty.");
		};

		return m_depthMap.crbegin()->first;
	}; //End: getMaxDepth


	/**
	 * \brief	Thsi function returns the number of leafs of the tree.
	 *
	 * \throw 	If *this is empty.
	 */
	Size_t
	getLeafCount()
	 const
	{
		if(m_depthMap.empty() == true)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("the depth map is empty.");
		};

		//Accumulator for the number of leafs
		Size_t leafs = 0;

		//Goingt through all the different depths (first) and sum up
		//the number of their apperiance (second).
		for(const auto& p : m_depthMap)
		{
			yggdrasil_assert(p.second > 0);	//We requier this
			leafs += p.second;		//Accumulate
		}; //End for(p): going through all the different deptsh

		return leafs;
	}; //End: leaf count



	/*
	 * ===================
	 * Iterator functions
	 *
	 * The iterators allow access to pairs.
	 * the type if derferenced is:
	 * 	::std::pair<Depth_t, Size_t>
	 * the memeber first, is the depth, and the memeber second
	 * represnents the number of times this depth was recorded.
	 */
public:
	/**
	 * \brief	This beginning iterator
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		return m_depthMap.cbegin();
	}; //End: begin


	/**
	 * \brief	THis returns the past the end iterator.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		return m_depthMap.cend();
	}; //End: end



	/*
	 * =====================
	 * Private Members
	 */
private:
	DepthMap_t 		m_depthMap;	//!< This is the depth map
	yggdrasil_statistic_t 	m_stat;		//!< The statistic object.
}; //End class(yggdrasil_treeDepthMap)


YGGDRASIL_NS_END(yggdrasil)






