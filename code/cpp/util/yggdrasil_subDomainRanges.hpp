#pragma once
/**
 * \brief	THis class stores where the subdomains starts and end for each new domain.
 *
 * This is basically to keep track of them.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_subDomainEnum.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

//INcluide std
#include <vector>
#include <algorithm>

YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD
class yggdrasil_singelSubDomainRange_t;

/**
 * \brief	This is an internal class that allows the adapting of the index array, to enable range loops.
 * \class 	yggdrasil_singleSubDomainRangeLoopAddaptor_t
 *
 * This class is for internal use only.
 * It allows the usage of rangebased for loops to iterate over the index directly.
 * The user will never use it, if she/he does, then expect 1st Edition UNIX kernel code.
 */
template<bool isConst>
class yggdrasil_singleSubDomainRangeLoopAddaptor_t
{
	/*
	 * ===============
	 * Typedef
	 */
public:
	using IndexArray_t 	= yggdrasil_indexArray_t;	//!< This is the index array
	using IndexArray_It 	= IndexArray_t::iterator;	//!< This is the iterator for the index array
	using IndexArray_cIt 	= IndexArray_t::const_iterator;	//!< This is the const iterator of the index array
	using iterator 		= typename std::conditional<isConst == true, IndexArray_cIt, IndexArray_It>::type;
	using Size_t 		= ::yggdrasil::Size_t;		//!< This is used for the index position


	/*
	 * ==============
	 * Constructors
	 */
public:
	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleSubDomainRangeLoopAddaptor_t(
		const yggdrasil_singleSubDomainRangeLoopAddaptor_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleSubDomainRangeLoopAddaptor_t(
		yggdrasil_singleSubDomainRangeLoopAddaptor_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleSubDomainRangeLoopAddaptor_t&
	operator= (
		const yggdrasil_singleSubDomainRangeLoopAddaptor_t&) = default;


	/**
	 * \brief 	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleSubDomainRangeLoopAddaptor_t&
	operator= (
		yggdrasil_singleSubDomainRangeLoopAddaptor_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_singleSubDomainRangeLoopAddaptor_t() = default;


	/*
	 * ===========================
	 * Private constructors
	 *
	 * Accessable only for this and the friend
	 */
private:
	yggdrasil_singleSubDomainRangeLoopAddaptor_t()
	 noexcept :
	  m_begin(),
	  m_end()
	{};


	/**
	 * \brief	building construictor
	 */
	yggdrasil_singleSubDomainRangeLoopAddaptor_t(
		iterator 		start,
		iterator 		ende)
	 noexcept :
	  m_begin(start),
	  m_end(ende)
	{};



	/*
	 * ================
	 * Begin & End
	 */
public:
	iterator
	begin()
	 const
	 noexcept
	{
		return m_begin;
	};

	iterator
	end()
	 const
	 noexcept
	{
		return m_end;
	};



	/*
	 * =======================
	 * Friend
	 */
private:
	//The whole clas
	friend class yggdrasil_singelSubDomainRange_t;

	/*
	 * ==================
	 * Private Member
	 */
private:
	iterator 	m_begin;	//!< The begin
	iterator 	m_end;		//!< The end
}; //End class: yggdrasil_singleSubDomainRangeLoopAddaptor_t



/**
 * \class	yggdrasil_singelSubDomainRange_t
 * \brief	This is a single range.
 *
 * This is an internal helper class for the spliting process.
 * The spliting works on an index array.
 * The to keep track where the ranges of the different subdomains starts we collect.
 *
 * This class stores a single final range.
 * It storse the index of the beginning and the past the end index.
 *
 * It is imporant that it stores "pointers" into an index array, and not to the sample
 * array, there are two levels of indirections.
 *
 * This class is inmutable, but assignment is supported.
 */
class yggdrasil_singelSubDomainRange_t
{
	/*
	 * ================
	 * Typedefs
	 */
public:
	using IndexArray_t 	= yggdrasil_indexArray_t;	//!< This is the index array
	using IndexArray_It 	= IndexArray_t::iterator;	//!< This is the iterator for the index array
	using IndexArray_cIt 	= IndexArray_t::const_iterator;	//!< This is the const iterator of the index array
	using Size_t 		= ::yggdrasil::Size_t;		//!< This is used for the index position


	/*
	 * ===============
	 * Constnst
	 */
private:
	static const Size_t INVALID_INDEX = Size_t(-1);		//!< This is the invalid index


	/*
	 * =====================
	 * Constructors
	 */
public:

	/**
	 * \brief	Building constructor.
	 *
	 * This constructor constructs a single range.
	 *
	 * \param  start	This is the start index.
	 * \param  end 		This is the past the end index.
	 *
	 * \throw 	If the arguments results in an invalid insatnce.
	 */
	yggdrasil_singelSubDomainRange_t(
		const Size_t 	start,
		const Size_t 	end) :
	  m_start(start),
	  m_end(end)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The construction of the single sub range " + this->print() + " failed.");
		};
	};


	/**
	 * \brief	Default constructor.
	 *
	 * Constructs an invalid single range.
	 * Pretty useless and onyl needed for internal purpose.
	 *
	 * \note 	The user should not use it.
	 */
	explicit
	yggdrasil_singelSubDomainRange_t()
	 noexcept :
	  m_start(INVALID_INDEX),
	  m_end(INVALID_INDEX)
	{};


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singelSubDomainRange_t(
		const yggdrasil_singelSubDomainRange_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singelSubDomainRange_t(
		yggdrasil_singelSubDomainRange_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singelSubDomainRange_t&
	operator= (
		const yggdrasil_singelSubDomainRange_t&) = default;


	/**
	 * \brief 	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singelSubDomainRange_t&
	operator= (
		yggdrasil_singelSubDomainRange_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_singelSubDomainRange_t() = default;


	/*
	 * ==========================
	 * Querry function
	 */
public:
	/**
	 * \brief	This function tests if \c *this is valid.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		if(m_start == INVALID_INDEX)
		{
			return false;
		};

		if(m_end == INVALID_INDEX)
		{
			return false;
		};

		//We allow equality
		if(m_end < m_start)
		{
			return false;
		};

		return true;
	}; //End: isValid


	/**
	 * \brief	Returns the size of the range.
	 */
	Size_t
	size()
	 const
	 noexcept
	{
		return (m_end - m_start);
	};


	/**
	 * \brief	Returns \c true if the range is empty.
	 */
	bool
	isEmpty()
	 const
	 noexcept
	{
		return (m_start == m_end) ? true : false;
	};


	/**
	 * \brief	This function returns a string that contains the start and the past the end index of \c *this .
	 */
	std::string
	print()
	 const
	{
		return "(" + std::to_string(m_start) + ", " + std::to_string(m_end) + ")";
	};




	/*
	 * ===========================
	 * Access functions
	 */
public:
	/**
	 * \brief	Returns the beginning of the range represented by \c *this .
	 *
	 * This is the first index of an index array that belongs
	 * to the rage represented by \c *this.
	 *
	 * There is also a versio of this function, begin(IndexArray_t&), that
	 * returns an iterator.
	 *
	 * \return 	This returns an \em index , a \em number, into an index array.
	 *
	 */
	Size_t
	begin()
	 const
	 noexcept
	{
		return m_start;
	};


	/**
	 * \brief	Returns the pass the end index of the range represented by \c *this .
	 *
	 * This is the past the end index of the range represented by \c *this .
	 * This is an index into the associated index array.
	 *
	 * There is also a version of this function that takes as argument
	 * the assoicated index array, end(IndexArray_t&), and directly returns the iterator.
	 *
	 * \return 	This returns an \em index , a \em number, into an index array.
	 */
	Size_t
	end()
	 const
	 noexcept
	{
		return m_end;
	};


	/*
	 * ================
	 * Iterator adaptor functions
	 *
	 * This function can addapt the iterator of the index array.
	 */
public:
	/**
	 * \brief	Returns the iterator to the beginning of the rage represented by \c *this .
	 *
	 * This directly returns the correct position inside the array.
	 *
	 * \param  ia 	This is the index array
	 */
	IndexArray_It
	begin(
		IndexArray_t& 	ia)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The singleSubRange is invalid.");
		};

		if(m_start > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this starts at " + std::to_string(m_start));
		};

		if(m_end > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this ends at " + std::to_string(m_end));
		};

		return (ia.begin() + m_start);
	};


	/**
	 * \brief	Returns the constant iterator to the beginning of the rage represented by \c *this .
	 *
	 * This directly returns the correct position inside the array.
	 *
	 * \param  ia 	This is the index array
	 */
	IndexArray_cIt
	begin(
		const IndexArray_t& 	ia)
	 const
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The singleSubRange is invalid.");
		};

		if(m_start > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this starts at " + std::to_string(m_start));
		};

		if(m_end > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this ends at " + std::to_string(m_end));
		};

		return (ia.begin() + m_start);
	};


	/**
	 * \brief	Returns the constant iterator to the beginning of the rage represented by \c *this .
	 *
	 * This directly returns the correct position inside the array.
	 * This is the explicit version.
	 *
	 * \param  ia 	This is the index array
	 */
	IndexArray_cIt
	cbegin(
		const IndexArray_t& 	ia)
	 const
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The singleSubRange is invalid.");
		};

		if(m_start > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this starts at " + std::to_string(m_start));
		};

		if(m_end > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this ends at " + std::to_string(m_end));
		};

		return (ia.begin() + m_start);
	};


	/**
	 * \brief 	Returns the past the end iterator of the range represented by \c *this.
	 *
	 * \param  ia 	This is the index array
	 */
	IndexArray_It
	end(
		IndexArray_t& 	ia)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The singleSubRange is invalid.");
		};

		if(m_start > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this starts at " + std::to_string(m_start));
		};

		if(m_end > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this ends at " + std::to_string(m_end));
		};

		return (ia.begin() + m_end);
	};


	/**
	 * \brief 	Returns the constant past the end iterator of the range associated by \c *this .
	 *
	 * \param  ia 	This is the index array
	 */
	IndexArray_cIt
	end(
		const IndexArray_t& 	ia)
	 const
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The singleSubRange is invalid.");
		};

		if(m_start > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this starts at " + std::to_string(m_start));
		};

		if(m_end > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this ends at " + std::to_string(m_end));
		};

		return (ia.begin() + m_end);
	};


	/**
	 * \brief 	Returns the constant past the end iterator of the range represented by \c *this .
	 *
	 * This is the explicit version.
	 *
	 * \param  ia 	This is the index array
	 */
	IndexArray_cIt
	cend(
		const IndexArray_t& 	ia)
	 const
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The singleSubRange is invalid.");
		};

		if(m_start > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this starts at " + std::to_string(m_start));
		};

		if(m_end > ia.size())
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The index array has a size of " + std::to_string(ia.size()) + ", but *this ends at " + std::to_string(m_end));
		};

		return (ia.begin() + m_end);
	};



	/*
	 * =================
	 * Range Addaptor
	 */
public:
	/**
	 * \brief	Range addaptor of index array.
	 *
	 * This function will generate an temporary that can be used to directly iterate over the indexes.
	 */
	yggdrasil_singleSubDomainRangeLoopAddaptor_t<false>
	loopOver(
		IndexArray_t& 	ia)
	{
		return yggdrasil_singleSubDomainRangeLoopAddaptor_t<false>(this->begin(ia), this->end(ia));
	};


	/**
	 * \brief	Range addaptor of index array; constant version.
	 *
	 * This function will generate an temporary that can be used to directly iterate over the indexes.
	 */
	yggdrasil_singleSubDomainRangeLoopAddaptor_t<true>
	loopOver(
		const IndexArray_t& 	ia)
	 const
	{
		return yggdrasil_singleSubDomainRangeLoopAddaptor_t<true>(this->begin(ia), this->end(ia));
	};


	/**
	 * \brief	Range addaptor of index array; constant and explicit version.
	 *
	 * This function will generate an temporary that can be used to directly iterate over the indexes.
	 */
	yggdrasil_singleSubDomainRangeLoopAddaptor_t<true>
	cloopOver(
		const IndexArray_t& 	ia)
	 const
	{
		return yggdrasil_singleSubDomainRangeLoopAddaptor_t<true>(this->begin(ia), this->end(ia));
	};


	/*
	 * =====================
	 * Private Member
	 */
private:
	Size_t 		m_start;	//!< The start index
	Size_t 		m_end;		//!< The past the end index
}; //End class: yggdrasil_singelSubDomainRange_t


/**
 * \brief	This class is used to stores the ranges of the subdomains we produce.
 * \class 	yggdrasil_subDomainRages_t
 *
 * This class stores where the different ranges starts and end.
 * It collects the possitions of all the subdomains that are created during the splitting.
 *
 * To accesses the individual ranges the subDomainEnum_t class is used.
 * This allows to uniquelly address the different ranges.
 *
 * It is inmportant to note that this is a map into an INDEX_ARRAY!!
 *
 * There is allways an associated index array.
 * As noted before this is an internal class, that the user should note use.
 *
 * It is so, that we allow only the modifications of invalid ranges.
 */
class yggdrasil_subDomainRanges_t
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using SingleRange_t 	= yggdrasil_singelSubDomainRange_t;	//!< This represents one sub domain
	using IndexArray_t 	= SingleRange_t::IndexArray_t;		//!< This is the type of the array index
	using SubDomainEnum_t 	= yggdrasil_subDomainEnum_t;		//!< This is the enumerate to represent one split
	using RangeVector_t 	= ::std::vector<SingleRange_t>;		//!< This is the lists of all splits that has to be performed.


	/*
	 * ===================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * One has to specifiy the number of splits that will be performed.
	 *
	 * \param  nSplits	Numbers of splits
	 *
	 * The numbers of sub domains is \f$ 2^\texttt{nSplits} \f$.
	 * Please notice that all subranges that are created by this constructors are invalid.
	 */
	yggdrasil_subDomainRanges_t(
		const Int_t 	nSplits) :
	  m_nSplits(nSplits),
	  m_ranges(1 << nSplits)
	{
		if(m_nSplits < 1)
		{
			throw YGGDRASIL_EXCEPT_InvArg("We requere at least one split, but only " + std::to_string(m_nSplits) + " where requested.");
		};
	};


	/**
	 * \brief	Default constructor.
	 *
	 * Is deleted.
	 */
	yggdrasil_subDomainRanges_t()
	 noexcept = delete;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_subDomainRanges_t(
		const yggdrasil_subDomainRanges_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_subDomainRanges_t(
		yggdrasil_subDomainRanges_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_subDomainRanges_t&
	operator= (
		const yggdrasil_subDomainRanges_t&) = default;


	/**
	 * \brief 	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_subDomainRanges_t&
	operator= (
		yggdrasil_subDomainRanges_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_subDomainRanges_t() = default;


	/*
	 * ====================
	 * Query functions
	 */
public:
	/**
	 * \brief	This function returns true \c if subdomain \c i is valid
	 *
	 * \param  i	The subdomain that should be checked. Must be an subdoman enumeration type.
	 *
	 * \throw 	If the enumeration is not capatible with \c *this .
	 */
	bool
	isValidSubDomain(
		const SubDomainEnum_t&		i)
	 const
	{
		//Tests if the dept is correct
		if(i.getDepth() != m_nSplits)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The split that is presented has a depth of " + std::to_string(i.getDepth()) + ", but this has a depth of " + std::to_string(m_nSplits));
		};

		return m_ranges.at(i.getWay()).isValid();
	};


	/**
	 * \brief	Tests if all subdomains of \c *this are valid.
	 */
	bool
	allSubDomainsValid()
	 const
	 noexcept
	{
		for(const auto& sd : m_ranges)
		{
			if(sd.isValid() == false)
			{
				return false;
			};
		}; //End for(sd):

		return true;
	};


	/**
	 * \brief	Get a reference to the \c i th subdomain.
	 *
	 * \param  i	The subdomain that should be returned. Must eb an enumeration type.
	 *
	 * \throw 	If the enumeration is not comatible with \c *this .
	 *
	 * \note 	One can only return constant references.
	 */
	const SingleRange_t&
	getSubDomain(
		const SubDomainEnum_t&		i)
	 const
	{
		//Tests if the dept is correct
		if(i.getDepth() != m_nSplits)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The split that is presented has a depth of " + std::to_string(i.getDepth()) + ", but this has a depth of " + std::to_string(m_nSplits));
		};

		return m_ranges.at(i.getWay());
	};


	/**
	 * \brief	This function allows to exchange the \c i th subdomain of \c *this with the new subdomain \c newDom .
	 *
	 * For this to work \c newDom must be valid and the currently storedrange fopr subdomain \c i , must be invalid.
	 * This is function allows write once semantic.
	 *
	 * \param  i		The subdomain to change
	 * \param  subDom	The new subdomain
	 *
	 * \throw	If the \c i th subdomain is invalid or the new subdomain is not.
	 * 		 Also if there are compability issues.
	 */
	void
	exchangeSubDomain(
		const SubDomainEnum_t&		i,
		const SingleRange_t&		newDom)
	{
		//Tests if the dept is correct
		if(i.getDepth() != m_nSplits)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The split that is presented has a depth of " + std::to_string(i.getDepth()) + ", but this has a depth of " + std::to_string(m_nSplits));
		};

		if(newDom.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The new domain " + newDom.print() + " is invalid.");
		};

		if(m_ranges.at(i.getWay()).isValid() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to exchange a valid domain " + m_ranges[i.getWay()].print());
		};


		//Now perform the exchanging
		m_ranges.at(i.getWay()) = newDom;

		return;
	};


	/**
	 * \brief	This function returns the number of sub ranges are stored inside \c *this .
	 *
	 * This is not the number of the valid subranges, but the total subranges that are stored.
	 */
	Size_t
	nSubRanges()
	 const
	 noexcept
	{
		return m_ranges.size();
	};


	/**
	 * \brief	This function returns the number of splits that where performed.
	 *
	 * This is basically the hight of the splitting process.
	 */
	Size_t
	nSplits()
	 const
	 noexcept
	{
		return m_nSplits;
	};


	/**
	 * \brief	This function performs the full test of \c *this .
	 *
	 * This function tests if the union of all the ranges of \c *this ,
	 * are of the form \f$ [0, n[ \f$ .
	 *
	 * This function should be called at the end of the spliting process.
	 * It verifies that are indexes are assigned to exactly one subrange.
	 *
	 * \return	If this is the case it returns n.
	 * 		 In the case of an error -1 is returned, if no exception happened.
	 * 		 Also all subdomains must be valid.
	 *
	 * \throw 	SOme checks are performed if they fail exceptions are generated.
	 *
	 * \note	\c n is the past the end index, it is also the number of stored index.
	 */
	Size_t
	findUnionRange()
	 const
	{
		if(this->allSubDomainsValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Not all subdomains are valid.");
		};

		//This is the value we search for
		Size_t searchValue = 0;
		Size_t nextSearchValue; 		//Is declared here for debugging purposes

		/*
		 * The number of passes that are needed is the number of non empty ranges.
		 * Since the empty range does not conain any samples.
		 * they are not contributing to the union.
		 */
		const Size_t nPasses = std::count_if(m_ranges.cbegin(), m_ranges.cend(), [](const SingleRange_t& sr) -> bool {return sr.isEmpty() == false;});

		//We must iterate severall times through the ranges
		for(Size_t pass = 0; pass != nPasses; ++pass)
		{
			bool foundValue = false;

			//Now we must iterate through the range
			for(Size_t r = 0; r != m_ranges.size(); ++r)
			{
				//We are searching for the current beginning
				//We have to keep in mind, that there could also be some
				//empty ranges, we have to exclude them.
				if(m_ranges[r].begin() == searchValue && m_ranges[r].isEmpty() == false)
				{
					//We have found the value

					if(foundValue == true)
					{
						//We have found the value twiche
						throw YGGDRASIL_EXCEPT_LOGIC("The value was found twice.");
					};

					//We now set that we have fiound the value
					foundValue = true;

					//We also have to store the next searc value
					nextSearchValue = m_ranges[r].end();
				}; //The value is the same
			}; //End for(r): Going throgh the ranges

			if(foundValue == false)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("Could not find the value " + std::to_string(searchValue));
			};

			//Now set the new search value
			searchValue = nextSearchValue;
		}; //End for(pass):

		//Return the found range
		return searchValue;
	}; //End function find union range




	/**
	 * \brief	This function performs some optimization of the index array.
	 *
	 * The different sub ranges of the index array can be in any order.
	 * This is bad for the cache.
	 * This is why there is this optimization.
	 * It sorts the rage in alternating acending and decending order.
	 * This has two advantages, the first is that we con potentially optain spatial locality.
	 * By the alternating sorting, we can potentially also get temporal locality, because we use
	 * cache lines that where potentially accessed just recently.
	 *
	 * \note	This is a hunch I have, one should perform some tests.
	 * 		 But I think that at least sorting is good, the alternating maybe.
	 *
	 * \param  indexArray 		The index array that is accossiated to \c *this .
	 *
	 * \note	This function will change the order of the indexses of \c indexArray.
	 * 		 This will invalidate iterators that depends that they points to certain value.
	 * 		 However any iterator that only has to point inside a certain subrange,
	 * 		 regardless of the value there, will remain valid.
	 */
	void
	finalizeArrayStructure(
		IndexArray_t& 		indexArray)
	 const
	{
		if(this->allSubDomainsValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can only optimize a valid sunrange.");
		};

		for(Size_t r = 0; r != m_ranges.size(); ++r)
		{
			if(r % 2 == 0)
			{
				//The ange is even; so accending order
				::std::sort(
					indexArray.begin() + m_ranges[r].begin(),
					indexArray.begin() + m_ranges[r].end(),
					[](const Size_t& l, const Size_t& r) -> bool { return (l < r); }
					   );
			}
			else
			{
				//The range is odd, so decending order
				::std::sort(
					indexArray.begin() + m_ranges[r].begin(),
					indexArray.begin() + m_ranges[r].end(),
					[](const Size_t& l, const Size_t& r) -> bool { return (r < l); }
					   );
			};
		}; //End for(r):


		return;
	}; //End finalize array


	/**
	 * \brief	This function is able to create an subrange enum from an interger.
	 *
	 * This function is unsafe and should only be used in internal code.
	 * It is bassically a hack.
	 */
	static
	SubDomainEnum_t
	CREATE_UNSAFE_SUBDOMAIN_ENUM(
		const yggdrasil_subDomainRanges_t&	r,
		const Size_t 				i)
	{
		return r.priv_getSubDomainEnum(i);
	};




	/*
	 * ===================
	 * Internal functions
	 */
private:
	/**
	 * \brief	This function transforms an integer into a subDomainEnum_t .
	 *
	 * This is an internal function.
	 *
	 * \param  i 		The integer to transform.
	 */
	SubDomainEnum_t
	priv_getSubDomainEnum(
		const Size_t 	i)
	 const
	{
		if(i >= m_ranges.size())
		{
			throw YGGDRASIL_EXCEPT_InvArg("Requested the " + std::to_string(i) + " subdomain, but stores only " + std::to_string(m_ranges.size()));
		};

		return yggdrasil_subDomainEnum_t(i, m_nSplits);
	};



	/*
	 * ==================
	 * Private Members
	 */
private:
	Int_t 			m_nSplits;	//!< This is the number of splits that are performed
	RangeVector_t 		m_ranges;	//!< This stores all the ranges
}; //End class: yggdrasil_subDomainRanges_t




YGGDRASIL_NS_END(yggdrasil)
