/**
 * \brief	This file implement the standard way of sorting.
 * 		 It is not recomended to use that.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_util.hpp>
#include <util/yggdrasil_chi2_frequencyTable.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sample.hpp>




//Incluide std
#include <memory>
#include <vector>
#include <numeric>
#include <algorithm>

//Include boost
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/normal.hpp>


YGGDRASIL_NS_BEGIN(yggdrasil)


bool
yggdrasil_chi2FreqTable_t::private_buildFreqTable_std(
	const DimArray_t& 		dimArray1,
	const BinEdgesArray_t& 		binEdges1,
	const DimArray_t& 		dimArray2,
	const BinEdgesArray_t& 		binEdges2)
{
	if(m_freqTable.empty() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The frequency table is allocated, but it should not.");
	};

	//This is teh number of samples
	const Size_t nSamples = dimArray1.nSamples();

	//Allocateing the table
	m_freqTable.assign(m_nRowsFirst * m_nColsSecond, 0);

	//this is for the end of the iteration
	const Size_t lastBinFirst  = m_nRowsFirst  - 1;
	const Size_t lastBinSecond = m_nColsSecond - 1;
	yggdrasil_assert(m_nRowsFirst == binEdges1.nBins());
	yggdrasil_assert(m_nColsSecond == binEdges2.nBins());


	//Now we creat references to the two underlying arrays
	const BinEdgesArray_t::ValueArray_t& binEdgeArray1 = binEdges1.getUnderlyingArray();
	const BinEdgesArray_t::ValueArray_t& binEdgeArray2 = binEdges2.getUnderlyingArray();

	//We requiere the array to be sorted
	yggdrasil_assert(::std::is_sorted(binEdgeArray1.cbegin(), binEdgeArray1.cend()));
	yggdrasil_assert(::std::is_sorted(binEdgeArray2.cbegin(), binEdgeArray2.cend()));
	yggdrasil_assert(dimArray1.getAssociatedDim() != dimArray2.getAssociatedDim());

	/*
	 * In this loop we will go throug all the samples
	 * and determine the positin where they belongs to
	 */
	for(Size_t i = 0; i != nSamples; ++i)
	{
		//Load the coordinate
		const Numeric_t xFirst  = dimArray1[i];
		const Numeric_t xSecond = dimArray2[i];

		//this are the binIdx where they belongs to
		//They are initialized such that we will notice an error
		Size_t binIdx1 = m_nRowsFirst  + 2;
		Size_t binIdx2 = m_nColsSecond + 2;


		switch(m_nRowsFirst)
		{
		  case 1:
		  	//There is just one bin, so the solution is trivial
		  	binIdx1 = 0;
		  break;

		  case 2:
		  	//This is the case that there is just two bins
		  	binIdx1 = (xFirst < binEdgeArray1[1]) ? 0 : 1;
		  break;

		  case 3:
		  	//This is the case if there are three bins
		  	if(xFirst < binEdgeArray1[1])
			{
				binIdx1 = 0;
			}
			else if(xFirst < binEdgeArray1[2])
			{
				binIdx1 = 1;
			}
			else
			{
				binIdx1 = 2;
			};
		  break;

		  default:
			//Now we search the first row
			//We test if we are smaller than the upper bound
			//of the current bin. we must do that for all the bins
			//except the last one. when we would check that,
			//accessing the last position in the bin edge array
			//we must finish.
			//Doing it that wasy will give us the saturating effect
			for(binIdx1 = 0; binIdx1 != lastBinFirst; ++binIdx1)
			{
				yggdrasil_assert((binIdx1 + 1) < binEdgeArray1.size());
				const Numeric_t currUpperBound = binEdgeArray1[binIdx1 + 1];

				//Test if the value is smaller than the the upper
				//limit. If this is the case, then we have
				//found the bin where the sample belongs to
				if(xFirst < currUpperBound)
				{
					//We must do notthing, just exit the loop
					break;
				}; //End found the bin

				//The sample must not lie inside the current bin
				yggdrasil_assert(binEdges1.getBinInterval(binIdx1).isInside(xFirst) == false);
			}; //End for(binIdx1):
		}; //ENd switch


		switch(m_nColsSecond)
		{
		  case 1:
		  	//There is just one bin, so the solution is trivial
		  	binIdx2 = 0;
		  break;

		  case 2:
		  	//This is the case that there is just two bins
		  	binIdx2 = (xSecond < binEdgeArray2[1]) ? 0 : 1;
		  break;

		  case 3:
		  	//This is the case if there are three bins
		  	if(xSecond < binEdgeArray2[1])
			{
				binIdx2 = 0;
			}
			else if(xSecond < binEdgeArray2[2])
			{
				binIdx2 = 1;
			}
			else
			{
				binIdx2 = 2;
			};
		  break;


		  default:
			//Now we must do the same thing for the second dimension
			for(binIdx2 = 0; binIdx2 != lastBinSecond; ++binIdx2)
			{
				yggdrasil_assert((binIdx2 + 1) < binEdgeArray2.size());
				const Numeric_t currUpperBound = binEdgeArray2[binIdx2 + 1];

				if(xSecond < currUpperBound)
				{
					//We have found the interval of the current sample
					break;
				};

				yggdrasil_assert(binEdges2.getBinInterval(binIdx2).isInside(xSecond) == false);
			}; //End for(binIdx2)
		  break;
		}; //End switzch

		//Tests
		yggdrasil_assert((binIdx1 == 0) ? (xFirst < binEdgeArray1.at(1))
			: (
				(binIdx1 == lastBinFirst) ? (binEdgeArray1.at(binEdgeArray1.size() - 2) <= xFirst)
				: (
					binEdges1.getBinInterval(binIdx1).isInside(xFirst)
				  )
			  )
			); //End assert

		yggdrasil_assert((binIdx2 == 0) ? (xSecond < binEdgeArray2.at(1))
			: (
				(binIdx2 == lastBinSecond) ? (binEdgeArray2.at(binEdgeArray2.size() - 2) <= xSecond)
				: (
					binEdges2.getBinInterval(binIdx2).isInside(xSecond)
				  )
			  )
			); //End assert

#ifndef NDEBUG
		const Size_t oldValue = this->getFrequency(binIdx1, binIdx2);
#endif

		/*
		 * Now we have found the index, so we have to
		 * increase the count
		 */
		m_freqTable[binIdx1 * m_nColsSecond + binIdx2] += 1;

		yggdrasil_assert((oldValue + 1) == this->getFrequency(binIdx1, binIdx2));
	}; //End for(i): determine the bin


	return true;
}; //End: building the freq table in standard way


YGGDRASIL_NS_END(yggdrasil)



