#pragma once
/**
 * \file	util/yggdrasil_util.hpp
 * \brief	This file implements generall utility functins, that could be usefull inside
 * 		 Yggdrasil.
 *
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_indexArray.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <util/yggdrasil_generalBinEdges.hpp>


//Including std
#include <vector>
#include <algorithm>
#include <utility>
#include <limits>


YGGDRASIL_NS_START(yggdrasil)


//FWD of the hypercube
class yggdrasil_hyperCube_t;


/**
 * \brief	This fucntion computes the sum of an array.
 *
 * It uses pairwise summation, this mean it will perform
 * a recursive splitting of the input array and apply itself
 * to that array.
 *
 * The base case is 100.
 *
 * \param  summands	The array to be summed up.
 */
extern
Real_t
yggdrasil_recursiveSumming(
	const yggdrasil_valueArray_t<Numeric_t>& 	summands);


/**
 * \brief	This fucntion computes the sum of an array.
 *
 * It uses pairwise summation, this mean it will perform
 * a recursive splitting of the input array and apply itself
 * to that array.
 *
 * The base case is 100.
 *
 * \param  summands	The array to be summed up.
 */
extern
Real_t
yggdrasil_recursiveSumming(
	const yggdrasil_valueArray_t<Real_t>& 	summands);



/**
 * \brief	This function generates a gird.
 *
 * Given a hypercube of dimension one or two.
 * this function computes a fine grid on that domain
 * The result is saved in a sample array.
 *
 * It is also so, the intervalk definition is applied.
 * Meaning that the upper bound is never included.
 *
 * It is possible to exclude the lower bound from the
 * grid.
 *
 * \param  domain	The domain that should be gridded.
 * \param  points1 	The number of points in the first dimension.
 * \param  points2 	The number of points in the second dimension.
 * 			 If zero the value of the first one is used.
 * \param  woLowest	This excludes the lower bound value.
 * 			 The effect is that only internal points are generated.
 */
extern
yggdrasil_arraySample_t
yggdrasil_generateGrid(
	const yggdrasil_hyperCube_t&	domain,
	Size_t 				points1,
	Size_t  			points2,
	const bool 			woLowest);




/**
 * \brief	This function allows to recursively define the binns.
 *
 * This function takes a range to an index array and dimension array.
 * Additionally it also takes a bin structure.
 * It will then compute the binning and rearange the bins inside.
 * It will return a vector that contains the start index, inside the
 * index array, very indirect I know, of each bin.
 * The format is the same as inside the bin class.
 *
 * Thsi fucntion is able to recursively splt the process and
 * reduce the problems to simple base cases.
 *
 * The performance will be best if the working set, the dimensional
 * array, at least the part that is needed and the index array fit
 * into cache.
 *
 * It is possible to sort the bins, inside the index array,
 * all in assending order.
 *
 *
 * \param  dimArray		The dmenional array to be processed.
 * \param  binArray		The bins that should be applied.
 * \param  idxArray_begin	The start of the index array.
 * \param  idxArray_end		The PAST THE END iterator.
 * \param  sortBins		Bool to indicate if the index array should be sorted.
 * 				 This is important for the frequency table.
 *
 * \thorow	If compability conditions are violated or an
 * 		 internal fucntion throws.
 */
extern
yggdrasil_valueArray_t<Size_t>
yggdrasil_recArgBinning(
	const yggdrasil_dimensionArray_t&			dimArray,
	const yggdrasil_genBinEdges_t&				binArray,
	const yggdrasil_valueArray_t<SortIndex_t>::iterator 	idxArray_begin,
	const yggdrasil_valueArray_t<SortIndex_t>::iterator 	idxArray_end,
	const bool 						sortBins);


/**
 * \brief	Also recursive binning but a bit more convenitnet
 *
 * It can handle an the index array directly
 *
 */
inline
yggdrasil_valueArray_t<Size_t>
yggdrasil_recArgBinning(
	const yggdrasil_dimensionArray_t&		dimArray,
	const yggdrasil_genBinEdges_t&			binArray,
	yggdrasil_valueArray_t<SortIndex_t>* 		idxArray,
	const bool 					sortBins)
{
	return yggdrasil_recArgBinning(
		dimArray, binArray,
		idxArray->begin(), idxArray->end(),
		sortBins);
}; //ENd convenitnet interface





/**
 * \brief	This function implements argument sorting of the given dimensionaly array.
 *
 * This function takes a dimensionaal array and sorts it in ascending order.
 * However the array is nort moified. The ordering is one at an
 * index array that is returned.
 *
 * \param  dimArray	The dimensional array that should be sorted.
 *
 * \tparam  Index_T	This is the type that should be used for the indexing.
 *
 */
template<
	class Index_T>
yggdrasil_valueArray_t<Index_T>
yggdrasil_argSort(
	const yggdrasil_dimensionArray_t&			dimArray)
{
        //Test if we could index all
        const ::yggdrasil::Size_t maxPossibleIndexable = ::std::numeric_limits<Index_T>::max();
        if(maxPossibleIndexable <= dimArray.size())
        {
                throw YGGDRASIL_EXCEPT_illMethod("Can not address enough samples.");
        };

        const auto nSamples = dimArray.size();

        //Create the index array
        yggdrasil_valueArray_t<Index_T> indexArray;
        indexArray.resize(nSamples);
        for(Index_T i = 0; i != nSamples; ++i)
        {
                indexArray[i] = i;
        }; //End for(i)

        //Sorting
        yggdrasil_argSort<Index_T>(
        	indexArray.begin(), indexArray.end(),
        	dimArray);

        return indexArray;
}; //End: sorting




/**
 * \brief	This function implements argument sorting of the given dimensionaly array.
 *
 * This fucntion operates allready on an exosting index range, that
 * is passed to the function
 *
 * \param  indexArray_start	The beginning of the index array.
 * \param  indexArray_end	The end of the index array.
 * \param  dimArray		The dimensional array that should be sorted.
 *
 * \tparam  Index_T	This is the type that should be used for the indexing.
 *
 */
template<
	class Index_T>
void
yggdrasil_argSort(
	typename yggdrasil_valueArray_t<Index_T>::iterator 	indexArray_start,
	typename yggdrasil_valueArray_t<Index_T>::iterator 	indexArray_end,
	const yggdrasil_dimensionArray_t&			dimArray)
{
	yggdrasil_assert(indexArray_start <= indexArray_end);

	//Test if we could index all
	const ::yggdrasil::Size_t maxPossibleIndexable = ::std::numeric_limits<Index_T>::max();
	if(maxPossibleIndexable <= dimArray.size())
	{
		throw YGGDRASIL_EXCEPT_illMethod("Can not address enough samples.");
	};

#ifdef NDEBUG
	//Get the underling data
	const yggdrasil_dimensionArray_t::Array_t& uDimArray = dimArray.internal_getInternalArray();
	const Numeric_t* const rawDimArray = uDimArray.data();
#endif

	//Now we sort
	::std::sort(
		indexArray_start, indexArray_end,
#ifndef NDEBUG
		[&dimArray](const Index_T lhs, const Index_T rhs) -> bool
		 {
			return (dimArray.at(lhs) < dimArray.at(rhs));
		 }
#else
		[&rawDimArray](const Index_T lhs, const Index_T rhs) -> bool
		 {
			return (rawDimArray[lhs] < rawDimArray[rhs]);
		 }
#endif
		); //End sorting command

	yggdrasil_assert(
		std::is_sorted(
			indexArray_start, indexArray_end,
			[&dimArray](const Index_T lhs, const Index_T rhs) -> bool
			 {
		 		return (dimArray.at(lhs) < dimArray.at(rhs));
			 }
			      )
			);
	return;
}; //End: argSort



/**
 * \brief	This function is an argument sorter for general objects.
 *
 * The main difference between this function and other function is
 * that it operates on general objects and not just on dimensional
 * arrays. Thus allows a wider applicational range, but it is
 * not as optimized for that.
 * It operates on ranges.
 *
 * \param  startRange 		Iterator to the first index that should be treated.
 * \param  endRange		Past the end iterator to the index array.
 * \param  keys 		The container that contains the keys that should be sorted.
 * \param  comp 		The comparison fucntion that is used.
 *
 * \tparam  IdxIterator_t	The index operator that is used, must be random access.
 *
 * \tparam  Keys_t 	This is the container that is sorted. It is requiered that it supports
 * 			 operator[] and size() with obvious meaning.
 *
 * \tparam Comp_t 	This is the comparison object that should be used.
 */
template<
	class 	IdxIterator_t,
	class 	Keys_t,
	class 	Comp_t
>
void
yggdrasil_argSortContainer(
	IdxIterator_t 		startRange,
	IdxIterator_t 		endRange,
	const Keys_t& 		keys,
	Comp_t 			comp)
{
	//Make some tests
	if(endRange < startRange)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The range is wrong.");
	};

	//Test if the range is empty
	if(endRange == startRange)
	{
		//Range is empty, nothing to do
		return;
	}; //End if: range is empty

	//This is teh number of object we have
	const Size_t nKeys = keys.size();

	//Perform the sorting
	std::sort(
		startRange, endRange,	//The range
		//The predicate that we use
		[&comp, &keys, nKeys]
		 (const Size_t lhs, const Size_t rhs) -> bool
		 {
		   yggdrasil_assert(lhs < nKeys);
		   yggdrasil_assert(rhs < nKeys);
		   return comp(keys[lhs], keys[rhs]);
		 }
	); //End: sort call

	//End of teh function
	return;
}; //End: general arg sorter.


/*
 * \brief	This fucntion performs argument sorting on the given container.
 *
 * The difference between this and the other overload is, that it will generate an
 * index array on its own and then call the sorting function with that.
 * The array is returned.
 *
 * \param  keys 		The container that contains the keys that should be sorted.
 * \param  comp 		The comparison fucntion that is used.
 *
 * \tparam  Keys_t 	This is the container that is sorted. It is requiered that it supports
 * 			 operator[] and size() with obvious meaning.
 *
 * \tparam Comp_t 	This is the comparison object that should be used.
 */
template<
	class 	Keys_t,
	class 	Comp_t
>
yggdrasil_indexArray_t
yggdrasil_argSortContainer(
	const Keys_t& 		keys,
	Comp_t 			comp)
{
	//Test if the keys are empty then we emidetly return
	if(keys.size() == 0)
	{
		return yggdrasil_indexArray_t();
	}; //End if: empty keys

	//Cache the number of the keys
	const Size_t nKeys = keys.size();

	//This is the object used as index array
	yggdrasil_indexArray_t idxArray(nKeys);

	//Filling the index array
	for(Size_t i = 0; i != nKeys; ++i)
	{
		idxArray[i] = i;
	}; //End for(i): filling the array


	//Applying the function
	yggdrasil_argSortContainer(
		idxArray.begin(),
		idxArray.end(),
		keys,
		std::move(comp)
	); //End: call to worker fucntion


	//Return the index array
	return idxArray;
}; //End argumenbt sorting of with automatic generation of the index array


/**
 * \brief	This function applies the reordering of the container container
 * 		 according to the ordering that is given by idx range.
 *
 * This fucntion needs that the container that is something similar to the vector.
 * The object of it must be move constructable and move assignable.
 *
 * \param  startRange		The start of the index range.
 * \param  endRange 		Past the end iterator of the index range.
 * \param  container 		The container that should be reordered.
 *
 * \tparam  IdxIterator_t 	The type of the index iterator.
 *
 * \tparam  Container_t 	The container that is used, must have an size(),
 * 				 operator[] and a reserve() with obvious meaning.
 * 				 value_type must be move assignmablke and constructable.
 * 				 The container itself also needs to be move assignable.
 *
 * \throw 	If moemory allocation fails, also if the lize of the index range and the
 * 		 index range does not have the same size as the container has.
 * 		 If this function throws then the container can be in any state.
 */
template<
	class 	IdxIterator_t,
	class 	Container_t
>
void
yggdrasil_reorderContainer(
	IdxIterator_t 		startRange,
	IdxIterator_t 		endRange,
	Container_t& 		container)
{
	//Get the length of the index array
	const Size_t idxLength = std::distance(startRange, endRange);

	//Test if the size is enough
	if(idxLength != container.size())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The length of the index range is " + std::to_string(idxLength) + ", but the length of the container is " + std::to_string(container.size()));
	};

	//For reordering we need a temporary copy
	Container_t tmpCopy;		//Empty conatiner
	tmpCopy.reserve(idxLength);	//This

	//Perform a moving copy
	for(Size_t i = 0; i != idxLength; ++i)
	{
		//Move it into the temporaty container
		tmpCopy.push_back(std::move(container[i]));
	}; //End for(i): going through the array

	//Now we reassigne all of them
	//We do thsi by moving them back
	for(Size_t i = 0; i != idxLength; ++i)
	{
		//This is the source index
		const Size_t sourceIdx = *startRange;

		//Test if the index is inside the range
		yggdrasil_assert(sourceIdx < idxLength);

		//Increment the pointer for the next round
		++startRange;

		//Write the index back to the array
		container[i] = std::move(tmpCopy[sourceIdx]);
	}; //End for(i): moving the objects back

	//Test if the full range was searched
	if(startRange != endRange)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("THere is an error with the coping of the element.");
	};

	//end
	return;
}; //End: rorder the container



/**
 * \brief	This fucntion performs the rearanging of container.
 *
 * The index array is not given as a range but by an index array object.
 * This function calls the function of teh same name, that takes indexes.
 *
 * \param  idxArray 		This is the index array that is used for sorting.
 * \param  container 		The container that should be reordered.
 *
 * \tparam  IdxIterator_t 	The type of the index iterator.
 *
 * \tparam  Container_t 	The container that is used, must have an size(),
 * 				 operator[] and a reserve() with obvious meaning.
 * 				 value_type must be move assignmablke and constructable.
 * 				 The container itself also needs to be move assignable.
 *
 * \throw 	If moemory allocation fails, also if the lize of the index range and the
 * 		 index range does not have the same size as the container has.
 * 		 If this function throws then the container can be in any state.
 */
template<
	class 	Container_t
>
void
yggdrasil_reorderContainer(
	const yggdrasil_indexArray_t& 		idxArray,
	Container_t& 				container)
{
	//Calling the underling function
	yggdrasil_reorderContainer(
		idxArray.cbegin(),
		idxArray.cend(),
		container);

	//Return
	return;
}; //End: rearange based on index array



/**
 * \brief	This is a wrapper for the nth_element function form the standard.
 *
 * It works like partitioning, but one only have to know the iterator
 * position then the array is rearaged such that given iterator
 * is there in the corrected sequence.
 *
 * \param  indexArray_sStart	The iterator where the range start.
 * \param  indexArray_middle	This element should be correctly sorted.
 * \param  indexArray_end 	The iterator where the range ends (past the end).
 * \param  dimArray		This is the dimensional array.
 */
template<
	class Index_T>
void
yggdrasil_argNthElement(
	typename yggdrasil_valueArray_t<Index_T>::iterator 	indexArray_start,
	typename yggdrasil_valueArray_t<Index_T>::iterator 	indexArray_nth,
	typename yggdrasil_valueArray_t<Index_T>::iterator 	indexArray_end,
	const yggdrasil_dimensionArray_t&			dimArray)
{
	yggdrasil_assert(indexArray_start <= indexArray_end);
	yggdrasil_assert(indexArray_start <= indexArray_nth);
	yggdrasil_assert(indexArray_start == indexArray_end ? (indexArray_nth == indexArray_end) : (indexArray_nth < indexArray_end));

	//Test if we could index all
	const ::yggdrasil::Size_t maxPossibleIndexable = ::std::numeric_limits<Index_T>::max();
	if(maxPossibleIndexable <= dimArray.size())
	{
		throw YGGDRASIL_EXCEPT_illMethod("Can not address enough samples.");
	};

#ifdef NDEBUG
	//Get the underling data
	const yggdrasil_dimensionArray_t::Array_t& uDimArray = dimArray.internal_getInternalArray();
	const Numeric_t* const rawDimArray = uDimArray.data();
#endif

	//Now we sort
	::std::nth_element(
		indexArray_start, indexArray_nth, indexArray_end,
#ifndef NDEBUG
		[&dimArray](const Index_T lhs, const Index_T rhs) -> bool
		 {
			return (dimArray.at(lhs) < dimArray.at(rhs));
		 }
#else
		[&rawDimArray](const Index_T lhs, const Index_T rhs) -> bool
		 {
			return (rawDimArray[lhs] < rawDimArray[rhs]);
		 }
#endif
		); //End sorting command


#ifndef NDEBUG
	if(indexArray_end != indexArray_start)
	{
		yggdrasil_assert(indexArray_nth != indexArray_end);

		//load the index for the partitionin
		const Size_t partIndex = *indexArray_nth;

		//Load the associated value of in the dim array
		const Numeric_t partValue = dimArray.at(partIndex);
		if(indexArray_start < indexArray_nth)
		{
			for(auto it = indexArray_start; it != indexArray_nth; ++it)
			{
				//load index
				const Size_t idx_it = *it;

				//Load the value from the dimenioonal array
				const Numeric_t val_it = dimArray.at(idx_it);

				//Make the comparison
				yggdrasil_assert(val_it <= partValue);
			}; //End for(it)
		}; //End if: can compare the

		//Test the second range
		if(indexArray_nth < indexArray_end)
		{
			for(auto it = indexArray_nth + 1; it != indexArray_end; ++it)
			{
				//locad index
				const Size_t idx_it = *it;

				//Load the value
				const Numeric_t val_it = dimArray.at(idx_it);

				//Make the comparison
				yggdrasil_assert(partValue <= val_it);
			}; //End for(it):
		}; //End if: testing second range
	}; //End if: test
#endif
	return;
}; //End: nth_element


/**
 * \brief	This function performs a unique operation on a index array.
 *
 * This function requeres that the range that is pointed by the
 * range is allready sorted.
 * It will then apply the unique operation oin it and
 * returns te new end pointer.
 *
 * \param  dimArray		This is the dimensional array, that should be uniqued.
 * \param  indexArray_start	The start iterator of the index array.
 * \param  indeyArray_end	This is the pass the end iterator of the array-
 */
template<
	class Index_T>
typename yggdrasil_valueArray_t<Index_T>::iterator
yggdrasil_argUnique(
	const yggdrasil_dimensionArray_t&			dimArray,
	typename yggdrasil_valueArray_t<Index_T>::iterator	indexArray_start,
	typename yggdrasil_valueArray_t<Index_T>::iterator	indexArray_end)
{
	//Test if we could index all
	const ::yggdrasil::Size_t maxPossibleIndexable = ::std::numeric_limits<Index_T>::max();
	if(maxPossibleIndexable <= dimArray.size())
	{
		throw YGGDRASIL_EXCEPT_illMethod("Can not address enough samples.");
	};

	//Check if the input is sorted
	yggdrasil_assert(::std::is_sorted(indexArray_start, indexArray_end,
		[&dimArray](const Index_T lhs, const Index_T rhs) -> bool
		  { return (dimArray.at(lhs) < dimArray.at(rhs)); }
		  ));


#ifdef NDEBUG
	const yggdrasil_dimensionArray_t::Array_t& uDimArray = dimArray.internal_getInternalArray();
	const Numeric_t* const rawDimArray = uDimArray.data();
#endif

	const typename yggdrasil_valueArray_t<Index_T>::iterator
	  newLast = ::std::unique(
		indexArray_start, indexArray_end,
#ifndef NDEBUG
		[&dimArray](const Index_T lhs, const Index_T rhs) -> bool
		  {
		  	return (dimArray.at(lhs) == dimArray.at(rhs)) ? true : false;
		  } //End LAMBDA Debug
#else
		[&rawDimArray](const Index_T lhs, const Index_T rhs) -> bool
		  {
		  	return (rawDimArray[lhs] == rawDimArray[rhs]) ? true : false;
		  } //End LAMBDA Release
#endif
		); //End unique

	return newLast;
}; //End: argUnique


/**
 * \brief	This is a fucntion that does the partitioning.
 *
 * It is basically std::partition, but a bit optimized for our
 * case. Since it does not requiere to take a predicate.
 * It just takes the dimensional array and the partitioning point.
 *
 * \param  idxFirst	The beginning of the range,
 * \param  idxLast	The past the end of the index range.
 * \param  dimArray	The dimensional array.
 * \param  partPoint	The partition value.
 *
 * \note	This fuction is inspiered by the GNU implememtation.
 */

template<
	class Iterator_t
	>
Iterator_t
yggdrasil_argPartition(
	Iterator_t 				idxFirst,
	Iterator_t 				idxLast,
	const yggdrasil_dimensionArray_t&	dimArray,
	const Numeric_t 			partPoint)
{
	yggdrasil_assert(idxFirst <= idxLast);
#ifdef NDEBUG
	//Get the underling data
	const yggdrasil_dimensionArray_t::Array_t& uDimArray = dimArray.internal_getInternalArray();
	const Numeric_t* const rawDimArray = uDimArray.data();
#endif
	while(true)
	{
		//Going form the beginning to find the
		//point where we have to swap
		while(true)
		{
			if(idxFirst == idxLast)
			{
				return idxFirst;
			}
			else
			{
				//Load the value at that position
#ifndef NDEBUG
				yggdrasil_assert(idxFirst < idxLast);
				const Numeric_t currValue = dimArray.at(*idxFirst);
#else
				const Numeric_t currValue = rawDimArray[*idxFirst];
#endif
				if(currValue < partPoint)
				{
					++idxFirst;
				}
				else
				{
					break;
				};
			};
		}; //End while: coming from bellow

		yggdrasil_assert(idxFirst < idxLast);


		--idxLast;

		while(true)
		{
			if(idxFirst == idxLast)
			{
				return idxFirst;
			}
			else
			{
#ifndef NDEBUG
				const Numeric_t currValue = dimArray.at(*idxLast);
#else
				const Numeric_t currValue = rawDimArray[*idxLast];
#endif
				if(partPoint <= currValue)
				{
					--idxLast;
				}
				else
				{
					break;
				};
			};
		}; //End while: coming from above

		//Swap the two, this is allmost the efficient one we can do
		const auto tmp = *idxFirst;
		*idxFirst = *idxLast;
		*idxLast = tmp;

		//Increment to get the next step
		++idxFirst;
	}; //ENd while: global

	throw YGGDRASIL_EXCEPT_illMethod("Reached unreachable code.");
}; //End: arg parting





YGGDRASIL_NS_END(yggdrasil)

