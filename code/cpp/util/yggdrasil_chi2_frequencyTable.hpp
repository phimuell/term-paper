#pragma once
/**
 * \brief	This class implements a frequency table. This is needed for the independence test.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_generalBinEdges.hpp>
#include <util/yggdrasil_partitionedBinEdges.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sample.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>



//Incluide std
#include <memory>
#include <vector>
#include <numeric>
#include <algorithm>

//Include boost


YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \class 	yggdrasil_chi2FreqTable_t
 * \brief	This class implements a frquency table.
 *
 * A frequency table is needed for the independence tests.
 * This class basically mimiks the behaviour of the R code:
 * 	table(i1, i2)
 * That is used inside the original R code.
 *
 * It also has the same restriction as the yggdrasil_binEdgeArray_t ha
 * meaning.
 * It does not define the Size_t new, but thalkes the one from the edge array.
 *
 * This class also uses the late binding association that the other
 * parts of the test suits have.
 *
 * Such an instance is owned by each pair of dimensional interaction.
 *
 * The frequency table is stored in row major order, the same way as it is in C.
 * Thsi class will bound to two dimensions.
 * The number of bins of the first  dimension is used as the number of rows.
 * The number of bins of the second dimension is used as the number of columns.
 *
 * This class supports many ways of creating the table.
 * It supports a very inefficient ways that just finds the correct bin.
 * It also supports a more elegant way if a partitioning is aviable.
 * It also supports a sort based ways that is considered second best.
 * This methoif is based on the observation that one can successifely bin
 * the samples.
 *
 * If the partitioning is aviable it will be selected automatically.
 * If now partitioning is aviable, the user can select the sorting method
 * or the normal method.
 *
 * The bin edge need to be provided by external sources.
 *
 * \note	See also "http://www.ltcconline.net/greenl/courses/201/regression/chi2.htm"
 */
class yggdrasil_chi2FreqTable_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using HyperCube_t 	= yggdrasil_hyperCube_t;	//!< This is the type that represents a domain.
	using Numeric_t 	= ::yggdrasil::Numeric_t;	//!< This is the underling numeric type.
	using Real_t 		= ::yggdrasil::Real_t;		//!< This is the high precission type.
	using Sample_t 		= yggdrasil_sample_t;		//!< This is the type of a single sample.
	using DimArray_t 	= yggdrasil_dimensionArray_t;	//!< This is the type of the diomensional array.
	using IntervalBound_t 	= HyperCube_t::IntervalBound_t;	//!< This is the bound for oneinterval.
	using ParModel_t 	= yggdrasil_parametricModel_i;	//!< This is the parametric model, that is used.
	using cParModel_ptr 	= const ParModel_t*;		//!< This is a constant pointer to the parameteric model.

	/**
	 * \brief	Thye of the indexing while sorting.
	 *
	 * This type is used for the fast, but memory intense
	 * binning operation. It is 32Bit wide.
	 * Menaing that it can treat only ~4 Bilon samples.
	 * You can change that if a problem arrises.
	 */
	using SortIndex_t 	= ::yggdrasil::SortIndex_t;

	/*
	 * The typedefes are derived from the edges
	 */
	using BinEdgesArray_t   = yggdrasil_partionedBinEdges_t;	//!< This is the class that is used for the bin edges
	using IdxRange_t 	= BinEdgesArray_t::IdxRange_t;		//!< This is a range type.
	using IdxItRange_t 	= BinEdgesArray_t::IdxItRange_t;	//!< This is a range type of iterators.
	using Size_t 		= yggdrasil::Size_t;			//!< This is the type for counting and indexing.
	using IndexArray_t 	= ::std::vector<SortIndex_t>;   //!< This is the type for the index array.
	using IntFreqTable_t 	= yggdrasil_valueArray_t<Size_t>;       //!< This is the internal class that stores the frequency table.
	using MarginalTable_t   = yggdrasil_valueArray_t<Size_t>;	//!< This is the class for the marginals.


	/*
	 * =================
	 * Constructors
	 *
	 * All constructers are defaulted.
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * The building constructor is also the default
	 * constructor. It constructs an empty frequency
	 * table, that is not associated to any data.
	 * This follows the spirit of the rest of the
	 * classes that are implemented for that.
	 */
	yggdrasil_chi2FreqTable_t()
	 :
	  m_freqTable(),
	  m_nRowsFirst(-1),
	  m_nColsSecond(-1),
	  m_grandTotal(-1)
	{
	}; //End: building constructor


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_chi2FreqTable_t(
		const yggdrasil_chi2FreqTable_t&)
	 = default;

	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_chi2FreqTable_t(
		yggdrasil_chi2FreqTable_t&&)
	 = default;

	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_chi2FreqTable_t&
	operator= (
		const yggdrasil_chi2FreqTable_t&)
	 = default;

	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted
	 */
	yggdrasil_chi2FreqTable_t&
	operator= (
		yggdrasil_chi2FreqTable_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_chi2FreqTable_t()
	 = default;


	/*
	 * ================
	 * Utility Functions
	 */
public:
	/**
	 * \brief	Return the number of bins the first dimension has.
	 *
	 * This is also the number of rows that the frequency table has.
	 *
	 * \throw 	if \c *this is not associated to any data.
	 */
	Size_t
	nBinsFirst()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data has an undefined numbers of bins.");
		};

		return m_nRowsFirst;
	};


	/**
	 * \brief	Return the number of bins the second dimension has.
	 *
	 * This is also the number of columns that the frequency table has.
	 *
	 * \throw 	if \c *this is not associated to any data.
	 */
	Size_t
	nBinsSecond()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data has an undefined numbers of bins.");
		};

		return m_nColsSecond;
	};


	/**
	 * \brief	This function returns true if \c *this is associated to a domain.
	 *
	 * An object is associated with data if the frequency table is not empty.
	 */
	bool
	isAssociated()
	 const
	 noexcept
	{
		return (m_freqTable.empty() == false) ? true : false;
	};


	/*
	 * ===================
	 * Accessing function
	 */


	/**
	 * \brief	This function returns the row marginals of \c *this.
	 *
	 * The row marginal is the sum of the different colums.
	 * In mathematical terms the row marginal of the $i$ th row,
	 * denoted as $r_{i}$, is given as:
	 * 	r_{i} := \sum_{j = 1}^{nCols} F_{i, j}
	 * This means we have as many row marginals as we have rows.
	 * This function does compute all at once thus returning a vector.
	 *
	 * \throw	If \c *this is not associated to data.
	 *
	 * \note 	In the above description we have started with 1.
	 * 		 However the actual implementation starts with 0.
	 */
	MarginalTable_t
	getRowMarginals()
	 const;


	/**
	 * \brief	This function computes the column marginals of \c *this.
	 *
	 * The colum marginal is defined as the sum of all rows in a single column.
	 * This means we have as many marginals as we have columns.
	 * In mathematical the column marginal of the $j$ th column,
	 * denoted as $c_j$, is defined as:
	 * 	c_{j} := \sum_{i = 1}^{nRows} F_{i, j}
	 * This function returns a vector, since all marginals are comupted at once.
	 * The vector has a lengthequal to the numbers of columns.
	 *
	 * \note 	In the above description we have started with 1.
	 * 		 However the actual implementation starts with 0.
	 */
	MarginalTable_t
	getColMarginals()
	 const;


	/**
	 * \brief	This function Returns the number of samples that are recorded.
	 *
	 * This function basically first computes a marginal and
	 * then sums up the marginal, to optain the grand total.
	 * Hower this function is more efficient.
	 *
	 * \throw 	If *this is not associated to any data.
	 */
	Size_t
	getTotalObs()
	 const;


	/**
	 * \brief	This function returns the frequency of entry (i, j) from the table.
	 *
	 * This function allows accessing the frequency table.
	 * It returns the number of the samples that lies in the ith bin of the
	 * first dimension and at the same time also lies in the jth bin
	 * of the second dimension.
	 *
	 * \param  i	The bin index of the first dimension.
	 * \param  j	The bin index of the second dimension.
	 *
	 * \note	The indexing starts at zero.
	 *
	 * \throw	If *this is not associated to any data.
	 * 		 Or of out of bound is detected.
	 */
	Size_t
	getFrequency(
		const Size_t 		i,
		const Size_t 		j)
	 const;


	/*
	 * =================
	 * Statistical Functions
	 */
public:
	/**
	 * \brief	This function associates \c *this to the presented data.
	 *
	 * After this function ebnded a frequency table will be created.
	 * Each entry (i, j), in the under linying matrix will have the
	 * meaning that we obered that many samples with a first coordinate
	 * that falls in bin i rom the first dimension and in bin j from
	 * the second dimension.
	 *
	 * Thsi function does not compute the bins by itself, but must be provided.
	 * The bin functions may or may not apply unique to find better bins.
	 * This function will never apply unique.
	 * This function will allocate temporary space and perform a sort operation
	 * on them.
	 *
	 * \param  dimArray1		This is the dimensional array of the first dimension.
	 * \param  binEdges1 		This are the edges of the first dimension.
	 * \param  dimArray2 		This is the dimensional array of the second dimension.
	 * \param  binEdges2		This are the edges of the second dimension.
	 * \param  useSotring		Use the sorting version.
	 *
	 * \note 	No check is performed if the sampels lie in the full interval or not.
	 * 		 As  other parts of teh code a saturating method is used.
	 *
	 * \throw 	If this is allread accocoated wioth
	 */
	void
	buildInitialFreqTable(
		const DimArray_t& 		dimArray1,
		const BinEdgesArray_t& 		binEdges1,
		const DimArray_t& 		dimArray2,
		const BinEdgesArray_t& 		binEdges2,
		const bool 			useSorting);


	/**
	 * \brief	This function calculates the Chi2 value.
	 *
	 * This is the test statistic value, that is then needed to evaluate te test.
	 * This function computes basically:
	 * 	\frac{(observed - expected)^2}{expected}
	 * Expected is given by the value of the two marginals divided by the
	 * total numbers of observations inside *this.
	 *
	 * It is more efficient to use this function than to do it
	 * at a higher level, since some of the checks can be bypassed.
	 *
	 * \throw	If *this is not associated to anything.
	 *
	 * \note:	See also http://www.ltcconline.net/greenl/courses/201/regression/chi2.htm
	 */
	Real_t
	computeChi2Value()
	 const;

	/**
	 * \brief	Returns the number of degrees of freedom of this table.
	 *
	 * Notice that this function can return 0 (zero).
	 * Thsi means that at least one of the dimension has only one bin.
	 *
	 * This value is generatly (nRow - 1) * (nCol - 1).
	 */
	Real_t
	getDOF()
	 const;


	/*
	 * ===================
	 * Managing Functions
	 */
public:
	/**
	 * \brief	This function allows to unbound *this from data.
	 *
	 * This function does tries to dealocate memory by using the
	 * swap trick.
	 * It is only valid to call if *this is accosiated to data.
	 *
	 * This function is intended for internal use only.
	 */
	void
	unboundData();




	/*
	 * ================
	 * Private functions
	 */
private:

	/**
	 * \brief	This is the std fill up function.
	 *
	 * This fucntion does not use additionaol memeory.
	 */
	bool
	private_buildFreqTable_std(
		const DimArray_t& 		dimArray1,
		const BinEdgesArray_t& 		binEdges1,
		const DimArray_t& 		dimArray2,
		const BinEdgesArray_t& 		binEdges2);


	/**
	 * \brief	This function uses a sorting scheme, on one dimension.
	 *
	 * It needs additional memeory
	 */
	bool
	private_buildFreqTable_sort(
		const DimArray_t& 		dimArray1,
		const BinEdgesArray_t& 		binEdges1,
		const DimArray_t& 		dimArray2,
		const BinEdgesArray_t& 		binEdges2);


	/**
	 * \brief	This is the version that uses the partitioning.
	 */
	bool
	private_buildFreqTable_partion(
		const DimArray_t& 		dimArray1,
		const BinEdgesArray_t& 		binEdges1,
		const DimArray_t& 		dimArray2,
		const BinEdgesArray_t& 		binEdges2);


	/**
	 * \brief	THis function is present for handling the special cases.
	 *
	 * This is like there only one bin.
	 * This function does not check if the data lies in the spanned interval
	 *
	 * Its return values are:
	 * 	> -1 (minus one): There was an error while the handling
	 * 	>  0 (zero):      The special case was handled and no further actions needed to be done
	 * 	>  1 (plus one):  There was no special case, must use the default case.
	 */
	int
	private_buildFreqTable_specialCase(
		const DimArray_t& 		dimArray1,
		const BinEdgesArray_t& 		binEdges1,
		const DimArray_t& 		dimArray2,
		const BinEdgesArray_t& 		binEdges2);




	/*
	 * =================
	 * Private Members
	 */
private:
	IntFreqTable_t 		m_freqTable;	//!< This is the frequency table that stores the counts.
						//!<  It is used in row major ordering.
	Size_t 			m_nRowsFirst;	//!< This is the number of rows we have.
						//!<  It is equal the number of bins the first dimension has.
	Size_t 			m_nColsSecond;	//!< This is the number of columns that we have.
						//!<  It is equal the number of bins the secons dimension
	Size_t 			m_grandTotal;	//!< This is the total number of samples that where observed.
}; //End class: yggdrasil_chi2FreqTable_t



/**
 * \brief	This is a helper function that counts how many
 * 		 sampels are in the two ranges
 *
 * The range must be sorted.
 *
 * \param  range1	The first range.
 * \param  range2	The second range.
 *
 * \note 	This code builds on the ideas form std::set_intersection.
 * 		 The GNU implementation was used as an inspiration.
 */
extern
Size_t
ygInternal_countSameElements(
	const yggdrasil_chi2FreqTable_t::IdxItRange_t 	range1,
	const yggdrasil_chi2FreqTable_t::IdxItRange_t 	range2);






YGGDRASIL_NS_END(yggdrasil)
