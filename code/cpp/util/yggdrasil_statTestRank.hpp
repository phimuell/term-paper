#pragma once
/**
 * \brief	This file implements the test rank for a fuill series of tests.
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitSequence.hpp>

#include <util/yggdrasil_statSingleTestResults.hpp>


//Incluide std
#include <memory>
#include <vector>
#include <algorithm>


YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the node
class yggdrasil_DETNode_t;

/**
 * \class 	yggdrasil_statTestRank_t
 * \brief	This class represents the collection of all possible tests.
 *
 * It is basically a wrapper arround a vector of type yggdrasil_statSingleTestResult_t.
 * It allows to sort them it can then be used to generate the split sequence.
 *
 * At the beginning one has to set it up.
 *
 * Some function are designed to be thread safe, some like the setup functions
 * are not. In order to safely use this class to hold the result it must be
 * first set up.
 *
 * \tparam  Key_T	This is the type of key that is used.
 */
template<
	class Key_T>
class yggdrasil_statTestRank_t
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using Key_t 		= Key_T;			//!< This is the type of the key.
	using Numeric_t 	= ::yggdrasil::Numeric_t;	//!< This is the type of numerics.
	using SingleTestRes_t 	= ::yggdrasil::yggdrasil_statSingleTestResult_t<Key_t>;	//!< This is a single test result
	using TestResultVec_t 	= ::std::vector<SingleTestRes_t>;	//!< This is the collection of the test results
	using KeyList_t 	= ::std::vector<Key_t>;			//!< This is a list of all the keys that are managed by *this
	using iterator 		= typename TestResultVec_t::const_iterator;	//!< This si the iterator, we only support const version
	using const_iterator 	= typename TestResultVec_t::const_iterator;


	/*
	 * =================================
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * The building constructor takes the number of
	 * the different tests that should be stored inside *this.
	 *
	 * This constructor only allocates memory, but does not yet
	 * create the test result, this has to be done in a second step.
	 *
	 * The keys kan be made known by calling the function addTestInstance(Key_t).
	 *
	 * \param  nDiffTests 	This is the number of the different tests that are
	 * 			 to be stored inside *this.
	 *
	 * \throws	Some basic consistency checks are performed.
	 */
	yggdrasil_statTestRank_t(
		const Int_t 	nDiffTests)
	 :
	  m_results(),
	  m_nDiffTests(nDiffTests),
	  m_nNonTrivTest(0),
	  m_isSorted(false)
	{
		//We must now also guard ourself agains negative values
		if(nDiffTests < 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The number of managed tests is negative.");
		};

		/*
		 * Previously the reservation was done here, but this
		 * has changed since we want to save some storage.
		 *
		 * It is now done in the addInstance fucntion, upon its first call.
		 */
#if 0
		//Reserve storage
		m_results.reserve(m_nDiffTests);
#endif
	}; //End: building constructor



	/**
	 * \brief	Late Bunding constructor.
	 *
	 * This is the late bunding constructor.
	 * I sused for the case if the numbers of tests
	 * is not knwon upon constructor.
	 *
	 * It is impossible to use *this until it was
	 * properly set.
	 */
	yggdrasil_statTestRank_t()
	 :
	  m_results(),
	  m_nDiffTests(-1),
	  m_nNonTrivTest(0),
	  m_isSorted(false)
	{};


	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_statTestRank_t(
		const yggdrasil_statTestRank_t&)
	 = default;


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_statTestRank_t(
		yggdrasil_statTestRank_t&&)
	 = default;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_statTestRank_t&
	operator= (
		const yggdrasil_statTestRank_t&)
	 = default;

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_statTestRank_t&
	operator= (
		yggdrasil_statTestRank_t&&)
	 = default;


	/*
	 * =====================
	 * Normal functions
	 */
public:
	/**
	 * \brief	The late buinding function.
	 *
	 * This function is used to execute the postponed constructor.
	 * If the effect is the same as if *this was called with the
	 * argument of thsi function to the constructor.
	 *
	 * \param  nDiffTests		The number of different tests, that
	 * 				 are managed by this.
	 *
	 * \throw	If *thsi is allready bound.
	 */
	void
	lateBind(
		const Int_t 	nDiffTests)
	{
		yggdrasil_assert(m_nDiffTests == -1);
		if(m_nDiffTests != -1)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The test rank was allready bound.");
		};

		//Set the number of tests
		m_nDiffTests = nDiffTests;

		return;
	}; //ENd: lateBind


	/**
	 * \brief	Returns true if \c *thsi si valid.
	 *
	 * *this si valid, if it is complete and no of the single tests is invalid.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		if(this->isComplete() == false)
		{
			return false;
		};
		for(const SingleTestRes_t& s : m_results)
		{
			if(s.isValid() == false)
			{
				return false;
			};
		}; //End for(s)

		return true;
	}; //End: isValid


	/**
	 * \brief	This function returns true if all tests are present and valid.
	 *
	 * This fucntion is basically an alias of the isValid() function, but with a name that
	 * reflects this a bit better.
	 */
	bool
	allTestsDone()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		if(this->isComplete() == false)
		{
			return false;
		};
		for(const SingleTestRes_t& s : m_results)
		{
			if(s.isTestDone() == false)
			{
				return false;
			};
		}; //End for(s)

		return true;
	}; //End: allTestDone


	/**
	 * \brief	This fucntion returns \c true if a specific test is done.
	 *
	 * This function allows to query if a specific test is done.
	 * This is a bit more elegant than searching for the key and then
	 * query the result object.
	 *
	 * \param 	key	The key to test.
	 *
	 * \throw	If *this is not complete or the key is not found.
	 */
	bool
	isSpecificTestDone(
		const Key_t&		key)
	 const
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		//Searching for
		if(this->isComplete() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to access a non complete object.");
		};

		const auto f = std::find_if(
			m_results.begin(), m_results.end(),
			[&key](const SingleTestRes_t& x) -> bool { return (x.key() == key);});

		if(f == m_results.cend())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The key was not found.");
		};

		//Return the result
		return (f->isTestDone() == true) ? true : false;
	}; //End: isSpecificKeyDone


	/**
	 * \brief	This function tests if the test identified by
	 * 		 key, is a trivial test.
	 *
	 * \param 	key	The key to test.
	 *
	 * \throw	If *this is not complete or the key is not found.
	 */
	bool
	isTrivialTest(
		const Key_t&	key)
	{
		yggdrasil_assert(m_nDiffTests >= 0);
		yggdrasil_assert(this->isComplete());

		return this->getSingleTest(key).isTrivialTest();
	}; //End: is a test trivial



	/**
	 * \brief	This function retuirns true if the result are sorted.
	 *
	 * \c *this is sorted if the result are sorted according to the operator<
	 * defined on the single results.
	 *
	 * there is an assert that checks if *this is complete.
	 */
	bool
	isSorted()
	 const
	 noexcept
	{
		yggdrasil_assert(this->isComplete() == true);
		yggdrasil_assert(m_nDiffTests >= 0);

		return m_isSorted;
	};


	/*
	 * ==================
	 * Result managing
	 */
public:
	/**
	 * \brief	Tis function sorts the individual results.
	 *
	 * For sorting the operator< is used.
	 * This function will reorder the interbal structure.
	 *
	 * Thsi function is not thread safe.
	 */
	void
	sortResult()
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		if(this->allTestsDone() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can only sort a valid object.");
		};

		//It is also not possible to sort again
		if(this->isSorted() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to sort a sorted rank again.");
		};

		//Case distinction, between zero and more tests
		if(m_nDiffTests == 0)
		{
			//None test, so we are now by default sorted
			m_isSorted = true;
		}
		else
		{
			//More than zero tests

			//Sort the results, use stable sort, maybe not needed but I
			//like it from a conceptional point of view
			std::stable_sort(m_results.begin(), m_results.end());
			m_isSorted = true;

			yggdrasil_assert(::std::is_sorted(m_results.cbegin(), m_results.cend(),
				[](const SingleTestRes_t& lhs, const SingleTestRes_t& rhs) -> bool { return (lhs < rhs);}
				) == true);

		}; //End: else: Mode than zero tests

		return;
	}; //End: sortResults


	/**
	 * \brief	This function clears \c *this.
	 *
	 * A clearing means that all individual are reconstructed.
	 * For that the buildiongconstructor whih only takes the key is used.
	 *
	 * If a test was trivial, it will be reseted to not trivial.
	 */
	void
	resetResults()
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		//We requiert that we are complet
		if(this->isComplete() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to reset a not complete rank.");
		};

		const Size_t N = m_results.size();
		for(Size_t i = 0; i != N; ++i)
		{
			//Read the key
			const Key_t thisKey = m_results[i].key();

			//Reconstruct the object
			m_results[i] = SingleTestRes_t(thisKey);

			yggdrasil_assert(m_results[i].isValid() == false);
			yggdrasil_assert(m_results[i].key() == thisKey);
		}; //End for(i):

		//Set the sorte state back
		m_isSorted = false;

		return;
	}; //End: resetResults


	/**
	 * \brief	This function adds a test instance.
	 *
	 * What it realy does is that it informs \c *this, that
	 * a test that is identified by \c key should be created.
	 *
	 * \param  key		The identifier of the key.
	 *
	 * \throw 	If the key is allready known or if the size has been reached.
	 */
	void
	addTestInstance(
		const Key_t&	key)
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		//Test if the capacity of the vector is zereo
		//in that case reversse storage. This was previously done in the constructor
		if(m_results.capacity() == 0)
		{
			//We have to reserve some storrage
			m_results.reserve(m_nDiffTests);
		}; //If: Capacity is zereo, so reverse

		if(this->m_results.size() >= m_nDiffTests)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The container was only supposed to host " + std::to_string(m_nDiffTests) + ", but tried to add one.");
		};
		yggdrasil_assert(this->isComplete() == false);

		const Size_t nOccurence = ::std::count_if(
			m_results.cbegin(), m_results.cend(),
			[&key](const SingleTestRes_t& s) -> bool {return (s.key() == key);}
			);
		if(nOccurence != 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to add a key that was already known.");
		};

		//Now we can add the key
		m_results.emplace_back(key);

		//Increase the isze of the non trivial tests
		m_nNonTrivTest += 1;

		return;
	}; //End add test instance


	/**
	 * \brief	This function adds a trivial test instance.
	 *
	 * What it realy does is that it informs \c *this, that
	 * a test that is identified by \c key should be created.
	 * At this test is trivial.
	 * This is the only way, up to now, to insert a trivial test.
	 *
	 * \param  key		The identifier of the key.
	 *
	 * \throw 	If the key is allready known or if the size has been reached.
	 */
	void
	addTrivialTestInstance(
		const Key_t&	key)
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		//Test if the capacity of the vector is zereo
		//in that case reversse storage. This was previously done in the constructor
		if(m_results.capacity() == 0)
		{
			//We have to reserve some storrage
			m_results.reserve(m_nDiffTests);
		}; //If: Capacity is zereo, so reverse

		if(this->m_results.size() >= m_nDiffTests)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The container was only supposed to host " + std::to_string(m_nDiffTests) + ", but tried to add one.");
		};
		yggdrasil_assert(this->isComplete() == false);

		const Size_t nOccurence = ::std::count_if(
			m_results.cbegin(), m_results.cend(),
			[&key](const SingleTestRes_t& s) -> bool {return (s.key() == key);}
			);
		if(nOccurence != 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to add a key that was already known.");
		};

		//Now we can add the key
		m_results.emplace_back(key);

		yggdrasil_assert(m_results.back().key() == key);
		m_results.back().makeTrivial(true);
		yggdrasil_assert(m_results.back().isTrivialTest());

		return;
	}; //End add test instance



	/*
	 * ===================
	 * Access fucntions
	 */
public:
	/**
	 * \brief	Retuirns an iterator to the first element of the test results
	 *
	 * The order in which the tests are accessed is unspecific.
	 * And it can varii.
	 *
	 * The iterator allows only read access.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		return m_results.cbegin();
	};


	/**
	 * \brief	This si the iterator that points to the past the end elements.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		return m_results.cend();
	};


	/**
	 * \brief	This function sets the value of the result key to the provided one.
	 *
	 * This is like storing the result inside *this.
	 *
	 * \param  pValue	The pValue of the test.
	 * \param  sizePart 	This is the size of the key.
	 *
	 * \throw	If this the target is not invalid.
	 */
	void
	storeTestResult(
		const Key_t& 		key,
		const Numeric_t 	pValue,
		const Numeric_t 	sizePart)
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		if(this->isSorted() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not mess with a sorted rank.");
		};

		//Searching for
		if(this->isComplete() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to access a non complete object.");
		};

		const auto f = std::find_if(
			m_results.begin(), m_results.end(),
			[&key](const SingleTestRes_t& x) -> bool { return (x.key() == key);});

		if(f == m_results.cend())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The key was not found.");
		};

		if(f->isTrivialTest() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to write to a trivial test.");
		};
		if(f->isValid() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The result of the kex is valid.");
		};

		//Now we set a new one
		*f = SingleTestRes_t(key, pValue, sizePart);

		return;
	}; //End: store result


	/**
	 * \brief	This function returns a constant reference to the rank i test.
	 *
	 * In the case \c *thsi si sorted, one can now access the elements according
	 * to their pValue, worse first.
	 *
	 * \param  i	The test to access.
	 *
	 * \throw	If \c *thsi si not norted.
	 */
	const SingleTestRes_t&
	getRank(
		const uInt_t 	i)
	 const
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		if(this->isComplete() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to access a non complete object.");
		};
		if(this->isSorted() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Rank can only be accessed if *this is sorrted.");
		};

		return m_results.at(i);
	}; //End getRank


	/**
	 * \brief	Thsi function returns a constant reference to the test with key.
	 *
	 * \throw	If \c *thsi si not complete.
	 */
	const SingleTestRes_t&
	getSingleTest(
		const Key_t& 		key)
	 const
	{
		yggdrasil_assert(m_nDiffTests >= 0);

		if(this->isComplete() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to access a non complete object.");
		};

		const auto f = std::find_if(
			m_results.cbegin(), m_results.cend(),
			[&key](const SingleTestRes_t& x) -> bool { return (x.key() == key);});

		if(f == m_results.cend())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The key was not found.");
		};

		return (*f);
	}; //End: getSingleTest



	/*
	 * =======================
	 * Utility functions
	 */
public:
	/**
	 * \brief	This function returns a list (vector)
	 * 		 that contains all the keys that are
	 * 		 stored inside *this and are not trivial.
	 *
	 * It is importannt that only the keys that are non
	 * trivial are added to that list.
	 * If you need all the key use the getAllKeys() functunction.
	 *
	 * \throw 	*this must be conmplete.
	 *
	 * \note	The behaviour of that function was noce changed.
	 * 		 Earlier it returned all the keys, and not just
	 * 		 the ones that are non trivial
	 */
	KeyList_t
	getKeyList()
	 const
	{
		if(this->isComplete() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The list of tests is not complete.");
		};

		//Create the variable
		KeyList_t keyList;

		//The key might not be default constructable, so we must use the pop_back method
		//and preallocate some space
		keyList.reserve(m_nDiffTests);

		//Insert the keys
		for(const SingleTestRes_t& res : m_results)
		{
			if(res.isTrivialTest() == false)
			{
				keyList.push_back(res.key());
			};
		}; //End for(res)

		yggdrasil_assert(keyList.size() == this->nDifferentTests());

		//Return
		return keyList;
	}; //End: getKeyList



	/**
	 * \brief	This function returns the list of all stored
	 * 		 keys inside *this.
	 *
	 * This realy returns all keys inside that are located inside *this.
	 *
	 * \throw	If *this is not complete.
	 */
	KeyList_t
	getAllKey()
	 const
	{
		if(this->isComplete() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The list of tests is not complete.");
		};

		//Create the variable
		KeyList_t keyList;

		//The key might not be default constructable, so we must use the pop_back method
		//and preallocate some space
		keyList.reserve(m_nDiffTests);

		//Insert the keys
		for(const SingleTestRes_t& res : m_results)
		{
			keyList.push_back(res.key());
		}; //End for(res)

		yggdrasil_assert(keyList.size() == this->nAllTests());

		//Return
		return keyList;
	}; //End: getKeyList









	/*
	 * =======================
	 * Counting information
	 */
public:
	/**
	 * \brief	This fucntion returns a the number of known tests.
	 *
	 * This is the number of test that where added by the addTestInstance fucntion
	 */
	Size_t
	nKnownTests()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDiffTests >= 0);
		return m_results.size();
	};


	/**
	 * \brief	This function returns the number of non trivial tests,
	 * 		 *this are supposed to known.
	 *
	 * This does not return the total number of tests that are inside *this,
	 * but only the non trivial ones.
	 *
	 * YOu can get the total number of test, by calling the nAllTests()
	 *
	 * \note	This fucntion was changed to return the non trivial
	 * 		 ones, this was done in order to keep the consitence
	 * 		 with the getTestKeys() function.
	 */
	Size_t
	nDifferentTests()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDiffTests >= 0);
		return m_nNonTrivTest;
	};


	/**
	 * \brief	This returns the total number of tests.
	 *
	 * This are also the trivial ones.
	 * It is basically the size of the list that is returned
	 * by the getAllTests() function.
	 */
	Size_t
	nAllTests()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDiffTests >= 0);
		return m_nDiffTests;
	};



	/**
	 * \brief	This function returns true if \c *this is complete.
	 *
	 * An instance is complete if the number of added tests is teh number of tests that was expected.
	 */
	bool
	isComplete()
	 const
	 noexcept
	{
		yggdrasil_assert(m_nDiffTests >= 0);
		return (m_nDiffTests == sSize_t(m_results.size()));
	};






	/*
	 * =========================
	 * Private Memebers
	 */
private:
	TestResultVec_t 		m_results;	//!< This vector stores all the results.
	Int_t				m_nDiffTests;	//!< The number of different tests.
	Int_t 				m_nNonTrivTest;	//!< This is the number of non trivial tests.
	bool 				m_isSorted;	//!< Indicates if *this is sorted.
}; //End class: yggthe test rank for a fuill series of tests.


YGGDRASIL_NS_END(yggdrasil)
