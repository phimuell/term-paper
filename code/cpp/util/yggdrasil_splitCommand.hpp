#pragma once
/**
 * \brief	This file implements a split command.
 *
 * This file defines a class that can be used to encode a single split.
 * It is basically a pair, but adds syntactic sugar.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_intervalBound.hpp>

//#include <samples/yggdrasil_sample.hpp>
//#include <samples/yggdrasil_sampleCollection.hpp>

//INcluide std

YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \brief	This class determines a single split
 * \class 	yggdrasil_singleSplit_t
 *
 * This is basically a pair of a Numeric_t and an integer, that encodes the
 * location and the dimension of a split to perform.
 * Its main intent is to clarifiy interfaces and return types.
 *
 * This class is inmutable, this means that once created *this can not be changed.
 * However it supports assignments, so one could simply create a new instance and
 * assigne it to the instance to change.
 *
 * It is also not possible to accidently create an invalid object.
 * Because the default constructor is private.
 * However there is a way to create an invalid instance, but this should be
 * used only by internal code.
 */
class yggdrasil_singleSplit_t
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using Numeric_t 	= ::yggdrasil::Numeric_t;	//!< This is the numeric type

	/*
	 * =================
	 * Constructors
	 *
	 * All are defaulted
	 */
public:
	/**
	 * \brief	Building constructor
	 *
	 * This builds a split at poistion splitPos in dimension splitDim.
	 *
	 * \param  splitPos 	This is the position to split
	 * \param  splitDim	This is the dimension where we want to split
	 *
	 * \throw 	If the arguments results in an invalid split.
	 */
	yggdrasil_singleSplit_t(
		const Numeric_t 	splitPos,
		const Int_t 		splitDim) :
	  m_splitPos(splitPos),
	  m_splitDim(splitDim)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The arguments for the split command resulted in an invalid object. They where " + std::to_string(splitPos) + " and " + std::to_string(splitDim));
		};
	};


	/**
	 * \brief	Building constructor 2.
	 *
	 * In order to avoid implicit casting.
	 * The constructor with the swaped arguments is deleted.
	 */
	yggdrasil_singleSplit_t(
		const Int_t 		splitDim,
		const Numeric_t 	splitPos)
	 = delete;



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleSplit_t(
		const yggdrasil_singleSplit_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleSplit_t(
		yggdrasil_singleSplit_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleSplit_t&
	operator= (
		const yggdrasil_singleSplit_t&) = default;


	/**
	 * \brief 	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleSplit_t&
	operator= (
		yggdrasil_singleSplit_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_singleSplit_t() = default;


	/**
	 * \brief	Only way to create an invalid split.
	 *
	 * This is the only way to create an invalid split.
	 * This functionality is only ment for internal use.
	 * The user should never use it.
	 *
	 */
	static
	yggdrasil_singleSplit_t
	CONSTRUCT_INVALID_SINGLE_SPLIT()
	{
		return yggdrasil_singleSplit_t();
	};



	/*
	 * ==================
	 * Private Constructors
	 */
private:
	/**
	 * \brief	Default constructor.
	 *
	 * Is private.
	 * And constructs an invalid object.
	 */
	yggdrasil_singleSplit_t()
	 noexcept :
	  m_splitPos(NAN),
	  m_splitDim(Constants::YG_INVALID_DIMENSION)
	{};



	/*
	 * =====================
	 * Functions
	 */
public:
	/**
	 * \brief	Returns true if *this is valid.
	 *
	 * Technically this function returns always true.
	 * The reason is that we only allow the construction
	 * of valid objects.
	 *
	 * It is used in the constructor to verify the construction
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		if(::std::isnan(m_splitPos) == true)
		{
			return false;
		};

		if(m_splitDim == Constants::YG_INVALID_DIMENSION)
		{
			return false;
		};

		if(m_splitDim < 0)
		{
			return false;
		};

		if(m_splitDim >= Constants::YG_MAX_DIMENSIONS)
		{
			return false;
		};

		return true;
	}; //End: isValid


	/**
	 * \brief	Returns the split dimension.
	 */
	Int_t
	getSplitDim()
	 const
	 noexcept
	{
		yggdrasil_assert(this->isValid());
		return m_splitDim;
	};


	/**
	 * \brief	Returns the split position.
	 */
	Numeric_t
	getSplitPos()
	 const
	 noexcept
	{
		yggdrasil_assert(this->isValid());
		return m_splitPos;
	};





	/*
	 * =================
	 * Private Memebers
	 */
private:
	Numeric_t 	m_splitPos;		//!< This is the position of the split
	Int_t 		m_splitDim;		//!< This is the dimension of the split
}; //End class: yggdrasil_singleSplit_t



YGGDRASIL_NS_END(yggdrasil)



