#pragma once
/**
 * \brief	This is an interval bound.
 *
 * It is basically a hypercube in one dimension, also known as interval.
 * It is used as building block for the multidimensional hypercube.
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>

#include <core/yggdrasil_valueArray.hpp>

#include <tree_geni/yggdrasil_dimensionCondition.hpp>

//Incluide std
#include <utility>
#include <cmath>
#include <vector>
#include <iostream>
#include <sstream>
#include <iterator>

YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \class 	yggdrasil_intervalBound_t
 * \brief	This class implements an one dimensional interval.
 *
 * It is basically a pair that is equiped with syntactic sugar.
 * It is important to node that the interval is not closed but half open.
 * The lower bound of \c *this lelogs to the interval, whereas the
 * upper bound of \c *this does \em not belongs to \c *this.
 * This means the interval is of the form \f$ x_l \leq x < x_u \f$.
 * Where \f$ x_u \f$ is the lower bound.
 *
 * \note	This interval is inmutable, but it supports overwriting, by assignment.
 * 		It primarly uses asserts instead of throws.
 * 		The reason is that it is intended as used by the hypercube and not the user.
 * 		So the hypercube should do all the testing.
 */
class yggdrasil_intervalBound_t
{
	/*
	 * ===================
	 * Typedef
	 */
public:
	using Numeric_t 	= ::yggdrasil::Numeric_t;				//!< The unerling numeric type
	using stdPair_t 	= ::std::pair<Numeric_t, Numeric_t>;			//!< This is the expresion as pair
	using ValueArray_t 	= ::yggdrasil::yggdrasil_valueArray_t<Numeric_t>;	//!< This is the type for the value array
	using SingleCondition_t = ::yggdrasil::yggdrasil_singleDimCondition_t;		//!< This represents one condition.

	/*
	 * ==============
	 * Constructors
	 *
	 * The default constructor is private.
	 * For creating an invalid interval, both bounds are NAN
	 * use the CREAT_IVALID_INTERVAL() function.
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * It takes lower and upper bound and constructs itself.
	 *
	 * \param  lowerBound		This is the lower bound.
	 * \param  upperBound 		This is the upper bound.
	 *
	 * \throw 	If one of them is NAN.
	 */
	yggdrasil_intervalBound_t(
		const Numeric_t 	lowerBound,
		const Numeric_t 	upperBound) :
	  m_lowerBound(lowerBound),
	  m_upperBound(upperBound)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("A non valid interval was tried to construct.");
		};
	};


	/**
	 * \brief	Construct an interval form a pair.
	 *
	 * \param  pair 	\c first is taken as lower and \c second
	 * 			 as upper bound of \c *this.
	 *
	 * \throw	Throws if one of the bounds is NAN.
	 */
	yggdrasil_intervalBound_t(
		const stdPair_t&	pair) :
	  m_lowerBound(pair.first),
	  m_upperBound(pair.second)
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("A non valid interval was tried to construct.");
		};
	};


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_intervalBound_t(
		const yggdrasil_intervalBound_t&) noexcept = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_intervalBound_t(
		yggdrasil_intervalBound_t&&) noexcept = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted
	 */
	yggdrasil_intervalBound_t&
	operator= (
		const yggdrasil_intervalBound_t&) noexcept = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_intervalBound_t&
	operator= (
		yggdrasil_intervalBound_t&&) noexcept = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_intervalBound_t() noexcept = default;


	/*
	 * ======================
	 * Functions
	 */
public:
	/**
	 * \brief	Test if \c x lies inside the interval represented by \c *this.
	 *
	 * \param  x 	A position to test.
	 */
	bool
	isInside(
		const Numeric_t&	x)
	  const
	  noexcept
	{
		yggdrasil_assert(this->isValid());

		return ((m_lowerBound <= x) && (x < m_upperBound)) ? true : false;
	}; //End: isInside


	/**
	 * \brief	This fucntion tests if *this meets the conditions described by condi.
	 *
	 * This fucntion is basically an alias for isInside().
	 * An important remark is that the dimension parameter of
	 * the condition is ignored. The reason is that *this does
	 * not know its dimension.
	 *
	 * \param  condi 	The condition that should be tested.
	 *
	 * \throw 	This fucntion never throws, but tests are dene with asserts.
	 */
	bool
	intersectWithCondition(
		const SingleCondition_t&	condi)
	 const
	 noexcept
	{
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(condi.isValid());

		//Get the dimension parameter
		const Numeric_t x = condi.getConditionValue();

		//Make the test and return it
		return ((m_lowerBound <= x) && (x < m_upperBound)) ? true : false;
	}; //End test the condition.




	/**
	 * \brief	This function tests if all values inside the value array lies inside this.
	 *
	 * This function is like the value for the numeric types, but for vectors.
	 * It is considered more efficient to use this function
	 *
	 * For some reasons it is not considered an error if the vector is empty.
	 * In that case the result is true.
	 *
	 * \param  X 	Vector to tests
	 */
	bool
	isInside(
		const ValueArray_t& 	X)
	 const
	{
		yggdrasil_assert(this->isValid() == true);

		//THe soize, number of elements, in the vector
		const Size_t N = X.size();

		if(N == 0)
		{
			return true;
		};

		const Size_t unrollFactor = 12;
		const Size_t leftOver     = N % unrollFactor;
		const Size_t unrollEnd    = N - leftOver;

		//This is a pointer to the nderling array
		const Numeric_t* const rawX = X.data();

		for(Size_t i = 0; i != unrollEnd; i += unrollFactor)
		{
			//Load the value
			const Numeric_t x_0 = rawX[i + 0];
			const Numeric_t x_1 = rawX[i + 1];
			const Numeric_t x_2 = rawX[i + 2];
			const Numeric_t x_3 = rawX[i + 3];
			const Numeric_t x_4 = rawX[i + 4];
			const Numeric_t x_5 = rawX[i + 5];
			const Numeric_t x_6 = rawX[i + 6];
			const Numeric_t x_7 = rawX[i + 7];
			const Numeric_t x_8 = rawX[i + 8];
			const Numeric_t x_9 = rawX[i + 9];
			const Numeric_t x_10 = rawX[i + 10];
			const Numeric_t x_11 = rawX[i + 11];

			if(!((m_lowerBound <= x_0) && (x_0 < m_upperBound))) {return false;};
			if(!((m_lowerBound <= x_1) && (x_1 < m_upperBound))) {return false;};
			if(!((m_lowerBound <= x_2) && (x_2 < m_upperBound))) {return false;};
			if(!((m_lowerBound <= x_3) && (x_3 < m_upperBound))) {return false;};
			if(!((m_lowerBound <= x_4) && (x_4 < m_upperBound))) {return false;};
			if(!((m_lowerBound <= x_5) && (x_5 < m_upperBound))) {return false;};
			if(!((m_lowerBound <= x_6) && (x_6 < m_upperBound))) {return false;};
			if(!((m_lowerBound <= x_7) && (x_7 < m_upperBound))) {return false;};
			if(!((m_lowerBound <= x_8) && (x_8 < m_upperBound))) {return false;};
			if(!((m_lowerBound <= x_9) && (x_9 < m_upperBound))) {return false;};
			if(!((m_lowerBound <= x_10) && (x_10 < m_upperBound))) {return false;};
			if(!((m_lowerBound <= x_11) && (x_11 < m_upperBound))) {return false;};
		}; //End for(i): itzerating over the array

		//We did not find any violation of the inside property
		//also we return true
		return true;
	}; //End: isINside


	/**
	 * \brief	Get the length of \c *this.
	 *
	 * The length is basically the upper bound minus the lower bound.
	 * If the length is NAN, this means that both are NAN,
	 * since we guard the construction, NAN is returned.
	 *
	 * This function is also allowed to return infinity
	 */
	Numeric_t
	getLength()
	  const
	  noexcept
	{
		yggdrasil_assert(this->isValid());

		/*
		 * We must do this extra step here and can not merge it with
		 * the maximum.
		 * The reason is that depending on the implementation of the
		 * std::max, we get zero or NAN.
		 */
		const Numeric_t Length = m_upperBound - m_lowerBound;
		yggdrasil_assert(std::isnan(Length) == false);

		// The max is here to premevt some negative length
		// If length is NAN then the comparison is false and we return NAN
		return (Length < 0.0) ? 0.0 : Length;
	};//End: Getlenth


	/**
	 * \brief	Get the length of \c *this.
	 *
	 * This function is like the normal get Length function, but operates
	 * on ::yggdrasil::Real_t
	 */
	Real_t
	getLengthReal()
	  const
	  noexcept
	{
		yggdrasil_assert(this->isValid());

		/*
		 * We must do this extra step here and can not merge it with
		 * the maximum.
		 * The reason is that depending on the implementation of the
		 * std::max, we get zero or NAN.
		 */
		const Real_t Length = Real_t(m_upperBound) - Real_t(m_lowerBound);
		yggdrasil_assert(std::isnan(Length) == false);

		// The max is here to premevt some negative length
		// If length is NAN then the comparison is false and we return NAN
		return (Length < 0.0) ? 0.0 : Length;
	}; //End: getLengthREAL

	/**
	 * \brief	Returns \c true if \c *this has finite legth.
	 *
	 * This function was changd, see also the comment to isValid.
	 * This function is now a synonym to is valid, with the
	 * addition that it does not allows length that are infinite.
	 */
	bool
	hasFiniteLength()
	  const
	  noexcept
	{
		if((this->isValid() == true) && (std::isinf(this->getLength()) == false))
		{
			yggdrasil_assert(std::isnan(this->getLength()) == false);
			return true;
		};

		return false;
	}; //End: hasFiniteLength


	/**
	 * \brief	This fucntion returns \c true if \c *this is valid.
	 *
	 * A not valid interval is basically an interval where the
	 * lower bound is above the upper bound.
	 * One of the bound is NaN.
	 *
	 * However if \c *this has INFINITY as bound, we consider it not as invalid.
	 *
	 * \note 	We have changed this. In previous incranations, we have requiered
	 * 		 That the length was above some epsilon, but It turned out that this
	 * 		 was too restrictive. Maybe it is not, since we had this strange bug.
	 * 		 Never the less we have now defined a valid interval to have a length,
	 * 		 that is greater than zero, and its inverse also exists.
	 */
	bool
	isValid()
	  const
	  noexcept
	{
                if(std::isnan(m_lowerBound) == true)
                {
                        return false;
                };

                if(std::isnan(m_upperBound) == true)
                {
                        return false;
                };

		//They must be strictly greater
		if(!(m_lowerBound < m_upperBound))
		{
			return false;
		};

		/*
		 * Calculate the length
		 */
		const auto Length = m_upperBound - m_lowerBound;

		//Test if the computation of the length was valid.
		//This is needed for the rare case of infinity bound.
		if(::std::isinf(Length) == true)
		{
			//It is valid.
			return true;
		};
		yggdrasil_assert(::std::isnan(Length) == false);

		/*
		 * We requere positivity and non zero
		 *
		 * Since we requered that lower is strictly smaller than upper
		 * we know that a negative value is only an arefact from
		 * the finite precision aretmetic.
		 *
		 * Thus we combine it with the zero case, which we consider as invalid.
		 */
		if(Length <= 0.0)
		{
			return false;
		};

		//We knwo that the length is greater than zero, so we compute its
		//inverse and requere that irt is a valid float
		const auto iLength = 1.0 / Length;
		if(isValidFloat(iLength) == false)
		{
			return false;
		};


		/*
		 * If we are here the domain/interval fullfiles all our expectations
		 */
		return true;
	}; //End: isValid


	/**
	 * \brief	This function checks if \c x is close to the upper boundary.
	 *
	 * A point \c x is close to the upper boundary if the following equation
	 * holds:
	 * 	\abs(m_upperbound - x) < n * \epsilon
	 * Where n is a variable number and \epsilon is the machine presicion
	 *
	 * \param  x 	The point we want to test
	 * \param  n	A safty factro, fefaulted to 100
	 */
	bool
	isCloseToUpperBound(
		const Numeric_t&	x,
		const Numeric_t&	n = 100)
	  const
	  noexcept
	{
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(::std::isnan(x) == false);

		const Numeric_t distanceToUpperBound = std::abs(m_upperBound - x);

		if(distanceToUpperBound < n * Constants::EPSILON)
		{
			return true;
		};

		return false;
	};


	/**
	 * \brief	This function returns \c true, if at least
	 * 		 one of the bounds of \c *this is INFINITY.
	 */
	bool
	isUnbounded()
	 const
	 noexcept
	{
		if(::std::isinf(m_lowerBound) || ::std::isinf(m_upperBound))
		{
			return true;
		};

		return false;
	};


	/**
	 * \brief	This function returns \c true , if the upper bound
	 * 		 of \c *this is INFINITY
	 */
	bool
	isUpperBoundInf()
	 const
	 noexcept
	{
		return ::std::isinf(m_upperBound);
	};


	/**
	 * \brief	This function returns \c true , if the lower bound
	 * 		 of \c *this is INFINITY
	 */
	bool
	isLowerBoundInf()
	 const
	 noexcept
	{
		return ::std::isinf(m_lowerBound);
	};



	/*
	 * ====================
	 * Interaction fucntion
	 *
	 * These function are primarly ment to model the interaction between
	 * two intervals
	 */
public:
	/**
	 * \brief	Test if *this is inside the other interval, completely.
	 *
	 * This function tests strictly.
	 *
	 * \param  other	The other interval.
	 */
	bool
	isContainedIn(
		const yggdrasil_intervalBound_t&	other)
	 const
	{
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(other.isValid());

		return ((other.m_lowerBound < this->m_lowerBound) && (this->m_upperBound < other.m_upperBound)) ? true : false;
	}; //End contained in


	/**
	 * \brief	This fucntion tests if *this is weakly conatined inside the other.
	 *
	 * This function allows that the two intervals are the same.
	 *
	 * \param  other	The other interval.
	 */
	bool
	isWeaklyContainedIn(
		const yggdrasil_intervalBound_t&	other)
	 const
	{
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(other.isValid());

		return ((other.m_lowerBound <= this->m_lowerBound) && (this->m_upperBound <= other.m_upperBound)) ? true : false;
	}; //End contained in


	/**
	 * \brief	This fucntion returns true if *this is before other.
	 *
	 * Thsi function tests if the upper bound of this is striuctly smaller,
	 * than the lower bound of other.
	 *
	 * \param  other	The other interval.
	 */
	bool
	isBeforeOther(
		const yggdrasil_intervalBound_t&	other)
	 const
	{
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(other.isValid());

		return (this->m_upperBound < other.m_lowerBound) ? true : false;
	}; //End contained in



	/*
	 * ======================
	 * Splitting function
	 */
public:
	/**
	 * \brief	This function splits the interval in half.
	 *
	 * The middle point \c middPoint decided the new bounds.
	 * It will create the intervals Left ~ [m_lowerBound, middPoint[ and the Right ~ [middPoint, m_upperBound[
	 *
	 * \return 	A pair where \c first is the left interval and \c second is the right interval
	 *
	 * \param  middPoint	This is the new middpoint
	 *
	 * \throw	This function trows if \c *this is not valid or if the two results are not valid
	 * 		Also the split location must be inside \c *this.
	 */
	std::pair<yggdrasil_intervalBound_t, yggdrasil_intervalBound_t>
	doSplit(
		const Numeric_t 	middPoint)
	  const
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not split an invalid interval.");
		};

		//Test if inside
		if(this->isInside(middPoint) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The split location, " + std::to_string(middPoint) + ", does not lie inside the interval " + this->print());
		};

		//Create the two splits
		yggdrasil_intervalBound_t
			Left (this->m_lowerBound, middPoint         ),
			Right(middPoint         , this->m_upperBound);

		if(Left.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The left side of an interval has become invalid.");
		};

		if(Right.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The right split on an interval has become invalid.");
		};


		//Returning the two splits
		return ::std::make_pair(Left, Right);
	}; //End do split


	/*
	 * ========================
	 * Merging functions
	 */
public:
	/**
	 * \brief	This function merges *this and other together.
	 * 		 Hower *this is not modified, instead a copy is returned.
	 *
	 * This function merges the two interval.
	 * A merged interval is such that it spanns the entiere range
	 * of the interval.
	 * The lower bound is the min of all bounds and the upper is the max of all bounds.
	 * If one of the two intervals is invalid the result is also the invalid bound.
	 *
	 * Note that this function does not modifies *this.
	 * Instead a copy is returned.
	 *
	 * \param  other	The other interval that should be merged with this.
	 */
	yggdrasil_intervalBound_t
	mergeWith(
		const yggdrasil_intervalBound_t& 	other)
	 const
	{
		//Test if at leas one is invalid, if so return the invalid interval
		if(this->isValid() == false || other.isValid() == false)
		{
			return CREAT_INVALID_INTERVAL();
		}; //End if: handle invalid stuff

		//Now estimate the minimum
		const Numeric_t Minim = std::min(
						std::min(this->m_lowerBound, this->m_upperBound),
						std::min(other.m_lowerBound, other.m_upperBound)
						);
		//now the maximum
		const Numeric_t Mxim = std::max(
						std::max(this->m_lowerBound, this->m_upperBound),
						std::max(other.m_lowerBound, other.m_upperBound)
						);

		//Create the new interval and returning it
		return yggdrasil_intervalBound_t(Minim, Mxim);
	}; //End merge


	/*
	 * ===================================
	 * Mappings
	 */
public:
	/**
	 * \brief	Maps a value \e x, that have to lie in the interval [0, 1[ ,
	 * 		 into the interval represented by \c *this.
	 *
	 * This function basically calculates \f$ (\texttt{upper} - \texttt{lower}) \cdot  x + \textt{lower} \f$.
	 * This function is only for one value, there is also a version tat acts on many values.
	 *
	 * \param  x	The value we want to map to.
	 *
	 * \note 	This function does not check its argument.
	 * 		 This function was previuously called mappTo.
	 */
	Numeric_t
	mapFromUnit(
		const Numeric_t 	x)
	 const
	 noexcept
	{
		yggdrasil_assert(this->isValid());

		return ((m_upperBound - m_lowerBound) * x + m_lowerBound);
	}; //End: mapFromUnit (scalar)


	/**
	 * \brief	This function map _many_ values in the interval [0, 1[ into the interval represented by *this.
	 *
	 * This functiion is more effient if one deals with many value, since the tests are performed only once.
	 * Also the slope is calculated only once.
	 * The loop will be probably vecorized with fma.
	 *
	 * \param  X		The range of values that will be mapped
	 *
	 * \tparam V		The vector type
	 *
	 * \return 	This function returns a valueArray_t of type Numeric_t
	 *
	 * \note 	This function does not check its argument.
	 * 		 This function was previously called mappsTo.
	 */
	template<class V>
	ValueArray_t
	mapFromUnit(
		const V&	X)
	 const
	 noexcept
	{
		yggdrasil_assert(this->isValid());

		//Precompute the slope
		const Numeric_t slope = (m_upperBound - m_lowerBound);

		//Make a copy of the input vector, this will be used as output vector
		ValueArray_t Y(::std::begin(X), ::std::end(X));

		//Iterate through the values
		for(Numeric_t& x : Y)
		{
			//The compiler should put an fma here
			x = slope * x + m_lowerBound;
		}; //End for(x):

		return Y;
	}; //End mapFromUnit (vector)


	/**
	 * \brief	This function maps a value \c x, that is inside \c *this, to a value in the unit interval.
	 *
	 * This is like the inverse operation of the mapFromUnit functions.
	 * It also acts on a scalar value.
	 *
	 * \param  x	A value that is inside *this and will be mapped to the unit interval
	 *
	 * \note 	This function does not check its imput argument
	 */
	Numeric_t
	contractToUnit(
		const Numeric_t 	x)
	 const
	 noexcept
	{
		yggdrasil_assert(this->isValid());

		//We assert it here
		//We must also take into account that we habe something on the boundary
		yggdrasil_assert(this->isInside(x) || this->isCloseToUpperBound(x));

		//This is the length
		const Numeric_t l = m_upperBound - m_lowerBound;

		return ((x - m_lowerBound) / l);
	}; //End: contrantsToUnit


	/**
	 * \brief	This function maps a value \c x, that is inside \c *this,
	 * 		 to a value in the unit interval; but it acts on many variables.
	 *
	 * This is like the inverse operation of the mapTo functions.
	 * It acts on many variable at the same time.
	 *
	 * \param  X	A value vector which components are inside \c *this
	 * 		 and will be mapped to the unit interval-
	 *
	 * \tparam V	The vector type.
	 *
	 * \return 	This function returns a valueArray_t of type numeric.
	 *
	 * \note 	This function does not check its imput argument.
	 */
	template<class V>
	ValueArray_t
	contractToUnit(
		const V&	X)
	 const
	 noexcept
	{
		yggdrasil_assert(this->isValid());

		//Make a copy of the input vector, this will be used as output vector
		ValueArray_t Y(::std::begin(X), ::std::end(X));

		//Inorder to be consistent we use the inplace function
		this->contractToUnitInPlace(Y);

		//Return y it was replaced
		return Y;
	}; //End mappsTo (vector)


	/**
	 * \brief	This function maps a value \c x, that is inside \c *this,
	 * 		 to a value in the unit interval; but it acts on many variables.
	 * 		 This function is inplace, this means that the input array gets overritten.
	 *
	 * This is like the inverse operation of the mapTo functions.
	 * It acts on many variable at the same time.
	 *
	 * This function is like the other one, but it overrites the input.
	 * This mean it is modified.
	 *
	 * \param  X	A value vector which components are inside \c *this
	 * 		 and will be mapped to the unit interval-
	 *
	 * \tparam V	The vector type.
	 *
	 * \note 	This function does not check its imput argument.
	 * 		 This funciton changes the input
	 */
	template<class V>
	void
	contractToUnitInPlace(
		V&	X)
	 const
	 noexcept
	{
		yggdrasil_assert(this->isValid());

		//Precompute the slope
		const ::yggdrasil::Numeric_t iL = yggdrasil::Real_t(1.0) / (m_upperBound - m_lowerBound);
		yggdrasil_assert(isValidFloat(iL));
		yggdrasil_assert(iL > 0.0);

#ifndef NDEBUG
		const yggdrasil_intervalBound_t unitInterval = yggdrasil_intervalBound_t::CREAT_UNIT_INTERVAL();
#endif

		//This is the lower bound
		const Numeric_t lowerBound = m_lowerBound;

		//This is the unroll factor
		const Size_t unrollFactor = 12;

		//Get the length of the array
		const Size_t N = X.size();

		if(N == 0)
		{
			return;
		};

		//This is for the left over
		const Size_t leftOver  = N % unrollFactor;
		const Size_t unrollEnd = N - leftOver;

		//This is the pointer access to the array
		Numeric_t* const rawX = X.data();

		for(Size_t i = 0; i != unrollEnd; i += unrollFactor)
		{
			//Load the data
			const Numeric_t x_0 = rawX[i + 0];
			const Numeric_t x_1 = rawX[i + 1];
			const Numeric_t x_2 = rawX[i + 2];
			const Numeric_t x_3 = rawX[i + 3];
			const Numeric_t x_4 = rawX[i + 4];
			const Numeric_t x_5 = rawX[i + 5];
			const Numeric_t x_6 = rawX[i + 6];
			const Numeric_t x_7 = rawX[i + 7];
			const Numeric_t x_8 = rawX[i + 8];
			const Numeric_t x_9 = rawX[i + 9];
			const Numeric_t x_10 = rawX[i + 10];
			const Numeric_t x_11 = rawX[i + 11];

			//Subtract the lower bound
			const Numeric_t y_0 = x_0 - lowerBound;
			const Numeric_t y_1 = x_1 - lowerBound;
			const Numeric_t y_2 = x_2 - lowerBound;
			const Numeric_t y_3 = x_3 - lowerBound;
			const Numeric_t y_4 = x_4 - lowerBound;
			const Numeric_t y_5 = x_5 - lowerBound;
			const Numeric_t y_6 = x_6 - lowerBound;
			const Numeric_t y_7 = x_7 - lowerBound;
			const Numeric_t y_8 = x_8 - lowerBound;
			const Numeric_t y_9 = x_9 - lowerBound;
			const Numeric_t y_10 = x_10 - lowerBound;
			const Numeric_t y_11 = x_11 - lowerBound;

			//Transform the length
			const Numeric_t z_0 = iL * y_0;
			const Numeric_t z_1 = iL * y_1;
			const Numeric_t z_2 = iL * y_2;
			const Numeric_t z_3 = iL * y_3;
			const Numeric_t z_4 = iL * y_4;
			const Numeric_t z_5 = iL * y_5;
			const Numeric_t z_6 = iL * y_6;
			const Numeric_t z_7 = iL * y_7;
			const Numeric_t z_8 = iL * y_8;
			const Numeric_t z_9 = iL * y_9;
			const Numeric_t z_10 = iL * y_10;
			const Numeric_t z_11 = iL * y_11;

			//Write the result back
			rawX[i + 0] = z_0;
			rawX[i + 1] = z_1;
			rawX[i + 2] = z_2;
			rawX[i + 3] = z_3;
			rawX[i + 4] = z_4;
			rawX[i + 5] = z_5;
			rawX[i + 6] = z_6;
			rawX[i + 7] = z_7;
			rawX[i + 8] = z_8;
			rawX[i + 9] = z_9;
			rawX[i + 10] = z_10;
			rawX[i + 11] = z_11;
		}; //ENd for(i): unrolled


		//Handling the left overs if needed
		if(leftOver != 0)
		{
			for(Size_t i = unrollEnd; i != N; ++i)
			{
				//Load the data
				const Numeric_t x_i = rawX[i];

				//subtract the lower bound
				const Numeric_t y_i = x_i - lowerBound;

				//Apply the inverse length
				const Numeric_t z_i = iL * y_i;

				//Write back
				rawX[i] = z_i;
			}; //ENd for(i)
		}; //ENd if: handle left over

		return;
	}; //End mappsTo (vector) InPlace


	/*
	 * =====================
	 * Access functions
	 */
public:
	/**
	 * \brief	Returns the lower bound of \c *this .
	 */
	Numeric_t
	getLowerBound()
	  const
	  noexcept
	{
		return m_lowerBound;
	};


	/**
	 * \brief	An alias of getLowerBound() .
	 */
	Numeric_t
	lower()
	  const
	  noexcept
	{
		return m_lowerBound;
	};


	/**
	 * \brief	Get the upper bound of \c *this .
	 */
	Numeric_t
	getUpperBound()
	  const
	  noexcept
	{
		return m_upperBound;
	};


	/**
	 * \brief	An alias of getUpperBound() .
	 */
	Numeric_t
	upper()
	  const
	  noexcept
	{
		return m_upperBound;
	};


	/**
	 * \brief	This function returns the midd point of *this.
	 */
	Numeric_t
	middPoint()
	 const
	{
		return (m_upperBound + m_lowerBound) * 0.5;
	};


	/**
	 * \brief	Print out the bounds to a string
	 *
	 * By default paranteses are used
	 *
	 * \param  useBrackets		Uses brackets instead of paranteheses
	 */
	std::string
	print(
		const bool 	useBracket = false)
	  const
	{
		const char openToken  = (useBracket == true) ? '[' : '(';
		const char closeToken = (useBracket == true) ? '[' : ')';

		return (openToken + std::to_string(m_lowerBound) + ", " + std::to_string(m_upperBound) + closeToken);
	}; //End print


	/*
	 * ==================
	 * Static functions
	 */
public:
	/**
	 * \brief	This function creates an invalid (default constructed) interval.
	 *
	 * Both of the bounds are set to NAN.
	 * However no exception is raised
	 */
	static
	yggdrasil_intervalBound_t
	CREAT_INVALID_INTERVAL()
	 noexcept
	{
		return yggdrasil_intervalBound_t();
	};


	/**
	 * \brief	This function allows the creation of an interval without any checks
	 *
	 * This fucntion allows to bypass the checks of the interval constructior.
	 * Thius effectively allow the creation of an invalid and wrong interval.
	 *
	 * This function is ment only for interbal usage.
	 */
	static
	yggdrasil_intervalBound_t
	CREAT_UNCHECKED_INTERVAL(
		const Numeric_t 	lowerBound,
		const Numeric_t 	upperBound,
		const bool 		bypass = false)
	{
		return yggdrasil_intervalBound_t(lowerBound, upperBound, bypass);
	};


	/**
	 * \brief	This function creates a unit interval
	 */
	static
	yggdrasil_intervalBound_t
	CREAT_UNIT_INTERVAL()
	{
		return yggdrasil_intervalBound_t(0.0, 1.0, true);  //The true will bypass the checks
	};



	/*
	 * ===================
	 * Private Functions
	 */
private:
	/**
	 * \brief	Default constructor uses NaN as bounds.
	 *
	 * This is the only way to construct a non valid interval without triggering an exception.
	 * owever such intervals are pretty useless.
	 * This function is private, this means it can not be accessed from the outside.
	 * In order to provide external code the possibility to create deafult (invalid)
	 * intervals the static CREAT_INVALID_INTERVAL() function is provided.
	 *
	 * \note	This function is ment for internal use only.
	 * 		 So the user should never ever use it.
	 */
	explicit
	yggdrasil_intervalBound_t()
	 noexcept :
	  m_lowerBound(NAN),
	  m_upperBound(NAN)
	{};


	/**
	 * \brief	This is a private constructor that allows the creation of a interval without checks.
	 *
	 * This constructor is provided for internal use only.
	 */
	explicit
	yggdrasil_intervalBound_t(
		const Numeric_t 	lowerBound,
		const Numeric_t 	upperBound,
		const bool 		byPassCheck)
	 :
	  m_lowerBound(lowerBound),
	  m_upperBound(upperBound)
	{
		if(byPassCheck == false)
		{
			if(this->isValid() == false)
			{
				throw YGGDRASIL_EXCEPT_InvArg("A non valid interval was tried to construct.");
			}; //End if: Valid check
		}; //End if; bypass
	};




	/*
	 * ==============
	 * Private Variable
	 */
private:
	Numeric_t 	m_lowerBound;	//!< The lower bound
	Numeric_t 	m_upperBound;	//!< The upper bound
}; //End class: yggdrasil_intervalBound_t


/**
 * \brief	This function is for using the << operator
 */
inline
std::ostream&
operator<< (
	std::ostream& 				o,
	const yggdrasil_intervalBound_t&	i)
{
	return (o << i.print());
};




YGGDRASIL_NS_END(yggdrasil)
