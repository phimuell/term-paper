/**
 * \brief	This file provides a deapth map for the tree.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_statistics.hpp>
#include <util/yggdrasil_treeDepthMap.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>

//INcluide std
#include <map>



YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_treeDepthMap_t::yggdrasil_treeDepthMap_t(
	const Node_t& 		root)
 :
  m_depthMap(),
  m_stat()
{
	//We simply have to iterate ovber all the leafs and call the depth
	//function of the iterwator
	for(auto it = root.getLeafRange(); it.isEnd() == false; it.nextLeaf())
	{
		//Get the current depth
		const auto depth = it.getDepth();
		yggdrasil_assert(depth >= 0);

		//This is the type we want to insert
		//We insert zewor, this allows/requiere us to increment in all situations
		const auto toInsert = ::std::make_pair(depth, 0);

		//Variable for the insertion
		DepthMap_t::iterator deptLoc;
		bool wasInserted;

		//Now we try to insert it
		std::tie(deptLoc, wasInserted) = m_depthMap.insert(toInsert);

		//Regaradpess if it was inserted or not, we habve now to
		//increment it
		deptLoc->second += 1;


		//Update the statistic
		m_stat.add_Record(depth);
	}; //End for(it):
}; //End building constructor



YGGDRASIL_NS_END(yggdrasil)






