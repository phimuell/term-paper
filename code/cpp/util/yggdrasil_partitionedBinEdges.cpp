/**
 * \brief	This class implements the code for the partitioned bin edge array.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_partitionedBinEdges.hpp>
#include <util/yggdrasil_util.hpp>
#include <util/yggdrasil_generalBinEdges.hpp>


//Incluide std
#include <memory>
#include <vector>
#include <numeric>
#include <algorithm>

//Include boost


YGGDRASIL_NS_BEGIN(yggdrasil)


void
yggdrasil_partionedBinEdges_t::buildBins(
	const IntervalBound_t&	fullInterval,
	const ValueArray_t& 	innerEdges)
{
	//Test the argument and this
	if(this->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bin edges container is allready associated.");
	};
	if(this->isTrivial() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not build an allready trivial bin edge.");
	};
	if(fullInterval.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The upper interval is not valid.");
	};

	//There is the possibility that we
	//have zereo inner points, in that case it is trivially to build
	if(innerEdges.size() == 0)
	{
		//We have only one big bin
		m_binEdges.resize(2);

		//Set the two points
		m_binEdges[0] = fullInterval.getLowerBound();
		m_binEdges[1] = fullInterval.getUpperBound();

		//We are now finisthed
		return;
	}; //ENd if: no inner edges


	/*
	 * Test the input data.
	 *
	 * This has to be done in two steps.
	 * First we test if the innerEdges is sorted and distinct.
	 * This has only to be done if we have more than one inner
	 * edges.
	 * Second we have to test if the two end points are distinct
	 * from the fullInterval
	 */

	//Make the first test
	if(innerEdges.size() > 1)
	{

		for(
			ValueArray_t::const_iterator it = innerEdges.cbegin() + 1;
			it != innerEdges.cend();
			++it)
		{
			const Numeric_t low = *(it - 1);
			const Numeric_t up  = *(it    );

			//Test if they are distinct
			if(low == up)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The array contained values that where the same.");
			}; //end: not distinct

			//Test if they are sorted
			//Must be strict
			if(!(low < up))
			{
				throw YGGDRASIL_EXCEPT_InvArg("The array of inner edges is not sorted.");
			}; //ENd
		}; //End for(it): Going throug in pairs
	}; //End iof: make inner test

	//Second test compare it with the the interval bounds
	if(!(fullInterval.getLowerBound() < innerEdges.front()))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The inner edges has compability issues with the lower bound of the full interval.");
	}; //End if: lower compability
	if(!(innerEdges.back() < fullInterval.getUpperBound()))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The inner edges has compability issues with the upper bound of the full interval.");
	};

	//Now we can set fill the result
	m_binEdges.reserve(innerEdges.size() + 2);


	//First add the lower bound of the full interval
	m_binEdges.push_back(fullInterval.getLowerBound());

	//Now add all the one in the inner edges array
	for(const Numeric_t x : innerEdges)
	{
		m_binEdges.push_back(x);
	}; //End for(x): adding at the end

	//Adding the last one
	m_binEdges.push_back(fullInterval.getUpperBound());

	// Now tests if all teh intervals where successfull
	yggdrasil_assert(this->nBins() == (innerEdges.size() + 1));

	const Size_t NBins = this->nBins();
	for(Size_t i = 0; i != NBins; ++i)
	{
		const IntervalBound_t tI = this->getBinInterval(i);

		//Now test
		if(tI.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("While testing the newly build interval " + std::to_string(i) + " found to be invalid.");
		};
	}; //End for(i)


	//We are now finished
	return;
}; //End bould interval



void
yggdrasil_partionedBinEdges_t::addPartition(
	const DimArray_t& 	dimArray)
{
	//TEst if *this is associated
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bin edge is not yet associated.");
	};
	if(this->isTrivial() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not add a partiton to a trivial bin edge array.");
	};
	if(dimArray.isDimensionIllegal() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dim array is invalid.");
	};
	if(this->hasPartition() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not add a partition to a bin edge that has allready one.");
	};

	//We now need a generall interval.
	//I knwio this is not so supper efficient, but it will not kill us
	const yggdrasil_genBinEdges_t genBins(this->getGeneralEdges());
	yggdrasil_assert(m_idxArray.empty());
	yggdrasil_assert(m_idxPartition.empty());

	//Get the number of samples
	const Size_t nSamples = dimArray.nSamples();
	yggdrasil_assert(m_idxArray.empty());
	yggdrasil_assert(m_idxPartition.empty());

	//Now create the index array
	m_idxArray.resize(nSamples);
	yggdrasil_assert(m_idxArray.size() == nSamples);

	//Now generate the index array
	for(Size_t i = 0; i != nSamples; ++i)
	{
		m_idxArray[i] = i;
	};

	//Now generate the index array, we also need sorting
	m_idxPartition = yggdrasil_recArgBinning(dimArray, genBins, &m_idxArray, true);

	yggdrasil_assert(this->hasPartition() == true);
	yggdrasil_assert(m_binEdges.size() == m_idxPartition.size());
	yggdrasil_assert(dimArray.nSamples() == m_idxArray.size());

	return;
}; //ENd bound interval to the data






void
yggdrasil_partionedBinEdges_t::unboundData()
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can only unbind an associated bin edges.");
	};
	if(this->hasPartition() == true)
	{
		//We must now unbound the partitoning
		m_idxPartition.clear();
		BinPartArray_t eB;
		eB.swap(m_idxPartition);

		m_idxArray.clear();
		IndexArray_t eI;
		eI.swap(m_idxArray);
	}; //End clearing the partitoning

	if(this->isTrivial() == false)
	{
		//Unbound in the trivial case

		//Create a new and use the swap trick to
		//realy get rid of the memeory
		ValueArray_t e;
		e.swap(m_binEdges);

		yggdrasil_assert(m_binEdges.empty());
		yggdrasil_assert(m_idxArray.empty());
		yggdrasil_assert(m_idxPartition.empty());
		yggdrasil_assert(this->isAssociated() == false);
	}
	else
	{
		//In the trivial case unbinding is sinply setting a bool
		m_isTrivial = false;
	};

	yggdrasil_assert(this->isTrivial() == false);
	yggdrasil_assert(this->isAssociated() == false);

	return;
}; //End: unbpound


void
yggdrasil_partionedBinEdges_t::buildBins(
	const IntervalBound_t&	fullInterval,
	const ValueArray_t& 	innerEdges,
	const DimArray_t&	dimArray)
{
	if(this->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to build in an allready associated bin edge.");
	};

	this->buildBins(fullInterval, innerEdges);

	this->addPartition(dimArray);

	return;
}; //Add a partition

void
yggdrasil_partionedBinEdges_t::trivialBind()
{
	if(this->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bin is allready associated.");
	};
	yggdrasil_assert(m_binEdges.empty());
	yggdrasil_assert(m_idxPartition.empty());
	yggdrasil_assert(m_idxArray.empty());

	//Set the bool to trivial
	m_isTrivial = true;

	yggdrasil_assert(this->isTrivial());

	return;
}; //ENd trivial bind




YGGDRASIL_NS_END(yggdrasil)
