#pragma once
/**
 * \brief	This file implements a hypercube.
 *
 * This is a restricted hypercube in the way that it is axis alligned.
 * Meaning that there are no inclined bounds.
 *
 * We define a bounding box as the half open interval [x_l, x_u[.
 * This means that the upper bound is not included.
 *
 * The hypercubes are needed to bound the domains of the cubes.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_splitCommand.hpp>

#include <tree_geni/yggdrasil_multiDimCondition.hpp>

#include <samples/yggdrasil_sample.hpp>

//INcluide std
#include <vector>
#include <utility>
#include <tuple>

YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the dimensional array
class yggdrasil_dimensionArray_t;

//FWD of the sample containers
class yggdrasil_sampleList_t;
class yggdrasil_sampleCollection_t;
class yggdrasil_arraySample_t;


/**
 * \class 	yggdrasil_hyperCube_t
 * \brief	This implements the hypercube functionality
 *
 * As stated above there only axis alligned cubes are supported.
 * An hypercube of dimension n is basically 2n numbers.
 *
 * This class is implemented as a list of one dimensional intervals.
 * Each index of that list corresponds to a specific dimension.
 *
 * \note 	There is not guarantee about the underling memory layout.
 * 		 Even no guarantee if the storage is continuus or not.
 *
 * \note 	This class tries to be inmutable.
 * 		 However moving is allowed.
 *
 * \note	This class could be a bit more space efficient.
 */
class yggdrasil_hyperCube_t
{
	/*
	 * ==========================
	 * Typedefs
	 */
public:
	using Numeric_t 	= ::yggdrasil::Numeric_t;	//!< This is the numeric type
	using Size_t 		= ::yggdrasil::Size_t;		//!< This is for counting and indexing

	using IntervalBound_t 	= yggdrasil_intervalBound_t; 	 //!< This is the unerling type for the interval in one dimenson
	using Vector_t 		= ::std::vector<IntervalBound_t>;//!< This is the type that stores the data (For internal use only)

	using Sample_t 		= yggdrasil_sample_t;		 //!< This is the sample
	using SampleCollection_t= yggdrasil_sampleCollection_t;	 //!< The sample collection.
	using SampleArray_t 	= yggdrasil_arraySample_t;	 //!< The sample array.
	using SampleList_t 	= yggdrasil_sampleList_t;	 //!< The sample list.

	using SingleSplit_t 	= yggdrasil_singleSplit_t;	//!< This is the type that encodes a single split.


	using DimensionArray_t 	= yggdrasil_dimensionArray_t;	//!< This is the type for storing a single dimension
	using ValueArray_t 	= ::yggdrasil::yggdrasil_valueArray_t<Numeric_t>;	//!< This is the type for the value array

	using MultiCondition_t	= yggdrasil_multiCondition_t;	//!< This represents multivble conditions
	using SingleCondition_t = MultiCondition_t::SingleCondition_t;	//!< This represensts a single condition

	//This are used to attain compability
	using value_type 	= Vector_t::value_type; 	//!< The value type we operate on
	using reference		= Vector_t::reference;		//!< This is the type for the reference
	using const_reference	= Vector_t::const_reference;	//!< This ist he type for the const reference

	//This is the iterator for iterating over the different dimensions
	using iterator 		= Vector_t::const_iterator;	//!< This is the iterator; Is const to enforce imutability
	using const_iterator	= Vector_t::const_iterator;	//!< This is a const iterator


	/*
	 * ====================
	 * Constructors
	 *
	 * As we have stated above ths class tries to be inmutable.
	 * However building is a bit tricky then.
	 * The solution is to allow mutations only on dimensions hat
	 * are itself invalid.
	 * This means that invalidation will decrease and never increas.
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This constructs a hypercupe of diomension \c n .
	 * All dimensions are \em invalid intervals.
	 *
	 * \param  n 	The numbers of dimensions.
	 */
	explicit
	yggdrasil_hyperCube_t(
		const Int_t 	n) :
	  m_bounds(n, IntervalBound_t::CREAT_INVALID_INTERVAL() )
	{};


	/**
	 * \brief	This si the building constructor of the hypercube.
	 *
	 * It is provided for the conversion between intervals and
	 * hypercubes. For safty reason, and because I like
	 * type enforcing the constructor is marked explicit.
	 *
	 * \param  interval	The interval that should be used
	 */
	explicit
	yggdrasil_hyperCube_t(
		const IntervalBound_t&	interval)
	 :
	  yggdrasil_hyperCube_t(1, interval)
	{};


	/**
	 * \brief	This is a building constructor.
	 *
	 * This constructo takes a vector of intervals.
	 * This vector will be used to construct *this.
	 * After the construction a check if *This is
	 * valid is performed.
	 *
	 * \param  intervals 	This are the intervals that should be used.
	 */
	explicit
	yggdrasil_hyperCube_t(
		const std::vector<IntervalBound_t>& 	intervals)
	 :
	  m_bounds(intervals)
	{
		//Thest if this is valid
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The vector of intervals resulted in an invalid cube.");
		};
		yggdrasil_assert(intervals.empty() == false);
	}; //End building form list



	/**
	 * \brief 	Building constructor.
	 *
	 * This constructs an hypercube of dimension \c n .
	 * All dimensions are copies of the interval \c baseInterval .
	 *
	 * \param  n		The numbers of dimensions.
	 * \param  baseInterval	This is the base interval.
	 *
	 * \note	This constructor is not so usefull.
	 */
	yggdrasil_hyperCube_t(
		const Int_t 		n,
		const IntervalBound_t&	baseInterval) :
	  m_bounds(n, baseInterval)
	{};


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_hyperCube_t(
		const yggdrasil_hyperCube_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_hyperCube_t(
		yggdrasil_hyperCube_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_hyperCube_t() = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_hyperCube_t&
	operator= (
		const yggdrasil_hyperCube_t&) = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_hyperCube_t&
	operator= (
		yggdrasil_hyperCube_t&&) = default;


	/*
	 * =================
	 * Private constructors
	 */
private:
	/**
	 * \brief	Default constructor.
	 *
	 * It is private and defaulted.
	 * This is done to allow to create some instance.
	 * in order to make use of it the user has to use
	 * the static MAKE_INVALID_CUBE() function.
	 */
	yggdrasil_hyperCube_t() noexcept = default;




	/*
	 * ==========================
	 * Modifing function
	 *
	 * As stated above *this is inmutable.
	 * However we allow that invalid dimensions
	 * can be modified.
	 */
public:
	/**
	 * \brief	Set the interval of dimension \c d to \c newInterval .
	 *
	 * Dimension \c d must be invalid, for this operation to be legal.
	 * The same is true for \c newInterval .
	 *
	 * \param  d 		The dimension to change.
	 * \param  newInterval	The new interval.
	 *
	 * \throw 	This function throws if dimension \c d is not invalid.
	 *		Also the new interval must be valid.
	 */
	void
	setIntervalTo(
		const Int_t 		d,
		const IntervalBound_t&	newInterval)
	{
		//TEst if some neccessarly preconditions are met
		if(m_bounds.empty() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to set a dimension of an enpty cube.");
		};
		if(m_bounds.at(d).isValid() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to change the valid dimension " + std::to_string(d));
		};

		//Test if the interval that is passed is valid.
		if(newInterval.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The new interval must be valid, but it is " + newInterval.print());
		};


		//
		//Now we can change it
		m_bounds[d] = newInterval;


		/*
		 * The follwoing test is needed, because it could happens that
		 * all interval are indicidually valid, but the cube as a whole is not
		 */

		//We must check if all intervals are valid indibvidually
		bool allDimsAreValid = true;
		for(const IntervalBound_t& i : m_bounds)
		{
			//Test if we have found an invalid interval
			if(i.isValid() == false)
			{
				allDimsAreValid = false;
				break;
			};
		}; //End for(i): test if all are valid

		/*
		 * Since we impose tight controll the fact that we found an
		 * invalid interval means, that not all intervals are set yet.
		 *
		 * On the other side if all intervals, on its own are valid,
		 * then thsi means that all intervals are set.
		 * then we must perform the test.
		 */
		if(allDimsAreValid == true)
		{
			//All intervals are valid, so we must enforce that the whole thing is valid
			if(this->isValid() == false)
			{
				throw YGGDRASIL_EXCEPT_RUNTIME("The creation of the cube failed. All intervals individually where valid, but together they are not.");
			};
		}; //ENd if: all intervals are valid

		return;
	}; //End: setIntervalTo



	/*
	 * ==========================
	 * Main functions
	 *
	 * This are the main functions
	 */
public:
	/**
	 * \brief	This function performs a split along dimension \c d at location \c middPoint .
	 *
	 * This only splits the dimension \c d and no other is affected.
	 * Further *this is not modified, modified copies are returned.
	 *
	 * \return 	A pair is used to return the tow new cubes.
	 * 		 \c first is the \em left hypercube and \c second is the \em right hypercube.
	 *
	 * \param  d 		The dimension we whant to split.
	 * \param middPoint 	This is the point where we split.
	 *
	 * \throw 	This function thows if the underlind split function throws.
	 * 		 Also *this must be valid.
	 * 		 Also the resulting cubes must be valid.
	 */
	::std::pair<yggdrasil_hyperCube_t, yggdrasil_hyperCube_t>
	doSplit(
		const Int_t 		d,
		const Numeric_t 	middPoint)
	 const
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to split an invalid hypercube.");
		};

		if(this->isInside(d, middPoint) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The split location " + std::to_string(middPoint) + " is not inside interval of dimension " + std::to_string(d) + ", which is " + m_bounds.at(d).print());
		};

		//Make a copy of *this for left and right
		yggdrasil_hyperCube_t
			LeftCube (*this),
			RightCube(*this);

		//Now split the underling dimension
		std::tie(LeftCube.m_bounds.at(d), RightCube.m_bounds.at(d)) = this->at(d).doSplit(middPoint);

		if(LeftCube.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The left cube of a split becames invalid.");
		};

		if(RightCube.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The right cube of a split becames invalid.");
		};

		return std::make_pair(LeftCube, RightCube);
	}; //End do spliting


	::std::pair<yggdrasil_hyperCube_t, yggdrasil_hyperCube_t>
	doSplit(
		const Numeric_t 	middPoint,
		const Int_t 		d)
	 const
	 = delete;


	/**
	 * \brief	This function performs a split of \c *this, but takes a split command as argument.
	 *
	 * This function is lke the other doSplit(Int_t, Numeric_t) function, but with an reduced interface.
	 *
	 * \param sSplit 	The split command that scould be executed.
	 *
	 * \note 	This function simply passes its argument
	 * 		 to the explicit version of thei function.
	 *
	 * \throw 	This function tests if the argumnent \e sSplit is valid.
	 * 		 The underling function may throw.
	 */
	::std::pair<yggdrasil_hyperCube_t, yggdrasil_hyperCube_t>
	doSplit(
		const SingleSplit_t&	sSplit)
	 const
	{
		if(sSplit.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The split command is not valid.");
		};

		return this->doSplit(sSplit.getSplitDim(), sSplit.getSplitPos());
	};


	/**
	 * \brief	This si the merging function of the domain.
	 *
	 * This function merges *this with other.
	 * However *this is not modified and a ciopy of the result is returned.
	 * This function applies the merge function of the interval to every dimension.
	 *
	 * If *this or other is invalid, then te invalid cube is returned.
	 * However if *this and other have a different dimension number,
	 * then an erro is generated.
	 *
	 * \param  other	The other domain *this should be merged with.
	 */
	yggdrasil_hyperCube_t
	mergeWith(
		const yggdrasil_hyperCube_t&	other)
	 const
	{
		//Test if one is invalid, There is the quirk tha an invalid domain does not have any size
		if(this->isValid() == false || other.isValid() == false)
		{
			return MAKE_INVALID_CUBE();
		};

		//Now we must test iof the diomensions are correct
		if(this->m_bounds.size() != other.m_bounds.size())
		{
			throw YGGDRASIL_EXCEPT_InvArg("Tried to merge tow cubes with different dimensions.");
		};

		//Get the dimension
		const Size_t D = this->m_bounds.size();
		yggdrasil_assert(D > 0);

		//Now we create an outlut variable
		yggdrasil_hyperCube_t outPut;	//Default constructor, its size ids zero
		yggdrasil_assert(outPut.m_bounds.size() == 0);

		//Iterate through all the dimensions and merge them
		//insert it into the output vector
		for(Size_t i = 0; i != D; ++i)
		{
			//Generate the merged interval
			const IntervalBound_t interval_i(this->m_bounds[i].mergeWith(other.m_bounds[i]));
			yggdrasil_assert(interval_i.isValid());

			//Save it into the bound
			yggdrasil_assert((Size_t)outPut.nDims() == i);
			outPut.m_bounds.push_back(interval_i);
			yggdrasil_assert((Size_t)outPut.nDims() == (i + 1));
			yggdrasil_assert(outPut.isValid());
		}; //End for(i):
		yggdrasil_assert(this->nDims() == outPut.nDims());


		//Return the merged domain
		return outPut;
	}; //End: merge WIth



	/**
	 * \brief	This function tests if x is inside, but only for a particular dimension
	 *
	 * \param  x 	The position to test
	 * \param  d 	The dimension to test
	 *
	 * \throw 	If \c d exceeds the dimension.
	 *
	 * \note	Old versions of this funciton had the arguments in different order.
	 * 		 Also previous functins throwed if *this was not valid.
	 * 		 This was made to an assert.
	 */
	bool
	isInside(
		const Int_t 		d,
		const Numeric_t 	x)
	 const
	{
		yggdrasil_assert(this->isValid());

		return m_bounds.at(d).isInside(x);	//Will throw on out of bound
	};


	/**
	 * \brief	This function is deleted for consistency reasons.
	 *
	 * This is the old version of the function.
	 * We have deleted it to prevent implicit castings
	 * and thus nonsense input.
	 */
	bool
	isInside(
		const Numeric_t 	x,
		const Int_t 		d)
	 const
	 = delete;


	/**
	 * \brief	This function tests if the split location is inside \c *this .
	 *
	 * \param  splitPos 	This is the split location.
	 *
	 * \throw	If \c splitPos is not valid.
	 * 		 Also the underling functions may throw.
	 */
	bool
	isInside(
		const SingleSplit_t&	splitPos)
	 const
	{
		yggdrasil_assert(this->isValid());

		if(splitPos.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The presented split position is invalid.");
		};

		return this->isInside(splitPos.getSplitDim(), splitPos.getSplitPos());
	};



	/**
	 * \brief	This function tests if a sample is inside \c *this .
	 *
	 * This function basically iterates through the dimension and tests
	 * if the corresponding dimension is inside the inteval.
	 *
	 * \param  s	This is the sample.
	 *
	 * \throw	The dimensions have to match.
	 *
	 * \note 	In previous versions the it was also tested if
	 * 		 *this is valid, this was removed and made
	 * 		 to an assert.
	 */
	bool
	isInside(
		const Sample_t&		s)
	 const
	{
		yggdrasil_assert(this->isValid());

		if(this->nDims() != s.nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimensions does not agree, the cube has " + std::to_string(size()) + " dimensions, but the sample has " + std::to_string(s.nDims()));
		};

		const Int_t N = s.nDims();
		for(Int_t d = 0; d != N; ++d)
		{
			if(m_bounds[d].isInside(s[d]) == false)
			{
				return false;
			};
		}; //End for(d):

		return true;
	}; //End: is inside


	/**
	 * \brief	Test if all samples in the sample containers
	 * 		 are inside *this.
	 *
	 * This function operates on the passed sample collection.
	 *
	 * \param  samples 	The sample collection that is tested.
	 *
	 * \throw 	If *this is invalid, the dimensions are wrong
	 * 		 or an invalid sample is detected.
	 */
	bool
	isInside(
		const SampleCollection_t& 	samples)
	 const;


	/**
	 * \brief	Test if all samples in the sample containers
	 * 		 are inside *this.
	 *
	 * This function operates on the passed sample array.
	 *
	 * \param  samples 	The sample array that is tested.
	 *
	 * \throw 	If *this is invalid, the dimensions are wrong
	 * 		 or an invalid sample is detected.
	 */
	bool
	isInside(
		const SampleArray_t& 	samples)
	 const;


	/**
	 * \brief	Test if all samples in the sample containers
	 * 		 are inside *this.
	 *
	 * This function operates on the passed sample list.
	 *
	 * \param  samples 	The sample list that is tested.
	 *
	 * \throw 	If *this is invalid, the dimensions are wrong
	 * 		 or an invalid sample is detected.
	 */
	bool
	isInside(
		const SampleList_t& 	samples)
	 const;


	/**
	 * \brief	This function checks if at least one dimension of \c x is close to the boundary of \c *this .
	 *
	 * \param  x	the sample to test
	 */
	bool
	isCloseToUpperBound(
		const Sample_t&	x,
		const Numeric_t&	n = 100)
	  const
	{
		yggdrasil_assert(this->isValid());

		if(this->nDims() != x.nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimensions does not agree, the cube has " + std::to_string(size()) + " dimensions, but the sample has " + std::to_string(x.nDims()));
		};

		const Int_t N = x.nDims();
		for(Int_t d = 0; d != N; ++d)
		{
			if(m_bounds[d].isCloseToUpperBound(x[d], n) == true)
			{
				return true;
			};
		}; //End for(d):

		return false;
	}; //End: is close to upper boundary

	/**
	 * \brief	This fucntion tests if *This is wekly contained inside other.
	 *
	 * \param  other	The other cube.
	 */
	bool
	isWeaklyContainedIn(
		const yggdrasil_hyperCube_t&	other)
	 const
	{
		//Make some checks
		yggdrasil_assert(other.isValid());
		yggdrasil_assert(this->isValid());

		//Test if the dimension aggrees
		if(this->nDims() != other.nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The number of dimension is not the same.");
		};

		//The numebr of fdiemsnions
		const auto N = this->nDims();

		//Gow through the dimens and tests
		for(Int_t i = 0; i != N; ++i)
		{
			if(m_bounds[i].isWeaklyContainedIn(other.m_bounds[i]) == false)
			{
				return false;
			};
		}; //End for(i):

		return true;
	}; //End: weakly contained in


	/*
	 * ====================
	 * Condition intersection
	 */
public:
	/**
	 * \brief	This function returns true if *this has an
	 * 		 intersection with the condition c.
	 *
	 * The intersection is determined by testing if all conditions lies
	 * within the hypercube defined by *this.
	 * It is not tested if the condition and *this is valid.
	 *
	 * \param  condi 	A condition object.
	 *
	 * \throw 	If inconsistencies are detected.
	 */
	bool
	intersectsWithCondition(
		const MultiCondition_t& 	condi)
	 const
	{
		//Make some tests, only for the debuig mode
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(condi.isValid());

		//test the dimensionality
		if(this->m_bounds.size() != condi.nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension of the hyper cube, " + std::to_string(m_bounds.size()) + ", does not match with the one of the condition " + std::to_string(condi.nDims()));
		};

		//If there are no conditions to be applied, *this meet them
		//because of no violation.
		if(condi.noConditions() == true)
		{
			return true;
		}; //End if: trivial case, no restrictions

		/*
		 * Now we will go through all the conditions and test
		 * if they are applicable to *this
		 */
		for(const SingleCondition_t& dCondi : condi)
		{
			//Make some tests
			yggdrasil_assert(Size_t(dCondi.getConditionDim()) < m_bounds.size());
			yggdrasil_assert(isValidFloat(dCondi.getConditionValue()));

			/*
			 * We do not check fo intersection, instead we check for not
			 * intersection.
			 */
			if(m_bounds[dCondi.getConditionDim()].intersectWithCondition(dCondi) == false)
			{
				/*
				 * The current dimension does not intersect this at all
				 * so *this can not met this condition.
				 * So we returnfalse
				 */
				return false;
			}; //End if: no intersection
		}; //ENd for(dCondi)


		/*
		 * When we are here, *this was able to met all conditions
		 * we have imposed, thus we habe to return true to indicate this.
		 */
		return true;
	}; //End: does there is an intersection




	/*
	 * ======================
	 * Mapping function
	 *
	 * This is like the 1D interval that is able to map from and to the unit interval and the interval itself.
	 */
public:
	/**
	 * \brief	This function mapps a sample which components are
	 * 		 in \f$ [0, 1[^n \f$ into the hypercube of \c *this .
	 *
	 * There is no check performed if the sample is in the unit interval.
	 * This basically aplies the transformation to each dimension.
	 *
	 * This function is not very efficient and should be only used if neccessary.
	 *
	 * \param  x		The smaple in the unit interval.
	 *
	 * \throw 	The dimensions must agree.
	 *
	 * \note 	In earlier version of this function, it was requiered that
	 * 		 *thsi is valid, this restriction was removed.
	 */
	Sample_t
	mapFromUnit(
		const Sample_t&		x)
	 const
	{
		//The underling fucntion performs the test
		Sample_t s(x);

		//Perform the transformation
		this->mapFromUnit_inPlace(s);

		//Teturn the mapped
		return s;
	}; //End: Mapfrom unit on copy


	/**
	 * \brief	This function transforms the sample from the unit interval to the
	 * 		 cube described by *this.
	 *
	 * This function operates on the sample directly, thus its name.
	 *
	 * \param  x		The smaple in the unit interval.
	 *
	 * \throw 	This function throws if \c *this is invalid.
	 * 		 Also the dimensions must agree.
	 */
	void
	mapFromUnit_inPlace(
		Sample_t& 	s)
	 const
	{
		yggdrasil_assert(this->isValid());

		//get the dimension of the sample
		const Int_t N = s.nDims();

		if(this->nDims() != N)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Dimension missmatch; Cube has " + std::to_string(nDims()) + " dimensions, but sample has " + std::to_string(N));
		};

		//Make the transformation
		for(Int_t d = 0; d != N; ++d)
		{
			s[d] = m_bounds[d].mapFromUnit(s[d]);
		};

		return;
	}; //End: mapFromUnit_inPlace



	/**
	 * \brief	This function mapps a bunch of numbers from the unit interval, \f$ [0.0, 1.0[ \f$ into
	 * 		 the interval that defines the bound in  dimension \c d.
	 *
	 * This interval acts on the dmension d of a sample collection, which is not passed to \c *this .
	 * This means it does not act on many samples but on one dimension of many samples.
	 * This function does not check if the argumens are inside the unit interval.
	 *
	 * \param  d		The dimension we act on.
	 * \param  sa 		The samples array we act on-
	 *
	 * \return 	This function returns a value array.
	 *
	 * \throw 	This function throws if the dimensions of the array and \c d does not agree.
	 *  		 Another reason for an exception if \c *this is not valid or if the array is not valid.
	 *
	 * \note 	This function does not check the arguments but assers it.
	 */
	ValueArray_t
	mapFromUnit(
		const Int_t 		d,
		const DimensionArray_t&	sa)
	 const;


	/**
	 * \brief	This function transform the conditions from the unit
	 * 		 cube to the cube defined by this.
	 *
	 * This function is used to invecrse the contract function that
	 * operates on conditon objects. This function can be used
	 * to map a condition from the root data space to the global
	 * data space. Use cases of this function are quite exotic.
	 *
	 * Note a copy of the condition is returned.
	 *
	 * \param  condi	Conditions that are defined on the unit hyper cube.
	 *
	 * \throw 	If inconsistency arrises.
	 */
	MultiCondition_t
	mapFromUnit(
		const MultiCondition_t& 	condi)
	 const;





	/**
	 * \brief	This function mapps the samples dimension \c d associated to \c sa
	 * 		 from the respecitve interval of \c *this to the unit interval.
	 *
	 * This interval acts on the dmension d of a sample collection, which is not passed to \c *this .
	 * This means it does not act on many samples but on one dimension of many samples.
	 *
	 * \param  d		The dimension we act on.
	 * \param  sa 		The samples array we act on.
	 *
	 * \return 	This function returns a value array.
	 *
	 * \throw 	This function throws if the dimensions of the array and \c d does not agree.
	 *  		 Another reason for an exception if \c *this is not valid or if the array is not valid.
	 *
	 * \note 	This function does not check the arguments but asserts it.
	 */
	ValueArray_t
	contractToUnit(
		const Int_t 		d,
		const DimensionArray_t&	sa)
	 const;



	/**
	 * \brief	This function contracts a smaple to the unit interval.
	 *
	 * It does not check if the sample lies inside this (however it is asserted).
	 *
	 * \param  sample	The sample that will be contracted.
	 */
	Sample_t
	contractToUnit(
		const Sample_t&		sample)
	 const
	{
		//Make somne tests for debuging
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(this->isInside(sample));
		yggdrasil_assert(this->nDims() == sample.nDims());

		//Now make the contraction (NRVO)
		Sample_t nrvo(sample);

		//Contract the sample; all tests in relese mode
		//will be done by this function
		this->contractToUnit_inPlace(&nrvo);

		return nrvo;
	}; //End: contractToUnit


	/**
	 * \brief	This function contracts a smaple to the unit interval, in place.
	 *
	 * It does not check if the sample lies inside this (however it is asserted).
	 * Also the fucntion does not return a contarcted sample, but modifies
	 * its imput argument.
	 *
	 * \param  sample	The sample that will be contracted.
	 */
	void
	contractToUnit_inPlace(
		Sample_t* const		sample)
	 const
	{
		//Test if the nullpointer was passed
		if(sample == nullptr)
		{
			throw YGGDRASIL_EXCEPT_NULL("The nullptr was passed.");
		};
		if(sample->nDims() != this->nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension of the sample and the cube does not agree.");
		};

		//Test the validiy of the input arguments
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(this->isInside(*sample));

		//Make a reference to the sample
		Sample_t& nrvo = *sample;

		//Iterate through the dimension and contract it
		const Size_t D = this->m_bounds.size();
		for(Size_t i = 0; i != D; ++i)
		{
			//Load the value
			const auto val = nrvo[i];

			//Perform the transformation and save the result.
			nrvo[i] = m_bounds[i].contractToUnit(val);
		}; //ENd for(i)

		//This is another check that is a bit redundant, but it reveles some
		//problems with the numerics
		yggdrasil_assert(nrvo.isInsideUnit() == true);

		return;
	}; //End: contractToUnit_inPlace


	/**
	 * \brief	This function contrats the consition from
	 * 		 *this to the unit hyper cube.
	 *
	 * This fucntion is ment for transforming the conditions,
	 * which are definied on the global data space, to the
	 * root data space.
	 *
	 * Note the condition is not changed, since it is an imputable
	 * object, but a new one is constructed.
	 *
	 * \param  condi	This are the conditions that should be mapped.
	 *
	 * \throw 	If inconsistencies arrise.
	 */
	MultiCondition_t
	contractToUnit(
		const MultiCondition_t& 	condi)
	 const;



	/*
	 * =================
	 * General utility functions
	 */
public:
	/**
	 * \brief	Returns true if dimension \c d is valid.
	 *
	 * This checks if the dimension \c d is valid.
	 * Note this only checks the interval.
	 * Due to the full definition of a valid cube, it could be possible
	 * that every single interval is valid on its own, but together
	 * ith the other interval, this is not the case.
	 *
	 * \param  d	The dimension to check.
	 *
	 * \throw 	This function throws if dimension \c d does not exits
	 *
	 * \note 	Only dimension \c d is checked.
	 */
	bool
	isValid(
		const Int_t 	d)
	 const
	{
		//The at will cause an error, that is not realy related, I want to make a clear one
		if(m_bounds.empty() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("called the isValid(dim) function on a cube with zero dimenesion.");
		};

		return m_bounds.at(d).isValid();
	}; //End: isValid(dim)


	/**
	 * \brief	This function tests if all dimensions are valid.
	 *
	 * The valid check does not simply only check if all dimensions
	 * are valid. It also checks if the volume of *this and its
	 * inverse exists and are considered valid.
	 * This is done since this are the stuff we need.
	 *
	 * \note	A hypercube of dimension 0 is technically valid.
	 * 		 For our purpose this is useless and we define that case
	 * 		 to be invalid.
	 *
	 * \note	Also note that it is possible that each interval on oits own is valid.
	 * 		 But all intervals together are not. This is a rather strange situation.
	 * 		 If this happens, you have most likely other problems.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		//We requere to have at least one dimension.
		if(m_bounds.empty() == true)
		{
			return false;
		};

		Real_t volume = 1.0;

		for(const IntervalBound_t& i : m_bounds)
		{
			if(i.isValid() == false)
			{
				return false;
			};

			//Incooperate the volume of the current bound
			const auto cbLength = i.getLengthReal();
			yggdrasil_assert(isValidFloat(cbLength));
			yggdrasil_assert(cbLength > 0.0);

			volume *= cbLength;
		}; //End for(i): checking the dimensions

		/*
		 * Lets test if the value is not zero or below.
		 *
		 * It should never reach negative value and
		 * achiving that is serious prioblem.
		 * This is a serious problem, and I would throw an exception
		 * but we are in a no except fucntion.
		 * So lets hope that the caller code will ensure that.
		 */
		if(volume <= 0.0 || isValidFloat(volume) == false)
		{
			return false;
		};

		//Now we must check if the inverse of the volume exists
		const Real_t iVolume = 1.0 / volume;

		//Test if the volume has become some funky value
		if(isValidFloat(iVolume) == false)
		{
			return false;
		};


		/*
		 * If we are here, then we are most likely values.
		 * At least we pass all tests that we have come up with
		 */
		return true;
	}; //End: isValid


	/**
	 * \brief	Returns true if _all_ dimensions are invalid.
	 */
	bool
	allDimsInvalid()
	 const
	 noexcept
	{
		for(const IntervalBound_t& i : m_bounds)
		{
			if(i.isValid() == true)
			{
				return false;
			};
		}; //End for(i): checking the dimensions

		return true;
	};


	/**
	 * \brief	Get the numbers of dimensions of \c *this .
	 *
	 * This function returns an signed integer.
	 * This is due to consistency.
	 */
	Int_t
	nDims()
	 const
	 noexcept
	{
		return (Int_t)m_bounds.size();
	};


	/**
	 * \brief	Get the numbers of dimensions.
	 *
	 * This function returns a size_t.
	 */
	Size_t
	size()
	 const
	 noexcept
	{
		return m_bounds.size();
	};


	/**
	 * \brief	Computes the volume of \c *this .
	 *
	 * This calculate the products of the length of the
	 * different sub intervals.
	 *
	 * \throw 	This function throws if \c *this is invalid.
	 *
	 * \note	Due to our definition that a zero dimensional hypercube is
	 * 		 is invalid, the volume of a zero dimensional cube can not be
	 * 		 computed, since this raises an exception.
	 */
	Real_t
	getVol()
	 const
	{
		//We want to report this error seperatly
		if(m_bounds.empty() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The hypercube has dimension zero. Volume undifined.");
		};

		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not calculate the value of an invalid cube.");
		};

		Real_t vol = 1;
		for(const auto& l : m_bounds)
		{
			vol *= l.getLengthReal();
		}; //End for(l):

		return vol;
	}; //End: getVol


	/**
	 * \brief	This function is able to print out the domains.
	 *
	 * The cube is outputed by using the print function of the intervals.
	 * They are tied together with {}.
	 * As with the 1D interval the user can set if paranteheses are used or not
	 *
	 * \param  useBrackets 		Should [] be used; Defaulted to false
	 */
	std::string
	print(
		const bool 	useBracket = false)
	 const
	{
		std::stringstream s;
		bool isFirstIt = true;

		s << '{';
		for(const auto& i : m_bounds)
		{
			if(isFirstIt == false)
			{
				s << ", ";
			}; //End seperater

			s << i.print(useBracket);
			isFirstIt = false;
		}; //End for(i):

		s << '}';

		return s.str();
	}; //End printing



	/*
	 * ====================
	 * Access Functions
	 *
	 * There are only const versions.
	 */
public:
	/**
	 * \brief	Returns a constant reference to the interval of dimension \c d .
	 *
	 * \param  d	This is the dimension to access.
	 *
	 * \throw 	This function throws if \c d does not exists.
	 */
	const_reference
	at(
		const Size_t		d)
	 const
	{
		return m_bounds.at(d);
	};


	/**
	 * \brief	This fucntion returns a constant reference of the bound in dimension d.
	 *
	 * This fnction does not throw.
	 * But guards the dimension with an assert
	 *
	 * \param  d	This is the dimension to access
	 */
	const_reference
	operator[] (
		const Size_t 		d)
	 const
	 noexcept
	{
		yggdrasil_assert(d < this->size());
		return m_bounds[d];
	};


	/**
	 * \brief	Get the lower bound of the interval that defines dimension \c d .
	 *
	 * \param  d 	The diension to retrive.
	 *
	 * \throw 	If the dimension \c d does not exists an exception is raised.
	 */
	Numeric_t
	getLowerBound(
		const Size_t 	d)
	 const
	{
		return m_bounds.at(d).getLowerBound();
	};


	/**
	 * \brief	Get the upper bound of th einterval in dimension \c d .
	 *
	 * \param  d 	The dimension to retrive.
	 *
	 * \throw 	If the dimension \c d does not exists an exception is raised.
	 */
	Numeric_t
	getUpperBound(
		const Size_t 	d)
	 const
	{
		return m_bounds.at(d).getUpperBound();
	};


	/*
	 * ====================
	 * Iterators
	 *
	 * Only const iterators are supported.
	 */
public:
	/**
	 * \brief	Get a const interator to the interval of the first diemnsion, 0.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		return m_bounds.cbegin();
	};


	/**
	 * \brief	Get a const iterator to the internal of the first dimension.
	 *
	 * This is the explicit version.
	 */
	const_iterator
	cbegin()
	 const
	 noexcept
	{
		return m_bounds.cbegin();
	};


	/**
	 * \brief	Return the const past the end iterator to the intervals.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		return m_bounds.cend();
	};


	/**
	 * \brief	Return the const past the end iterator to the intervals.
	 *
	 * This is the explicit version.
	 */
	const_iterator
	cend()
	 const
	 noexcept
	{
		return m_bounds.cend();
	};


	/*
	 * =======================
	 * Comparision operators
	 */
public:
	/**
	 * \brief	Returns true if *this is less than other.
	 *
	 * The less condition is establishing by performing a
	 * lexicographical coparison of the lower bounds of the cubes.
	 * *this is interpreted as lhs and ither as rhs.
	 *
	 * \param rhs 		The right hand side that we compare.
	 *
	 * \throw	If *this or rhs are not valid (only in debug), or the dimensions
	 * 		 does not match.
	 */
	bool
	operator< (
		const yggdrasil_hyperCube_t& 	rhs)
	 const
	{
		//test if they are valid
#ifndef NDEBUG
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("*this (lhs) is not a valid hypercube.");
		};
		if(rhs.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("rhs is not a valid hyper cube.");
		};
#endif
		if(this->nDims() != rhs.nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimensions of teh lhs and the rhs does not agree.");
		};

		//This is the numnber of dimensions we are going to check
		const Size_t N = this->m_bounds.size();

		//This bool stoires if at least one element was fiund that is not equal
		bool foundUnequalComponent = false;

		//Going through the dimensions and apply the ordering
		for(Size_t i = 0; i != N; ++i)
		{
			//Load the two bounds
			const Numeric_t lhs_bound = this->getLowerBound(i);
			const Numeric_t rhs_bound = rhs.getLowerBound(i);

			/*
			 * Test if they are different.
			 *
			 * If they are the same, we need to do nothing, except considering
			 * the next dimension.
			 */
			if(lhs_bound != rhs_bound)
			{
				/*
				 * The bounds are diffetent, so we must check
				 * for less
				 */

				//Store that we have found a component that is not equal
				foundUnequalComponent = true;

				//Perform the less test
				if(lhs_bound < rhs_bound)
				{
					/*
					 * The lhs bound is less than the rhs bound.
					 * This means we have to consider the nexct level
					 * ~ doing nothing.
					 */
					continue;
				}
				else
				{
					/*
					 * If we are here we know that lhs_bound > rhs_bound,
					 * since we have already tested fro equality.
					 * in this case we are done and return false, because
					 * *this is not less than rhs.
					 */
					return false;
				}; //End if: less check
			}; //End if: bound are different
		}; //ENd for(i):

		/*
		 * If we are here then *this is equal to rhs, in every component or
		 * *this is indeed before rhs.
		 */
		return (
			  (foundUnequalComponent == true)	//We found a component where the values are different
			?
			   true 	//In the case of an unequal component, we know that *this must be less than rhs.
			:
			   true		//We did not found an unequal component, so all must be equal, thus *this is nto less
		       );		//  than rhs.
	}; //End: operator< with HyperCube


	/**
	 * \brief	This fucntion checks if the sample s is larger or equal
	 * 		 than the lower bound of *this, this test is applied for each component.
	 *
	 * \throw	If inconsistencies are detected.
	 */
	bool
	operator<= (
		const Sample_t& 	s)
	 const
	{
		//Test the dimensions
		if(this->nDims() != s.nDims())
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension of the hyper cube (" + std::to_string(this->nDims()) + ") and the dimension of the sample (" + std::to_string(s.nDims()) + ") does not agree.");
		}
		yggdrasil_assert(this->isValid());
		yggdrasil_assert(s.isValid());

		//Cache the dimension
		const Size_t N = s.nDims();

		//DO the test
		for(Size_t i = 0; i != N; ++i)
		{
			if(s[i] < m_bounds[i].getLowerBound())     //~ !(m_bounds[i].getLowerBound() <= s[i]))
			{
				/*
				 * We found a component that is not above the lowe bound
				 * so we return false
				 */
				return false;
			}; //End if:
		}; //End for(i):


		/*
		 * If we are here all components are above the the bound
		 * so we must return true
		 */
		return true;
	}; //End: operator<= (Sample)



	/*
	 * =======================
	 * Public Static functions
	 */
public:
	/**
	 * \brief	This function returns an invalid hyper cube.
	 *
	 * It is the only way to create a truly invalid cube.
	 * It has zero dimensions.
	 */
	static
	yggdrasil_hyperCube_t
	MAKE_INVALID_CUBE()
	{
		return yggdrasil_hyperCube_t();
	};


	/**
	 * \brief	This function returns an invalid hyer cube.
	 *
	 * There are some difference between this function and
	 * the MAKE_INVALID_CUBE() function.
	 * The main difference is that this one accepts an
	 * argument that can set the dimension of the invalid cube.
	 * If teh argument is zero, the default, then this function
	 * is equvivalent to MAKE_INVALID_CUBE().
	 * If grater than one it is like the constructor that takes an argument.
	 *
	 * This function is mainly provided for the Python interface.
	 *
	 * \param  d 		The dimension of teh cube; defualts to 0.
	 */
	static
	yggdrasil_hyperCube_t
	CREAT_INVALID_CUBE(
		const Size_t d = 0)
	{
		if(d == 0)
		{
			return yggdrasil_hyperCube_t();
		}
		else
		{
			return yggdrasil_hyperCube_t(d);
		};

		/*
		 * This will not be reached.
		 */
	}; //End: create invalid cube


	/**
	 * \brief	This function creates a n dimensional unit hypercube
	 */
	static
	yggdrasil_hyperCube_t
	MAKE_UNIT_CUBE(
		const Int_t 	n)
	{
		return yggdrasil_hyperCube_t(n, IntervalBound_t::CREAT_UNIT_INTERVAL());
	};


	/*
	 * ============================
	 * Friends
	 */
private:
	/**
	 * \brief	This fucntion is able to test if all samples of the
	 * 		 passed container lies inside self.
	 *
	 * \param  self 	This the the hyper cube that is used.
	 * \param  samples	This is the sample container.
	 *
	 * \tparam  Container_t This is the container that is used. It must
	 * 			 model the UCI.
	 *
	 * \note	This function is onmly used for the list and the array
	 * 		 The sample collection is implemented seperatly.
	 */
	template<
		class 	Container_t
	>
	friend
	bool
	ygInternal_testIfAllInside(
		const yggdrasil_hyperCube_t& 		self,
		const Container_t& 			samples);


	/*
	 * ==========================
	 * Private variable
	 */
private:
	Vector_t 		m_bounds;	//!< This variable stores all the boundaries
}; //End class: yggdrasil_hyperCube_t




YGGDRASIL_NS_END(yggdrasil)




