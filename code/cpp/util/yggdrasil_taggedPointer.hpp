#pragma once
/**
 * \brief	The tagged point is a pointer that is able to store some payload.
 *
 * The tagged pointer is an important helper class inside yggdrasil.
 * The user should not actively use it.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

//INcluide std
#include <type_traits>
#include <climits>
#include <cstddef>

YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \class	yggdrasil_taggedPointer_t
 * \brief	This is a non owning pointer that is able to carry 8Bit payload.
 *
 * This class build on the fact, that the upper part of an address are zero
 * anyway (in usersapce). So this function assumes that the highest 8bit of
 * any pointer are zero to begin with, there are checks to ensure that this
 * is indeed the case. This 8Bit can be used to store some meta information.
 *
 * It is primarly used inside the node, where the tag, another name for the
 * payload, representsd the dimension where the split happened.
 * This is class is also the reason why the dimensionality of the samples
 * is limited to 255.
 *
 * This class is also able to hold an array, in this case the operator[](Size_t)
 * is pressent. The array fucntionality can be activated by setting the
 * second template argument to true.
 *
 * This pointer is inspired by the unuique pointer, with some differences.
 * The most important one is that *this does not own the pointer
 * as the unique pointer does. So when *this goes out of scope, it
 * simply goes out of scope, without freeing the pointer.
 * This must be do by hand.
 * It also allows that *this can be copied.
 */
template<
	class 	T,
	bool	isArray = false
	>
class yggdrasil_taggedPointer_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using value_type 	= T;			//!< This is the underling type
	using reference 	= value_type&;		//!< This is the reference
	using pointer 		= T*;			//!< This is the pointer type
	using Tag_t 		= uInt8_t;		//!< This is the type of the tag
	static_assert(sizeof(Int8_t) * CHAR_BIT == 8, "The size of the tag is not correct.");
	static_assert(sizeof(pointer) == 8, "Your address space is not large enough.");

	/*
	 * =================
	 * Internal constants
	 */
private:
	/*
	 * This is the mask for removing the payload,
	 * the highest byte is zero and the rest are all one.
	 */
	constexpr static Size_t MASK_REMOVE_TAG = 0x00ffffffffffffff;

	/*
	 * This is the number of bits to shift to get the tag at the beginning
	 */
	constexpr static Size_t SHIFT_TAG_TO_START = 64 - 8;

	/*
	 * This is needed for cleaining the tag.
	 */
	constexpr static Size_t MASK_CLEAING_TAG = 0x00000000000000ff;

	/*
	 * This are some internal macros
	 */
#define GET_PURE_ADDRESS(p) (T*)(Size_t(p) & MASK_REMOVE_TAG)


	/*
	 * ===================
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * Constructs *this with an address.
	 * The tag is zero.
	 *
	 * \param  p 	The address that the pointer points to.
	 *
	 * \throw 	If the pointer is to large. This will probably never happen.
	 */
	yggdrasil_taggedPointer_t(
		pointer		p) :
	  yggdrasil_taggedPointer_t()
	{
		this->setPointerTo(p);		//This will set the tag to zero.
	};


	/**
	 * \brief	Building constructor.
	 *
	 * This function is here to have a constrauctor of the form 'yggdrasil_taggedPointer_t(nullptr)' that
	 * does not throw.
	 */
	constexpr
	yggdrasil_taggedPointer_t(
		std::nullptr_t)
	 noexcept :
	  m_pointer(nullptr)
	{};


	/**
	 * \brief	Default constructor.
	 *
	 * Sets \c *this to the null pointer.
	 * The tag is also set to zero.
	 */
	constexpr
	yggdrasil_taggedPointer_t()
	 noexcept :
	  m_pointer(nullptr)
	{};


	/**
	 * \brief	The copy constructor.
	 *
	 * Copies \c other into \c *this .
	 * Notice that the unique poiinter does not
	 * provide such a constructor.
	 *
	 * Not only the address but also the tag is copied.
	 */
	yggdrasil_taggedPointer_t(
		const yggdrasil_taggedPointer_t& other)
	 noexcept :
	  m_pointer(other.m_pointer)
	{};


	/**
	 * \brief	The move constructor.
	 *
	 * This constructor does behaves as the unique_ptr does.
	 * Concrete this means that the address and the tag of
	 * other are set to all zero.
	 *
	 * \param  other	The pointerthat should be loaded.
	 */
	yggdrasil_taggedPointer_t(
		yggdrasil_taggedPointer_t&& other)
	 noexcept :
	  m_pointer(other.m_pointer)
	{
		other.m_pointer = nullptr;
	};


	/**
	 * \brief	The copy constructor overwrite \c *this with a copy of \c other .
	 *
	 * Both instances are then pointing to the \em same memory address.
	 * This is not a problem since nither frees them automatically.
	 * The user has to do than.
	 *
	 * Also note that the tag is also copied.
	 */
	yggdrasil_taggedPointer_t&
	operator= (
		const yggdrasil_taggedPointer_t& other)
	  noexcept
	{
		this->m_pointer = other.m_pointer;

		return *this;
	};


	/**
	 * \brief	The move assignment moves the pointer from \c other
	 * 		 into \c *this; \c other is set to null.
	 *
	 * This is like first deliting \c *this and then using the move
	 * constructor to construct \c *this form \c other anew.
	 *
	 * This means other is set to all zero and the content,
	 * address and tag, now reside in *this.
	 *
	 * \param  other	The pointer that is loaded and clerared.
	 *
	 */
	yggdrasil_taggedPointer_t&
	operator= (
		yggdrasil_taggedPointer_t&& other)
	  noexcept
	{
		this->m_pointer = other.m_pointer;
		other.m_pointer = nullptr;

		return *this;
	};


	/**
	 * \brief	The destructor sets the pointer of *this to null.
	 *
	 * This is actually unnecessary, but who cares.
	 */
	~yggdrasil_taggedPointer_t()
	{
		this->m_pointer = nullptr;
	};


	/*
	 * =======================
	 * Access Operators
	 *
	 * Here are the functions that allows accessing the stored data
	 */
public:
	/**
	 * \brief	Single dereferencing.
	 *
	 * There is an assert that test if *this is not the nullpointer.
	 *
	 * \throw 	This function may throw if T has a throwing * operator
	 *
	 * \note 	The function is const but only returns a reference.
	 * 		This behaviour is consistent with the one of unique_ptr.
	 */
	reference
	operator* ()
	  const
	{
		yggdrasil_assert(GET_PURE_ADDRESS(m_pointer) != nullptr);

		return *(GET_PURE_ADDRESS(m_pointer));
	};


	/**
	 * \brief	This is the -> operator.
	 *
	 * This fnction asserts that \c *this is not the nullptr.
	 */
	pointer
	operator-> ()
	  const
	  noexcept
	{
		yggdrasil_assert(GET_PURE_ADDRESS(m_pointer) != nullptr);

		return (GET_PURE_ADDRESS(m_pointer));
	};


	/**
	 * \brief	This is the subskript operator.
	 *
	 * Inspired by the unique pointer this only returns a reference.
	 * It tests if \c *this is not the nullptr.
	 *
	 * \note 	It is only present if the array functionality is enabled.
	 */
	template<bool _isArray = isArray>
	typename std::enable_if<_isArray == true, reference>::type
	operator[](
		const Size_t 	i)
	  const
	{
		static_assert(isArray == _isArray, "Must be the same; No cheating.");

		yggdrasil_assert(GET_PURE_ADDRESS(m_pointer) != nullptr);
		return *((GET_PURE_ADDRESS(m_pointer)) + i);
	};


	/**
	 * \brief	This function returns the pointer.
	 *
	 * This returns the address of the pointer that is stored.
	 * The tag that is also atteched to the address is stripped
	 * before the real addrss is returned.
	 */
	pointer
	get()
	  const
	  noexcept
	{
		return GET_PURE_ADDRESS(m_pointer);
	};


	/*
	 * =========================
	 * Pointer Managing functions
	 */
public:
	/**
	 * \brief	This funtion tests if *this is the nullpointer.
	 *
	 * This test only involves the real addrsss, as returned by get().
	 * The value of the tag is ignored.
	 */
	bool
	isNull()
	  const
	  noexcept
	{
		return (GET_PURE_ADDRESS(m_pointer) == nullptr) ? true : false;
	};


#if 0
	/**
	 * \brief	This function sets *this to nullpointer, tag is deleted.
	 *
	 * The tag is also set to zero.
	 * Since the tag should be code the pointer removing the pointer
	 * should also remove the tag.
	 */
	void
	setZero()
	  noexcept
	{
		m_pointer = nullptr;	//Will clear also the tag.
		return;
	};
#endif


	/**
	 * \brief	This function loads a new pointer \c p into \c *this,
	 * 		 the tag \c *this had before is removed.
	 *
	 * Again the tag is tied to the pointer, and will thus
	 * be removed, meaning that all bits are set to zero.
	 * Address and tag.
	 * This means afterwards, hasTag() will return false.
	 * Remember that also 0 can have a meaning as tag.
	 *
	 * \throw 	If the pointer occupies more space than assumed.
	 *
	 * \param  p	The new address the \c *this points to.
	 */
	void
	setPointerTo(
		const pointer 	p)
	{
		//Test if the higest byte is free
		if(p != GET_PURE_ADDRESS(p))
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("A pointer is to long, can not operate with it.");
		};

		m_pointer = GET_PURE_ADDRESS(p);	//Will remove the tag

		return;
	}; //End: setPointerTo


	/**
	 * \brief	This function exchanges the pointer \c *this currently
	 * 		 points to with \p. However the tag is preserved, and
	 * 		 remains the same.
	 *
	 * This function is different from the setPointerTo() function by
	 * preserving the tag value of *this.
	 * This means the pointer of *this is set to p, but the tag is the same
	 * as it wqas before.
	 *
	 * \throw 	If the new pointer is to long.
	 *
	 * \param  p 	The new address.
	 */
	void
	exchangePointer(
		const pointer p)
	{
		//Test if the higest byte is free
		if(p != GET_PURE_ADDRESS(p))
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("A pointer is to long, can not operate with it.");
		};

		m_pointer = pointer((Size_t)GET_PURE_ADDRESS(p) | ((Size_t)m_pointer & (~MASK_REMOVE_TAG)));
		return;
	}; //End: exchangePointer



	/*
	 * ====================
	 * Tag management
	 */
public:
	/**
	 * \brief	This function returns the tag of \c *this .
	 */
	Tag_t
	getTag()
	  const
	  noexcept
	{
		return Tag_t( ((Size_t)m_pointer >> SHIFT_TAG_TO_START) & MASK_CLEAING_TAG);
	};


	/**
	 * \brief	Test if the tag is different from zero .
	 *
	 * \note 	A tag of zero can also have a meaning.
	 * 		 Please keep that in mind.
	 */
	bool
	hasTag()
	  const
	  noexcept
	{
		return (Size_t((Size_t)m_pointer & (~MASK_REMOVE_TAG)) == 0) ? false : true;
	};


	/**
	 * \brief	Removes the tag of \c *this , the address remains unchanged.
	 *
	 * This function sets the tag to zero.
	 * Again please keep in mind, that a tag value of zero can also bear a meaning.
	 */
	void
	removeTag()
	  noexcept
	{
		m_pointer = GET_PURE_ADDRESS(m_pointer);
		return;
	};


	/**
	 * \brief	This function sets the tag of \c *this to \c newTag .
	 *
	 * The address of \c *this reamins unchanged.
	 *
	 * \param  newTag	This is the new tag of \c *this .
	 */
	void
	setTag(
		const Tag_t 	newTag)
	  noexcept
	{
		m_pointer = pointer(Size_t(GET_PURE_ADDRESS(m_pointer)) | (Size_t(newTag) << SHIFT_TAG_TO_START));
		return;
	};



	/*
	 * ======================
	 * Comparison operators
	 *
	 * The overloaded operators acts on the address of
	 * the pointer alone, the value of the tag is ignored.
	 */
public:
	/**
	 * \brief	Returns true if *this and other has the same tag.
	 *
	 * This only compares the tag value.
	 *
	 * \note 	They do not to be of the same type.
	 *
	 * \param  other 	This is the other instance we want to compare to.
	 */
	template<
		class 	T1,
		bool 	isArray1
		>
	bool
	hasSameTagAs(
		yggdrasil_taggedPointer_t<T1, isArray1>& other)
	  const
	  noexcept
	{
		return (this->getTag() == other.getTag()) ? true : false;
	};


	/**
	 * \brief	Compares the address of *this with other.
	 *
	 * They have to be of the same type.
	 */
	bool
	operator== (
		const yggdrasil_taggedPointer_t& 	rhs)
	  const
	  noexcept
	{
		return (this->get() == rhs.get()) ? true : false;
	};


	/**
	 * \brief	Returns true if the *this and rhs are not the same pointer.
	 */
	bool
	operator!= (
		const yggdrasil_taggedPointer_t&	rhs)
	  const
	  noexcept
	{
		return (this->get() != rhs.get()) ? true : false;
	};


	/**
	 * \brief	Makes a less than test on the pointer.
	 *
	 * \param  rhs		The right side of the comparisson.
	 */
	bool
	operator< (
		const yggdrasil_taggedPointer_t& 	rhs)
	  const
	  noexcept
	{
		return (this->get() < rhs.get()) ? true : false;
	};


	/*
	 * ================
	 * Private Member
	 */
private:
	pointer 	m_pointer;	//!< This is the pointer to the _observed_ data

#undef GET_PURE_ADDRESS

}; //End class: yggdrasil_tagggedPointer_t


YGGDRASIL_NS_END(yggdrasil)





