#pragma once
/**
 * \brief	This class implements generall bins, that can be used on any interval.
 *
 * It is based on the one that was originally designed for the chi2 tests.
 * But since it was noticed that it has to be used elsewhere too, it was
 * moved.
 * It is basically an array that offers nice access to it.
 *
 * If it manages n bins, then the underling array has a length of n + 1.
 * The value that is stored in array posistion i, is the lower bound of
 * the ith bin, the value stored at location i + 1, is the upper bound
 * of the ith bin. At the same time it is also the lower bound of the
 * i+1 bin.
 * The last array position is only the upper bound of the last bin.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>


//Incluide std

//Include boost


YGGDRASIL_NS_BEGIN(yggdrasil)


/**
 * \class	yggdrasil_genBinEdges_t
 * \brief	This is a simple class that is able to model a binned interval.
 *
 * This is a class that provides a conventient wrapper arround an array.
 * It mnodels a binned interval.
 * It devides an interval into several smaller interval.
 * Not that we use the notion of interval that is common inside Yggdrasil.
 *
 * The format that is used to store the edges, is the same as it is used throughout Yggdrasil.
 * The bins are actually little intervals, as in yggdrasil, an interval is a right open
 * mathematical interval, which means that the upper bound does not belongs to the interval.
 * Assume that there are n bins, then this class is basically an array of size n + 1.
 * The value at index i, is the lower bound of the bin i.
 * It is important that a sample with that value, belonges to interval i.
 * The value at index i + 1 is upper bound of the interval i and the lower bound
 * of bin i + 1, it is important that a smple with that value belongs to the bin i + 1
 * rather than bin i.
 * the last value, n + 1, in the array is the upper bound of the last bin.
 * samples with that values does nto belongs to this interval.
 */
class yggdrasil_genBinEdges_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Numeric_t 	= ::yggdrasil::Numeric_t;	//!< This is the underling numeric type.
	using Real_t 		= ::yggdrasil::Real_t;		//!< This is the high precission type.
	using Size_t 		= ::yggdrasil::Size_t;		//!< This is the size type, it is different form the other usages
	using ValueArray_t      = ::yggdrasil::yggdrasil_valueArray_t<Numeric_t>;	//!< This vector contains the edge boundaries
	using IntervalBound_t 	= ::yggdrasil::yggdrasil_intervalBound_t;	//!< This is the type that is used for the interval
	using const_iterator 	= ValueArray_t::const_iterator;	//!< Thsi si the iterator for the individual sizes

private:
	using iterator 		= ValueArray_t::iterator;	//!< This is the oiterator, but we allow only const.

	/*
	 * =================
	 * Constructors
	 *
	 * All constructers are defaulted.
	 */
public:
	/**
	 * \brief	This is the building constructor that
	 * 		 associates this directly with data.
	 *
	 * The user gives the interval that should be binned.
	 * And then the user must give the \e inner endpoints.
	 * This means if array innerEdges has n values,
	 * then *this will have n + 1 many bins.
	 *
	 * The array of the end points must be sorted in acending order.
	 *
	 * \param  fullInterval		This is the full interval.
	 * \param  innerEdges 		This are the inner edges, must be sorted.
	 *
	 * \throw	If this is allready associated or the data is not sorted.
	 * 		 Also if an zero sized bin bould be constructed.
	 */
	yggdrasil_genBinEdges_t(
		const IntervalBound_t& 		fullInterval,
		const ValueArray_t& 		innerEdges)
	 :
	  yggdrasil_genBinEdges_t()
	{
		//All the tests are done in teh underling function
		this->buildBins(fullInterval, innerEdges);
	}; //End: building with direct buinding


	/**
	 * \brief	Building constructor and default constuctor.
	 *
	 * In this class the building constructor is actually also
	 * the default consztructor.
	 * The reaosn is that as every statistical test this class
	 * has to support lazzy binding, my term.
	 * This means it must be created and then filled with data later.
	 *
	 * \throws 	If underling functions throws.
	 */
	yggdrasil_genBinEdges_t()
	 :
	  m_binEdges()
	{};


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_genBinEdges_t(
		const yggdrasil_genBinEdges_t&)
	 = default;

	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_genBinEdges_t(
		yggdrasil_genBinEdges_t&&)
	 = default;

	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_genBinEdges_t&
	operator= (
		const yggdrasil_genBinEdges_t&)
	 = default;

	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted
	 */
	yggdrasil_genBinEdges_t&
	operator= (
		yggdrasil_genBinEdges_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_genBinEdges_t()
	 = default;

	/*
	 * ================
	 * Utility Functions
	 */
public:
	/**
	 * \brief	Return the number of bins \c *this has.
	 *
	 * \throw 	if \c *this is not associated to any data.
	 */
	Size_t
	nBins()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data has an undefined numbers of bins.");
		};
		assert(m_binEdges.size() >= 2);

		return (m_binEdges.size() - 1);
	}; //End: nBins


	/**
	 * \brief	This function returns true if \c *this is associated to a domain.
	 *
	 * \c *thsi is accociated to a data if there are some bin edges.
	 */
	bool
	isAssociated()
	 const
	 noexcept
	{
		return (m_binEdges.empty() == true) ? false : true;
	};


	/*
	 * ===================
	 * Accessing functions
	 */
public:
	/**
	 * \brief	Thid fucntion returns the spanned interval of *this.
	 *
	 * This is basically the interval that was passed to *this during its construction.
	 * It is the interval formed from the very first lower bound and the very last
	 * upper bound.
	 *
	 * \throw	If *this is not associated.
	 */
	IntervalBound_t
	getSpanningInterval()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("*this is not associated.");
		};
		const IntervalBound_t spanInter(m_binEdges.front(), m_binEdges.back());
		yggdrasil_assert(spanInter.isValid() == true);

		return spanInter;
	}; //End: getSpannedInterval


	/**
	 * \brief	This function returns the interval that is spanned by the \c i th bin.
	 *
	 * \param  i 	Which interval should be queried
	 *
	 * \throw	If \c *this is not associated with any data.
	 * 		 i is out of range or if the interval that is constructed is invalid.
	 */
	IntervalBound_t
	getBinInterval(
		const uInt_t 	i)
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data, does not have bins.");
		};
		if((m_binEdges.size() - 1) <= i)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The bin " + std::to_string(i) + " does not exists.");
		};

		//The constructor of the interval throws if it is not valid
		const IntervalBound_t binInterval(m_binEdges.at(i), m_binEdges.at(i + 1));
		yggdrasil_assert(binInterval.isValid() == true);

		return binInterval;
	}; //End: getBinInterval


	/**
	 * \brief	This function returns the lower bound of the ith bin.
	 *
	 * This function is more efficient than callint the above and
	 * then use the lowerbound on the returned.
	 *
	 * However no interval is constructed, so no safty can be guaranteed.
	 * However this is most likely accaptable.
	 */
	Numeric_t
	getLowerBound(
		const uInt_t 	i)
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data, does not have bins.");
		};
		if((m_binEdges.size() - 1) <= i)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The bin " + std::to_string(i) + " does not exists.");
		};

		return m_binEdges[i];
	}; //End: getLowerBound


	/**
	 * \brief	This function returns the upper bound of the ith bin.
	 *
	 * This function is more efficient than callint the above and
	 * then use the upperBound fucntion on the returned.
	 *
	 * However no interval is constructed, so no safty can be guaranteed.
	 * However this is most likely accaptable.
	 */
	Numeric_t
	getUpperBound(
		const uInt_t 	i)
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data, does not have bins.");
		};
		if((m_binEdges.size() - 1) <= i)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The bin " + std::to_string(i) + " does not exists.");
		};

		return m_binEdges.at(i + 1);
	}; //End: getUpperBound


	/**
	 * \brief	Returns the iterator to the beiging of the underling array.
	 *
	 * This is the lower bound of the first interval.
	 *
	 * One can see the iterators allow the accessing of all lower bounds.
	 * consecutively.
	 * This holds except for the last one (end() -1 ), which is the upper bound
	 * of the last bin.
	 */
	const_iterator
	begin()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data, does not have bins.");
		};

		return m_binEdges.cbegin();
	};


	/**
	 * \brief	Returns the past the end iterator of the underling array.
	 */
	const_iterator
	end()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("A table without associated data, does not have bins.");
		};

		return m_binEdges.cend();
	};



	/**
	 * \brief	This fucntion returns the beginning of the innerEdges range.
	 *
	 * The inner edges range the bin boundary that are in the inside of the
	 * spanned interval. It is bassically the same as the argument that was
	 * used to build the bins.
	 *
	 * This fuction is technically equivalent with begin() + 1.
	 *
	 * In the case of just one bin, the end iiterator, which is this->end()
	 * is returned.
	 */
	const_iterator
	innerEdgesBegin()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("This is not associated.");
		};

		//Test if we have only one bin
		if(m_binEdges.size() == 2)
		{
			yggdrasil_assert(this->nBins() == 1);

			//In the case of jsut one bin, we have the empty range
			return m_binEdges.cend();
		}; //End only one bin

		//THe normal case
		const const_iterator beginInner = m_binEdges.cbegin() + 1;
		return beginInner;
	}; //End: innerEdgesBegin


	/**
	 * \brief	This function returns the past the end iterator of the inner range.
	 *
	 * This are all the internal bin boundaries that are not the boundaries given the the
	 * spanned interval.
	 *
	 * This is basically end() - 1, but with a correction for the case that only one bin
	 * is present. In that case this->end() is returned.
	 */
	const_iterator
	innerEdgesEnd()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("This is not associated.");
		};

		//Test if we have only one bin
		if(m_binEdges.size() == 2)
		{
			yggdrasil_assert(this->nBins() == 1);

			//In the case of jsut one bin, we have the empty range
			return m_binEdges.cend();
		}; //End only one bin

		//THe normal case
		const const_iterator endInner = m_binEdges.cend() - 1;
		return endInner;
	}; //End: innerEdgesBegin



	/**
	 * \brief	This fucntion returns a constant reference to the bin edges of \c *this.
	 *
	 * The format of the edges is a bit strange.
	 * First of all also this class adheres to the Yggdrasil definition, that an interval is
	 * a right open mathematical interval.
	 * This menans that the upper bound does not belong to the actual interval.
	 * Assume \c *this has n bins, then the returned array is of legth n + 1.
	 * The first n entried in the arrays are the lower bounds of the bins.
	 * To be precise the value that is stored at index i is the lower bound
	 * of the ith interval, the value stored at interval i + 1 is upper bound
	 * of the ith bin, and at the same time, the lower bound of the i + 1 bin.
	 * This means that last entry in the array is the upper bound of the last bin.
	 */
	const ValueArray_t&
	getUnderlyingArray()
	 const
	{
		if(this->isAssociated() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not acccess an table that is not associated.");
		};

		return m_binEdges;
	}; //End: getBinEdges



	/*
	 * ================
	 * Managing functions
	 *
	 * These functions allows to manage *this.
	 * Like adding samples or so.
	 */
public:

	/**
	 * \brief	This function associated \c *thsi with data.
	 *
	 * The user gives the interval that should be binned.
	 * And then the user must give the \e inner endpoints.
	 * This means if array innerEdges has n values,
	 * then *this will have n + 1 many bins.
	 *
	 * The array of the end points must be sorted in acending order.
	 *
	 * \param  fullInterval		This is the full interval.
	 * \param  innerEdges 		This are the inner edges, must be sorted.
	 *
	 * \throw	If this is allready associated or the data is not sorted.
	 * 		 Also if an zero sized bin bould be constructed.
	 */
	void
	buildBins(
		const IntervalBound_t& 		fullInterval,
		const ValueArray_t& 		innerEdges);


	/**
	 * \brief	This function returns a copy of \c *this, but not associated to any data.
	 *
	 * This function calls the building constructor.
	 * The parameters are the same as were used to construct \c *this.
	 *
	 * Thsi function works even if *this is not associated.
	 *
	 * This function is a bit old and should be removed.
	 * It is kept for compability.
	 */
	yggdrasil_genBinEdges_t
	getUnassociatedCopy()
	 const
	{
		return yggdrasil_genBinEdges_t();
	};


	/**
	 * \brief	This function undounds *this.
	 *
	 * It basically deletes the internal array.
	 * Notice that this fucntion is manyly for internal purpose.
	 *
	 * \throw	If *this is not associated.
	 */
	void
	unboundData();


	/*
	 * =================
	 * Friends
	 */
private:

	//The partitioned bin edges is a firend
	friend class yggdrasil_partionedBinEdges_t;


	/*
	 * =================
	 * Private Members
	 */
private:
	ValueArray_t 		m_binEdges;	//!< This vector stopres the bin edges, notice that there
						//!<  is one more edges than there are bins.
						//!<  Value i is the lower bound of the ith bin, hence i+1 is the upper bound of bin i.
}; //End class: yggdrasil_genBinEdges_t





YGGDRASIL_NS_END(yggdrasil)
