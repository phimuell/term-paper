/**
 * \brief	This class implements generall bins, that can be used on any interval.
 *
 * It is based on the one that was originally designed for the chi2 tests.
 * But since it was noticed that it has to be used elsewhere too, it was
 * moved.
 * It is basically an array that offers nice access to it.
 *
 * If it manages n bins, then the underling array has a length of n + 1.
 * The value that is stored in array posistion i, is the lower bound of
 * the ith bin, the value stored at location i + 1, is the upper bound
 * of the ith bin. At the same time it is also the lower bound of the
 * i+1 bin.
 * The last array position is only the upper bound of the last bin.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_generalBinEdges.hpp>


//Incluide std
#include <memory>
#include <vector>
#include <numeric>
#include <algorithm>

//Include boost


YGGDRASIL_NS_BEGIN(yggdrasil)


void
yggdrasil_genBinEdges_t::buildBins(
	const IntervalBound_t&	fullInterval,
	const ValueArray_t& 	innerEdges)
{
	//Test the argument and this
	if(this->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bin edges container is allready associated.");
	};
	if(fullInterval.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The upper interval is not valid.");
	};

	//There is the possibility that we
	//have zereo inner points, in that case it is trivially to build
	if(innerEdges.size() == 0)
	{
		//We have only one big bin
		m_binEdges.resize(2);

		//Set the two points
		m_binEdges[0] = fullInterval.getLowerBound();
		m_binEdges[1] = fullInterval.getUpperBound();

		//We are now finisthed
		return;
	}; //ENd if: no inner edges


	/*
	 * Test the input data.
	 *
	 * This has to be done in two steps.
	 * First we test if the innerEdges is sorted and distinct.
	 * This has only to be done if we have more than one inner
	 * edges.
	 * Second we have to test if the two end points are distinct
	 * from the fullInterval
	 */

	//Make the first test
	if(innerEdges.size() > 1)
	{

		for(
			ValueArray_t::const_iterator it = innerEdges.cbegin() + 1;
			it != innerEdges.cend();
			++it)
		{
			const Numeric_t low = *(it - 1);
			const Numeric_t up  = *(it    );

			//Test if they are distinct
			if(low == up)
			{
				throw YGGDRASIL_EXCEPT_InvArg("The array contained values that where the same.");
			}; //end: not distinct

			//Test if they are sorted
			//Must be strict
			if(!(low < up))
			{
				throw YGGDRASIL_EXCEPT_InvArg("The array of inner edges is not sorted.");
			}; //ENd
		}; //End for(it): Going throug in pairs
	}; //End iof: make inner test

	//Second test compare it with the the interval bounds
	if(!(fullInterval.getLowerBound() < innerEdges.front()))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The inner edges has compability issues with the lower bound of the full interval.");
	}; //End if: lower compability
	if(!(innerEdges.back() < fullInterval.getUpperBound()))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The inner edges has compability issues with the upper bound of the full interval.");
	};

	//Now we can set fill the result
	m_binEdges.reserve(innerEdges.size() + 2);


	//First add the lower bound of the full interval
	m_binEdges.push_back(fullInterval.getLowerBound());

	//Now add all the one in the inner edges array
	for(const Numeric_t x : innerEdges)
	{
		m_binEdges.push_back(x);
	}; //End for(x): adding at the end

	//Adding the last one
	m_binEdges.push_back(fullInterval.getUpperBound());

	// Now tests if all teh intervals where successfull
	yggdrasil_assert(this->nBins() == (innerEdges.size() + 1));

	const Size_t NBins = this->nBins();
	for(Size_t i = 0; i != NBins; ++i)
	{
		const IntervalBound_t tI = this->getBinInterval(i);

		//Now test
		if(tI.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("While testing the newly build interval " + std::to_string(i) + " found to be invalid.");
		};
	}; //End for(i)


	//We are now finished
	return;
}; //End bould interval


void
yggdrasil_genBinEdges_t::unboundData()
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can only unbind an associated bin edges.");
	};

	//Create a new and use the swap trick to
	//realy get rid of the memeory
	ValueArray_t e;
	e.swap(m_binEdges);

	yggdrasil_assert(m_binEdges.empty());
	yggdrasil_assert(this->isAssociated() == false);

	return;
}; //End: unbpound




YGGDRASIL_NS_END(yggdrasil)
