#pragma once
/**
 * \brief	This file implements a split spequence.
 *
 * This file defines a class that can be used to encode a series/sequence of splits.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

//INcluide std
#include <vector>
#include <algorithm>

YGGDRASIL_NS_BEGIN(yggdrasil)


/**
 * \brief	This is a complete split sequence
 * \class 	yggdrasil_splitSequence_t
 *
 * This class is basically a list of integers, that encodes
 * the dimensions which has to be splitted.
 * The location where this dimensions has to be splitted is
 * then deducted from the actual samples in the actuall split routine.
 *
 * This class is basically a syntacticly enhached vector.
 * It is also inmutable, this means a once created object can not be modified.
 * Instead a modification operations, which are supported, will first
 * copy *this then modify *this and return the mutated copy to the user.
 * Never the less the class supports assignemnt.
 *
 * The length of the sequence is Constants::MAXIMAL_SPLIT_SEQUENCE_LENGTH.
 * The splits are applied sequently.
 *
 * The intention of this class is that from a test result
 * an instance of yggdrasil_splitSequence_t will be created and then
 * the splits will be performed.
 *
 *
 * An important node is that the code relies on the fact that
 * in each split sequence a dimension is only contained once.
 * In theory I would say that the code can perfectly handle
 * cases without this assumption, but I have never tested it
 * and I would not sell my life for it to work.
 */
class yggdrasil_splitSequence_t
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using DimensionID_t 	= Int_t;				//!< This is the type that identify a dimension
	using SplitSequence_t 	= ::std::vector<DimensionID_t>;		//!< This is the interbal sequence of splits
	using iterator 		= SplitSequence_t::const_iterator;	//!< This is the iterator
	using const_iterator 	= SplitSequence_t::const_iterator;	//!< This is the const iterator

	/*
	 * =================
	 * Constructors
	 *
	 * All are defaulted
	 */
public:
	/**
	 * \brief	Default constructor.
	 *
	 * Creates an empty sequence.
	 */
	yggdrasil_splitSequence_t()
	 noexcept = default;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_splitSequence_t(
		const yggdrasil_splitSequence_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_splitSequence_t(
		yggdrasil_splitSequence_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_splitSequence_t&
	operator= (
		const yggdrasil_splitSequence_t&) = default;


	/**
	 * \brief 	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_splitSequence_t&
	operator= (
		yggdrasil_splitSequence_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_splitSequence_t() = default;


	/*
	 * ===================
	 * Manipulate functions
	 *
	 * NOTICE:	This function will not manipulate *this!
	 * 		 They will create a copy of *this manitulate
	 * 		 the copy and then return the copy.
	 */
public:
	/**
	 * \brief	This function makes a copy of \c *this adds \c sp to the copy and returns the copy.
	 *
	 * The new split is added at the end of \c *this.
	 * It is only possible to add a dimension once.
	 *
	 * \param  sp 	The new split
	 *
	 * \throw 	If \c sp is not valid and exception is throwed.
	 * 		 If the maximal number of splits is exceeded.
	 * 		 Further if \c sp was added before to \c *this.
	 *
	 */
	yggdrasil_splitSequence_t
	copyAndAddNewLevelOfSplit(
		const DimensionID_t&	sp)
	 const
	{
		if(sp == Constants::YG_INVALID_DIMENSION)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The split taht was tried to add to the sequence is invalid.");
		};

		if(sp >= Constants::YG_MAX_DIMENSIONS)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The dimension exceeds the maximal allowed dimension.");
		};

		//This is a programatic restriction
		if(this->size() >= Constants::MAXIMAL_SPLIT_SEQUENCE_LENGTH)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Exceeded the maximum number of splits.");
		};

		//This is an implementation restriction
		if(this->size() >= Constants::MAX_SPLIT_DEPTH)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Exceeded the maximqal supported splitting depth.");
		};

		/*
		 *
		 * Never remove this code. It is resposible for enforcing
		 * that each dimension is only splitted once in a given sequence.
		 *
		 * The code can probably handle thsi correectly but
		 * it is not tested, and you enter the dark side when doing.
		 */
		for(const auto& d : m_seq)
		{
			if(d == sp)
			{
				throw YGGDRASIL_EXCEPT_LOGIC("Tried to add dimension " + std::to_string(sp) + " twiche to the sequence.");
			};
		};

		//Make a copy of *this
		yggdrasil_splitSequence_t newSeq(*this);

		//Now add the split to the copied version; at the END
		newSeq.m_seq.push_back(sp);

		//Retrurns the copy
		return newSeq;
	}; //End: add a new split


	/**
	 *  \brief	Copy \c *this, remove the frst split of the copy and return the copy.
	 *
	 * It is important that the operations are performed on a copy.
	 * This simple removes the split that is currently the head of \c *this,
	 * accessable by yggdrasil_splitSequence_t::getSplit(0).
	 * The size of \c *this is reduced by one.
	 *
	 * \throw 	If *this is empty an exception is generated.
	 */
	yggdrasil_splitSequence_t
	copyAndRemoveHeadSplit()
	 const
	{
		if(this->isEmpty() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Tried to remove a split from an empty sequence.");
		};

#if 0
		/*
		 * This is the old and buggy version.
		 * It caused segementation faults and also
		 * smashed the stack and invalidated some
		 * other objects.
		 *
		 * I do not understand why this happens,
		 * but I have fixed it now
		 */
		//Make a copy of this
		yggdrasil_splitSequence_t newSeq(*this);

		//Erase the first on the copy
		newSeq.m_seq.erase(m_seq.begin());
#else
		//Make a copy of this
		//yggdrasil_splitSequence_t newSeq(*this);
		yggdrasil_splitSequence_t newSeq;

		//Erase the first on the copy
		//newSeq.m_seq.erase(m_seq.begin());
		newSeq.m_seq.assign(m_seq.begin() + 1, m_seq.end());
#endif

		//return the modified copy
		return newSeq;
	}; //End remobve first split



	/*
	 * =====================
	 * Query functions
	 */
public:
	/**
	 * \brief	Returns the numbers of splits, that are stored in \c *this.
	 *
	 * This returns a signed integer.
	 */
	Int_t
	nSplits()
	 const
	 noexcept
	{
		return (Int_t)m_seq.size();
	};


	/**
	 * \brief	Returns the size of \c *this.
	 *
	 * This function returns a std::size_t.
	 */
	Size_t
	size()
	 const
	 noexcept
	{
		return m_seq.size();
	};


	/**
	 * \brief	Returns true if \c *this has more than zero splits.
	 */
	bool
	isEmpty()
	 const
	 noexcept
	{
		return m_seq.empty();
	};


	/**
	 * \brief	Returns true if \c *this is valid.
	 */
	bool
	isValid()
 	 const
 	 noexcept
 	{
 		for(const auto& i : m_seq)
		{
			if(i == Constants::YG_INVALID_DIMENSION)
			{
				return false;
			};

			if(i >= Constants::YG_MAX_DIMENSIONS)
			{
				return false;
			};
		}; //End for(i):

		//Test if only one split is there
		if(m_seq.size() > 1)
		{
			for(const auto& i : m_seq)
			{
				const auto nOccurence = ::std::count(
						m_seq.cbegin(),
						m_seq.cend(),
						i);

				if(nOccurence != 1)
				{
					return false;
				}; //End if:

			}; //End for(i): itertae through all the values
		}; //End if: there is more than one split


		return true;
	};



	/*
	 * ======================
	 * Accesser function
	 */
public:
	/**
	 * \brief	Return the ith split.
	 *
	 * This function allows an access to a arbitrary split of \c *this.
	 * This function is only provided for completness.
	 * Use the getHeadSplit() fucntion to optain the current
	 * head dimension to split.
	 *
	 * \param  i 	Which split sould be returned.
	 *
	 * \throw 	If the split does not exists
	 */
	DimensionID_t
	getSplit(
		const Size_t 	i)
	 const
	{
		return m_seq.at(i);
	};


	/**
	 * \brief	This function returns the current head dimnension.
	 *
	 * This is the the dimension that should be splitted next.
	 * This function is equivalent to calling getSplit(0).
	 *
	 * \throw 	If \c *this is empty.
	 */
	DimensionID_t
	getHeadSplit()
	 const
	{
		if(m_seq.empty() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("An empty split sequence does not have a head dimension.");
		};

		return m_seq[0];
	};


	/**
	 * \brief	Returns an iterator to the first split.
	 *
	 * The first split is the current head split.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		return m_seq.cbegin();
	};


	/**
	 * \brief	Returns an iterator after the last split.
	 *
	 * The last split is the dimension that is split at the very last step.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		return m_seq.cend();
	};


	/*
	 * =======================
	 * Private Memebrs
	 */
private:
	SplitSequence_t 	m_seq;	//!< This is the list of all splits
}; //End class: yggdrasil_splitSequence_t



YGGDRASIL_NS_END(yggdrasil)



