/**
 * \brief	This file implement the functionality offered by the chi2testtable function of the r code.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_util.hpp>
#include <util/yggdrasil_chi2_frequencyTable.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sample.hpp>




//Incluide std
#include <memory>
#include <vector>
#include <numeric>
#include <algorithm>

//Include boost
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/normal.hpp>


YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_chi2FreqTable_t::MarginalTable_t
yggdrasil_chi2FreqTable_t::getRowMarginals()
 const
{
	//Test if associate
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The table you tryed to access is not associated to any data.");
	};

	MarginalTable_t rowMarginals(m_nRowsFirst, 0);
	for(Size_t r = 0; r != m_nRowsFirst; ++r)
	{
		//This is the index where the current row starts and end
		const ::yggdrasil::Size_t rowStart = m_nColsSecond *  r     ;
		const ::yggdrasil::Size_t rowEnd   = m_nColsSecond * (r + 1);

		//This is not so efficient
		rowMarginals.at(r) = ::std::accumulate(
			m_freqTable.cbegin() + rowStart,	//Where the row starts
			m_freqTable.cbegin() + rowEnd  ,	//Where the row end
			Size_t(0)				//Init, we start at zero
			);
	}; //End for(r): Iterating over the rows

	return rowMarginals;
}; //End: RowMarginals


yggdrasil_chi2FreqTable_t::MarginalTable_t
yggdrasil_chi2FreqTable_t::getColMarginals()
 const
{
	//Test if associate
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The table you tryed to access is not associated to any data.");
	};

	//This is the marginals
	MarginalTable_t colMarginals(m_nColsSecond, 0);	//Important to initalize it with zero

	//Starange we will iterate over the row in the outer loop
	for(Size_t r = 0; r != m_nRowsFirst; ++r)
	{
		//This is the start index of the current row
		const ::yggdrasil::Size_t rowStart = m_nColsSecond *  r     ;

		//This is the iterator that opints to the beginning
		auto rowIterator = m_freqTable.cbegin() + rowStart;

		//Now we will iterate over the colums and sum up
		for(Size_t c = 0; c != m_nColsSecond; ++c, ++rowIterator)
		{
			colMarginals.at(c) += *rowIterator;
		}; //End for(c):
	}; //End for(r): Iterating over the rows

	return colMarginals;
}; //End: col Marginals


yggdrasil_chi2FreqTable_t::Size_t
yggdrasil_chi2FreqTable_t::getTotalObs()
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to access an unassociated frequency table.");
	};


	return m_grandTotal;
}; //End: get Total Ops


yggdrasil_chi2FreqTable_t::Size_t
yggdrasil_chi2FreqTable_t::getFrequency(
	const Size_t 		i,
	const Size_t 		j)
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to access an unassociated frequency table.");
	};

	if(!(i < m_nRowsFirst))
	{
		throw YGGDRASIL_EXCEPT_OutOfBound("The requested row " + std::to_string(i) + " exceeds the number of valid dimension " + std::to_string(m_nRowsFirst));
	};
	if(!(j < m_nColsSecond))
	{
		throw YGGDRASIL_EXCEPT_OutOfBound("The requested column " + std::to_string(j) + " exceeds the number of valid dimensions of " + std::to_string(m_nColsSecond));
	};

	//Now access
	const ::yggdrasil::Size_t indexToAccess = i * m_nColsSecond + j;
	yggdrasil_assert(indexToAccess < m_freqTable.size());

	const Numeric_t obsFrequency = Numeric_t(m_freqTable[indexToAccess]);

	return obsFrequency;
}; //End: get frequency




void
yggdrasil_chi2FreqTable_t::buildInitialFreqTable(
	const DimArray_t& 		dimArray1,
	const BinEdgesArray_t& 		binEdges1,
	const DimArray_t& 		dimArray2,
	const BinEdgesArray_t& 		binEdges2,
	const bool 			useSorting)
{
	//This must not be associated
	if(this->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not bound data to an allready associated table.");
	};

	//The two diemsnional must have valid dimensions
	if(dimArray1.isDimensionIllegal() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("dimArray1 has an invalid dimension.");
	};
	if(dimArray2.isDimensionIllegal() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("dimArray2 has an invalid dimension.");
	};
	if(dimArray1.nSamples() != dimArray2.nSamples())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The two dimensonal arrays have different size. dimArray1 hat " + std::to_string(dimArray1.nSamples()) + " many samples. dimArray2 has " + std::to_string(dimArray2.nSamples()) + " samples.");
	};
	if(dimArray1.nSamples() == 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("There are no sample here.");
	};
	if(dimArray1.getAssociatedDim() == dimArray2.getAssociatedDim())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The two dimensional array have the same associated dimension.");
	};

	//The two edges must be associated
	if(binEdges1.isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("binEdges1 is not associated.");
	};
	if(binEdges2.isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("binEdges2 is not associated.");
	};

	//Test if all is unbound state
	if(m_freqTable.size() != 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The frequancy table is allocated, but it should not.");
	};
	if(m_grandTotal != Size_t(-1))
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The grand total has a value that is not the null value.");
	};
	if(m_nRowsFirst != Size_t(-1))
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The row parameter has a value that is not the null value.");
	};
	if(m_nColsSecond != Size_t(-1))
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The col parameter has a value that is not the null value.");
	};

	//This is teh number of samples
	const Size_t nSamples = dimArray1.nSamples();

	//The grand total is equal the numbers of samples
	m_grandTotal = nSamples;

	//Now write the dimension
	m_nRowsFirst  = binEdges1.nBins();
	m_nColsSecond = binEdges2.nBins();

	yggdrasil_assert(m_freqTable.empty() == true);

	/*
	 * Handle the spcial case
	 */
	const int specialCaseVal = this->private_buildFreqTable_specialCase(dimArray1, binEdges1, dimArray2, binEdges2);

	if(specialCaseVal == 0)
	{
		//There was no problem in handling it
		return;
	}
	if(specialCaseVal == -1)
	{
		//There was an error and the porocess failed
		throw YGGDRASIL_EXCEPT_RUNTIME("The special hase handling failed.");
	};

	//Nothing happens use the other code
	yggdrasil_assert(specialCaseVal == 1);
	yggdrasil_assert(m_freqTable.empty() == true);

	//This is the return value of the creaion
	bool freqTableBuildOK;

	//Test if the two bins have a partitioning
	if(binEdges1.hasPartition() && binEdges2.hasPartition())
	{
		//THey have a partitioning, so call the version
		freqTableBuildOK = this->private_buildFreqTable_partion(
				dimArray1, binEdges1,
				dimArray2, binEdges2);
	}
	else
	{
		/*
		 * The conventional support
		 */
		if(useSorting == false || Constants::TEMP_ARRAY_FT_KB == 0)
		{
			freqTableBuildOK = this->private_buildFreqTable_std(
					dimArray1, binEdges1,
					dimArray2, binEdges2);
		}
		else
		{
			freqTableBuildOK = this->private_buildFreqTable_sort(
					dimArray1, binEdges1,
					dimArray2, binEdges2);
		};
	}; //End else: conventional creation


	if(freqTableBuildOK == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The building of the freq table faile.");
	};

	/*
	 * Now we test if all was placed correct.
	 * This is in a sense an ovbershoot
	 */
	yggdrasil_assert(::std::accumulate(m_freqTable.cbegin(), m_freqTable.cend(), Size_t(0)) == m_grandTotal);


	return;
}; //End: buildInitialFrequancy table




int
yggdrasil_chi2FreqTable_t::private_buildFreqTable_specialCase(
	const DimArray_t& 		dimArray1,
	const BinEdgesArray_t& 		binEdges1,
	const DimArray_t& 		dimArray2,
	const BinEdgesArray_t& 		binEdges2)
{
	if(m_freqTable.empty() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The frequency table is allocated, but it should not.");
	};

	//this is the sizes of both
	const Size_t nBin1 = binEdges1.nBins();
	const Size_t nBin2 = binEdges2.nBins();

	if(nBin1 == 0 || nBin2 == 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("There is a zero sized bin");
	};

	//The first case there is just one bin
	if(nBin1 == 1 && nBin2 == 1)
	{
		//There is only one bin
		m_freqTable.resize(1);

		//all sample belongs to this case
		m_freqTable.at(0) = dimArray1.nSamples();

		//We return zero to indicate all went well
		return 0;
	}; //End just one bin


	/*
	 * We could handle more special cases
	 * But curtrently we do not implement them
	 *
	 * We return 1 to indicate this
	 */
	return 1;
}; //End handling special cases




Real_t
yggdrasil_chi2FreqTable_t::computeChi2Value()
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to compute the test statistic on a not associated table.");
	};

	//We first compute the the marginals
	const MarginalTable_t rowMarginals = this->getRowMarginals();
	const MarginalTable_t colMarginals = this->getColMarginals();

	//Const this is the grand total as a Numeric type
	const ::yggdrasil::Real_t grandTotalREAL = ::yggdrasil::Real_t(m_grandTotal);
	yggdrasil_assert(m_grandTotal != 0);

	//This Numeric vector is used to increase stability
	const Size_t nChi2 = std::max<Size_t>(4, m_nRowsFirst / 3);
	yggdrasil_assert(nChi2 > 0);

	::yggdrasil::yggdrasil_valueArray_t<::yggdrasil::Real_t> Chi2(nChi2, 0.0);

	//This is a counter that is used for indexing
	Size_t iChi2 = 0;

	//We handle rows after rows
	for(Size_t r = 0; r != m_nRowsFirst; ++r)
	{
		//This is the current value of the row marginal
		const Size_t rMarginal 	   = rowMarginals.at(r);
		const Real_t rMarginalREAL = Real_t(rMarginal);  //Casted to a real

		//this is the start/end of the current row
		const Size_t startCurrentRow = m_nColsSecond *  r;

		//Now we iterate over the colum
		for(Size_t c = 0; c != m_nColsSecond; ++c)
		{
			//This is for handling the index array
			if(iChi2 == nChi2)
			{
				iChi2 = 0;
			}; //End reset index

			//Get the current marginal of the column
			yggdrasil_assert(c < colMarginals.size());
			const Size_t cMarginal     = colMarginals[c];
			const Real_t cMarginalREAL = Real_t(cMarginal);	//Casted to a real

			//Now we compute the expected value under the null hypotheis
			//Since all are reals the result will also be a real
			const Real_t expectedCount = (rMarginalREAL * cMarginalREAL) / grandTotalREAL;
			yggdrasil_assert(expectedCount >= Real_t(0.0));

			//Now we load the observed frequency
			const Size_t observedCountSIZE = m_freqTable[startCurrentRow + c];
			const Real_t observedCount = Real_t(observedCountSIZE);	//As real


			/*
			 * Several things can happens.
			 *
			 * First both observed and expected counts are non zero.
			 * In that case we have no problems.
			 *
			 * In the case that observed count is zero and
			 * expected is non zero we have again no problems.
			 *
			 * In the case that the expected count is zero
			 * and the observed is greater than zero, we habe a
			 * contribution of infinity.
			 * This is handled correctly by the IEEE 754 standard.
			 *
			 * In the case that both expected and observed counts
			 * are zero, there is no problems form a mathematical
			 * point, since we requiere it, so the contribution should be zero
			 * a perfect match.
			 * However IEEE 754 defines this as a NAN, so we must handle it explicitly
			 */

			if((observedCountSIZE == 0) && (expectedCount < 100 * Constants::EPSILON))
			{
				//We have the 0 / 0 case, which is defined to have zero contriibution
				//we do this by just incrementing
				continue;
			};


			/*
			 * Now we compute the contribution
			 */
			const Real_t countDiff  = observedCount - expectedCount;
			const Real_t countDiff2 = countDiff * countDiff;
			const Real_t thisContribution = countDiff2 / expectedCount;

			yggdrasil_assert(::std::isnan(thisContribution) == false);

			//Now add the contribution
			Chi2.at(iChi2) += thisContribution;
		}; //End for(c) consume a full colum
	}; //End for(r): looping over the row

	/*
	 * Now make the final reducing
	 */
	Real_t accum1 = 0.0,
	       accum2 = 0.0,
	       accum3 = 0.0,
	       accum4 = 0.0;

	const Size_t nChi2Mod4 = nChi2 % 4;  //This is the left over
	const Size_t nChi2End  = nChi2 - nChi2Mod4; //Until this we have to iterate
	for(Size_t i = 0; i != nChi2End; i += 4)
	{
		accum1 += Chi2.at(i + 0);
		yggdrasil_assert(::std::isnan(accum1) == false);

		accum2 += Chi2.at(i + 1);
		yggdrasil_assert(::std::isnan(accum2) == false);

		accum3 += Chi2.at(i + 2);
		yggdrasil_assert(::std::isnan(accum3) == false);

		accum4 += Chi2.at(i + 3);
		yggdrasil_assert(::std::isnan(accum4) == false);
	}; //End for(i): Make the reduction

	//Handling the left overs
	for(Size_t i = 0; i != nChi2Mod4; ++i)
	{
		accum1 += Chi2.at(nChi2End + i);
		yggdrasil_assert(::std::isnan(accum1) == false);
	}; //End for(i): handling left overs

	//Final sum
	const Real_t fullChi2 = (accum1 + accum2) + (accum3 + accum4);

	//This is a last test, that is not an assert
	if(::std::isnan(fullChi2) == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The computation of the chi2 value of the frequency table, resulted in a NaN value.");
	};


	//Return the final sum
	return fullChi2;
}; //End: calculate test statistic


Real_t
yggdrasil_chi2FreqTable_t::getDOF()
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("A table without any associated data, does not have a DOF.");
	};

	//See: http://www.ltcconline.net/greenl/courses/201/regression/chi2.htm
	const Real_t DOF = (m_nRowsFirst - 1) * (m_nColsSecond - 1);

	yggdrasil_assert(DOF >= 0.0);

	return DOF;
}; //End: getDOF


void
yggdrasil_chi2FreqTable_t::unboundData()
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Can not unbound from an empyt table.");
	};

	//We have to create a new empty object
	IntFreqTable_t emptyObject;

	//Perform a swap
	m_freqTable.swap(emptyObject);

	//Set all the other parmeter to undbound
	m_grandTotal  = Size_t(-1);
	m_nRowsFirst  = Size_t(-1);
	m_nColsSecond = Size_t(-1);

	return;
}; //End: unboundData





YGGDRASIL_NS_END(yggdrasil)



