/**
 * \brief	This file implement the functionality offered by the chi2testtable function of the r code.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_util.hpp>
#include <util/yggdrasil_chi2_frequencyTable.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sample.hpp>




//Incluide std
#include <memory>
#include <vector>
#include <numeric>
#include <algorithm>

//Include boost
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/normal.hpp>


YGGDRASIL_NS_BEGIN(yggdrasil)


bool
yggdrasil_chi2FreqTable_t::private_buildFreqTable_sort(
	const DimArray_t& 		dimArray1,
	const BinEdgesArray_t& 		binEdges1,
	const DimArray_t& 		dimArray2,
	const BinEdgesArray_t& 		binEdges2)
{
	yggdrasil_assert(dimArray1.getAssociatedDim() != dimArray2.getAssociatedDim());
	if(m_freqTable.empty() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The frequency table is allocated, but it should not.");
	};

	//This is teh number of samples
	const Size_t nSamples = dimArray1.nSamples();
	yggdrasil_assert(nSamples == dimArray2.nSamples());

	const ::yggdrasil::Size_t maxPossibleIndexable = ::std::numeric_limits<SortIndex_t>::max();
	if(maxPossibleIndexable <= nSamples)
	{
		throw YGGDRASIL_EXCEPT_OutOfBound("Two many samples, you have to extend the SortIndex_t type.");
	};


	//Allocateing the table
	m_freqTable.assign(m_nRowsFirst * m_nColsSecond, 0);
	if(m_freqTable.size() == 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The size of the frequewnzy table is zero.");
	};

	//Set the ganrnad total to zero
	m_grandTotal = 0;

	//this is for the end of the iteration
	const Size_t lastBinFirst  = m_nRowsFirst  - 1;
	const Size_t lastBinSecond = m_nColsSecond - 1;
	yggdrasil_assert(m_nRowsFirst == binEdges1.nBins());
	yggdrasil_assert(m_nColsSecond == binEdges2.nBins());

	//Get the general bin edges
	const yggdrasil_genBinEdges_t binEdgesGen1 = binEdges1.getGeneralEdges();
	const yggdrasil_genBinEdges_t binEdgesGen2 = binEdges2.getGeneralEdges();
	yggdrasil_assert(binEdgesGen1.nBins() == binEdges1.nBins());
	yggdrasil_assert(binEdgesGen2.nBins() == binEdges2.nBins());

	yggdrasil_assert(dimArray1.getAssociatedDim() != dimArray2.getAssociatedDim());


	if(m_nRowsFirst == 1 && m_nColsSecond == 1)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The case with one bin can not be handled in that function. This sould be handed by the special case.");
	};

	//Calculate the numbers of samples that we are allowed to handle in one go.
	//This is due to the limited memory. This is the smae value as is used in the
	//test table, but only half, the reason is that, we operate with twioce as many data.
	const Size_t nSamplesPerIteration = Constants::TEMP_ARRAY_FT_KB * 1024 / sizeof(SortIndex_t) / 2;
	yggdrasil_assert(nSamplesPerIteration > 0);

	//Calculate the numbers of times we have to apply the recursive binning algotithm
	//SInce the number of samples is the same, the index calculations agre the same for all.
	const Size_t nBinningSteps_full = (nSamples / nSamplesPerIteration);	//This is the number of fulol steps
	const Size_t nBinningSteps  = nBinningSteps_full + ((nSamples % nSamplesPerIteration) == 0 ? 0 : 1);	//This is for handling the left overes


	/*
	 * Now we iterate through the samples and call the binning
	 * fucntion, we only get the indices of the index array,
	 * but from that we can calculate the size
	 */

	//This variable stores the index that are needed to supply
	yggdrasil_valueArray_t<SortIndex_t> indexArray2(0);
	yggdrasil_assert(indexArray2.empty() == true);
	yggdrasil_valueArray_t<SortIndex_t> indexArray1(0);
	yggdrasil_assert(indexArray2.empty() == true);
	yggdrasil_assert(indexArray1.empty() == true);

	//Going through the loop
	for(Size_t binStep = 0; binStep != nBinningSteps; ++binStep)
	{
		//Caculating the sizes we now need
		const Size_t thisStepStart  = binStep * nSamplesPerIteration;
		const Size_t thisStepStop   = ::std::min<Size_t>((binStep + 1) * nSamplesPerIteration, nSamples);
		yggdrasil_assert(thisStepStart < thisStepStop);
		yggdrasil_assert(thisStepStart >= 0);

		//This is the size of the current step
		const Size_t sizeCurrentSpet = thisStepStop - thisStepStart;
		yggdrasil_assert(sizeCurrentSpet > 0);

		//Now generate the index array
		//It is important that we fill it with the correct values
		indexArray1.resize(sizeCurrentSpet);
		indexArray2.resize(sizeCurrentSpet);
		{
			//This is the counter for the array
			Size_t k = 0;

			//The loop variable is for the index array, not for indexing the index array
			for(Size_t mIdx = thisStepStart; mIdx != thisStepStop; ++mIdx)
			{
				yggdrasil_assert(k < indexArray1.size());
				yggdrasil_assert(k < indexArray2.size());
				yggdrasil_assert(mIdx < dimArray1.size());
				yggdrasil_assert(mIdx < dimArray2.size());
				indexArray1[k] = mIdx;
				indexArray2[k] = mIdx;

				//Increment k Manualy
				k += 1;
			}; //ENd for(mIdx):
			yggdrasil_assert(k == indexArray1.size());
			yggdrasil_assert(k == indexArray2.size());
		}; //End scope: filling the index array

		/*
		 * Perform the partitioning on a part of the data
		 */

		//First dimension
		const yggdrasil_valueArray_t<Size_t> binEdgesIdx1 =
			yggdrasil_recArgBinning(dimArray1, binEdgesGen1, &indexArray1, true); //We need sorting
		yggdrasil_assert(binEdgesIdx1.size() == (m_nRowsFirst + 1));


		//Second dimension
		const yggdrasil_valueArray_t<Size_t> binEdgesIdx2 =
			yggdrasil_recArgBinning(dimArray2, binEdgesGen2, &indexArray2, true); //We need sorting
		yggdrasil_assert(binEdgesIdx2.size() == (m_nColsSecond + 1));


		/*
		 * Now update Frequency table
		 */
		for(Size_t i = 0; i != m_nRowsFirst; ++i)
		{
			//Get the range of the row
			const IdxItRange_t rowRange = ::std::make_pair(
				indexArray1.cbegin() + binEdgesIdx1.at(i    ),
				indexArray1.cbegin() + binEdgesIdx1.at(i + 1)
				);


			//Test if the range is empty
			if(rowRange.first != rowRange.second)
			{
				//THey are not empty
				yggdrasil_assert(rowRange.first < rowRange.second);

				for(Size_t j = 0; j != m_nColsSecond; ++j)
				{
					//Get the colum row range
					const IdxItRange_t colRange = ::std::make_pair(
						indexArray2.cbegin() + binEdgesIdx2.at(j    ),
						indexArray2.cbegin() + binEdgesIdx2.at(j + 1)
						);


					//Find the number of equal elements
					const Size_t samplesInThatPart = ygInternal_countSameElements(rowRange, colRange);

					//Add to grand total
					m_grandTotal += samplesInThatPart;

					//Write it into the table
					const ::yggdrasil::Size_t indexToAccess = i * m_nColsSecond + j;
					yggdrasil_assert(indexToAccess < m_freqTable.size());

#ifndef NDEBUG
					const Size_t oldCount = this->getFrequency(i, j);
#endif
					m_freqTable[indexToAccess] += samplesInThatPart;
					yggdrasil_assert(this->getFrequency(i, j) == (samplesInThatPart + oldCount));
				}; //End for(j): second index
			}; //ENd if: row range is empty
		}; //ENd for(i): going through the first index

	}; //End for(i): binning steps

	//Return true for ther success
	return true;
}; //End sorting



YGGDRASIL_NS_END(yggdrasil)



