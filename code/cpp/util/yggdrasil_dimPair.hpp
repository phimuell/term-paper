#pragma once
/**
 * \brief	This file implements a dimensiona pair.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

//INcluide std
#include <climits>
#include <limits>
#include <functional>
#include <string>

YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \class 	yggdrasil_dimPair_t
 * \brief	This class models a dimension pair.
 *
 * This class is basically an internal class.
 * It is primary used for the independence test.
 *
 * The user can think of it as a nice wrapper arround a pair.
 *
 */
class yggdrasil_dimPair_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t		= ::yggdrasil::uInt32_t;	//!< This is the type that is used to store both
	using SingleDimID_t 	= ::yggdrasil::uInt16_t;	//!< This is the type that is used as a single dimension


	/*
	 * ============
	 * Internal Masks
	 */
public:
	static const Base_t MASK_UPPER = 0x0000FFFF;	//!< This is for masking out the upper half of the int
	static const Base_t SHIFT_DOWN = ::std::numeric_limits<SingleDimID_t>::digits; //Thi is the amount of shifting down

	//Make the tests
	static_assert(2 * sizeof(SingleDimID_t) == sizeof(Base_t), "The base is not large enought.");
	static_assert(Constants::YG_MAX_DIMENSIONS <= ::std::numeric_limits<SingleDimID_t>::max(), "Single dimension can not hold the maximum value.");


	/*
	 * ===================
	 * Constructors
	 */
public:
	/**
	 * \brief	Default constructor.
	 *
	 * Is deleted.
	 */
	yggdrasil_dimPair_t()
	 noexcept
	 = delete;


	/**
	 * \brief	This si the buiding constructor
	 *
	 * The building constructor creats a valid instance of this.
	 * We requirere that the fist dimension is smaller than the second one.
	 *
	 * \param  iDim		The first dimension.
	 * \param  jDim 	This is the second dimension.
	 */
	yggdrasil_dimPair_t(
		const SingleDimID_t 	iDim,
		const SingleDimID_t 	jDim)
	 :
	  m_dimCode(((Base_t(iDim) << SHIFT_DOWN) | (Base_t(jDim))))
	{
		if(iDim < 0 || iDim == ::yggdrasil::Constants::YG_INVALID_DIMENSION)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The first dimension is ilegal.");
		};
		if(!(iDim < Constants::YG_MAX_DIMENSIONS))
		{
			throw YGGDRASIL_EXCEPT_InvArg("The first dimension exceeds the maximal allowed dimension. The dimension was " + std::to_string(iDim));
		};
		if(jDim < 0 || jDim == ::yggdrasil::Constants::YG_INVALID_DIMENSION)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The first dimension is ilegal.");
		};
		if(!(jDim < Constants::YG_MAX_DIMENSIONS))
		{
			throw YGGDRASIL_EXCEPT_InvArg("The first dimension exceeds the maximal allowed dimension. The dimension was " + std::to_string(jDim));
		};

		if(!(iDim < jDim))
		{
			throw YGGDRASIL_EXCEPT_InvArg("The ordering of the dimension was not correct. The first dimension was " + std::to_string(iDim) + " and the second dimension was " + std::to_string(jDim));
		};

		yggdrasil_assert(this->getFirstDim()  == iDim);
		yggdrasil_assert(this->getSecondDim() == jDim);
	}; //End: building constructor


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_dimPair_t(
		const yggdrasil_dimPair_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_dimPair_t(
		yggdrasil_dimPair_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_dimPair_t&
	operator= (
		const yggdrasil_dimPair_t&) = default;


	/**
	 * \brief 	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_dimPair_t&
	operator= (
		yggdrasil_dimPair_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_dimPair_t() = default;


	/*
	 * =================
	 * Funcitons
	 */
public:

	/**
	 * \brief	Returns the first dimension.
	 */
	SingleDimID_t
	getFirstDim()
	 const
	 noexcept
	{
		return SingleDimID_t((m_dimCode >> SHIFT_DOWN) & MASK_UPPER);
	};


	/**
	 * \brief	Returns the second dimension.
	 */
	SingleDimID_t
	getSecondDim()
	 const
	 noexcept
	{
		return SingleDimID_t(m_dimCode & MASK_UPPER);
	};


	/**
	 * \brief	Thsi function returns the compressed data
	 */
	Base_t
	getBase()
	 const
	 noexcept
	{
		return m_dimCode;
	};


	/**
	 * \brief	This is a less than comparisson.
	 */
	bool
	operator< (
		const yggdrasil_dimPair_t& rhs)
	 const
	 noexcept
	{
		return (m_dimCode < rhs.m_dimCode);
	};

	/**
	 * \brief	This is an equal tester
	 */
	bool
	operator== (
		const yggdrasil_dimPair_t& rhs)
	 const
	 noexcept
	{
		return (m_dimCode == rhs.m_dimCode);
	};


	/**
	 * \brief	Thif cuntion outputs *this as a string
	 */
	std::string
	print()
	 const
	{
		return std::string("(" + std::to_string(this->getFirstDim()) + ", " + std::to_string(this->getSecondDim()) + ")");
	}; //End print


	/*
	 * ==============
	 * Private Memmbers
	 */
private:
	Base_t 		m_dimCode;	//!< This is the encoded dimension
					//!< The encoding is such that the first dimension occupies the upper part.
}; //End class: yggdrasil_dimPair_t

YGGDRASIL_NS_END(yggdrasil)



/*
 * Overload for the hash map
 */
YGGDRASIL_NS_BEGIN(std)

/**
 * \brief	This is a specialization for the hash function
 */
template<>
struct hash<::yggdrasil::yggdrasil_dimPair_t>
{
	size_t
	operator() (
		const ::yggdrasil::yggdrasil_dimPair_t& d)
	 const
	{
		return m_hash(d.getBase());
	};

private:
	std::hash<::yggdrasil::yggdrasil_dimPair_t::Base_t>	m_hash;
}; //End struct: hash

YGGDRASIL_NS_END(std)





