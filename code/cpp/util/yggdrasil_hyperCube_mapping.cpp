/**
 * \brief	This file implements some function of the hypercube, that deals with the mapping of samples.
 *
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_splitCommand.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <tree_geni/yggdrasil_multiDimCondition.hpp>


//INcluide std
#include <vector>
#include <utility>
#include <tuple>

YGGDRASIL_NS_BEGIN(yggdrasil)

yggdrasil_hyperCube_t::ValueArray_t
yggdrasil_hyperCube_t::mapFromUnit(
	const Int_t 		d,
	const DimensionArray_t&	sa)
 const
{
	//We requiere that the full cube is valid, not just the dimension we need
	if(this->isValid())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to work with an invalid cube");
	};

	if(sa.isDimensionIllegal() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension array has an illegal dimension.");
	};

	if(d == Constants::YG_INVALID_DIMENSION)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension to act on is invalid.");
	};

	//Test if the dimenssions agree
	if(d != sa.getAssociatedDim())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The specified dimension, " + std::to_string(d) + ", and the associated dimension " + std::to_string(sa.getAssociatedDim()) + " does not agree.");
	};

	if(uInt_t(d) >= Constants::YG_MAX_DIMENSIONS)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimeion, " + std::to_string(d) + ", eceeds the maximum dimension of " + std::to_string(Constants::YG_MAX_DIMENSIONS));
	};


	/*
		* Perform the mapping by just calling the underling function
		*/
	return m_bounds.at(d).mapFromUnit(sa);
}; //End maps from


yggdrasil_hyperCube_t::ValueArray_t
yggdrasil_hyperCube_t::contractToUnit(
	const Int_t 		d,
	const DimensionArray_t&	sa)
 const
{
	//We requiere that the full cube is valid, not just the dimension we need
	if(this->isValid())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to work with an invalid cube");
	};

	if(sa.isDimensionIllegal() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The dimension array has an illegal dimension.");
	};

	if(d == Constants::YG_INVALID_DIMENSION)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension to act on is invalid.");
	};

	//Test if the dimenssions agree
	if(d != sa.getAssociatedDim())
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The specified dimension, " + std::to_string(d) + ", and the associated dimension " + std::to_string(sa.getAssociatedDim()) + " does not agree.");
	};

	if(uInt_t(d) >= Constants::YG_MAX_DIMENSIONS)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimeion, " + std::to_string(d) + ", eceeds the maximum dimension of " + std::to_string(Constants::YG_MAX_DIMENSIONS));
	};


	/*
		* Perform the mapping by just calling the underling function
		*/
	return m_bounds.at(d).contractToUnit(sa);
}; //End maps from

yggdrasil_hyperCube_t::MultiCondition_t
yggdrasil_hyperCube_t::contractToUnit(
	const MultiCondition_t& 	condi)
 const
{
	//Make some tests, only for the debuig mode
	yggdrasil_assert(this->isValid());
	yggdrasil_assert(condi.isValid());

	//test the dimensionality
	if(this->m_bounds.size() != condi.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the hyper cube, " + std::to_string(m_bounds.size()) + ", does not match with the one of the condition " + std::to_string(condi.nDims()));
	};

	//This is the vector of conditions that
	//will be used fro the constructing
	MultiCondition_t::ConditionVector_t transCondiVec;

	//This is teh number of the conditions
	const Size_t nCondi = condi.nConditions();

	//Preallocate space
	transCondiVec.reserve(nCondi);

	//Going through the dimension and apply the transformation to the value
	for(Size_t i = 0; i != nCondi; ++i)
	{
		//Load the data
		const Size_t cDim = condi[i].getConditionDim();
		const auto   cVal = condi[i].getConditionValue();

		//Now transform the condition value
		const auto   tVal = m_bounds[cDim].contractToUnit(cVal);
		yggdrasil_assert(0.0 <= tVal);
		yggdrasil_assert(tVal <= 1.0);

		//Construct a new condition
		const SingleCondition_t tCondi(cDim, tVal);

		//Insert the condition into the vector we use of constructing
		transCondiVec.push_back(tCondi);
	}; //End for(i):

	//Construct the conditon
	const MultiCondition_t transCondi(condi.nDims(), transCondiVec);

	//Test if teh condition is correct
	yggdrasil_assert(transCondi.isValid());

	//Return the transformed conditions
	return transCondi;
}; //ENd contacr ri hyper cube


yggdrasil_hyperCube_t::MultiCondition_t
yggdrasil_hyperCube_t::mapFromUnit(
	const MultiCondition_t& 	condi)
 const
{
	//Make some tests, only for the debuig mode
	yggdrasil_assert(this->isValid());
	yggdrasil_assert(condi.isValid());

	//test the dimensionality
	if(this->m_bounds.size() != condi.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the hyper cube, " + std::to_string(m_bounds.size()) + ", does not match with the one of the condition " + std::to_string(condi.nDims()));
	};

	//This is the vector of conditions that
	//will be used fro the constructing
	MultiCondition_t::ConditionVector_t transCondiVec;

	//This is teh number of the conditions
	const Size_t nCondi = condi.nConditions();

	//Preallocate space
	transCondiVec.reserve(nCondi);

	//Going through the dimension and apply the transformation to the value
	for(Size_t i = 0; i != nCondi; ++i)
	{
		//Load the data
		const Size_t cDim = condi[i].getConditionDim();
		const auto   cVal = condi[i].getConditionValue();

		//Make consistency checks
		yggdrasil_assert(0.0  <= cVal);
		yggdrasil_assert(cVal <= 1.0);

		//Now transform the condition value
		const auto   tVal = m_bounds[cDim].mapFromUnit(cVal);

		//Construct a new condition
		const SingleCondition_t tCondi(cDim, tVal);

		//Insert the condition into the vector we use of constructing
		transCondiVec.push_back(tCondi);
	}; //End for(i):

	//Construct the conditon
	const MultiCondition_t transCondi(condi.nDims(), transCondiVec);

	//Test if teh condition is correct
	yggdrasil_assert(transCondi.isValid());

	//Return the transformed conditions
	return transCondi;
}; //End: map condition from unit











YGGDRASIL_NS_END(yggdrasil)
