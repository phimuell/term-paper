/**
 * \brief	This file implements some function of the hypercube, that
 * 		 deal with the testing if a sample is inside a cube or not.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_splitCommand.hpp>
#include <util/yggdrasil_hyperCube.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sampleList.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <tree_geni/yggdrasil_multiDimCondition.hpp>


//INcluide std
#include <vector>
#include <utility>
#include <tuple>

YGGDRASIL_NS_BEGIN(yggdrasil)


//FWD of the function, that implements the test

template<
	class 	Container_t
>
extern
bool
ygInternal_testIfAllInside(
	const yggdrasil_hyperCube_t& 		self,
	const Container_t& 			samples);


bool
yggdrasil_hyperCube_t::isInside(
	const SampleCollection_t& 	samples)
 const
{
	using DimensionArray_t = SampleCollection_t::DimensionArray_t;
	using ValueArrayDA_t   = DimensionArray_t::ValueArray_t;

	/*
	 * This implementation does not use the internal implemrentation.
	 * Buit a more optimized version.
	 */
	if(this->isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The hyper cube object is invalid.");
	};
	if(samples.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The passed sample collection is not valid.");
	};
	if(this->nDims() != samples.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the hyper cube (" + std::to_string(this->nDims()) + ") and the sampels (" + std::to_string(samples.nDims()) + ") does not agree.");
	};

	//This is the dimension of teh sampels
	const Size_t D = samples.nDims();

	/*
	 * Going through the dimensions and use the fast test that is implemented
	 * for arrays in the interval.
	 */
	for(Size_t d = 0; d != D; ++d)
	{
		//Get the dimensional array of thet dimensions
		const DimensionArray_t& dimArray = samples[d];

		//Get the underling array
		const ValueArray_t& dimValArray = dimArray.internal_getInternalArray();

		//Get the interval of that dimension
		const IntervalBound_t& currInterval = m_bounds[d];

		//Test the interval
		if(currInterval.isInside(dimValArray) == false)
		{
			/*
			 * Some dimension dontains at least one sample that is not inside
			 * so we have to retrurn false
			 */
			return false;
		};
	}; //ENd for(d): going through the dimensions

	/*
	 * All samples lied inside *this, so we return tur
	 */
	return true;
}; //End: isInside for sample collections.



bool
yggdrasil_hyperCube_t::isInside(
	const SampleArray_t& 	samples)
 const
{
	return ygInternal_testIfAllInside(*this, samples);
};


bool
yggdrasil_hyperCube_t::isInside(
	const SampleList_t& 	samples)
 const
{
	return ygInternal_testIfAllInside(*this, samples);
};


/*
 * ==================
 * Implementation
 */
template<
	class 	Container_t
>
bool
ygInternal_testIfAllInside(
	const yggdrasil_hyperCube_t& 		self,
	const Container_t& 			samples)
{
	using Sample_t = yggdrasil_hyperCube_t::Sample_t;

	if(self.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The hyper cube object is invalid.");
	};
	if(self.nDims() != samples.nDims())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension of the hyper cube (" + std::to_string(self.nDims()) + ") and the sampels (" + std::to_string(samples.nDims()) + ") does not agree.");
	};

	//This is the number of samples that we have to test
	const Size_t N = samples.nSamples();

	//This is the dimension of teh sampels
	const Size_t D = samples.nDims();

	//Going through the sample and test them
	for(Size_t i = 0; i != N; ++i)
	{
		//Load the sample
		const Sample_t iSample = samples.getSample(i);

		//Now testing the samples, goping throzg the components
		for(Size_t j = 0; j != D; ++j)
		{
			//Load the current value of the sample
			const Numeric_t jVal = iSample[j];

			//Test if the sample in that dimension is invalid
			if(isValidFloat(jVal) == false)
			{
				throw YGGDRASIL_EXCEPT_InvArg("Found an invalid sample.");
			};

			//Perform the testing
			if(self.m_bounds[j].isInside(jVal) == false)
			{
				/*
				 * We found a sample that is not inside the cube.
				 * This means we can stop no and return false
				 */
				return false;
			}; //End if: some was outside
		}; //End for(j): gouing throgh the dimensions
	}; //End for(i): going through the collection

	/*
	 * If we are here we have found no sample tah lies outside.
	 * So we can return true.
	 */
	return true;
}; //End: implementation




YGGDRASIL_NS_END(yggdrasil)
