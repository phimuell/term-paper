/**
 * \brief	This file implements the code for the recursive summing
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_indexArray.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <util/yggdrasil_generalBinEdges.hpp>
#include <util/yggdrasil_util.hpp>
#include <util/yggdrasil_hyperCube.hpp>


//Including std
#include <vector>
#include <algorithm>
#include <utility>
#include <limits>
#include <iterator>


YGGDRASIL_NS_START(yggdrasil)

using SumArray_t 	= yggdrasil_valueArray_t<Numeric_t>;
using SumIterator_t	= SumArray_t::const_iterator;


/*
 * \brief	This is the internal implementation of the recursive summing.
 *
 * Instead of operatiting on arrays it operates on iterators
 *
 * \param  startRange 		The start of the range.
 * \param  endRange 		Past the end pointer to the range
 */
template<
	class 	Iterator_t
>
extern
Real_t
ygInternal_recSumming(
	const Iterator_t 		startRange,
	const Iterator_t 		endRange);


Real_t
yggdrasil_recursiveSumming(
	const yggdrasil_valueArray_t<Real_t>& 	summands)
{
	//Get the number of summands
	const Size_t nSummands = summands.size();

	//Solve some trivail base cases
	if(nSummands == 0)
	{
		return Real_t(0.0);
	};

	if(nSummands == 1)
	{
		yggdrasil_assert(isValidFloat(summands[0]));

		return Real_t(summands[0]);
	};

	/*
	 * Handle the actual case
	 */
	return ygInternal_recSumming(summands.cbegin(), summands.cend());
}; //End: interface fucntions


Real_t
yggdrasil_recursiveSumming(
	const yggdrasil_valueArray_t<Numeric_t>& 	summands)
{
	//Get the number of summands
	const Size_t nSummands = summands.size();

	//Solve some trivail base cases
	if(nSummands == 0)
	{
		return Real_t(0.0);
	};

	if(nSummands == 1)
	{
		yggdrasil_assert(isValidFloat(summands[0]));

		return Real_t(summands[0]);
	};

	/*
	 * Handle the actual case
	 */
	return ygInternal_recSumming(summands.cbegin(), summands.cend());
}; //End: interface fucntions



template<
	class 	Iterator_t
>
Real_t
ygInternal_recSumming(
	const Iterator_t 		startRange,
	const Iterator_t 		endRange)
{
	//Get the number elements in the range
	const Size_t nSummands = std::distance(startRange, endRange);

	//Test the zero case, it makes things more clearer down the road
	if(nSummands == 0)
	{
		return Real_t(0.0);
	};

	//Test if the base case can be applied
	if(nSummands <= 100)
	{
		Real_t accumulator = 0.0;

		//Going through the range and summ up
		for(Iterator_t it = startRange; it != endRange; ++it)
		{
			yggdrasil_assert(isValidFloat(*it));

			accumulator += *it;
		}; //End for(it): summing up

		//Make a test thsi willalso be active in debug
		if(isValidFloat(accumulator) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The summation detected an invalid number.");
		};

		//Return the summation value
		return accumulator;
	}; //End: handling base case


	/*
	 * We have to go down firther
	 */

	//Get the middle
	const Iterator_t middel = startRange + nSummands / 2;


	/*
	 * Compute the sum of the two subranges
	 */
	const Real_t sum1 = ygInternal_recSumming(startRange, middel);
	const Real_t sum2 = ygInternal_recSumming(middel, endRange);

	//Sum up the two sums
	const Real_t sum12 = sum1 + sum2; 	//This could be made better

	//Return
	return sum12;
}; //End: internal implemetation of summing







YGGDRASIL_NS_END(yggdrasil)

