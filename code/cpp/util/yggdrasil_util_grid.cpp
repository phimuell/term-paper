/**
 * \brief	This file implements the code for the grid generation.
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_indexArray.hpp>
#include <core/yggdrasil_valueArray.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_arraySample.hpp>

#include <util/yggdrasil_generalBinEdges.hpp>
#include <util/yggdrasil_util.hpp>
#include <util/yggdrasil_hyperCube.hpp>


//Including std
#include <vector>
#include <algorithm>
#include <utility>
#include <limits>


YGGDRASIL_NS_START(yggdrasil)


yggdrasil_arraySample_t
yggdrasil_generateGrid(
	const yggdrasil_hyperCube_t&	domain,
	Size_t	 			points1,
	Size_t  			points2,
	const bool 			woLowest)
{
	if(domain.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The domain is invalid.");
	};

	//This is the real dimension
	const Size_t D = domain.nDims();
	if(D <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The domain has zero dimensions.");
	};
	if(D > 2)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The domain has more than two dimension.");
	};
	if(points1 == 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The number of points in the first dimension is zero.");
	};

	//This is the dimension that must be used if we are
	//in dirichlet mode
	const Size_t effectiveDim = D;

	if(effectiveDim == 1)
	{
		/*
		 * One dimension
		 */

		//Get the geomentrical properties of the domain
		const Numeric_t length1     = domain.at(0).getLength();
		const Numeric_t spacing1    = length1 / ((woLowest == true) ? (points1 + 1) : points1);
		const Numeric_t lowerBound1 = domain.at(0).getLowerBound() + ((woLowest == true) ? spacing1 : 0.0);

		yggdrasil_arraySample_t ret(D, points1);
		yggdrasil_assert(ret.nSamples() == points1);
		yggdrasil_assert(ret.nDims() == 1);


		for(Size_t i = 0; i != points1; ++i)
		{
			const Numeric_t x_i = i * spacing1 + lowerBound1;
			ret.loadSample(&x_i, i);
		}; //End for(i):

		return ret;
	}
	else
	{
		/*
		 * Two dimension
		 */
		yggdrasil_assert(effectiveDim == 2);
		if(points2 == 0)
		{
			points2 = points1;
		};

		//Get the geomentrical properties of the domain
		const Numeric_t length1     = domain.at(0).getLength();
		const Numeric_t length2     = domain.at(1).getLength();

		const Numeric_t spacing1    = length1 / ((woLowest == true) ? (points1 + 1) : points1);
		const Numeric_t spacing2    = length2 / ((woLowest == true) ? (points1 + 1) : points2);

		const Numeric_t lowerBound1 = domain.at(0).getLowerBound() + ((woLowest == true) ? spacing1 : 0.0);
		const Numeric_t lowerBound2 = domain.at(1).getLowerBound() + ((woLowest == true) ? spacing2 : 0.0);

		const Numeric_t nSamples = points1 * points2;
		yggdrasil_arraySample_t ret(D, nSamples);
		yggdrasil_assert(ret.nDims() == 2);
		yggdrasil_assert(ret.nSamples() == points1 * points2);

		Numeric_t x_k[2];

		Size_t k = 0;
		for(Size_t i = 0; i != points1; ++i)
		{
			x_k[0] = lowerBound1 + i * spacing1;

			for(Size_t j = 0; j != points2; ++j)
			{
				x_k[1] = lowerBound2 + j * spacing2;

				yggdrasil_assert(k < ret.nSamples());
				ret.loadSample(x_k, k);
				k += 1;
			}; //End for(j):
		};//End for(i)

		yggdrasil_assert(k == ret.nSamples());

		return ret;
	}; //End else: D==2

	//We will never be here
	throw YGGDRASIL_EXCEPT_illMethod("Enered unreachable code.");
}; //End generate grid


YGGDRASIL_NS_END(yggdrasil)

