#pragma once
/**
 * \brief	This file implements a single test result.
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>


//Incluide std
#include <memory>
#include <tuple>


YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD of the node
class yggdrasil_DETNode_t;

//FWD of the test rank
template<
	class Key_T>
class yggdrasil_statTestRank_t;

/**
 * \class 	yggdrasil_statSingleTestResult_t
 * \brief	This class implements a single test result.
 *
 * It stores exactly one test result.
 * It stores the p-bvalue of a test, and the size component.
 * The size component is used as a tie breaker.
 *
 * It is also so that every instant of this saves a key.
 * This can be the dimension or a dimension combination.
 *
 * It is important that this class can only encode a single test.
 *
 * \tparam  Key_T	This is the type of key that is used.
 */
template<
	class Key_T>
class yggdrasil_statSingleTestResult_t
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using Key_t 		= Key_T;			//!< This is the type of the key.
	using Numeric_t 	= ::yggdrasil::Numeric_t;	//!< This is the type of numerics.


	/*
	 * =================================
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * The building constructor is takes a key, p-value and
	 * a size component part and constructs such an object.
	 *
	 * Notice for sorting the inverse part of the size is used.
	 *
	 * \param  key 		The key.
	 * \param  pValue	The p-Value.
	 * \param  sizePart 	The size component of the test.
	 *
	 * \throws	Some basic consistency checks are performed.
	 */
	yggdrasil_statSingleTestResult_t(
		const Key_t& 		key,
		const Numeric_t 	pValue,
		const Numeric_t 	sizePart)
	 :
	  m_key(key),
	  m_pValue(pValue),
	  m_sizeValue(sizePart),
	  m_isTrivialTest(false)
	{
		if(m_pValue < 0.0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The p-avlue was negative.");
		};
		if(m_pValue >= 1.001)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The p-Value was to high, it was " + std::to_string(m_pValue));
		};
		if(m_sizeValue <= 0.0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The size part is negative or zero.");
		};
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The construction resulted in an invalid object.");
		};
	}; //End: building constructor


	/**
	 * \brief	This function is the second building constructor.
	 *
	 * Only the key is needed to construct an instance.
	 * The values are set to NAN.
	 * This means that object that is resulted is invalid.
	 *
	 * \param  key		This is the key that should be used.
	 */
	yggdrasil_statSingleTestResult_t(
		const Key_t& 	key)
	 noexcept
	 :
	  m_key(key),
	  m_pValue(NAN),
	  m_sizeValue(NAN),
	  m_isTrivialTest(false)
	{};


	/**
	 * \brief	Default constructor
	 *
	 * Is allowed but produces an invalid object.
	 */
	yggdrasil_statSingleTestResult_t()
	 noexcept
	 = delete;


	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_statSingleTestResult_t(
		const yggdrasil_statSingleTestResult_t&)
	 = default;


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_statSingleTestResult_t(
		yggdrasil_statSingleTestResult_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_statSingleTestResult_t&
	operator= (
		const yggdrasil_statSingleTestResult_t&)
	 noexcept
	 = default;

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_statSingleTestResult_t&
	operator= (
		yggdrasil_statSingleTestResult_t&&)
	 noexcept
	 = default;



	/*
	 * =====================
	 * Normal functions
	 */
public:
	/**
	 * \brief	This function returns the key of \c *this
	 */
	const Key_t&
	key()
	 const
	{
		return m_key;
	}; //End: key


	/**
	 * \brief	This function returns the p-Value of \c *this.
	 *
	 * \throw 	Thsi funciton throws if \c *this is not valid.
	 */
	Numeric_t
	pValue()
	 const
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Can not access an invalid test result.");
		};

		return m_pValue;
	}; //End pValue




	/**
	 * \brief	This function returns true if \c *this is valid.
	 *
	 * A valid object is an object that has no NAN in the pValue or the isze value.
	 * The key is not tested, since it is treated in an agnostic way.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		return (::std::isnan(m_pValue) == false && ::std::isnan(m_sizeValue) == false);
	};


	/**
	 * \brief	This function returns \c true if *this makrs a test that is done.
	 *
	 * A test that is done is a test that has a valid p-Value and a valid size
	 * value.
	 * This is basically an alias of the isVAlid() function, but with a better name.
	 */
	bool
	isTestDone()
	 const
	 noexcept
	{
		return (::std::isnan(m_pValue) == false && ::std::isnan(m_sizeValue) == false);
	};


	/**
	 * \brief	Returns true if *this is a trivial test.
	 *
	 * A trivial test is a test, that is passed by itself.
	 */
	bool
	isTrivialTest()
	 const
	 noexcept
	{
		yggdrasil_assert(m_isTrivialTest == true ? (this->isTestDone() && (m_pValue >= 1.0)) : true);
		return m_isTrivialTest;
	};


	/**
	 * \brief	This function compares \c *this with rhs, it returns true if \c *this is smaller than rhs.
	 *
	 * The comaprisson is done in the way that first the pValue is used.
	 * If they are the same then 1/sizeValue is used, meaning that larger values
	 * will go less.
	 *
	 * \param  rhs		The other result.
	 *
	 * \throws 	If \c *this or \c rhs are invalid.
	 *
	 * \note	This function is implemented by using std::tuple.
	 */
	bool
	operator< (
		const yggdrasil_statSingleTestResult_t&	rhs)
	 const
	{
		if(this->isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("*this is invalid in a comparisson.");
		};
		if(rhs.isValid() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("rhs is invalid, so no comaprisson possible.");
		};

		//Using tie to construct a tuple and use the operator< of tuple.
		const Numeric_t s1 = 1.0 / (this->m_sizeValue);
		const Numeric_t s2 = 1.0 / (  rhs.m_sizeValue);
		return (std::tie(this->m_pValue, s1) < std::tie(rhs.m_pValue, s2));
	}; //End: operator<


	/*
	 * =======================
	 * Friends
	 */
private:
	friend
	class yggdrasil_statTestRank_t<Key_T>;


	/*
	 * ==========================
	 * Privaze fucntions
	 */
private:
	/**
	 * \brief	This function transforms *thsi into a trivial test.
	 *
	 * \throw	If *this is allready set.
	 */
	void
	makeTrivial(
		const bool isTrivial = false)
	{
		if(this->isTestDone() == true)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("*this is done, so it can not made trivial.");
		};
		if(isTrivial == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("It apears that you are not aware that you want to make a trivbial test.");
		};

		m_isTrivialTest = true;
		m_pValue = 1.0001;
		m_sizeValue = Constants::YG_SMALLEST_LENGTH;

		yggdrasil_assert(this->isTestDone() == true);

		return;
	}; //End: make trivial



	/*
	 * =========================
	 * Private Memebers
	 */
private:
	Key_t 			m_key;		//!< This is the key
	Numeric_t 		m_pValue;	//!< This is the p value
	Numeric_t 		m_sizeValue;	//!< This is the size value, that is used as tie breaker
	bool 			m_isTrivialTest;//!< This indicates if *this is a trivial test.
						//!<  A trivial test is a test that is passed.
}; //End class: yggdrasil_statSingleTestResult_t


YGGDRASIL_NS_END(yggdrasil)
