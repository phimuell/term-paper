#pragma once
/**
 * \brief	This file implements a class that is able to enumerate the subdomains.
 *
 * This class in manyl used for internal purpose.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

//INcluide std
#include <climits>

YGGDRASIL_NS_BEGIN(yggdrasil)

//FWD
class yggdrasil_subDomainRanges_t;

/**
 * \brief	This class is used to enumerate and identify a subdomain.
 * \class 	yggdrasil_subDomainEnum_t
 *
 * The enumerating is done by storing the way of the decending.
 * This is done by a bit string.
 * A zero means that we go to the left and a one means we go to the right.
 *
 * We allow a depth of Constants::MAMX_SPLIT_DEPTH, this results in rougthly 1'000'000'000
 * many addressable subdomains, since we only work with 2 this is more then enought.
 *
 * This class is inmutable.
 * This means that modification operation does not changes the object itself.
 * Instead \c *this is copied and then the copy is modified.
 *
 * This class is inmutable.
 * This means that modification operation does not changes the object itself.
 * Instead \c *this is copied and then the copy is modified.
 * The copy is then returned to the caller.
 *
 * This class supports assignments.
 *
 * \note	This class is used inside the splitting process, to directly address
 * 		 the many resulting new subdomains (leafs).
 * 		 It is manyl for internal purpose, so put on sopme safty glasses.
 */
class yggdrasil_subDomainEnum_t
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Base_t 		= ::yggdrasil::uInt_t;		//!< This is the base type for sorage the size
	using Count_t 		= ::yggdrasil::uInt8_t; 	//!< This is for counting

	/*
	 * ========================
	 * Constants
	 */
public:
	static const Size_t MAX_DEPTH = Constants::MAX_SPLIT_DEPTH;	//!< This is the maximum depth we allow this to go
	static_assert(sizeof(Base_t) * CHAR_BIT >= MAX_DEPTH, "The underling bitstring is to short.");
	static_assert((~Count_t(0)) >=  MAX_DEPTH, "We can not count that many splits.");


	/*
	 * ===================
	 * Constructors
	 */
public:
	/**
	 * \brief	Default constructor.
	 *
	 * Is defleted.
	 */
	yggdrasil_subDomainEnum_t()
	 noexcept :
	  m_descendWay(0),
	  m_depth(0)
	{};


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_subDomainEnum_t(
		const yggdrasil_subDomainEnum_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_subDomainEnum_t(
		yggdrasil_subDomainEnum_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_subDomainEnum_t&
	operator= (
		const yggdrasil_subDomainEnum_t&) = default;


	/**
	 * \brief 	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_subDomainEnum_t&
	operator= (
		yggdrasil_subDomainEnum_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_subDomainEnum_t() = default;


	/*
	 * =================
	 * Funcitons
	 */
public:
	/**
	 * \brief	Return the depth of \c *this.
	 *
	 * Another interpretation of the dept is, the number of descent operation that was performed.
	 */
	Count_t
	getDepth()
	 const
	 noexcept
	{
		return m_depth;
	};


	/**
	 * \brief	Transform the descend path into an integer.
	 *
	 * As type we return a Base_t value.
	 * This value can then be used to index the resulting subdomains in an unique way.
	 *
	 * \throw 	This function throws if the depth is zero.
	 */
	Base_t
	getWay()
	 const
	{
		if(m_depth == 0)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The dept of *this is zero so there is no unique way.");
		};

		return m_descendWay;
	};


	/**
	 * \brief	Returns true if the \c i th decend was left.
	 *
	 * \param  i 	Which descend.
	 *
	 * \throw 	If \c i is greater than the current depth (start counting at zero).
	 */
	bool
	wasLeft(
		const Int_t 	i)
	 const
	{
		if(i > m_depth)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Requested the depth of " + std::to_string(i) + ", but the depth of *this is only " + std::to_string(m_depth));
		};

		return ((m_descendWay >> i) & 1) == 0 ? true : false;
	}; //End: wasLeft


	/**
	 * \brief	Returns true if the \c i th decent das going to the right.
	 *
	 * \param  i 	Which decend.
	 *
	 * \throw 	If \c i is greater than the cirrent depth (start counting at zero).
	 */
	bool
	wasRight(
		const Int_t 	i)
	 const
	{
		if(i > m_depth)
		{
			throw YGGDRASIL_EXCEPT_InvArg("Requested the depth of " + std::to_string(i) + ", but the depth of *this is only " + std::to_string(m_depth));
		};

		return ((m_descendWay >> i) & 1) == 1 ? true : false;
	}; //End: wasRight


	/**
	 * \brief	This function performs a decend step to the left.
	 *
	 * This class does not mutate \c *this.
	 * Instead the mutation is performed on a copy of \c *this,
	 * which is then returned to the caller.
	 *
	 * \throw	If the maximum supported depth is reached.
	 */
	yggdrasil_subDomainEnum_t
	decendToLeft()
	 const
	{
		if(m_depth >= MAX_DEPTH)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Already at the deepest level, can not decend further.");
		};

		yggdrasil_subDomainEnum_t newDom(*this);

		//Since we are left, we only have to increase the depth
		newDom.m_depth += 1;

		return newDom;
	};


	/**
	 * \brief	This function performs a decend to the right.
	 *
	 * This class does not mutate \c *this.
	 * Instead the mutation is performed on a copy of \c *this,
	 * which is then returned to the caller.
	 *
	 * \throw	If the maximum supported depth is reached.
	 */
	yggdrasil_subDomainEnum_t
	decendToRight()
	 const
	{
		if(m_depth >= MAX_DEPTH)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("Already at the deepest level, can not decend further.");
		};

		//Make a copy of *this
		yggdrasil_subDomainEnum_t newDom(*this);

		//We must set the bit before we increase the counter
		newDom.m_descendWay = newDom.m_descendWay | (1 << newDom.m_depth);

		//Increase
		newDom.m_depth += 1;

		return newDom;
	};


	/**
	 * \brief	Returns true if \c *this did not record any splits.
	 *
	 * It basically checks if the depth is zero.
	 */
	bool
	isZeroDepth()
	 const
	 noexcept
	{
		return (m_depth == 0) ? true : false;
	};


	/*
	 * ====================
	 * Internal function
	 */
private:
	/**
	 * \brief	This is a constructo to explicitly construct a subDomainEnum_t.
	 *
	 * This is an internal function, thus it is private.
	 *
	 * \param  descendWay		This is the decend way
	 * \param  depth		This is the dept
	 *
	 * \throw 	If some inconcistencies are detected.
	 *
	 * \note	SOme inconsistencies can not be detected.
	 */
	yggdrasil_subDomainEnum_t(
		const Int_t 	descendWay,
		const Count_t 	depth) :
	  m_descendWay(descendWay),
	  m_depth(depth)
	{
		if(m_depth > MAX_DEPTH)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The depth is to large, it is " + std::to_string(depth) + " maximum value is " + std::to_string(MAX_DEPTH));
		};

		if((m_descendWay >> m_depth) != 0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The decend way is to deep.");
		};
	};

	//Make the other funtion a firend if this
	//It is the whole class, It is not nice, but more qwork to do it that way.
	friend class ::yggdrasil::yggdrasil_subDomainRanges_t;


	/*
	 * ===================
	 * Private memeber
	 */
private:
	Base_t 		m_descendWay;	//!< This is the way of decending
	Count_t 	m_depth;	//!< This is the dept
}; //End class: yggdrasil_subDomainEnum_t




YGGDRASIL_NS_END(yggdrasil)
