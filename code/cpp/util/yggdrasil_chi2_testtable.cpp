/**
 * \brief	This file implement the functionality offered by the chi2testtable function of the r code.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_util.hpp>

#include <samples/yggdrasil_dimensionArray.hpp>
#include <samples/yggdrasil_sample.hpp>

#include <util/yggdrasil_chi2_testtable.hpp>

#include <stat_tests/yggdrasil_chi2_utilities.hpp>


//Incluide std
#include <memory>
#include <vector>
#include <numeric>
#include <algorithm>


YGGDRASIL_NS_BEGIN(yggdrasil)



bool
yggdrasil_chi2TestTable_t::buildInitialChi2Table(
		const uInt_t 			d,
		const IntervalBound_t&		domain,
		const DimArray_t&		dimArray,
		cParModel_ptr 			parModel,
		const bool 			useSorting)
{
	/*
	 * First we have to test, if we are valid or not
	 */
	if(this->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this is already associated to data. canonly be associated once.");
	};
	if(domain.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The provided domain is not valid.");
	};
	if(dimArray.isDimensionIllegal() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimensional array has an invalid dimension.");
	};
	if((uInt_t)dimArray.getAssociatedDim() != d)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimension is not the same as with the array.");
	};

	//This is the number of samples that we have
	const Size_t nSamples = dimArray.nSamples();

	//Comnpute the number of bins that should be made
	const Size_t numberOfBins = yggdrasil_chi2_calcNBins(m_sigLevel, m_isMultiVariate, nSamples);

	if(numberOfBins <= 0)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The number of bins where zero or negatiove.");
	};

	//Create the bin edge object
	m_bins = parModel->generateBins(d, numberOfBins, domain);

	if(m_bins.nBins() <= 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bins that where supplied dos not conatin a bin or negativ bins");
	};
	if(m_bins.isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The generated bins are not associated.");
	};


	//Create the bin counts
	bool binDistriSucc = false;
	if(useSorting == true)
	{
		binDistriSucc = this->priv_distributeToBins_sort(dimArray);
	}
	else
	{
		binDistriSucc = this->priv_distributeToBins_std(dimArray);
	};

	if(binDistriSucc == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The creation of the bin count arrays failed.");
	};


	//We return true to indicate that everything went good
	return true;
}; //End: build table





bool
yggdrasil_chi2TestTable_t::internal_buildInitialChi2TableDirect(
		const DimArray_t&		dimArray,
		const BinEdgesArray_t& 		bins,
		const bool 			useSorting)
{
	/*
	 * First we have to test, if we are valid or not
	 */
	if(this->isAssociated() == true)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("*this is already associated to data. canonly be associated once.");
	};
	if(dimArray.isDimensionIllegal() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dimensional array has an invalid dimension.");
	};
	if(bins.nBins() == 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The were zero bins passed.");
	};

	//This is the number of samples that we have
	const Size_t nSamples = dimArray.nSamples();

	//Comnpute the number of bins that should be made
	const Size_t numberOfBins = bins.nBins();

	//Create the bin edge object
	m_bins = bins;

	if(m_bins.nBins() <= 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bins that where supplied dos not conatin a bin or negativ bins");
	};
	if(m_bins.isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The generated bins are not associated.");
	};


	//Create the bin counts
	bool binDistriSucc = false;
	if(useSorting == true || Constants::TEMP_ARRAY_TT_KB > 0)	//We need memopry to achive that
	{
		binDistriSucc = this->priv_distributeToBins_sort(dimArray);
	}
	else
	{
		binDistriSucc = this->priv_distributeToBins_std(dimArray);
	};

	if(binDistriSucc == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The creation of the bin count arrays failed.");
	};


	//We return true to indicate that everything went good
	return true;
}; //End: build table




Real_t
yggdrasil_chi2TestTable_t::computeChi2Value(
	const ValueArray_t& 	expectedCounts)
 const
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("This is not associated.");
	};

	//Get the number of bins that we are managing
	const Size_t nBins = this->nBins();
	yggdrasil_assert(nBins == this->m_binCounts.size());

	//Get the length of the array for the expected count
	const Size_t nExpectedCountVal = expectedCounts.size();

	if(nExpectedCountVal != nBins)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The number of bins (" + std::to_string(nBins) + ") and the length of teh expected couznts array (" + std::to_string(nExpectedCountVal) + ") differs.");
	};

	::yggdrasil::Real_t Chi2 = 0.0;
	for(Size_t i = 0; i != nExpectedCountVal; ++i)
	{
		//This is the observed count
		const Size_t    obsCountI = this->m_binCounts[i];
		const Numeric_t obsCount  = obsCountI;

		//No scalling is need, this is different when comparing with
		//earlier versions. Now the number of samples is inccoperated
		//in the getExpectedCountsInBins() function of the parameteric
		//model.
		const Numeric_t expCount = expectedCounts[i];

		//Test the value
		if(::std::isnan(expCount) == true)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The expected number of samples of bin " + std::to_string(i) + " is nan.");
		};
		if(expCount < -0.001)  //A save margin
		{
			throw YGGDRASIL_EXCEPT_InvArg("The expected number of samples of bin " + std::to_string(i) + " is negative, the value was " + std::to_string(expCount) + ".");
		};

		//Test if the value is supper small
		const bool expCountSmall = (expCount < 100 * Constants::EPSILON) ? true : false;

		//In the case that both are zero we have a contriibution of zero
		if((obsCountI == 0) && (expCountSmall == true))
		{
			//There is no contribution so we can continue
			continue;
		}; //End if: Zero contribution

		//Compute the contribution
		//This can result in infinity but not in nan
		const Real_t diffBetweenObsAndExp  = obsCount - expCount;
		const Real_t diffBetweenObsAndExp2 = diffBetweenObsAndExp * diffBetweenObsAndExp;

		const Numeric_t contribution = diffBetweenObsAndExp2 / expCount;
		yggdrasil_assert(::std::isnan(contribution) == false);

		//Sum up the test statistic
		Chi2 += contribution;

		//Test if the contribution is ionfinity, then we can stop
		if(::std::isinf(Chi2) == true)
		{
			break;
		};
	}; //End for(i)


	//Test a final official time iof teh value is not nan
	if(::std::isnan(Chi2) == true)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The Chi2 value has become nan.");
	};


	//Return the value
	return Chi2;
}; //End: computeChi2Value







void
yggdrasil_chi2TestTable_t::unboundData()
{
	if(this->isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("Tried to unbound an empty test table.");
	};

	//unbound the bin edge
	m_bins.unboundData();

	//Unbound this
	BinCountArray_t emptyCountArray;
	m_binCounts.swap(emptyCountArray);

	return;
}; //End: unbound data


bool
yggdrasil_chi2TestTable_t::priv_distributeToBins_std(
	const DimArray_t&	dimArray)
{
	if(m_bins.isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bins are not associated.");
	};
	if(m_bins.nBins() <= 0)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The number of bins is zero, but it is allocated.");
	};
	if(m_binCounts.empty() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bin count array is allocated.");
	};

	//Get the number of bins
	const Size_t nBins = m_bins.nBins();

	//The number of samples
	const Size_t nSamples = dimArray.nSamples();


	//Trapü door for the case that only one bin is there
	if(nBins == 1)
	{
		//All belongs to one bin
		m_binCounts.resize(1);
		m_binCounts.at(0) = dimArray.nSamples();

		return true;
	}; //only one bin

	//A special case for two bins
	if(nBins == 2)
	{
		//Count the number of samples that are in the first bin
		const auto binBound = m_bins.getUpperBound(0);

		//TODO: Unrole
		Size_t samplesInFirstBin = 0;
		for(Size_t i = 0; i != nSamples; ++i)
		{
			if(dimArray[i] < binBound)
			{
				samplesInFirstBin += 1;
			};
		}; //End for(i)

		//Fill in the count
		m_binCounts.resize(2);

		//Fill in the results
		m_binCounts.at(0) = samplesInFirstBin;
		m_binCounts.at(1) = nSamples - samplesInFirstBin;

		//Return
		return true;
	}; //End: only two bins


	/*
	 * General case
	 */

	//get the iterators to the inner nodes
	const BinEdge_it innerEdgeBegin = m_bins.innerEdgesBegin();
	const BinEdge_it innerEdgeEnd   = m_bins.innerEdgesEnd();

	//Make sanity checks
	yggdrasil_assert(m_bins.getLowerBound(0) < *innerEdgeBegin);
	yggdrasil_assert(m_bins.getUpperBound(nBins - 1) == *innerEdgeEnd);

	//Now we resize the vector
	m_binCounts.assign(nBins, 0);

	//Loop over all the samples
	for(Size_t s = 0; s != nSamples; ++s)
	{
		//Load the sample
		const Numeric_t sValue = dimArray[s];

		//Now iterate over all the inner edge boundary to find the correct one
		Size_t bIdx = 0;
		for(BinEdge_it b_it = innerEdgeBegin; b_it != innerEdgeEnd; ++b_it)
		{
			/*
			 * Since the bins are ordered, we can iterate throug it.
			 * if the curreent bin edge is GREATER than the value of the sample
			 * we have found the bin in which it belongs
			 */
			if(sValue < *b_it)
			{
				//We have found, we can end the iteration
				break;
			}; //Found

			//We have not found it, so we must increment the index of the bin
			bIdx += 1;
		}; //End for(b_it)

		yggdrasil_assert(bIdx < nBins);

		//Now increment
		m_binCounts.at(bIdx) += 1;
	}; //End for(s): loop over samples

	//It is a trivial test, but I sleep much more confident with it
	yggdrasil_assert(::std::accumulate(m_binCounts.cbegin(), m_binCounts.cend(), Size_t(0)) == nSamples);

	//Now we return true, for the success
	return true;
}; //End: distribute to bins slow



bool
yggdrasil_chi2TestTable_t::priv_distributeToBins_sort(
	const DimArray_t&	dimArray)
{
	if(m_bins.isAssociated() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bins are not associated.");
	};
	if(m_binCounts.empty() == false)
	{
		throw YGGDRASIL_EXCEPT_LOGIC("The bin count array is allocated.");
	};

	//Get the number of bins
	const Size_t nBins = m_bins.nBins();
	yggdrasil_assert(nBins > 0);

	//The number of samples
	const Size_t nSamples = dimArray.nSamples();

	//Make the test if it is possible to perfom the computation
	const ::yggdrasil::Size_t maxSupportedSample = ::std::numeric_limits<SortIndex_t>::max();
	if(maxSupportedSample <= nSamples)	//It is importat that it is a <= comparison
	{
		/*
		 * NOTICE:
		 *
		 * If you run into this problem, and thinks you have the memeory (money) to allow for
		 * that many samples then you can remove this restriction by changing the line
		 * 	using Size_t = ::yggdrasil::uInt32_t;
		 * to
		 * 	using Size_t = ::yggdrasil::Size_t;
		 * However this is only possible if you where a consistent programer.
		 *
		 * Also be warned, that thsi will increase the memeory needs.
		 *
		 * There is an old saying.
		 * "X is to memory what Ronald Reagan is to money"
		 * This saying is also applicable to this.
		 */
		throw YGGDRASIL_EXCEPT_RUNTIME("More than supported samples. There where " + std::to_string(nSamples) + " but only " + std::to_string(maxSupportedSample) + " are supported.");
	}; //End problem with handling

	//Handle the case of zero samples
	if(nSamples == 0)
	{
		m_binCounts.assign(nBins, 0);

		return true;
	}; //End zero samples


	//Trapü door for the case that only one bin is there
	if(nBins == 1)
	{
		//All belongs to one bin
		m_binCounts.resize(1);
		m_binCounts.at(0) = dimArray.nSamples();

		return true;
	}; //only one bin


	/*
	 * General case
	 */

	//Allocate the bins
	yggdrasil_assert(m_binCounts.empty());
	m_binCounts.assign(nBins, 0);

	//Calculate the numbers of samples that we are allowed to handle in one go.
	//This is due to the limited memory
	const Size_t nSamplesPerIteration = Constants::TEMP_ARRAY_TT_KB * 1024 / sizeof(SortIndex_t);
	yggdrasil_assert(nSamplesPerIteration > 0);

	//Calculate the numbers of times we have to apply the recursive binning algotithm
	const Size_t nBinningSteps_full = (nSamples / nSamplesPerIteration);	//This is the number of fulol steps
	const Size_t nBinningSteps  = nBinningSteps_full + ((nSamples % nSamplesPerIteration) == 0 ? 0 : 1);	//This is for handling the left overes


	/*
	 * Now we iterate through the samples and call the binning
	 * fucntion, we only get the indices of the index array,
	 * but from that we can calculate the size
	 */

	//This variable stores the index that are needed to supply
	yggdrasil_valueArray_t<SortIndex_t> indexArray;

	//Going through the loop
	for(Size_t i = 0; i != nBinningSteps; ++i)
	{
		//Caculating the sizes we now need
		const Size_t thisStepStart  = i * nSamplesPerIteration;
		const Size_t thisStepStop   = ::std::min<Size_t>((i + 1) * nSamplesPerIteration, nSamples);
		yggdrasil_assert(thisStepStart < thisStepStop);
		yggdrasil_assert(thisStepStart >= 0);

		//This is the size of the current step
		const Size_t sizeCurrentSpet = thisStepStop - thisStepStart;
		yggdrasil_assert(sizeCurrentSpet > 0);

		//Now generate the index array
		//It is important that we fill it with the correct values
		indexArray.resize(sizeCurrentSpet);
		{
			//This is the counter for the array
			Size_t k = 0;

			//The loop variable is for the index array, not for indexing the index array
			for(Size_t mIdx = thisStepStart; mIdx != thisStepStop; ++mIdx)
			{
				yggdrasil_assert(k < indexArray.size());
				indexArray[k] = mIdx;

				//Increment k Manualy
				k += 1;
			}; //ENd for(mIdx):
			yggdrasil_assert(k == indexArray.size());
		}; //End scope: filling the index array

		//Now we can execute the binning code
		const yggdrasil_valueArray_t<Size_t> binEdgesIdx =
			yggdrasil_recArgBinning(dimArray, m_bins, &indexArray, false); //No sorting is needed

		//Now we update the count in the bining array
		for(Size_t bin = 0; bin != nBins; ++bin)
		{
			const Size_t binIdxBegin = binEdgesIdx.at(bin    );
			const Size_t binIdxEnd   = binEdgesIdx.at(bin + 1);
			yggdrasil_assert(binIdxBegin <= binIdxEnd);

			//The numbers of samples in that bin
			const Size_t nSamplesInBin = binIdxEnd - binIdxBegin;

			//Add
			m_binCounts.at(bin) += nSamplesInBin;
		}; //End for(bin):
	}; //End for(i): binning steps


	//It is a trivial test, but I sleep much more confident with it
	yggdrasil_assert(::std::accumulate(m_binCounts.cbegin(), m_binCounts.cend(), Size_t(0)) == nSamples);

	//Return true since everyting went well
	return true;
}; //End: sort version





YGGDRASIL_NS_END(yggdrasil)



