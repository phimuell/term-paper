/*
 * \brief	Contains the implementation of the factory for the split finder.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <factory/yggdrasil_treeBuilder_Enums.hpp>
#include <factory/yggdrasil_treeBuilder.hpp>

//Including of the interfaces
#include <interfaces/yggdrasil_interface_splitStrategy.hpp>

/*
 * Including of the actual implementations
 */
#include <split_scheme/yggdrasil_scoreBasedSplitting.hpp>
#include <split_scheme/yggdrasil_sizeBasedSplitting.hpp>


//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_splitFinder_i::SplitStragety_ptr
yggdrasil_splitFinder_i::FACTORY(
	const yggdrasil_treeBuilder_t&		builder)
{
	using SplitStragety_ptr = yggdrasil_splitFinder_i::SplitStragety_ptr;

	if(builder.isMedianSplitter() == true)
	{
		return SplitStragety_ptr(new yggdrasil_scoreSplit_t());
	}
	else
	{
		return SplitStragety_ptr(new yggdrasil_sizeSplit_t());
	};


	throw YGGDRASIL_EXCEPT_RUNTIME("Reached an unreachable part.");
	return SplitStragety_ptr(nullptr);
};





YGGDRASIL_NS_END(yggdrasil)
