#pragma once
/**
 * \brief	THis file contains the enums that are used to select the
 * 		 correct implementation.
 *
 * These are also the enums that have to be extended if new models and
 * test are added to yggdrasil.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

//INcluide std
#include <string>

YGGDRASIL_NS_BEGIN(yggdrasil)


/*
 * ======================
 * 	P A R    M O D E L
 */

/**
 * \brief	This is the enum for selecting the model.
 * \enum 	eParModel
 *
 * This is the enum that names the parameteric model.
 *
 * If a new model is added to Yggdrasil it must also
 * be set here.
 */
enum class eParModel : ::yggdrasil::Size_t
{
	NoModel,	//!< This is the illegal model that indicated no selecting.
	ConstModel,	//!< This is the constant model.
	LinModel,	//!< This is the linear model.
}; //End enum(eParModel)


/**
 * \brief	This function returns true if the
 * 		 model enum parMod is the no Model.
 *
 * Note that this function is not able to detect if the
 * model exuist or not. It can only check if the argument
 * is not the no model.
 *
 * \param  parMod	The model enum to examine.
 */
inline
constexpr
bool
yggdrasil_isNoModel(
	const eParModel 	parMod)
{
	return (parMod == eParModel::NoModel) ? true : false;
}; //ENd: isNoModel



/**
 * \brief	This function parses a test string and outputs the
 * 		 corresponding model enum.
 *
 * This function parses the given string and generates the corresponding
 * enum. This fucntion is technically allowed to return the eParModel::NoModel
 * value but it is recomended to throw an exception in that case.
 *
 * \note	This function is implemented in the
 * 		 "tree/yggdrasil_treeBuilder_Enums.cpp" file.
 *
 * \param  modelString		This is the model string that should be parsed.
 *
 * \throw	This fucntion throws if the model is not recognized.
 */
extern
eParModel
yggdrasil_parse_parModel(
	const ::std::string&	modelString);




/*
 * =======================
 * 	G O F   T E S T E R
 */

/**
 * \brief	This enum is for naming the GOF test.
 * \enum 	eGOFTester
 *
 * This enum is like the eParModel enum a numerical constant
 * that can be used to identify the implementation for the
 * GOF tester.
 */
enum class eGOFTester : ::yggdrasil::Size_t
{
	NoTester,	//!< This is for indicating that no tester is selected.
	Chi2Tester,	//!< This is for the Chi2 implementation.
}; //End enum(eGOFTester)


/**
 * \brief	This function tests if not the NoTester was selcted.
 *
 * As with the yggdrasil_isNoModel() this function can not detect
 * if themodel realy exists or not. It can only detect, if the
 * NoTester was selected.
 *
 * \param  gofTest	The value which to test.
 */
inline
constexpr
bool
yggdrasil_isNoGOFTester(
	const eGOFTester 	gofTest)
{
	return (gofTest == eGOFTester::NoTester) ? true : false;
}; //End: is no gof tester


/**
 * \brief	This function parses a string that describes the GOF tester.
 * 		 It should then return the corresponding enum value.
 *
 * This function parses the given string and generates the corresponding
 * enum. This fucntion is technically allowed to return the eGOFTester::NoTester
 * value but it is recomended to throw an exception in that case.
 *
 * \note	This function is implemented in the
 * 		 "tree/yggdrasil_treeBuilder_Enums.cpp" file.
 *
 * \param  gofString		This is the gof string that should be parsed.
 *
 * \throw	This fucntion throws if the model is not recognized.
 */
extern
eGOFTester
yggdrasil_parse_GOFTester(
	const std::string&	gofString);




/*
 * =====================
 * 	I N D E P E N D E N C E    T E S T
 */

/**
 * \brief	This enum is for naming the independence test.
 * \enum	eIndepTest
 *
 * As with the eGOFTester this enum is used for selecting
 * the independence test.
 */
enum class eIndepTester : ::yggdrasil::Size_t
{
	NoTester,	//!< This is the tag for no tester
	Chi2Tester,	//!< This is the tag for the chi2 implementation.
}; //End enum(eIndepTester)


/**
 * \brief	This function tests if not the NoTester was selcted.
 *
 * As with the yggdrasil_isNoModel() this function can not detect
 * if themodel realy exists or not. It can only detect, if the
 * NoTester was selected.
 *
 * \param  indepTest	The value which to test.
 */
inline
constexpr
bool
yggdrasil_isNoIndepTester(
	const eIndepTester 	indepTest)
{
	return (indepTest == eIndepTester::NoTester) ? true : false;
}; //End: is no gof tester


/**
 * \brief	This function parses a string that describes the Indep tester.
 * 		 It should then return the corresponding enum value.
 *
 * This function parses the given string and generates the corresponding
 * enum. This fucntion is technically allowed to return the eIndepTester::NoTester
 * value but it is recomended to throw an exception in that case.
 *
 * \note	This function is implemented in the
 * 		 "tree/yggdrasil_treeBuilder_Enums.cpp" file.
 *
 * \param  indepString		This is the independence string that should be parsed.
 *
 * \throw	This fucntion throws if the model is not recognized.
 */
extern
eIndepTester
yggdrasil_parse_IndepTester(
	const std::string&	indepString);







YGGDRASIL_NS_END(yggdrasil)



