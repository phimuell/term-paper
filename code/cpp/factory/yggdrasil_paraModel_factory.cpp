/*
 * \brief	COntains the implementation of the factory of the parameteric model
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <factory/yggdrasil_treeBuilder_Enums.hpp>
#include <factory/yggdrasil_treeBuilder.hpp>

//Interface of the model
#include <interfaces/yggdrasil_interface_parametricModel.hpp>

/*
 * Including of the actual implementations
 */
#include <para_models/yggdrasil_constModel.hpp>
#include <para_models/yggdrasil_linModel.hpp>


//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_parametricModel_i::ParametricModel_ptr
yggdrasil_parametricModel_i::FACTORY(
	const eParModel 			parModel,
	const yggdrasil_treeBuilder_t&		builder)
{
	using ParametricModel_ptr = yggdrasil_parametricModel_i::ParametricModel_ptr;

	switch(parModel)
	{

	  case eParModel::ConstModel:
		return ParametricModel_ptr(new yggdrasil_constantModel_t(0));

	  case eParModel::LinModel:
		return ParametricModel_ptr(new yggdrasil_linearModel_t(0));

	  case eParModel::NoModel:
		throw YGGDRASIL_EXCEPT_InvArg("The no model was passed to the factory.");

	  default:
	  	throw YGGDRASIL_EXCEPT_RUNTIME("A unkown model was passed to the model factory.");
	}; //End switch


	throw YGGDRASIL_EXCEPT_RUNTIME("Reached an unreachable part.");
	return ParametricModel_ptr(nullptr);

	(void)builder;
}; //End paramateric model factory





YGGDRASIL_NS_END(yggdrasil)
