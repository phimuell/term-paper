/*
 * \brief	Contains the implementation of the factory for the GOF tester.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <factory/yggdrasil_treeBuilder_Enums.hpp>
#include <factory/yggdrasil_treeBuilder.hpp>

//Including of the interfaces
#include <interfaces/yggdrasil_interface_statisticalTests.hpp>
#include <interfaces/yggdrasil_interface_GOFTest.hpp>

/*
 * Including of the actual implementations
 */
#include <stat_tests/yggdrasil_GOFTest_chi2.hpp>


//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_GOFTest_i::GOFTest_ptr
yggdrasil_GOFTest_i::FACTORY(
	const eGOFTester 			gofModel,
	const yggdrasil_treeBuilder_t&		builder)
{
	using GOFTest_ptr = yggdrasil_GOFTest_i::GOFTest_ptr;

	switch(gofModel)
	{
	  case eGOFTester::Chi2Tester:
	  	return GOFTest_ptr(new yggdrasil_Chi2GOFTest_t(builder.getGOFSigLevel()));

	  case eGOFTester::NoTester:
		throw YGGDRASIL_EXCEPT_InvArg("The noTester was passed to the gof factory");

	  default:
	  	throw YGGDRASIL_EXCEPT_RUNTIME("An unkown gof tester was passed to the gof factory.");
	}; //End switch


	throw YGGDRASIL_EXCEPT_RUNTIME("Reached an unreachable part.");
	return GOFTest_ptr(nullptr);
};





YGGDRASIL_NS_END(yggdrasil)
