/**
 * \brief	This function implements a builder concept for the syntactic sugar.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>
#include <interfaces/yggdrasil_interface_GOFTest.hpp>
#include <interfaces/yggdrasil_interface_IndepTest.hpp>
#include <interfaces/yggdrasil_interface_splitStrategy.hpp>

#include <factory/yggdrasil_treeBuilder_Enums.hpp>
#include <factory/yggdrasil_treeBuilder.hpp>


//INcluide std
#include <climits>
#include <limits>
#include <functional>
#include <string>
#include <sstream>

YGGDRASIL_NS_BEGIN(yggdrasil)


std::string
yggdrasil_treeBuilder_t::print()
 const
{
	std::stringstream s;

	/*
	 * Parametric model
	 */
	switch(this->m_parModel)
	{
	  case eParModel::LinModel:
	  	s << "ParModel: Linear\n";
	  break;

	  case eParModel::ConstModel:
	  	s << "ParModel: Constant\n";
	  break;

	  default:
	  	s << "ParModel: Unkonwn\n";
	  break;
	}; //End switch: par model

	/*
	 * Independence test
	 */
	s << "Independence Test: ";
	switch(this->m_indepTester)
	{
	  case eIndepTester::Chi2Tester:
	  	s << "Chi2;  \\alpha = " << this->m_indepSigLevel << "\n";
	  break;

	  default:
	  	s << "Unknown\n";
	  break;
	}; //ENd switch: indep tester

	/*
	 * Independence test
	 */
	s << "GOF Test: ";
	switch(this->m_gofTester)
	{
	  case eGOFTester::Chi2Tester:
	  	s << "Chi2;  \\alpha = " << this->m_gofSigLevel << "\n";
	  break;

	  default:
	  	s << "Unknown\n";
	  break;
	}; //ENd switch: indep tester

	/*
	 * Splitter:
	 */
	s << "Splitting: ";
	if(this->isMedianSplitter() == true)
	{
		s << "Median/Score";
	}
	else
	{
		s << "Const/Size";
	};


	/*
	 * Return the string
	 */
	return s.str();
}; //End print



YGGDRASIL_NS_END(yggdrasil)



