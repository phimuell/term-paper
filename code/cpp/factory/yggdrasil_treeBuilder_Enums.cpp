/**
 * \brief	THis file contains the enums that are used to select the
 * 		 correct implementation.
 *
 * The parsing fucntion in this file must also be updated if new models where added.
 *
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <factory/yggdrasil_treeBuilder_Enums.hpp>

//INcluide std
#include <climits>
#include <limits>
#include <functional>
#include <string>

//Including boos
#include <boost/algorithm/string/case_conv.hpp>

YGGDRASIL_NS_BEGIN(yggdrasil)


/*
 * ======================
 * 	P A R    M O D E L
 */

eParModel
yggdrasil_parse_parModel(
	const ::std::string&	modelString_)
{
	//COnvert the model string to lower cases
	const std::string modelString = ::boost::algorithm::to_lower_copy(modelString_);

#define LIN(what) if(modelString == what) { return eParModel::LinModel;}
	LIN("linearmodle");
	LIN("lin");
	LIN("linear");
	LIN("linmod");
	LIN("linear model");
#undef LIN

#define CONST(what) if(modelString == what) { return eParModel::ConstModel;}
	CONST("const");
	CONST("const model");
	CONST("constant");
#undef CONST

	/*
	 * If we are hewre, the model was not recognized
	 */
	throw YGGDRASIL_EXCEPT_InvArg("The model string \"" + modelString_ + "\" was not recognized.");
	return eParModel::NoModel;
}; //ENd: parse model string


/*
 * =======================
 * 	G O F   T E S T E R
 */

/**
 * \brief	This function parses a string that describes the GOF tester.
 * 		 It should then return the corresponding enum value.
 *
 * This function parses the given string and generates the corresponding
 * enum. This fucntion is technically allowed to return the eGOFTester::NoTester
 * value but it is recomended to throw an exception in that case.
 *
 * \note	This function is implemented in the
 * 		 "tree/yggdrasil_treeBuilder_Enums.cpp" file.
 *
 * \param  gofString		This is the gof string that should be parsed.
 *
 * \throw	This fucntion throws if the model is not recognized.
 */
eGOFTester
yggdrasil_parse_GOFTester(
	const std::string&	gofString_)
{
	//Convert the string into lower cases
	const std::string gofString = ::boost::algorithm::to_lower_copy(gofString_);
#define GOF(what) if(gofString == what) {return eGOFTester::Chi2Tester;}
	GOF("chi2");
	GOF("org");
	GOF("chis");
	GOF("chisquare");
#undef GOF


	/*
	 * If we are here then the string was not recognized.
	 */
	throw YGGDRASIL_EXCEPT_InvArg("The GOF string \"" + gofString_ + "\" was not recognized.");

	return eGOFTester::Chi2Tester;
}; //End parse gof




/*
 * =====================
 * 	I N D E P E N D E N C E    T E S T
 */

/**
 * \brief	This function parses a string that describes the Indep tester.
 * 		 It should then return the corresponding enum value.
 *
 * This function parses the given string and generates the corresponding
 * enum. This fucntion is technically allowed to return the eIndepTester::NoTester
 * value but it is recomended to throw an exception in that case.
 *
 * \note	This function is implemented in the
 * 		 "tree/yggdrasil_treeBuilder_Enums.cpp" file.
 *
 * \param  gofString		This is the gof string that should be parsed.
 *
 * \throw	This fucntion throws if the model is not recognized.
 */
eIndepTester
yggdrasil_parse_IndepTester(
	const std::string&	indepString_)
{
	//Convert to lower case
	const std::string indepString = ::boost::algorithm::to_lower_copy(indepString_);

#define INDEP(what) if(indepString == what) {return eIndepTester::Chi2Tester;}
	INDEP("chi2");
	INDEP("org");
	INDEP("chis");
	INDEP("chisquare");
#undef INDEP

	throw YGGDRASIL_EXCEPT_InvArg("THe indep string \"" + indepString_ + "\" was not recognized.");

	return eIndepTester::NoTester;
}; //End: parse indep







YGGDRASIL_NS_END(yggdrasil)



