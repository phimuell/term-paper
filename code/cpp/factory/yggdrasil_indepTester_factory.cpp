/*
 * \brief	Contains the implementation of the factory for the indep tester.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <factory/yggdrasil_treeBuilder_Enums.hpp>
#include <factory/yggdrasil_treeBuilder.hpp>

//Including of the interfaces
#include <interfaces/yggdrasil_interface_statisticalTests.hpp>
#include <interfaces/yggdrasil_interface_IndepTest.hpp>

/*
 * Including of the actual implementations
 */
#include <stat_tests/yggdrasil_IndepTest_chi2.hpp>


//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)


yggdrasil_IndepTest_i::IndepTest_ptr
yggdrasil_IndepTest_i::FACTORY(
	const eIndepTester 			indepTester,
	const yggdrasil_treeBuilder_t&		builder)
{
	using IndepTest_ptr = yggdrasil_IndepTest_i::IndepTest_ptr;

	switch(indepTester)
	{
	  case eIndepTester::Chi2Tester:
	  	return IndepTest_ptr(new yggdrasil_Chi2IndepTest_t(builder.getIndepSigLevel()));

	  case eIndepTester::NoTester:
		throw YGGDRASIL_EXCEPT_InvArg("The noTester was passed to the indep factory");

	  default:
	  	throw YGGDRASIL_EXCEPT_RUNTIME("An unkown gof tester was passed to the indep factory.");
	}; //End switch


	throw YGGDRASIL_EXCEPT_RUNTIME("Reached an unreachable part.");
	return IndepTest_ptr(nullptr);
};





YGGDRASIL_NS_END(yggdrasil)
