#pragma once
/**
 * \brief	This function implements a builder concept for the syntactic sugar.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>

#include <interfaces/yggdrasil_interface_parametricModel.hpp>
#include <interfaces/yggdrasil_interface_GOFTest.hpp>
#include <interfaces/yggdrasil_interface_IndepTest.hpp>
#include <interfaces/yggdrasil_interface_splitStrategy.hpp>

#include <factory/yggdrasil_treeBuilder_Enums.hpp>


//INcluide std
#include <climits>
#include <limits>
#include <functional>
#include <string>

YGGDRASIL_NS_BEGIN(yggdrasil)


/**
 * \class 	yggdrasil_treeBuilder_t
 * \brief	This class provides a builder concept.
 *
 * The builder concept allows the user to reduce the number
 * of arguments when generating a tree.
 *
 * Currently the semantic of this is fully focused on the
 * original scope.
 */
class yggdrasil_treeBuilder_t
{
	/*
	 * =================
	 * Typedefs
	 */
public:
	using ParametricModel_ptr 	= yggdrasil_parametricModel_i::ParametricModel_ptr;
	using GOFTest_ptr 		= yggdrasil_GOFTest_i::GOFTest_ptr;
	using IndepTest_ptr		= yggdrasil_IndepTest_i::IndepTest_ptr;
	using SplitStragety_ptr 	= ::std::unique_ptr<yggdrasil_splitFinder_i>;



	/*
	 * ===================
	 * Constructors
	 */
public:
	/**
	 * \brief	This si the buiding constructor
	 *
	 * The building constructor takes the parameteric
	 * model that is used.
	 *
	 * This constructor sets the other vaiable accordingly.
	 * The ginificance levels are 0.01.
	 *
	 * The splitter is set to the median.
	 *
	 * \param  parModel	The parameteric model that should be used.
	 */
	yggdrasil_treeBuilder_t(
		const eParModel 	parModel)
	 :
	  m_parModel(parModel),
	  m_gofTester(eGOFTester::Chi2Tester),
	  m_gofSigLevel(0.01),
	  m_indepTester(eIndepTester::Chi2Tester),
	  m_indepSigLevel(0.01),
	  m_isMedianSpliter(true)
	{
		if(yggdrasil_isNoModel(parModel) == true)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The no model was used.");
		};
	}; //End: building constructor


	/**
	 * \brief	The default constructor.
	 *
	 * THis will construct the builder as a NoModel.
	 * Notice that this is ilegal and will result
	 * in a problem when using it
	 */
	yggdrasil_treeBuilder_t()
	 :
	  m_parModel(eParModel::NoModel),
	  m_gofTester(eGOFTester::Chi2Tester),
	  m_gofSigLevel(0.05),
	  m_indepTester(eIndepTester::Chi2Tester),
	  m_indepSigLevel(0.05),
	  m_isMedianSpliter(true)
	{
	}; //End default constructor.


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_treeBuilder_t(
		const yggdrasil_treeBuilder_t&) = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_treeBuilder_t(
		yggdrasil_treeBuilder_t&&) = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_treeBuilder_t&
	operator= (
		const yggdrasil_treeBuilder_t&) = default;


	/**
	 * \brief 	Move assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_treeBuilder_t&
	operator= (
		yggdrasil_treeBuilder_t&&) = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_treeBuilder_t() = default;


	/*
	 * =================
	 * General fucntion
	 *
	 * Thei are the general fucntion that takes an enum
	 * and the optional argument.
	 */
public:
	/**
	 * \brief	Set the parametzeric model.
	 *
	 * This function only takes the model
	 * enumeration.
	 *
	 * \param  parModel	The parameteric model to use.
	 *
	 * \throw 	If the model is considered invalid.
	 */
	yggdrasil_treeBuilder_t&
	setParModel(
		const eParModel&	parModel)
	{
		if(yggdrasil_isNoModel(parModel) == true)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The given model enum is considered ilegal.");
		};

		m_parModel = parModel;

		return *this;
	}; //ENd: set parameteric model.


	/**
	 * \brief	This function allows to set the gof tester.
	 *
	 * This function takes as optional argument the significance level.
	 *
	 * \param  gofTester	This is the enum for the gof tester.
	 * \param  sigLevel	This is the significance level.
	 */
	yggdrasil_treeBuilder_t&
	setGOFTester(
		const eGOFTester 	gofTester,
		const Numeric_t 	sigLevel)
	{
		if(yggdrasil_isNoGOFTester(gofTester) == true)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The gof tester is considered illegal.");
		};
		if(sigLevel < 0.0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The significanbce level for the GOF tester is negative.");
		};
		if(isValidFloat(sigLevel) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The significance level is invalid.");
		};

		//Set the values
		m_gofTester   = gofTester;
		m_gofSigLevel = sigLevel;

		return *this;
	}; //end: set gof tester


	/**
	 * \breif	This fucntion only sets the gof tester.
	 *
	 * The significance level of the gof test is not affected
	 * by this call.
	 *
	 * \param  gofTester	The GOF tester to use
	 */
	yggdrasil_treeBuilder_t&
	setGOFTester(
		const eGOFTester 	gofTester)
	{
		return this->setGOFTester(gofTester, m_gofSigLevel);
	};


	/**
	 * \brief	This function sets the independence test tester.
	 *
	 * This fucntion takes an independence test enum and a
	 * significance level.
	 *
	 * \param  indepTester		The tester used for the independence test.
	 * \param  sigLevel		The significance level
	 */
	yggdrasil_treeBuilder_t&
	setIndepTester(
		const eIndepTester 	indepTerster,
		const Numeric_t 	sigLevel)
	{
		if(yggdrasil_isNoIndepTester(indepTerster) == true)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The enum for the independence tester is considered illegal.");
		};
		if(sigLevel < 0.0)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The significance level is considered negative.");
		};
		if(isValidFloat(sigLevel) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The significance level is invalid.");
		};

		//Set the values
		m_indepTester   = indepTerster;
		m_indepSigLevel = sigLevel;

		return *this;
	}; //End: set indep tester


	/**
	 * \brief	This function sets only the idependence test.
	 *
	 * The significance level is not affected.
	 *
	 * \param  indepTest	The new independence test to use.
	 */
	yggdrasil_treeBuilder_t&
	setIndepTester(
		const eIndepTester	indepTester)
	{
		return this->setIndepTester(indepTester, this->m_indepSigLevel);
	};



	/*
	 * ==================
	 * String fucntions
	 *
	 * These are like the one that takes enums,
	 * but instead uses strings.
	 */
public:
	/**
	 * \brief	Set the parameteric model using a string.
	 *
	 * The string is tried to be interpreted and to be converted into an
	 * eParModel enum.
	 *
	 * \param  parString	The string that is parsed.
	 */
	yggdrasil_treeBuilder_t&
	setParModel(
		const std::string&	parString)
	{
		return this->setParModel(yggdrasil_parse_parModel(parString));
	};


	/**
	 * \brief	Set the gof tester from a string.
	 *
	 * This function uses the string to parse the gof model.
	 *
	 * \param  gofString	The sting that is parsed.
	 */
	yggdrasil_treeBuilder_t&
	setGOFTester(
		const std::string&	gofString)
	{
		return this->setGOFTester(yggdrasil_parse_GOFTester(gofString));
	};


	/**
	 * \brief	Set the independence test from a string.
	 *
	 * This fucntion does not chance the significance level.
	 *
	 * \param  indepString		The string to parse.
	 */
	yggdrasil_treeBuilder_t&
	setIndepTester(
		const std::string&	indepString)
	{
		return this->setIndepTester(yggdrasil_parse_IndepTester(indepString));
	};





	/*
	 * =======================
	 * Fast select function
	 *
	 * Since at least at the begining, we only have a very limited
	 * set of implementations, we provide some
	 * selecting functions
	 */
public:
	/**
	 * \brief	This fucntion sets the model to the constant model.
	 */
	yggdrasil_treeBuilder_t&
	useConstantModel()
	{
		return this->setParModel(eParModel::ConstModel);
	};


	/**
	 * \brief	Thsi fucntion sets the linear model.
	 */
	yggdrasil_treeBuilder_t&
	useLinearModel()
	{
		return this->setParModel(eParModel::LinModel);
	};


	/**
	 * \brief	Set the chi2 implementation for the gof tester.
	 *
	 * This function does not affect the significance level.
	 */
	yggdrasil_treeBuilder_t&
	useChi2GOF()
	{
		return this->setGOFTester(eGOFTester::Chi2Tester);
	};


	/**
	 * \brief	Set the chi2 implementation for the independence test.
	 *
	 * This function does not affects the significance level.
	 */
	yggdrasil_treeBuilder_t&
	useChi2Indep()
	{
		return this->setIndepTester(eIndepTester::Chi2Tester);
	};




	/*
	 * ====================
	 * Level functions
	 *
	 * These function onyl sets the significance levels.
	 */
public:
	/**
	 * \brief	Thsi function sets only the significance level of the gof test.
	 *
	 * \param  sigLevel	The new significance level.
	 */
	yggdrasil_treeBuilder_t&
	setGOFLevel(
		const Numeric_t& 	sigLevel)
	{
		return this->setGOFTester(this->m_gofTester, sigLevel);
	}; //End: set the sig level of the gof


	/**
	 * \brief	Thsi function sets only the significance level of the independence test.
	 *
	 * \param  sigLevel	This is the significance level.
	 */
	yggdrasil_treeBuilder_t&
	setIndepLevel(
		const Numeric_t&	sigLevel)
	{
		return this->setIndepTester(this->m_indepTester, sigLevel);
	}; //End set sig level of independence test.


	/*
	 * =====================
	 * Splitter stagety
	 */
public:
	/**
	 * \brief	Set the split stragety to the median splitter.
	 */
	yggdrasil_treeBuilder_t&
	useMedianSplitter()
	 noexcept
	{
		m_isMedianSpliter = true;

		return *this;
	};


	/**
	 * \brief	Set the split stragety to the median splitter.
	 *
	 * This is just an alias of useMedianSplitter().
	 */
	yggdrasil_treeBuilder_t&
	useScoreBasedSplitter()
	 noexcept
	{
		return this->useMedianSplitter();
	};


	/**
	 * \brief	Set the split stargety to the size splitter.
	 *
	 * The size spliter cuts the domain in half.
	 */
	yggdrasil_treeBuilder_t&
	useSizeSplitter()
	 noexcept
	{
		m_isMedianSpliter = false;

		return *this;
	};




	/*
	 * ==================
	 * Getters
	 *
	 * There are only some of the getters
	 */
public:
	/**
	 * \brief	Get the significance level of the GOF test.
	 */
	Numeric_t
	getGOFSigLevel()
	 const
	 noexcept
	{
		return m_gofSigLevel;
	};


	/**
	 * \brief	Return the significance level of the independence test.
	 */
	Numeric_t
	getIndepSigLevel()
	 const
	 noexcept
	{
		return m_indepSigLevel;
	};


	/**
	 * \brief	Returns the parameteric model of *this.
	 *
	 * This returns an enum of the parameteric model
	 */
	eParModel
	getParModel()
	 const
	 noexcept
	{
		return m_parModel;
	};


	/**
	 * \brief	Returns the GOF tester enum of *this.
	 */
	eGOFTester
	getGOFTester()
	 const
	 noexcept
	{
		return m_gofTester;
	};


	/**
	 * \brief	Returns the independence tester enum of *this.
	 */
	eIndepTester
	getIndepTerser()
	 const
	 noexcept
	{
		return m_indepTester;
	};




	/*
	 * ==================
	 * Status functions
	 *
	 * This function only returns a bool value.
	 * They are like more primitive versions of getters.
	 */
public:
	/**
	 * \brief	Returns ture if *this is a linear model.
	 */
	bool
	isLinearModel()
	 const
	 noexcept
	{
		return (m_parModel == eParModel::LinModel) ? true : false;
	};


	/**
	 * \brief	Return true if this is a constant model.
	 */
	bool
	isConstantModel()
	 const
	 noexcept
	{
		return (m_parModel == eParModel::ConstModel) ? true : false;
	};


	/**
	 * \brief	Returns true if a median spliter should be constructed.
	 *
	 */
	bool
	isMedianSplitter()
	 const
	 noexcept
	{
		return m_isMedianSpliter;
	};


	/**
	 * \brief	Returns true if *this is a size splitter.
	 *
	 * A size splitter cut the domain at exactly the half.
	 */
	bool
	isSizeSplitter()
	 const
	 noexcept
	{
		return (m_isMedianSpliter == false) ? true : false;
	};


	/**
	 * \brief	Returns true if *this is not the NoModel.
	 *
	 * This is like a test for the validity of *this.
	 */
	bool
	hasModel()
	 const
	 noexcept
	{
		return (m_parModel != eParModel::NoModel) ? true : false;
	};


	/**
	 * \brief	Thsi function returns true if the gof test is not the NoTest.
	 *
	 * A false indicate that the test is not set.
	 */
	bool
	hasGOFTester()
	 const
	 noexcept
	{
		return (m_gofTester != eGOFTester::NoTester) ? true : false;
	};


	/**
	 * \brief	Returns true if *this has a set independence tester.
	 *
	 * This tests if the independence test is not the NoTester.
	 */
	bool
	hasIndepTester()
	 const
	 noexcept
	{
		return (m_indepTester != eIndepTester::NoTester) ? true : false;
	};





	/*
	 * ==================
	 * Builder
	 */
public:
	/**
	 * \brief	This function returns a parameteric model.
	 *
	 * The parameteric model is described by *this.
	 *
	 * \note	The spelling error is intended.
	 */
	ParametricModel_ptr
	creatParModel()
	 const
	{
		if(this->hasModel() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("The builder does not have a valid model.");
		};

		return yggdrasil_parametricModel_i::FACTORY(m_parModel, *this);
	};


	/**
	 * \brief	This function builds the GOF tester.
	 *
	 */
	GOFTest_ptr
	creatGOFTester()
	 const
	{
		if(this->hasGOFTester() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("No wallid GOF tester was set.");
		};

		return yggdrasil_GOFTest_i::FACTORY(m_gofTester, *this);
	};


	/**
	 * \brief	This fucntion builds the independence test.
	 */
	IndepTest_ptr
	creatIndepTester()
	 const
	{
		if(this->hasIndepTester() == false)
		{
			throw YGGDRASIL_EXCEPT_LOGIC("No valid independence tester was set.");
		};

		return yggdrasil_IndepTest_i::FACTORY(m_indepTester, *this);
	};


	/**
	 * \brief	Returns teh split stragety
	 */
	SplitStragety_ptr
	creatSplitStragety()
	 const
	{
		return yggdrasil_splitFinder_i::FACTORY(*this);
	};


	/**
	 * \brief	This is a simple printer.
	 */
	std::string
	print()
	 const;






	/*
	 * ==============
	 * Private Memmbers
	 */
private:
	eParModel 	m_parModel;	//!< Which parametric model.

	eGOFTester 	m_gofTester;	//!< This is the GOF tester.
	Numeric_t 	m_gofSigLevel;	//!< The significance level of the gof tester.

	eIndepTester 	m_indepTester;	//!< This is the Indpeendence tester.
	Numeric_t 	m_indepSigLevel;//!< This is the sigificant level of the independence test.

	bool 		m_isMedianSpliter;//!< Indicates if the median splitter is selected.
}; //End class: yggdrasil_treeBuilder_t

YGGDRASIL_NS_END(yggdrasil)



