/**
 *
 * Contains the implementation.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <util/yggdrasil_splitCommand.hpp>
#include <util/yggdrasil_util.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <split_scheme/yggdrasil_scoreBasedSplitting.hpp>

//INcluide std
#include <iterator>
#include <memory>
#include <algorithm>

YGGDRASIL_NS_BEGIN(yggdrasil)


std::unique_ptr<yggdrasil_splitFinder_i>
yggdrasil_scoreSplit_t::creatOffspring()
 const
{
	return std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil_scoreSplit_t());
};



yggdrasil_scoreSplit_t::SplitCommand_t
yggdrasil_scoreSplit_t::determineSplitLocation(
	const HyperCube_t& 		domain,
	const SplitSequence_t& 		splitSeq,
	IndexArray_It 			startRange,
	IndexArray_It 			endRange,
	const DimensionArray_t&		dimArray)
 const
{
	//We requiere the the domain is valid
	if(domain.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The domain presented to the splitter is invalid.");
	};

	//Test if the split is valid
	if(splitSeq.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The split sequence is not valid.");
	};

	//By definition an empty split is valid, but useless in this situation
	if(splitSeq.isEmpty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The split is empty.");
	};

	if(!(startRange <= endRange))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The pointer into the interval are bad.");
	};

	if(splitSeq.getHeadSplit() != dimArray.getAssociatedDim())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The head dimension of the split " + std::to_string(splitSeq.getHeadSplit()) + " and the associated dimension of teh array " + std::to_string(dimArray.getAssociatedDim()) + ", are not the same.");
	};

	if(domain.at(splitSeq.getHeadSplit()).getLength() < Constants::YG_SMALLEST_LENGTH)
	{
		throw YGGDRASIL_EXCEPT_InvArg("It was tried to split a doimain that is below the threshhold.");
	};

	SplitCommand_t splitComm = SplitCommand_t::CONSTRUCT_INVALID_SINGLE_SPLIT();

	//When the two iterators are the same, then tehre are no samples in the domain
	if(startRange == endRange)
	{
		//Zero samples
		splitComm = this->priv_sizeBasedSplit(domain, splitSeq, startRange, endRange, dimArray);
	}
	else
	{
		//We have samples
		splitComm = this->priv_scoreBasedSplit(domain, splitSeq, startRange, endRange, dimArray);
	};

	//test if it worked
	if(splitComm.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("Was not able to determine the split.");
	};
	if(splitComm.getSplitDim() != splitSeq.getHeadSplit())
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The dimensions are not the same.");
	};

	//Test if the split point is inside the domain

	//get the split dimension
	const auto splitDim = splitSeq.getHeadSplit();
	yggdrasil_assert(dimArray.getAssociatedDim() == splitDim);

	//Get the interval that describes it
	const IntervalBound_t splitInterval = domain.at(splitDim);
	yggdrasil_assert(splitInterval.isValid());

	//Now transform the split location into the space of the parent domeins
	const Numeric_t transSplitPos = splitInterval.mapFromUnit(splitComm.getSplitPos());

	//Test if the position is inside the domain
	if(domain.isInside(splitDim, transSplitPos) == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The (transformed) split position " + std::to_string(transSplitPos) + ", the estimated one was " + std::to_string(splitComm.getSplitPos()) + " was ouside the interval of dimension D = " + std::to_string(splitComm.getSplitDim()) + ", the interval was " + splitInterval.print());
	}; //End test if inside

	//Return the split position
	return splitComm;
}; //End: find the split location.



yggdrasil_scoreSplit_t::SplitCommand_t
yggdrasil_scoreSplit_t::priv_sizeBasedSplit(
	const HyperCube_t& 		domain,
	const SplitSequence_t& 		splitSeq,
	IndexArray_It 			startRange,
	IndexArray_It 			endRange,
	const DimensionArray_t&		dimArray)
 const
{
	//Get the 1D domain of the dimension to split
	const HyperCube_t::IntervalBound_t dimInterval = domain.at(splitSeq.getHeadSplit());
	yggdrasil_assert(dimInterval.isValid());

	//Now we compose the middle value
	const Numeric_t middlePoint = 0.5;
	const Int_t     splitDim    = splitSeq.getHeadSplit();

	//Return the split
	return SplitCommand_t(middlePoint, splitDim);
	(void)startRange;
	(void)endRange;
	(void)dimArray;
}; //End: sizeBased



yggdrasil_scoreSplit_t::SplitCommand_t
yggdrasil_scoreSplit_t::priv_scoreBasedSplit(
	const HyperCube_t& 		domain,
	const SplitSequence_t& 		splitSeq,
	IndexArray_It 			startRange,
	IndexArray_It 			endRange,
	const DimensionArray_t&		dimArray)
 const
{
	//Determine the number of samples
	const Size_t nSamples = ::std::distance(startRange, endRange);

	//Make a sanity check
	yggdrasil_assert(nSamples > 0);

	//Shortcut for one sample
	if(nSamples == 1)
	{
		const Int_t     splitDim    = splitSeq.getHeadSplit();
		const Numeric_t middlePoint = dimArray.at(*startRange);

		return SplitCommand_t(middlePoint, splitDim);
	}; //End if: one sample

	if(nSamples == 2)
	{
		const Int_t     splitDim     = splitSeq.getHeadSplit();
		const Numeric_t firstSampel  = dimArray.at(*startRange);
		const Numeric_t secondSampel = dimArray.at(*(startRange + 1));

		const Numeric_t middlePoint  = (firstSampel + secondSampel) * 0.5;

		return SplitCommand_t(middlePoint, splitDim);
	}; //End: two samples

	//Regardless if we have of cases or not, we have to
	//find the middle sample
	//The substraction is needed, to accomodate the start of the indexing.
	const Size_t middleSample = nSamples / 2 - (nSamples % 2 == 0 ? 1 : 0);

	//Find the location in the array that will contain the middle sample
	//This will reorder the index array.
	IndexArray_It middleIT = startRange + middleSample;
	yggdrasil_assert(startRange < middleIT);
	yggdrasil_assert(middleIT   < endRange);
	yggdrasil_argNthElement<typename IndexArray_t::value_type>(
			startRange, middleIT, endRange, dimArray);

	//In the case of the odd one we are now complete
	if(nSamples % 2 == 1)
	{
		const Int_t     splitDim    = splitSeq.getHeadSplit();
		const Numeric_t middlePoint = dimArray.at(*middleIT);

		//The splitting positioon has to be in the unit inteval
		yggdrasil_assert(0.0 <= middlePoint);
		yggdrasil_assert(middlePoint < 1.0);

		return SplitCommand_t(middlePoint, splitDim);
	}; //End if: odd numbers of samples

	/*
	 * Since nth_element reorder the array such that it paritionate.
	 * So we now have to find teh minimum valu of that position to get the
	 * second middle element.
	 */
	IndexArray_It secondMiddleIT = ::std::min_element(
		middleIT + 1,		//The begin of the new range, we do not need middleIT
		endRange,		//The end of the range, it is the real end
		[&dimArray](const Size_t lhs, const Size_t rhs) -> bool
		  {
#ifndef NDEBUG
			return (dimArray[lhs] < dimArray[rhs]) ? true : false;
#else
			return (dimArray.at(lhs) < dimArray.at(rhs)) ? true : false;
#endif
		  }
		);

	//Now we can determine the split
	const Numeric_t lowerMiddleElem = dimArray.at(*middleIT);
	const Numeric_t upperMiddleElem = dimArray.at(*secondMiddleIT);

	//A samll sanity check
	yggdrasil_assert(lowerMiddleElem <= upperMiddleElem);

	const Int_t     splitDim    = splitSeq.getHeadSplit();
	const Numeric_t middlePoint = (lowerMiddleElem + upperMiddleElem) * 0.5;

	//The splitting positioon has to be in the unit inteval
	yggdrasil_assert(0.0 <= middlePoint);
	yggdrasil_assert(middlePoint < 1.0);

	return SplitCommand_t(middlePoint, splitDim);

	(void)domain;
}; //End: scoreBased

yggdrasil_scoreSplit_t::~yggdrasil_scoreSplit_t()
 noexcept = default;

yggdrasil_scoreSplit_t::yggdrasil_scoreSplit_t()
 noexcept = default;


yggdrasil_scoreSplit_t::yggdrasil_scoreSplit_t(
	const yggdrasil_scoreSplit_t&)
 noexcept = default;


yggdrasil_scoreSplit_t::yggdrasil_scoreSplit_t(
	yggdrasil_scoreSplit_t&&)
 noexcept = default;


yggdrasil_scoreSplit_t&
yggdrasil_scoreSplit_t::operator= (
	const yggdrasil_scoreSplit_t&)
 noexcept = default;

yggdrasil_scoreSplit_t&
yggdrasil_scoreSplit_t::operator= (
	yggdrasil_scoreSplit_t&&)
 noexcept = default;








YGGDRASIL_NS_END(yggdrasil)
