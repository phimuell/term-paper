/**
 *
 * Contains the implementation.
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_intervalBound.hpp>
#include <util/yggdrasil_splitSequence.hpp>
#include <util/yggdrasil_splitCommand.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <split_scheme/yggdrasil_sizeBasedSplitting.hpp>

//INcluide std
#include <iterator>

YGGDRASIL_NS_BEGIN(yggdrasil)


std::unique_ptr<yggdrasil_splitFinder_i>
yggdrasil_sizeSplit_t::creatOffspring()
 const
{
	return std::unique_ptr<yggdrasil_splitFinder_i>(new yggdrasil_sizeSplit_t());
};

yggdrasil_sizeSplit_t::SplitCommand_t
yggdrasil_sizeSplit_t::determineSplitLocation(
	const HyperCube_t& 		domain,
	const SplitSequence_t& 		splitSeq,
	IndexArray_It 			startRange,
	IndexArray_It 			endRange,
	const DimensionArray_t&		dimArray)
 const
{
	//We requiere the the domain is valid
	if(domain.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The domain presented to the splitter is invalid.");
	};

	//Test if the split is valid
	if(splitSeq.isValid() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The split sequence is not valid.");
	};

	//We do not need it but it is good practice
	if(!(startRange <= endRange))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The pointer into the interval are bad.");
	};

	//By definition an empty split is valid, but useless in this situation
	if(splitSeq.isEmpty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The split is empty.");
	};

	if(splitSeq.getHeadSplit() != dimArray.getAssociatedDim())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The head dimension of the split " + std::to_string(splitSeq.getHeadSplit()) + " and the associated dimension of teh array " + std::to_string(dimArray.getAssociatedDim()) + ", are not the same.");
	};

	if(domain.at(splitSeq.getHeadSplit()).getLength() < Constants::YG_SMALLEST_LENGTH)
	{
		throw YGGDRASIL_EXCEPT_InvArg("It was tried to split a doimain that is below the threshhold.");
	};

	//Since we alwqys operate on the rescalled unit intervals, we can return 0.5
	const Numeric_t middlePoint = 0.5;
	const Int_t     splitDim    = splitSeq.getHeadSplit();

	//Return the split
	return SplitCommand_t(middlePoint, splitDim);

	(void)dimArray;
}; //End: find the split location.



yggdrasil_sizeSplit_t::~yggdrasil_sizeSplit_t()
 noexcept = default;

yggdrasil_sizeSplit_t::yggdrasil_sizeSplit_t()
 noexcept = default;


yggdrasil_sizeSplit_t::yggdrasil_sizeSplit_t(
	const yggdrasil_sizeSplit_t&)
 noexcept = default;


yggdrasil_sizeSplit_t::yggdrasil_sizeSplit_t(
	yggdrasil_sizeSplit_t&&)
 noexcept = default;


yggdrasil_sizeSplit_t&
yggdrasil_sizeSplit_t::operator= (
	const yggdrasil_sizeSplit_t&)
 noexcept = default;

yggdrasil_sizeSplit_t&
yggdrasil_sizeSplit_t::operator= (
	yggdrasil_sizeSplit_t&&)
 noexcept = default;








YGGDRASIL_NS_END(yggdrasil)
