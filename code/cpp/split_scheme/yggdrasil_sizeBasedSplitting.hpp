#pragma once
/**
 * \brief	This file implements the size based split stragety.
 *
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitCommand.hpp>
#include <util/yggdrasil_splitSequence.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <interfaces/yggdrasil_interface_splitStrategy.hpp>

//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \class 	yggdrasil_sizeSplit_t
 * \brief	This class splits a domain in half.
 */
class yggdrasil_sizeSplit_t final : public yggdrasil_splitFinder_i
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using IndexArray_t 	= yggdrasil_indexArray_t;	//!< This is the index Array
	using IndexArray_It 	= IndexArray_t::iterator;	//!< This is the iterator on which we work
	using DimensionArray_t 	= yggdrasil_dimensionArray_t;	//!< This is the dimeions array
	using SplitCommand_t 	= yggdrasil_singleSplit_t;	//!< This is the type that stores a split
	using SplitSequence_t 	= yggdrasil_splitSequence_t;	//!< This is the split sequence
	using HyperCube_t 	= yggdrasil_hyperCube_t;	//!< This is the hypercube type
	using IntervalBound_t 	= yggdrasil_intervalBound_t;	//!< This is the type that reprensents an interval


	/*
	 * ===================================
	 * Constructor
	 *
	 * Here are the public constructors.
	 */
public:
	/**
	 * \brief	Default constructor
	 *
	 * Defaulted.
	 * Since there is no state whats so ever,
	 * this shall be the only way to build an instance.
	 */
	yggdrasil_sizeSplit_t()
	 noexcept;


	/**
	 * \brief	Destructor
	 *
	 * It is defaulted.
	 * But it is virtual
	 */
	virtual
	~yggdrasil_sizeSplit_t()
	 noexcept;



	/*
	 * ====================
	 * Interface functions
	 *
	 * Here are the functions that defines the interface
	 */
public:
	/**
	 * \brief	This function calculates the split location.
	 *
	 * It does this by simply splitting the domain in half along the dimension.
	 * It is also the case that the value operates on the rescalled domain.
	 * This means that the split position that is returned is alwys 0.5.
	 *
	 * \param  domain	This is the full cube that has to be splitted.
	 * \param  spliSeq 	This is the split sequence, the head is used as dimension to select.
	 * \param  startRange 	This is an iterator to the first index that belongs to the samples that are splited
	 * \param  endRange 	This is the past the end iterator
	 * \param  dimArray 	This is all the samples
	 *
	 * \return 	This function returns a single split
	 *
	 * \post 	The dinmesion where the split has to be occure, as reported
	 * 		 by SplitCommand_t::getSplitDim has to be the same as the head of the split sequence.
	 */
	virtual
	SplitCommand_t
	determineSplitLocation(
		const HyperCube_t& 		domain,
		const SplitSequence_t& 		splitSeq,
		IndexArray_It 			startRange,
		IndexArray_It 			endRange,
		const DimensionArray_t&		dimArray)
	 const
	 override;

	/**
	 * \brief	Thsi function returns a copy of \c *this.
	 *
	 */
	std::unique_ptr<yggdrasil_splitFinder_i>
	creatOffspring()
	 const
	 override;



	/*
	 * =================================
	 * Protected constructors
	 */
protected:
	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_sizeSplit_t(
		const yggdrasil_sizeSplit_t&)
	 noexcept;


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_sizeSplit_t(
		yggdrasil_sizeSplit_t&&)
	 noexcept;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_sizeSplit_t&
	operator= (
		const yggdrasil_sizeSplit_t&)
	 noexcept;

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_sizeSplit_t&
	operator= (
		yggdrasil_sizeSplit_t&&)
	 noexcept;

}; //End class: yggdrasil_sizeSplit_t


YGGDRASIL_NS_END(yggdrasil)
