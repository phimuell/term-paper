#pragma once
/**
 * \brief	This file implements the score based splitting.
 *
 */


//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_consts.hpp>
#include <core/yggdrasil_int.hpp>
#include <core/yggdrasil_indexArray.hpp>

#include <util/yggdrasil_hyperCube.hpp>
#include <util/yggdrasil_splitCommand.hpp>
#include <util/yggdrasil_splitSequence.hpp>

#include <samples/yggdrasil_sample.hpp>
#include <samples/yggdrasil_sampleCollection.hpp>

#include <interfaces/yggdrasil_interface_splitStrategy.hpp>

//INcluide std
#include <memory>

YGGDRASIL_NS_BEGIN(yggdrasil)

/**
 * \class 	yggdrasil_scoreSplit_t
 * \brief	This class splits a domain in half.
 *
 * FOr that the meadian is used, such that on each side of the soplit
 * the amopunt of samples is the same.
 */
class yggdrasil_scoreSplit_t final : public yggdrasil_splitFinder_i
{
	/*
	 * ============================
	 * Typedef
	 */
public:
	using IndexArray_t 	= yggdrasil_indexArray_t;	//!< This is the index Array
	using IndexArray_It 	= IndexArray_t::iterator;	//!< This is the iterator on which we work
	using DimensionArray_t 	= yggdrasil_dimensionArray_t;	//!< This is the dimeions array
	using SplitCommand_t 	= yggdrasil_singleSplit_t;	//!< This is the type that stores a split
	using SplitSequence_t 	= yggdrasil_splitSequence_t;	//!< This is the split sequence
	using HyperCube_t 	= yggdrasil_hyperCube_t;	//!< This is the hypercube type
	using IntervalBound_t 	= yggdrasil_intervalBound_t;	//!< This is the type that reprensents an interval


	/*
	 * ===================================
	 * Constructor
	 *
	 * Here are the public constructors.
	 */
public:
	/**
	 * \brief	Default constructor
	 *
	 * Defaulted.
	 * Since there is no state whats so ever,
	 * this shall be the only way to build an instance.
	 */
	yggdrasil_scoreSplit_t()
	 noexcept;


	/**
	 * \brief	Destructor
	 *
	 * It is defaulted.
	 * But it is virtual
	 */
	virtual
	~yggdrasil_scoreSplit_t()
	 noexcept;



	/*
	 * ====================
	 * Interface functions
	 *
	 * Here are the functions that defines the interface
	 */
public:
	/**
	 * \brief	This function calculates the split location.
	 *
	 * This function finds the split location hy halving the samples.
	 * In the case of an odd numbers of samples, the sample in the middle is used.
	 * In the case of an even number we search the twio samples in the middle and
	 * compute the middle of them.
	 *
	 * This function will reorder the iondex array.
	 * If no samples are present, \c *this will act as a size splitting.
	 *
	 * The splitting position is ionterpreted in the sense of the unit interval.
	 * Thsi means that the split position is allways in the range [0, 1[.
	 *
	 * It also means that the imput has to be all the time be kept in the
	 * range [0, 1[.
	 *
	 * \param  domain	This is the full cube that has to be splitted, in the original data space.
	 * \param  spliSeq 	This is the split sequence, the head is used as dimension to select.
	 * \param  startRange 	This is an iterator to the first index that belongs to the samples that are splited
	 * \param  endRange 	This is the past the end iterator
	 * \param  dimArray 	This is all the samples
	 *
	 * \return 	This function returns a single split
	 *
	 * \post 	The dinmesion where the split has to be occure, as reported
	 * 		 by SplitCommand_t::getSplitDim has to be the same as the head of the split sequence.
	 *
	 * \note	There is a funny border case, when only one sample is present.
	 * 		 In this case the sample is in the upper domain and the lower (left) is empty.
	 *
	 */
	virtual
	SplitCommand_t
	determineSplitLocation(
		const HyperCube_t& 		domain,
		const SplitSequence_t& 		splitSeq,
		IndexArray_It 			startRange,
		IndexArray_It 			endRange,
		const DimensionArray_t&		dimArray)
	 const
	 override;


	/**
	 * \brief	Thsi function returns a copy of \c *this.
	 *
	 */
	std::unique_ptr<yggdrasil_splitFinder_i>
	creatOffspring()
	 const
	 override;


	/*
	 * ================================
	 * Private Function
	 */
private:
	/**
	 * \brief	This function implements the size based splitting.
	 *
	 * This functionality is provided for the case that a
	 * domain without any samples in it has to be splitted.
	 *
	 * \note	All arguments assumed to be checked.
	 */
	SplitCommand_t
	priv_sizeBasedSplit(
		const HyperCube_t& 		domain,
		const SplitSequence_t& 		splitSeq,
		IndexArray_It 			startRange,
		IndexArray_It 			endRange,
		const DimensionArray_t&		dimArray)
	 const;


	/**
	 * \brief	This function estimates the split based on teh score.
	 *
	 * The score meas that it is tried to halfe the samples.
	 *
	 * In the case of an odd numbers of samples the split location,
	 * is the coordinate of the middle sample, this means it is send to the right domain.
	 *
	 * In the case of an even number of samples, the location is
	 * determined by finding the two samples in the middle and then to
	 * takes the average.
	 *
	 * In the case of just one sample the odd case is taken.
	 *
	 * \note 	This function requieres at least one sample.
	 *
	 * \note	It is assumed that all arguemnts are checked.
	 */
	SplitCommand_t
	priv_scoreBasedSplit(
		const HyperCube_t& 		domain,
		const SplitSequence_t& 		splitSeq,
		IndexArray_It 			startRange,
		IndexArray_It 			endRange,
		const DimensionArray_t&		dimArray)
	 const;


	/*
	 * =================================
	 * Protected constructors
	 */
protected:
	/**
	 * \brief	The copy constructor
	 *
	 * Defaulted
	 */
	yggdrasil_scoreSplit_t(
		const yggdrasil_scoreSplit_t&)
	 noexcept;


	/**
	 * \brief	The move constructor
	 *
	 * Defaulted.
	 */
	yggdrasil_scoreSplit_t(
		yggdrasil_scoreSplit_t&&)
	 noexcept;


	/**
	 * \brief	The copy assignment
	 *
	 * Defaulted
	 */
	yggdrasil_scoreSplit_t&
	operator= (
		const yggdrasil_scoreSplit_t&)
	 noexcept;

	/**
	 * \brief	Move assignment
	 *
	 * Defaulted
	 */
	yggdrasil_scoreSplit_t&
	operator= (
		yggdrasil_scoreSplit_t&&)
	 noexcept;

}; //End class: yggdrasil_scoreSplit_t


YGGDRASIL_NS_END(yggdrasil)
