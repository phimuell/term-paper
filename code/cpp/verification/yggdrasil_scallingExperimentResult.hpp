#pragma once
/**
 * \brief	This class stores the results of several experiments
 * 		 that together forms a scalling experiment.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_statistics.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>

#include <verification/yggdrasil_singleExperimentResult.hpp>

//INcluide std
#include <set>
#include <fstream>



YGGDRASIL_NS_BEGIN(yggdrasil)


class yggdrasil_scallingExperimentResult_t
{
	/*
	 * =================================
	 * Typedefs
	 */
public:
	using SingleExperiment_t	= yggdrasil_singleExperimentResult_t;	//!< THis is a songle experiment
	using DepthMap_t 		= SingleExperiment_t::DepthMap_t;
	using CompositionPack_t		= SingleExperiment_t::CompositionPack_t;
	using TimeBase_t		= SingleExperiment_t::TimeBase_t;

	using ExperimentMap_t 		= ::std::multiset<SingleExperiment_t>;	//!< This is the map for storing all the experimens
											//!<  Muist be a multimap, because we do more of them.
	using iterator 			= ExperimentMap_t::iterator;
	using const_iterator		= ExperimentMap_t::const_iterator;

	/*
	 * =======================
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * Is also the building constructor.
	 */
	yggdrasil_scallingExperimentResult_t()
	 = default;


	/**
	 * \brief	Copy constructor.
	 *
	 * Defaulted.
	 */
	yggdrasil_scallingExperimentResult_t(
		const yggdrasil_scallingExperimentResult_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_scallingExperimentResult_t&
	operator= (
		const yggdrasil_scallingExperimentResult_t&)
	 = default;


	/**
	 * \breif	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_scallingExperimentResult_t(
		yggdrasil_scallingExperimentResult_t&& )
	 = default;


	/**
	 * \brief	Move assignemnt.
	 *
	 * Is defaulted.
	 */
	yggdrasil_scallingExperimentResult_t&
	operator= (
		yggdrasil_scallingExperimentResult_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_scallingExperimentResult_t()
	 = default;


	/*
	 * =======================
	 * Add a new experiment
	 */
public:
	/**
	 * \brief	This function adds the experiment to *this.
	 *
	 * \param  experiemnt		The new experiment, that should be added.
	 */
	void
	addExperiment(
		const SingleExperiment_t&	experiemnt)
	{
		m_experiments.insert(experiemnt);
		return;
	}; //End of inserting


	/*
	 * ========================
	 * Dumper
	 */
public:
	/**
	 * \brief	This functions writes a condensed
	 * 		 version of *this to the stream o.
	 *
	 * For each experiment the function writes the following
	 * to the stream.
	 *
	 * 	- N	     	~ Number of samples for fitting.
	 * 	- Nq		~ Numbers of samples for the evaluation.
	 * 	- MeanDepth   	~ The mean depth in the tree.
	 * 	- StdDepth	~ The standard deviation of the depth.
	 * 	- MaxDepth	~ The deepest node encounteres.
	 * 	- MISE 		~ The MISE of the experiment
	 *	- NTSplits	~ The number of true splits in the tree.
	 *	- NISplits	~ The number of indirect splits in the tree.
	 * 	- NLeafs	~ The number of leafs in the tree.
	 * 	- fitTime	~ The time for constructing in secodn.
	 * 	- evalTime	~ The time in seconds for performing the experiment.
	 *
	 * Every experiment a new line chanracter is inserted, even after the very
	 * last experiment.
	 *
	 * \param  o		The stream we write to.
	 * \param  sep		The seperatiojn string (default TAB)
	 */
	std::ofstream&
	writeToStream(
		std::ofstream& 		o,
		const std::string& 	sep = "\t")
	 const;


	/**
	 * \brief	This fucntion writes to a file.
	 *
	 * The file is overwritten if it exosts.
	 *
	 * \param  fileName	The file we have to write to
	 */
	void
	writeToFile(
		const std::string&	fileName,
		const std::string&	sep = "\t")
	 const;


	/*
	 * ======================
	 * Quierier
	 */
public:

	/*
	 * ======================
	 * Private Memobers
	 */
private:
	ExperimentMap_t 	m_experiments;		//!< This is the map that stores all the experiments
}; //End: class(yggdrasil_scallingExperimentResult_t)


YGGDRASIL_NS_END(yggdrasil)






