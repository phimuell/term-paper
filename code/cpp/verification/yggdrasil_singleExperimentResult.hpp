#pragma once
/**
 * \brief	THhis function stores the result of exactly one
 * 		 experiment, at a specific size and distribution.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_statistics.hpp>
#include <util/yggdrasil_treeDepthMap.hpp>


//INcluide std
#include <string>
#include <tuple>
#include <chrono>
#include <fstream>



YGGDRASIL_NS_BEGIN(yggdrasil)


/**
 * \brief	This class is one result of an experiment.
 * \class 	yggdrasil_singleExperimentResult_t
 *
 * This class stores exactly one experiment.
 * A result of this experiment has an MISE and some other
 * statistical properties atached to it.
 */
class yggdrasil_singleExperimentResult_t
{
	/*
	 * =================================
	 * Typedefs
	 */
public:
	using DepthMap_t 		= yggdrasil_treeDepthMap_t;	//!< This is the depth map
	using CompositionPack_t		= ::std::tuple<Size_t, Size_t, Size_t>;	//!> COmposition of the tree, as returned by the tree function
	using TimeBase_t		= ::std::chrono::duration<Numeric_t, ::std::ratio<1, 1> >;	//!< This is the timebase in seconds

	/*
	 * =======================
	 * Constructors
	 */
public:
	/**
	 * \brief 	Building constructor.
	 *
	 * THis is the building constructor.
	 * Given the data it initializes it.
	 *
	 * \param  nSamples	The number of samples in the experiment for fitting the tree.
	 * \param  qSampels	The number of samples that where used to generate the MISE.
	 * \param  depthMaü	The depth map of teh resulting tree.
	 * \param  treeComp	The composition (counts of the different node types) of the tree.
	 * \param  MISE		The MISE of the exoeriment.
	 * \param  fitTime	The time needed to fit the tree.
	 * \param  evalTime	The time needed for the evaluation, the fitting is excluded.
	 */
	yggdrasil_singleExperimentResult_t(
		const Size_t 			nSamples,
		const Size_t 			qSamples,
		const DepthMap_t& 		depthMap,
		const CompositionPack_t&	treeComp,
		const Numeric_t 		MISE,
		const TimeBase_t 		fitTime,
		const TimeBase_t 		evalTime)
	 :
	  m_nSamples(nSamples),
	  m_qSamples(qSamples),
	  m_depthMap(depthMap),
	  m_treeCompos(treeComp),
	  m_MISE(MISE),
	  m_fitTime(fitTime),
	  m_evalTime(evalTime)
	{};


	/**
	 * \brief	Default constructor.
	 *
	 * Is deleted.
	 */
	yggdrasil_singleExperimentResult_t()
	 = delete;


	/**
	 * \brief	Copy constructor.
	 *
	 * Defaulted.
	 */
	yggdrasil_singleExperimentResult_t(
		const yggdrasil_singleExperimentResult_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleExperimentResult_t&
	operator= (
		const yggdrasil_singleExperimentResult_t&)
	 = default;


	/**
	 * \breif	Move constructor.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleExperimentResult_t(
		yggdrasil_singleExperimentResult_t&& )
	 = default;


	/**
	 * \brief	Move assignemnt.
	 *
	 * Is defaulted.
	 */
	yggdrasil_singleExperimentResult_t&
	operator= (
		yggdrasil_singleExperimentResult_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~yggdrasil_singleExperimentResult_t()
	 = default;


	/*
	 * ========================
	 * Dumper
	 */
public:
	/**
	 * \brief	This functions writes a condensed
	 * 		 version of *this to the stream o.
	 *
	 * This function writes the following to the stream.
	 *
	 * 	- N	     	~ Number of samples for fitting.
	 * 	- Nq		~ Numbers of samples during evaluation.
	 * 	- MeanDepth   	~ The mean depth in the tree.
	 * 	- StdDepth	~ The standard deviation of the depth.
	 * 	- MaxDepth	~ The deepest node encounteres.
	 * 	- MISE 		~ The MISE of the experiment.
	 *	- NTSplits	~ The number of true splits in the tree.
	 *	- NISplits	~ The number of indirect splits in the tree.
	 * 	- NLeafs	~ The number of leafs in the tree.
	 * 	- fitTime	~ The time for constructing in secodn.
	 * 	- evalTime	~ The time in seconds for performing the experiment.
	 *
	 * At the end of the strem NO new line is appended.
	 * It is also possooble to select the intention.
	 *
	 * \param  o		The stream we write to.
	 * \param  sep		The seperatiojn string (default TAB)
	 */
	std::ofstream&
	writeToStream(
		std::ofstream& 		o,
		const std::string& 	sep = "\t")
	 const;


	/*
	 * ======================
	 * Getter
	 */
public:
	/*
	 * Currently they are not implemnted.
	 */


	/*
	 * ====================
	 * Operator
	 */
public:
	/**
	 * \brief	Less than operator.
	 *
	 * COmpares *this, as LHS, with rhs.
	 * The comparaition is done on the numbers
	 * of samples
	 *
	 * \param  rhs		The result that is the right hand.
	 */
	bool
	operator< (
		const yggdrasil_singleExperimentResult_t& 	rhs)
	 const
	 noexcept
	{
		return this->m_nSamples < rhs.m_nSamples;
	}; //End: operator<


	/*
	 * ======================
	 * Private Memobers
	 */
private:
	Size_t 			m_nSamples;	//!< The number of samples that where used for the fitting.
	Size_t 			m_qSamples;	//!< The number of samples for the quering.
	DepthMap_t 		m_depthMap;	//!< This is the depth map.
	CompositionPack_t 	m_treeCompos;	//!< This is the tree composition, in terms of nodes.
	Numeric_t 		m_MISE;		//!< This is the mean error.
	TimeBase_t 		m_fitTime;	//!< The time for fitting.
	TimeBase_t 		m_evalTime;	//!< THhe time spent for evaluating the experiment (without fitting).
}; //End: class(yggdrasil_singleExperimentResult_t)



YGGDRASIL_NS_END(yggdrasil)






