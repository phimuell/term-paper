#we will add the cpp files to the source and the rest as bublic
target_sources(
	Yggdrasil
  PRIVATE
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_singleExperimentResult.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_scallingExperimentResult.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_scallingDriver.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_distributionDriver.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_stdExperimentDriver.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_dumpDistribut.cpp"
  PUBLIC
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_singleExperimentResult.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_scallingExperimentResult.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/yggdrasil_verificationDriver.hpp"
  )


