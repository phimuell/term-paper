/**
 * \brief	THhis function stores the result of exactly one
 * 		 experiment, at a specific size and distribution.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_statistics.hpp>
#include <util/yggdrasil_treeDepthMap.hpp>

#include <verification/yggdrasil_singleExperimentResult.hpp>


//INcluide std
#include <string>
#include <tuple>
#include <chrono>
#include <fstream>



YGGDRASIL_NS_BEGIN(yggdrasil)


	/**
	 * \brief	This functions writes a condensed
	 * 		 version of *this to the stream o.
	 *
	 * This function writes the following to the stream.
	 *
	 * 	- N	     	~ Number of samples for fitting.
	 * 	- Nq		~ Number of samples for evaluation.
	 * 	- MeanDepth   	~ The mean depth in the tree.
	 * 	- StdDepth	~ The standard deviation of the depth.
	 * 	- MaxDepth	~ The deepest node encounteres.
	 * 	- MISE 		~ The MISE of the experiment
	 *	- NTSplits	~ The number of true splits in the tree.
	 *	- NISplits	~ The number of indirect splits in the tree.
	 * 	- NLeafs	~ The number of leafs in the tree.
	 * 	- fitTime	~ The time for constructing in secodn.
	 * 	- evalTime	~ The time in seconds for performing the experiment.
	 *
	 * At the end of the strem NO new line is appended.
	 * It is also possooble to select the intention.
	 *
	 * \param  o		The stream we write to.
	 * \param  sep		The seperatiojn string (default TAB)
	 */
std::ofstream&
yggdrasil_singleExperimentResult_t::writeToStream(
	std::ofstream& 		o,
	const std::string& 	sep)
 const
{
	if(o.is_open() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The stream that was given is not open.");
	};
	if(sep.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The seperation character is empty.");
	};

	//ENpack the tuple
	Size_t NTSplits, NISplits, NLeafs;
	std::tie(NTSplits, NISplits, NLeafs) = m_treeCompos;

	//Get the current precision
	const auto oCurrPrec = o.precision();

	//Set the precision to a high value
	o.precision(16);

	o
		<< m_nSamples 			<< sep
		<< m_qSamples 			<< sep
		<< m_depthMap.getDepthMean() 	<< sep
		<< m_depthMap.getDepthStd() 	<< sep
		<< m_depthMap.getMaxDepth() 	<< sep
		<< m_MISE 			<< sep
		<< NTSplits 			<< sep
		<< NISplits 			<< sep
		<< NLeafs 			<< sep
		<< m_fitTime.count() 		<< sep
		<< m_evalTime.count();

	//Set the precision back to its original value
	o.precision(oCurrPrec);

	return o;
}; //ENd: write to file



YGGDRASIL_NS_END(yggdrasil)


