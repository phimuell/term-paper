/**
 * \brief	This fiule implements the dunmping of the data.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_util.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>

#include <verification/yggdrasil_verificationDriver.hpp>

//INcluide std
#include <fstream>


//Include Boost
#include <boost/filesystem.hpp>

#if defined(YGGDRASIL_FOUND_HDF5) && YGGDRASIL_FOUND_HDF5 == 1

//Include HDF if found
#include <H5Cpp.h>

#else
//Make sure it is undefined
#undef YGGDRASIL_FOUND_HDF5

#endif

namespace fs = ::boost::filesystem;


YGGDRASIL_NS_BEGIN(yggdrasil)


/*
 * This si the fallback dumper
 */
static
void
ygInternal_fallbackDumper(
	const Path_t&						dumpPath,
	const Path_t& 						destPath,
	const yggdrasil_arraySample_t&				sampels_x,
	const yggdrasil_randomDistribution_i::ValueArray_t&	pdf_est,
	const yggdrasil_randomDistribution_i::ValueArray_t* const pdf_ex);




void
yggdrasil_dumpDistribution(
	const Path_t&						dumpPath,
	const Path_t& 						destPath,
	const yggdrasil_arraySample_t&				sampels_x,
	const yggdrasil_randomDistribution_i::ValueArray_t&	pdf_est,
	const yggdrasil_randomDistribution_i::ValueArray_t* const pdf_ex)
{
	if(sampels_x.nSamples() == 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("No sampels at all where given.");
	};
	if(sampels_x.nSamples() != pdf_est.size())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The number of samples and the number of estimated probabilities differ.");
	};
	if(pdf_ex != nullptr && pdf_ex->size() != sampels_x.nSamples())
	{
		throw YGGDRASIL_EXCEPT_InvArg("The number of sampels and the numbers of exact probabilities differs.");
	};


	//Currently only the fallback is supported
	bool useFallback = true;


	if(useFallback == true)
	{
		//We only test if teh file exists and if not we make a folder
		//The fallback implementation will take care of the rest
		if(fs::exists(dumpPath) == false)
		{
			fs::create_directories(dumpPath);
		};

		ygInternal_fallbackDumper(dumpPath, destPath, sampels_x, pdf_est, pdf_ex);
	}
	else
	{
		/*
		 * Use HDF5
		 */
		throw YGGDRASIL_EXCEPT_illMethod("The HDF5 dumping is not yet implemented.");
	};

	return;
};




/*
 * ========================
 * Implementation of tzhe helper function
 */


void
ygInternal_fallbackDumper(
	const Path_t&						dumpPath,
	const Path_t& 						destPath,
	const yggdrasil_arraySample_t&				sampels_x,
	const yggdrasil_randomDistribution_i::ValueArray_t&	pdf_est,
	const yggdrasil_randomDistribution_i::ValueArray_t* const pdf_ex)
{
	//We requere this
	if(fs::is_directory(dumpPath) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The path argument \"" + dumpPath.string() + "\" is not a folder");
	};

	//Now compose the real dump path
	const Path_t realDumpPath = dumpPath / destPath;

	//Test if the folder exists
	if(fs::exists(realDumpPath) == false)
	{
		//THe folder does not exist, so we create it
		fs::create_directories(realDumpPath);
	}
	else
	{
		if(fs::is_directory(realDumpPath) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The path \"" + destPath.string() + "\" inside the dump path exiosts, but is not a folder.");
		};
	};


	/*
	 * ==============
	 * Dump the samples
	 */
	{
		const Path_t sample_path = realDumpPath / "sample_x";
		std::ofstream out;
		out.open(sample_path.c_str(), std::fstream::out | std::fstream::trunc);
		out.precision(16);

		if(out.is_open() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Could not open the file \"" + sample_path.string() + "\"");
		};

		const Size_t N = sampels_x.nSamples();
		const Size_t D = sampels_x.nDims();

		for(Size_t k = 0; k != N; ++k)
		{
			const auto S = sampels_x[k];

			for(Size_t j = 0; j != D; ++j)
			{
				if(j != 0)
				{
					out << '\t';
				};
				out << S[j];
			}; //ENd for(j):

			out << '\n';
		}; //ENd for(k):

		out.close();
	}; //End: scample writing scope


	/*
	 * =================
	 * Writing the estimated pdf
	 */
	{
		const Path_t pdf_est_path = realDumpPath / "pdf_est";
		std::ofstream out;
		out.open(pdf_est_path.c_str(), std::fstream::out | std::fstream::trunc);
		out.precision(16);

		if(out.is_open() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Could not open the file \"" + pdf_est_path.string() + "\"");
		};

		const Size_t N = pdf_est.size();

		for(Size_t k = 0; k != N; ++k)
		{
			out << pdf_est[k] << '\n';
		}; //ENd for(k):

		out.close();
	}; //ENd scope: estimated pdf

	/*
	 * =================
	 * Writing the exact pdf
	 */
	if(pdf_ex != nullptr)
	{
		const Path_t pdf_ex_path = realDumpPath / "pdf_ex";
		std::ofstream out;
		out.open(pdf_ex_path.c_str(), std::fstream::out | std::fstream::trunc);
		out.precision(16);

		if(out.is_open() == false)
		{
			throw YGGDRASIL_EXCEPT_RUNTIME("Could not open the file \"" + pdf_ex_path.string() + "\"");
		};

		const Size_t N = pdf_ex->size();
		auto p = pdf_ex->data();

		for(Size_t k = 0; k != N; ++k)
		{
			out << p[k] << '\n';
		}; //ENd for(k):

		out.close();
	}; //ENd scope: estimated pdf

	return;
}; //End fallback






YGGDRASIL_NS_END(yggdrasil)


