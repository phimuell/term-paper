/**
 * \brief	This file hosts the functions for performing a scalling experiment.
 *
 * The experiment is done on one distribution and one type of builder.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <tree/yggdrasil_treeImpl.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>

#include <verification/yggdrasil_singleExperimentResult.hpp>
#include <verification/yggdrasil_verificationDriver.hpp>

//INcluide std
#include <chrono>


//Include Boost
#include <boost/filesystem.hpp>

namespace fs = ::boost::filesystem;


YGGDRASIL_NS_BEGIN(yggdrasil)

extern
bool
ygInternal_isTreeFullyValid(
	const yggdrasil_DETree_t& 	tree);



yggdrasil_scallingExperimentResult_t
yggdrasil_performScallingExperiment(
	yggdrasil_randomDistribution_i&	distribution,
	yggdrasil_pRNG_t&		geni,
	std::vector<Size_t>		sizeList,
	const Size_t 			nReps,
	const Size_t 			miseFactor,
	const yggdrasil_treeBuilder_t&	builder,
	const yggdrasil_hyperCube_t&	domain,
	const std::string&		description,
	const Path_t&			dumpFolder,
	const std::string&		dumpFilePre)
{
	if(sizeList.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The size list is empyt.");
	};
	if(miseFactor <= 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The MISE factor is negative or zero.");
	};
	if(description.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The description is empyt.");
	};
	if(::std::all_of(sizeList.cbegin(), sizeList.cend(), [](const Size_t& s) -> bool {return s > 0;}) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The size list contains invalid arguments.");
	};
	if(nReps == 0)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The number of repetitions are zero.");
	};
	if(fs::is_directory(dumpFolder) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The given dump folder, " + dumpFolder.string() + ", is not a directory.");
	};
	if(dumpFilePre.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dump prefix was empty.");
	};

	/*
	 * ======================
	 * Typedefs
	 */
	using Clock_t 		= ::std::chrono::high_resolution_clock;
	using ScallingResult_t 	= yggdrasil_scallingExperimentResult_t;
	using SingleResult_t 	= ScallingResult_t::SingleExperiment_t;
	using CompositionPack_t = SingleResult_t::CompositionPack_t;
	using DepthMap_t 	= SingleResult_t::DepthMap_t;
	using TimePoint_t  	= Clock_t::time_point;
	using TimeBase_t 	= SingleResult_t::TimeBase_t;
	using Tree_t 		= ::yggdrasil::yggdrasil_DETree_t;
	using Sample_t 		= Tree_t::Sample_t;
	using Distribution_t	= yggdrasil_randomDistribution_i;
	using SampleArray_t 	= Distribution_t::SampleArray_t;
	using SampleList_t 	= Distribution_t::SampleList_t;
	using PDFValues_t 	= Distribution_t::ValueArray_t;



	//Sorting the size list
	std::sort(sizeList.begin(), sizeList.end());

	//Output results
	ScallingResult_t scallingResult;

	// Globale times
	TimePoint_t globalStart = Clock_t::now();
	TimePoint_t globalEnd;

	//The dimension size
	const Size_t nDimensions = distribution.nDims();

	std::cout
		<< "Starting with the experiment of:\n"
		<< "\t" << description << "\n" << std::endl;

	//Iterating through the current size
	for(Size_t i = 0; i != sizeList.size(); ++i)
	{
		//Size of the experiment
		const Size_t sampleSize = sizeList[i];
		const Size_t querySize  = sampleSize * miseFactor;

		//This is the time for one size
		TimePoint_t sizeTimeStart, sizeTimeEnd;

		//Informms what happened
		std::cout << "Start with size(" << i << ") ~ " << sampleSize << " (" << querySize << ")" << std::endl;

		//Start the clock
		sizeTimeStart = Clock_t::now();

		/*
		 * Perform the repetitions
		 */
		for(Size_t rep = 0; rep != nReps; ++rep)
		{
			/*
			 * ================
			 * Dump the state
			 */
			//This is the general pattern
			const std::string fullDumpName = dumpFilePre + ".N_" + std::to_string(sampleSize) + "_" + std::to_string(querySize) + ".Rep_" + std::to_string(rep);

			//Now make the file for the two specific ones
			const Path_t fullDumpName_dist = dumpFolder / Path_t(fullDumpName + ".dist");
			const Path_t fullDumpName_geni = dumpFolder / Path_t(fullDumpName + ".geni");

			//Write the distribution to the file
			distribution.dumpToFile(fullDumpName_dist.string());

			//Write the generator to a file
			yggdrasil_pRNG_dump(fullDumpName_geni.string(), geni);



			/*
			 * ================
			 * Output and start the clock
			 */

			//Make an output to inform the user where we are
			std::cout << "\t" << "Start with iteration " << (rep + 1) << " of " << nReps << std::endl;

			//This is the start of the experiment
			TimePoint_t repetitionStart, repetitionEnd;

			//Start with the repertition timer
			repetitionStart = Clock_t::now();



			/*
			 * ==============
			 */

			//Locale times
			TimePoint_t fittingStart, fittingEnd;
			TimePoint_t evalStart, evalEnd;

			//Time spans
			TimeBase_t fittingTime, evaluatingTime;

			/*
			 * Create the samples for the fitting
			 */
			SampleArray_t sampleArray(distribution.generateSamplesArray(geni, sampleSize));


			/*
			 * Fitting the tree
			 */
			fittingStart = Clock_t::now();
			Tree_t currentTree(sampleArray, domain, builder);
			fittingEnd = Clock_t::now();

			//Calculating the fitting time
			fittingTime = ::std::chrono::duration_cast<TimeBase_t>(fittingEnd - fittingStart);

			/*
			 * Make some tests
			 */
			if(ygInternal_isTreeFullyValid(currentTree) == false)
			{
				std::cerr << " --  " << "Tree did not pass the tests." << std::endl;
				std::abort();
			};

			/*
			 * Gather the statistics
			 */
			CompositionPack_t treeComposition;
			DepthMap_t        treeDepthMap;

			treeComposition = currentTree.getTreeComposition();
			treeDepthMap    = currentTree.getDepthMap();
			yggdrasil_assert(treeDepthMap.nDiffDepth() > 0);

			/*
			 * ============
			 * 	MISE
			 */

			/*
			 * Generating the samples for the MISE
			 */

			//Variable for holding them
			SampleList_t querySamples(nDimensions);
			PDFValues_t   trueQueryPDFs;

			//Actuall generation
			::std::tie(querySamples, trueQueryPDFs) = distribution.generateSamplesListPDF(geni, querySize);

			yggdrasil_assert(querySamples.size() == querySize);
			yggdrasil_assert(querySamples.size() == trueQueryPDFs.size());
			yggdrasil_assert(::std::all_of(
					querySamples.cbegin(), querySamples.cend(),
					[](const Sample_t& s) -> bool {return s.isValid();}));
			yggdrasil_assert(::std::all_of(
					trueQueryPDFs.cbegin(), trueQueryPDFs.cend(),
					[](const Real_t x) -> bool {return ((x >= 0.0) && isValidFloat(x));}));


			/*
			 * Evaluating the Tree at the query points
			 */

			//Prepare the results
			PDFValues_t estQueryPDF;

			//Evaluating the PDF of the estimator
			evalStart = Clock_t::now();
			estQueryPDF = currentTree.evaluateSamplesAt(querySamples);
			evalEnd = Clock_t::now();

			//Get the evaluation count
			evaluatingTime = ::std::chrono::duration_cast<TimeBase_t>(evalEnd - evalStart);

			yggdrasil_assert(estQueryPDF.size() == querySize);
			yggdrasil_assert(::std::all_of(
					estQueryPDF.cbegin(), estQueryPDF.cend(),
					[](const Real_t x) -> bool {return ((x >= 0.0) && isValidFloat(x));}));




			/*
			 * Evaluating the MISE
			 */

			//This is the variable for summing up the Values
			::std::vector<Real_t> pMISE(16, 0.0);

			//Terate over the two samples and iterate over the
			//the two value ranges.
			//using equation (9) of the paper
			for(Size_t q = 0; q != querySize; ++q)
			{
				//Load the two values
				const auto pEst  = estQueryPDF[q];	//This is the estimated value
				const auto pTrue = trueQueryPDFs[q];	//This is the true value

				yggdrasil_assert(isValidFloat(pEst) == true);
				yggdrasil_assert(pEst >= 0.0);
				yggdrasil_assert(isValidFloat(pTrue) == true);
				yggdrasil_assert(pTrue > 0.0);	//Must be strigtly greater than zero

				//Compute the local MISE, We will never have a division by zero

				//Calculating the three individual terms

				//First term \frac{\hat{p}(x_q)^2}{p(x_q)}
				const Real_t term1 = pEst * pEst / pTrue;
				yggdrasil_assert(isValidFloat(term1));

				// Second term -2.0 \cdot p(x_q)
				const Real_t term2 = -2.0 * pEst;

				//Third term
				const Real_t term3 = pTrue;

				//Sum up the value and store it
				pMISE[q % 16] += (term1 + term2 + term3);
			}; //End for(q): evaluating the tree

			//Makeing the reduction
			Size_t RedCoef[] = {8, 4, 2, 1};
			for(const Size_t ende : RedCoef)
			{
				for(Size_t k = 0; k != ende; ++k)
				{
					pMISE[k] += pMISE[k + ende];
					pMISE[k + ende] = Real_t(0.0);
				}; //ENd for(k)
			}; //End for(ende)

			yggdrasil_assert(pMISE[0] > 0.0);
			yggdrasil_assert(::std::all_of(
				pMISE.cbegin() + 1, pMISE.cend(),
				[](const Real_t x) -> bool {return (x == Real_t(0.0));}));


			//Now make the true reduction
			const Real_t trueMISE = pMISE[0] / querySize;


			/*
			 * ==============
			 * Store the result
			 */


			//Construct a single experiment
			SingleResult_t sResult(
				sampleSize,		//The sample size used to generate the tree
				querySize, 		//The number of samples that are used for generating the query.
				treeDepthMap,		//The depoth map
				treeComposition,	//The tree composition
				trueMISE,		//The MISE
				fittingTime,		//The time for fitting the result
				evaluatingTime		//The time for evaluating the tree
				);

			//Store the single experiment result in the container
			scallingResult.addExperiment(sResult);

			/*
			 * =====================
			 */

			//The repetition now ends
			repetitionEnd = Clock_t::now();

			//The time for the repetition
			const TimeBase_t repetitionTime = ::std::chrono::duration_cast<TimeBase_t>(repetitionEnd - repetitionStart);

			//Make an output to inform the user where we are
			std::cout << "\t" << " Finished iteration " << (rep + 1) << " after " << repetitionTime.count() << "s\n";
		}; //End for(rep)

		//Stop the clock for the computation of the size
		sizeTimeEnd = Clock_t::now();

		//Get the duration, in the standard way
		const auto sizeDuration = sizeTimeEnd - sizeTimeStart;

		//Convert the duration in better readable parts
		const ::std::chrono::hours   sizeDuration_h = ::std::chrono::duration_cast<::std::chrono::hours>(sizeDuration);
		const ::std::chrono::minutes sizeDuration_m = ::std::chrono::duration_cast<::std::chrono::minutes>(sizeDuration);
		const ::std::chrono::seconds sizeDuration_s = ::std::chrono::duration_cast<::std::chrono::seconds>(sizeDuration);

		//Now get the fractional part
		const Size_t sizeDur_Hours = sizeDuration_h.count();
		const Size_t sizeDur_Mins  = sizeDuration_m.count() % 60;
		const Size_t sizeDur_Secs  = sizeDuration_s.count() % 60;

		std::cout
			<< "Finisched with size " << i << " after " << sizeDur_Hours << "h " << sizeDur_Mins << "mins " << sizeDur_Secs << "s"
			<< "\n"
			<< std::endl;
	}; //End for(i): testing all the different sizes

	/*
	 * ================
	 * Perform the output
	 */

	//Finishing the time of the global counter
	globalEnd = Clock_t::now();

	//Get the duration, in the standard way
	const auto globalDuration = globalEnd - globalStart;

	//Convert the duration in better readable parts
	const ::std::chrono::hours   globalDuration_h = ::std::chrono::duration_cast<::std::chrono::hours>(globalDuration);
	const ::std::chrono::minutes globalDuration_m = ::std::chrono::duration_cast<::std::chrono::minutes>(globalDuration);
	//const ::std::chrono::seconds globalDuration_s = ::std::chrono::duration_cast<::std::chrono::seconds>(globalDuration);

	//Now get the fractional part
	const Size_t globalDur_Hours = globalDuration_h.count();
	const Size_t globalDur_Mins  = globalDuration_m.count() % 60;
	//const Size_t globalDur_Secs  = globalDuration_s.count() % 60;

	std::cout
		<< "\n\n"
		<< "Finisched the computaion of:" << "\n"
		<< "\t" << description << "\n"
		<< "after " << globalDur_Hours << " hours and " << globalDur_Mins << " minuts."
		<< "\n\n"
		<< std::endl;


	/*
	 * Return the result
	 */
	return scallingResult;
}; //End of the driver



/*
 * This fuinction performs a small test of the tree
 */
bool
ygInternal_isTreeFullyValid(
	const yggdrasil_DETree_t& 	tree)
{
	if(tree.checkIntegrity() == false)
	{
		std::cerr << "The tree diod not pass the integrity check." << std::endl;
		return false;
	};


	auto it = tree.getLeafIterator();
	for(; it.isEnd() == false; it.nextLeaf())
	{
		const auto nf = it.access();

		if(nf.isFullySplittedLeaf() == false)
		{
			std::cerr << "The node is not fully splitted." << std::endl;
			return false;
		};
		if(nf.checkIntegrity() == false)
		{
			std::cerr << "The node failed the integrty check." << std::endl;
			return false;
		};
	}; //ENd for(it)

	if(tree.isFullySplittedTree() == false)
	{
		std::cerr << "The tree did not appear to be fully fitted." << std::endl;
		return false;
	};

	return true;
}; //End tree test



YGGDRASIL_NS_END(yggdrasil)


