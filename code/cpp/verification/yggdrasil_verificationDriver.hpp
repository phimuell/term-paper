/**
 * \brief	This file defines all functions that are needed
 * 		 To perform the verification process.
 *
 * Since this functions are very longly, they are distributed among severall
 * files to increaese verbosity.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <tree/yggdrasil_treeImpl.hpp>

#include <factory/yggdrasil_treeBuilder.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>

#include <verification/yggdrasil_singleExperimentResult.hpp>
#include <verification/yggdrasil_scallingExperimentResult.hpp>

//INcluide std


//Include Boost
//#include <boost/filesystem.hpp>
//FWD of the path
namespace boost
{
	namespace filesystem
	{
		class path;
	};
};

YGGDRASIL_NS_BEGIN(yggdrasil)

//Using the Path

using Path_t = ::boost::filesystem::path;


/**
 * \brief	This is the standard scalling element driver.
 *
 * This function takes a distribution and performs the scalling experiment.
 *
 * \param  distribution		This is the distribution that is used.
 * \param  geni			This is the generator that is used.
 * \param  domain 		This is the domain that is used, can be invalid.
 * \param  distName		Nice name of the distribution.
 * \param  distToken		A space free part of the filename, that names the distribution.
 * \param  saveFolder		The folder where we wqant to save to.
 *
 * \note	This function is bassically a wrapper arround
 * 		 the distribution driver, that canonize the
 * 		 parameters of the test.
 *
 * \note	This file is implemented in the file "yggdrasil_stdExperimentDriver.cpp"
 */
extern
void
yggdrasil_performStandardExperiment(
	yggdrasil_randomDistribution_i&		distribution,
	yggdrasil_pRNG_t&			geni,
	const yggdrasil_hyperCube_t&		domain,
	std::string				distName,
	std::string 				distToken,
	const Path_t& 				saveFolder);






/**
 * \brief	This function performs scalling
 * 		 experiments on a distribution.
 *
 * The difference between this function and the scalling driver is,
 * that this function performs the experiment on all possible trees,
 * that are implemented, which are currently 4.
 *
 * The distribution is reused every time.
 * The distribution description (\c distDescr) should only name
 * the distribution like "7D Gaussian" or so.
 * The distribution code howver must not contain any spaces and is
 * used for the file name, it should also be something descriptive
 * like "gauss_7d".
 *
 * The folder is used for saving the result.
 * The file prefix is used as a file prefix.
 * The full path of the reuslt file will look like:
 * 	${FOLDER_PATH}/${PREFIX}.${distDescr}.${treeDesc}.dat
 * Where treeDesc is a tokan that describes what the tree was.
 *
 * \param  dist			The distribution to use.
 * \param  geni 		The generator to use.
 * \param  sizeList		This is a list of the different sizes that should be genrated.
 * \param  reps			How many times an experiment should be repetited.
 * \param  miseFactor		How many samples more should be genrated for the MISE.
 * \param  gofSigLevel		The significance level used in the GOF tester.
 * \param  indepSigLevel	The significance level usde in the independence tester.
 * \param  domain		This domain is used for the tree, can also be invalid.
 * \param  distDescr		The description token of the distribution.
 * \param  distName		The name of the distribution.
 * \param  filePrefix		The file prefix that is used for the generation of the file.
 * \param  saveFolder		The folder where we save the result in.
 *
 * \note	This function is implemented in the file "yggdrasil_dsitributionDriver.cpp"
 */
extern
void
yggdrasil_distributionDriver(
	yggdrasil_randomDistribution_i&		distribution,
	yggdrasil_pRNG_t&			geni,
	std::vector<Size_t>&			sizeList,
	const Size_t 				reps,
	const Size_t 				miseFactor,
	const Numeric_t 			gofSigLevel,
	const Numeric_t 			indepSigLevel,
	const yggdrasil_hyperCube_t&		domain,
	std::string				distDescr,
	std::string 				distName,
	std::string				filePrefix,
	const Path_t& 				saveFolder);





/**
 * \brief	This function performs a scalling experiment.
 *
 * This function performs a scalling experiment, thsi means it will generate
 * severall of trees fitt them and then evaluate it.
 * The parameter are the following.
 *
 * \param  dist		This is the distribution that is used to generate the samples.
 * \param  geni		THis is the random number generator that is used.
 * \param  sizeList	This is a list of the different sizes that should be genrated.
 * \param  reps		How many times an experiment should be repetited.
 * \param  miseFactor	How many samples more should be genrated for the MISE.
 * \param  builder	This is the builder that describes how to build a tree.
 * \param  domain	This domain is used for the tree, can also be invalid.
 * \param  description	A description of the experiment, this is a string that is printed.
 * \param  dumpFolder	The path to a folder where the random state will be written to.
 * \param  dimpNamePre	The file prefix, ended without dot, this ios the prefix of the state dumper.
 *
 * After each fitting process the tree is tested.
 *
 * \note	This function is implemented in the "ygdrasil_scallingDriver.cpp"
 * 		 file.
 */
extern
yggdrasil_scallingExperimentResult_t
yggdrasil_performScallingExperiment(
	yggdrasil_randomDistribution_i&		distribution,
	yggdrasil_pRNG_t&			geni,
	std::vector<Size_t>			sizeList,
	const Size_t 				reps,
	const Size_t 				miseFactor,
	const yggdrasil_treeBuilder_t&		builder,
	const yggdrasil_hyperCube_t&		domain,
	const std::string&			description,
	const Path_t&				dumpFolder,
	const std::string&			dumpNamePre);

/**
 * \brief	This function writes an experiment to an HDF5 file.
 *
 * This funciton is for dumping the data such that it is possible,
 * to plot it after wards.
 * It operates on hd5 data files, but provides a fall back,
 * if it is not pressent.
 *
 * It takes two path argument, the first one is interpreted
 * as the file name, where the data should be written to,
 * aexisting files are not overwritten.
 * The second path argument is interpreted as a path
 * inside the HDF file.
 * If hfd5 is not present or the first path argument
 * is a folder, the fall back is invoked, that interpretes
 * the second path relative to teh first path.
 *
 * Inside the file it will creates thre files.
 *
 * 	samples_x	~ This is the sample position.
 * 	pdf_est		~ This is the estimated pdf from the tree.
 * 	pdf_ex 		~ This is the exact pdf.
 *
 * If the exact pdf is the null pointer, it will be ignored and not written.
 *
 * \param  dumpLoc	Path to the file/folder we want to write to.
 * \param  destPath	Path inside the file/folder to write the data.
 * \param  samples_x	This are the samples that should be written.
 * \param  pdf_est	This is a vector of estimated PDFs.
 * \param  pdf_ex	This are the exact pdf. If nullptr it will
 * 			 be ignored.
 */
extern
void
yggdrasil_dumpDistribution(
	const Path_t&						dumpPath,
	const Path_t& 						destPath,
	const yggdrasil_arraySample_t&				sampels_x,
	const yggdrasil_randomDistribution_i::ValueArray_t&	pdf_est,
	const yggdrasil_randomDistribution_i::ValueArray_t* const pdf_ex);




extern
bool
ygInternal_isTreeFullyValid(
	const yggdrasil_DETree_t& 	tree);





YGGDRASIL_NS_END(yggdrasil)


