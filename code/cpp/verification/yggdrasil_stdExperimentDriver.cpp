/**
 * \brief	This file contains the standard experiment driver.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <tree/yggdrasil_treeImpl.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>

#include <verification/yggdrasil_singleExperimentResult.hpp>
#include <verification/yggdrasil_verificationDriver.hpp>

//INcluide std
#include <chrono>


//Include Boost
#include <boost/filesystem.hpp>

namespace fs = ::boost::filesystem;



YGGDRASIL_NS_BEGIN(yggdrasil)
void
yggdrasil_performStandardExperiment(
	yggdrasil_randomDistribution_i&		distribution,
	yggdrasil_pRNG_t&			geni,
	const yggdrasil_hyperCube_t&		domain,
	std::string				distName,
	std::string 				distToken,
	const Path_t& 				saveFolder)
{
	/*
	 * Check if the argumets are correct
	 */
	if(distToken.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The description token of the distribution is empty.");
	};
	if(::std::any_of(distToken.cbegin(), distToken.cend(), [](const char c) -> bool {return (c == ' ');}))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The description token \"" + distToken + "\" contains spaces.");
	};
	if(distName.empty() == true)
	{
		//The name of the distribution is not set, so we use a generic one
		distName = "UNKOWN DISTRIBUTION";
	};

	/*
	 * Create the test folder if it does not exist
	 */
	if(fs::exists(saveFolder) == false)
	{
		//The folder does not exists, so we construct it.
		fs::create_directories(saveFolder);
	}
	else
	{
		//THe path exists, we must test if it is a folder
		if(fs::is_directory(saveFolder) == false)
		{
			throw YGGDRASIL_EXCEPT_InvArg("The path \"" + saveFolder.string() + "\" exists but is not a folder.");
		};
	}; //End: folder exists

	/*
	 * ========================
	 * Typedefs
	 */
	using Clock_t 		= ::std::chrono::high_resolution_clock;
	using ScallingResult_t 	= yggdrasil_scallingExperimentResult_t;
	using SingleResult_t 	= ScallingResult_t::SingleExperiment_t;
	using TimePoint_t  	= Clock_t::time_point;
	using Tree_t 		= ::yggdrasil::yggdrasil_DETree_t;
	using Builder_t 	= Tree_t::TreeBuilder_t;



	/*
	 * The standard parameters
	 */

	//The significance levels; they are taken from the paper
	const Numeric_t gofSigLevel   = 0.001;
	const Numeric_t indepSigLevel = 0.001;

#ifdef YGGDRASIL_STD_DRIVER_SMALL_SET
#pragma message "The small test set is selected"
	/*
	 * This is the test that are run in the small test ste mode.
	 * This is mostly for debugging.
	 */
	//Equidistance spaced in log splace
	std::vector<Size_t> sizeList = {    100,     316,
					   1000,    3162};
#else
#pragma message "The full test set is selected."

	/*
	 * This is the speperation of two sizes, in log space.
	 * This means that the samplings are spaced
	 * equaly in logspace.
	 * But in real space each size is $10^{logSizeSpaceing}$
	 * times larger than the previous one.
	 */
	const Numeric_t logSiszeSpacing = 0.25;

	// This is the initial exponent that is used.
	const Numeric_t initialExponent = 2.0;

	// This is the base
	const Numeric_t Base = 10.0;

	//This is the number of different sizes that are tested
	const Size_t nDiffSizes = 20;

	//This is the vector that contains all the sizes
	std::vector<Size_t> sizeList;

	//Generate the list
	for(Size_t i = 0; i != nDiffSizes; ++i)
	{
		//Calculate the current exponent
		const Numeric_t exp_i  = initialExponent + i * logSiszeSpacing;

		//Calculate the size of thsi step (is Numeric)
		const Numeric_t size_i = ::std::pow(Base, exp_i);

		//Save the result in the list
		sizeList.push_back(Size_t(size_i));
	}; //End for(i): generating the list
#endif


	//This is the MISE factor, also the numbers of sample we use to evaluate.
#ifdef YGGDRASIL_STD_DRIVER_SMALL_SET
	const Size_t miseFactor = 2;
#else
	const Size_t miseFactor = 9;
#endif

	//This is the number of times we repet an experiment
#ifdef YGGDRASIL_STD_DRIVER_SMALL_SET
	const Size_t nReps = 2;
#else
	const Size_t nReps = 6;
#endif

	//This is the file prefix
	const std::string filePrefix = "StandardScalingExper";


	/*
	 * Calling the driver
	 */
	yggdrasil_distributionDriver(
		distribution,
		geni,
		sizeList,
		nReps,
		miseFactor,
		gofSigLevel,
		indepSigLevel,
		domain,
		distToken,
		distName,
		filePrefix,
		saveFolder);

	return;
}; //End: distribution driver


YGGDRASIL_NS_END(yggdrasil)


