/**
 * \brief	This class stores the results of several experiments
 * 		 that together forms a scalling experiment.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_statistics.hpp>

#include <tree/yggdrasil_nodeFacace.hpp>

#include <verification/yggdrasil_singleExperimentResult.hpp>
#include <verification/yggdrasil_scallingExperimentResult.hpp>

//INcluide std
#include <set>
#include <fstream>



YGGDRASIL_NS_BEGIN(yggdrasil)


std::ofstream&
yggdrasil_scallingExperimentResult_t::writeToStream(
	std::ofstream& 		o,
	const std::string& 	sep)
 const
{
	if(o.is_open() == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("CAn not write to a close file.");
	};

	for(const SingleExperiment_t& experiment : m_experiments)
	{
		experiment.writeToStream(o, sep) << "\n";
	}; //End for(experiment)

	return o;
}; //End: write to stream


void
yggdrasil_scallingExperimentResult_t::writeToFile(
	const std::string&	fileName,
	const std::string& 	sep)
 const
{
	if(fileName.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The filename is empty.");
	};
	if(sep.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("the seperation string is empty.");
	};

	::std::ofstream file;
	file.open(fileName.c_str(), ::std::ofstream::trunc | ::std::ofstream::out);

	if(file.is_open() == false)
	{
		throw YGGDRASIL_EXCEPT_RUNTIME("The file could not be created/opened");
	};

	this->writeToStream(file, sep);

	file.close();

	return;
}; //End: write to file


YGGDRASIL_NS_END(yggdrasil)


