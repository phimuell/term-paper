/**
 * \brief	This function hostes the code to test a distribution.
 *
 * It builds a lot of trees that are then tested.
 */

//Includeing Yggdrasil core
#include <core/yggdrasil_core.hpp>
#include <core/yggdrasil_assert.hpp>
#include <core/yggdrasil_exception.hpp>
#include <core/yggdrasil_int.hpp>

#include <util/yggdrasil_hyperCube.hpp>

#include <tree/yggdrasil_treeImpl.hpp>

#include <random/yggdrasil_random.hpp>
#include <random/yggdrasil_interface_distribution.hpp>

#include <verification/yggdrasil_singleExperimentResult.hpp>
#include <verification/yggdrasil_verificationDriver.hpp>

//INcluide std
#include <chrono>


//Include Boost
#include <boost/filesystem.hpp>




YGGDRASIL_NS_BEGIN(yggdrasil)

namespace fs = ::boost::filesystem;

void
yggdrasil_distributionDriver(
	yggdrasil_randomDistribution_i&		distribution,
	yggdrasil_pRNG_t&			geni,
	std::vector<Size_t>&			sizeList,
	const Size_t 				reps,
	const Size_t 				miseFactor,
	const Numeric_t 			gofSigLevel,
	const Numeric_t 			indepSigLevel,
	const yggdrasil_hyperCube_t&		domain,
	std::string				distDescr,
	std::string 				distName,
	std::string				filePrefix,
	const Path_t& 				saveFolder)
{
	/*
	 * Check if the argumets are correct
	 */
	if(distDescr.empty() == true)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The description token of the distribution is empty.");
	};
	if(::std::any_of(distDescr.cbegin(), distDescr.cend(), [](const char c) -> bool {return (c == ' ');}))
	{
		throw YGGDRASIL_EXCEPT_InvArg("The description token \"" + distDescr + "\" contains spaces.");
	};
	if(distName.empty() == true)
	{
		//The name of the distribution is not set, so we use a generic one
		distName = "UNKOWN DISTRIBUTION";
	};
	if(filePrefix.empty() == true)
	{
		//The file prefix is empyt, so we use a generic expresion
		filePrefix = "genericScallingExperiment";
	};
	if(fs::is_directory(saveFolder) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The folder argument \"" + saveFolder.string() + "\" is not a folder.");
	};

	/*
	 * ========================
	 * Typedefs
	 */
	using Clock_t 		= ::std::chrono::high_resolution_clock;
	using ScallingResult_t 	= yggdrasil_scallingExperimentResult_t;
	using SingleResult_t 	= ScallingResult_t::SingleExperiment_t;
	using TimePoint_t  	= Clock_t::time_point;
	using Tree_t 		= ::yggdrasil::yggdrasil_DETree_t;
	using Builder_t 	= Tree_t::TreeBuilder_t;


	//Sorting the size list
	std::sort(sizeList.begin(), sizeList.end());

	//Output results
	ScallingResult_t scallingResult;

	// Globale times
	TimePoint_t globalStart, globalEnd;

	//This ist the dump folder for the distribution
	const Path_t dumpFolder = saveFolder / "randState";

	//test if the folder exists and if not then create it
	if(fs::exists(dumpFolder) == false)
	{
		//Folder does not exist
		fs::create_directories(dumpFolder);
	};
	if(fs::is_directory(dumpFolder) == false)
	{
		throw YGGDRASIL_EXCEPT_InvArg("The dumpfolder path " + dumpFolder.string() + ", exists, but is not a folder.");
	};



	/*
	 * Set up the variable so that we can iterate over the struf
	 */

	//This vector will contains all the building instances
	::std::vector<Builder_t> builders;

	//This vector will contains all the description of the experiments
	::std::vector<::std::string> experimentDescriptions;

	//This vector will contain the experiment tokens that are used for the file name
	::std::vector<::std::string> experiTockens;

#if 1
	// 1)
	// Constant model with size splitting
	builders.push_back(Builder_t(eParModel::ConstModel).useSizeSplitter().setGOFLevel(gofSigLevel).setIndepLevel(indepSigLevel));
	experimentDescriptions.push_back(distName + " const. model, size splitting");
	experiTockens.push_back("const_size");
#endif


	// 2)
	// Constant model with median splitting
	builders.push_back(Builder_t(eParModel::ConstModel).useMedianSplitter().setGOFLevel(gofSigLevel).setIndepLevel(indepSigLevel));
	experimentDescriptions.push_back(distName + " const. model, median splitting");
	experiTockens.push_back("const_median");


	// 3)
	// Linear model with size splitting
	builders.push_back(Builder_t(eParModel::LinModel).useSizeSplitter().setGOFLevel(gofSigLevel).setIndepLevel(indepSigLevel));
	experimentDescriptions.push_back(distName + " lin. model, size splitting");
	experiTockens.push_back("lin_size");


	// 4)
	// Linear model with meidan splitting
	builders.push_back(Builder_t(eParModel::LinModel).useMedianSplitter().setGOFLevel(gofSigLevel).setIndepLevel(indepSigLevel));
	experimentDescriptions.push_back(distName + " lin. model, median splitting");
	experiTockens.push_back("lin_median");


	/*
	 * Give some output
	 */
	std::cout
		<< "\n"
		<< "Staring with the Distribution experiment of the \"" << distName << "\" distribution."
		<< "\n\n"
		<< std::endl;


	/*
	 * ===================
	 * Starting the real experiment
	 */
	globalStart = Clock_t::now();


	//Going through the different compination for that distribution
	for(Size_t e = 0; e != builders.size(); ++e)
	{
		//Load the parameter
		const Builder_t 	eBuilder    = builders[e];
		const ::std::string 	eTocken     = experiTockens[e];
		const ::std::string 	eName 	    = experimentDescriptions[e];

		//Make the dump file prefix
		const std::string 	dumpFilePre = filePrefix + "." + distDescr + "." + eTocken;

		//Now perform a scalling experiment
		const ScallingResult_t scallingExperiment = yggdrasil_performScallingExperiment(
			distribution,
			geni,
			sizeList,
			reps,
			miseFactor,
			eBuilder,
			domain,
			eName,
			dumpFolder,
			dumpFilePre);

		//compose the filename to save the file to
		const ::std::string saveLocation
			= (saveFolder / Path_t(filePrefix + "." + distDescr + "." + eTocken + ".dat")).string();

		//Save the result
		scallingExperiment.writeToFile(saveLocation);
	}; //End for(e):


	/*
	 * Stop the clock and give some output
	 */
	globalEnd = Clock_t::now();


	//Get the duration, in the standard way
	const auto globalDuration = globalEnd - globalStart;

	//Convert the duration in better readable parts
	const ::std::chrono::hours   globalDuration_h = ::std::chrono::duration_cast<::std::chrono::hours>(globalDuration);
	const ::std::chrono::minutes globalDuration_m = ::std::chrono::duration_cast<::std::chrono::minutes>(globalDuration);
	//const ::std::chrono::seconds globalDuration_s = ::std::chrono::duration_cast<::std::chrono::seconds>(globalDuration);

	//Now get the fractional part
	const Size_t globalDur_Hours = globalDuration_h.count();
	const Size_t globalDur_Mins  = globalDuration_m.count() % 60;
	//const Size_t globalDur_Secs  = globalDuration_s.count() % 60;

	std::cout
		<< "\n\n"
		<< "Finisched the computaion of:" << "\n"
		<< "\t" << distName << "\n"
		<< "after " << globalDur_Hours << " hours and " << globalDur_Mins << " minuts."
		<< "\n\n"
		<< std::endl;


	return;
}; //End: distribution driver


YGGDRASIL_NS_END(yggdrasil)


