# Info
This folder contains the “practical” part of the documentation.
It is recommended that the user first reads or at least briefly consult the manual of Yggdrasil before he/she continues with the material in this folder.
It is also recommended to read the actual C++ code, after all it is _the_ documentation 
that is used by Yggdrasil when it decides what to do next. 

The examples are split into two parts, see next sections for more information.
Together with the manual this should be sufficiently to use Yggdrasil in a useful and efficient manner.


## Notebooks
This folder contains Jupyter Notebooks.
Jupyter allows to run Python in a browser, the whole things look like Mathematica, and the usage is very similar (Shift+Enter to run the cell).
They are "walk through" like scripts that offers an interactive and instructive way of the usage of pyYggdrasil.
They are also a form of testing the code itself.
The user is advised to first read the manual and afterwards going through them.
Even if the user will use the C++ interface, it is recommended to go through the examples that are provided here.
The reason is that the interface of pyYggdrasil is very similar to the one that Yggdrasil provides on C++ with some small differences.


## Scripts
This are classical Python scripts.
That can be executed and produce some output.
However they are more meant to be read rather than to be executed.
Since they are not considered very important there are currently non of them.
However the Notebooks should be enough.


## exercise
This is a small set of exercises that the user can solve after the notebook, manual and script were studied.
It is written like an exam, there are no solution since the result should be clear and the Notebooks show the most part anyway.
Note it is expected that at least the Python part can be solved, if some time was devoted to it.

The C++ part is considered very hard, it is more meant as a to do list or possible points for improvement than anything else.



# Note to the C++ Interface
When the pyYggdrasil (the interface to Python) was written it was done in a way to mimic the C++ interface.
So by studying pyYggdrasil you also studying the C++ interface.
Also note that C++ allows a more direct way of interaction with Yggdrasil, and it is recommended to use the C++ interface directly.
