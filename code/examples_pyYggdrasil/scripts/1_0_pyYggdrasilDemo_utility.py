#!/bin/python

# This module is about the basics.
# It demonstrate how the utility code is used
#
# THIS FILE IS OLD AND WAS SUPERSEDED BY A
#  JUPYTER NOTEBOOK.
# =============================


# First of all we need to load the module
# Make sure that the module is placed in a location where
# Python can find it. placing it into the local folder works.
# Here we working with a symlink.
# You do not have to use the complex name of the binary.
# Just "pyYggdrasil" is enough
import pyYggdrasil as pyY

# = I N T E R V A L S
#
# Intervals are domains in one dimensions.
# In Yggdrasil an interval is a right open interval, this
# means that the point of the upper bound is not included the
# interval.
#
# Ths interval is an imutable object, this means it is
# Impossible to change it after it was created.

# This will create a unit interval
# The paranthesis at the end are requested
unitInterval = pyY.Interval()

# They can be printed and converted to strings
print("The just created interval is: {}".format(unitInterval))

# It is also possible to create a custom interval
# First comes the lower and the the upper bound
customInterval = pyY.Interval(-10.0, 7.768)

# Again print the custom interval
print("The custom interval is: {}".format(customInterval))

# As with python we also support named argumenst
customInterval2 = pyY.Interval(lower=5.0, upper=10)
print("The second custom interval is: {}".format(customInterval2))

# The interval is so inteligent, that it does check if the interval is valid
try:
    invalidInterval = pyY.Interval(lower = 10.0, upper = 1)
except:
    print("It was not possible to construct an interval, where the upper bound ls lower than the lower bound.")
    print("Such an interval is invalid.")

# Because of the interval definitions, the bounds must be distinct
try:
    anINtervalOfZeroLength = pyY.Interval(lower = 1.0, upper = 1.0)
except:
    print("Again it is not possible to construct a zero length interval.")


# It is possible to query if the interval is valid or not
# pyYggdrasil does not allows the user to create such an interval,
# without raising an exception.
# But it could be possible that Yggdrasil creates one.
if(unitInterval.isValid() == True):
    print("The unit interval, we had created in the beginning is valid.")
else:
    raise ValueError("The unit interval we have created is invalid, this is big truble.")

print("The interval has more usefull fucntions.")
print("you can view them by typing 'help(pyYggdrasil.Interval)'")
print("Let's do it.")
#help(pyY.Interval)


# = H Y P E R C U B E S
#
# Hyper cubes are lists of intervals.
# They are used to describe a domain inside $\mathbf{R}^n$.
# They are implemented by storing several intervals in an array.

#
# You can create a hypercube, in serveral ways

# First you can create a hyper cube out of intervals
# The cube will have dimension 2, and the dimensions are copyis of the
# Interval that is provided with it.
hyperCube1 = pyY.HyperCube(2, customInterval)

# Lets look at the output
print("The cube we just created is: {}".format(hyperCube1))

# The cube also supports named arguments
HyperCube2 = pyY.HyperCube(n = 3, interval = customInterval2)

# If the interval is not specified then the unit interval is used.
unitCube = pyY.HyperCube(n = 5)
print("The 5 dimensional hyper cube is: {}".format(unitCube))

# It also supports the bracket operations
print("We now iterate through the second hyper cube we have generated.")
for i in range(len(HyperCube2)):
    print("   d = {} -> {}".format(i, HyperCube2[i]))

# For consistency it is not able to iterate through the cube
try:
    for dInt in HyperCube2:
        print(" looking at the interval: {}".format(dInt))
except:
    print("It was not possible to iterate over.")
    print("The reason was assignment was possible, but did not have any effect nor generated an error.")


#
# It is easy to generate a cube that is a copy of a single interval.
# It is even more easier to generate a cube which is based on the unit interval.
# Hower it is quite trubblesome to create an cube of different dimensions.
# The reason is that the cube is an imutable object.
# The only way to generate such a cube is to build it out of an invalid cube

# first of all, assignment is not supported
try:
    HyperCube2[0] = customInterval
except:
    print("It was not possible to assign the interval to a dimension of teh cube.")
    print("Because the cube is inmutable, in a sense.")

# First of all we have to create a cube, of the approriate dimension
# pyYggdrasil allows only one way to do it, it also restricts the usage of such a
# cube more than it is done in the C++ interface.

# Create an invalid cube of doimension 2
# This function is the only way of creating one.
# The argument is optional, if omitted a zero dimensional cube will be generated.
# Such a cube is invalid
invalidArea = pyY.CREAT_INVALID_CUBE(2)

# To show this is is invalid to generate a zero dimensional cube manualy
try:
    zeroDimVol = pyY.HyperCube(0, pyY.Interval())
except:
    print("Generation of a zero dimensional cube resulted in an exception.")
    print("C++ allows this.")

if(invalidArea.isValid() == False):
    print("We have now generated an invalid cube of dimension {}".format(len(invalidArea)))
else:
    raise ValueError("The cube was not invalid.")

# As it was mentioned pyYggrasil places several restriction onto it.
try:
    print("Print an invalid interval: {}".format(invalidArea[0]))
except:
    print("For example a dimension which is invalid can not be accessed.")

# But hwo we get the cube we want?
# For that we must first create the intervals we need.
firstDim  = pyY.Interval(0.2, 0.9)
secondDim = pyY.Interval(-10.0, 20)

# We have seen that the hyper cube does not support the assignment of an
# Interval, thus excahning it.
# It does not even support reading an invalid dimension.
# But it is posible to chgange an invalid dimension

# We can do this with the following function
invalidArea.setInvalidDimension(0, firstDim)

# The first dimension is now valid
if(invalidArea.isValid(0) == True):
    print("It was possible to exchange the first dimension.")
    print(" with the interval: {}".format(invalidArea[0]))
else:
    raise ValueError("IT was not possible to exchange the first domain.")

# But the second one and the cube as a whole is still invalid
if(invalidArea.isValid() == False and invalidArea.isValid(1) == False):
    print("The second dimension and the whole cube is still invalid.")
else:
    raise ValueError("We have a problem, the exchanging did not work.")

# It is not possible to exchange the domain again
# or any valid domain
try:
    invalidArea.setInvalidDimension(0, secondDim)
except:
    print("It was not possible to set the domain again.")
    print(" This is true for any other valid dimension.")

# Now we can exchange the second dimension
invalidArea.setInvalidDimension(1, secondDim)

if(invalidArea.isValid() == True):
    print("The cube is now valid.")
else:
    raise ValueError("The domain is not valid.")

print("Now we will print out the domai, using the [] operator.")
targetDom = (firstDim, secondDim)

for i in range(invalidArea.nDims()):    # nDims is a widly supported fucntion
    print("  [{}] -> {};  it should be {}".format(i, invalidArea[i], targetDom[i]))




