# Info 
This folder contains the script examples or would contain them, since there are none.
They are numbered and the user should go through them in ascending order.
The provide some non trivial examples that does something.
The only one that was provided is an old version that was incorporated into a Jupyter Notebook.

Before the user going through them, the notebooks and the manual should be consulted.



# Setup
In order to be run the scripts must load the pyYggdrasil module, by ``importing`` it.
For that the Python interpreter must be able to find the module, the manual discusses this further, so see there too.
This folder provides a symlink that points to the location where pyYggdrasil will most likely land after the compilation on a LINUX computer.
It may be invalid/broken, if the library is not yet compiled and reside in the assumed location.
Since the name of the library is platform dependent, it could be that your library is named differently.
So you have to create the symlink again, as described in the manual.

