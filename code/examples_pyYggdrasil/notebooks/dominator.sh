#!/bin/bash
#
# This script is used for starting the Sibyl engine.
# The name 'Dominator' comes from the series 'Psycho Pass' where also Sibyl comes from.
# The dominator is a scanner and a weapon.
#
##################


# The name of the Sybil notebook
SIBYL_FILE="Sibyl.ipynb"


# This are the parts for the listening
IP_TO_LISTEN="localhost"
PORT_TO_LISTEN="8888"
NO_BROWSER_COM="--no-browser"

#####################



#
# Starting

echo "==================================="
echo "   Enforcement mode: Lethal Eliminator."
echo "   Please aim carefully and eliminate the target."
echo " "

sleep 3

# Compose a special name of the script
SIBYL_FILE_EXTENSION="${SIBYL_FILE##*.}"
SIBYL_FILE_FILENAME="${SIBYL_FILE%.*}"

# Test if a argument was given
# if yes we add it to the unique file
if [ $# -eq 1 ] && [ "$1" != "REMOTE" ]
then
	NAME_EXTENSION=$(echo "$1" | tr ' ' '_')

	SIBYL_FILE_FILENAME="${SIBYL_FILE_FILENAME}_${NAME_EXTENSION}"

elif  [ $# -ge 1 ] && [ "$1" == "REMOTE" ]
then
	echo "On REMOTE MACHINE"

	echo ""
	echo "Did you start SCREEN?"
	echo ""
	sleep 3

	# Remove the remote
	shift

	if [ "$1" != "load" ]
	then
		IP_TO_LISTEN="${2:-192.168.1.120}"
	fi

	PORT_TO_LISTEN="8888"
	NO_BROWSER_COM="--no-browser"

	echo "IP: ${IP_TO_LISTEN}"
fi

# Compose a new name
DATE="$(date +%Y_%m_%d__%H_%M)"
SIBYL_FILE_TO_USE="${SIBYL_FILE_FILENAME}_${DATE}.${SIBYL_FILE_EXTENSION}"
SIBYL_FILE_TO_LOG="${SIBYL_FILE_FILENAME}_${DATE}.log"


# test if two arguments are given
if [ $# -eq 2 ]
then
	if [ "$1" == "load" ]
	then
		# We wahnt to load

		# This is an ugly hack
		SIBYL_FILE_TO_USE=$(basename "$2")

		# test if file exits
		if [ ! -e "${SIBYL_FILE_TO_USE}" ]
		then
			echo "The file \"${SIBYL_FILE_TO_USE}\" does not exits"
			echo "Abort"

			exit 1
		fi

		SIBYL_FILE_TO_LOG="${SIBYL_FILE_TO_USE%.*}.log"
	else
		echo "Unkown command \"$1\""
		echo "Abort"

		exit 1
	fi
else
	if [ -e "${SIBYL_FILE_TO_USE}" ]
	then
		echo "The file \"${SIBYL_FILE_TO_USE}\" already exists"
		echo "Abort."

		exit 1
	fi

	# We do not load, so we copy the template
	cp "${SIBYL_FILE}" "${SIBYL_FILE_TO_USE}"
fi



echo "load the file: ${SIBYL_FILE_TO_USE}"
sleep 1


if [ $? -ne 0 ]
then
	echo "Error while coping the file"
	echo "Abort."

	exit 1
fi

# We reapend because maybe we want to load
jupyter notebook --ip=${IP_TO_LISTEN} --port=${PORT_TO_LISTEN} ${NO_BROWSER_COM} "${SIBYL_FILE_TO_USE}" 2>&1  | tee --append "${SIBYL_FILE_TO_LOG}"

JUP_EX=$?


echo " "
echo " "
echo " "
echo "The targets thread judgement has been reappraised."
echo "Actions are no longer requiered."
echo "============================="
echo " "


exit ${JUP_EX}



















