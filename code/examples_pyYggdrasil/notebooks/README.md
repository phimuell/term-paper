# Info
These folder contains the interactive Jupyter Notebooks.
They allow a Mathematica like usage of Python.
They provide an “interactive” demonstration of the capabilities of py/Yggdrasil.
Before you go through them it is recommended to study the manual.


# Chapters
The demos are organized in several chapters.


## Chap.: 1
This shows how the utility functionality is used.
This is the interval or the hyper cube.
It provides the basics.


## Chap.: 2
This shows how the containers are used.
Containers are collections of several samples.
The interaction of the higher level functionality of Yggdrasil operated directly on them.
They also provides interaction with NumPy.


## Chap.: 5
This shows how the distributions are used.
The distributions are parametric random number distributions.
They are mostly used for testing and demonstration.
They provide a tight integration into Yggdrasil since they also operate on Containers.
They are not implemented in a very efficient way, they where designed to be extensible as possible.


## Chap.: 7
This chapter demonstrate how the tree and the tree sampler works.



# Setup
In order to be run the scripts must load the pyYggdrasil module, by ``importing`` it.
For that the Python interpreter must be able to find the module, the manual discusses this further, so see there too.
This folder provides a symlink that points to the location where pyYggdrasil will most likely land after the compilation on a LINUX computer.
It may be invalid/broken, if the library is not yet compiled and reside in the assumed location.
Since the name of the library is platform dependent, it could be that your library is named differently.
So you have to create the symlink again, as described in the manual.

