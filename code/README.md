# Info
This is the code folder of the py/Yggdrasil project.



# Folders
Here is a list and short description of the different folders that can be found here.


## cpp
This folder contains the C++ code of the project.
In a sense this _is_ Yggdrasil.
It also contains the C++ code for pyYggdrasil and the pybind11 as submodule.


## org_R
This is the original implementation of the method written in R by Daniel Meyer.
It contains a patched version of test routine, which was not super efficient.


## org_matlab
This is also an implementation of the method, but written in Matlab.
It was authored by Daniel Meyer.
It was also patched.


## plotting
This folder contains some small python scripts that can be used to plot results obtained by Yggdrasil’s plot generator program.
See the manual for more information.


## examples_pyYggdrasil
This folder hosts the practical part of the demonstrations.
It contains the Jupyter Notebooks that shows how pyYggdrasil should be used.
