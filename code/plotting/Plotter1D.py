# Thsi file contains code for plotting the dumped distribution
#

# Importing some files
import sys
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import argparse


#
# This function plots a one d Function.
# if a two d is passed, the second dimension is sptriped
# only a notice on the scrin is putputted
# this is for reaching compability with dirichlet.
def plot_1d(pathToDump, nameOfDist, saveFolder, xLog, yLog, xMin, xMax):

    pathToDumpI = Path(pathToDump)
    if(pathToDumpI.is_dir() == False):
        raise ValueError("The passed path'{}' is not a directory.".format(pathToDumpI.name))
    # end test

    if(nameOfDist is None):
        pName = pathToDumpI.stem
    else:
        pName = nameOfDist


    # open the data
    samplePath = pathToDumpI / "sample_x";
    if(samplePath.exists() == False):
        raise ValueError("The samples in '{}' do not exoists.".format(pathToDumpI.name))
    #endi if

    # load the data
    xSamples = np.loadtxt(samplePath, delimiter='\t')

    # Test if we must reach compability
    if(len(xSamples.shape) == 2):
        xSamples = xSamples[:, 0]
    #end extract
    if(len(xSamples.shape) != 1):
        raise ValueError("Thesample has the wrong dimension.")
    nSamples = xSamples.shape[0]

    # Load the estimated pdf
    estPDF_path = pathToDumpI / "pdf_est"
    if(estPDF_path.exists() == False):
        raise ValueError("The estimated pdf of '{}' does not exists.".format(pathToDumpI.name))
    #end test

    estPDF = np.loadtxt(estPDF_path, delimiter = '\t')

    if(len(estPDF) != nSamples):
        raise ValueError("The number of sampes {} differs from the number of estimated pdfs {}.".format(nSamples, len(estPDF)))

    hasExPdf = False
    exPDF_path = pathToDumpI / "pdf_ex"
    if(exPDF_path.exists() == True):
        hasExPdf = True
        exPDF = np.loadtxt(exPDF_path, delimiter = '\t')
    #endif


    # Plot the tree distribution
    plt.plot(
        xSamples,
        estPDF,
        label='estPDF'
        )

    # Optionaly plot the exact distribution
    if(hasExPdf == True):
        plt.plot(
            xSamples,
            exPDF,
            label='exPDF'
            )

    ax = plt.gca()

    # Set to log scale
    if(xLog == True):
        print('x axis is scalled logarithmically')
        ax.set_xscale('log')
    if(yLog == True):
        print('y axis is scalled logarithmically')
        ax.set_yscale('log')

    # set up labels
    plt.xlabel('x')
    plt.ylabel('PDF', rotation=0,
        horizontalalignment='right')
    ax.yaxis.set_label_coords(0.05, 1.03)
    plt.tick_params(left='off')

    # set up title
    plt.title('Plot of the distribution "{}"'.format(pName), fontsize=16)

    # set up generl layout
    plt.grid(True, which='major', axis='y', color='w', linewidth=2, linestyle='-')
    ax.set_facecolor('#D1D1E0')
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # See: https://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot/43439132#43439132
    lgd = plt.legend(bbox_to_anchor=(1.04,1), loc="upper left")

    # Restriuct to specific x Range
    if((xMin is not None) and (xMax is not None)):
        ax.set_xlim([xMin, xMax])

    # Only save if the name of dist parameter is given.
    if(nameOfDist is not None):
        plt.savefig(saveFolder / '{}_plot.eps'.format(nameOfDist), bbox_extra_artists=(lgd,), bbox_inches='tight')

    # Show the result
    plt.show()
#end: plot_1d


#
# This is the main function
def main():

    # Thsi is the parser object
    parser = argparse.ArgumentParser()

    #
    # Add some arguments that we are willing to support

    # Folder to load, this is a positional argument
    parser.add_argument('folder',
            help='Folder which shouold be processed.')

    # Using log scalling in the plot
    parser.add_argument('-x', '--logx',
            help='Enable logarithmic scalling on the x axis.',
            action='store_true' # This will turn this inbto a flag
            )
    parser.add_argument('-y', '--logy',
            help='Enable logarithmic scalling on the y axis.',
            action='store_true' # This will turn this inbto a flag
            )

    # This allows for restricting the x range
    parser.add_argument('--xstart',
            help='The minimal x value that should be plotted. To take effect, also --xend must be specified.',
            type=float)
    parser.add_argument('--xend',
            help='The maximal x value that should be plotted. To take effect, also --xstart must be specified.',
            type=float)

    # This will instruct the code to save it under that folder
    parser.add_argument('-d', '--savedir',
            help='This is the folder to save the .esp to, defaulted to .',
            default = '.'
            )

    # This is the name of the distribution, this is most important for printing
    parser.add_argument('-n', '--name',
            help='This is the name of the distribution, is important for plotting.'
            )

    #
    # Now parse the line
    pArgs = parser.parse_args()

    #
    # Call the name
    plot_1d(
            pathToDump = pArgs.folder,
            nameOfDist = pArgs.name,
            saveFolder = Path(pArgs.savedir),
            xLog = pArgs.logx,
            yLog = pArgs.logy,
            xMin = pArgs.xstart,
            xMax = pArgs.xend)
#end main function





if __name__ == "__main__":
    main()







