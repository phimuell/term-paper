# This is the result data base, it provides an uniform
# access to the results of of a single experiment.
# It is constructed with the path to a dat file,
# which is then parsed and loaded.
#

import numpy as np
import copy
import pathlib

# This is the databse class
# It allows an unform access to the result data
class Database:

    # ==================
    # Accessing

    # This is a nice name for the database
    def getNameOfExperiment(self):
        return copy.deepcopy(self.m_dbName)
    #end: getNameOfExperiment

    # This function returns the path from which *this
    # was constructed
    def getDatPath(self):
        return copy.deepcopy(self.m_dbPath)
    #end: getDatPath


    # This function returns the number of sampels
    # The where used to construct the tree.
    # This is a vector
    def getNSamples(self):
        return copy.deepcopy(self.m_fittingSizes)
    #end: getNSamples


    # This function returns the number of quering
    # Samples, this is the number of sampels
    # that where used to generate the MISE
    # This is a vector
    def getQSamples(self):
        return copy.deepcopy(self.m_querySizes)
    #end: getQSamples


    # This function returns the mean of the maximal
    # depths that were observed.
    # This is a vector, of each of the sizes one
    def getDepthMean(self):
        return copy.deepcopy(self.m_treeDepth_mean)
    #end: getDepthMean


    # This fucntion returns the standard deviation
    # of the maximal depth that were observed.
    # The ordering is the same as it was for the mean
    def getDepthStd(self):
        return copy.deepcopy(self.m_treeDepth_std)
    #end: getDepthStd


    # This fucntion returns the mean MISE
    # for each size that were observed
    # This returns a vector, the ordering
    # corresponds to the one of the getNSamples
    # function
    def getMiseMean(self):
        return copy.deepcopy(self.m_MISE_mean)
    #end: getMiseMean


    # This function returns the std
    # of the MISE for each size.
    def getMiseStd(self):
        return copy.deepcopy(self.m_MISE_std)
    #end: getMiseStd


    # This function returns the mean of the building
    # or fitting time that was needed.
    # This also returns a vector, that enscodes
    # the time for each size
    def getFittingTimeMean(self):
        return copy.deepcopy(self.m_fitTime_mean)
    #end: getFiitingTimeMean

    # This fucntion returns the std of the
    # building times and the different sizes.
    def getFittingTimeStd(self):
        return copy.deepcopy(self.m_fitTime_std)
    #end: getFittingTimeStd


    # This fucntion returns the mean evaluation time
    # that was needed. This also returns the size
    # for each experiment in a vector.
    def getEvalTimeMean(self):
        return copy.deepcopy(self.m_evalTime_mean)
    #end: getEvalTime


    # This function returns the std of the eval time.
    def getEvalTimeStd(self):
        return copy.deepcopy(self.m_evalTime_std)
    #end: getEvalTimeStd


    # This returns the mean number of nodes inside
    # the tree, again a vector a component represents
    # the mean of one experiment size.
    def getNumbOfNodesMean(self):
        return copy.deepcopy(self.m_nNodes_mean)
    #end: getNumbOfNodes


    # This returns the std of the number of nodes
    def getNumbOfNodesStd(self):
        return copy.deepcopy(self.m_nNodes_std)
    #end: getNumbOfNodes





    # ==================
    # Constructon

    # This is the main constructor.
    #   dbPath      Path to the file that should be read in
    #   dbName      This is the name of the db, is defaulted to "Unkown"
    def __init__(self, dbPath, dbName = 'Unkown'):

        # Save the db name
        self.m_dbName = dbName

        # Convert the dbPath to a real path and save it as member too
        self.m_dbPath = pathlib.Path(dbPath)
        #print(self.m_dbPath)


        # Test if the path names a file
        if(not self.m_dbPath.is_file()):
            raise ValueError("The file '{}' is not considered to be a file".format(self.m_dbPath))
        # end if: check if it is file

        # Load the data file as a matrix
        rawData = np.loadtxt(self.m_dbPath, delimiter='\t')

        # Safe the raw data as an internal member
        self.m_rawData = copy.deepcopy(rawData)

        ######
        # now we will extract the data and bring threm in a form that
        # makes them easier to process
        #

        # But first we have to figurring out how many different zizes we have
        allSizes = rawData[:, 0]

        # This gives us the unique sizes
        uniqeSizes = np.unique(allSizes)

        # Get the number of the unique sizes
        nUniqueSizes = len(uniqeSizes)

        # Save the sample or fitting sizes
        self.m_fittingSizes = copy.deepcopy(uniqeSizes)

        # Now get the index vector that gives us the informatioon where which sizes are located
        self.m_indexMap = dict()

        for i in uniqeSizes:
            self.m_indexMap[i] = np.where(allSizes == i)
        #end for(i): iterating through the unique sizes


        #######
        # Now create the private variables
        #

        #
        # Sample sizes
        # self.m_fittingSizes = np.zeros(nUniqueSizes)  # already created
        self.m_querySizes = np.zeros(nUniqueSizes)

        #
        # Tree paramneters
        self.m_treeDepth_mean = np.zeros(nUniqueSizes)
        self.m_treeDepth_std  = np.zeros(nUniqueSizes)
        self.m_nNodes_mean    = np.zeros(nUniqueSizes)
        self.m_nNodes_std     = np.zeros(nUniqueSizes)

        #
        # MISE values
        self.m_MISE_mean      = np.zeros(nUniqueSizes)
        self.m_MISE_std       = np.zeros(nUniqueSizes)

        #
        # Times
        self.m_fitTime_mean   = np.zeros(nUniqueSizes)
        self.m_fitTime_std    = np.zeros(nUniqueSizes)
        self.m_evalTime_mean  = np.zeros(nUniqueSizes)
        self.m_evalTime_std   = np.zeros(nUniqueSizes)


        ########
        # Now we iterate through the raw data we handle one
        # size in each iteration
        #

        for i in range(nUniqueSizes):

            # Load the current size
            currSize = uniqeSizes[i]

            # Get the index map
            currIdxMap = self.m_indexMap[currSize]

            # Load the current data
            currRawData = rawData[currIdxMap, ][0]

            # Get the query time, we are only interested in one, since
            # we assume that all are the same
            self.m_querySizes[i] = float(currRawData[0, 1])

            # Compute now the nbumber of nodes in the tree
            numOfNodes = currRawData[:, 6] + currRawData[:, 7] + currRawData[:, 8]

            # Compte the mean and the std of the number of nodes
            self.m_nNodes_mean[i] = np.mean(numOfNodes)
            self.m_nNodes_std[i]  = np.std(numOfNodes)

            # Get the maximal tree depth
            maxTreeDepth = currRawData[:, 4]

            # Save te mean and std of the tree depth
            self.m_treeDepth_mean[i] = np.mean(maxTreeDepth)
            self.m_treeDepth_std[i]  = np.std(maxTreeDepth)

            # Load the MISE
            currMISE = currRawData[:, 5]

            # Same the mean and std of the MISE
            self.m_MISE_mean[i] = np.mean(currMISE)
            self.m_MISE_std[i]  = np.std(currMISE)


            # Get the current fitting time
            currFittTimes = currRawData[:, 9]

            # Compute the mean and std of the fitting time
            self.m_fitTime_mean[i] = np.mean(currFittTimes)
            self.m_fitTime_std[i]  = np.std(currFittTimes)


            # Get the eval times
            currEvalTimes = currRawData[:, 10]

            # Save the mean and std
            self.m_evalTime_mean[i] = np.mean(currEvalTimes)
            self.m_evalTime_std[i]  = np.std(currEvalTimes)
        # end for(i): going through the different sizes (indexes)
    # end constructor

# end class: Database




