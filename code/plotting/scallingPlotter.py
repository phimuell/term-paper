# This file contains the plotter for the scalling experiment.
#

# Importing some files
import sys
from pathlib import Path
import numpy as np
import resultDB as db
import matplotlib.pyplot as plt


#
# This function iterates through the list of the given DBs
# And plot them into one plot of the mise
#
def plotDB_MISE(diffDB, expCalss, saveFolder = None):

    # Going through the db and plot them
    for s in diffDB:
        plt.errorbar(
            x = s.getNSamples(),
            y = s.getMiseMean(),
            yerr = s.getMiseStd(),
            capsize = 5, markeredgewidth = 1,
            label='{}'.format(s.getNameOfExperiment())
            )
    #end for(s)

    ax = plt.gca()

    # Set to log scale
    ax.set_yscale('log')
    ax.set_xscale('log')

    # set up labels
    plt.xlabel('Samples for Fitting')
    plt.ylabel('MISE', rotation=0,
        horizontalalignment='right')
    ax.yaxis.set_label_coords(0.05, 1.03)
    plt.tick_params(left='off')

    # set up title
    plt.title('Development of the MISE for different Sizes ({})'.format(expCalss), fontsize=16)

    # set up generl layout
    plt.grid(True, which='major', axis='y', color='w', linewidth=2, linestyle='-')
    ax.set_facecolor('#D1D1E0')
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # See: https://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot/43439132#43439132
    lgd = plt.legend(bbox_to_anchor=(1.04,1), loc="upper left")

    # plot specific
    #ax.set_ylim([0.1, 30])

    # save
    if(saveFolder is None):
        plt.savefig('{}_MISE.eps'.format(expCalss), bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
        plt.savefig(saveFolder / '{}_MISE.eps'.format(expCalss), bbox_extra_artists=(lgd,), bbox_inches='tight')

    plt.show()
#end: plotDBs

#
# Thsi function plots the average compute time.
#
def plotDB_fitTime(diffDB, expCalss, saveFolder = None):

    # Going through the db and plot them
    for s in diffDB:
        plt.errorbar(
            x = s.getNSamples(),
            y = s.getFittingTimeMean(),
            yerr = s.getFittingTimeStd(),
            capsize = 5, markeredgewidth = 1,
            label='{}'.format(s.getNameOfExperiment())
            )
    #end for(s)

    ax = plt.gca()

    # Set to log scale
    ax.set_yscale('log')
    ax.set_xscale('log')

    # set up labels
    plt.xlabel('Samples for Fitting')
    plt.ylabel('Mean Compute Time [s]', rotation=0,
        horizontalalignment='right')
    ax.yaxis.set_label_coords(0.05, 1.03)
    plt.tick_params(left='off')

    # set up title
    plt.title('Development of the mean compute time ({})'.format(expCalss), fontsize=16)

    # set up generl layout
    plt.grid(True, which='major', axis='y', color='w', linewidth=2, linestyle='-')
    ax.set_facecolor('#D1D1E0')
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # See: https://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot/43439132#43439132
    lgd = plt.legend(bbox_to_anchor=(1.04,1), loc="upper left")

    # plot specific
    #ax.set_ylim([0.1, 30])

    # show and save
    if(saveFolder is None):
        plt.savefig('{}_meanCompTime.eps'.format(expCalss), bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
        plt.savefig(saveFolder / '{}_meanCompTime.eps'.format(expCalss), bbox_extra_artists=(lgd,), bbox_inches='tight')

    plt.show()
#end: plotDBs

#
# This function plots the mean depth
#
def plotDB_treeDepth(diffDB, expCalss, saveFolder = None):

    # Going through the db and plot them
    for s in diffDB:
        plt.errorbar(
            x = s.getNSamples(),
            y = s.getDepthMean(),
            yerr = s.getDepthStd(),
            capsize = 5, markeredgewidth = 1,
            label='{}'.format(s.getNameOfExperiment())
            )
    #end for(s)

    #print("tree depth for {}:".format(s.getNameOfExperiment()))
    #print(s.getMiseMean

    ax = plt.gca()

    # Set to log scale
    #ax.set_yscale('log')
    ax.set_xscale('log')

    # set up labels
    plt.xlabel('Samples for Fitting')
    plt.ylabel('Mean Tree Depth', rotation=0,
        horizontalalignment='right')
    ax.yaxis.set_label_coords(0.05, 1.03)
    plt.tick_params(left='off')

    # set up title
    plt.title('Development of the tree depth ({})'.format(expCalss), fontsize=16)

    # set up generl layout
    plt.grid(True, which='major', axis='y', color='w', linewidth=2, linestyle='-')
    ax.set_facecolor('#D1D1E0')
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # See: https://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot/43439132#43439132
    lgd = plt.legend(bbox_to_anchor=(1.04,1), loc="upper left")

    # plot specific
    #ax.set_ylim([0.1, 30])

    # show and save
    if(saveFolder is None):
        plt.savefig('{}_meanTreeDepth.eps'.format(expCalss), bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
        plt.savefig(saveFolder / '{}_meanTreeDepth.eps'.format(expCalss), bbox_extra_artists=(lgd,), bbox_inches='tight')

    plt.show()
#end: plotDBs



#
# This function plots the mean number of leafs
#
def plotDB_nodes(diffDB, expCalss, saveFolder = None):

    # Going through the db and plot them
    for s in diffDB:
        plt.errorbar(
            x = s.getNSamples(),
            y = s.getNumbOfNodesMean(),
            yerr = s.getNumbOfNodesStd(),
            capsize = 5, markeredgewidth = 1,
            label='{}'.format(s.getNameOfExperiment())
            )
    #end for(s)

    ax = plt.gca()

    # Set to log scale
    #ax.set_yscale('log')
    ax.set_xscale('log')

    # set up labels
    plt.xlabel('Samples for Fitting')
    plt.ylabel('Mean Number of nodes', rotation=0,
        horizontalalignment='right')
    ax.yaxis.set_label_coords(0.05, 1.03)
    plt.tick_params(left='off')

    # set up title
    plt.title('Development of the node number ({})'.format(expCalss), fontsize=16)

    # set up generl layout
    plt.grid(True, which='major', axis='y', color='w', linewidth=2, linestyle='-')
    ax.set_facecolor('#D1D1E0')
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # See: https://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot/43439132#43439132
    lgd = plt.legend(bbox_to_anchor=(1.04,1), loc="upper left")

    # plot specific
    #ax.set_ylim([0.1, 30])

    # show and save
    if(saveFolder is None):
        plt.savefig('{}_meanNodeCount.eps'.format(expCalss), bbox_extra_artists=(lgd,), bbox_inches='tight')
    else:
        plt.savefig(saveFolder / '{}_meanNodeCount.eps'.format(expCalss), bbox_extra_artists=(lgd,), bbox_inches='tight')

    plt.show()
#end: plotDBs



# This is a combined plotting function
def plotDB_stat(diffDB, expCalss, saveFolder = None):

    # We silently return if the db list is empty
    if(len(diffDB) == 0):
        return
    # end: Handling empty db list

    # Call the plot functions
    plotDB_MISE(diffDB, expCalss = expCalss, saveFolder = saveFolder)
    plotDB_treeDepth(diffDB, expCalss = expCalss, saveFolder = saveFolder)
    plotDB_fitTime(diffDB, expCalss = expCalss, saveFolder = saveFolder)
    plotDB_nodes(diffDB, expCalss = expCalss, saveFolder = saveFolder)

#end: plotDB_stat


#
# This is the main function
def main():

    # Examine the argument
    if(len(sys.argv) < 2):
        print('usage: ', sys.argv[0], ' path_to_dataset1_of_a_scalling_experiment [Path for Saving ~ thisPlots]')

    # Get the folder where the experiment is saved
    sePath = Path(sys.argv[1])

    # Get the save folder that is used
    if(len(sys.argv) >= 3):
        saveFolder = Path(sys.argv[2])
    else:
        saveFolder = Path("thisPlot")
    # end: plot save folder

    if(sePath.is_dir() == False):
        raise ValueError("The given argument '{}' does not name a folder.".format(sys.argv[1]))
    # end if: test

    # Test if the save fodler exists if not crease it
    if(saveFolder.exists() == True):
        if(saveFolder.is_dir() == False):
            raise ValueError("The given path for saving the plots '{}' exists, but is not a folder.".format(saveFolder.name))
        # end raise if:
    else:
        # Create the folder
        saveFolder.mkdir()
    #end: handling the save folder


    #
    #   L O A D
    #
    # Load the files from disc and create data bases
    # Also collect them in lists to better handling them
    #


    if(Path(sePath / 'Gauss').is_dir() == True):
        #
        # Tha gauss folder is present

        # Gauss 2D
        g2D_const_median = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_2d_paper.const_median.dat', dbName = 'const median')
        g2D_const_size   = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_2d_paper.const_size.dat', 'const size')
        g2D_lin_median   = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_2d_paper.lin_median.dat', 'lin median')
        g2D_lin_size     = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_2d_paper.lin_size.dat', 'lin size')

        # Gauss 4D
        g4D_const_median = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_4d.const_median.dat', 'const median')
        g4D_const_size   = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_4d.const_size.dat', 'const size')
        g4D_lin_median   = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_4d.lin_median.dat', 'lin median')
        g4D_lin_size     = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_4d.lin_size.dat', 'lin size')

        # Gauss 7D
        g7D_const_median = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_7d.const_median.dat', 'const median')
        g7D_const_size   = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_7d.const_size.dat', 'const size')
        g7D_lin_median   = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_7d.lin_median.dat', 'lin median')
        g7D_lin_size     = db.Database(sePath / 'Gauss/StandardScalingExper.gauss_7d.lin_size.dat', 'lin size')

        # Collect everything in a list
        g2D_list = (g2D_lin_size, g2D_lin_median, g2D_const_size, g2D_const_median)
        g7D_list = (g7D_lin_size, g7D_lin_median, g7D_const_size, g7D_const_median)
        g4D_list = (g4D_lin_size, g4D_lin_median, g4D_const_size, g4D_const_median)

    else:
        #
        # Create the empty lists in case the gauss folder is not preset

        print("The folder '{}' does not contain a 'Gauss' folder.".format(sePath))
        g2D_list = ()
        g7D_list = ()
        g4D_list = ()
    #end: load Gauss

    if(Path(sePath / 'Dirichlet').is_dir() == True):
        #
        # The Dirichlet folder is present
        # Notice that this is is the new convention of dirichlet, that follows Daniel's paper.

        # Dirichlet 2D
        d2D_const_median = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_2d.const_median.dat', 'const median')
        d2D_const_size   = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_2d.const_size.dat', 'const size')
        d2D_lin_median   = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_2d.lin_median.dat', 'lin median')
        d2D_lin_size     = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_2d.lin_size.dat', 'lin size')

        # Dirichlet 4D
        d4D_const_median = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_4d.const_median.dat', 'const median')
        d4D_const_size   = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_4d.const_size.dat', 'const size')
        d4D_lin_median   = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_4d.lin_median.dat', 'lin median')
        d4D_lin_size     = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_4d.lin_size.dat', 'lin size')

        # Dirichlet 7D
        d7D_const_median = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_7d.const_median.dat', 'const median')
        d7D_const_size   = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_7d.const_size.dat', 'const size')
        d7D_lin_median   = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_7d.lin_median.dat', 'lin median')
        d7D_lin_size     = db.Database(sePath / 'Dirichlet/StandardScalingExper.dirichlet_7d.lin_size.dat', 'lin size')

        # Create the list of the databases to collect them
        d2D_list = (d2D_lin_size, d2D_lin_median, d2D_const_size, d2D_const_median)
        d4D_list = (d4D_lin_size, d4D_lin_median, d4D_const_size, d4D_const_median)
        d7D_list = (d7D_lin_size, d7D_lin_median, d7D_const_size, d7D_const_median)
    else:
        #
        # Create the empty lists in case the Dirichlet folder is not preset

        print("The folder '{}' does not contain a 'Dirichlet' folder.".format(sePath))
        d2D_list = ()
        d7D_list = ()
        d4D_list = ()
    #end if: Handling Dirichlet


    if(Path(sePath / 'Gamma').is_dir() == True):
        # \alpha = 1    \beta = 2
        cg1_const_median = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDist_1_2.const_median.dat', 'const median')
        cg1_const_size   = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDist_1_2.const_size.dat', 'const size')
        cg1_lin_median   = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDist_1_2.lin_median.dat', 'lin median')
        cg1_lin_size     = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDist_1_2.lin_size.dat', 'lin size')

        # \alpha = 9    \beta = 0.5
        cg2_const_median = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDist_9_05.const_median.dat', 'const median')
        cg2_const_size   = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDist_9_05.const_size.dat', 'const size')
        cg2_lin_median   = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDist_9_05.lin_median.dat', 'lin median')
        cg2_lin_size     = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDist_9_05.lin_size.dat', 'lin size')

        # Paper
        #   \alpha = 2/3
        #   \beta  = 50
        cg3_const_median = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDistPaper.const_median.dat', 'const median')
        cg3_const_size   = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDistPaper.const_size.dat', 'const size')
        cg3_lin_median   = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDistPaper.lin_median.dat', 'lin median')
        cg3_lin_size     = db.Database(sePath / 'Gamma/StandardScalingExper.gammaDistPaper.lin_size.dat', 'lin size')


        cg1_list = (cg1_lin_size, cg1_lin_median, cg1_const_size, cg1_const_median)
        cg2_list = (cg2_lin_size, cg2_lin_median, cg2_const_size, cg2_const_median)
        cg3_list = (cg3_lin_size, cg3_lin_median, cg3_const_size, cg3_const_median)
    else:
        #
        # No gamma folder was present
        print("The folder '{}' does not contain a 'Gamma' folder.".format(sePath))

        cg1_list = ()
        cg2_list = ()
        cg3_list = ()
    #end: handling the gamma distribution


    if(Path(sePath / 'Outlier').is_dir() == True):
        # Outlier from paper
        #   \mu         = 0
        #   \sigma_1    = 1
        #   \sigma_1    = 0.1
        #   \alpha      = 0.1
        out1_const_median = db.Database(sePath / 'Outlier/StandardScalingExper.outlier_decay.const_median.dat', 'const median')
        out1_const_size   = db.Database(sePath / 'Outlier/StandardScalingExper.outlier_decay.const_size.dat', 'const size')
        out1_lin_median   = db.Database(sePath / 'Outlier/StandardScalingExper.outlier_decay.lin_median.dat', 'lin median')
        out1_lin_size     = db.Database(sePath / 'Outlier/StandardScalingExper.outlier_decay.lin_size.dat', 'lin size')

        out1_list = (out1_lin_size, out1_lin_median, out1_const_size, out1_const_median)
    else:
        #
        # No Outlier folder was present
        print("The folder '{}' does not contain an 'Outlier' folder.".format(sePath))

        out1_list = ()
    #end: handling outlier distribution


    if(Path(sePath / 'Uniform').is_dir() == True):

        # The spyky linear from the paper
        spUni_const_median = db.Database(sePath / 'Uniform/StandardScalingExper.spikyUniformPaper.const_median.dat', 'const median')
        spUni_const_size   = db.Database(sePath / 'Uniform/StandardScalingExper.spikyUniformPaper.const_size.dat', 'const size')
        spUni_lin_median   = db.Database(sePath / 'Uniform/StandardScalingExper.spikyUniformPaper.lin_median.dat', 'lin median')
        spUni_lin_size     = db.Database(sePath / 'Uniform/StandardScalingExper.spikyUniformPaper.lin_size.dat', 'lin size')

        # Uniform 2D
        uni2D_const_median = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform2D.const_median.dat', dbName = 'const median')
        uni2D_const_size   = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform2D.const_size.dat', 'const size')
        uni2D_lin_median   = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform2D.lin_median.dat', 'lin median')
        uni2D_lin_size     = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform2D.lin_size.dat', 'lin size')

        # Uniform 4D
        uni4D_const_median = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform4D.const_median.dat', 'const median')
        uni4D_const_size   = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform4D.const_size.dat', 'const size')
        uni4D_lin_median   = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform4D.lin_median.dat', 'lin median')
        uni4D_lin_size     = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform4D.lin_size.dat', 'lin size')

        # Uniform 7D
        uni7D_const_median = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform7D.const_median.dat', 'const median')
        uni7D_const_size   = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform7D.const_size.dat', 'const size')
        uni7D_lin_median   = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform7D.lin_median.dat', 'lin median')
        uni7D_lin_size     = db.Database(sePath / 'Uniform/StandardScalingExper.multiUniform7D.lin_size.dat', 'lin size')

        spUni_list = (spUni_lin_size, spUni_lin_median, spUni_const_size, spUni_const_median)
        uni2D_list = (uni2D_lin_size, uni2D_lin_median, uni2D_const_size, uni2D_const_median)
        uni4D_list = (uni4D_lin_size, uni4D_lin_median, uni4D_const_size, uni4D_const_median)
        uni7D_list = (uni7D_lin_size, uni7D_lin_median, uni7D_const_size, uni7D_const_median)

    else:
        #
        # No Uniform folder was present
        print("The folder '{}' does not contain an 'Uniform' folder.".format(sePath))

        spUni_list = ()
        uni2D_list = ()
        uni4D_list = ()
        uni7D_list = ()
    #end: handling the gamma distribution



    #
    # Call the plot function
    #

    plotDB_stat(g7D_list, expCalss = 'gauss7D', saveFolder = saveFolder)
    plotDB_stat(g4D_list, expCalss = 'gauss4D', saveFolder = saveFolder)
    plotDB_stat(g2D_list, expCalss = 'gauss2D', saveFolder = saveFolder)

    plotDB_stat(d7D_list, expCalss = 'dirichlet7D', saveFolder = saveFolder)
    plotDB_stat(d4D_list, expCalss = 'dirichlet4D', saveFolder = saveFolder)
    plotDB_stat(d2D_list, expCalss = 'dirichlet2D', saveFolder = saveFolder)

    plotDB_stat(cg1_list, expCalss = 'Gamma_1_2', saveFolder = saveFolder)
    plotDB_stat(cg2_list, expCalss = 'Gamma_9_05', saveFolder = saveFolder)
    plotDB_stat(cg3_list, expCalss = 'Gamma_paper', saveFolder = saveFolder)

    plotDB_stat(out1_list, expCalss = 'Outlier_paper', saveFolder = saveFolder)

    plotDB_stat(spUni_list, expCalss = 'spikyUniformPaper', saveFolder = saveFolder)
    plotDB_stat(uni2D_list, expCalss = 'Uniform2D', saveFolder = saveFolder)
    plotDB_stat(uni4D_list, expCalss = 'Uniform4D', saveFolder = saveFolder)
    plotDB_stat(uni7D_list, expCalss = 'Uniform7D', saveFolder = saveFolder)
#end main function





if __name__ == "__main__":
    main()







