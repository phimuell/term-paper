# Info
This folder contains all the papers that relates to the project.


## Chi2
This are several paper.
They are mostly related with the Chi2 Test that is implemented for the test.
The paper were consulted for reference about the correct implementation of the Chi2 tests.


## IQ_Agent
This is a paper about an online quantile approximation scheme.
It was not implemented, since some changes where made in the internal working of the process.
That also relates to the Chi2 tests.


## Meyer_DET
This is the original paper about the method that was implemented.
It describes the method.
It also contains a large series of tests that are done for judging the accuracy and the performance on the method itself.
The scaling experiments of Yggdrasil are based on them.


## Meyer_DET_generator
This is a follow up paper to the DET paper, also authored by Daniel Meyer.
It describes a generator that is based on the computed tree.
It is also referred to as “generator paper”.


## R_Integration
This is a set of papers, also a book that deals with rcpp.
Rcpp is similar to pybind11, but allows integration in R.
This was not done, but could be used.
However a small test showed that is usage seams a bit more cumbersome than pybind11.


## RTFM
It contains a study about the usages of manuals in general.
You should read it.



