# INFO
This is the repository of Yggdrasil.
It is the “Seminar in Fluid Dynamics für CSE” in the autumn semester 2019 at ETH Zürich.
This project was done by Philip Müller (phimuell@student.ethz.ch or philip.paul.mueller@alumni.ethz.ch).
It is based on a paper by Daniel Werner Meyer-Massetti, paer can be found in the “paper” folder.

Yggdrasil implements the described method along with a generator that uses the generated tree.
The implementation is done in C++.
An interface in C++ and in Python, by using pybin11, https://github.com/pybind/pybind11, is provided.

Along with the code a manual that explains some details is provided.



# Licence
At other points is is also mentioned, but I want to mentioned it here too.
The code, I mean the code I have written is licensed under the GNU PGL version 3 or newer.



# Errors, Bugs and Warrenty
I am pretty confident that the code is mostly bug free.
I have written several small programs to verify these.
I also did some scaling experiments to see if it is working correctly.
I have tried my best to ensure that the code is working correctly, but I will not guarantee it.
I will also deny all responsibility of errors that arise when using this software.
It is your responsibility alone.
In the testing I have mostly focused on Gauss distributions.



# Branches
I have deleted all branches of the Repro.
They where mostly old and not in use anymore.
Since I like merging with the “--no-ff” option, you will see where the branches where and for what they were used.
For some branches I deem of “historical” interest I created a tag, with a small description.



# Getting Started
As written above a manual is provided.
Before doing anything else the user should read it completely.
The manual is not a complete manual, it only covers the basic usage.

After the user has read the manual the examples should be studied.
The examples are a series of Jupyter Notebooks that demonstrate the usage of pyYggdrasil, the Python bindings for Yggdrasil.
They are also a good reference for the C++ interface since pyYggdrasil was designed as a wrapper around Yggdrasil.
However there are certain behavior in which the two differs, most of them are mentioned in the manual.
The basic difference is that pyYggdrasil is much more protective that Yggdasil is.

The last part and the ultimate documentation that is provided is the source code itself, after all this is the documentation that Yggdrasil itself uses when its deciding what to do next.
So for an indeed understanding of the inner working of Yggdrasil there is no way around reading the source. 
Or as Plato put it “Let None But Geometers Enter Here”.

The code was extensively documented.
It explains way something is trivial and explains how complicated stuff works.
The interface of the code itself was documented with doxygen,
So you can run it through the doxygen parser and get a documentation.
Note that I am not a doxygen expert, I am a novice at best, so do not expect something like Eigen.
But you can still read the code all functions and their arguments are described at least somewhere.
It needs time, as everything needs time.



# Folder Structure
In this section we will explain the folder structure.
You will find similar files in every folder so navigating should not be a problem.
Note that as everything, doing something well, requires time and devotion and a manual or a documentation can only serve as a starting point, nothing more.


## runtime_data
This folder contains the result of the scaling experiments that were done to test the working of the code.
However the project suffers something like a dark age.
See the README file of the folder for more information about it.


## doc
It contains some part of the documentation.
Such as the manual and the specifications.
For more information see the README for more information.


## paper
This folder contains the papers Yggdrasil is based of.


## code
This folder contains the code that relates to Yggdrasil.
It not only contains Yggdrasil, but also the demonstration code.
See the README file for more information.



# Avatar
The avatar logo image of Yggdrasil is a picture taken from a manuscript from the 17th century.
The source of the image is "https://commons.wikimedia.org/wiki/File:AM_738_4to_Yggdrasill.png".
The file is called ".avatar.jpg".




