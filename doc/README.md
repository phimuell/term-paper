#info
This folder contains some part of the documentation.

## project_admin
This folder contains the files that are related to the work at IFD, the ETH institute that offers the seminar in the first place, itself.
It also contains the “project description” which is mostly out of data, since the project description was far too much for such a work.


## manual
This is a manual of py/Yggdrasil.
It is also something like the final report that is requested by the IFD institute.
Since the project was mostly “implement the method” writing a “scientific” report is a bit meaningless, it was decided that a manual and some training materials, located in the code folder, was better suited.
It is important that you read the manual before using Yggdrasil.


## specifications
This folder contains a document that declares the specification of Yggdrasil.
It was written in the beginning, before the coding begun.
It is an important note that the specification targets the full scope of the original project.
So a lot of the functionality that is described there is not implemented.
However Yggdrasil was designed to meet these requirements even after the reduction of the scope.
If it is decided to extend Yggdrasil, the specification is a document that must be read very carefully.


## presentation
This folder contains the final presentation of the project.


## handover_certificate
This is a small description of the final state of the project.
The document was written in German.
It does not contain any real new information, but shortly summarize what was already said.
Further it is only intended for internal use.


