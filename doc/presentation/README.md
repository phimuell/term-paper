# Info
This is the final presentation of the project.
It was done with Libre Office.
The presentation has note pages that contains the text that is associated with each page.
The presentation does not feature animation, each effect has its own page.


## PDF
There is a PDF print out of the presentation, that also contains the note pages.


## Demo
There is a demo to the presentation.
It can be found in the notebook folder together with the other tutorials.
